<?php
class Admin extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function login($email,$password)
    {
        $results = $this->db->query("SELECT * FROM Admin WHERE email = ? AND password = ?",
            array( $email, hash("sha512", $password) )
        );
        return $results->result_array();
    }
}
