<?php

class Merchant extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function login($username, $password)
    {
        $results = $this->db->query(
            "   SELECT
                merchantid
                , businessName
                , timestamp
                , status
                , packageId
                , shouldLaunchQuicksetup
                , trial_end
                , enabled
                FROM Merchant
                WHERE username = ? AND password = ?
            ",
            array(
                $username,
                hash("sha512", $password)
            )
        );

        return $results->result_array();
    }

    public function register($posts = NULL)
    {
        if ($posts['package'] == 'RUSH Pro') {
            $packageID = 1;
            $status = 'Trial';
        } else if ($posts['package'] == 'Rush Basic') {
            $packageID = 2;
            $status = 'Trial';
        }

        $data = array(
            'packageId' => $packageID,
            'business_system_link' => $this->session->userdata('business_system_link'),
            'businessName' => addslashes($this->session->userdata('business_name')),
            'businessType' => $this->session->userdata('business_type'). addslashes($this->session->userdata('business_type_other')),
            'businessProgramObjective' =>  addslashes($this->session->userdata('program_objective')). addslashes($this->session->userdata('program_objective_other')),
            'PackageAddons' => $this->session->userdata('packageaddons'),
            'business_branches' => $this->session->userdata('business_branches'),
            'address' =>  addslashes($this->session->userdata('business_location')),
            'averageCustomer' => $this->session->userdata('customer_no'),
            'loyaltyProgramReason' =>  addslashes($this->session->userdata('loyalty_program')),
            'TIN' => $this->session->userdata('business_tin'),
            'company_reg_no' => $this->session->userdata('business_regno'),
            'personName' => $this->session->userdata('person_name'),
            'personPosition' => $this->session->userdata('person_position'),
            'contactPerson' => $this->session->userdata('person_name'),
            'contactPersonEmail' => $this->session->userdata('person_email'),
            'contactPersonNumber' => $this->session->userdata('person_contact'),
            'username' => $this->session->userdata('person_email'),
            'password' => hash("sha512", $this->session->userdata('person_password')),
            'timestamp' => date('Y-m-d H:i:s'),
            'trial_end' => date('Y-m-d H:i:s', strtotime("+30 days")),
            'status' => $status,
            'shouldLaunchQuicksetup' => 1
        );
        $this->db->insert('Merchant', $data);
        $new_id = $this->db->insert_id();
        $update_data['account_number'] = $this->generateAccountNumber($new_id);
        $this->db->update('Merchant', $update_data, ['merchantid' => $new_id]);
    }

    private function generateAccountNumber($id)
    {
      $account_number = sprintf('%04d', $id );
      $account_number .= '-' . sprintf('%06d', rand(1,999999));
      $account_number .= '-' . date('my');
      return $account_number;
    }

    public function checkemail($email = NULL)
    {
        $results = $this->db->query("SELECT username, contactPerson FROM Merchant WHERE username = '".addslashes($email)."'");
        return $results->result_array();
    }

    public function resetpassword($email = NULL, $password = NULL)
    {
        $results = $this->db->query("UPDATE Merchant SET password = '".hash("sha512", $password)."', requireChangePassword = 1 WHERE username = '".addslashes(trim($email))."'");
    }

    public function checkbusinessname($name = NULL)
    {
        $results = $this->db->query("SELECT businessName FROM Merchant WHERE businessName = '".addslashes(trim($name))."'");
        return $results->result_array();
    }

    public function creditcard()
    {
        $this->db->query("UPDATE Merchant SET creditcardno = '".$this->session->userdata('creditcardno')."', creditcardholder = '".$this->session->userdata('creditcardholder')."', creditcardcode = '".$this->session->userdata('creditcardcode')."', creditcardmonth = '".$this->session->userdata('creditcardmonth')."', creditcardyear = '".$this->session->userdata('creditcardyear')."' WHERE merchantid = ".$this->session->userdata('merchant_id')."");
    }

    public function subuserlogin($username, $password)
    {
        $results = $this->db->query(
            "   SELECT
                m.merchantid
                , u.id
                , u.name
                , m.timestamp
                , m.status
                , m.packageId
                , m.trial_end
                , m.enabled
                FROM Merchant m
                JOIN merchant_users u
                ON m.merchantid = u.merchant_id
                WHERE u.email = ? AND u.password = ? AND u.is_deleted = 0
            ",
            array(
                $username,
                hash("sha512", $password)
            )
        );

        return $results->result_array();
    }
}
