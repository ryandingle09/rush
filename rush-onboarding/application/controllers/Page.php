<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Rush\Citolaravel\Helpers\MigrationHelper;

class Page extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
		$this->load->model('Admin');
		$this->load->model('Merchant');
	}

	public function index()
	{
		if (getenv('NO_SIGNUP')) {
			redirect(MigrationHelper::getOnboardingPrefix().'/sign-in');
		}
		$this->load->view("template", array( 'view' => 'pages/index' ));
	}

	public function products()
	{
		$this->load->view("template", array( 'view' => 'pages/products' ));
	}

	public function aboutus()
	{
		$this->load->view("template", array( 'view' => 'pages/about-us' ));
	}

	public function contactus()
	{
        $this->load->library('form_validation');
        $this->load->helper('form');

        $data['contact_form_show'] = 'show';
        $data['contact_message_show'] = 'hide';
        $data['packages'] = [
        	'a1' => 'RUSH Basic (White Label Punch Card Program)',
        	'a2' => 'RUSH Pro (White Label Points Program)'
        ];
        $data['objectives'] = [
        	'b1' => 'Enhance customer engagement',
        	'b2' => 'Improve customer retention',
        	'b3' => 'Increase frequency of customer visits',
        	'b4' => 'Drive revenue per transaction',
        	'b5' => 'Gather insights and understand customer behavior',
        	'b6' => 'Promote and push new products and services',
        	'b7' => 'Others'
        ];

		if ($this->input->post('submit') != NULL) {

			$this->load->library('emailtemplate');
			$this->load->library('email');
			$this->load->helper('email');
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('company', 'Company Name', 'required');
            $this->form_validation->set_rules('mobile', 'Mobile Number', 'required');
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');
            $this->form_validation->set_rules('g-recaptcha-response', 'Captcha', 'callback_recaptcha_check');

            if ($this->form_validation->run() == TRUE)
            {
                $data['name'] = $this->input->post('name');
                $data['company'] = $this->input->post('company');
                $data['industry'] = $this->input->post('industry');
                $data['mobile'] = $this->input->post('mobile');
                $data['email'] = $this->input->post('email');
                $data['objective'] = $this->input->post('objective[]');
                $data['package'] = $this->input->post('package');
                $data['message'] = $this->input->post('message');

                $this->email->from('info@rush.ph', 'Rush');
                $this->email->to( getenv('REQUEST_DEMO_TO') );
                $this->email->cc( getenv('REQUEST_DEMO_CC') );
                $this->email->bcc( getenv('REQUEST_DEMO_BCC') );
                $this->email->subject('[RUSH] Request A Demo');
                $this->email->set_mailtype("html");
                $this->email->message($this->emailtemplate->contactus($data));
                $this->email->send();

                $this->email->from('info@rush.ph', 'Rush');
                $this->email->to($this->input->post('email'));
                $this->email->subject('Thank you for your RUSH interest!');
                $this->email->set_mailtype("html");
                $this->email->message($this->emailtemplate->contactUsConfirmation($data['name']));
                $this->email->send();

                $this->session->set_flashdata('contactmessage', '<p style="font-size: 20px;font-weight:bold;">Thank you for your RUSH interest!<br/><br/>An account manager will get in touch with you shortly to arrange the demo of the loyalty program solution for your needs.</p>');

                $data['contact_form_show'] = 'hide';
        		$data['contact_message_show'] = 'show';
            }
		}
		$this->load->view("template", array( 'view' => 'pages/contact-us', 'data' => $data ));
	}

	public function recaptcha_check($captcha)
    {
        $verify_captcha = json_decode(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='. getenv('G_RECAPTCHA_SECRET') .'&response='. $captcha));
        if($verify_captcha) {
            if (isset($verify_captcha->success) && $verify_captcha->success) {
                return true;
            }
        }

        $this->form_validation->set_message('recaptcha_check', 'The Captcha field is invalid.');
        return false;
    }


	public function signin()
	{
		$isPunchcardPackage = true;
		if ($this->input->post('submitchangepassword') != NULL) {

			$email = $this->input->post('emailpassword',TRUE);

			$rs = $this->Merchant->checkemail($email);


			if (count($rs)) {

				$password = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 8);
				$this->Merchant->resetpassword($email, $password);

				$this->load->library('emailtemplate');
				$this->load->library('email');
				$this->load->helper('email');

				$this->email->from('info@rush.ph', 'Rush');
				$this->email->to(trim(strtolower($email)));
				$this->email->subject('Forgot password');
				$this->email->set_mailtype("html");
				$this->email->message($this->emailtemplate->resetpassword($email, $rs[0]['username'], $rs[0]['contactPerson'], $password));
				$this->email->send();

				$this->session->set_flashdata('loginmessage', 'Please check your email for password reset instructions');

			} else {

				$this->session->set_flashdata('loginmessage', 'Invalid email please try again');

			}

		}

		if ($this->input->post('submit') != NULL) {
			$username = $this->input->post('email',TRUE);
			$password = $this->input->post('password',TRUE);

			$username = trim($username);
			$password = trim($password);

			$username_length = strlen($username);
			$password_length = strlen($password);

			$message = 'Invalid username or password';

			$authenticated = false;
			$user_data = array(
				'user_id' => 0,
				'user_type' => 0,
				'login_name' => '',
				'forceQuickSetup' => 0,
				'package_id' => 0,
				'app_id' => getenv('APP_PREFIX')
			);

			if (($username_length >= 3 && $username_length <= 128) && ($password_length >= 6 && $password_length <= 32)) {
				if (filter_var($username,FILTER_VALIDATE_EMAIL)) {

					//Check first if Admin Account
					$result = $this->Admin->login($username,$password);
					$info = count($result) > 0 ? $result[0] : NULL;
					if ($info != NULL && is_array($info)) {
						if ( $info['is_active'] ) {
							$user_data['admin_id'] = $info['id'];
							$user_data['admin_name'] = $info['firstname'] . ' ' . $info['lastname'];
							$user_data['admin_type'] = 1;
							$user_data['is_admin'] = TRUE;
							$this->session->set_userdata($user_data);
							redirect(MigrationHelper::getAppPrefix().'/admin');
						}
					} else {

						$result = $this->Merchant->login($username,$password);
						$info = count($result) > 0 ? $result[0] : NULL;
						if ($info != NULL && is_array($info)) {
							$expired = false;
							if ($info['status'] === 'Trial') {
								// if (date('Y-m-d', strtotime('+30 days', strtotime($info['timestamp']))) <= date('Y-m-d')) {
								if ( $info['trial_end'] <= date('Y-m-d') ) {
									$message = 'Your trial has expired';
									$expired = true;
								}
							}
							if ( $info['enabled'] == 0 ) {
								$message = 'Your account is disabled.';
								$expired = true;
							}
							if ($expired == false) {
							    $user_data['user_id'] = $info['merchantid'];
							    $user_data['login_name'] = $info['businessName'];
							    $user_data['user_type'] = 1;
							    $user_data['forceQuickSetup'] = $info['shouldLaunchQuicksetup'];
							    $user_data['package_id'] = $info['packageId'];
								$user_data['is_admin'] = FALSE;
							    $authenticated = true;
							    $isPunchcardPackage = ($info['packageId'] == 1) ? false : true;
						  }
						  
						} else {

							$result = $this->Merchant->subuserlogin($username,$password);
							$info = count($result) > 0 ? $result[0] : NULL;
							if ($info != NULL && is_array($info)) {
								$expired = false;
								if ($info['status'] === 'Trial') {
									if ( $info['trial_end'] <= date('Y-m-d') ) {
										$message = 'Your trial has expired';
										$expired = true;
									}
								}
								if ( $info['enabled'] == 0 ) {
									$message = 'Your merchant is disabled.';
									$expired = true;
								}
								if ($expired == false) {
								    $user_data['user_id'] = $info['merchantid'];
								    $user_data['login_name'] = $info['name'];
								    $user_data['user_type'] = 2;
								    $user_data['subuser_id'] = $info['id'];
								    $user_data['forceQuickSetup'] = false;
								    $user_data['package_id'] = $info['packageId'];
									$user_data['is_admin'] = FALSE;
								    $authenticated = true;
								    $isPunchcardPackage = ($info['packageId'] == 1) ? false : true;
							  }
							}
						}
					}
				}
			}

			if ($authenticated) {
				$this->session->set_userdata($user_data);
				if ($isPunchcardPackage) {
					redirect(MigrationHelper::getAppPrefix() .'/dashboard');
				} else {
					redirect(MigrationHelper::getAppPrefix() .'/dashboard');
				}
			} else {
				$this->session->set_flashdata('loginmessage', $message);
			}
		}

		$this->load->view("template", array( 'view' => 'pages/sign-in' ));
	}

	public function signup()
	{
		$this->session->unset_userdata('emailtaken');
		$this->session->unset_userdata('email_sent');
        $this->load->library('form_validation');

		if ($this->input->post('submit') != NULL) {

			$rs_email = $this->Merchant->checkemail($this->input->post('person_email'));
			$rs_businessname = $this->Merchant->checkbusinessname($this->input->post('business_name'));
            $this->form_validation->set_rules('g-recaptcha-response', 'Captcha', 'callback_recaptcha_check');

			if (count($rs_email) || count($rs_businessname) || $this->form_validation->run() == FALSE) {

                if(count($rs_email)) {
                    $this->session->set_userdata('emailtaken', true);
                }

                if(count($rs_businessname)) {
                    $this->session->set_userdata('businessnametaken', true);
                }

                if($this->form_validation->run() == FALSE) {
                    $this->session->set_userdata('invalid_captcha', true);
                }

			} else {

				$removed = array(".", "'", "!", "(", ")", "@", "+", '"', ",", "/", "?");
				$business_system_link = str_replace($removed, "", trim($this->input->post('business_name')));
				$business_system_link = str_replace(' ', '-', $business_system_link);
				$business_system_link = strtolower($business_system_link);

				$person_contact = $this->input->post('person_contact1').'-'.$this->input->post('person_contact2');

				$input = array(
					'business_system_link' => $business_system_link,
					'business_name' => trim($this->input->post('business_name')),
					'business_type' => $this->input->post('business_type'),
					'business_type_other' => $this->input->post('business_type_other'),
					'business_tin' => $this->input->post('business_tin'),
					'business_regno' => $this->input->post('business_regno'),
					'person_name' => $this->input->post('person_name'),
					'person_contact' => $person_contact,
					'person_position' => $this->input->post('person_position'),
					'person_email' => $this->input->post('person_email'),
					'business_location' => $this->input->post('business_location'),
					'customer_no' => $this->input->post('customer_no'),
					'business_branches' => $this->input->post('business_branches'),
					'program_objective' => $this->input->post('program_objective'),
					'program_objective_other' => $this->input->post('program_objective_other'),
					'person_password' => substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 8)
				);

				$this->session->set_userdata($input);

				redirect('/select-package/');

			}

		}

		$this->load->view("template", array( 'view' => 'pages/sign-up' ));
	}

	public function selectpackage()
	{
		$newMerchantId = null;

		if ($this->session->userdata('business_name') == '') redirect('/sign-up/');

		if ($this->input->post('package') != NULL) {
			$packageaddons = '';
			foreach ($this->input->post('app') as $app) {
				$packageaddons = $packageaddons . $app;
			}
			$input = array(
				'business_package' => $this->input->post('package'),
				'packageaddons' => $packageaddons,
			);
			$this->session->set_userdata($input);
			$this->Merchant->register($this->input->post());

			// trigger email alert on new Merchant Signup
			$newMerchantId = $this->session->userdata('person_email');
			$laravelEventEndpoint = MigrationHelper::getAppBaseUrl().'alert/merchantSignup';
			MigrationHelper::postCURL($laravelEventEndpoint, ['merchantId' => $newMerchantId]);

			redirect('/payment-details/');
		}

		$this->load->view("template", array(
			'view' => 'pages/select-package',
			'serviceAgreement' => $this->load->view('pages/service-agreement', array(), true)
		));
	}

	public function payment()
	{
		if ($this->input->post('submit') != NULL) {

			$input = array(
				'creditcardno' => $this->input->post('creditcardno'),
				'creditcardholder' => $this->input->post('creditcardholder'),
				'creditcardcode' => $this->input->post('creditcardcode'),
				'creditcardmonth' => $this->input->post('creditcardmonth'),
				'creditcardyear' => $this->input->post('creditcardyear'),
			);

			$this->session->set_userdata($input);

			$this->Merchant->creditcard();

			redirect('/payment-details/');

		}

		$this->load->view("template", array( 'view' => 'pages/payment' ));
	}

	public function paymentdetails()
	{
		if ($this->session->userdata('email_sent') == NULL) {
			// trigger MerchantWelcome
			$laravelEventEndpoint = MigrationHelper::getAppBaseUrl().'alert/merchantWelcome';
			MigrationHelper::postCURL($laravelEventEndpoint, [
				'person_name' => $this->session->userdata('person_name'),
				'business_name' => $this->session->userdata('business_name'),
				'business_package' => $this->session->userdata('business_package'),
				'person_email' => $this->session->userdata('person_email'),
				'person_password' => $this->session->userdata('person_password')
			]);

			$this->session->userdata('email_sent', true);
		}

		$this->load->view("template", array( 'view' => 'pages/payment-details' ));
	}

	public function faq()
	{
		$this->load->view("template", array( 'view' => 'pages/faq' ));
	}

	public function serviceAgreement()
	{
		$this->load->view("template", array( 'view' => 'pages/service-agreement' ));
	}

	public function termsOfUse()
	{
		$this->load->view("template", array( 'view' => 'pages/terms-of-use' ));
	}

	public function privacyPolicy()
	{
		$this->load->view("template", array( 'view' => 'pages/privacy-policy' ));
	}

	public function packages()
	{
		$this->load->view("template", array( 'view' => 'pages/products' ));
	}

	public function printserviceagreeement()
	{
		$this->load->view(
			'pages/print-service-agreement'
			, array('serviceAgreementSection' => $this->load->view('pages/service-agreement', '', true))
		);
	}

	public function emailServiceAgreement()
	{
		$serviceAgreement = $this->load->view(
			'pages/print-service-agreement',
			array('serviceAgreementSection' => $this->load->view('pages/service-agreement', array('sendAsEmail' => true), true)),
			true
		);

		$this->load->library('emailtemplate');
		$this->load->library('email');
		$this->load->helper('email');

		$this->email->from('noreply.info@rush.com.ph', 'Rush');
		$this->email->to(trim(strtolower($this->session->userdata('person_email'))));
		$this->email->subject('RUSH Service Agreement');
		$this->email->set_mailtype("html");
		$this->email->message($serviceAgreement);
		$this->email->send();
	}
}
