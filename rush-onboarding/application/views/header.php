<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="Fidelity" content="">
    <title>RUSH</title>
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>public/images/rush-favicon.png" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/fonts.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/css/bootstrap-theme.min.css"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/stylesheets/style.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/chosen.css"/>
    
    <script src="<?php echo base_url(); ?>public/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url(); ?>public/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>public/js/chosen.jquery.js"></script>
    <script src="<?php echo base_url(); ?>public/js/custom.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>

<div id="wrapper">
<?php if ($this->uri->segment(1) != 'sign-in') : ?>
<section class="<?php if ($this->uri->total_segments() === 0) echo 'header-section re-vamp'; else echo 'header-section2';?>">
    <div class="container">
    	
		<?php // if ($this->uri->total_segments() !== 0) echo '<a href="'. base_url().'" id="rush-logo"></a>'; ?>
       
        <?php $this->load->view("includes/menu"); ?>
        
        <?php if ($this->uri->total_segments() === 0) : ?>
        <div id="head-content">
            <div class="headings row">
                <h1>Feel the rush of loyalty at your fingertips.</h1>
            </div>
            <div class="headings row  btn-bottom">
                        Let's build your program together. <em>First month's on us!</em>
                        <a href="<?php echo base_url();?>sign-up" class="start-btn">
                            Get Started Now!
                        </a>
                    </div>
                </div>
                <?php endif; ?>
                
            </div>
        </section>
    <?php endif; ?>