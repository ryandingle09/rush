<nav class="navbar navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed mobile-nav-icon" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <i class="fa fa-bars"></i>
        </button>
        <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>public/images/rush-logo.png" alt="Rush"></a>
    </div>			    
    <div class="collapse navbar-collapse mobile-nav rv-navs" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
            <li><a href="<?php echo base_url(); ?>products" class="head-nav">Products</a></li>
            <!-- <li><a href="<?php echo base_url(); ?>about-us">About</a></li> -->
            <li><a href="<?php echo base_url(); ?>request-a-demo">Request A Demo</a></li>
            <li><a class="active" href="<?php echo base_url(); ?>sign-in">Sign In</a></li>
            <!--<li><a href="<?php echo base_url(); ?>sign-up">Sign Up</a></li>-->			        
        </ul>			      
    </div>				
</nav>