<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width" />
</head>
<body style="width: 100% !important; min-width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; color: #222222; font-family: \'Helvetica\', \'Arial\', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 12px; margin: 0; padding: 0;">

<table class="body" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; height: 100%; width: 100%; color: #222222; font-family: \'Helvetica\', \'Arial\', sans-serif; font-weight: normal; line-height: 19px; font-size: 12px; margin: 0; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="center" align="center" valign="top" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: center; color: #222222; font-family: \'Helvetica\', \'Arial\', sans-serif; font-weight: normal; line-height: 19px; font-size: 12px; margin: 0; padding: 0;">
<center style="width: 100%; min-width: 580px;">

<table class="row header" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; border-bottom-style: solid; border-bottom-color: #0f1a21; border-bottom-width: 3px; background: #fafafa; padding: 0px;" bgcolor="#fafafa"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="center" align="center" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: center; color: #222222; font-family: \'Helvetica\', \'Arial\', sans-serif; font-weight: normal; line-height: 19px; font-size: 12px; margin: 0; padding: 0;" valign="top">
<center style="width: 100%; min-width: 580px;">

<table class="container" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; width: 590px; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="wrapper last" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #222222; font-family: \'Helvetica\', \'Arial\', sans-serif; font-weight: normal; line-height: 19px; font-size: 12px; margin: 0; padding: 10px 0px 0px;" align="left" valign="top">

<table class="twelve columns" style="background: #0f1a21; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 580px; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="six sub-columns" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; min-width: 0px; width: 50%; color: #222222; font-family: \'Helvetica\', \'Arial\', sans-serif; font-weight: normal; line-height: 19px; font-size: 12px; margin: 0; padding: 10px 10px 10px 0px;" align="left" valign="top">
<img src="<?php echo base_url() ?>cpanel/assets/images/logo_2016.png" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: auto; float: left; clear: both; display: block;" align="left" /></td>
<td class="six sub-columns last" style="text-align: right; vertical-align: middle; word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; min-width: 0px; width: 50%; color: #222222; font-family: \'Helvetica\', \'Arial\', sans-serif; font-weight: normal; line-height: 19px; font-size: 12px; margin: 0; padding: 10px 0px;" align="right" valign="middle">

</td>
<td class="expander" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; visibility: hidden; width: 0px; color: #222222; font-family: \'Helvetica\', \'Arial\', sans-serif; font-weight: normal; line-height: 19px; font-size: 12px; margin: 0; padding: 0;" align="left" valign="top"></td>
</tr></table></td>
</tr></table></center>
</td>
</tr></table>

<table class="container" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; width: 590px; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #222222; font-family: \'Helvetica\', \'Arial\', sans-serif; font-weight: normal; line-height: 19px; font-size: 12px; margin: 0; padding: 0;" align="left" valign="top">

<table class="row" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0px;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="wrapper last" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #222222; font-family: \'Helvetica\', \'Arial\', sans-serif; font-weight: normal; line-height: 19px; font-size: 12px; margin: 0; padding: 10px 0px 0px;" align="left" valign="top">

<table class="twelve columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 580px; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #222222; font-family: \'Helvetica\', \'Arial\', sans-serif; font-weight: normal; line-height: 19px; font-size: 12px; margin: 0; padding: 10px 0px;" align="left" valign="top">
<p class="lead" style="color: #666; font-family: \'Helvetica\', \'Arial\', sans-serif; font-weight: normal; text-align: left; line-height: 21px; font-size: 18px; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #aaa; margin: 20px 0 10px; padding: 0 0 10px;" align="left">Welcome</p>

<p style="color: #222222; font-family: \'Helvetica\', \'Arial\', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 12px; margin: 0 0 10px; padding: 0;" align="left"><strong>Hi, <?php echo trim(ucfirst($name)) ?> of <?php echo $company ?>, </strong></p>

<p style="color: #222222; font-family: \'Helvetica\', \'Arial\', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 12px; margin: 0 0 10px; padding: 0;" align="left">Thank you for signing up for <?php echo $package ?>!</p>

<p style="color: #222222; font-family: \'Helvetica\', \'Arial\', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 12px; margin: 0 0 10px; padding: 0;" align="left">Login to <a href="<?php echo base_url() ?>sign-in" target="_blank">RUSH Merchant Dashboard</a> with the information below to get you started:</p>

<p style="color: #222222; font-family: \'Helvetica\', \'Arial\', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 12px; margin: 0 0 10px; padding: 0;" align="left">
Username: <?php echo $user ?>
<br />
Temporary Password: <?php echo $password ?>
</p>

<p style="color: #222222; font-family: \'Helvetica\', \'Arial\', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 12px; margin: 0 0 10px; padding: 0;" align="left">We\'ve also attached a quick training guide for your reference.</p>

<p style="color: #222222; font-family: \'Helvetica\', \'Arial\', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 12px; margin: 0 0 10px; padding: 0;" align="left">Let us know if you have any questions at <a href="mailto:support@rush.ph">support@rush.ph</a> and we\'ll be happy to help.</p>

<p style="color: #222222; font-family: \'Helvetica\', \'Arial\', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 12px; margin: 0 0 10px; padding: 0;" align="left">Welcome to RUSH! Your ultimate, on-demand rewards solution!</p>

<p style="color: #222222; font-family: \'Helvetica\', \'Arial\', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 12px; margin: 0 0 10px; padding: 0;" align="left"><br />RUSH Loyalty on Demand</p>

<p style="color: #222222; font-family: \'Helvetica\', \'Arial\', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 12px; margin: 0 0 10px; padding: 0;" align="left"><em>Globe Telecom Inc. The Globe Tower, 32nd St, Taguig, Metro Manila</em></p>

</td>

</tr></table></td>
</tr></table><table class="container " style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; width: 590px; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="wrapper last" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #222222; font-family: \'Helvetica\', \'Arial\', sans-serif; font-weight: normal; line-height: 19px; font-size: 12px; margin: 0; padding: 10px 0px 0px;" align="left" valign="top">

<table class="twelve columns " style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 580px; margin: 0 auto; padding: 0;"><tr class="footerr" style="vertical-align: top; text-align: left; background: #e1e1e1; padding: 0;" align="left" bgcolor="#e1e1e1"><td align="center" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #222222; font-family: \'Helvetica\', \'Arial\', sans-serif; font-weight: normal; line-height: 19px; font-size: 12px; margin: 0; padding: 8px 5px;" valign="top">
<center style="width: 100%; min-width: 580px;">
<p style="text-align: center; color: #222222; font-family: \'Helvetica\', \'Arial\', sans-serif; font-weight: normal; line-height: 19px; font-size: 12px; margin: 0; padding: 0;" align="center"><a href="<?php echo base_url() ?>contact-us" target="_blank" style="color: #99CC01; text-decoration: none;">Contact Us</a> | <a href="<?php echo base_url() ?>terms-of-use" target="_blank" style="color: #99CC01; text-decoration: none;">Terms of Use</a> | <a href="<?php echo base_url() ?>privacy-policy" target="_blank" style="color: #99CC01; text-decoration: none;">Privacy Policy</a> | <a href="<?php echo base_url() ?>faq" target="_blank" style="color: #99CC01; text-decoration: none;">FAQS</a>
</p>

</center>

</td>
</tr><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #222222; font-family: \'Helvetica\', \'Arial\', sans-serif; font-weight: normal; line-height: 19px; font-size: 12px; margin: 0; padding: 10px 0px;" align="left" valign="top">
<p class="small" style="font-size: 10px; text-align: center; color: #999; font-family: \'Helvetica\', \'Arial\', sans-serif; font-weight: normal; line-height: 19px; margin: 0 0 0px; padding: 0;" align="center">All Rights Reserved. Rush 2016</p>
</td>
</tr></table></td>
</tr></table></td>
</tr></table></center>
</td>
</tr></table></body>
</html>