<?php if ($this->uri->segment(1) != 'sign-in') : ?>	
<footer>
    <div class="copyright foot-navs">
        <div class="container">
            <div class="social-media">
                <a href="https://www.facebook.com/rushrewardsph" target='_blank'><i class="fa fa-facebook"></i></a>
                <a href="https://twitter.com/rushrewardsph" target='_blank'><i class="fa fa-twitter"></i></a>
                <a href="https://www.instagram.com/rushrewardsph" target='_blank'><i class="fa fa-instagram"></i></a>
            </div>
            <div class="row">
                <ul class="footer-nav">
                    <li><a href="<?php echo base_url(); ?>contact-us">Contact Us</a></li>
                    <li><a href="<?php echo base_url('terms-of-use'); ?>">Terms of Use</a></li>
                    <li><a href="<?php echo base_url('privacy-policy'); ?>">Privacy Policy</a></li>
                    <!-- <li><a href="<?php echo base_url('service-agreement'); ?>">Service Agreement</a></li> -->
                    <li><a href="<?php echo base_url('faq'); ?>">FAQS</a></li>
                </ul>
            </div>
        </div>
    </div>    
    <div class="rv-copyright">
        <p>Copyright © RUSH 2015 . All Rights Reserved</p>
    </div>
</footer>
<?php endif; ?>

</div>

</body>
</html>
