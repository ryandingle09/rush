<section class="content2">
    <div class="container form-width">
        <form id="form" action="" method="post">
                <label>What is your company called?<span class="required">&nbsp;*</span> <?php if ($this->session->userdata('businessnametaken') == true) echo '<div style="padding-left:10px; color:#F00; float:right;">Business name is already taken</div>'; ?></label>
                <div class="styled-input">
                    <input type="text" required class="form-control" name="business_name" value="<?php echo $this->input->post('business_name'); ?>">
                </div>
                <br/>
                <label style="width:100%;">What industry are you in?<span class="required">&nbsp;*</span> <div class="clear"></div><em style="font-size:11px; float:left; margin-top:3px; margin-right:5px;">(Hold down the CTRL key to select multiple items below)</em></label><br/>
                <div class="container-text">
		    	<select id="industry" name="business_type" class="form-control chosen-select" multiple data-placeholder="&nbsp;">              
                    <option <?php if (strpos($this->input->post('business_type'), '|1') !== false) echo 'selected';?> value="|1">Automotive Dealers and Manufacturers</option>
                    <option <?php if (strpos($this->input->post('business_type'), '|2') !== false) echo 'selected';?> value="|2">Automotive Services</option>
                    <option <?php if (strpos($this->input->post('business_type'), '|3') !== false) echo 'selected';?> value="|3">Entertainment</option>
                    <option <?php if (strpos($this->input->post('business_type'), '|4') !== false) echo 'selected';?> value="|4">Fitness</option>
                    <option <?php if (strpos($this->input->post('business_type'), '|5') !== false) echo 'selected';?> value="|5">Food and Beverage Establishment</option>
                    <option <?php if (strpos($this->input->post('business_type'), '|6') !== false) echo 'selected';?> value="|6">Fuel and Petroleum</option>
                    <option <?php if (strpos($this->input->post('business_type'), '|7') !== false) echo 'selected';?> value="|7">Grocery and Supermarket</option>
                    <option <?php if (strpos($this->input->post('business_type'), '|8') !== false) echo 'selected';?> value="|8">Grooming and Wellness</option>
                    <option <?php if (strpos($this->input->post('business_type'), '|9') !== false) echo 'selected';?> value="|9">Hospitals and Healthcare Services</option>
                    <option <?php if (strpos($this->input->post('business_type'), '|10') !== false) echo 'selected';?> value="|10">Insurance</option>
                    <option <?php if (strpos($this->input->post('business_type'), '|11') !== false) echo 'selected';?> value="|11">Petroleum</option>
                    <option <?php if (strpos($this->input->post('business_type'), '|12') !== false) echo 'selected';?> value="|12">Retail - Apparel</option>
                    <option <?php if (strpos($this->input->post('business_type'), '|18') !== false) echo 'selected';?> value="|18">Retail - Electronics</option>
                    <option <?php if (strpos($this->input->post('business_type'), '|13') !== false) echo 'selected';?> value="|13">Retail - Household and Hardware</option>
                    <option <?php if (strpos($this->input->post('business_type'), '|14') !== false) echo 'selected';?> value="|14">Retail - Sporting Goods</option>
                    <option <?php if (strpos($this->input->post('business_type'), '|15') !== false) echo 'selected';?> value="|15">Telecommunication and Utilities</option>
                    <option <?php if (strpos($this->input->post('business_type'), '|16') !== false) echo 'selected';?> value="|16">Transportation</option>
                    <option <?php if (strpos($this->input->post('business_type'), '|17') !== false) echo 'selected';?> value="|17">Travel and Leisure</option>
                    <option <?php if (strpos($this->input->post('business_type'), '|Others') !== false) echo 'selected';?> value="|Others">Others</option>
                    </select>
                </div>
                <br/>
               
                <div id="others" class="" <?php if (strpos($this->input->post('business_type'), '|Others') !== false) echo 'style="display:block;"';?>>
                <label>Others<span class="required">&nbsp;*</span></label>
                    <input id="others_ID" type="text" class="form-control" name="business_type_other" value="<?php echo $this->input->post('business_type_other'); ?>">
                    <br/>
                </div>

            
                <br/>
                <label>Authorized Company Representative<span class="required">&nbsp;*</span></label>
                <div class="styled-input">
                    <input type="text" required class="form-control" name="person_name" value="<?php echo $this->input->post('person_name'); ?>">
                </div>
                <br/>
                <label>Designation/Position<span class="required">&nbsp;*</span></label>
                <div class="styled-input">
                    <input type="text" required class="form-control" name="person_position" value="<?php echo $this->input->post('person_position'); ?>">
                </div>
                <br/>
                <label>Telephone No.<span class="required">&nbsp;*</span></label>
                <div class="styled-input">
                <table width="100%">
                    <tr>
                        <td style="width:20%;"><input type="text" pattern="\d{2}" maxlength="2" required class="form-control" name="person_contact1" placeholder="02" value="<?php echo $this->input->post('person_contact1'); ?>"></td>
                        <td style="width:2%;">&nbsp;-&nbsp;</td>
                        <td style="width:78%;"><input type="text" pattern="\d{7}" maxlength="7" required class="form-control" name="person_contact2" placeholder="1234567" value="<?php echo $this->input->post('person_contact2'); ?>"></td>
                    </tr>
                </table>
                </div>
                <br/>
                <label>Email Address<span class="required">&nbsp;*</span> <?php if ($this->session->userdata('emailtaken') == true) echo '<div style="padding-left:10px; color:#F00; float:right;">Email is already taken</div>'; ?></label>
                <input type="email" required class="form-control" id="person_email" name="person_email" value="<?php echo $this->input->post('person_email'); ?>">
                <br/>
                <label>Where are you located?<span class="required">&nbsp;*</span></label>
                <div class="styled-select">
                    <select name="business_location" class="form-control" required>
                    	<option <?php if ($this->input->post('business_location') == '') echo 'selected'; ?> value="">Select location</option>
                        <option <?php if ($this->input->post('business_location') == 'Alaminos') echo 'selected'; ?>>Alaminos</option>
                        <option <?php if ($this->input->post('business_location') == 'Angeles') echo 'selected'; ?>>Angeles</option>
                        <option <?php if ($this->input->post('business_location') == 'Antipolo') echo 'selected'; ?>>Antipolo</option>
                        <option <?php if ($this->input->post('business_location') == 'Bacolod') echo 'selected'; ?>>Bacolod</option>
                        <option <?php if ($this->input->post('business_location') == 'Bacoor') echo 'selected'; ?>>Bacoor</option>
                        <option <?php if ($this->input->post('business_location') == 'Bago') echo 'selected'; ?>>Bago</option>
                        <option <?php if ($this->input->post('business_location') == 'Baguio') echo 'selected'; ?>>Baguio</option>
                        <option <?php if ($this->input->post('business_location') == 'Bais') echo 'selected'; ?>>Bais</option>
                        <option <?php if ($this->input->post('business_location') == 'Balanga') echo 'selected'; ?>>Balanga</option>
                        <option <?php if ($this->input->post('business_location') == 'Batac') echo 'selected'; ?>>Batac</option>
                        <option <?php if ($this->input->post('business_location') == 'Batangas City') echo 'selected'; ?>>Batangas City</option>
                        <option <?php if ($this->input->post('business_location') == 'Bayawan') echo 'selected'; ?>>Bayawan</option>
                        <option <?php if ($this->input->post('business_location') == 'Baybay') echo 'selected'; ?>>Baybay</option>
                        <option <?php if ($this->input->post('business_location') == 'Bayugan') echo 'selected'; ?>>Bayugan</option>
                        <option <?php if ($this->input->post('business_location') == 'Biñan') echo 'selected'; ?>>Biñan</option>
                        <option <?php if ($this->input->post('business_location') == 'Bislig') echo 'selected'; ?>>Bislig</option>
                        <option <?php if ($this->input->post('business_location') == 'Bogo') echo 'selected'; ?>>Bogo</option>
                        <option <?php if ($this->input->post('business_location') == 'Borongan') echo 'selected'; ?>>Borongan</option>
                        <option <?php if ($this->input->post('business_location') == 'Butuan') echo 'selected'; ?>>Butuan</option>
                        <option <?php if ($this->input->post('business_location') == 'Cabadbaran') echo 'selected'; ?>>Cabadbaran</option>
                        <option <?php if ($this->input->post('business_location') == 'Cabanatuan') echo 'selected'; ?>>Cabanatuan</option>
                        <option <?php if ($this->input->post('business_location') == 'Cabuyao') echo 'selected'; ?>>Cabuyao</option>
                        <option <?php if ($this->input->post('business_location') == 'Cadiz') echo 'selected'; ?>>Cadiz</option>
                        <option <?php if ($this->input->post('business_location') == 'Cagayan de Oro') echo 'selected'; ?>>Cagayan de Oro</option>
                        <option <?php if ($this->input->post('business_location') == 'Calamba') echo 'selected'; ?>>Calamba</option>
                        <option <?php if ($this->input->post('business_location') == 'Calapan') echo 'selected'; ?>>Calapan</option>
                        <option <?php if ($this->input->post('business_location') == 'Calbayog') echo 'selected'; ?>>Calbayog</option>
                        <option <?php if ($this->input->post('business_location') == 'Caloocan') echo 'selected'; ?>>Caloocan</option>
                        <option <?php if ($this->input->post('business_location') == 'Candon') echo 'selected'; ?>>Candon</option>
                        <option <?php if ($this->input->post('business_location') == 'Canlaon') echo 'selected'; ?>>Canlaon</option>
                        <option <?php if ($this->input->post('business_location') == 'Carcar') echo 'selected'; ?>>Carcar</option>
                        <option <?php if ($this->input->post('business_location') == 'Catbalogan') echo 'selected'; ?>>Catbalogan</option>
                        <option <?php if ($this->input->post('business_location') == 'Cauayan') echo 'selected'; ?>>Cauayan</option>
                        <option <?php if ($this->input->post('business_location') == 'Cavite City') echo 'selected'; ?>>Cavite City</option>
                        <option <?php if ($this->input->post('business_location') == 'ManCebu Cityila') echo 'selected'; ?>>Cebu City</option>
                        <option <?php if ($this->input->post('business_location') == 'Cotabato City') echo 'selected'; ?>>Cotabato City</option>
                        <option <?php if ($this->input->post('business_location') == 'Dagupan') echo 'selected'; ?>>Dagupan</option>
                        <option <?php if ($this->input->post('business_location') == 'Danao') echo 'selected'; ?>>Danao</option>
                        <option <?php if ($this->input->post('business_location') == 'Dapitan') echo 'selected'; ?>>Dapitan</option>
                        <option <?php if ($this->input->post('business_location') == 'Dasmariñas') echo 'selected'; ?>>Dasmariñas</option>
                        <option <?php if ($this->input->post('business_location') == 'Davao City') echo 'selected'; ?>>Davao City</option>
                        <option <?php if ($this->input->post('business_location') == 'Digos') echo 'selected'; ?>>Digos</option>
                        <option <?php if ($this->input->post('business_location') == 'Dipolog') echo 'selected'; ?>>Dipolog</option>
                        <option <?php if ($this->input->post('business_location') == 'Dumaguete') echo 'selected'; ?>>Dumaguete</option>
                        <option <?php if ($this->input->post('business_location') == 'El Salvador') echo 'selected'; ?>>El Salvador</option>
                        <option <?php if ($this->input->post('business_location') == 'Escalante') echo 'selected'; ?>>Escalante</option>
                        <option <?php if ($this->input->post('business_location') == 'Gapan') echo 'selected'; ?>>Gapan</option>
                        <option <?php if ($this->input->post('business_location') == 'General Santos') echo 'selected'; ?>>General Santos</option>
                        <option <?php if ($this->input->post('business_location') == 'General Trias') echo 'selected'; ?>>General Trias</option>
                        <option <?php if ($this->input->post('business_location') == 'Gingoog') echo 'selected'; ?>>Gingoog</option>
                        <option <?php if ($this->input->post('business_location') == 'Guihulngan') echo 'selected'; ?>>Guihulngan</option>
                        <option <?php if ($this->input->post('business_location') == 'Himamaylan') echo 'selected'; ?>>Himamaylan</option>
                        <option <?php if ($this->input->post('business_location') == 'Ilagan') echo 'selected'; ?>>Ilagan</option>
                        <option <?php if ($this->input->post('business_location') == 'Iligan') echo 'selected'; ?>>Iligan</option>
                        <option <?php if ($this->input->post('business_location') == 'Iloilo City') echo 'selected'; ?>>Iloilo City</option>
                        <option <?php if ($this->input->post('business_location') == 'Imus') echo 'selected'; ?>>Imus</option>
                        <option <?php if ($this->input->post('business_location') == 'Iriga') echo 'selected'; ?>>Iriga</option>
                        <option <?php if ($this->input->post('business_location') == 'Isabela') echo 'selected'; ?>>Isabela</option>
                        <option <?php if ($this->input->post('business_location') == 'Kabankalan') echo 'selected'; ?>>Kabankalan</option>
                        <option <?php if ($this->input->post('business_location') == 'Kidapawan') echo 'selected'; ?>>Kidapawan</option>
                        <option <?php if ($this->input->post('business_location') == 'Koronadal') echo 'selected'; ?>>Koronadal</option>
                        <option <?php if ($this->input->post('business_location') == 'La Carlota') echo 'selected'; ?>>La Carlota</option>
                        <option <?php if ($this->input->post('business_location') == 'Lamitan') echo 'selected'; ?>>Lamitan</option>
                        <option <?php if ($this->input->post('business_location') == 'Laoag') echo 'selected'; ?>>Laoag</option>
                        <option <?php if ($this->input->post('business_location') == 'Lapu-Lapu') echo 'selected'; ?>>Lapu-Lapu</option>
                        <option <?php if ($this->input->post('business_location') == 'Las Piñas') echo 'selected'; ?>>Las Piñas</option>
                        <option <?php if ($this->input->post('business_location') == 'Legazpi') echo 'selected'; ?>>Legazpi</option>
                        <option <?php if ($this->input->post('business_location') == 'Ligao') echo 'selected'; ?>>Ligao</option>
                        <option <?php if ($this->input->post('business_location') == 'Lipa') echo 'selected'; ?>>Lipa</option>
                        <option <?php if ($this->input->post('business_location') == 'Lucena') echo 'selected'; ?>>Lucena</option>
                        <option <?php if ($this->input->post('business_location') == 'Maasin') echo 'selected'; ?>>Maasin</option>
                        <option <?php if ($this->input->post('business_location') == 'Mabalacat') echo 'selected'; ?>>Mabalacat</option>
                        <option <?php if ($this->input->post('business_location') == 'Makati') echo 'selected'; ?>>Makati</option>
                        <option <?php if ($this->input->post('business_location') == 'Malabon') echo 'selected'; ?>>Malabon</option>
                        <option <?php if ($this->input->post('business_location') == 'Malaybalay') echo 'selected'; ?>>Malaybalay</option>
                        <option <?php if ($this->input->post('business_location') == 'Malolos') echo 'selected'; ?>>Malolos</option>
                        <option <?php if ($this->input->post('business_location') == 'Mandaluyong') echo 'selected'; ?>>Mandaluyong</option>
                        <option <?php if ($this->input->post('business_location') == 'Mandaue') echo 'selected'; ?>>Mandaue</option>
                        <option <?php if ($this->input->post('business_location') == 'Manila') echo 'selected'; ?>>Manila</option>
                        <option <?php if ($this->input->post('business_location') == 'Marawi') echo 'selected'; ?>>Marawi</option>
                        <option <?php if ($this->input->post('business_location') == 'Marikina') echo 'selected'; ?>>Marikina</option>
                        <option <?php if ($this->input->post('business_location') == 'Masbate City') echo 'selected'; ?>>Masbate City</option>
                        <option <?php if ($this->input->post('business_location') == 'Mati') echo 'selected'; ?>>Mati</option>
                        <option <?php if ($this->input->post('business_location') == 'Meycauayan') echo 'selected'; ?>>Meycauayan</option>
                        <option <?php if ($this->input->post('business_location') == 'Muñoz') echo 'selected'; ?>>Muñoz</option>
                        <option <?php if ($this->input->post('business_location') == 'Muntinlupa') echo 'selected'; ?>>Muntinlupa</option>
                        <option <?php if ($this->input->post('business_location') == 'Naga') echo 'selected'; ?>>Naga</option>
                        <option <?php if ($this->input->post('business_location') == 'Navotas') echo 'selected'; ?>>Navotas</option>
                        <option <?php if ($this->input->post('business_location') == 'Olongapo') echo 'selected'; ?>>Olongapo</option>
                        <option <?php if ($this->input->post('business_location') == 'Ormoc') echo 'selected'; ?>>Ormoc</option>
                        <option <?php if ($this->input->post('business_location') == 'Oroquieta') echo 'selected'; ?>>Oroquieta</option>
                        <option <?php if ($this->input->post('business_location') == 'Ozamiz') echo 'selected'; ?>>Ozamiz</option>
                        <option <?php if ($this->input->post('business_location') == 'Pagadian') echo 'selected'; ?>>Pagadian</option>
                        <option <?php if ($this->input->post('business_location') == 'Palayan') echo 'selected'; ?>>Palayan</option>
                        <option <?php if ($this->input->post('business_location') == 'Panabo') echo 'selected'; ?>>Panabo</option>
                        <option <?php if ($this->input->post('business_location') == 'Parañaque') echo 'selected'; ?>>Parañaque</option>
                        <option <?php if ($this->input->post('business_location') == 'Pasay') echo 'selected'; ?>>Pasay</option>
                        <option <?php if ($this->input->post('business_location') == 'Pasig') echo 'selected'; ?>>Pasig</option>
                        <option <?php if ($this->input->post('business_location') == 'Passi') echo 'selected'; ?>>Passi</option>
                        <option <?php if ($this->input->post('business_location') == 'Puerto Princesa') echo 'selected'; ?>>Puerto Princesa</option>
                        <option <?php if ($this->input->post('business_location') == 'Quezon City') echo 'selected'; ?>>Quezon City</option>
                        <option <?php if ($this->input->post('business_location') == 'Roxas') echo 'selected'; ?>>Roxas</option>
                        <option <?php if ($this->input->post('business_location') == 'Sagay') echo 'selected'; ?>>Sagay</option>
                        <option <?php if ($this->input->post('business_location') == 'Samal') echo 'selected'; ?>>Samal</option>
                        <option <?php if ($this->input->post('business_location') == 'San Carlos') echo 'selected'; ?>>San Carlos</option>
                        <option <?php if ($this->input->post('business_location') == 'San Carlos') echo 'selected'; ?>>San Carlos</option>
                        <option <?php if ($this->input->post('business_location') == 'San Fernando') echo 'selected'; ?>>San Fernando</option>
                        <option <?php if ($this->input->post('business_location') == 'San Fernando') echo 'selected'; ?>>San Fernando</option>
                        <option <?php if ($this->input->post('business_location') == 'San Jose') echo 'selected'; ?>>San Jose</option>
                        <option <?php if ($this->input->post('business_location') == 'San Jose del Monte') echo 'selected'; ?>>San Jose del Monte</option>
                        <option <?php if ($this->input->post('business_location') == 'San Juan') echo 'selected'; ?>>San Juan</option>
                        <option <?php if ($this->input->post('business_location') == 'San Pablo') echo 'selected'; ?>>San Pablo</option>
                        <option <?php if ($this->input->post('business_location') == 'San Pedro') echo 'selected'; ?>>San Pedro</option>
                        <option <?php if ($this->input->post('business_location') == 'Santa Rosa') echo 'selected'; ?>>Santa Rosa</option>
                        <option <?php if ($this->input->post('business_location') == 'Santiago') echo 'selected'; ?>>Santiago</option>
                        <option <?php if ($this->input->post('business_location') == 'Silay') echo 'selected'; ?>>Silay</option>
                        <option <?php if ($this->input->post('business_location') == 'Sipalay') echo 'selected'; ?>>Sipalay</option>
                        <option <?php if ($this->input->post('business_location') == 'Sorsogon City') echo 'selected'; ?>>Sorsogon City</option>
                        <option <?php if ($this->input->post('business_location') == 'Surigao City') echo 'selected'; ?>>Surigao City</option>
                        <option <?php if ($this->input->post('business_location') == 'Tabaco') echo 'selected'; ?>>Tabaco</option>
                        <option <?php if ($this->input->post('business_location') == 'Tabuk') echo 'selected'; ?>>Tabuk</option>
                        <option <?php if ($this->input->post('business_location') == 'Tacloban') echo 'selected'; ?>>Tacloban</option>
                        <option <?php if ($this->input->post('business_location') == 'Tacurong') echo 'selected'; ?>>Tacurong</option>
                        <option <?php if ($this->input->post('business_location') == 'Tagaytay') echo 'selected'; ?>>Tagaytay</option>
                        <option <?php if ($this->input->post('business_location') == 'Tagbilaran') echo 'selected'; ?>>Tagbilaran</option>
                        <option <?php if ($this->input->post('business_location') == 'Taguig') echo 'selected'; ?>>Taguig</option>
                        <option <?php if ($this->input->post('business_location') == 'Tagum') echo 'selected'; ?>>Tagum</option>
                        <option <?php if ($this->input->post('business_location') == 'Talisay') echo 'selected'; ?>>Talisay</option>
                        <option <?php if ($this->input->post('business_location') == 'Tanauan') echo 'selected'; ?>>Tanauan</option>
                        <option <?php if ($this->input->post('business_location') == 'Tandag') echo 'selected'; ?>>Tandag</option>
                        <option <?php if ($this->input->post('business_location') == 'Tangub') echo 'selected'; ?>>Tangub</option>
                        <option <?php if ($this->input->post('business_location') == 'Tanjay') echo 'selected'; ?>>Tanjay</option>
                        <option <?php if ($this->input->post('business_location') == 'Tarlac City') echo 'selected'; ?>>Tarlac City</option>
                        <option <?php if ($this->input->post('business_location') == 'Tayabas') echo 'selected'; ?>>Tayabas</option>
                        <option <?php if ($this->input->post('business_location') == 'Toledo') echo 'selected'; ?>>Toledo</option>
                        <option <?php if ($this->input->post('business_location') == 'Trece Martires') echo 'selected'; ?>>Trece Martires</option>
                        <option <?php if ($this->input->post('business_location') == 'Tuguegarao') echo 'selected'; ?>>Tuguegarao</option>
                        <option <?php if ($this->input->post('business_location') == 'Urdaneta') echo 'selected'; ?>>Urdaneta</option>
                        <option <?php if ($this->input->post('business_location') == 'Valencia') echo 'selected'; ?>>Valencia</option>
                        <option <?php if ($this->input->post('business_location') == 'Valenzuela') echo 'selected'; ?>>Valenzuela</option>
                        <option <?php if ($this->input->post('business_location') == 'Victorias') echo 'selected'; ?>>Victorias</option>
                        <option <?php if ($this->input->post('business_location') == 'Vigan') echo 'selected'; ?>>Vigan</option>
                        <option <?php if ($this->input->post('business_location') == 'Zamboanga City') echo 'selected'; ?>>Zamboanga City</option>
                    </select>
                </div>
                <br />
                <label>How many customers do you serve per month? <span class="required">&nbsp;*</span></label>
                <div class="styled-select">
                    <select name="customer_no" class="form-control" required>
                            <option <?php if ($this->input->post('customer_no') == '50 - 100') echo 'selected'; ?> value="50 - 100">50 - 100</option>
                            <option <?php if ($this->input->post('customer_no') == '101 - 500') echo 'selected'; ?> value="101 - 500">101 - 500</option>
                            <option <?php if ($this->input->post('customer_no') == '501 - 1000') echo 'selected'; ?> value="501 - 1000">501 - 1000</option>
                            <option <?php if ($this->input->post('customer_no') == '1001 - 5000') echo 'selected'; ?> value="1001 - 5000">1001 - 5000</option>
                            <option <?php if ($this->input->post('customer_no') == '5001 - 10000') echo 'selected'; ?> value="5001 - 10000">5001 - 10000</option>
                            <option <?php if ($this->input->post('customer_no') == '10000 and above') echo 'selected'; ?> value="10000 and above">10000 and above</option>
                    </select>
                </div>
                <br/>
                <label>How many branches do you operate?<span class="required">&nbsp;*</span></label>
                <div class="styled-input">
                    <input type="text" required class="form-control" name="business_branches" placeholder="" value="<?php echo $this->input->post('business_branches'); ?>">
                </div>
                <br/>
                <label style="width:100%;">What is the objective of your loyalty program? <div class="clear"></div><em style="font-size:11px; float:left; margin-top:3px; margin-right:5px;">(Hold down the CTRL key to select multiple items below)</em></label>
                <!-- <div class="styled-select">
                    <select name="customer_no" class="form-control"> -->
		      <div class="loyalty-holder">
                    <select id="loyalty" name="program_objective" class="form-control chosen-select" multiple data-placeholder="&nbsp;"> 
                        <option <?php if (strpos($this->input->post('program_objective'), '|1') !== false) echo 'selected';?> value="|1">Enhance customer engagement</option>
                        <option <?php if (strpos($this->input->post('program_objective'), '|2') !== false) echo 'selected';?> value="|2">Improve customer retention</option>
                        <option <?php if (strpos($this->input->post('program_objective'), '|3') !== false) echo 'selected';?> value="|3">Increase frequency of customer visits</option>
                        <option <?php if (strpos($this->input->post('program_objective'), '|4') !== false) echo 'selected';?> value="|4">Drive revenue per transaction</option>
                        <option <?php if (strpos($this->input->post('program_objective'), '|5') !== false) echo 'selected';?> value="|5">Gather insights and understand customer behavior</option>
                        <option <?php if (strpos($this->input->post('program_objective'), '|6') !== false) echo 'selected';?> value="|6">Promote and push new products and services</option>
                    	<option <?php if (strpos($this->input->post('program_objective'), '|Others') !== false) echo 'selected';?> value="|Others">Others</option>
                    </select>
                </div>
                <br/>

                <div id="others2" class="" <?php if (strpos($this->input->post('program_objective'), '|Others') !== false) echo 'style="display:block;"';?>>
                <label >Others<span class="required">&nbsp;*</span></label>
                    <input id="others_ID2" type="text" class="form-control" name="program_objective_other" value="<?php echo $this->input->post('program_objective_other'); ?>">
                    <br/>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?php if ($this->session->userdata('invalid_captcha') == true) : ?>
                            <div class="bg-danger">
                                The Captcha field is invalid.
                            </div>
                        <?php endif; ?>
                        <div class="g-recaptcha" data-sitekey="6LeSJSkTAAAAAJqYJJczKaKyhU6G_0DXkRsZM_1-"></div>
                    </div>
                </div>

                <!-- <input type="text" class="form-control" name="loyalty_program" value="<?php echo $this->input->post('loyalty_program'); ?>">-->
                <span class="space">&nbsp;</span>
                <input type="submit" name="submit" id="form-submit" style="display:none;">
                <a onclick="javascript:$('#form-submit').click();" style="cursor:pointer;" class="start-btn">
                    <span>Continue</span>
                </a>
        </form>
    </div>
</section>