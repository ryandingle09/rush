<?php use Rush\Citolaravel\Helpers\MigrationHelper; ?>
<section class="login">
    <div class="container">
        <div class="row form">
            <div class="col-md-12">
                    <img src="<?php echo base_url(); ?>public/images/rush-logo.png" alt="">
                    <br />
                    <form action="" method="post">
                        <input type="text" name="email" class="form-control" placeholder="email">
                        <br />
                        <input type="password" name="password" class="form-control" placeholder="password">
                        <p><a href="#" class="start-btn2" data-toggle="modal" data-target=".package-modal" aria-label="Close" aria-hidden="true">Forgot Password?</a></p>
                       	<p><?php echo $this->session->flashdata('loginmessage');?></p>
                        <input type="submit" name="submit" value="submit" id="submit" style="display:none;">
                        <a onclick="javascript:$('#submit').click();" style="cursor:pointer;" class="start-btn">
                            <span>Continue</span>
                        </a>
                    </form>
            </div>
        </div>
        <div class="modal fade package-modal " tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        
        	<form action="" method="post">
            <div id="modal-dimension" class="modal-dialog" >
                <div class="modal-content form">
                <button type="button" class="close" data-dismiss="modal">
                <img src="<?php echo base_url(); ?>public/images/close.png">
                </button>
                <p> Lost your password? type in your email and we'll email you instructions to create a new password </p>					
                <input type="text" name="emailpassword" class="form-control">
                <br/>
                <input type="submit" name="submitchangepassword" value="submit" id="submitchangepassword" style="display:none;">
                <a onclick="javascript:$('#submitchangepassword').click();" style="cursor:pointer;" class="start-btn">
                    <span>Continue</span>
                </a>		
                </div>
            </div>
            </form>
            
        </div>
    </div>
    <br>
    <div class="backHome" style="text-align: center;">
        <a href="<?php echo base_url().MigrationHelper::getOnboardingPrefix(); ?>"> <i class="fa fa-angle-left" aria-hidden="true"></i><i class="fa fa-angle-left" aria-hidden="true"></i>
 Back to Home</a>
    </div>
    <div class="overlay"></div>
</section>