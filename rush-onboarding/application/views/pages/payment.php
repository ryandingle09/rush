<section class="content2">
	<form action="" method="post">
    <div class="container">
        <label>Mode of Payment</label>
        <div class="styled-select">
            <select id="payment-type" name="payment-type" class="form-control">
                    <option value="1">Credit Card</option>
            </select>
        </div>
        <div id="credit-card" class="payment">	
            <div class="trigger">
                <div class="row">						
                    <div class="col-md-12">
                            <label>Credit Card No.</label>
                            <input type="text" required class="form-control" name="creditcardno">
                    </div>
                </div>
                <br/>
                <div class="row">						
                    <div class="col-md-12">
                            <label>Cardholder's Name</label>
                            <input type="text" required class="form-control" name="creditcardholder">
                    </div>
                </div>
                <br/>
                <div class="row">
                        <div class="col-md-6">
                                <label>Securtiy Code</label>
                                <input type="number" required class="form-control" name="creditcardcode">
                        </div>
                        <div class="col-md-3">
                                <label>Expiration</label>
                                <div class="styled-select">
                                    <select name="creditcardmonth" class="form-control">
                                        <option value="01">01</option>
                                        <option value="02">02</option>
                                        <option value="02">03</option>
                                        <option value="02">04</option>
                                        <option value="02">05</option>
                                        <option value="02">06</option>
                                        <option value="02">07</option>
                                        <option value="02">08</option>
                                        <option value="02">09</option>
                                        <option value="02">10</option>
                                        <option value="02">11</option>
                                        <option value="02">12</option>
                                    </select>
                                </div>
                        </div>
                        <div class="col-md-3">
                                <label>&nbsp;</label>
                                <div class="styled-select">
                                    <select name="creditcardyear" class="form-control">
                                    	<?php for ($x = date('Y'); $x <= date('Y', strtotime('+10 years')); $x++) { ?>
                                        <option value="<?php echo $x; ?>"><?php echo $x; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                        </div>
                </div>
            </div>	
        </div>
        <span class="space">&nbsp;</span>
        <input type="submit" name="submit" id="form-submit" style="display:none;">
        <a onclick="javascript:$('#form-submit').click();" style="cursor:pointer;" class="start-btn">
            <span>Submit</span>
        </a>
    </div>
    </form>
</section>