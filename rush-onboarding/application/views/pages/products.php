<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<section class="content2 no-pad-LR">
    <div class="container rs-wider">
        <div class="row">
            <div class="rs-header">
                <h1>Choose the plan that&#39;s right for you.<br>Try RUSH for 30 days</h1>
                <!-- <h3>Start your trial today. You can switch plans or cancel at any time.</h3> -->
                <a href="<?php echo base_url();?>sign-up" class="start-btn prodBtn">
                    Get Started Now!
                </a>
            </div><!-- /.rs-header -->
        </div><!-- /.row -->
    </div><!-- /.container 1 -->

    <div class="container full-width">
        <div class="row">
            <div class="rs-package">
                <div class="plan-col col-sm-6 col-md-6">
                    <div class="rs-pimg">
                        <img src="<?php echo base_url(); ?>public/images/img-rushbasic.png" style="max-width: 100%;">
                    </div>
                    <div class="plan-wrap bord-red">
                        <span class="package-amt">
                            <h2>RUSH Basic</h2>
                            <p class="para-1">Starts at</p>
                            <h1 class="amt-val"><sup>Php</sup> 1,299</h1>
                            <p>per branch per month</p>
                            <span class="arrow"></span>
                        </span>
                        <span class="rs-users">
                            <strong>Unlimited memberships</strong>
                        </span>
                    </div>
                </div>
                <div class="plan-col col-sm-6 col-md-6">
                    <div class="rs-pimg">
                        <img src="<?php echo base_url(); ?>public/images/img-rushpro.png" style="max-width: 100%;">
                    </div>
                    <div class="plan-wrap bord-yellow">
                        <span class="package-amt">
                            <h2>RUSH Pro</h2>
                            <p class="para-1">Starts at</p>
                            <h1 class="amt-val">1.5<sup>%</sup></h1>
                            <p>of Gross Transaction</p>
                            <span class="arrow"></span>
                        </span>
                        <span class="rs-users">
                            <strong>Unlimited memberships</strong>
                        </span>
                    </div>
                </div>
                <!-- <div class="plan-col col-md-4">
                    <div class="plan-wrap bord-teal">
                        <span class="package-amt">
                            <h2>RUSH Ultimate</h2>
                            <p class="para-1">Starts at</p>
                            <h1 class="amt-val"><sup>Php</sup> 1.40</h1>
                            <p>per point seed <br> &nbsp;</p>
                            <p>RUSH-Branded, Premiere <br> Coalition Rewards Program</p>
                            <span class="arrow"></span>
                        </span>
                        <span class="rs-users">
                            <strong>Unlimited memberships</strong>
                        </span>
                        <span class="pck-btn">
                            <button type="submit" class="pck-teal add-padTB" id="ultimate-btn">Free 30-Day Trial</button>
                        </span>
                    </div>
                </div>Rush Ultimate Hidden -->
                <div class="clearfix"></div>
            </div><!-- /.rs-package -->
        </div><!-- /.row -->

    </div><!-- /.container.full-width -->

    <div class="container rs-wider">

        <div class="row">
            <div class="rs-table">
                <h2>Compare Plan Features</h2>
                <table>
                    <tr class="rs-no-border-bottom">
                        <!-- <td></td>
                        <td><img src="<?php echo base_url(); ?>public/images/img-rushbasic.png"></td>
                        <td><img src="<?php echo base_url(); ?>public/images/img-rushpro.png"></td>
                        <td><img src="<?php// echo base_url(); ?>public/images/img-rushultimate.png"></td> Hide Rush Ultimate -->
                    </tr>
                    <tr class="rs-no-border-bottom">
                        <td></td>
                        <td>
                            <span class="rs-plan rsp-red">
                                <h4>RUSH Basic</h4>
                                <p>Taste of Loyalty with <br> White-Labeled Digital <br> Punch Card</p>
                            </span>
                        </td>
                        <td>
                            <span class="rs-plan rsp-yellow">
                                <h4>RUSH Pro</h4>
                                <p>White-Labeled Points <br> Program for medium to <br> Large Enterprise</p>
                            </span>
                        </td>
                        <!-- <td>
                            <span class="rs-plan rsp-teal">
                                <h4>Rush Ultimate</h4>
                                <p>RUSH-Branded, Premiere <br> Coalition Rewards Program <br> &nbsp;</p>
                            </span>
                        </td> Hide Rush Ultimate -->
                    </tr>
                    <tr class="rs-no-border-bottom">
                        <td>&nbsp;</td>
                        <td class="rstd-red">
                            Free 30-Day Trial <br>
                            <em>Waived Service Fees</em>
                        </td>
                        <td class="rstd-yellow">
                            Free 30-Day Trial <br>
                            <em>Waived Service Fees</em>
                        </td>
                        <!-- <td class="rstd-teal">Free for first 90 Days</td> Hide Rush Ultimate -->
                    </tr>
                    <tr class="rs-head rs-no-border-bottom">
                        <td colspan="4">Channels</td>
                    </tr>
                    <tr>
                        <td>
                            <span class="rs-tooltip" data-toggle="popover" data-placement="top" data-content="Run your loyalty program in stores without need for POS integration.">
                                Android Merchant Mobile App <i class="fa fa-question-circle faq-tool"></i>
                            </span>
                        </td>
                        <td><i class="ic-check"></i></td>
                        <td><i class="ic-check"></i></td>
                        <!-- <td><i class="ic-check"></i></td> Hide Rush Ultimate -->
                    </tr>
                    <tr>
                        <td>
                            <span class="rs-tooltip" data-toggle="popover" data-placement="top" data-content="Allow your customers to register and track their loyalty via mobile app.">
                                Android Customer Mobile App <i class="fa fa-question-circle faq-tool"></i>
                            </span>
                        </td>
                        <td><i class="ic-check"></i></td>
                        <td><i class="ic-check"></i></td>
                        <!-- <td><i class="ic-check"></i></td> Hide Rush Ultimate -->
                    </tr>
                    <tr>
                        <td>iOS Customer Mobile App</td>
                        <td><i class="ic ic-check"></i></td>
                        <td><i class="ic ic-check"></i></td>
                        <!-- <td><i class="ic ic-check"></i></td> Hide Rush Ultimate -->
                    </tr>
                    <tr>
                        <td>
                            <span class="rs-tooltip" data-toggle="popover" data-placement="top" data-content="Allow your customers to register or track their loyalty via your website.">
                                Web Portal <i class="fa fa-question-circle faq-tool"></i>
                            </span>
                        </td>
                        <td><i class="ic ic-close"></i></td>
                        <td><i class="ic ic-check"></i></td>
                        <!-- <td><i class="ic ic-check"></i></td> Hide Rush Ultimate -->
                    </tr>
                    <tr class="rs-no-border-bottom">
                        <td>Loyalty Card</td>
                        <td><i class="ic ic-close"></i></td>
                        <td>PHP 50 per card</td>
                        <!-- <td><i class="ic-check"></i></td> Hide Rush Ultimate -->
                    </tr>
                    <tr class="rs-head rs-no-border-bottom">
                        <td colspan="4">Management</td>
                    </tr>
                    <tr>
                        <td>
                            <span class="rs-tooltip" data-toggle="popover" data-placement="top" data-content="Design and manage your loyalty program on the fly via web.">
                                Web Dashboard <i class="fa fa-question-circle faq-tool"></i>
                            </span>
                        </td>
                        <td><i class="ic-check"></i></td>
                        <td><i class="ic-check"></i></td>
                        <!-- <td><i class="ic-check"></i></td> Hide Rush Ultimate -->
                    </tr>
                    <tr>
                        <td>Member Database</td>
                        <td><i class="ic-check"></i></td>
                        <td><i class="ic-check"></i></td>
                        <!-- <td><i class="ic-check"></i></td> Hide Rush Ultimate -->
                    </tr>
                    <tr>
                        <td>
                            <span class="rs-tooltip" data-toggle="popover" data-placement="top" data-content="Track your program&#39;s performance and understand customer behavior.">
                                Data Report &amp; Analytics <i class="fa fa-question-circle faq-tool"></i>
                            </span>
                        </td>
                        <td><i class="ic-check"></i></td>
                        <td><i class="ic-check"></i></td>
                        <!-- <td><i class="ic-check"></i></td> Hide Rush Ultimate -->
                    </tr>
                    <tr class="rs-no-border-bottom">
                        <td>
                            <span class="rs-tooltip" data-toggle="popover" data-placement="top" data-content="Conveniently send SMS to your members from your Dashboard.">
                                CMS Tool for SMS <i class="fa fa-question-circle faq-tool"></i>
                            </span>
                        </td>
                        <td>PHP 199 per month</td>
                        <td><i class="ic-check"></i></td>
                        <!-- <td><i class="ic-check"></i></td> Hide Rush Ultimate -->
                    </tr>
                    <tr class="rs-head rs-no-border-bottom">
                        <td colspan="4">Marketing Support</td>
                    </tr>
                    <tr>
                        <td>Rush Website Feature</td>
                        <td><i class="ic-check"></i></td>
                        <td><i class="ic-check"></i></td>
                        <!-- <td><i class="ic-check"></i></td> Hide Rush Ultimate -->
                    </tr>
                    <tr>
                        <td>In-store Merchandising</td>
                        <td><i class="ic-check"></i></td>
                        <td><i class="ic-check"></i></td>
                        <!-- <td><i class="ic-check"></i></td> Hide Rush Ultimate -->
                    </tr>
                    <tr>
                        <td>Additional SMS Credits</td>
                        <td>PHP 0.50 per SMS</td>
                        <td>PHP 0.50 per SMS</td>
                        <!-- <td>PHP 0.40 per SMS</td> Hide Rush Ultimate -->
                    </tr>
                    <tr class="rs-no-border-bottom">
                        <td>
                            <span class="rs-tooltip" data-toggle="popover" data-placement="top" data-content="Give your customers access to your mobile app without data connectivity.">
                                App - Zero Rating <i class="fa fa-question-circle faq-tool"></i>
                            </span>
                        </td>
                        <td>PHP 2.00 per MB</td>
                        <td>PHP 2.00 per MB</td>
                        <!-- <td><i class="ic-check"></i></td> Hide Rush Ultimate -->
                    </tr>
                </table>
            </div><!-- /.rs-table -->
        </div><!-- /.row -->
    </div><!-- /.container 2 -->

    <div class="container rs-wider xtraMarginT">
        <div class="row">
            <div class="rs-header">
                <h1>Choose the plan that&#39;s right for you.<br>Try RUSH for 30 days</h1>
                <!-- <h3>Start your trial today. You can switch plans or cancel at any time.</h3> -->
                <a href="<?php echo base_url();?>sign-up" class="start-btn prodBtn">
                    Get Started Now!
                </a>
            </div><!-- /.rs-header -->
        </div><!-- /.row -->
    </div><!-- /.container 1 -->
</section>

<script type="text/javascript">
    $(function () {
      //$('[data-toggle="tooltip"]').tooltip();
      $('[data-toggle="popover"]').popover({ trigger: "hover" });
    });
</script>
