<section class="content2">
    <div class="container rad">
    <h2>Request A Demo</h2>
        <?php if(validation_errors()) : ?>
            <div class="bg-danger">
                <?php echo validation_errors(); ?>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-md-6 contact-text">
                <p><b>Loyalty made easy.</b></p>
                <p>Build &amp; foster long-lasting customer relationships with a RUSH-powered loyalty program.</p>
                <p>Create, design and manage your own customer loyalty program easily with RUSH. Conveniently develop and deploy a custom-branded mobile app in as short as 3 days.</p>
            </div>
            <div class="col-md-6 <?php echo $contact_message_show; ?>">
                <?php echo $this->session->flashdata('contactmessage'); ?>
            </div>
            <div class="col-md-6 <?php echo $contact_form_show; ?>">
                <p class="contact-title">Contact Information</p>
                <form action="" method="post" id="form">
                    <div class="row form-input-div">
                        <div class="col-md-12">
                            <input type="name" required name="name" class="form-control" placeholder="Name*" value="<?php echo set_value('name','') ?>" required>
                        </div>
                    </div>
                    <div class="row form-input-div">
                        <div class="col-md-12">
                            <input type="mobile" required name="mobile" class="form-control" placeholder="Mobile Number*" value="<?php echo set_value('mobile','') ?>" required>
                        </div>
                    </div>
                    <div class="row form-input-div">
                        <div class="col-md-12">
                            <input type="email" required name="email" class="form-control" placeholder="Email Address*" value="<?php echo set_value('email','') ?>" required>
                        </div>
                    </div>
                    <div class="row form-input-div">
                        <div class="col-md-12 mb-1">
                            <input type="company" required name="company" class="form-control" placeholder="Company Name*" value="<?php echo set_value('company','') ?>" required>
                        </div>
                    </div>
                    <div class="row form-input-div">
                        <div class="col-md-12">
                            <p class="contact-title">Industry</p>
                            <select id="industry" name="industry" class="form-control" style="color:#999">
                                <option value="" selected >Select Industry</option>
                                <option value="Automotive Dealers and Manufacturers">Automotive Dealers and Manufacturers</option>
                                <option value="Automotive Services">Automotive Services</option>
                                <option value="Entertainment">Entertainment</option>
                                <option value="Fitness">Fitness</option>
                                <option value="Food and Beverage Establishment">Food and Beverage Establishment</option>
                                <option value="Fuel and Petroleum">Fuel and Petroleum</option>
                                <option value="Grocery and Supermarket">Grocery and Supermarket</option>
                                <option value="Grooming and Wellness">Grooming and Wellness</option>
                                <option value="Hospitals and Healthcare Services">Hospitals and Healthcare Services</option>
                                <option value="Insurance">Insurance</option>
                                <option value="Petroleum">Petroleum</option>
                                <option value="Retail - Apparel">Retail - Apparel</option>
                                <option value="Retail - Electronics">Retail - Electronics</option>
                                <option value="Retail - Household and Hardware">Retail - Household and Hardware</option>
                                <option value="Retail - Sporting Goods">Retail - Sporting Goods</option>
                                <option value="Telecommunication and Utilities">Telecommunication and Utilities</option>
                                <option value="Transportation">Transportation</option>
                                <option value="Travel and Leisure">Travel and Leisure</option>
                                <option value="Others">Others</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-input-div">
                        <div class="col-md-12">
                            <p class="contact-title">Loyalty Package</p>
                            <?php foreach ($packages as $key => $value): ?>
                            <div class="tix">
                                <input type="radio" id="<?php echo $key ?>" name="package" value="<?php echo $key ?>">
                                <label for="<?php echo $key ?>"><?php echo $value ?></label>
                            </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <div class="row form-input-div">
                        <div class="col-md-12">
                            <p class="contact-title">Objective of the Loyalty Program</p>
                            <div class="drp-down" id="objective">
                            <button type="button" class="field form-control" >Select Objective</button>
                            <div class="contents">
                                <?php foreach ($objectives as $key => $value): ?>
                                <div class="tix">
                                    <input type="checkbox" id="<?php echo $key ?>" name="objective[]" value="<?php echo $key ?>">
                                    <label for="<?php echo $key ?>"><?php echo $value ?></label>
                                </div>
                                <?php endforeach ?>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    <div class="row form-input-div">
                        <div class="col-md-12">
                            <textarea name="message" class="form-control" placeholder="Tell us more about your business (optional)"><?php echo set_value('message','') ?></textarea>
                        </div>
                    </div>
                    
                    <div class="row form-input-div">
                        <div class="col-md-12">
                            <div class="g-recaptcha" data-sitekey="6LeSJSkTAAAAAJqYJJczKaKyhU6G_0DXkRsZM_1-"></div>
                            <small class="text-danger hidden hidden-text">Please verify captcha</small>
                        </div>
                    </div>
                    
                    <input type="submit" name="submit" id="form-submit" style="display:none;">
                    <a onclick="javascript:$('#form-submit').click();" style="cursor:pointer;" class="start-btn">
                        <span>Send Request</span>
                    </a>
                </form>
            </div>
        </div>   
    </div>
</section>