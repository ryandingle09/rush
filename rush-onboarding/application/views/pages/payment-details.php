<section class="content2">
    <div class="container">
    <span class="check">
            <img src="<?php echo base_url(); ?>public/images/thankyou_thankyou_icon.png">
            <p>Your account details has been sent to your email.</p>
            <p><b><?php echo $this->session->userdata('person_email'); ?></b></p>
            <p class='spamMessage'>If you don't see the account details email in your inbox, please check your Junk or Spam folders.</p>
            <a href="<?php echo base_url(); ?>" class="start-btn">
                <span>Back to homepage</span>
            </a>
    </span> 
    </div>
</section>