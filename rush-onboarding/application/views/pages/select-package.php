<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<section class="content2 no-pad-LR">
    <div class="container rs-wider">
        <div class="row">
            <div class="rs-header">
                <h1>Choose the plan that&#39;s right for you.<br>Try RUSH for 30 days</h1>
                <!-- <h3>Start your trial today. You can switch plans or cancel at any time.</h3> -->
            </div><!-- /.rs-header -->
        </div><!-- /.row -->
    </div><!-- /.container 1 -->

    <div class="container full-width">
        <div class="row">
            <div class="rs-package">
                <div class="plan-col col-sm-6 col-md-6">
                    <div class="rs-pimg">
                        <img src="<?php echo base_url(); ?>public/images/img-rushbasic.png" style="max-width: 100%;">
                    </div>
                    <div class="plan-wrap bord-red">
                        <span class="package-amt">
                            <h2>RUSH Basic</h2>
                            <p class="para-1">Starts at</p>
                            <h1 class="amt-val"><sup>Php</sup> 1,299</h1>
                            <p>per branch per month</p>
                            <span class="arrow"></span>
                        </span>
                        <span class="rs-users">
                            <strong>Unlimited memberships</strong>
                        </span>
                        <span class="pck-btn">
                        	<?php if ($this->session->userdata('customer_no') == '5001 - 10000' || $this->session->userdata('customer_no') == '10000 and above') {
    ?>
                            <button type="submit" disabled="disabled" style="background:#eeeeee; border:none; text-decoration:line-through;" class="pck-red add-padTB" id="basic-btn" data-toggle="modal" data-target=".package-modal2" aria-label="Close" aria-hidden="true">
                            Free 30-Day Trial
                            <span class="sub">Waived service fees</span>
                            </button>
							<?php

} else {
    ?>
                            <button type="submit" class="pck-red" id="basic-btn" data-toggle="modal" data-target=".package-modal2" aria-label="Close" aria-hidden="true">
                            Begin Trial
                            <!-- <span class="sub">Enjoy waived service fees for your first 30 days!</span> -->
                            </button>
                        	<?php

} ?>
                        </span>
                    </div>
                </div>
                <div class="plan-col col-sm-6 col-md-6">
                    <div class="rs-pimg">
                        <img src="<?php echo base_url(); ?>public/images/img-rushpro.png" style="max-width: 100%;">
                    </div>
                    <div class="plan-wrap bord-yellow">
                        <span class="package-amt">
                            <h2>RUSH Pro</h2>
                            <p class="para-1">Starts at</p>
                            <h1 class="amt-val">1.5<sup>%</sup></h1>
                            <p>of Gross Transaction</p>
                            <span class="arrow"></span>
                        </span>
                        <span class="rs-users">
                            <strong>Unlimited memberships</strong>
                        </span>
                        <span class="pck-btn">
                            <button type="submit" class="pck-yellow" id="pro-btn" data-toggle="modal" data-target=".package-modal" aria-label="Close" aria-hidden="true">
                                Begin Trial
                                <!-- <span class="sub">Enjoy waived service fees for the first 30 days!</span>
                                <span class="sub">We&#39;ll also waive the one-time registration fee for your first 1,000 customers.</span> -->
                            </button>
                        </span>
                    </div>
                </div>
                <!-- <div class="plan-col col-md-4">
                    <div class="plan-wrap bord-teal">
                        <span class="package-amt">
                            <h2>RUSH Ultimate</h2>
                            <p class="para-1">Starts at</p>
                            <h1 class="amt-val"><sup>Php</sup> 1.40</h1>
                            <p>per point seed <br> &nbsp;</p>
                            <p>RUSH-Branded, Premiere <br> Coalition Rewards Program</p>
                            <span class="arrow"></span>
                        </span>
                        <span class="rs-users">
                            <strong>Unlimited memberships</strong>
                        </span>
                        <span class="pck-btn">
                            <button type="submit" class="pck-teal add-padTB" id="ultimate-btn">Free 30-Day Trial</button>
                        </span>
                    </div>
                </div>Rush Ultimate Hidden -->
                <div class="clearfix"></div>
            </div><!-- /.rs-package -->
        </div><!-- /.row -->

    </div><!-- /.container.full-width -->

    <div class="container rs-wider">

        <div class="row">
            <div class="rs-table">
                <h2>Compare Plan Features</h2>
                <table>
                    <tr class="rs-no-border-bottom">
                        <!-- <td></td>
                        <td><img src="<?php echo base_url(); ?>public/images/img-rushbasic.png"></td>
                        <td><img src="<?php echo base_url(); ?>public/images/img-rushpro.png"></td>
                        <td><img src="<?php// echo base_url(); ?>public/images/img-rushultimate.png"></td> Hide Rush Ultimate -->
                    </tr>
                    <tr class="rs-no-border-bottom">
                        <td></td>
                        <td>
                            <span class="rs-plan rsp-red">
                                <h4>RUSH Basic</h4>
                                <p>Taste of Loyalty with <br> White-Labeled Digital <br> Punch Card</p>
                            </span>
                        </td>
                        <td>
                            <span class="rs-plan rsp-yellow">
                                <h4>RUSH Pro</h4>
                                <p>White-Labeled Points <br> Program for medium to <br> Large Enterprise</p>
                            </span>
                        </td>
                        <!-- <td>
                            <span class="rs-plan rsp-teal">
                                <h4>Rush Ultimate</h4>
                                <p>RUSH-Branded, Premiere <br> Coalition Rewards Program <br> &nbsp;</p>
                            </span>
                        </td> Hide Rush Ultimate -->
                    </tr>
                    <tr class="rs-no-border-bottom">
                        <td>&nbsp;</td>
                        <td class="rstd-red">
                            Free 30-Day Trial <br>
                            <em>Waived Service Fees</em>
                        </td>
                        <td class="rstd-yellow">
                            Free 30-Day Trial <br>
                            <em>Waived Service Fees <br>
                            Waived Membership Fees <br> for first 1,000</em>
                        </td>
                        <!-- <td class="rstd-teal">Free for first 90 Days</td> Hide Rush Ultimate -->
                    </tr>
                    <tr class="rs-head rs-no-border-bottom">
                        <td colspan="4">Channels</td>
                    </tr>
                    <tr>
                        <td>
                            <span class="rs-tooltip" data-toggle="popover" data-placement="top" data-content="Run your loyalty program in stores without need for POS integration.">
                                Android Merchant Mobile App <i class="fa fa-question-circle faq-tool"></i>
                            </span>
                        </td>
                        <td><i class="ic-check"></i></td>
                        <td><i class="ic-check"></i></td>
                        <!-- <td><i class="ic-check"></i></td> Hide Rush Ultimate -->
                    </tr>
                    <tr>
                        <td>
                            <span class="rs-tooltip" data-toggle="popover" data-placement="top" data-content="Allow your customers to register and track their loyalty via mobile app.">
                                Android Customer Mobile App <i class="fa fa-question-circle faq-tool"></i>
                            </span>
                        </td>
                        <td><i class="ic-check"></i></td>
                        <td><i class="ic-check"></i></td>
                        <!-- <td><i class="ic-check"></i></td> Hide Rush Ultimate -->
                    </tr>
                    <tr>
                        <td>iOS Customer Mobile App</td>
                        <td><i class="ic ic-check"></i></td>
                        <td><i class="ic ic-check"></i></td>
                        <!-- <td><i class="ic ic-check"></i></td> Hide Rush Ultimate -->
                    </tr>
                    <tr>
                        <td>
                            <span class="rs-tooltip" data-toggle="popover" data-placement="top" data-content="Allow your customers to register or track their loyalty via your website.">
                                Web Portal <i class="fa fa-question-circle faq-tool"></i>
                            </span>
                        </td>
                        <td><i class="ic ic-close"></i></td>
                        <td><i class="ic ic-check"></i></td>
                        <!-- <td><i class="ic ic-check"></i></td> Hide Rush Ultimate -->
                    </tr>
                    <tr class="rs-no-border-bottom">
                        <td>Loyalty Card</td>
                        <td><i class="ic ic-close"></i></td>
                        <td>PHP 50 per card</td>
                        <!-- <td><i class="ic-check"></i></td> Hide Rush Ultimate -->
                    </tr>
                    <tr class="rs-head rs-no-border-bottom">
                        <td colspan="4">Management</td>
                    </tr>
                    <tr>
                        <td>
                            <span class="rs-tooltip" data-toggle="popover" data-placement="top" data-content="Design and manage your loyalty program on the fly via web.">
                                Web Dashboard <i class="fa fa-question-circle faq-tool"></i>
                            </span>
                        </td>
                        <td><i class="ic-check"></i></td>
                        <td><i class="ic-check"></i></td>
                        <!-- <td><i class="ic-check"></i></td> Hide Rush Ultimate -->
                    </tr>
                    <tr>
                        <td>Member Database</td>
                        <td><i class="ic-check"></i></td>
                        <td><i class="ic-check"></i></td>
                        <!-- <td><i class="ic-check"></i></td> Hide Rush Ultimate -->
                    </tr>
                    <tr>
                        <td>
                            <span class="rs-tooltip" data-toggle="popover" data-placement="top" data-content="Track your program&#39;s performance and understand customer behavior.">
                                Data Report &amp; Analytics <i class="fa fa-question-circle faq-tool"></i>
                            </span>
                        </td>
                        <td><i class="ic-check"></i></td>
                        <td><i class="ic-check"></i></td>
                        <!-- <td><i class="ic-check"></i></td> Hide Rush Ultimate -->
                    </tr>
                    <tr class="rs-no-border-bottom">
                        <td>
                            <span class="rs-tooltip" data-toggle="popover" data-placement="top" data-content="Conveniently send SMS to your members from your Dashboard.">
                                CMS Tool for SMS <i class="fa fa-question-circle faq-tool"></i>
                            </span>
                        </td>
                        <td>PHP 199 per month</td>
                        <td><i class="ic-check"></i></td>
                        <!-- <td><i class="ic-check"></i></td> Hide Rush Ultimate -->
                    </tr>
                    <tr class="rs-head rs-no-border-bottom">
                        <td colspan="4">Marketing Support</td>
                    </tr>
                    <tr>
                        <td>Rush Website Feature</td>
                        <td><i class="ic-check"></i></td>
                        <td><i class="ic-check"></i></td>
                        <!-- <td><i class="ic-check"></i></td> Hide Rush Ultimate -->
                    </tr>
                    <tr>
                        <td>In-store Merchandising</td>
                        <td><i class="ic-check"></i></td>
                        <td><i class="ic-check"></i></td>
                        <!-- <td><i class="ic-check"></i></td> Hide Rush Ultimate -->
                    </tr>
                    <tr>
                        <td>Additional SMS Credits</td>
                        <td>PHP 0.50 per SMS</td>
                        <td>PHP 0.50 per SMS</td>
                        <!-- <td>PHP 0.40 per SMS</td> Hide Rush Ultimate -->
                    </tr>
                    <tr class="rs-no-border-bottom">
                        <td>
                            <span class="rs-tooltip" data-toggle="popover" data-placement="top" data-content="Give your customers access to your mobile app without data connectivity.">
                                App - Zero Rating <i class="fa fa-question-circle faq-tool"></i>
                            </span>
                        </td>
                        <td>PHP 2.00 per MB</td>
                        <td>PHP 2.00 per MB</td>
                        <!-- <td><i class="ic-check"></i></td> Hide Rush Ultimate -->
                    </tr>
                </table>
            </div><!-- /.rs-table -->
        </div><!-- /.row -->
    </div><!-- /.container 2 -->
</section>

<form action="" method="post">
    <div class="modal fade package-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div id="modal-dimension" class="modal-dialog" >
            <div class="modal-content">
                 <button type="button" class="close" data-dismiss="modal">
                         <img src="<?php echo base_url(); ?>public/images/close2.png">
                </button>
                <div class ="modal-container registered-content">
                    <table id="modal-package" width="100%">
                            <tr class="header2">
                                <th colspan="2" class="loyalty">
                                    <h1 class="pck-img"><img src="<?php echo base_url(); ?>public/images/img-rushpro.png"></h1>

                                    <div class="plan-wrap bord-yellow add-ons">
                                        <span class="package-amt">
                                            <h2>RUSH Pro</h2>
                                        </span>
                                    </div>
                                </th>
                            </tr>
                            <tr>
                                <td colspan="2"><h3 style="text-algin: center!important; margin: 20px 0;">Available Add Ons</h3></td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="app[]" id="app" value="|2"><label for="app">App - Zero Rating</label></td>
                                <td>PHP 2.00 per MB</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="app[]" id="card" value="|3"><label for="card">White-Labeled Loyalty Card</label></td>
                                <td>PHP 50</td>
                            </tr>
                        </table>
                        <br/>
                        <div class="button-package">
                            <div class="row">
                                <div class="col-md-12">
                                    <a data-toggle="modal" data-target=".agree-modal-loyalty" onclick="$('.modal').modal('hide');" style="cursor:pointer;" class="start-btn3">
                                        Continue
                                    </a>
                                </div>
                                <!--<div class="col-md-6">
                                    <input type="submit" name="package" value="complete-loyalty-suite-full-version" id="complete-loyalty-suite-full-version" style="display:none;">
                                    <a onclick="javascript:$('#complete-loyalty-suite-full-version').click();" style="cursor:pointer;" class="start-btn">
                                        complete loyalty suite
                                        <span>skip the 30 day trial</span>
                                    </a>
                                </div>-->
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade package-modal2" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabell">
        <div id="modal-dimension" class="modal-dialog" >
            <div class="modal-content">
                 <button type="button" class="close" data-dismiss="modal">
                         <img src="<?php echo base_url(); ?>public/images/close2.png">
                </button>
                <div class ="modal-container registered-content">
                    <table id="modal-package" width="100%">
                            <tr class="header2">
                                <th colspan="2" class="card">
                                    <h1 class="pck-img"><img src="<?php echo base_url(); ?>public/images/img-rushbasic.png"></h1>

                                    <div class="plan-wrap bord-yellow add-ons">
                                        <span class="package-amt">
                                            <h2>RUSH Basic</h2>
                                        </span>
                                    </div>
                                </th>
                                <tr>
                                    <td colspan="2"><h3 style="text-algin: center!important; margin: 20px 0;">Available Add Ons</h3></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="app[]" id="app" value="|2"><label for="app">App - Zero Rating</label></td>
                                    <td>PHP 2.00 per MB</td>
                                </tr>
                            </tr>
                        </table>
                        <br/>
                        <div class="button-package">
                            <div class="row">
                                <div class="col-md-12" >
                                    <a data-toggle="modal" data-target=".agree-modal-punchcard" onclick="$('.modal').modal('hide');" style="cursor:pointer;" class="start-btn3">
                                        Continue
                                    </a>
                                </div>
                                <!--<div class="col-md-6">
                                	<input type="submit" name="package" value="punchcard-full-version" id="punchcard-full-version" style="display:none;">
                                    <a onclick="javascript:$('#punchcard-full-version').click();" style="cursor:pointer;" class="start-btn">
                                        complete loyalty suite
                                        <span>skip the 30 day trial</span>
                                    </a>
                                </div>-->
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade agree-modal-punchcard" tabindex="-1" role="dialog" >
        <div class="modal-dialog" >
            <div class="modal-content">
                 <button type="button" class="close" data-dismiss="modal">
                         <img src="<?php echo base_url(); ?>public/images/close.png">
                </button>
                <div class ="modal-container registered-content">
                    <table>
                        <tr class="header2"></tr>
                        <tr>
                            <td><h3 style="font-family: Open Sans Semibold;text-align: center!important; margin: 20px 0;">Service Agreement</h3></td>
                        </tr>
                        <tr>
                            <td class='serviceAgreement'>
                                <div class='saContainer'>
                                    <div class="saContent">
                                        <div id="service-agreement-content">
                                            <?php echo $serviceAgreement; ?>
                                        </div>
                                    </div>
                                    <div class="button-package saButton">
                                        <center>
                                            <label class="checkbox-terms" position="center">
                                                <input type="checkbox" value="" id="confirmServiceAgreementbasic" class="confirm" /> I acknowledge that I have read and agree to the above Service Agreement
                                            </label>
                                        </center>
                                        <div class="row">
                                            <div class="col-md-6" >
                                                <input type="submit" name="package" value="Rush Basic" id="rush-basic" style="display:none;">
                                                <a onclick="javascript:$('#rush-basic').click();" id="rush-basic-btn" style="cursor:pointer;" class="start-btn3 inactiveLink">
                                                   I AGREE
                                                   <span style="font-size:12px !important;">SIGN ME UP</span>
                                                </a>
                                            </div>
                                            <div class="col-md-6" >
                                                <a data-dismiss="modal" style="cursor:pointer; background:#ff5254; border:#ff5254; padding:25px;" class="start-btn3">
                                                    DECLINE
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade agree-modal-loyalty" tabindex="-1" role="dialog" >
        <div class="modal-dialog" >
            <div class="modal-content">
                 <button type="button" class="close" data-dismiss="modal">
                         <img src="<?php echo base_url(); ?>public/images/close.png">
                </button>
                <div class ="modal-container registered-content">
                    <table>
                        <tr class="header2"></tr>
                        <tr>
                            <td><h3 style="font-family: Open Sans Semibold;text-align: center!important; margin: 20px 0;">Service Agreement</h3></td>
                        </tr>
                        <tr>
                            <td class='serviceAgreement'>
                                <div class='saContainer'>
                                    <div class="saContent">
                                        <?php echo $serviceAgreement; ?>
                                    </div>
                                    <div class="button-package saButton">
                                        <center>
                                            <label class="checkbox-terms" position="center">
                                                <input type="checkbox" value="" id="confirmServiceAgreementpro" class="confirm" /> I acknowledge that I have read and agree to the above Service Agreement
                                            </label>
                                        </center>
                                        <div class="row">
                                            <div class="col-md-6" >
                                                <input type="submit" name="package" value="RUSH Pro" id="rush-pro" style="display:none;">
                                                <a onclick="javascript:$('#rush-pro').click();" id="rush-pro-btn" style="cursor:pointer;" class="start-btn3 inactiveLink">
                                                    I AGREE
                                                    <span style="font-size:12px !important;">SIGN ME UP</span>
                                                </a>
                                            </div>
                                            <div class="col-md-6" >
                                                <a data-dismiss="modal" style="cursor:pointer; background:#ff5254; border:#ff5254; padding:25px;" class="start-btn3">
                                                    DECLINE
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    $(function () {
      //$('[data-toggle="tooltip"]').tooltip();
      $('[data-toggle="popover"]').popover({ trigger: "hover" });
    });

$(document).ready(function() {
    // $('#confirmServiceAgreement').click(function() {
    $('.confirm').click(function() {
        if ($('#confirmServiceAgreementbasic').prop('checked')) {
            $('#rush-basic-btn').removeClass('inactiveLink');
        }
        else {
            $('#rush-basic-btn').addClass('inactiveLink');
        }
        if ($('#confirmServiceAgreementpro').prop('checked')) {
            $('#rush-pro-btn').removeClass('inactiveLink');
        }
        else {
            $('#rush-pro-btn').addClass('inactiveLink');
        }
    });
});
</script>
