<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="Fidelity" content="">
    <title>RUSH | Service Agreement</title>

    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/fonts.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/bootstrap-theme.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/style.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/stylesheets/style.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/scss/style.scss"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/chosen.css"/>

    <script src="<?php echo base_url(); ?>public/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url(); ?>public/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>public/js/chosen.jquery.js"></script>
    <script src="<?php echo base_url(); ?>public/js/custom.js"></script>
</head>
<body>

<div id="wrapper">
    <?php echo $serviceAgreementSection ?>
</div>
</body>
</html>