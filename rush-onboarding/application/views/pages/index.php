<section class="content revamp">
    <div class="container">
        <h2 class="rush-how">RUSH has got you covered</h2>
        <div class="row">
            <div class="col-md-12">
            <!-- Carousel with Tabs -->
            <div id="featCarousel" class="carousel slide" data-ride="carousel">
                <ul class="nav nav-tabs feat-tabs">
                    <li data-target="#featCarousel" data-slide-to="0" class="active">
                        <a href="" class="features"><span class="ic-feat buildrel"></span></a>
                    </li>
                    <li data-target="#featCarousel" data-slide-to="1">
                        <a href="" class="features"><span class="ic-feat plugnplay"></span></a>
                    </li>
                    <li data-target="#featCarousel" data-slide-to="2">
                        <a href="" class="features"><span class="ic-feat allaccess"></span></a>
                    </li>
                    <li data-target="#featCarousel" data-slide-to="3">
                        <a href="" class="features"><span class="ic-feat understand"></span></a>
                    </li>
                    <li data-target="#featCarousel" data-slide-to="4">
                        <a href="" class="features"><span class="ic-feat support"></span></a>
                    </li>
                </ul>
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="feat-cap">
                            <div class="container">
                                <div class="col-sm-5 col-md-5 col-sm-offset-1 col-md-offset-1 rv-feat-det">
                                    <h1>Build Relationships</h1>
                                    <p>Give your customers a reason to come back. Thrill and excite them in more ways than one with our range of dynamic loyalty tools.</p>
                                </div>
                                <div class="col-sm-6 col-md-6 rv-feat-img">
                                    <img src="<?php echo base_url(); ?>public/images/img-buildrel.png">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Item -->
                    <div class="item">
                        <div class="feat-cap">
                            <div class="container">
                                <div class="col-sm-6 col-md-6 rv-feat-img" align="right">
                                    <img src="<?php echo base_url(); ?>public/images/img-plugnplay.png">
                                </div>
                                <div class="col-sm-5 col-md-5 rv-feat-det">
                                    <h1>Plug it and play</h1>
                                    <p>Quick to setup and easy to use. Start rewarding your customers with stamps or points in a jiff.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Item -->
                    <div class="item">
                        <div class="feat-cap">
                            <div class="container">
                                <div class="col-sm-5 col-md-5 col-sm-offset-1 col-md-offset-1 rv-feat-det">
                                    <h1>Give your patrons all access</h1>
                                    <p>Scale your loyalty program through multiple channels. Connect with customers via mobile app, web, and SMS.</p>
                                </div>
                                <div class="col-sm-6 col-md-6 rv-feat-img">
                                    <img src="<?php echo base_url(); ?>public/images/img-allaccess.png">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Item -->
                    <div class="item">
                        <div class="feat-cap">
                            <div class="container">
                                <div class="col-sm-6 col-md-6 rv-feat-img" align="right">
                                    <img src="<?php echo base_url(); ?>public/images/img-understand.png">
                                </div>
                                <div class="col-sm-5 col-md-5 rv-feat-det">
                                    <h1>Understand and connect with your fans</h1>
                                    <p>Know your customers better with with actionable insights. Reach them with built-in marketing tools.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Item -->
                    <div class="item">
                        <div class="feat-cap">
                            <div class="container">
                                <div class="col-sm-5 col-md-5 col-sm-offset-1 col-md-offset-1 rv-feat-det">
                                    <h1>Get dedicated support</h1>
                                    <p>We match you with a Partner Manager dedicated to making RUSH work for you. From program set up to a walkthrough on your data &#45 we are here to answer any questions you have along the way.</p>
                                </div>
                                <div class="col-sm-6 col-md-6 rv-feat-img">
                                    <img src="<?php echo base_url(); ?>public/images/img-support.png">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Item -->
                </div>
                <!-- End Carousel Inner -->
            </div>
            <!-- End Carousel -->
            </div>
        </div>	
    </div>
</section>