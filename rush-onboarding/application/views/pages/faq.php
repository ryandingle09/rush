    <section class="content revamp">
        <div class="container">
            <h2 class="rush-how" style="text-align:left;">FAQ</h2>
            <h4 class="rush-faq">How do I Get RUSH?</h4>
            <p>Sign up for a merchant account through the RUSH website. Completely and accurately fill out the registration form, select a program package for your business, and avail of any desired add-ons. Once you’ve accepted the Terms and Conditions, an email will be sent to you with your RUSH account details. Use the credentials to log into the Merchant Dashboard where you will define and design your loyalty program and app.</p>
            <br>
            <h4 class="rush-faq">What do I get with the Free Trial?</h4>
            <p>Once you sign up for a RUSH account, we’ll give you a 30-day free trial which starts the day login credentials are sent via email. For RUSH Basic, the Monthly Service Fee for all branches will be waived during the free trial period. For RUSH Pro, the membership fee for the first 1000 customers and all transaction fees will be waived during the 30-day free trial.</p>
            <br>
            <h4 class="rush-faq">Why do we need to provide our logos, branding, and other markings to RUSH?</h4>
            <p>Because RUSH Basic and RUSH Pro are white-label solutions, we’ll need authorized access to use your logos, markings, and other branding assets to create your loyalty tools.</p>
            <br>
            <h4 class="rush-faq">How much is the set-up fee?</h4>
            <p>We do not charge any set-up fee to use RUSH.</p>
            <br>
            <h4 class="rush-faq">When will my loyalty program be ready to use?</h4>
            <p>Once you have completed Quick Start setup in the Merchant Dashboard, an email will be sent to you for an estimate of how many days it will take to publish your apps.</p>
            <br>
            <h4 class="rush-faq">How am I charged for the solution?</h4>
            <p>Depending on the package you have selected, you may be charged monthly a fixed service fee, or per transaction plus any applicable ancillary fees.</p>
            <br>
            <h4 class="rush-faq">What are the available payment options?</h4>
            <p>You can pay via cash or check deposit and credit card.</p>
            <br>
            <h4 class="rush-faq">How do I cancel my subscription?</h4>
            <p>You can cancel your subscription via the Merchant Dashboard or sending an email to terminate to <a>support@rush.ph</a>.</p>
            <br>
            <h4 class="rush-faq">Who will develop and publish my mobile apps?</h4>
            <p>Once you’ve provided your design assets, we’ll take care of developing and publishing your mobile apps to the Google Play Store and iOS App Store.</p>
            <br>
            <h4 class="rush-faq">How will RUSH benefit me?</h4>
            <p>Once you’ve provided your design assets, we’ll take care of developing and publishing your mobile apps to the Google Play Store and iOS App Store.</p>
            <hr>
            <h4 class="rush-try">We take your program a step further by empowering you with multiple ways to reward and engage, and at the same time give you deeper, actionable insight on how your patrons interact with you.</h4>
            <h4 class="rush-try">...But don’t just take our word for it. <!--<a href="https://www.google.com" style="text-decoration:underline;">!-->Try it out for free</a> to find out!</h4>
            <a class="try-btn" href="<?php echo base_url('sign-up'); ?>">Try it out for free!</a>

        </div>
    </section>