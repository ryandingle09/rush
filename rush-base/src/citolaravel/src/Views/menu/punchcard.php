<?php use Rush\Citolaravel\Helpers\MigrationHelper; ?>
<div id="sidemenu">
    <div class="logoholder">
        <a href=""><img src="<?php echo MigrationHelper::getBaseUrl() ?>assets/images/rush_logo_2016.png" alt=""></a>
    </div>
    <div id="menu">
        <ul>
            <li class="das">
                <a href="<?php echo MigrationHelper::getAppBaseUrl() ?>analytics">
                    <i class="icon" data-toggle="tooltip" data-placement="right" title="DASHBOARD"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="pro">
                <a href="<?php echo MigrationHelper::getBaseUrl() ?>promos">
                    <i class="icon" data-toggle="tooltip" data-placement="right" title="PUNCHCARD MANAGEMENT"></i>
                    <span>Punchcard Management</span>
                </a>
            </li>
            <li class="ach">
                <a href="<?php echo MigrationHelper::getAppBaseUrl() ?>achievement-unlock">
                    <i class="icon" data-toggle="tooltip" data-placement="right" title="ACHIVEMENT UNLOCK"></i>
                    <span>Achievement Unlock</span>
                </a>
            </li>
            <li class="des">
                <a href="<?php echo MigrationHelper::getAppBaseUrl() ?>design#tablet">
                    <i class="icon" data-toggle="tooltip" data-placement="right" title="DESIGN MANAGEMENT"></i>
                    <span>Design Management</span>
                </a>
            </li>
            <li class="tra">
                <a href="<?php echo MigrationHelper::getAppBaseUrl() ?>transactionhistory">
                    <i class="icon" data-toggle="tooltip" data-placement="right" title="TRANSACTION HISTORY"></i>
                    <span>Transaction History</span>
                </a>
            </li>
            <li class="man">
                <a href="<?php echo MigrationHelper::getAppBaseUrl() ?>management">
                    <i class="icon" data-toggle="tooltip" data-placement="right" title="STORE & CUSTOMER MANAGEMENT"></i>
                    <span>Store & Customer Management</span>
                </a>
            </li>
            <li class="sms">
                <a href="<?php echo MigrationHelper::getAppBaseUrl() ?>broadcast/sms">
                    <i class="icon" data-toggle="tooltip" data-placement="right" title="BROADCASTING TOOL"></i>
                    <span>Broadcasting Tool</span>
                </a>
            </li>
            <li class="hel hide">
                <a href="<?php echo MigrationHelper::getAppBaseUrl() ?>help">
                    <i class="icon" data-toggle="tooltip" data-placement="right" title="HELP"></i>
                    <span>Help</span>
                </a>
            </li>
            <li class="set">
                <a href="<?php echo MigrationHelper::getAppBaseUrl() ?>settings">
                    <i class="icon" data-toggle="tooltip" data-placement="right" title="SETTINGS"></i>
                    <span>Settings</span>
                </a>
            </li>
      			<li class="acc">
                <a href="<?php echo MigrationHelper::getAppBaseUrl() ?>account">
                    <i class="icon" data-toggle="tooltip" data-placement="right" title="ACCOUNT"></i>
                    <span>Account</span>
                </a>
            </li>
        </ul>
    </div>
</div>
