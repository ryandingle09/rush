<?php

namespace Rush\Citolaravel\Middleware;

use Closure;
use \Rush\Citolaravel\Helpers\MigrationHelper;
use Auth;
use Session;

class AuthenticateWithCi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (!$request->session()->get('login_name', null)) {
            return app('redirect')->away(MigrationHelper::getSignInUrl());
        } else {
            // check if the session belongs to the current app_id
            // $path = explode('/', $_SERVER['REQUEST_URI']);
            if ($request->session()->get('app_id', 'app') != env('APP_PREFIX', null)) {
                //MigrationHelper::clearSession();
                Auth::logout();
                Session::flush();
                return app('redirect')->away(MigrationHelper::getSignInUrl());
            }
        }

        if ($request->session()->get('forceQuickSetup')) {
            return app('redirect')->away(MigrationHelper::getCiDashboard());
        }

        return $next($request);
    }
}
