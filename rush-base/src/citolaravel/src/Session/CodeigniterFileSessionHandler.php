<?php

namespace Rush\Citolaravel\Session;

use Carbon\Carbon;
use Illuminate\Filesystem\Filesystem;
use Rush\Citolaravel\SessionData;
use SessionHandlerInterface;
use Symfony\Component\Finder\Finder;

class CodeigniterFileSessionHandler implements SessionHandlerInterface
{
    /**
     * The filesystem instance.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;

    /**
     * The path where sessions should be stored.
     *
     * @var string
     */
    protected $path;

    /**
     * The number of minutes the session should be valid.
     *
     * @var int
     */
    protected $minutes;

    /**
     * Create a new file driven handler instance.
     *
     * @param  \Illuminate\Filesystem\Filesystem  $files
     * @param  string  $path
     * @param  int  $minutes
     * @return void
     */
    public function __construct(Filesystem $files, $path, $minutes)
    {
        $this->path = $path;
        $this->files = $files;
        $this->minutes = $minutes;
    }

    /**
     * {@inheritdoc}
     */
    public function open($savePath, $sessionName)
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function close()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function read($sessionId)
    {
        $cookieName = config('ci_session.sess_cookie_name');
        if ($this->files->exists($path = $this->path.'/'.$cookieName.$sessionId)) {
            // return session data from laravelCookie if it contains CI session data
            $laravelCookieFile = $this->path.'/'.$sessionId;
            if ($this->files->exists($laravelCookieFile)) {
                $laravelSessionString = $this->files->get($laravelCookieFile);
                if (strpos($laravelSessionString, 'user_id') !== false) {
                    return $laravelSessionString;
                }
            }
            if (filemtime($path) >= Carbon::now()->subMinutes($this->minutes)->getTimestamp()) {
                $sessionString = $this->files->get($path);
                $sessionData = new SessionData($sessionString);
                return serialize($sessionData->getAll());
            }
        }

        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function write($sessionId, $data)
    {
        $this->files->put($this->path.'/'.$sessionId, $data, true);
    }

    /**
     * {@inheritdoc}
     */
    public function destroy($sessionId)
    {
        $this->files->delete($this->path.'/'.$sessionId);
    }

    /**
     * {@inheritdoc}
     */
    public function gc($lifetime)
    {
        $files = Finder::create()
                    ->in($this->path)
                    ->files()
                    ->ignoreDotFiles(true)
                    ->date('<= now - '.$lifetime.' seconds');

        foreach ($files as $file) {
            $this->files->delete($file->getRealPath());
        }
    }
}
