<?php

namespace Rush\Citolaravel;

class SessionData
{
    protected $sessionString;

    protected $data = array(
        '__ci_last_regenerate' => null,
        'user_id' => null,
        'user_type' => null,
        'subuser_id' => null,
        'login_name' => null,
        'forceQuickSetup' => null,
        'package_id' => null,
        'admin_id' => null,
        'admin_name' => null,
        'admin_type' => null,
        'is_admin' => null,
        'app_id' => null
    );

    public function __construct($sessionString)
    {
        $this->sessionString = $sessionString;
        $this->parse($this->sessionString);
    }

    public function parse($sess)
    {
        if(!isset($_SESSION)){
            session_start();
        }
        session_decode($sess);
        $sessArr = $_SESSION;

        foreach ($this->data AS $key => $value) {
            $this->data[$key] = isset($sessArr[$key]) ? $sessArr[$key] : null;
        }
    }

    public function get($key)
    {
        return isset($this->data[$key]) ? $this->data[$key] : null;
    }

    public function getAll()
    {
        return $this->data;
    }

    public function getId()
    {
        return isset($this->data['user_id']) ? $this->data['user_id'] : null;
    }

    public function isLoggedIn()
    {
        return (boolean) $this->data['login_name'];
    }
}
