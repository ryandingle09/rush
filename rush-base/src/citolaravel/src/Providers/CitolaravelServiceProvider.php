<?php

namespace Rush\Citolaravel\Providers;

use Illuminate\Support\ServiceProvider;

class CitolaravelServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // register package views
        $this->loadViewsFrom(__DIR__.'/../Views', 'citolaravel');

        if (! $this->app->routesAreCached()) {
            require __DIR__.'/../Http/routes.php';
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
