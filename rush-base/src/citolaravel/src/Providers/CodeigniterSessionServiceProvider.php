<?php

namespace Rush\Citolaravel\Providers;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\ServiceProvider;
use Rush\Citolaravel\Session\CodeigniterFileSessionHandler;
use Session;

class CodeigniterSessionServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        Session::extend('codeigniter_file', function($app) {
            // Return implementation of SessionHandlerInterface...
            return new CodeigniterFileSessionHandler(
                new Filesystem(),
                env('SESSION_SAVE_PATH', storage_path('framework/sessions')),
                120
            );
        });
    }

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}