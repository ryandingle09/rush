<?php

namespace Rush\Citolaravel\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $connection = 'mysql';

    public function getDailySignups($merchantId) {
        $count = 0;
        $sql =
        "   SELECT
            COUNT(*) AS count
            FROM Customer c
            WHERE
            c.merchantid = ?
            AND  DATE(timestamp) >= ?
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId, date('Y-m-d', time()))
        );
        $count = $query[0]->count;

        return $count;
    }

    public function getMonthlySignups($merchantId) {
        $count = 0;
        $sql =
        "   SELECT
            COUNT(*) AS count
            FROM Customer c
            WHERE
            c.merchantid = ?
            AND DATE_FORMAT(timestamp, '%Y-%m') = ?
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId, date('Y-m', time()))
        );
        $count = $query[0]->count;

        return $count;
    }

    public function getDailyPointsEarn($merchantId)
    {
        $count = 0;
        $sql =
        "   SELECT
            COUNT(*) AS count
            FROM
            CustomerTransaction ct
            LEFT JOIN Customer c ON ct.customerId = c.customerId
            WHERE
            ct.transactionType = 'earn'
            AND ct.merchant_id = ?
            AND DATE(ct.timestamp) >= ?
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId, date('Y-m-d', time()))
        );
        $count = $query[0]->count;

        return $count;
    }

    public function getMonthlyPointsEarn($merchantId)
    {
        $count = 0;
        $sql =
        "   SELECT
            COUNT(*) AS count
            FROM
            CustomerTransaction ct
            LEFT JOIN Customer c ON ct.customerId = c.customerId
            WHERE
            ct.transactionType = 'earn'
            AND ct.merchant_id = ?
            AND DATE_FORMAT(ct.timestamp, '%Y-%m') = ?
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId, date('Y-m', time()))
        );
        $count = $query[0]->count;

        return $count;
    }

    public function getDailyPointsBurn($merchantId)
    {
        $count = 0;
        $sql =
        "   SELECT
            COUNT(*) AS count
            FROM
            CustomerTransaction ct
            LEFT JOIN Customer c ON ct.customerId = c.customerId
            WHERE
            ct.transactionType IN ('paypoints', 'redeem')
            AND ct.merchant_id = ?
            AND DATE(ct.timestamp) >= ?
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId, date('Y-m-d', time()))
        );
        $count = $query[0]->count;

        return $count;
    }

    public function getMonthlyPointsBurn($merchantId)
    {
        $count = 0;
        $sql =
        "   SELECT
            COUNT(*) AS count
            FROM
            CustomerTransaction ct
            LEFT JOIN Customer c ON ct.customerId = c.customerId
            WHERE
            ct.transactionType IN ('paypoints', 'redeem')
            AND ct.merchant_id = ?
            AND DATE_FORMAT(ct.timestamp, '%Y-%m') = ?
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId, date('Y-m', time()))
        );
        $count = $query[0]->count;

        return $count;
    }

    public function getDailyRedeemed($merchantId)
    {
        $count = 0;
        $sql =
        "   SELECT
            COUNT(*) AS count
            FROM
            CustomerPunchCardReward cpcr
            LEFT JOIN MerchantPunchCardRewards mpcr ON cpcr.reward_id = mpcr.id
            WHERE
            mpcr.merchant_id = ?
            AND DATE(cpcr.date_added) >= ?
            AND cpcr.void = 0
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId, date('Y-m-d', time()))
        );
        $count = $query[0]->count;

        return $count;
    }

    public function getMonthlyRedeemed($merchantId)
    {
        $count = 0;
        $sql =
        "   SELECT
            COUNT(*) AS count
            FROM
            CustomerPunchCardReward cpcr
            LEFT JOIN MerchantPunchCardRewards mpcr ON cpcr.reward_id = mpcr.id
            WHERE
            mpcr.merchant_id = ?
            AND DATE_FORMAT(cpcr.date_added, '%Y-%m') = ?
            AND cpcr.void = 0
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId, date('Y-m', time()))
        );
        $count = $query[0]->count;

        return $count;
    }

    public function getDailyTransactions($merchantId)
    {
        $count = 0;
        $sql =
        "   SELECT
            COUNT(*) AS count
            FROM
            CustomerPunchCardTransactions cpct
            WHERE
            cpct.merchant_id = ?
            AND DATE(cpct.date_punched) >= ?
            AND cpct.void = 0
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId, date('Y-m-d', time()))
            );
        $count = $query[0]->count;

        return $count;
    }

    public function getMonthlyTransactions($merchantId)
    {
        $count = 0;
        $sql =
        "   SELECT
            COUNT(*) AS count
            FROM
            CustomerPunchCardTransactions cpct
            WHERE
            cpct.merchant_id = ?
            AND DATE_FORMAT(cpct.date_punched, '%Y-%m') = ?
            AND cpct.void = 0
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId, date('Y-m', time()))
            );
        $count = $query[0]->count;

        return $count;
    }
}
