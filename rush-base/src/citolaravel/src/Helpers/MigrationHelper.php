<?php

namespace Rush\Citolaravel\Helpers;

class MigrationHelper
{
    const CI_FOLDER = 'cpanel';
    const BASE_FOLDER = 'rush-base';
    const APP_FOLDER = 'app';
    const ONBOARDING_FOLDER = 'onboarding';
    const CPANEL_FOLDER = 'cpanel';

    static function getServerName()
    {
        if (!isset($_SERVER['SERVER_NAME'])) {
            $url = parse_url(env('APP_URL', 'http://localhost'));
            $_SERVER['SERVER_NAME'] = $url['host'];
        }

        return $_SERVER['SERVER_NAME'];
    }

    static function getBasePrefix()
    {
        return getenv('BASE_PREFIX') ?: self::BASE_FOLDER;
    }

    static function getAppPrefix()
    {
        return getenv('APP_PREFIX') ?: self::APP_FOLDER;
    }

    static function getOnboardingPrefix()
    {
        return getenv('ONBOARDING_PREFIX') ?: self::ONBOARDING_FOLDER;
    }

    static function getCpanelPrefix()
    {
        return getenv('CPANEL_PREFIX') ?: self::CPANEL_FOLDER;
    }

    static function getServerUrl()
    {
        return 'http://'.self::getServerName().'/';
    }

    static function getBaseUrl()
    {
        return self::getServerUrl().self::getCpanelPrefix().'/';
    }

    static function getAppBaseUrl()
    {
        return self::getServerUrl().self::getAppPrefix().'/';
    }

    static function getSignInUrl()
    {
        return self::getServerUrl().self::getOnboardingPrefix().'/sign-in';
    }

    static function getVendorViewPath()
    {
        return '../../vendor/Fidelity-section8/'.self::getBasePrefix().'/src/citolaravel/src/Views/';
    }

    static function getCiDashboard()
    {
        return self::getServerUrl().self::getCpanelPrefix().'/dashboard';
    }

    static function postCURL($_url, $_param)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($_param));

        session_write_close();
        $output=curl_exec($ch);

        curl_close($ch);

        return $output;
    }

    static function deleteSessionfile($file)
    {
        if (file_exists($file)) {
            unlink($file);
        }
    }

    static function clearSession()
    {
        if (isset($_COOKIE['ci_session'])) {
            self::deleteSessionfile(getenv('SESSION_SAVE_PATH').'/'.$_COOKIE["ci_session"]);
            self::deleteSessionfile(getenv('SESSION_SAVE_PATH').'/ci_session'.$_COOKIE["ci_session"]);
            unset($_COOKIE['ci_session']);
            setcookie('ci_session', '', time() - 3600, '/');
        }
        if (isset($_COOKIE['PHPSESSID'])) {
            self::deleteSessionfile(getenv('SESSION_SAVE_PATH').'/sess_'.$_COOKIE["PHPSESSID"]);
            unset($_COOKIE['PHPSESSID']);
            setcookie('PHPSESSID', '', time() - 3600, '/');
        }
    }
}
