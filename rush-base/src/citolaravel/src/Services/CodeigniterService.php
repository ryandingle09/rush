<?php
namespace Rush\Citolaravel\Services;

use Cookie;
use Rush\Citolaravel\SessionData;
use Illuminate\Session\FileSessionHandler;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Session;

class CodeigniterService
{
    protected $data;

    public function __construct()
    {
/*        session_name('ci_session');
        //$sess_id = session_id();
        //session_id('ci_session'.$_COOKIE['ci_session']);
        session_start();
        var_dump(session_name());
        var_dump(session_id());
        var_dump($_COOKIE);
        var_dump($_SESSION);
        // var_dump(Session::all());
        exit;*/

        $cookieName = config('ci_session.sess_cookie_name');
        $cookieValue = Cookie::get($cookieName);

        // match the codeigniter cookie filename
        $ciSession = $cookieName.$cookieValue;
        $sessionHandler = new FileSessionHandler(
            new Filesystem(),
            env('SESSION_SAVE_PATH', storage_path('framework/sessions')),
            120
        );
        $sess = $sessionHandler->read($ciSession);
        $this->data = new SessionData($sess);
    }

    public function getData()
    {
        return $this->data;
    }
}
