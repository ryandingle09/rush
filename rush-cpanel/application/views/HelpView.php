<div id="rightcontent" >
    <div class="header">
        <div class="row">
            <div class="col-xs-7">
                <h3>Help</h3>
            </div>
            <div class="col-xs-5">
                <div class="howdy">

                    <!-- Split button -->
                    <div class="btn-group">
                        <!-- Large button group -->
                        <div class="btn-group">
                            <button class="btn btn-default btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="{{base_url}}assets/images/avatar.png" alt=""/> <span>Hi, {{login_name}}!</span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                              <li><a href="{{base_url}}account">Change Password</a></li>
                              <li><a href="{{base_url}}logout">Logout</a></li>
                            </ul>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container" >
        <div style="height:600px;">
            <object data="{{base_url}}{{ helpPdf }}" type="application/pdf" width="100%" height="100%">
                <iframe src="{{base_url}}{{ helpPdf }}" width="100%" height="100%" style="border: none;">This browser does not support PDFs. Please download the PDF to view it: <a href="{{base_url}}{{ helpPdf }}">Download PDF</a>
                </iframe>
            </object>
        </div><!--/content-->
    </div> <!-- /container -->
</div> <!--rightcontent-->
