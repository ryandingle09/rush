<div id="rightcontent">
    <div class="header">
        <div class="row">
            <div class="col-xs-7">
                <h3>Transaction History</h3>
            </div>
            <div class="col-xs-5">
                <div class="howdy">

                    <!-- Split button -->
                    <div class="btn-group">
                        <!-- Large button group -->
                        <div class="btn-group">
                            <button class="btn btn-default btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="{{base_url}}assets/images/avatar.png" alt=""/> <span>Hi, {{login_name}}!</span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                              <li><a href="{{base_url}}account">Change Password</a></li>
                              <li><a href="{{base_url}}logout">Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <label></label>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class='input-group date datetimepicker' id='dateStart'>
                                <input type='text' class="form-control" name="dateStart" placeholder="Date Start" data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="" required>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class='input-group date datetimepicker' id='dateEnd'>
                                <input type='text' class="form-control" name="dateEnd" placeholder="Date End" data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="" required>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5 forAddBtn pull-right filterLabel">
                <div class="input-group">
                    <span class="input-group-addon " id="basic-addon2">&nbsp;</span>
                </div>
            </div>
        </div>
        <div id="content" class="transHistory hide">
            <div class="noTab">
                <table id="historyTable" class="data-table management">
                    <thead>
                    <tr>
                        <th data-sortable="true">Date &amp; Time</th>
                        <th>Promo Title</th>
                        <th>Transaction Type</th>
                        <th data-sortable="true">Customer Name</th>
                        <th>Transaction Amount</th>
                        <th>Reward/Stamps</th>
                        <th>Branch</th>
                        <th data-filter-control="select">Emp#</th>
                    </tr>
                    </thead>
                    <tbody>
		            {% foreach transactions as transaction %}
                    <tr>
                        <td>{{transaction[timestamp]}}</td>
                        <td>{{transaction[promoTitle]}}</td>
                        <td>{{transaction[transactionType]}}</td>
                        <td>{{transaction[customerName]}}</td>
                        <td>{{transaction[amountPaidWithCash]}}</td>
                        <td>{{transaction[stamps]}}</td>
                        <td>{{transaction[branchName]}}</td>
                        <td>{{transaction[employeeNumber]}}</td>
                    </tr>
					{% endforeach %}
                    </tbody>
                </table>
            </div><!--noTab-->
        </div><!--/content-->
    </div> <!-- /container -->
</div> <!--rightcontent-->
<style type="text/css">
    tfoot input {
        width: 100%;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
    }
</style>