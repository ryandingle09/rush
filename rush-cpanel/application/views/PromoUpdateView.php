<div id="rightcontent">
  <div class="header">
    <div class="row">
      <div class="col-xs-7">
        <h3>Punchcard Management</h3>
      </div>
      <div class="col-xs-5">
        <div class="howdy">
          <!-- Split button -->
          <div class="btn-group">
            <!-- Large button group -->
            <div class="btn-group">
              <button class="btn btn-default btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img src="{{base_url}}assets/images/avatar.png" alt=""/> <span>Hi, {{login_name}}!</span>
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                <li><a href="{{base_url}}account">Change Password</a></li>
                <li><a href="{{base_url}}logout">Logout</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <form action="{{base_url}}promos/updatePromoMechanics/{{promoId}}" method="POST" role="form" data-toggle="validator" enctype="multipart/form-data" id="updatePromoMechanics">
      <div id="content" class="promos spacing">
        <section>
          <h4 class="text-center">Update Punchcard</h4>
          {{errors}}
          <h5 class="withIco">STEP 1: General Mechanics</h5>
          <div class="form-group">
            <label for="promoTitle">Promo Title</label>
            <input type="text" class="form-control" name="promoTitle" id="promoTitle" placeholder="Enter promo title" value="{{ promo_title }}" maxlength="100" required>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label>Duration of Promo</label>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class='input-group date durationStart datetimepicker' id='promoDurationStart'>
                      <input type='text' class="form-control" name="durationStart" id="durationStart" placeholder="Start"  data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="{{duration_from_date}}" required>
                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                    <div class="help-block with-errors"></div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class='input-group date durationEnd datetimepicker' id='promoDurationEnd'>
                      <input type='text' class="form-control" name="durationEnd" id="durationEnd" placeholder="End" data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="{{duration_to_date}}" required>
                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                    <div class="help-block with-errors"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Redemption Period</label>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <div class='input-group date redemptionDurationStart datetimepicker' id="promoRedemptionDurationStart">
                        <input type='text' class="form-control" name="redemptionDurationStart" id="redemptionDurationStart" placeholder="Start" data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="{{redemption_duration_from_date}}" required>
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                      </div>
                      <div class="help-block with-errors"></div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <div class='input-group date redemptionDurationEnd datetimepicker' id="promoRedemptionDurationEnd">
                        <input
                        type='text'
                        class="form-control"
                        name="redemptionDurationEnd"
                        id="redemptionDurationEnd"
                        placeholder="End"
                        data-error="Invalid date value"
                        pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="{{redemption_duration_to_date}}" required>
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                      </div>
                      <div class="help-block with-errors"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section>
          <h5>STEP 2: Stamp Conversion</h5>
          <div class="stamp-conversion">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <div class="clearfix">
                    <div class="stamp-no">
                      <label><strong>1</strong> Stamp = Php </label>
                    </div>
                    <div class="stamp-amount">
                      <div class="form-group">
                        <input
                        class="form-control"
                        name="amount"
                        placeholder="0.00"
                        data-error="Invalid amount"
                        pattern="^[-+]?[0-9]*\.?[0-9]+$"
                        value="{{ amount }}" required>
                        <div class="help-block with-errors"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <label for="stamps">Click on stamp below to add reward.</label>
                <div class="" id="stamps">
                  <ul class="">
                    {{ promoUpdateViewStamps }}
                  </ul>
                </div>
                {{ addRemoveButton }}
              </div>
            </div>
          </div>
        </section>
        <div class="form-group">
          <input type="hidden" name="status" value="{{status}}">
          <input type="hidden" name="rewards" value="[]">
          <button class="btn btn-warning saveButton savePromo" type="submit">Draft</button>
          <button class="btn btn-primary saveButton publishPromo" type="button">Publish</button>
        </div>
      </div><!--/content-->
    </form>
  </div> <!-- /container -->
</div> <!--rightcontent-->