<div id="rightcontent">
    <div class="header">
        <div class="row">
            <div class="col-xs-7">
                <h3>Account Settings</h3>
            </div>
            <div class="col-xs-5">
                <div class="howdy">

                    <!-- Split button -->
                    <div class="btn-group">
                        <!-- Large button group -->
                        <div class="btn-group">
                            <button class="btn btn-default btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="{{base_url}}assets/images/avatar.png" alt=""/> <span>Hi, {{login_name}}!</span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="{{base_url}}account">Change Password</a></li>
                                <li><a href="{{base_url}}logout">Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <div class="container">

        <div id="content" class="management spacing account">
            <h4>Change Password</h4>
            <form action="{{base_url}}account/changepass" id="changePassword" method="post">
              <div class="row">
                <div class="col-sm-5">
                      <div class="form-group">
                          <label for="old_password">Old Password *</label>
                          <input type="password" class="form-control" name="old_password" id="old_password" maxlength="32">
                      </div>
                      <div class="form-group">
                          <label for="new_password">New Password *</label>
                          <input type="password" class="form-control" name="new_password" id="new_password" maxlength="32">
                      </div>
                      <div class="form-group">
                          <label for="confirm_password">Confirm New Password *</label>
                          <input type="password" class="form-control" name="confirm_password" id="confirm_password" maxlength="32">
                      </div>
                </div>
                <div class="col-md-12">
                  <button class="btn btn-primary updateProfile" type="submit">Update</button>
                </div>
              </div>
              </form>


        </div><!--/content-->
    </div> <!-- /container -->
</div> <!--rightcontent-->
