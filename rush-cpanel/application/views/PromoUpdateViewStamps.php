<?php for ($i = 1; $i <= $n; $i++): ?>
<li>
    <a href="#stampModal<?php echo $i; ?>" class="{{reward_stamp_class_<?php echo $i; ?>}}" data-toggle="modal" data-target="#stampModal<?php echo $i; ?>"><?php echo $i; ?></a>
    <div class="modal fade" id="stampModal<?php echo $i; ?>" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h9 class="modal-title" id="stampModal<?php echo $i; ?>Label">STAMP <?php echo $i; ?></h9>
                </div>
                <div class="modal-body">
                    <div class='alert alert-danger hide'>
                        <p>Image does not meet the image size requirement. Please check again.</p>
                    </div>
                    <div class="form-group">
                        <span class="btn btn-success fileinput-button {{reward_img_class_<?php echo $i; ?>}}">
                            <span>Click to add/change image <br /> <i>Required size is 350px x 200px</i></span>
                            <input id="stamp_reward_image_<?php echo $i; ?>" type="file" name="stamp_reward_image_<?php echo $i; ?>">
                            <input type="hidden" name="stamp_reward_change<?php echo $i; ?>" value="0">
                            {{reward_image_canvas_<?php echo $i; ?>}}
                        </span>
                    </div>
                    <div class="form-group">
                        <label>Reward Description</label>
                        <textarea class="stamp_reward_description_<?php echo $i; ?> form-control" name="stamp_reward_description_<?php echo $i; ?>" maxlength="75">{{reward_description_<?php echo $i; ?>}}</textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-6">
                            <button type="button" class="btn btn-default clearStamp">Clear</button>
                        </div>
                        <div class="col-md-6">
                            <button type="button" class="btn btn-primary saveStamp" >Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</li>
<?php endfor ?>