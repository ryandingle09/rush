<div id="rightcontent">
    <div class="header">
        <div class="row">
            <div class="col-xs-7">
                <h3>Rewards Catalogue</h3>
            </div>
            <div class="col-xs-5">
                <div class="howdy">

                    <!-- Split button -->
                    <div class="btn-group">
                        <!-- Large button group -->
                        <div class="btn-group">
                            <button class="btn btn-default btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="{{base_url}}assets/images/avatar.png" alt=""/> <span>Hi, {{login_name}}!</span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                              <li><a href="{{base_url}}account">Change Password</a></li>
                              <li><a href="{{base_url}}logout">Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">

        <div id="content" class="redemption spacing">
            <div class="noTab">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Items to be redeemed</h4>
                    </div>
                    <form method="post" action="{{base_url}}redemption/save" enctype="multipart/form-data">
                    <div class="itemsRedeemed webLabels">
                        <div class="col-md-1">
                            <label for="rProdCount">&nbsp;</label>
                        </div>                        
                        <div class="col-md-2">
                            <label for="rProdName redemption-header">Product Name</label>
                        </div>
                        <div class="col-md-2">
                            <label for="rProdImg redemption-header">Product Image</label>
                        </div>
                        <div class="col-md-3 ">
                            <label for="rDescription redemption-header">Description</label>
                        </div>
                        <div class="col-md-2 ">
                            <label for="rDescription redemption-header">Points</label>
                        </div>
                    </div><!--itemsRedeemed-->
                    <div id="redeem-rows">
                        {% foreach redemptions as redemption %}
                        <div class="itemsRedeemed">
                            <div class="col-md-1">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="number">{{redemption[number]}}</i></span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                    <span class="redemption-name">{{redemption[name]}}</span>
                                    <input type="hidden" name="productName[]" value="{{redemption[name]}}">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group rProdImg redemption-image">
                                  <img class="imgPrev" alt="" src="{{redemption[imageURL]}}">
                                  <span class="btn btn-default btn-file">Browse <input type="file" name="productImage[]"></span>
                                </div>
                            </div>
                            <div class="col-md-3 ">
                                <div class="input-group">
                                  <span class="redemption-description">{{redemption[details]}}</span>
                                  <input type="hidden" name="productDescription[]" value="{{redemption[details]}}">
                                </div>
                            </div>
                            <div class="col-md-2 ">
                                <div class="input-group">
                                  <span class="redemption-points">{{redemption[pointsRequired]}} pts</span>
                                  <input type="hidden" name="points[]" value="{{redemption[pointsRequired]}}">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <a href="" onclick="return false"><i class="fa edit fa-pencil edit-redeem"></i></a>
                                <a href="" onclick="return false"><i class="fa del fa-times remove-redeem"></i></a>
                            </div>
                            <input type="hidden" name="redeemId[]" class="rRedeemId" id="rRedeemId" value="{{redemption[redeemItemId]}}">
                        </div><!--itemsRedeemed-->
                        {% endforeach %}
                    </div>
                    <div class="col-md-12">
                        <button class="btn btn-default addRedeemItem" type="button"><i class="fa fa-plus"></i> Add Item</button>
                    </div>

                     <div class="col-md-12">
                        <br clear="all" />
                        <button class="btn btn-primary saveButton" type="submit">Save</button>
                    </div>
                    </form>
                </div>
            </div>
        </div><!--/content-->
    </div> <!-- /container -->
</div> <!--rightcontent-->
{{ redeemRowTemplate }}
{{ redeemRowEditTemplate }}