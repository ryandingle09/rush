<div id="rightcontent">
    <div class="header">
        <div class="row">
            <div class="col-xs-7">
                <h3>Achivement Unlock</h3>
            </div>
            <div class="col-xs-5">
                <div class="howdy">

                    <!-- Split button -->
                    <div class="btn-group">
                        <!-- Large button group -->
                        <div class="btn-group">
                            <button class="btn btn-default btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="{{base_url}}assets/images/avatar.png" alt=""/> <span>Hi, {{login_name}}!</span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                              <li><a href="{{base_url}}account">Change Password</a></li>
                              <li><a href="{{base_url}}logout">Logout</a></li>
                            </ul>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div id="content" class="achiveUnlock spacing">
            <div class="noTab">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Achievements to be unlocked</h4>
                    </div>
                    <form method="post" action="{{base_url}}achievementunlock/addAchievements">
                    <div class="achiveMents">
                      <div class="col-xs-4">
                          <label for="aSharePoints">Achievement</label>
                      </div>
                      <div class="col-xs-1 noPadLeft noPadRight">
                          <label for="">Points</label>
                      </div>
                      <div class="col-xs-1 noPadLeft noPadRight">
                          <label for="">Multiplier</label>
                      </div>
                      <div class="col-xs-2">
                          <label for="expiration">From</label>
                      </div>
                      <div class="col-xs-2">
                          <label for="expiration">To</label>
                      </div>
                    </div><!--achiveMents-->
                    <div class="achievements" id="achievements-row">
                      {% foreach achievements as achievement %}
                      <div class="achievement" id="achievement-row">
                        <div class="col-xs-4">
                            <div class="input-group">
                              <span class="input-group-addon"><i class="number">1</i></span>
                              <input type="text" class="form-control description" name="description[]" placeholder="Share 200 points to a friend">

                            </div>
                        </div>
                        <div class="col-xs-1 noPadLeft noPadRight">
                            <div class="input-group">
                              <input type="text" class="form-control points" name="points[]" value="20" placeholder="">
                              <span class="input-group-addon mul-color">x</span>
                            </div>
                        </div>
                        <div class="col-xs-1 noPadLeft noPadRight">
                            <div class="input-group">
                              <select name="multiplier[] multiplier" class="form-control">
                                  <option value="1">1</option>
                                  <option value="2">2</option>
                              </select>
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="input-group date fordatepkr">
                                <input type="text" class="form-control fromDate" name="fromDate[]">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="input-group date todatepkr">
                                <input type="text" class="form-control toDate" name="toDate[]">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-xs-1">
                            <a class="remove-achivement"><i class="fa del fa-times"></i></a>
                        </div>
                      </div><!--achiveMents-->
                      {% endforeach %}      
                    </div>
                    
                    <div class="col-xs-12">
                        <button class="btn btn-default addAchievement" type="button"><i class="fa fa-plus"></i> Add Achievement</button>
                    </div>
                     <div class="col-xs-12">
                          <button class="btn btn-primary saveButton" type="submit">Save</button>
                    </div>
                  </form>
                </div>
            </div>
        </div><!--/content-->
    </div> <!-- /container -->
</div> <!--rightcontent-->
