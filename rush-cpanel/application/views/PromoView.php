<div id="rightcontent">
    <div class="header">
        <div class="row">
            <div class="col-xs-7">
                <h3>Punchcard Management</h3>
            </div>
            <div class="col-xs-5">
                <div class="howdy">
                    <!-- Split button -->
                    <div class="btn-group">
                        <!-- Large button group -->
                        <div class="btn-group">
                            <button class="btn btn-default btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="{{base_url}}assets/images/avatar.png" alt=""/> <span>Hi, {{login_name}}!</span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                              <li><a href="{{base_url}}account">Change Password</a></li>
                              <li><a href="{{base_url}}logout">Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">

      <div id="content" class="promos promo-list">
        <a href="{{ base_url }}promos/add" class="btn btn-primary btn-large new-punchcard">+ New Punchcard</a>
        {{ message }}
        {{ errors }}
        <table id="punchcardView" data-toggle="table" class="data-table punchcard" data-pagination="true" data-search="true">
            <thead>
                <tr>
                    <th>Promo</th>
                    <th>Status</th>
                    <th>Duration</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {% foreach promos as promo %}
                <tr>
                    <td>{{ promo[title] }}</td>
                    <td>{{ promo[status] }}</td>
                    <td>{{ promo[duration_from_date] }} - {{ promo[duration_to_date] }}</td>
                    <td>
                        <a href="#" data-toggle="modal" data-target="#deletePromo{{promo[id]}}" class="deletePromo">
                            <i class="fa fa-trash"></i>Delete
                        </a>
                        <a href="{{base_url}}promos/update/{{promo[id]}}" class="promoEdit" style="margin-left:10px;">
                            <i class="fa fa-pencil"></i>Edit
                        </a>
                        <!-- Modal -->
                        <div class="modal fade" id="deletePromo{{promo[id]}}" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title">Confirm Delete</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Are you sure you want to delete "{{promo[title]}}"?</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                    <a href="{{base_url}}promos/delete/{{promo[id]}}" class="btn btn-danger">Yes</a>
                                </div>
                              </div>
                            </div>
                        </div>
                        <!-- ./ Modal -->
                    </td>
                </tr>
                {% endforeach %}
            </tbody>
        </table>
      </div><!--/content-->
    </div> <!-- /container -->
</div> <!--rightcontent-->




<!--  MODALS -->
<div id="addTablet" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Add Tablet</div>
            <div class="modal-body">
                <form action="">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="tabletID">Tablet ID (IMEI)</label>
                            <input type="text" class="form-control" name="tabletID" id="tabletID">
                        </div>
                        <div class="col-md-12">
                            <label for="tabletBrand">Tablet Brand</label>
                            <select name="tabletBrand" id="tabletBrand" class="form-control">
                                <option value="">Samsung</option>
                                <option value="">Apple</option>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label for="branchAssigned">Branch Assigned to</label>
                            <select name="branchAssigned" id="branchAssigned" class="form-control">
                                <option value="">Megamall</option>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button class="btn btn-primary pull-right">ADD</button>
                        </div>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>
