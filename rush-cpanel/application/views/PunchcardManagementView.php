<div id="rightcontent">
    <div class="header">
        <div class="row">
            <div class="col-xs-7">
                <h3>Management</h3>
            </div>
            <div class="col-xs-5">
                <div class="howdy">

                    <!-- Split button -->
                    <div class="btn-group">
                        <!-- Large button group -->
                        <div class="btn-group">
                            <button class="btn btn-default btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="{{base_url}}assets/images/avatar.png" alt=""/> <span>Hi, {{login_name}}!</span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                              <li><a href="{{base_url}}account">Change Password</a></li>
                              <li><a href="{{base_url}}logout">Logout</a></li>
                            </ul>
                        </div>


                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="container">
        <div id="content" class="management marginTop">
            <!-- Nav tabs -->
            <div class="navTabsWrap">
                <ul class="nav nav-tabs" role="tablist" id="tabs">
                    <li role="presentation" class="active"><a href="#branch" aria-controls="branch" role="tab" data-toggle="tab">BRANCH</a></li>
                    <li role="presentation"><a href="#employee" aria-controls="employee" role="tab" data-toggle="tab">EMPLOYEE</a></li>
                    <li role="presentation"><a href="#customer" aria-controls="customer" role="tab" data-toggle="tab">CUSTOMER</a></li>
                    <li role="presentation"><a href="#points" aria-controls="points" role="tab" data-toggle="tab">STAMPS SUMMARY</a></li>
                    <li role="presentation"><a href="#feedback" aria-controls="feedback" role="tab" data-toggle="tab">CUSTOMER FEEDBACK</a></li>
                </ul>
                <a href="#addBranch" data-toggle="modal" class="addNew addBranch" onclick="return false"><i class="fa fa-plus"></i> Add Branch</a>
            </div>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="branch">
                    <table id="branchTable" data-toggle="table" class="data-table management" data-pagination="true" data-search="true">
                        <thead>
                        <tr>
                            <!-- <th>Branch ID</th> -->
                            <th>Branch Name</th>
                            <th>Branch Address</th>
                            <th>Branch Logo</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {% foreach branches as branch %}
                        <tr data-id="{{branch[id]}}" data-name="{{branch[branchName]}}" data-street-name="{{branch[street_name]}}" data-district="{{branch[district]}}" data-city="{{branch[city]}}" data-zipcode="{{branch[zipcode]}}" data-branchlogo="{{branch[branch_logo]}}">
                            <!-- <td>{{branch[id]}}</td> -->
                            <td>{{branch[branchName]}}</td>
                            <td>{{branch[street_name]}}, {{branch[district]}}, {{branch[city]}}, {{branch[zipcode]}}</td>
                            <td><img src="{{branch[branch_logo]}}" class="imgPrev" alt="" height="64" width="64"></td>
                            <td>
                                <!-- href="{{base_url}}management/deletebranch/{{branch[id]}}" -->
                                <a href="#" data-toggle="modal" data-target="#deleteBranch" data-action="{{base_url}}management/deletebranch/{{branch[id]}}" onclick="return false" class="delete branchDelete"><i class="fa fa-trash"></i>Delete</a>
                                <a href="#" data-toggle="modal" data-target="#editBranch" onclick="return false" class="edit branchEdit"><i class="fa fa-pencil"></i>Edit</a>
                            </td>
                        </tr>
                        {% endforeach %}
                        </tbody>
                    </table>
                    {{branchQrCodeLink}}
                </div>
                <div role="tabpanel" class="tab-pane" id="employee">
                    <table id="employeeTable" data-toggle="table" class="data-table management" data-pagination="true" data-search="true">
                        <thead>
                        <tr>
                            <th>Employee ID No.</th>
                            <th  data-sortable="true">Employee Name</th>
                            <th>Branch Assigned</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {% foreach employees as employee %}
                        <tr data-id="{{employee[userId]}}" data-employee="{{employee[employeeName]}}" data-requirepass="{{employee[requirePassword]}}" data-fourdigit="{{employee[fourDigit]}}" data-branch="{{employee[branchId]}}">
                            <td>{{employee[userId]}}</td>
                            <td>{{employee[employeeName]}}</td>
                            <td>{{employee[branch]}}</td>
                            <td>{{employee[status]}}</td>
                            <td>
                                <a href="#" data-toggle="modal" data-target="#deleteEmployee" class="delete employeeDelete" data-action="{{base_url}}management/deleteemployee/{{employee[userId]}}"><i class="fa fa-trash"></i> Delete</a>
                                <a href="#" data-toggle="modal" data-target="#editEmployee" class="edit employeeEdit"  onclick="return false"><i class="fa fa-pencil"></i> Edit</a>
                            </td>
                        </tr>
                        {% endforeach %}
                        </tbody>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane" id="customer">
                    <table id="customerTable" class="data-table management">
                        <thead>
                        <tr>
                            <th data-sortable="true">Date Registered</th>
                            <th data-sortable="true">Customer Name</th>
                            <th>Mobile No.</th>
                            <th>Email Address</th>
                            <th>Birthdate</th>
                            <th>Gender</th>
                            <th>Registration Channel</th>
                            <th>Branch</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% foreach customers as customer %}
                        <tr>
                            <td>{{customer[timestamp]}}</td>
                            <td>{{customer[fullName]}}</td>
                            <td>{{customer[mobileNumber]}}</td>
                            <td>{{customer[email]}}</td>
                            <td>{{customer[birthDate]}}</td>
                            <td>{{customer[gender]}}</td>
                            <td>{{customer[registrationChannel]}}</td>
                            <td>{{customer[branchName]}}</td>
                        </tr>
                        {% endforeach %}
                        </tbody>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane" id="points">
                    <table id="pointsTable" class="data-table management">
                        <thead>
                        <tr>
                            <th data-sortable="true">Date Registered</th>
                            <th data-sortable="true">Customer Name</th>
                            <th data-sortable="true">Mobile No.</th>
                            <th data-sortable="true">Target</th>
                            <th data-sortable="true">Accomplished</th>
                            <th data-sortable="true">Balance</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% foreach stamps as stamp %}
                        <tr>
                            <td>{{stamp[timestamp]}}</td>
                            <td>{{stamp[fullName]}}</td>
                            <td>{{stamp[mobileNumber]}}</td>
                            <td>{{stamp[target]}}</td>
                            <td>{{stamp[accomplished]}}</td>
                            <td>{{stamp[balance]}}</td>
                        </tr>
                        {% endforeach %}
                        </tbody>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane" id="feedback">
                    <table id="reportsTable" class="data-table management">
                        <thead>
                        <tr>
                            <th data-sortable="true">Date</th>
                            <th data-sortable="true">Customer Name</th>
                            <th data-sortable="true">Mobile No.</th>
                            <th data-sortable="true">Message</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% foreach reports as report %}
                        <tr data-id="{{report[id]}}" data-date="{{report[created_at]}}" data-customer="{{report[fullName]}}" data-mobile="{{report[mobileNumber]}}" data-type="{{report[type]}}" data-message="{{report[message]}}">
                            <td>{{report[created_at]}}</td>
                            <td>{{report[fullName]}}</td>
                            <td>{{report[mobileNumber]}}</td>
                            <td class="text-center">
                                <a href="#" data-toggle="modal" data-target="#viewMessage" class="view-message" onclick="return false"><i class="fa {{report[class]}}"></i></a>
                            </td>
                        </tr>
                        {% endforeach %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!--/content-->
    </div> <!-- /container -->
</div> <!--rightcontent-->


<!--  MODALS -->
<div id="viewMessage" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">View Feedback</div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-md-4"><b>Date</b></div>
                    <div class="col-md-6" id="view-report-date"></div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4"><b>Customer Name</b></div>
                    <div class="col-md-6" id="view-report-customer"></div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4"><b>Mobile Number</b></div>
                    <div class="col-md-6" id="view-report-mobile"></div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4"><b>Message</b></div>
                    <div class="col-md-6" id="view-report-message"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="addBranch" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Add Branch</div>
            <div class="modal-body">
                <form method="POST" action="{{base_url}}management/addbranch" enctype="multipart/form-data">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label for="branchName">Branch Name</label>
                                    <input type="text" class="form-control" name="branchName" id="branchName"/>
                                </div>
                                <div class="col-md-12">
                                    <label for="streetName">Street Name/Unit</label>
                                    <textarea class="form-control" name="streetName" id="streetName" cols="30" rows="5"></textarea>
                                </div>
                                <div class="col-md-12">
                                    <label for="district">District</label>
                                    <input type="text" class="form-control" name="district" id="district"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class='form-group row'>
                                <div class="col-md-12">
                                    <label for="city">City/Province</label>
                                    <input type="text" class="form-control" name="city" id="city"/>
                                </div>
                                <div class="col-md-12">
                                    <label for="zipcode">Zipcode</label>
                                    <input type="text" class="form-control" name="zipcode" id="zipcode"/>
                                </div>
                                <div class="col-md-12 clearfix addBranchLogo">
                                    <label for="zipcode">Branch Logo</label>
                                    <br><span class="error-msg hide">Image does not meet the image size requirement. Please check again.</span>
                                    <br><img src="./assets/images/addImage.jpg" class="imgPrev" alt="branch logo" height="64" width="64" />
                                        <input type="file" name="branch_logo" class="fuImage" ew="200" eh="200" id="branchLogoFile">
                                        <i>Required image size: 200px x 200px</i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button class="btn btn-primary pull-right">ADD</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="editBranch" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Edit Branch</div>
            <div class="modal-body">
                <form method="POST" action="{{base_url}}management/editbranch" enctype="multipart/form-data">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label for="branchName">Branch Name</label>
                                    <input type="text" class="form-control" name="branchName" id="ebranchName"/>
                                </div>
                                <div class="col-md-12">
                                    <label for="eStreetName">Street Name/Unit</label>
                                    <textarea class="form-control" name="streetName" id="eStreetName" cols="30" rows="5"></textarea>
                                </div>
                                <div class="col-md-12">
                                    <label for="eDistrict">District</label>
                                    <input type="text" class="form-control" name="district" id="eDistrict"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class='form-group row'>
                                <div class="col-md-12">
                                    <label for="eCity">City/Province</label>
                                    <input type="text" class="form-control" name="city" id="eCity"/>
                                </div>
                                <div class="col-md-12">
                                    <label for="eZipcode">Zipcode</label>
                                    <input type="text" class="form-control" name="zipcode" id="eZipcode"/>
                                </div>
                                <div class="col-md-12 clearfix editBranchLogo">
                                    <label for="zipcode">Branch Logo</label>
                                    <br><span class="error-msg hide">Image does not meet the image size requirement. Please check again.</span>
                                    <br><img src="./assets/images/addImage.jpg" class="imgPrev" alt="branch logo" height="64" width="64" id="eBranchLogo" />
                                        <input type="file" name="branch_logo" class="fuImage" ew="200" eh="200" style="display:none;" id="eBranchLogoFile">
                                        <input type="button" value='Choose File' onclick="document.getElementById('eBranchLogoFile').click();">
                                        <i>Required image size: 200px x 200px</i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button class="btn btn-primary pull-right">UPDATE</button>
                        </div>
                    </div>
                    <input type="hidden" name="id" id="ebranchId">
                </form>
            </div>
        </div>
    </div>
</div>
<div id="deleteBranch" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Delete Branch</div>
            <div class="modal-body">
                <p>Are you sure you want to delete this branch?</>
                <span id="branchToDelete"></span>
                <form method="get" id="deleteBranchForm">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button class="btn btn-primary pull-right">DELETE</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
    <div id="addEmployee" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content modlogin">
                <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
                <div class="modal-header">Add Employee</div>
                <div class="modal-body">
                    <form method="POST" action="{{base_url}}management/addEmployee" name="postaddEmployee" id="postaddEmployee">
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="employeeName">Employee Name</label>
                                <input type="text" class="form-control" name="employeeName" id="employeeName" />
                            </div>
                            <div class="col-md-8">
                                <label for="selectBranch">Select Branch</label>
                                <select class="form-control" name="selectBranch" id="selectBranch">
                                {% foreach branches as b %}
                                    <option value="{{b[id]}}">{{b[branchName]}}</option>
                                {% endforeach %}
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label for="fourDigit">4 Digit Code <a href="#"><i class="fa fa-question-circle" data-toggle="tooltip" title="4-Digit PIN to grant employee access to merchant app"></i></a></label>
                                <input type="text" class="form-control" name="fourDigit" id="fourDigit" maxlength="4"/>
                            </div>
                            <div class="col-md-12">
                                <label for="requirePassword">Require password upon login?</label>
                                <input type="checkbox" name="requirePassword" id="requirePassword" checked>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                                <button class="btn btn-primary pull-right">ADD</button>
                            </div>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>
    <div id="editEmployee" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content modlogin">
                <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
                <div class="modal-header">Edit Employee</div>
                <div class="modal-body">
                    <form method="POST" action="{{base_url}}management/editEmployee" id="eEmployeeSubmit">
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="eEmployeeName">Employee Name</label>
                                <input type="text" class="form-control" name="employeeName" id="eEmployeeName" />
                            </div>
                            <div class="col-md-8">
                                <label for="eSelectBranch">Select Branch</label>
                                <select class="form-control" name="selectBranch" id="eSelectBranch">
                                {% foreach branches as b %}
                                    <option value="{{b[id]}}">{{b[branchName]}}</option>
                                {% endforeach %}
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label for="eFourDigit">4 Digit Code <a href=""><i class="fa fa-question-circle"></i></a></label>
                                <input type="text" class="form-control" name="fourDigit" id="eFourDigit" maxlength="4"/>
                            </div>
                            <div class="col-md-12">
                                <label for="requirePassword">Require password upon login?</label>
                                <input type="checkbox" name="requirePassword" id="eRequirePassword" checked>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                                <button class="btn btn-primary pull-right">UPDATE</button>
                            </div>
                        </div>
                        <input type="hidden" name="employeeId" id="eEmployeeId">
                    </form>

                </div>

            </div>
        </div>
    </div>
<div id="deleteEmployee" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Delete Employee</div>
            <div class="modal-body">
                <p>Are you sure you want to delete this employee?</>
                <span id="employeeToDelete"></span>
                <form method="get" id="deleteEmployeeForm">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button class="btn btn-primary pull-right">DELETE</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>