
<div id="sidemenu">
    <div class="logoholder">
        <a href=""><img src="{{base_url}}assets/images/rush_logo_2016.png" alt=""></a>
    </div>
    <div id="menu">
        <ul>
            <li class="das"><a href="{{base_url}}dashboard"><i class="icon" data-toggle="tooltip" data-placement="right" title="DASHBOARD"></i><span>Dashboard</span></a></li>
            <li class="pro"><a href="{{base_url}}promos"><i class="icon" data-toggle="tooltip" data-placement="right" title="PROGRAM MECHANICS"></i><span>Program Mechanics</span></a></li>
            <li class="ach"><a href="{{base_url}}achievementunlock"><i class="icon" data-toggle="tooltip" data-placement="right" title="ACHIVEMENT UNLOCK"></i><span>Achievement Unlock</span></a></li>
            <li class="rep"><a href="{{base_url}}redemption"><i class="icon" data-toggle="tooltip" data-placement="right" title="REWARD CATALOGUE"></i><span>Reward Catalogue</span></a></li>            
            <li class="des"><a href="{{base_url}}design#tablet"><i class="icon" data-toggle="tooltip" data-placement="right" title="DESIGN MANAGEMENT"></i><span>Design Management</span></a></li>
            <li class="tra"><a href="{{base_url}}transactionhistory"><i class="icon" data-toggle="tooltip" data-placement="right" title="TRANSACTION HISTORY"></i><span>Transaction History</span></a></li> 
            <li class="man"><a href="{{base_url}}management"><i class="icon" data-toggle="tooltip" data-placement="right" title="STORE & CUSTOMER MANAGEMENT"></i><span>Store & Customer Management</span></a></li>
            <li class="sms"><a href="{{base_url}}sms"><i class="icon" data-toggle="tooltip" data-placement="right" title="SMS TOOL"></i><span>SMS Tool</span></a></li>
            <li class="add"><a href="{{base_url}}addons"><i class="icon" data-toggle="tooltip" data-placement="right" title="ADD-ONS"></i><span>Add-Ons</span></a></li>
            <li class="hel"><a href="{{base_url}}help"><i class="icon" data-toggle="tooltip" data-placement="right" title="HELP"></i><span>Help</span></a></li>
            <li class="set"><a href="{{base_url}}settings"><i class="icon" data-toggle="tooltip" data-placement="right" title="SETTINGS"></i><span>Settings</span></a></li>
        </ul>
    </div>
</div>

