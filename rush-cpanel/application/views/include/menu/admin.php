
<div id="sidemenu">
    <div class="logoholder">
        <a href=""><img src="{{base_url}}assets/images/rush_logo_2016.png" alt=""></a>
    </div>
    <div id="menu">
        <ul>
            <li class="das"><a href="{{base_url}}dashboard"><i class="icon" data-toggle="tooltip" data-placement="right" title="DASHBOARD"></i><span>Dashboard</span></a></li>
            <li class="mer"><a href="{{base_url}}merchant"><i class="icon" data-toggle="tooltip" data-placement="right" title="MERCHANT"></i></i><span>Merchant</span></a></li>
            <li class="usr"><a href="{{base_url}}users"><i class="icon" data-toggle="tooltip" data-placement="right" title="USERS"></i><span>Users</span></a></li>          
            <li class="rep"><a href="{{base_url}}reports"><i class="icon" data-toggle="tooltip" data-placement="right" title="REPORTS"></i><span>Reports</span></a></li>
            <li class="tra"><a href="{{base_url}}transactionhistory"><i class="icon" data-toggle="tooltip" data-placement="right" title="TRANSACTION HISTORY"></i><span>Transaction History</span></a></li>
            <li class="hel"><a href="{{base_url}}help"><i class="icon" data-toggle="tooltip" data-placement="right" title="HELP"></i><span>Help</span></a></li>
        </ul>
    </div>
</div>

