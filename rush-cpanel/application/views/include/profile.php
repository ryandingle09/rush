<div class="howdy">

    <!-- Split button -->
    <div class="btn-group">
        <!-- Large button group -->
        <div class="btn-group">
            <button class="btn btn-default btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img src="assets/images/avatar.png" alt=""/> <span>Hi, {{login_name}}!</span>
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="{{base_url}}account">Change Password</a></li>
                <li><a href="{{base_url}}logout">Logout</a></li>
            </ul>
        </div>


    </div>
</div>
