

<script type="text/javascript" src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
<script type="text/javascript" src="{{base_url}}assets/javascripts/lib/jquery.validate.js"></script>
<script type="text/javascript" src="{{base_url}}assets/bower_components/moment/min/moment.min.js"></script>
<script type="text/javascript" src="{{base_url}}assets/bower_components/bootstrap-sass/assets/javascripts/bootstrap.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.9.1/bootstrap-table.min.js"></script>
<script type="text/javascript" src="{{base_url}}assets/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="//d3js.org/d3.v3.min.js"></script>
<script type="text/javascript" src="{{base_url}}assets/javascripts/lib/jquery.fittext.js"></script>
<script type="text/javascript" src="{{base_url}}assets/javascripts/lib/jquery.activeNavigation.js"></script>
<script type="text/javascript" src="{{base_url}}assets/javascripts/lib/bootstrap-table-filter-control.js"></script>
<script type="text/javascript" src="{{base_url}}assets/javascripts/lib/spectrum.js"></script>
<script type="text/javascript" src="{{base_url}}assets/javascripts/lib/my.class.min.js"></script>
<script type="text/javascript" src="{{base_url}}assets/javascripts/lib/blur.min.js"></script>
<script type="text/javascript" src="{{base_url}}assets/javascripts/lib/jquery.ui.widget.js"></script>
<script type="text/javascript" src="{{base_url}}assets/javascripts/lib/load-image.all.min.js"></script>
<script type="text/javascript" src="{{base_url}}assets/javascripts/lib/jquery.fileupload.js"></script>
<script type="text/javascript" src="{{base_url}}assets/javascripts/lib/jquery.fileupload-process.js"></script>
<script type="text/javascript" src="{{base_url}}assets/javascripts/lib/jquery.fileupload-image.js"></script>
<script type="text/javascript" src="{{base_url}}assets/javascripts/djslo-1.0.0.js" data-base_url="{{base_url}}" data-quicksetup="{{quickstart}}" data-module="{{module}}"></script>

</body>
</html>
