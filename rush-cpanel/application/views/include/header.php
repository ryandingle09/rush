<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="Fidelity" content="">
    <title>CMS - RUSH</title>



    <!-- fonts-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,300,300italic,400italic,600italic,700,700italic' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.9.1/bootstrap-table.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{base_url}}assets/stylesheets/lib/spectrum.css" />
	<link rel="stylesheet" href="{{base_url}}assets/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" href="{{base_url}}assets/stylesheets/lib/jquery.fileupload.css" />
    <!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="{{base_url}}assets/stylesheets/app.css" />

	<!-- DATATABLES -->
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/buttons/1.2.1/css/buttons.dataTables.min.css">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script type="text/javascript" src="{{base_url}}assets/bower_components/jquery/dist/ie10-viewport-bug-workaround.js"></script>

</head>
<body>
