<div class="quickstart loyalty" id="qStart">
    <div class="content clearfix">
        <div class="steps row">
            <!-- <div class="col-md-1-5 col-xs-12"><h3>INITIAL SETUP</h3></div> -->
            <div class="col-md-1-7 col-xs-2 active pc1">
                <span>1</span>
                <div class="right">
                    <p class="t">STEP 1</p>
                    <!-- <p class="d">Design your App</p> -->
                </div>
                <img src="{{base_url}}assets/images/pointer.png" alt="">
            </div>
            
            <div class="col-md-1-7 col-xs-2 pc2">
                <span>2</span>
                <div class="right">
                    <p class="t">STEP 2</p>
                    <!-- <p class="d">&nbsp;</p> -->
                </div>
                <img src="{{base_url}}assets/images/pointer.png" alt="">
            </div>

            <div class="col-md-1-7 col-xs-2 pc3">
                <span>3</span>
                <div class="right">
                    <p class="t">STEP 3</p>
                    <!-- <p class="d">&nbsp;</p> -->
                </div>
                <img src="{{base_url}}assets/images/pointer.png" alt="">
            </div>

            <div class="col-md-1-7 col-xs-2 pc4">
                <span>4</span>
                <div class="right">
                    <p class="t">STEP 4</p>
                    <!-- <p class="d">&nbsp;</p> -->
                </div>
                <img src="{{base_url}}assets/images/pointer.png" alt="">
            </div>                        
                  
            <div class="col-md-1-7 col-xs-2 pc5">
                <span>5</span>
                <div class="right">
                    <p class="t">FINISH</p>
                    <!-- <p class="d">&nbsp;</p> -->
                </div>
                <img src="{{base_url}}assets/images/pointer.png" alt="">
            </div>
        </div><!--steps-->

        <div class="qsWrapper qsOne">
          <form action="{{base_url}}quicksetup/setupProgram" method="POST" enctype="multipart/form-data" id="step1">
          <div class="qsHeader">
              <h2>Name Your Program</h2>
          </div><!--qsheader-->
          <div class="qsBody promos">
              <div class="stampsWrap clearfix">
                  <div class="col-md-12 form-group">
                      <label for="programName" id="programNameLabel">Program Name *</label>
                      <input type="text" class="form-control" name="programName" id="programName" value="{{programName}}" maxlength="128" required>
                  </div>
                  <div class="col-md-12 form-group">
                      <label for="pointsName" id="pointsNameLabel">Points Name *</label>
                      <input type="text" class="form-control" name="pointsName" id="pointsName" value="{{pointsName}}" maxlength="128" required>
                  </div>

              </div>
          </div><!--qsBody-->

          <div class="qsFooter">
              <!--<a href="#" class="skipQuickStart">Skip Quick Start</a>-->
              <button class="btn btn-default nextStep" type="button">NEXT <i class="fa fa-angle-right"></i></button>
          </div><!--qsFooter-->
        </form>
        </div><!--qsWrapper qsOne-->

        <div class="qsWrapper qsTwo">
            <div class="qsHeader">
                <h2>Design Your Merchant Mobile App</h2>
            </div>
            <!--qsheader-->
            <div class="qsBody">
                <div class="design" id="merchantAppDesign">
                    <form action="{{base_url}}quicksetup/saveMerchantAppDesign" method="POST" enctype="multipart/form-data" id="step2">
                        <div class="tabletTab" style="display:block;">
                            <div class="col-md-4">
                                <div class="row left">
                                    <div class="col-xs-12 clearfix tabletUploadLogo">
                                        <h5 class="withIco"><i class="number">1</i>Upload Logo</h5>
                                        <span class="error-msg hide">Image does not meet the image size requirement. Please check again.</span>
                                        <img src="{{merchant_app.logo}}" class="imgPrev" alt="">
                                        <div class="pull-left">
                                            <span class="btn btn-default btn-file">
                                              Choose File <input type="file" name="logo" class="fuImage" ew="200" eh="200">
                                              </span>
                                            <i>Required image size: 200px x 200px</i>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 clearfix tabletAddBackImage">
                                        <h5 class="withIco"><i class="number">2</i>Add Background Image</h5>
                                        <span class="error-msg hide">Image does not meet the image size requirement. Please check again.</span>
                                        <img src="{{merchant_app.background}}" class="imgPrev" alt="">
                                        <div class="pull-left">
                                            <span class="btn btn-default btn-file">
                                              Choose File <input type="file" name="background" class="fuImage" ew="1136" eh="640">
                                              </span>
                                            <i>Required image size: 1136px x 640px</i>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 clearfix tabletChoosePalete">
                                        <h5 class="withIco"><i class="number">3</i>Choose Color Palette</h5>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                {{merchantAppPalette}}
                                                <button class="btn btn-default savePalette" id="saveMerchantAppPalette" type="button">Save Palette</button>
                                            </div>
                                            <div class="customPaletteBox">
                                                <!-- <div class="col-xs-4">
                                                      <i>Theme</i>
                                                      <input type='hidden' id="tpOverlay" name="overlayColor" value="{{merchant_app.overlayColor}}"/>
                                                      <span class="hex" id="mThemeVal">#00A8A4</span>
                                                  </div> -->
                                                <div class="col-xs-4">
                                                    <i>Text</i>
                                                    <input type='hidden' id="tpText" name="textColor" value="{{merchant_app.textColor}}" />
                                                    <span class="hex" id="mTextVal">#00E6C5</span>
                                                </div>
                                                <div class="col-xs-4">
                                                    <i>Buttons</i>
                                                    <input type='hidden' id="tpButtons" name="buttonsColor" value="{{merchant_app.buttonsColor}}" />
                                                    <span class="hex" id="mButtonsVal">#8BFF6F</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 right">
                                <div class="tabletFrame">
                                    <div class="tabletLayout">
                                        <div class="customTablet">
                                            <img src="assets/images/fidelityLogo.png" alt="" class="tabletCustomLogo">
                                            <button class="btn btn-default customButton">Login</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!--qsBody-->
            <div class="qsFooter">
                <button class="btn btn-default backStep" type="button"><i class="fa fa-angle-left"></i>BACK</button>
                <button class="btn btn-default nextStep" type="button">NEXT <i class="fa fa-angle-right"></i></button>
            </div>
        </div>

        <div class="qsWrapper qsThree">
            <div class="qsHeader">
                <h2>Design Your Customer Mobile App</h2>
            </div>
            <div class="qsBody">
                <div class="design" id="mobileAppDesign">
                    <form action="{{base_url}}quicksetup/saveCustomerAppDesign" method="POST" enctype="multipart/form-data" id="step3">
                        <div class="col-md-4">
                            <div class="row left">
                                <div class="col-xs-12 clearfix uploadLogo">
                                    <h5 class="withIco"><i class="number">2</i>Upload Logo</h5>
                                    <span class="error-msg hide">Image does not meet the image size requirement. Please check again.</span>
                                    <img src="{{customer_app.logo}}" class="imgPrev" alt="">
                                    <div class="pull-left">
                                        <span class="btn btn-default btn-file">
                                            Choose File <input type="file" name="logo" class="fuImage" ew="200" eh="200">
                                        </span>
                                        <i>Required image size: 200px x 200px</i>
                                    </div>
                                </div>
                                <div class="col-xs-12 clearfix addBackImage">
                                    <h5 class="withIco"><i class="number">3</i>Add Background Image</h5>
                                    <span class="error-msg hide">Image does not meet the image size requirement. Please check again.</span>
                                    <img src="{{customer_app.background}}" class="imgPrev" alt="">
                                    <div class="pull-left">
                                        <span class="btn btn-default btn-file">
                                            Choose File <input type="file" name="background" class="fuImage" ew="640" eh="1136">
                                        </span>
                                        <i>Required image size: 640px x 1136px</i>
                                    </div>
                                </div>
                                    <div class="col-xs-12 clearfix choosePalete">
                                        <h5 class="withIco"><i class="number">4</i>Choose Color Palette</h5>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                {{customerAppPalette}}
                                                <button class="btn btn-default savePalette" id="saveCustomerAppPalette" type="button">Save Palette</button>
                                            </div>
                                            <div class="customPaletteBox">
                                                <div class="col-xs-4">
                                                    <i>Theme</i>
                                                    <input type='hidden' id="pOverlay" name="overlayColor" value="{{customer_app.overlayColor}}" />
                                                    <span class="hex" id="cThemeVal">#00A8A4</span>
                                                </div>
                                                <div class="col-xs-4">
                                                    <i>Text</i>
                                                    <input type='hidden' id="pText" name="textColor" value="{{customer_app.textColor}}" />
                                                    <span class="hex" id="cTextVal">#00E6C5</span>
                                                </div>
                                                <div class="col-xs-4">
                                                    <i>Buttons</i>
                                                    <input type='hidden' id="pButtons" name="buttonsColor" value="{{customer_app.buttonsColor}}" />
                                                    <span class="hex" id="cButtonsVal">#8BFF6F</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 right">
                                <div class="mobileFrame">
                                    <div class="col-xs-6">
                                        <div class="screen1">
                                            <div class="customMobileBgImg">
                                                <div class="customMobileBgColor">
                                                    <div class="inside"><img src="assets/images/fidelityLogo.png" alt="" class="customLogo"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="screen2">
                                            <div class="screen2box">
                                                <div class="topPane"><span id="topPaneLabel">{{programName}}</span></div>
                                                <div class="qrcodeWrap">
                                                </div>
                                                <div>
                                                    <span class="pts_value">1286<span class="pts"> PTS</span></span>
                                                    <p class="pts_date">As of Nov 26, 2015</p>
                                                </div>
                                                <button class="redeem btn" type="button">REDEEM</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </form>
                </div>
                <div class="qsFooter">
                    <button class="btn btn-default backStep" type="button"><i class="fa fa-angle-left"></i>BACK</button>
                    <button class="btn btn-default nextStep" type="button">NEXT <i class="fa fa-angle-right"></i></button>
                </div>
            </div>
        </div>

        <div class="qsWrapper qsFour">
          <form action="{{base_url}}quicksetup/saveApplicationDetails" method="POST" enctype="multipart/form-data" id="step4">
            <div class="qsHeader">
                <h2>Provide Your App Details</h2>
            </div><!--qsheader-->
            <div class="qsBody">
              <div class="stampsWrap clearfix step5 design">
                  <div class="col-md-12 form-group">
                      <label for="programName" id="appNameLabel">App Name *</label>
                      <input type="text" class="form-control" name="appName" id="appName" value="{{appName}}">
                  </div>
                  <div class="col-md-12 form-group">
                      <label for="pointsName" id="appDescriptionLabel">Description *</label>
                      <input type="text" class="form-control" name="appDescription" id="appDescription" value="{{appDescription}}">
                  </div>
                  <div class="col-md-12 form-group">
                      <label for="pointsName" class="appStoreLogoLabel">Appstore Logo *</label>
                      <div class="appstoreLogo">
                      <span class="error-msg hide">Image does not meet the image size requirement. Please check again.</span>
                          <img src="{{appLogo}}" class="imgPrev" alt="">
                          <span class="btn btn-default btn-file">
                          Choose File <input type="file" name="logo" class="fuImage" ew="1024" eh="1024">
                          </span>
                          <i>Required Size: 1024 x 1024</i>
                      </div>
                  </div>
              </div>
            </div><!--qsBody-->

            <div class="qsFooter">
              <button class="btn btn-default backStep" type="button"><i class="fa fa-angle-left"></i>BACK</button>
              <button class="btn btn-default nextStep" type="button">NEXT <i class="fa fa-angle-right"></i></button>
            </div><!--qsFooter-->
          </form>
        </div><!--qsWrapper-->
         
        <div class="qsWrapper qsFinish">
          <form>
            <div class="qsHeader">
                <h2>Finish Setup</h2>
            </div><!--qsheader-->
            <div class="qsBody">
                 <h4>Your loyalty program will be ready in 3 to 5 business days. We will send you an update via email.</h4>
                <button class="btn btn-default skipQuickStart" type="button">Go to Dashboard  <i class="fa fa-angle-right"></i></button>
<!--                 <button class="btn btn-default nextStep" onclick="location.href='account'" type="button">Change default password  <i class="fa fa-angle-right"></i></button> -->
            </div><!--qsBody-->

            <div class="qsFooter">

            </div><!--qsFooter-->
          </form>
        </div><!--qsWrapper-->

    </div><!--content-->

</div><!--quickstart-->
