<div id="rightcontent">
    <div class="header">
        <div class="row">
            <div class="col-xs-7">
                <h3>Settings</h3>
            </div>
            <div class="col-xs-5">
                <div class="howdy">

                    <!-- Split button -->
                    <div class="btn-group">
                        <!-- Large button group -->
                        <div class="btn-group">
                            <button class="btn btn-default btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="{{base_url}}assets/images/avatar.png" alt=""/> <span>Hi, {{login_name}}!</span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="{{base_url}}account">Change Password</a></li>
                                <li><a href="{{base_url}}logout">Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div id="content" class="promos spacing settings">
            {{ message }}
            <div class="noTab">
                <form action="{{base_url}}settings/saveAppStoreLogo" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Program Name</h4>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label for="programName">Program Name</label>
                                <input type="text" class="form-control" name="programName" id="programName" value="{{programName}}" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label for="pointsName">{{ stampPointsName }} Name</label>
                                <input type="text" class="form-control" name="pointsName" id="pointsName" value="{{pointsName}}" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4>App Details</h4>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label for="programName">App Name</label>
                                <input type="text" class="form-control" name="appName" id="appName" value="{{appName}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label for="pointsName">Description</label>
                                <input type="text" class="form-control" name="appDescription" id="appDescription" value="{{appDescription}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10 form-group">
                                <label for="pointsName">Appstore Logo</label>
                                <div class="appstoreLogo">
                                    <span id="appLogoMsg" class="error-msg hide">Image does not meet the image size requirement. Please check again.<br></span>
                                    <img src="{{logo}}" class="imgPrev" alt="">
                                    <span class="btn btn-default btn-file">
                                    Choose File <input type="file" name="logo" class="fuImage" ew="1024" eh="1024" value="Choose File">
                                    </span>
                                    <input class="btn btn-default btn-file" type="submit" value="Submit">
                                    <br><i>Required Size: 1024px x 1024px</i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div> <!-- /container -->
</div> <!--rightcontent-->
