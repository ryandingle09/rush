<div id="rightcontent">
        <div class="header">
            <div class="row">
                <div class="col-xs-7">
                    <h3>Design Your Loyalty Card</h3>
                </div>
                <div class="col-xs-5">
                    <div class="howdy">

                        <!-- Split button -->
                        <div class="btn-group">
                            <!-- Large button group -->
                            <div class="btn-group">
                                <button class="btn btn-default btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img src="{{base_url}}assets/images/avatar.png" alt=""/> <span>Hi, {{login_name}}!</span>
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                  <li><a href="{{base_url}}account">Change Password</a></li>
                                  <li><a href="{{base_url}}logout">Logout</a></li>
                                </ul>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div class="container">
        <div class="row">

            <div class="col-md-4 forAddBtn pull-right">
                <!-- <a href="#addTablet" data-toggle="modal" class="btn btn-default" onclick="return false"><i class="fa fa-plus"></i> Add Tablet</a> -->
            </div>
        </div>

		<div id="content" class="design spacing">
                    <form action="{{base_url}}design/saveCardDesign" method="POST" enctype="multipart/form-data">
                    <div class="cardTab">
                        <div class="col-md-4">
                            <div class="row left">
                                <div class="col-xs-12 clearfix cardUploadLogo">
                                    <h5 class="withIco"><i class="number">1</i>Upload Logo</h5>
                                    <span class="error-msg hide">Image does not meet the image size requirement. Please check again.</br></span>
                                    <img src="{{card_design.logo}}" class="imgPrev" alt="">
                                    <div class="pull-left">
                                    <span class="btn btn-default btn-file">
                                    Choose File <input type="file" name="logo" class="fuImage" ew="200" eh="200">
                                    </span>
                                        <i>Required image size: 200px x 200px</i>
                                    </div>
                                </div>
                                <div class="col-xs-12 clearfix cardAddBackImage">
                                    <h5 class="withIco"><i class="number">2</i>Add Background Image</h5>
                                    <span class="error-msg hide">Image does not meet the image size requirement. Please check again.</br></span>
                                    <img src="{{card_design.background}}" class="imgPrev" alt="">
                                    <div class="pull-left">
                                    <span class="btn btn-default btn-file">
                                    Choose File <input type="file" name="background" class="fuImage" ew="500" eh="200">
                                    </span>
                                        <i>Required image size: 500px x 200px</i>
                                    </div>
                                </div>
                                <div class="col-xs-12 clearfix cardAddBackColor">
                                    <h5 class="withIco"><i class="number">3</i>Choose Background Color</h5>
                                    <div class="hexwrap">
                                        <input type='text' id="cardBgColor" name="cardbgcolor" value="{{card_design.cardbgcolor}}"/>
                                        <span class="hex" id="cardBackgroundColor">#00A8A4</span>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <button class="btn btn-primary saveDesign">Save Design</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 right">
                            <div class="cardFrame">
                                <div class="cardWrap">
                                    <div class="overlay">
                                        <img src="{{base_url}}assets/images/cardLogo.png" class="cardLogo" alt="">
                                        <img src="{{base_url}}assets/images/qr.png" alt="" class="qr">
                                    </div>
                                </div><!--cardWrap-->
                            </div><!--cardFrame-->
                        </div><!--right-->
                    </div><!--cardTab-->
                    </form>
<!-- 			<div div class="noTab">
                    <table id="tabletAddons" data-toggle="table" class="data-table management " data-pagination="true" data-search="true">
                        <thead>
                        <tr>
                            <th>Tablet ID</th>
                            <th data-sortable="true">Tablet Brand</th>
                            <th>Branch Assigned to </th>
                        </tr>
                        </thead>
                        <tbody>
						{% foreach addons as addon %}
                        <tr>
                            <td>{{addon[tablet_id]}}</td>
                            <td>{{addon[tablet_brand]}}</td>
                            <td>{{addon[branch_assigned]}}</td>
                        </tr>
						{% endforeach %}
                        </tbody>
                    </table>
                </div>
            </div>-->
         </div>
    </div> <!-- /container -->
</div> <!--rightcontent-->


<!--  MODALS -->
<div id="addTablet" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Add Tablet</div>
            <div class="modal-body">
                <form method="POST" action="<?= base_url('Addons/addTablet');?>">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="tabletID">Tablet ID (IMEI)</label>
                            <input type="text" class="form-control" name="tabletID" id="tabletID">
                        </div>
                        <div class="col-md-12">
                            <label for="tabletBrand">Tablet Brand</label>
                            <select name="tabletBrand" id="tabletBrand" class="form-control">
                                <option value="samsung">Samsung</option>
                                <option value="apple">Apple</option>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label for="branchAssigned">Branch Assigned to</label>
                            <select name="branchAssigned" id="branchAssigned" class="form-control">
                                <option value="megamall">Megamall</option>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button class="btn btn-primary pull-right">ADD</button>
                        </div>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>
