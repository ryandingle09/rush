<?php for ($i = 1; $i <= $n; $i++): ?>
<li>
    <a href="#stampModal<?php echo $i; ?>" data-toggle="modal" data-target="#stampModal<?php echo $i; ?>"><?php echo $i; ?></a>
    <div class="modal fade" id="stampModal<?php echo $i; ?>" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="stampModal<?php echo $i; ?>Label">STAMP <?php echo $i; ?></h4>
                </div>
                <div class="modal-body">
                    <div class='alert alert-danger hide'>
                        <p>Image does not meet the image size requirement. Please check again.</p>
                    </div>
                    <div class="form-group">
                        <span class="btn btn-success fileinput-button">
                            <span>Click to add/change image <br /> <i>Required size is 350px x 200px</i></span>
                            <input id="stamp_reward_image_<?php echo $i; ?>" type="file" name="stamp_reward_image_<?php echo $i; ?>">
                        </span>
                    </div>
                    <div class="form-group">
                        <label>Reward Description</label>
                        <textarea class="stamp_reward_description_<?php echo $i; ?> form-control" name="stamp_reward_description_<?php echo $i; ?>" maxlength="75"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-6">
                            <button type="button" class="btn btn-default clearStamp">Clear</button>
                        </div>
                        <div class="col-md-6">
                            <button type="button" class="btn btn-primary saveStamp" >Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</li>
<?php endfor ?>