    <div id="rightcontent">
        <div class="header">
            <div class="row">
                <div class="col-xs-7">
                    <h3>Design</h3>
                </div>
                <div class="col-xs-5">
                    <div class="howdy">
                        <!-- Split button -->
                        <div class="btn-group">
                            <!-- Large button group -->
                            <div class="btn-group">
                                <button class="btn btn-default btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img src="{{base_url}}assets/images/avatar.png" alt=""/> <span>Hi, {{login_name}}!</span>
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                  <li><a href="{{base_url}}account">Change Password</a></li>
                                  <li><a href="{{base_url}}logout">Logout</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div id="content" class="design spacing">
                <div class="row" id="merchantAppDesign">
                    <div class="mobileTablet col-md-12 clearfix">
                        <button class="btn btn-default" id="btnTablet">Merchant App</button>
                        <button class="btn btn-primary" id="btnMobile">Customer App</button>
                        <button class="btn btn-primary active" id="btnStampIcon">Stamp Icon</button>
                    </div>
                    <form action="{{base_url}}design/saveStampIcon" method="POST" enctype="multipart/form-data">
                        <div class="stampIconTab">
                            <div class="col-xs-12 clearfix upStampC">
                                <h5 class="withIco">Upload Stamp Design</h5>
                                <span class="sp">Colored</span>
                                <span id="appLogoMsg" class="error-msg hide">Image does not meet the image size requirement. Please check again.<br></span>
                                <img src="{{merchant_app.stamp}}" class="imgPrev" alt="">
                                <div class="pull-left">
                                <span class="btn btn-default btn-file">
                                Choose File <input type="file" name="stamp" class="fuImage" ew="200" eh="200">
                                </span>
                                    <i>Required Size: 200px x 200px</i>
                                </div>
                            </div>
                            <div class="col-xs-12 clearfix upStampG">
                                <span class="sp">Grayscale</span>
                                <img src="{{merchant_app.stamp}}" class="imgPrev stamp-grayscale" alt="">
                            </div>
                            <div class="col-xs-12">
                                <button class="btn btn-primary saveDesign">Save Design</button>
                            </div>
                        </div>
					</form>
                    <form action="{{base_url}}design/saveCustomerAppDesign" method="POST" enctype="multipart/form-data">
                        <div class="mobileTab" id="mobileAppDesign">
                            <div class="col-md-4">
                                <div class="row left">
                                    <div class="col-xs-12 clearfix merchName">
                                        <h5 class="withIco"><i class="number">1</i>Merchant Name</h5>
                                        <input type="text" class="form-control" name="merchantName" id="merchantName" value="{{customer_app.name}}">
                                    </div>
                                    <div class="col-xs-12 clearfix uploadLogo">
                                        <h5 class="withIco"><i class="number">2</i>Upload Logo</h5>
                                        <span class="error-msg hide">Image does not meet the image size requirement. Please check again.<br></span>
                                        <img src="{{customer_app.logo}}" class="imgPrev" alt="">
                                        <div class="pull-left">
                                            <span class="btn btn-default btn-file">
                                            Choose File <input type="file" name="logo" class="fuImage" ew="200" eh="200">
                                            </span>
                                                <i>Required image size: 200px x 200px</i>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 clearfix addBackImage">
                                        <h5 class="withIco"><i class="number">3</i>Add Background Image</h5>
                                        <span class="error-msg hide">Image does not meet the image size requirement. Please check again.<br></span>
                                        <img src="{{customer_app.background}}" class="imgPrev" alt="">
                                        <div class="pull-left">
                                            <span class="btn btn-default btn-file">
                                            Choose File <input type="file" name="background" class="fuImage" ew="640" eh="1136">
                                            </span>
                                                <i>Required image size: 640px x 1136px</i>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 clearfix choosePalete">
                                        <h5 class="withIco"><i class="number">4</i>Choose Color Palette</h5>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                {{customerAppPalette}}
                                                <button class="btn btn-default savePalette" id="saveCustomerAppPalette" type="button">Save Palette</button>
                                            </div>
                                            <div class="customPaletteBox">
                                                <div class="col-xs-6">
                                                    <i>Theme</i>
                                                    <input type='hidden' id="pOverlay" name="overlayColor" value="{{customer_app.overlayColor}}"/>
                                                    <span class="hex" id="cThemeVal">#00A8A4</span>
                                                </div>
                                                <div class="col-xs-6">
                                                    <i>Text</i>
                                                    <input type='hidden' id="pText" name="textColor" value="{{customer_app.textColor}}"/>
                                                    <span class="hex" id="cTextVal">#00E6C5</span>
                                                </div>
                                                <div class="col-xs-6">
                                                    <i>Buttons</i>
                                                    <input type='hidden' id="pButtons" name="buttonsColor" value="{{customer_app.buttonsColor}}"/>
                                                    <span class="hex" id="cButtonsVal">#8BFF6F</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <button class="btn btn-primary saveDesign">Save Design</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 right">
                                <div class="mobileFrame">
                                    <div class="col-xs-6">
                                        <div class="screen1">
                                            <div class="customMobileBgImg">
                                                <div class="customMobileBgColor">
                                                    <div class="inside"><img src="assets/images/fidelityLogo.png" alt="" class="customLogo"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="screen2">
                                            <div class="screen2box">
                                                <div class="topPane"><span id="topPaneLabel"></span></div>
                                                <div class="qrcodeWrap">
                                                </div><!--qrcodeWrap-->
                                                <div>
                                                    <span class="pts_value">1286<span class="pts"> PTS</span></span>
                                                    <p class="pts_date">As of Nov 26, 2015</p>
                                                </div>
                                                <button class="redeem btn" type="button">REDEEM</button>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--mobileFrame-->
                            </div><!--right-->
                        </div><!--mobileTab-->
                    </form>
					<form action="{{base_url}}design/saveMerchantAppDesign" method="POST" enctype="multipart/form-data">
                        <div class="tabletTab">
                            <div class="col-md-4">
                                <div class="row left">
                                    <div class="col-xs-12 clearfix tabletUploadLogo">
                                        <h5 class="withIco"><i class="number">1</i>Upload Logo</h5>
                                        <div class="row">
                                            <span id="appLogoMsg" class="error-msg hide">Image does not meet the image size requirement. Please check again.<br></span>
                                            <div class="col-md-4">
                                                <img src="{{merchant_app.logo}}" class="imgPrev" alt="">
                                            </div>
                                            <div class="col-md-8">
                                                <span class="btn btn-default btn-file">
                                                Choose File <input type="file" name="logo" class="fuImage" ew="200" eh="200">
                                                </span>
                                                <i>Required Size: 200px x 200px</i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 clearfix tabletAddBackImage">
                                        <h5 class="withIco"><i class="number">2</i>Add Background Image</h5>
                                        <div class="row">
                                            <span id="appLogoMsg" class="error-msg hide">Image does not meet the image size requirement. Please check again.<br></span>
                                            <div class="col-md-4">
                                                <img src="{{merchant_app.background}}" class="imgPrev" alt="">
                                            </div>
                                            <div class="col-md-8">
                                                <span class="btn btn-default btn-file">
                                                    Choose File <input type="file" name="background" class="fuImage" ew="1136" eh="640">
                                                </span>
                                                <i>Required Size: 1136px x 640px</i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 clearfix tabletChoosePalete">
                                        <h5 class="withIco"><i class="number">3</i>Choose Color Palette</h5>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                {{merchantAppPalette}}
                                                <button class="btn btn-default savePalette" id="saveMerchantAppPalette" type="button">Save Palette</button>
                                            </div>
                                            <div class="customPaletteBox">
                                                <div class="col-xs-6">
                                                    <i>Text</i>
                                                    <input type='text' id="tpText" name="textColor" value="{{merchant_app.textColor}}"/>
                                                    <span class="hex" id="mTextVal">#00E6C5</span>
                                                </div>
                                                <div class="col-xs-6">
                                                    <i>Buttons</i>
                                                    <input type='text' id="tpButtons" name="buttonsColor" value="{{merchant_app.buttonsColor}}"/>
                                                    <span class="hex" id="mButtonsVal">#8BFF6F</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <button class="btn btn-primary saveDesign">Save Design</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 right">
                                <div class="tabletFrame">
                                    <div class="tabletLayout">
                                        <div class="customTablet">
                                            <img src="assets/images/fidelityLogo.png" alt="" class="tabletCustomLogo">
                                            <button class="btn btn-default customButton">Login</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
					</form>
                </div><!--row-->
            </div><!--/content-->
        </div> <!-- /container -->
    </div> <!--rightcontent-->