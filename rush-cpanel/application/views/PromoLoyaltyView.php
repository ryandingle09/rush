<div id="rightcontent">
    <div class="header">
        <div class="row">
            <div class="col-xs-7">
                <h3>Program Mechanics</h3>
            </div>
            <div class="col-xs-5">
                <div class="howdy">

                    <!-- Split button -->
                    <div class="btn-group">
                        <!-- Large button group -->
                        <div class="btn-group">
                            <button class="btn btn-default btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="{{base_url}}assets/images/avatar.png" alt=""/> <span>Hi, {{login_name}}!</span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                              <li><a href="{{base_url}}account">Change Password</a></li>
                              <li><a href="{{base_url}}logout">Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="container">
        <form action="{{base_url}}promos/saveLoyaltyPromoMechanics" method="post">
        <div id="content" class="promos spacing setupPunch">
            <div class="noTab">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Setup Your Points Mechanics</h4>
                    </div>
                    <div class="col-md-12">
                        <span>Earning Conversion</span>
                    </div>
                    <div class="col-xs-6 noPadRight">
                        <div class="input-group">
                            <span class="input-group-addon">Php</span>
                            <input type="text" class="form-control" id="earningPointsAmount" name="earningPointsAmount" value="{{earningPointsAmount}}">
                            <span class="input-group-addon">= </span>
                        </div>
                    </div>
                    <div class="col-xs-6 noPadLeft">
                        <div class="input-group">
                            <select name="earningPoints" class="form-control" id="earningPoints" aria-describedby="basic-addon2">
                                {% foreach earningPoints as earningPoint %}
                                <option value="{{earningPoint[value]}}" {{earningPoint[selected]}}>{{earningPoint[value]}}</option>
                                {% endforeach %}
                            </select>
                            <span class="input-group-addon" id="basic-addon2">Point</span>
                        </div>
                    </div>
                </div>                
                <div class="row">
                    <div class="col-md-12">
                        <span>Redemption Conversion</span>
                    </div>
                    <div class="col-xs-6 noPadRight">
                        <div class="input-group">
                            <span class="input-group-addon">Point</span>
                            <select name="redemptionPoints" class="form-control" id="redemptionPoints" aria-describedby="basic-addon2">
                                {% foreach redemptionPoints as redemptionPoint %}
                                <option value="{{redemptionPoint[value]}}" {{redemptionPoint[selected]}}>{{redemptionPoint[value]}}</option>
                                {% endforeach %}
                            </select>
                            <span class="input-group-addon">= </span>
                        </div>
                    </div>
                    <div class="col-xs-6 noPadLeft">
                        <div class="input-group">
                            <input type="text" class="form-control" id="redemptionPointsAmount" name="redemptionPointsAmount" value="{{redemptionPointsAmount}}">
                            <span class="input-group-addon" id="basic-addon2">Php</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 actionButton">
                        <button class="btn btn-default resetButton">Reset</button>
                        <button class="btn btn-primary saveButton">Save Card</button>
                    </div>
                </div>
            </div><!--noTab-->
        </div><!--/content-->
    </form>
    </div> <!-- /container -->
</div> <!--rightcontent-->