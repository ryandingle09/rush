<?php

class Migration_display_decimal_value_of_pointsearned_of_customertransaction_table extends CI_Migration {
    /**
     * up
     */
    public function up() {
        $this->db->query(
        "   ALTER TABLE CustomerTransaction
            MODIFY COLUMN pointsEarned DECIMAL(10,2) DEFAULT '0'");

        echo 'done - 20160617151619_display_decimal_value_of_pointsearned_of_customertransaction_table.php';
        echo PHP_EOL;
    }

    /**
     * rollback
     */
    public function down() {
        $this->db->query(
        "   ALTER TABLE CustomerTransaction
            MODIFY COLUMN pointsEarned int(11) DEFAULT '0'");
    }

}