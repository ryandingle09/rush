<?php

class Migration_add_registrationchannel_to_customertable extends CI_Migration {
    /**
     * up
     */
    public function up() {
        $this->db->query(
        "   ALTER TABLE Customer
            ADD COLUMN registrationChannel varchar(255) DEFAULT NULL");

        $this->db->query(
        "   UPDATE Customer
            SET registrationChannel = 'Merchant App' ");

        echo 'done - 20160523154122_add_registrationchannel_to_customertable.php';
        echo PHP_EOL;
    }

    /**
     * rollback
     */
    public function down() {
    }

}