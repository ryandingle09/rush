<?php

class Migration_add_transaction_type_column_to_sms_log extends CI_Migration
{
    /**
     * up
     */
    public function up()
    {
        // do stuff

$this->load->dbforge();
        $fields = array(
          'transaction_type' => array(
                    'type' => 'VARCHAR',
                    'constraint'  => 255
               )
        );
        $this->dbforge->add_column('notification_sms_logs', $fields);
        echo 'done - 20160613025710_add_transaction_type_column_to_sms_log.php';
        echo PHP_EOL;
    }

    /**
     * rollback
     */
    public function down()
    {
    }
}
