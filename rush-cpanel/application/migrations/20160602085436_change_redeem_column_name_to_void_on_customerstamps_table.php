<?php

class Migration_change_redeem_column_name_to_void_on_customerstamps_table extends CI_Migration {
    /**
     * up
     */
    public function up() {
        // do stuff
        $this->load->dbforge();
        $fields = array(
          'redeem'  => array(
               'name' => 'void',
               'type' => 'INT'
          )
        );
        $this->dbforge->modify_column('CustomerStamps', $fields);
        echo 'done - 20160602085436_change_redeem_column_name_to_void_on_customerstamps_table.php';
        echo PHP_EOL;
    }

    /**
     * rollback
     */
    public function down() {
    }

}
