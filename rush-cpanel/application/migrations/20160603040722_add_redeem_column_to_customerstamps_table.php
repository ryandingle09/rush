<?php

class Migration_add_redeem_column_to_customerstamps_table extends CI_Migration {
    /**
     * up
     */
    public function up() {
        // do stuff
        $this->load->dbforge();
        $fields = array(
          'redeem' => array(
                    'type' => 'INT',
                    'default' => 0
               )
        );
        $this->dbforge->add_column('CustomerStamps', $fields);
        echo 'done - 20160603040722_add_redeem_column_to_customerstamps_table.php';
        echo PHP_EOL;
    }

    /**
     * rollback
     */
    public function down() {
    }

}
