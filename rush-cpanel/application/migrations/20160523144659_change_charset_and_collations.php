<?php

class Migration_change_charset_and_collations extends CI_Migration {
    /**
     * up
     */
    public function up() {
        // alter the database
        $sql =
        "   ALTER DATABASE kollab CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE AchievementsUnlock CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE Addons CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE AlertMessages CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE Branches CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE BusinessType CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE Cities CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE ConversionSettings CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE Customer CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE CustomerCodes CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE CustomerPunchCard CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE CustomerPunchCardReward CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE CustomerPunchCardTransactions CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE CustomerSession CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE CustomerStamps CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE CustomerTransaction CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE Device CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE ItemsForRedeem CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE Merchant CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE MerchantConversionSettings CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE MerchantMechanicsRewards CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE MerchantPackage CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE MerchantPromoAmountStamp CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE MerchantPromoMechanics CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE MerchantPromoMechanicsStamps CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE MerchantPunchCard CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE MerchantPunchCardRewards CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE MerchantSession CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE MerchantSettings CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE Package CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE PackageAddons CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE ProgramObjective CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE QRCode CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE Quicksetup CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE QuicksetupProgress CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE Redeem CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE Roles CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE Users CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE admin_user CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE appSettings CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE customerPoints CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ALTER TABLE redeemedItems CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
        ";
        $sqls = explode(';', $sql);
        array_pop($sqls);

        foreach ($sqls as $statement) {
            $statment = $statement . ";";
            $this->db->query($statement);
        }

        echo 'done - 20160523144659_change_charset_and_collations.php';
        echo PHP_EOL;
    }

    /**
     * rollback
     */
    public function down() {
        // do nothing
    }

}