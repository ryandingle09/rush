<?php

class Migration_add_branch_id_in_customer_table extends CI_Migration {
    /**
     * up
     */
    public function up() {
        $this->db->query(
        "   ALTER TABLE Customer
            ADD COLUMN branch_id int(11) DEFAULT NULL");

        echo 'done - 20160530032311_add_branch_id_in_customer_table.php';
        echo PHP_EOL;
    }

    /**
     * rollback
     */
    public function down() {
        $this->db->query(
        "   ALTER TABLE Customer DROP COLUMN branch_id");
    }

}