<?php

class Migration_add_column_stamp_card_id_to_customerpunchcardreward_table extends CI_Migration {
    /**
     * up
     */
    public function up() {
        // do stuff
        $this->load->dbforge();
        $fields = array(
           'stamp_card_id' => array('type' => 'INT')
        );
        $this->dbforge->add_column('CustomerPunchCardReward', $fields);
        echo 'done - 20160602090200_add_column_stamp_card_id_to_customerpunchcardreward_table.php';
        echo PHP_EOL;
    }

    /**
     * rollback
     */
    public function down() {
    }

}
