<?php

class Migration_add_notification_sms_credentials_table extends CI_Migration {
    /**
     * up
     */
    public function up() {
        // do stuff
        $this->load->dbforge();
        $this->dbforge->add_key('id', TRUE);
        $fields = array(
                    'id'                => array(
                       'type'           => 'INT',
                       'constraint'     => 5,
                       'unsigned'       => TRUE,
                       'auto_increment' => TRUE
                    ),
                    'merchant_id'       => array(
                        'type'          => 'INT',
                        'unsigned'      => TRUE
                    ),
                    'shortcode'         => array(
                          'type'        => 'VARCHAR',
                          'constraint'  => 100
                    ),
                    'app_id'            => array(
                        'type'          => 'VARCHAR',
                        'constraint'    => '255'
                    ),
                    'app_secret'        => array(
                        'type'          => 'VARCHAR',
                        'constraint'    => '255'
                    )
                  );
        $this->dbforge->add_field($fields);
        $this->dbforge->create_table('notifcation_sms_credentials');
        echo 'done - 20160606033838_add_notification_sms_credentials_table.php';
        echo PHP_EOL;
    }

    /**
     * rollback
     */
    public function down() {
    }

}
