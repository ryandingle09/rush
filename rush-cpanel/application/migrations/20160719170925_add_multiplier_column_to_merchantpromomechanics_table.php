<?php

class Migration_add_multiplier_column_to_merchantpromomechanics_table extends CI_Migration {
    /**
     * up
     */
    public function up() {
        $this->db->query(
        "   ALTER TABLE MerchantPromoMechanics
            ADD COLUMN multiplier int(11) DEFAULT 3");

        $this->db->query(
        "   UPDATE MerchantPromoMechanics
            SET multiplier=3");

        echo 'done - 20160719170925_add_multiplier_column_to_merchantpromomechanics_table.php';
        echo PHP_EOL;
    }

    /**
     * rollback
     */
    public function down() {
        $this->db->query("ALTER TABLE MerchantPromoMechanics DROP COLUMN multiplier");
    }

}