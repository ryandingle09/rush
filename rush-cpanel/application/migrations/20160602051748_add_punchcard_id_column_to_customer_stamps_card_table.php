<?php

class Migration_add_punchcard_id_column_to_customer_stamps_card_table extends CI_Migration {
    /**
     * up
     */
    public function up() {
        // do stuff
        $this->db->query(
        "   ALTER TABLE CustomerStampsCard
            ADD COLUMN punchcard_id int(11) DEFAULT NULL");

        echo 'done - 20160602051748_add_punchcard_id_column_to_customer_stamps_card_table.php';
        echo PHP_EOL;
    }

    /**
     * rollback
     */
    public function down() {
    }

}
