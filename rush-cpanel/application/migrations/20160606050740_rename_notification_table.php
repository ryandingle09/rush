<?php

class Migration_rename_notification_table extends CI_Migration
{
    /**
     * up
     */
    public function up()
    {
        // do stuff
        $this->load->dbforge();
        $this->dbforge->rename_table('notifcation_sms_credentials', 'notification_sms_credentials');
        echo 'done - 20160606050740_rename_notification_table.php';
        echo PHP_EOL;
    }

    /**
     * rollback
     */
    public function down()
    {
    }
}
