<?php

class Migration_add_promo_id_column_to_customerpunchcardreward_table extends CI_Migration {
    /**
     * up
     */
    public function up() {
        // do stuff
        $this->db->query(
        "   ALTER TABLE CustomerPunchCardReward
            ADD COLUMN promo_id INT(11) DEFAULT 0");

        echo 'done - 2016053003voivoidd1052_add_promo_id_column_to_customerpunchcardreward_table.php';
        echo PHP_EOL;
    }

    /**
     * rollback
     */
    public function down() {
    }

}
