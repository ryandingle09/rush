<?php

class Migration_add_stamp_card_id_column_to_customerpunchcardtransactions_table extends CI_Migration {
    /**
     * up
     */
    public function up() {
        // do stuff
        $this->load->dbforge();
        $fields = array(
          'stamp_card_id' => array(
                    'type' => 'INT',
               )
        );
        $this->dbforge->add_column('CustomerPunchCardTransactions', $fields);
        echo 'done - 20160603045515_add_stamp_card_id_column_to_customerpunchcardtransactions_table.php';
        echo PHP_EOL;
    }

    /**
     * rollback
     */
    public function down() {
    }

}
