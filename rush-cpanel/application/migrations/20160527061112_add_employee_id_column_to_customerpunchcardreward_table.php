<?php

class Migration_add_employee_id_column_to_customerpunchcardreward_table extends CI_Migration {
    /**
     * up
     */
    public function up() {
        // do stuff
        $this->db->query(
        "   ALTER TABLE CustomerPunchCardReward
            ADD COLUMN employee_id INT(11) NOT NULL");

        echo 'done - 20160527061112_add_employee_id_column_to_customerpunchcardreward_table.php';
        echo PHP_EOL;
    }

    /**
     * rollback
     */
    public function down() {
    }

}