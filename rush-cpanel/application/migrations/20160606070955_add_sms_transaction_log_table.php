<?php

class Migration_add_sms_transaction_log_table extends CI_Migration {
    /**
     * up
     */
    public function up() {
        // do stuff
        $this->load->dbforge();
        $this->dbforge->add_key('id', TRUE);
        $fields = array(
                    'id'                => array(
                       'type'           => 'INT',
                       'constraint'     => 5,
                       'unsigned'       => TRUE,
                       'auto_increment' => TRUE
                    ),
                    'merchant_id'       => array(
                        'type'          => 'INT',
                        'unsigned'      => TRUE
                    ),
                    'message'         => array(
                          'type'        => 'TEXT'
                    ),
                    'mobile_number'        => array(
                        'type'          => 'VARCHAR',
                        'constraint'    => '255'
                    ),
                    'response'        => array(
                        'type'          => 'TEXT'
                    )
                  );
        $this->dbforge->add_field($fields);
        $this->dbforge->create_table('notification_sms_logs');
        echo 'done - 20160606070955_add_sms_transaction_log_table.php';
        echo PHP_EOL;
    }

    /**
     * rollback
     */
    public function down() {
    }

}
