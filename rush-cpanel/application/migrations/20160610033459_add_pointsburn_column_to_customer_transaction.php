<?php

class Migration_add_pointsburn_column_to_customer_transaction extends CI_Migration
{
    /**
     * up
     */
    public function up()
    {
        // do stuff
        $this->load->dbforge();
        $fields = array(
          'pointsBurn' => array(
                    'type' => 'INT',
               )
        );
        $this->dbforge->add_column('CustomerTransaction', $fields);
        echo 'done - 20160610033459_add_pointsburn_column_to_customer_transaction.php';
        echo PHP_EOL;
    }

    /**
     * rollback
     */
    public function down()
    {
    }
}
