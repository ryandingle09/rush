<?php

class Migration_add_void_column_to_customerpunchcardreward_table extends CI_Migration {
    /**
     * up
     */
    public function up() {
        // do stuff
        $this->db->query(
        "   ALTER TABLE CustomerPunchCardReward
            ADD COLUMN void INT(11) DEFAULT 0");

        echo 'done - 20160601025736_add_void_column_to_customerpunchcardreward_table.php';
        echo PHP_EOL;
    }

    /**
     * rollback
     */
    public function down() {
    }

}
