<?php

class Migration_add_passhphrase_column_to_notification_sms_credentials_table extends CI_Migration {
    /**
     * up
     */
    public function up() {
        // do stuff

        $this->db->query(
        "   ALTER TABLE notification_sms_credentials
            ADD COLUMN passphrase varchar(255) DEFAULT NULL");

        echo 'done - 20160606094424_add_passhphrase_column_to_notification_sms_credentials_table.php';
        echo PHP_EOL;
    }

    /**
     * rollback
     */
    public function down() {
    }

}
