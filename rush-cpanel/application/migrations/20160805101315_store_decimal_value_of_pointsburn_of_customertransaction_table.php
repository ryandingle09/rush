<?php

class Migration_store_decimal_value_of_pointsburn_of_customertransaction_table extends CI_Migration {
    /**
     * up
     */
    public function up() {
        $this->db->query(
        "   ALTER TABLE CustomerTransaction
            MODIFY COLUMN pointsBurn DECIMAL(10,2) DEFAULT '0'");

        echo 'done - 20160805101315_store_decimal_value_of_pointsburn_of_customertransaction_table.php';
        echo PHP_EOL;
    }

    /**
     * rollback
     */
    public function down() {
        $this->db->query(
        "   ALTER TABLE CustomerTransaction
            MODIFY COLUMN pointsBurn int(11) DEFAULT '0'");
    }

}