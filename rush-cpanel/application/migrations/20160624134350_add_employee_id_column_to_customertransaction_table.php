<?php

class Migration_add_employee_id_column_to_customertransaction_table extends CI_Migration {
    /**
     * up
     */
    public function up() {
        $this->db->query(
        "   ALTER TABLE CustomerTransaction
            ADD COLUMN employee_id int(11) DEFAULT NULL");

        echo 'done - 20160624134350_add_employee_id_column_to_customertransaction_table.php';
        echo PHP_EOL;
    }

    /**
     * rollback
     */
    public function down() {
        $this->db->query("ALTER TABLE CustomerTransaction DROP COLUMN employee_id");
    }

}