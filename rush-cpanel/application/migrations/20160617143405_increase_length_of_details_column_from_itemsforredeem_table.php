<?php

class Migration_increase_length_of_details_column_from_itemsforredeem_table extends CI_Migration {
    /**
     * up
     */
    public function up() {
        $this->db->query(
        "   ALTER TABLE ItemsForRedeem
            MODIFY COLUMN details VARCHAR(400) DEFAULT 'none'");

        echo 'done - 20160617143405_increase_length_of_details_column_from_itemsforredeem_table.php';
        echo PHP_EOL;
    }

    /**
     * rollback
     */
    public function down() {
        $this->db->query(
        "   ALTER TABLE ItemsForRedeem
            MODIFY COLUMN details VARCHAR(45) DEFAULT 'none'");
    }

}