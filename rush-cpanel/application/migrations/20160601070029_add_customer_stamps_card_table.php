<?php

class Migration_add_customer_stamps_card_table extends CI_Migration {
    /**
     * up
     */
    public function up() {
        // do stuff
        $this->load->dbforge();
        $this->dbforge->add_key('id', TRUE);
        $fields = array(
                    'id' => array(
                       'type'           => 'INT',
                       'constraint'     => 5,
                       'unsigned'       => TRUE,
                       'auto_increment' => TRUE
                    ),
                    'customer_id' => array(
                        'type'     => 'INT',
                        'unsigned' => TRUE
                    )
                  );
        $this->dbforge->add_field($fields);
        $this->dbforge->create_table('CustomerStampsCard');
        echo 'done - 20160601070029_add_customer_stamps_card_table.php';
        echo PHP_EOL;
    }

    /**
     * rollback
     */
    public function down() {
    }

}
