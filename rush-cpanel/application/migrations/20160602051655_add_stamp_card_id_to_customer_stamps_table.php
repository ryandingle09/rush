<?php

class Migration_add_stamp_card_id_to_customer_stamps_table extends CI_Migration {
    /**
     * up
     */
    public function up() {
        // do stuff
        $this->db->query(
        "   ALTER TABLE CustomerStamps
            ADD COLUMN stamp_card_id int(11) DEFAULT NULL");

        echo 'done - 20160602051655_add_stamp_card_id_to_customer_stamps_table.php';
        echo PHP_EOL;
    }

    /**
     * rollback
     */
    public function down() {
    }

}
