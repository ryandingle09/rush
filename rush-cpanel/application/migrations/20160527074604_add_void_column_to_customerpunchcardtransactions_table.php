<?php

class Migration_add_void_column_to_customerpunchcardtransactions_table extends CI_Migration {
    /**
     * up
     */
    public function up() {
        // do stuff
        $this->db->query(
        "   ALTER TABLE CustomerPunchCardTransactions
            ADD COLUMN void INT(11) DEFAULT 0");

        echo 'done - 20160527074604_add_void_column_to_customerpunchcardtransactions_table.php';
        echo PHP_EOL;
    }

    /**
     * rollback
     */
    public function down() {
    }

}