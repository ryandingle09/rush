<?php

class Migration_creat_table_merchant_sms_notification_settings extends CI_Migration
{
    /**
     * up
     */
    public function up()
    {
        // do stuff
        $this->load->dbforge();
        $this->dbforge->add_key('id', true);
        $fields = array(
                    'id' => array(
                       'type'           => 'INT',
                       'constraint'     => 5,
                       'unsigned'       => true,
                       'auto_increment' => true
                    ),
                    'merchant_id' => array(
                        'type'     => 'INT',
                        'unsigned' => true
                    ),
                    'near_completion_enable' => array(
                        'type'      => 'INT',
                        'unsinged'  => true
                    ),
                    'near_completion_message' => array(
                        'type'      => 'Text',
                    ),
                    'near_completion_no_days' => array(
                        'type'      => 'INT',
                        'unsinged'  => true
                    ),
                    'near_expiration_enable' => array(
                        'type'      => 'INT',
                        'unsinged'  => true
                    ),
                    'near_expiration_message' => array(
                        'type'      => 'Text',
                    ),
                    'near_expiration_no_days' => array(
                        'type'      => 'INT',
                        'unsinged'  => true
                    )
                  );
        $this->dbforge->add_field($fields);
        $this->dbforge->create_table('merchant_sms_notification_settings');
        echo 'done - 20160613015846_creat_table_merchant_sms_notification_settings.php';
        echo PHP_EOL;
    }

    /**
     * rollback
     */
    public function down()
    {
    }
}
