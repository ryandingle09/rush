<?php
class M_quicksetup extends CI_Model {
    public function __constuct() {
        $this->load->database();
    }

    public function getCompletionState($merchantId) {
        $result = $this->db->select('completed')->from('Quicksetup')->where('merchant_id',$merchantId)->get();
        return $result;
    }

    public function createQuicksetupProgressTrack($merchantId,$package_id,$num_steps=0) {
        $query = $this->db->select('id')->from('Quicksetup')->where(array('merchant_id' => $merchantId))->get();
        if (!($query->num_rows() > 0)) {
            $query->free_result();
            $this->db->insert('Quicksetup',array('merchant_id' => $merchantId,'completed' => 0, 'package_id' => $package_id, 'num_steps' => $num_steps));
        }
    }

    public function shouldLaunchQuicksetup($merchantId) {
        $this->db->update('Merchant',array('shouldLaunchQuicksetup' => 1),array('merchantid' => $merchantId));
    }

    public function getSavedProgramSettings($merchantId) {
        $data = array(
            'programName' => '',
            'pointsName' => ''
        );
        $query = $this->db->select('id,program_name,points_name')->from('MerchantSettings')->where(array('merchant_id' => $merchantId))->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            $data = array(
                'programName' => $result[0]['program_name'],
                'pointsName' => $result[0]['points_name']
            ); 
        }
        return $data;
    }

    public function saveProgramSettings($merchantId,$programName,$pointsName) {
        $data = array(
            'program_name' => $programName,
            'points_name' => $pointsName
        );
        $query = $this->db->select('id')->from('MerchantSettings')->where(array('merchant_id' => $merchantId))->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            if (count($result) > 0) {
                $merchant_settings_id = $result[0]['id'];
                $this->db->update('MerchantSettings',$data,array('id' => $merchant_settings_id,'merchant_id' => $merchantId));    
            }
            $query->free_result();
        } else {
            $data['merchant_id'] = $merchantId;
            $this->db->insert('MerchantSettings',$data);
        }
    }

    public function getSavedApplicationDetails($merchantId) {
        $data = array(
            'appName' => '',
            'appDescription' => '',
            'appLogo' => ''
        );

        $query = $this->db->select('app_name,app_description,app_logo')->from('MerchantSettings')->where(array('merchant_id' => $merchantId))->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $result = $result[0];
            $query->free_result();
            $data = array(
                'appName' => $result['app_name'],
                'appDescription' => $result['app_description'],
                'appLogo' => $result['app_logo'] ? $result['app_logo'] : 'assets/images/addImage.jpg'
            );
        }
        return $data;
    }

    public function saveApplicationDetails($merchantId,$appName,$appDescription,$appLogo) {
        $data = array(
            'app_name' => $appName,
            'app_description' => $appDescription,
            'app_logo' => $appLogo
        );
        $query = $this->db->select('id')->from('MerchantSettings')->where(array('merchant_id' => $merchantId))->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            $result = $result[0];

            $merchant_settings_id = $result['id'];
            $this->db->update('MerchantSettings',$data,array('id' => $merchant_settings_id,'merchant_id' => $merchantId));
        } else {
            $data['merchant_id'] = $merchantId;
            $this->db->insert('MerchantSettings',$data);
        }
    }

    public function trackProgress($merchantId,$progress) {
        $query = $this->db->select('id')->from('QuicksetupProgress')->where(array('merchant_id' => $merchantId,'flag' => $progress))->get();
        if (!($query->num_rows() > 0)) {
            $query = $this->db->select('id')->from('Quicksetup')->where(array('merchant_id' => $merchantId))->get();
            if ($query->num_rows() > 0) {
                $result = $query->result_array();
                $query->free_result();

                $quicksetup_id = $result[0]['id'];
                $this->db->insert('QuicksetupProgress',array('merchant_id' => $merchantId,'quicksetup_id' => $quicksetup_id, 'flag' => $progress));
                $query = $this->db->select('completed,num_steps')->from('Quicksetup')->where(array('merchant_id' => $merchantId))->get();
                if ($query->num_rows() > 0) {
                    $result = $query->result_array();
                    $query->free_result();
                    $completed = ($result[0]['completed'] + (100 / $result[0]['num_steps']));
                    if ($completed >= 99) {
                        $completed = 100;
                    }
                    $this->db->update('Quicksetup',array('completed' => $completed),array('merchant_id' => $merchantId));
                }
            }
        }
    }
}