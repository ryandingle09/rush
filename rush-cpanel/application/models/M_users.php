<?php
class M_users extends CI_Model {
    public function __constuct() {
        $this->load->database();
    }
    
    public function getAllUsers() {
    	$users = array();
    	$query = $this->db->select("u.admin_id,u.admin_name,u.username,u.email,IF(u.is_active = 1,'Active','Inactive') AS status,r.name AS role")->from('admin_user AS u')->join('Roles AS r','u.role_id = r.id')->get();
    	if ($query->num_rows() > 0) {
    		$users = $query->result_array();
            $query->free_result(); 
    	}
    	return $users;
    }

    public function getRoles() {
        $roles = array();
        $query = $this->db->select('id,name')->from('Roles')->get();
        if ($query->num_rows() > 0) {
            $roles = $query->result_array();
            $query->free_result();
        }
        return $roles;
    }

    public function getUserPassword($id) {
        $password = null;
        $query = $this->db->select('password')->from('admin_user')->where(array('admin_id' => $id))->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $password = $result[0]['password'];
            $query->free_result();
        }
        return $password;
    }

    public function updateUserPassword($id,$password) {
        $data = array(
            'password' => $password
        );
        $this->db->update("admin_user",$data,array('admin_id' => $id));
    }

    public function getMerchantPassword($id) {
        $password = null;
        $query = $this->db->select('password')->from('Merchant')->where(array('merchantid' => $id))->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $password = $result[0]['password'];
            $query->free_result();
        }
        return $password;
    }

    public function updateMerchantPassword($id,$password) {
        $data = array(
            'password' => $password
        );
        $this->db->update("Merchant",$data,array('merchantid' => $id));
    }
}
