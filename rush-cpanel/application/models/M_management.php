<?php

Class M_management extends CI_Model {

    public function __constuct() {
        $this->load->database();
    }

    public function getBranches($merchantID)
    {
        $branches = array();
        $query = $this->db
            ->select('id, branchName, street_name, district, city, zipcode, branch_logo')
            ->from('Branches')
            ->where(array('merchantId' => $merchantID,'is_deleted' => 0))
            ->get();
        if ($query->num_rows() > 0) {
            $branches = $query->result_array();
            $query->free_result();
        }

        return $branches;
    }

    public function addBranch($data) {
        $this->db->insert('Branches',$data);
    }

    public function updateBranch($merchantId,$id,$data) {
        $this->db->update('Branches',$data,array('id' => $id,'merchantId' => $merchantId));
    }

    public function deleteBranch($merchantId,$id)
    {
        $data = array(
            'is_deleted' => 1,
            'deleted_at' => date('Y-m-d H:is'),
            'deleted_by' => $merchantId,
            'branch_logo' => null
        );
        $this->db->update('Branches',$data,array('id' => $id,'merchantId' => $merchantId));
    }

    public function getBranch($branchId)
    {
        $branch = null;
        $query = $this->db->select("branchName")->from("Branches")->where(array("id" => $branchId))->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $branch = $result[0]['branchName'];
            $query->free_result();
        }

        return $branch;
    }

    public function getBranchLogo($branchId)
    {
        $branchLogo = null;
        $query = $this->db->select("branch_logo")->from("Branches")->where(array("id" => $branchId))->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $branchLogo = $result[0]['branch_logo'];
            $query->free_result();
        }

        return $branchLogo;
    }

    public function getEmployees($merchantID) {
        $employees = array();
        $query = $this->db->select("userId,employeeId,employeeName,requirePassword,fourDigit,branchId")->from("Users")->where(array("merchantId" => $merchantID,"is_deleted" => 0))->get();
        if ($query->num_rows() > 0) {
            $employees = $query->result_array();
            $query->free_result();
            $index = 0;
            $last = count($employees);
            while ($index < $last) {
                $branch = $this->getBranch($employees[$index]['branchId']);
                $employees[$index]['status'] = 'Enabled';
                if ($branch != null) {
                    $employees[$index]['branch'] = $branch;
                }
                $index++;
            }
        }
        return $employees;
    }

    public function checkUsernameAvailable($username) {
        $not_available = false;
        $query = $this->db->query("SELECT COUNT(userId) AS num_found FROM Users WHERE username = ?",array(
            $username
        ));
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            if ($result[0]['num_found'] > 0) {
                $not_available = true;
            }
            $query->free_result();
        }
        return $not_available;
    }

    public function addEmployee($employee) {
        $this->db->insert("Users",$employee);
    }

    public function updateEmployee($merchantId,$employeeId,$employee) {
        $this->db->update("Users",$employee,array(
            "merchantId" => $merchantId,
            "userId" => $employeeId
        ));
    }

    public function deleteEmployee($merchantId,$id) {
        $data = array(
            'is_deleted' => 1,
            'deleted_at' => date('Y-m-d H:is'),
            'deleted_by' => $merchantId
        );
        $this->db->update('Users',$data,array('userId' => $id,'merchantId' => $merchantId));
    }

    public function getCustomer($merchantID) {
        $query = $this->db->from('Customer AS c')
            ->join('Branches b', 'c.branch_id = b.id', 'left')
            ->where(array('c.merchantId' => $merchantID))
            ->order_by('timestamp', 'DESC')->get();
        return $query->result_array();
    }

    public function getPoints($merchantID)
    {
        $query = $this->db
            ->select(
            '   c.timestamp
                , c.fullName
                , c.mobileNumber
                , p.totalPoints
                , p.usedPoints
                , p.currentPoints
            ')
            ->from('Customer AS c')
            ->join('customerPoints p', 'c.customerId = p.customerId', 'left')
            ->where(array('c.merchantId' => $merchantID))
            ->order_by('c.timestamp', 'DESC')->get();
        return $query->result_array();
    }

    public function getStamps($merchantId)
    {
        $sql =
        "   SELECT
            c.timestamp
            , c.fullName
            , c.mobileNumber
            , mpm.num_stamps AS target
            , COUNT(cs.transaction_id) AS accomplished
            FROM
            CustomerPunchCardTransactions cpct
            INNER JOIN MerchantPromoMechanics mpm
                ON (cpct.promo_id = mpm.id
                    AND ? BETWEEN DATE_FORMAT(mpm.duration_from_date, '%Y-%m-%d') AND DATE_FORMAT(mpm.duration_to_date, '%Y-%m-%d')
                )
            LEFT JOIN Customer c ON cpct.customer_id = c.customerId
            LEFT JOIN CustomerStamps cs ON cpct.id = cs.transaction_id
            WHERE
            cpct.merchant_id = ?
            AND cpct.void = 0
            GROUP BY
            cpct.customer_id
        ";
        $query = $this->db->query($sql, array(date('Y-m-d', time()), $merchantId));
        $stamps = $query->result_array();

        for ($i=0; $i < count($stamps); $i++) {
            $balance = $stamps[$i]['target'] - $stamps[$i]['accomplished'];
            $stamps[$i]['balance'] = ($balance > 0) ? $balance : '0';
        }
        return $stamps;
    }

    public function getReports($merchantID)
    {
        $query = $this->db
            ->select('cs.id, cs.message, cs.type, cs.created_at, c.fullName, c.mobileNumber')
            ->from('lara_customer_support as cs')
            ->join('Customer as c', 'cs.customer_id = c.customerId', 'left')
            ->where(array('cs.merchant_id' => $merchantID))
            ->order_by('cs.id', 'ASC')->get();
        $results = $query->result_array();
        for ($i=0; $i < count($results); $i++) {
            $results[$i]['class'] = ( $results[$i]['type'] == "text" ) ? 'fa-file-o' : 'fa-play';
        }
        return $results;
    }
}