<?php
class M_Reward extends CI_Model {
    public function __constuct() {
        $this->load->database();
    }

    public function getDailyRedeemed($merchantId)
    {
        $count = 0;
        $sql =
        "   SELECT
            COUNT(*) AS count
            FROM
            CustomerPunchCardReward cpcr
            LEFT JOIN MerchantPunchCardRewards mpcr ON cpcr.reward_id = mpcr.id
            WHERE
            mpcr.merchant_id = ?
            AND DATE(cpcr.date_added) >= ?
            AND cpcr.void = 0
        ";
        $query = $this->db->query($sql, array($merchantId, date('Y-m-d', time())));
        $row = $query->row_array(0);
        $count = $row['count'];

        return $count;
    }

    public function getMonthlyRedeemed($merchantId)
    {
        $count = 0;
        $sql =
        "   SELECT
            COUNT(*) AS count
            FROM
            CustomerPunchCardReward cpcr
            LEFT JOIN MerchantPunchCardRewards mpcr ON cpcr.reward_id = mpcr.id
            WHERE
            mpcr.merchant_id = ?
            AND DATE_FORMAT(cpcr.date_added, '%Y-%m') = ?
            AND cpcr.void = 0
        ";
        $query = $this->db->query($sql, array($merchantId, date('Y-m', time())));
        $row = $query->row_array(0);
        $count = $row['count'];

        return $count;
    }
}