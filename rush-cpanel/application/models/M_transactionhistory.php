<?php

/**
 * Created by PhpStorm.
 * User: Glenn Matotes
 * Date: 2/17/2015
 * Time: 5:21 PM
 */
class M_transactionhistory extends CI_Model
{

    public function __constuct()
    {
        $this->load->database();
    }

    public function getMerchantTransactions($merchantId)
    {
        $sql =
        "   SELECT
            ct.timestamp
            , CASE ct.transactionType
                WHEN 'void-earn' THEN 'void'
                WHEN 'void-paypoints' THEN 'void'
                WHEN 'void-redeem' THEN 'void'
                WHEN 'achievement-unlock' THEN
                      (CASE
                          WHEN aut.name = 'complete_profile' THEN 'Achievement: Profile Complete'
                          WHEN aut.name = 'customer_app_activation' THEN 'Achievement: App Activation'
                          WHEN aut.name = 'facebook_like' THEN 'Achievement: Facebook Like'
                          WHEN aut.name = 'members_referral' THEN 'Achievement: Successful Invite'
                          WHEN aut.name = 'transaction_no_with_minimum_amount' THEN 'Achievement: 1st Transactions Milestone'
                          WHEN aut.name = 'redemption_no_with_minimum_points' THEN 'Achievement: 1st Redemptions Milestone'
                          WHEN aut.name = 'purchase_no_within_year' THEN 'Achievement: Total Purchase Milestone'
                          WHEN aut.name = 'special_day_purchase' THEN CONCAT('Achievement: Special Day (', aut.options_date , ')')
                          WHEN aut.name = 'double_points' THEN 'Achievement: Double Points'
                          WHEN aut.name = 'attended_event' THEN CONCAT('Achievement: Event (', aut.options_name, ')')
                          WHEN aut.name = 'answering_survey' THEN CONCAT('Achievement: Survey (', aut.options_name, ')')
                      END)
                ELSE ct.transactionType
              END as transactionType
            , ct.amountPaidWithCash
            , IFNULL(ct.receiptReferenceNumber, ct.transactionReferenceCode) AS receiptReferenceNumber
            , c.fullName AS customerName
            , CASE ct.transactionType
                WHEN 'redeem' THEN ct.pointsRedeem
                WHEN 'void-redeem' THEN CONCAT('+',ct.voided_points)
                WHEN 'paypoints' THEN ct.pointsBurn
                WHEN 'void-paypoints' THEN CONCAT('+',ct.pointsBurn)
                WHEN 'earn' THEN ct.pointsEarned
                WHEN 'void-earn' THEN CONCAT('-',ct.voided_points)
                WHEN 'achievement-unlock' THEN ct.pointsEarned
                WHEN 'transfer' THEN ct.pointsTransferred
              END as points
            , b.branchName
            , CASE ct.transactionType
                WHEN 'void-redeem' THEN CONCAT('(', ifr.name, ')')
                ELSE ifr.name
              END AS reward
            , ct.employee_id AS employeeNumber
            FROM CustomerTransaction AS ct
            LEFT JOIN Customer c
                ON ct.customerId = c.customerId
            LEFT JOIN Branches b
                ON ct.branchId = b.id
            LEFT JOIN Redeem r
                ON ( ct.transactionReferenceCode = r.redeemReferenceCode and r.status = 1 )
            LEFT JOIN redeemedItems ri
                ON r.redeemId = ri.redeemId
            LEFT JOIN ItemsForRedeem ifr
                ON ri.redeemItemId = ifr.redeemItemId
            LEFT JOIN achievements_unlock_transactions aut
                ON ct.achievement_id = aut.id
            WHERE
            ct.merchant_id = ?
            ORDER BY timestamp DESC
        ";
        $result = $this->db->query($sql, array($merchantId));

        return $result->result_array();
    }

    public function getAllMerchantTransactions()
    {
        $result = $this->db->query(
        "   SELECT
            ct.timestamp
            , ct.transactionType
            , ct.amountPaidWithCash
            , ct.receiptReferenceNumber
            , CONCAT(c.firstName,' ',c.lastName) AS customerName
            , b.branchName
            FROM CustomerTransaction AS ct
            LEFT JOIN Customer c
                ON ct.customerId = c.customerId
            LEFT JOIN Branches b
                ON ct.branchId = b.id
        ");

        return $result->result_array();
    }

    public function getInAppTransactions($merchantId)
    {
        $sql =
        "  SELECT * FROM (
                SELECT
                cpct.date_punched AS timestamp
                , cpct.type AS transactionType
                , c.fullName AS customerName
                , cpct.amount AS amountPaidWithCash
                , CASE cpct.type
                   WHEN 'redeem' THEN (CASE WHEN cpct.void = 1 THEN CONCAT('(',mpcr.reward,')') ELSE mpcr.reward END)
                   WHEN 'earn' THEN (CASE WHEN cpct.void= 1 THEN CONCAT('-', COUNT(cs.transaction_id)) ELSE COUNT(cs.transaction_id) END)
                  END AS stamps
                , b.branchName AS branchName
                , u.userId AS employeeNumber
                , mpm.promo_title as promoTitle
                FROM
                CustomerPunchCardTransactions cpct
                LEFT JOIN Customer c ON cpct.customer_id = c.customerId
                LEFT JOIN Users u ON cpct.employee_id = u.userId
                LEFT JOIN Branches b ON u.branchId = b.id
                LEFT JOIN CustomerStamps cs ON cpct.id = cs.transaction_id
                LEFT JOIN MerchantPromoMechanics mpm ON cpct.promo_id = mpm.id
                LEFT JOIN MerchantPunchCardRewards mpcr ON cpct.reward_id = mpcr.id
                WHERE
                cpct.merchant_id = ?
                GROUP BY cpct.id
            ) history
        ";
        $result = $this->db->query($sql, array($merchantId));
        $transactions = $result->result_array();
        for ($i=0; $i < count($transactions); $i++) {
            if (is_numeric($transactions[$i]['amountPaidWithCash'])) {
                $transactions[$i]['amountPaidWithCash'] = number_format($transactions[$i]['amountPaidWithCash'], 2);
            }
        }

        return $transactions;
    }

    public function getDailyTransactions($merchantId)
    {
        $count = 0;
        $sql =
        "   SELECT
            COUNT(*) AS count
            FROM
            CustomerPunchCardTransactions cpct
            WHERE
            cpct.merchant_id = ?
            AND DATE(cpct.date_punched) >= ?
            AND cpct.void = 0
        ";
        $query = $this->db->query($sql, array($merchantId, date('Y-m-d', time())));
        $row = $query->row_array(0);
        $count = $row['count'];

        return $count;
    }

    public function getMonthlyTransactions($merchantId)
    {
        $count = 0;
        $sql =
        "   SELECT
            COUNT(*) AS count
            FROM
            CustomerPunchCardTransactions cpct
            WHERE
            cpct.merchant_id = ?
            AND DATE_FORMAT(cpct.date_punched, '%Y-%m') = ?
            AND cpct.void = 0
        ";
        $query = $this->db->query($sql, array($merchantId, date('Y-m', time())));
        $row = $query->row_array(0);
        $count = $row['count'];

        return $count;
    }

    public function getDailyPointsBurn($merchantId)
    {
        $count = 0;
        $sql =
        "   SELECT
            COUNT(*) AS count
            FROM
            CustomerTransaction ct
            LEFT JOIN Customer c ON ct.customerId = c.customerId
            WHERE
            ct.transactionType IN ('paypoints', 'redeem')
            AND ct.merchant_id = ?
            AND DATE(ct.timestamp) >= ?
        ";
        $query = $this->db->query($sql, array($merchantId, date('Y-m-d', time())));
        $row = $query->row_array(0);
        $count = $row['count'];

        return $count;
    }

    public function getMonthlyPointsBurn($merchantId)
    {
        $count = 0;
        $sql =
        "   SELECT
            COUNT(*) AS count
            FROM
            CustomerTransaction ct
            LEFT JOIN Customer c ON ct.customerId = c.customerId
            WHERE
            ct.transactionType IN ('paypoints', 'redeem')
            AND ct.merchant_id = ?
            AND DATE_FORMAT(ct.timestamp, '%Y-%m') = ?
        ";
        $query = $this->db->query($sql, array($merchantId, date('Y-m', time())));
        $row = $query->row_array(0);
        $count = $row['count'];

        return $count;
    }

    public function getDailyPointsEarn($merchantId)
    {
        $count = 0;
        $sql =
        "   SELECT
            COUNT(*) AS count
            FROM
            CustomerTransaction ct
            LEFT JOIN Customer c ON ct.customerId = c.customerId
            WHERE
            ct.transactionType = 'earn'
            AND ct.merchant_id = ?
            AND DATE(ct.timestamp) >= ?
        ";
        $query = $this->db->query($sql, array($merchantId, date('Y-m-d', time())));
        $row = $query->row_array(0);
        $count = $row['count'];

        return $count;
    }

    public function getMonthlyPointsEarn($merchantId)
    {
        $count = 0;
        $sql =
        "   SELECT
            COUNT(*) AS count
            FROM
            CustomerTransaction ct
            LEFT JOIN Customer c ON ct.customerId = c.customerId
            WHERE
            ct.transactionType = 'earn'
            AND ct.merchant_id = ?
            AND DATE_FORMAT(ct.timestamp, '%Y-%m') = ?
        ";
        $query = $this->db->query($sql, array($merchantId, date('Y-m', time())));
        $row = $query->row_array(0);
        $count = $row['count'];

        return $count;
    }
}
