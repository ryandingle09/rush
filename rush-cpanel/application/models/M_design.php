<?php

/**
 * Created by PhpStorm.
 * User: Glenn Matotes
 * Date: 2/17/2015
 * Time: 5:21 PM
 */
class M_design extends CI_Model
{

    public function __constuct()
    {
        $this->load->database();
    }

    public function storeDesignDetails($merchantId, $packageId, $designType, $data)
    {
        $query = $this->db->select('merchantPackageId')->from('MerchantPackage')->where(array('packageId' => $packageId, 'merchantId' => $merchantId, 'designType' => $designType))->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            $id = $result[0]['merchantPackageId'];
            $data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->update('MerchantPackage', $data, array(
                'merchantId' => $merchantId,
                'packageId' => $packageId,
                'designType' => $designType
            ));
        } else {
            $data['merchantId'] = $merchantId;
            $data['packageId'] = $packageId;
            $data['designType'] = $designType;
            $data['created_at'] = date('Y-m-d H:i:s');
            $this->db->insert('MerchantPackage', $data);
        }
    }

    public function getSavedLoyaltyCardDesign($merchantId)
    {
        $data = array(
            'logo' => '',
            'background' => '',
            'cardbgcolor' => ''
        );
        $query = $this->db->select('logoURL,backgroundURL,backgroundColorHEX')->from('MerchantPackage')->where(array('merchantId' => $merchantId, 'designType' => 2))->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();

            $result = $result[0];
            $data = array(
                'logo' => $result['logoURL'],
                'background' => $result['backgroundURL'],
                'cardbgcolor' => $result['backgroundColorHEX']
            );
        }
        return $data;
    }

    public function getSavedMerchantApplication($merchantId)
    {
        $data = array(
            'logo' => '',
            'background' => '',
            'overlayColor' => '',
            'textColor' => '',
            'buttonsColor' => '',
            'stamp' => ''
        );
        $query = $this->db->select('logoURL,backgroundURL,stampURL,overlayColor,textColor,buttonsColor')->from('MerchantPackage')->where(array('merchantId' => $merchantId, 'designType' => 1))->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            $result = $result[0];
            $data = array(
                'logo' => $result['logoURL'],
                'background' => $result['backgroundURL'],
                'overlayColor' => $result['overlayColor'],
                'textColor' => $result['textColor'],
                'buttonsColor' => $result['buttonsColor'],
                'stamp' => $result['stampURL'] ? $result['stampURL'] : 'assets/images/addImage.jpg',
            );
        }
        return $data;
    }

    public function getSavedCustomerAppDesign($merchantId)
    {
        $data = array(
            'logo' => '',
            'background' => '',
            'overlayColor' => '',
            'textColor' => '',
            'buttonsColor' => '',
            'stamp' => '',
            'name' => ''
        );
        $query = $this->db->select('logoURL,backgroundURL,stampURL,overlayColor,textColor,buttonsColor,merchantName')->from('MerchantPackage')->where(array('merchantId' => $merchantId, 'designType' => 3))->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();

            $result = $result[0];
            $data = array(
                'logo' => $result['logoURL'],
                'background' => $result['backgroundURL'],
                'overlayColor' => $result['overlayColor'],
                'textColor' => $result['textColor'],
                'buttonsColor' => $result['buttonsColor'],
                'stamp' => $result['stampURL'] ? $result['stampURL'] : 'assets/images/addImage.jpg',
                'name' => $result['merchantName']
            );
        }
        return $data;
    }

    public function getMerchantAppDesignData($merchantId)
    {
        $merchant_app_data = array();
        $query = $this->db->select('logoURL,backgroundURL,stampURL,overlayColor,textColor,buttonsColor')->from('MerchantPackage')->where(array("merchantid" => $merchantId, 'designType' => 1))->limit(1, 1)->get();
        if ($query->num_rows() > 0) {
            $merchant_app_data = $query->result_array();
            $query->free_result();
        }
        return $merchant_app_data;
    }

    public function getCardDesignData($merchantId)
    {
        $card_design_data = array();
        $query = $this->db->select('logoURL,backgroundURL,backgroundColorHEX')->from('MerchantPackage')->where(array("merchantid" => $merchantId, 'designType' => 2))->limit(1, 1)->get();
        if ($query->num_rows() > 0) {
            $card_design_data = $query->result_array();
            $query->free_result();
        }
        return $card_design_data;
    }

    public function getCustomerAppDesignData($merchantId)
    {
        $customer_app_data = array();
        $query = $this->db->select('logoURL,backgroundURL,stampURL,overlayColor,textColor,buttonsColor')->from('MerchantPackage')->where(array("merchantid" => $merchantId, 'designType' => 3))->limit(1, 1)->get();
        if ($query->num_rows() > 0) {
            $customer_app_data = $query->result_array();
            $query->free_result();
        }
        return $customer_app_data;
    }

    public function getMerchantSettings($merchantId)
    {
        $settings = array();
        $query = $this->db->select("program_name,points_name")->from("MerchantSettings")->where(array("merchant_id" => $merchantId))->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $settings = $result[0];
            $query->free_result();
        }
        return $settings;
    }

    public function updateMerchantStamp($merchantId, $stampURL)
    {
        $data = array('stampURL' => $stampURL);
        $this->db->where(array('merchantId' => $merchantId, 'designType' => 1));
        $this->db->update('MerchantPackage', $data);
    }

    public function convert_grayscale($filename)
    {
        $image_info = getimagesize($filename);
        $mime_type = $image_info['mime'];
        $image = $mime_type == 'image/png' ? imagecreatefrompng($filename) : imagecreatefromjpeg($filename);
        if ($image) {
            imagefilter($image, IMG_FILTER_GRAYSCALE);
            if ($mime_type == 'image/png') {
                imagepng($image);
            } else {
                imagejpeg($image);
            }
        }
        imagedestroy($image);
    }

    public function updateAppStoreLogo($merchantId, $appLogo)
    {
        $data = array(
            'app_logo' => $appLogo
        );

        $query = $this->db->select('id')->from('MerchantSettings')
            ->where(array('merchant_id' => $merchantId))->get();

        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            $result = $result[0];

            $merchant_settings_id = $result['id'];
            $this->db->update('MerchantSettings', $data, array('id' => $merchant_settings_id));
        }
    }
}
