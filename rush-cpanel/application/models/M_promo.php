<?php
class M_promo extends CI_Model {
    public function __constuct() {
        $this->load->database();
    }

    public function savePromoMechanics($merchantId, $packageId, $status, $promoTitle, $noOfStamps, $durationStart, $durationEnd, $redemption_promo_start, $redemption_promo_end, $amount, $rewards) 
    {
        $data = array(
        	'promo_title' => $promoTitle,
        	'num_stamps' => $noOfStamps,
        	'duration_from_date' => $durationStart,
        	'duration_to_date' => $durationEnd,
            'redemption_duration_from_date' => $redemption_promo_start,
            'redemption_duration_to_date' => $redemption_promo_end,
            'merchant_id' => $merchantId,
            'package_id'  => $packageId,
            'created_at'  => date('Y-m-d H:i:s'),
            'created_by'  => $merchantId,
            'status'      => $status
        ); 
    	
    	$this->db->insert('MerchantPromoMechanics',$data);

    	$promo_mechanics_id = $this->db->insert_id();

        $amount_promo = array(
                'promo_id'  => $promo_mechanics_id,
                'amount'    => $amount,
                'no_of_stamp' => 1
            );

        $this->db->insert('MerchantPromoAmountStamp',$amount_promo);

        if( $rewards ) {
            foreach($rewards as $reward) {
                $reward['promo_id'] = $promo_mechanics_id;
                $reward['merchant_id'] = $merchantId;

                $this->db->insert('MerchantPunchCardRewards',$reward);
            }
        }

        return $promo_mechanics_id;
    }

    public function getSavedPromoMechanics($merchantId,$packageId) 
    {
        $promo_mechanics = [];
        $this->db->order_by("duration_from_date", "desc"); 
        $query = $this->db->select('id,promo_title,num_stamps,duration_from_date,duration_to_date,status')->from('MerchantPromoMechanics')->where(array('merchant_id' => $merchantId,'package_id' => $packageId))->get();
        if ($query->num_rows() > 0) {
            $results = $query->result_array();
            $query->free_result();
            for($i = 0; $i < count($results); $i++) {
               $promo_mechanics[$i]['id'] = $results[$i]['id'];
               $promo_mechanics[$i]['title'] = $results[$i]['promo_title'];
               $promo_mechanics[$i]['status'] = $this->formatStatus($results[$i]['status'], $results[$i]['duration_from_date'], $results[$i]['duration_to_date']);
               $promo_mechanics[$i]['duration_from_date'] = date("M d, Y", strtotime($results[$i]['duration_from_date']));
               $promo_mechanics[$i]['duration_to_date'] = date("M d, Y", strtotime($results[$i]['duration_to_date']));
            }
        }
        return $promo_mechanics;
    }

    public function formatStatus($status, $durationStart, $durationEnd)
    {
        $formatted_status = null;
        $now = strtotime(Date('Y-m-d'));
        $start_ts = strtotime($durationStart);
        $end_ts = strtotime($durationEnd);

        if($status == 1) { // published
            $formatted_status = "Pending";

            //check if expired
            if( $end_ts < $now )  {
                $formatted_status = "Expired";
                return $formatted_status;
            }

            // check if pending
            if(  $now >= $start_ts && $now <= $end_ts )  {
                $formatted_status = "Live";
            }
        } else {
            $formatted_status = "Draft";
        }

        return $formatted_status;
    }

    public function saveConversionSettings($merchantId,$packageId,$earningPeso,$earningPoints,$redemptionPoints,$redemptionPeso) 
    {
        $data = array(
            'redemption_points' => $redemptionPoints,
            'redemption_peso' => $redemptionPeso,
            'earning_points' => $earningPoints,
            'earning_peso' => $earningPeso
        );
        $query = $this->db->select('id')->from('MerchantConversionSettings')->where(array('merchant_id' => $merchantId,'package_id' => $packageId))->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            $conversion_settings_id = $result[0]['id'];

            $data['modified_by'] = $merchantId;
            $data['modified_at'] = date('Y-m-d H:i:s');
            $this->db->update('MerchantConversionSettings',$data,array('id' => $conversion_settings_id,'merchant_id' => $merchantId,'package_id' => $packageId));
        } else {
            $data['merchant_id'] = $merchantId;
            $data['package_id'] = $packageId;
            $data['created_by'] = $merchantId;
            $data['created_at'] = date('Y-m-d H:i:s');
            $this->db->insert('MerchantConversionSettings',$data);
        }
    }

    public function getSavedConversionSettings($merchantId,$packageId) 
    {
        $data = array(
            'redemptionPoints' => 0,
            'redemptionPointsAmount' => 0,
            'earningPoints' => 0,
            'earningPointsAmount' => 0
        );

        $query = $this->db->select('id,redemption_points,redemption_peso,earning_points,earning_peso')->from('MerchantConversionSettings')->where(array('merchant_id' => $merchantId,'package_id' => $packageId))->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            $result = $result[0];
            $data = array(
                'redemptionPoints' => $result['redemption_points'],
                'redemptionPointsAmount' => $result['redemption_peso'],
                'earningPoints' => $result['earning_points'],
                'earningPointsAmount' => $result['earning_peso']
            );
        }
        return $data;
    }

    public function deletePromoMechanics($merchantId,$promoId)
    {
        $query = $this->db->select('id')
                        ->from('MerchantPromoMechanics')
                        ->where(array('merchant_id' => $merchantId,'id' => $promoId))
                        ->get();
        if ($query->num_rows() > 0) {
            $results = $query->result_array();
            $query->free_result(); 
            $this->db->delete('MerchantPromoMechanics', array('id' => $promoId)); 
            $this->db->delete('MerchantPunchCardRewards', array('promo_id' => $promoId)); 
            $this->db->delete('MerchantPromoAmountStamp', array('promo_id' => $promoId)); 
        }
    }

    public function getPromoMechanics($promoId)
    {
        $result = [];
        $query = $this->db->select('*')
                        ->from('MerchantPromoMechanics')
                        ->where(array('id' => $promoId))
                        ->get();
        if ($query->num_rows() > 0) {
           $result = $query->result_array()[0]; 
           $query->free_result(); 
           //get amount
            $query = $this->db->select('amount')
                        ->from('MerchantPromoAmountStamp')
                        ->where(array('promo_id' => $promoId))
                        ->get();
            $result = array_merge($result, $query->result_array()[0]);
            $query->free_result(); 
            //get rewards
            $query =  $this->db->select('*')
                        ->from('MerchantPunchCardRewards')
                        ->where(array('promo_id' => $promoId))
                        ->get();
            $result['rewards'] = $query->result_array(); 
            $query->free_result(); 
        }

        return $result;
    }

    public function updatePromoMechanics(
        $merchantId,
        $promoId,
        $status,
        $promoTitle,
        $durationStart,
        $durationEnd,
        $redemptionStart,
        $redemptionEnd,
        $amount,
        $numStamps
    ) {
        $data = array(
            'promo_title' => $promoTitle,
            'duration_from_date' => $durationStart,
            'duration_to_date' => $durationEnd,
            'redemption_duration_from_date' => $redemptionStart,
            'redemption_duration_to_date' => $redemptionEnd,
            'status' => $status,
            'num_stamps' => $numStamps
        );
        $this->db->where('id', $promoId);
        $this->db->update('MerchantPromoMechanics', $data);
        $amount_data = array(
                'amount' => $amount
            );
        $this->db->where('promo_id', $promoId);
        $this->db->update('MerchantPromoAmountStamp', $amount_data);
    }

    public function getPromoReward($promoId, $stampNo)
    {
        $result = [];
        $query = $this->db->select('*')
                        ->from('MerchantPunchCardRewards')
                        ->where(array('promo_id' => $promoId))
                        ->where(array('no_stamps' => $stampNo))
                        ->get();
        if ($query->num_rows() > 0) {
           $result = $query->result_array()[0]; 
           $query->free_result(); 
        }

        return $result;
    }

    public function updatePromoReward($promoId, $stampNo, $description, $imageUrl)
    {
        $data = array(
            'reward' => $description,
        );
        if ($imageUrl) {
            $data = array_merge($data, array('image_url' => $imageUrl));
        }
        $this->db->where('promo_id', $promoId);
        $this->db->where('no_stamps', $stampNo);
        $this->db->update('MerchantPunchCardRewards', $data);
    }

    public function addPromoReward($promoId, $stampNo, $description, $imageUrl, $merchantId)
    {
        $data = array(
            'promo_id' => $promoId,
            'no_stamps' => $stampNo,
            'reward' => $description,
            'merchant_id' => $merchantId,
            'image_url' => $imageUrl
        );
        $this->db->insert('MerchantPunchCardRewards', $data);
    }

    public function removePromoReward($promoId, $stampNo)
    {
        $this->db->delete('MerchantPunchCardRewards', array('promo_id' => $promoId, 'no_stamps' => $stampNo) );
    }

    public function removePromoRewardBeyondStampsCounts($promoId, $stampsCount)
    {
        $this->db->delete('MerchantPunchCardRewards', array('promo_id' => $promoId, 'no_stamps >' => $stampsCount) );
    }

    public function isPromoDurationValid($merchantId, $durationStart, $durationEnd, $promoId = null)
    {
        $return = true;
        if($durationStart) {
            if($promoId) {
                $this->db->where('id !=', $promoId);
            }
            $this->db->where('merchant_id', $merchantId);
            $this->db->where('duration_from_date <= ', $durationStart);
            $this->db->where('duration_to_date >=', $durationStart); 
            $query = $this->db->select('*')
                        ->from('MerchantPromoMechanics')
                        ->get();
            if ($query->num_rows() > 0) {
                $return = false;
            }
            $query->free_result();

            if($promoId) {
                $this->db->where('id !=', $promoId);
            }
            $this->db->where('merchant_id', $merchantId);
            $this->db->where('duration_from_date <=', $durationEnd);
            $this->db->where('duration_to_date >=', $durationEnd);
            $query = $this->db->select('*')
                        ->from('MerchantPromoMechanics')
                        ->get();
            if ($query->num_rows() > 0) {
                $return = false;
            }
            $query->free_result();
        }
        return $return;
    }
}
