<?php
class M_Merchant extends CI_Model {
    public function __constuct() {
        $this->load->database();
    }

    public function getMerchantData($merchantId) {
        $query = $this->db->select('isfirstlogin,packageid,businessName,requireChangePassword,shouldLaunchQuicksetup')->from('Merchant')->where(array('merchantid' => $merchantId))->get();
        $merchantData = array();
        if ($query->num_rows() > 0) {
            $merchantData = $query->result_array();
            $query->free_result();
        }
    	return $merchantData;
    }

    public function clearFirstLoginFlag($merchantId) {
    	$data = array(
    		'isfirstlogin' => 0
    	);
    	$this->db->update('Merchant',$data,array('merchantid' => $merchantId));
    }

    public function clearQuicksetupLaunchFlag($merchantId) {
        $data = array(
            'shouldLaunchQuicksetup' => 0
        );
        $this->db->update('Merchant',$data,array('merchantid' => $merchantId));        
    }

    public function clearChangePasswordFlag($merchantId) {
        $data = array(
            'requireChangePassword' => 0
        );
        $this->db->update('Merchant',$data,array('merchantid' => $merchantId));
    }

    public function changePasswordRequired($merchantId) {
        $query = $this->db->select('requireChangePassword')->from('Merchant')->where(array('merchantId' => $merchantId))->get();
        if($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            if ($result[0]['requireChangePassword'] == 1) {
                return true;
            } 
        }
        return false;
    }

    public function getDailySignups($merchantId) {
        $count = 0;
        $sql =
        "   SELECT
            COUNT(*) AS count
            FROM Customer c
            WHERE
            c.merchantid = ?
            AND  DATE(timestamp) >= ?
        ";
        $query = $this->db->query($sql, array($merchantId, date('Y-m-d', time())));
        $row = $query->row_array(0);
        $count = $row['count'];

        return $count;
    }

    public function getMonthlySignups($merchantId) {
        $count = 0;
        $sql =
        "   SELECT
            COUNT(*) AS count
            FROM Customer c
            WHERE
            c.merchantid = ?
            AND DATE_FORMAT(timestamp, '%Y-%m') = ?
        ";
        $query = $this->db->query($sql, array($merchantId, date('Y-m', time())));
        $row = $query->row_array(0);
        $count = $row['count'];

        return $count;
    }

    public function getAllMerchants() {
        $merchants = array();
        $query = $this->db->select('m.merchantId,m.businessName,m.contactPerson,m.businessType,m.address,m.packageId,p.name')->from('Merchant AS m')->join('Package AS p','m.packageId = p.packageId')->get();
        if ($query->num_rows() > 0) {
            $merchants = $query->result_array();
            $query->free_result();
        }
        return $merchants;
    }
}
