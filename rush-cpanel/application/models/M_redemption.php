<?php
class M_redemption extends CI_Model {
    public function __constuct() {
        $this->load->database();
    }

    public function saveItemForRedeem($merchantId,$data) {
    	foreach ($data as $d) {
            $item = array(
                'name' => $d['name'],
                'details' => $d['details'],
                'pointsRequired' => $d['pointsRequired'],
                'timestamp' => $d['timestamp'],
                'merchantid' => $d['merchantid'],
                'status' => $d['status'],
            );

            if (isset($d['imageURL'])) {
                $item['imageURL'] = $d['imageURL'];
            }
            if ($d['redeemItemId'] == 0) {
                $this->db->insert('ItemsForRedeem',$item);
            } else {
                $this->db->update('ItemsForRedeem',$item,array("redeemItemId" => $d['redeemItemId'],"merchantid" => $merchantId));
            }
        }
    }

    public function deleteItemForRedeem($merchantId,$redeemId) {
        $data = array(
            'status' => 0
        );
        $this->db->update('ItemsForRedeem',$data,array("redeemItemId" => $redeemId,"merchantid" => $merchantId));
    }

    public function getRedemptions($merchantId) {
        $redemptions = array();
        $query = $this->db->select('redeemItemId,name,imageURL,details,pointsRequired')->from('ItemsForRedeem')->where(array('merchantid' => $merchantId,'status' => 1))->order_by('redeemItemId','ASC')->get();
        if ($query->num_rows() > 0) {
            $redemptions = $query->result_array();
            $index = 0;
            $last = count($redemptions);
            while ($index < $last) {
                $redemptions[$index]['number'] = ($index+1);
                $index++;
            }
            $query->free_result();
        }
        return $redemptions;
    }
}
