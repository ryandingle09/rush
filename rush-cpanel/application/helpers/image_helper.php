<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('has_upload_file')) {
    function has_upload_file($tag)
    {
        if (isset($_FILES) && isset($_FILES[$tag]['tmp_name'])) {
            if (!empty($_FILES[$tag]['tmp_name'])) {
                return true;
            }
        }
        return false;
    }
}

if (!function_exists('check_image_supported')) {
    function check_image_supported($tag)
    {
        if (isset($_FILES) && isset($_FILES[$tag]['tmp_name'])) {
            $image_info = getimagesize($_FILES[$tag]['tmp_name']);
            $mime_type = $image_info['mime'];
            if (in_array($mime_type,array('image/jpeg', 'image/png', 'image/gif'))) {
                return true;
            }
        }
        return false;
    }
}

if (!function_exists('get_fileupload_options')) {
    function get_fileupload_options($width, $height, $upload_path)
    {
        $options = array();

        $options['allowed_types'] = 'jpg|png|gif';
        $options['max_width'] = $width;
        $options['max_height'] = $height;
        $options['max_size'] = 0;
        $options['remove_spaces'] = TRUE;
        $options['encrypt_name'] = TRUE;
        $options['upload_path'] = create_path($upload_path);

        return $options;
    }
}

if (!function_exists('create_path')) {
    function create_path($file_path)
    {
        $real_path = realpath(dirname(getcwd())) . '/repository' . $file_path;
        return $real_path;
    }
}

if (!function_exists('create_uri')) {
    function create_uri($file_path)
    {
        $path = '/repository/merchant' . $file_path;
        return $path;
    }
}

if (!function_exists('do_upload')) {
    function do_upload($width, $height, $upload_path, $name)
    {
        $CI =& get_instance();
        $options = get_fileupload_options($width, $height, $upload_path);

        $CI->upload->initialize($options);

        if (!$CI->upload->do_upload($name)) {
            print $CI->upload->display_errors();
        }
        return $CI->upload->data();
    }
}