<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Rush\Citolaravel\Helpers\MigrationHelper;

class Users extends CI_Controller
{
	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
        $this->load->library(array('session','cpanel'));
		$this->load->model('m_users');

		if ($this->cpanel->checkpoint(2) == false) {
            redirect('../'.MigrationHelper::getOnboardingPrefix().'/sign-in');
		}
	}

	public function index() {
        $this->cpanel->load_view('UsersView',array(
        	'module' => 'users',
        	'users' => $this->m_users->getAllUsers(),
        	'roles' => $this->m_users->getRoles()
        ));
	}

	public function adduser() {
		if ($this->input->server('REQUEST_METHOD') == "POST") {

		}

		redirect('users','refresh');
	}
}