<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Rush\Citolaravel\Helpers\MigrationHelper;

class Dashboard extends CI_Controller
{
	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
        $this->load->library(array('session', 'cpanel'));
        $this->load->model(array("m_transactionhistory", "m_merchant", "m_reward"));

		if ($this->cpanel->checkpoint() == false) {
            redirect('../'.MigrationHelper::getOnboardingPrefix().'/sign-in');
		}
	}

	public function index() {
        // if forceQuickSetup
        if (!$this->session->userdata['forceQuickSetup']) {
            redirect('../'.MigrationHelper::getAppPrefix().'/analytics');
        }

        if ($this->cpanel->is_loyalty()) {
            $this->cpanel->load_view('DashboardLoyaltyView',array(
                'module' => 'dashboard',
                'current_date' => date('F j, Y', time()),
                'current_month_year' => date('F Y', time()),
                'count_daily_signups' => $this->m_merchant->getDailySignups($this->cpanel->getId()),
                'count_monthly_signups' => $this->m_merchant->getMonthlySignups($this->cpanel->getId()),
                'count_daily_points_earn' => $this->m_transactionhistory->getDailyPointsEarn($this->cpanel->getId()),
                'count_monthly_points_earn' => $this->m_transactionhistory->getMonthlyPointsEarn($this->cpanel->getId()),
                'count_daily_points_burn' => $this->m_transactionhistory->getDailyPointsBurn($this->cpanel->getId()),
                'count_monthly_points_burn' => $this->m_transactionhistory->getMonthlyPointsBurn($this->cpanel->getId())
            ));
        } else {
            $this->cpanel->load_view('DashboardView',array(
            	'module' => 'dashboard',
                'current_date' => date('F j, Y', time()),
                'current_month_year' => date('F Y', time()),
                'count_daily_signups' => $this->m_merchant->getDailySignups($this->cpanel->getId()),
            	'count_monthly_signups' => $this->m_merchant->getMonthlySignups($this->cpanel->getId()),
                'count_daily_redeemed' => $this->m_reward->getDailyRedeemed($this->cpanel->getId()),
            	'count_monthly_redeemed' => $this->m_reward->getMonthlyRedeemed($this->cpanel->getId()),
                'count_daily_transactions' => $this->m_transactionhistory->getDailyTransactions($this->cpanel->getId()),
            	'count_monthly_transactions' => $this->m_transactionhistory->getMonthlyTransactions($this->cpanel->getId())
            ));
        }
	}
}