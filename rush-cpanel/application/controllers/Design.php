<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Rush\Citolaravel\Helpers\MigrationHelper;

class Design extends CI_Controller
{
	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
        $this->load->library(array('session','cpanel','upload'));
		$this->load->model('m_design' ,'',TRUE);

		if ($this->cpanel->checkpoint() == false) {
            redirect('../'.MigrationHelper::getOnboardingPrefix().'/sign-in');
		}
	}

	public function index() {
		$buttons = array(
			0 => array(
				'id' => 'btnTablet',
				'label' => 'Tablet'
			),
			1 => array(
				'id' => 'btnCard',
				'label' => 'Card'
			),
			2 => array(
				'id' => 'btnMobile',
				'label' => 'Mobile'
			)
		);

		$palettes = array(
			0 => array(
				"name" => "Palette 1",
				"r" => "#222532",
				"g" => "#009573",
				"b" => "#8CE994"
			),
			1 => array(
				"name" => "Palette 2",
				"r" => "#43062E",
				"g" => "#922648",
				"b" => "#F90047"
			),
			2 => array(
				"name" => "Palette 3",
				"r" => "#2F2F2C",
				"g" => "#005B67",
				"b" => "#00A8A0"
			),
			3 => array(
				"name" => "Palette 4",
				"r" => "#FFF7EA",
				"g" => "#00ECFF",
				"b" => "#00CBD6"
			)
		);

		// $button_index = $this->cpanel->is_loyalty() ? 1 : 2;
		$button_index = 0;

		$merchant_app_data = $this->m_design->getSavedMerchantApplication($this->cpanel->getId());
		$card_design_data = $this->m_design->getSavedLoyaltyCardDesign($this->cpanel->getId());
		$customer_app_data = $this->m_design->getSavedCustomerAppDesign($this->cpanel->getId());
		$settings = $this->m_design->getMerchantSettings($this->cpanel->getId());

		$data = array(
        	'module' => 'design',
        	'design_button_id' => $buttons[$button_index]['id'],
        	'design_button_label' => $buttons[$button_index]['label'],
        	'palettes' => $palettes,

        	'merchant_app.logo' => $merchant_app_data['logo'],
        	'merchant_app.background' => $merchant_app_data['background'],
        	'merchant_app.stamp' => $merchant_app_data['stamp'],
        	'merchant_app.overlayColor' => $merchant_app_data['overlayColor'],
        	'merchant_app.textColor' => $merchant_app_data['textColor'],
        	'merchant_app.buttonsColor' => $merchant_app_data['buttonsColor'],

        	'card_design.logo' => $card_design_data['logo'],
        	'card_design.background' => $card_design_data['background'],
        	'card_design.cardbgcolor' => $card_design_data['cardbgcolor'],

        	'customer_app.logo' => $customer_app_data['logo'],
        	'customer_app.background' => $customer_app_data['background'],
        	'customer_app.stamp' => $customer_app_data['stamp'],
        	'customer_app.name' => $customer_app_data['name'],
         	'customer_app.overlayColor' => $customer_app_data['overlayColor'],
        	'customer_app.textColor' => $customer_app_data['textColor'],
        	'customer_app.buttonsColor' => $customer_app_data['buttonsColor'],
        	'settings.program_name' => isset($settings["program_name"]) ? $settings["program_name"] : '',
        	'message' => $this->session->flashdata('message') ? "<div class='alert alert-success' role='alert'><p>" . $this->session->flashdata('message') . "</p></div><style>.promo-list .new-punchcard{margin-bottom:-225px;}</style>" : null,
        	'merchantAppPalette' =>$this->load->view('templates/merchantAppPaletteTemplate', array('palettes' => $palettes), true)
        );

		$view = 'PunchcardDesignView';
		if ($this->cpanel->is_loyalty()) {
			$view = 'DesignView';
			$data['customerAppPalette'] = $this->load->view('templates/customerAppPaletteTemplate', array('palettes' => $palettes), true);
		}

        $this->cpanel->load_view($view, $data);
	}

	public function saveCardDesign() {
		$cardDesignData = array();
		$error = false;

		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			if ($this->has_upload_file('logo')) {
				if ($this->check_image_supported('logo')) {
				    $upload_data = $this->do_upload(200,200,'/merchant/logo','logo');
				    $cardDesignData['logoURL'] = $this->create_uri('/logo/' . $upload_data['file_name']);
			    } else {
			    	$error = true;
			    }
			}

			if ($this->has_upload_file('background')) {
	            if ($this->check_image_supported('background')) {
	   			    $upload_data = $this->do_upload(1136,640,'/merchant/background','background');
				    $cardDesignData['backgroundURL'] = $this->create_uri('/background/' . $upload_data['file_name']);
	            } else {
	            	$error = true;
	            }
	        }

            if ($error == false) {
				$cardbgcolor = $this->input->post("cardbgcolor",TRUE);
				$cardDesignData['backgroundColorHEX'] = $cardbgcolor;

				$this->m_design->storeDesignDetails($this->cpanel->getId(),$this->cpanel->getPackageId(),2,$cardDesignData);
				$this->session->set_flashdata('message', 'Design Added Successfully!');
            }
		}

		redirect('addons', 'refresh');
	}

	public function saveCustomerAppDesign() {
		$designData = array();

		$error = false;

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
        	if ($this->has_upload_file('logo')) {
	        	if ($this->check_image_supported('logo')) {
				    $upload_data = $this->do_upload(200,200,'/merchant/logo','logo');
				    $designData['logoURL'] = $this->create_uri('/logo/' . $upload_data['file_name']);
			    } else {
			    	$error = true;
			    }
			}

			if ($this->has_upload_file('background')) {
	            if ($this->check_image_supported('background')) {
				    $upload_data = $this->do_upload(640,1136,'/merchant/background','background');
				    $designData['backgroundURL'] = $this->create_uri('/background/' . $upload_data['file_name']);
			    } else {
			    	$error = true;
			    }
			}

			if ($this->has_upload_file('stamp')) {
	            if ($this->check_image_supported('stamp')) {
				    $upload_data = $this->do_upload(200,200,'/merchant/stamps','stamp');
				    $designData['stampURL'] = $this->create_uri('/stamps/' . $upload_data['file_name']);
			    } else {
			    	$error = true;
			    }
			}

		    if ($error == false) {
				$designData['merchantName'] = $this->input->post('merchantName',TRUE);

                $overlay = $this->input->post("overlayColor",TRUE);
				$designData['overlayColor'] = $overlay;

				$text = $this->input->post("textColor",TRUE);
				$designData['textColor'] = $text;

				$buttons = $this->input->post("buttonsColor",TRUE);
				$designData['buttonsColor'] = $buttons;

				$this->m_design->storeDesignDetails($this->cpanel->getId(),$this->cpanel->getPackageId(),3,$designData);
				$this->session->set_flashdata('message', 'Design Added Successfully!');
		    }
        }

	    redirect('design#mobile', 'refresh');
	}

	public function saveMerchantAppDesign() {
        $merchantAppData = array();
        $error = false;
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
        	if ($this->has_upload_file('logo')) {
	        	if ($this->check_image_supported('logo')) {
				    $upload_data = $this->do_upload(200,200,'/merchant/logo','logo');
				    $merchantAppData['logoURL'] = $this->create_uri('/logo/' . $upload_data['file_name']);
			    } else {
			    	$error = true;
			    }
			}

            if ($this->has_upload_file('background')) {
	            if ($this->check_image_supported('background')) {
				    $upload_data = $this->do_upload(1136,640,'/merchant/background','background');
				    $merchantAppData['backgroundURL'] = $this->create_uri('/background/' . $upload_data['file_name']);
			    } else {
			    	$error = true;
			    }
			}




            if ($error == false) {
				$text = $this->input->post("textColor",TRUE);
				$merchantAppData['textColor'] = $text;

				$buttons = $this->input->post("buttonsColor",TRUE);
				$merchantAppData['buttonsColor'] = $buttons;

				$this->m_design->storeDesignDetails($this->cpanel->getId(),$this->cpanel->getPackageId(),1,$merchantAppData);
				$this->session->set_flashdata('message', 'Design Added Successfully!');
            }
		    redirect('design#tablet', 'refresh');
        }
	}

	public function saveStampIcon()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$error = false;
			$stampURLC = null;
			$stampURLG = null;
			if ($this->has_upload_file('stamp')) {
	            if ($this->check_image_supported('stamp')) {
				    $upload_data = $this->do_upload(200,200,'/merchant/stamps','stamp');
				    $stampURL = $this->create_uri('/stamps/' . $upload_data['file_name']);
			    } else {
			    	$error = true;
			    }
			}
			if( $error == false) {
				if(isset($stampURL) && $stampURL){
					$this->m_design->updateMerchantStamp($this->cpanel->getId(), $stampURL);
				}
				$this->session->set_flashdata('message', 'Stamp successfully updated!');
				redirect('design#stamp-icon', 'refresh');
			}
		}
	}

	private function check_image_supported($tag) {
        if (isset($_FILES) && isset($_FILES[$tag]['tmp_name'])) {
        	$image_info = getimagesize($_FILES[$tag]['tmp_name']);
        	$mime_type = $image_info['mime'];
        	if (in_array($mime_type,array('image/jpeg','image/png'))) {
        		return true;
        	}
        }
        return false;
	}

	private function has_upload_file($tag) {
		if (isset($_FILES) && isset($_FILES[$tag]['tmp_name'])) {
		    if (!empty($_FILES[$tag]['tmp_name'])) {
		    	return true;
		    }
		}
		return false;
	}

	private function do_upload($width,$height,$upload_path,$name) {
		$options = $this->get_fileupload_options($width,$height,$upload_path);

		$this->upload->initialize($options);

		if (!$this->upload->do_upload($name)) {
            print $this->upload->display_errors();
		}
		return $this->upload->data();
	}

	private function create_path($file_path) {
		$real_path = realpath(dirname(getcwd())) . '/repository' . $file_path;
		return $real_path;
	}

	private function create_uri($file_path) {
		$path = '/repository/merchant' . $file_path;
		return $path;
	}

	private function get_fileupload_options($width,$height,$upload_path) {
		$options = array();

		$options['allowed_types'] = 'jpg|png';
		$options['max_width'] = $width;
		$options['max_height'] = $height;
		$options['max_size'] = 0;
		$options['remove_spaces'] = TRUE;
		$options['encrypt_name'] = TRUE;
		$options['upload_path'] = $this->create_path($upload_path);

		return $options;
	}
}