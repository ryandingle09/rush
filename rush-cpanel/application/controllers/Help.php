<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Rush\Citolaravel\Helpers\MigrationHelper;

class Help extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
        $this->load->library(array('session','cpanel'));
		$this->load->model('' ,'',TRUE);

		if ($this->cpanel->checkpoint() == false) {
            redirect('../'.MigrationHelper::getOnboardingPrefix().'/sign-in');
		}
	}

	public function index()
	{
        $this->cpanel->load_view('HelpView',array(
        	'module' => 'help',
            'helpPdf' => 'assets/pdf/RUSH_PRO_20161108.pdf'
        ));
	}
}