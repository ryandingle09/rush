<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Rush\Citolaravel\Helpers\MigrationHelper;

class Addons extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
        $this->load->library(array('session','cpanel'));
		$this->load->model('m_addons');
		$this->load->model('m_design');

		if ($this->cpanel->checkpoint() == false) {
            redirect('../'.MigrationHelper::getOnboardingPrefix().'/sign-in');
		}
	}

	public function index() {
		$card_design_data = $this->m_design->getSavedLoyaltyCardDesign($this->cpanel->getId());
		$this->cpanel->load_view('AddonViewLoyalty',array( // AddonView
			'module' => 'addons',
			'card_design.logo' => $card_design_data['logo'],
        	'card_design.background' => $card_design_data['background'],
        	'card_design.cardbgcolor' => $card_design_data['cardbgcolor'],

			'addons' => $this->m_addons->getAddons($this->cpanel->getId())
		));
	}

	public function addTablet() {
		$this->M_addons->addTablet($_POST);
		$this->session->set_flashdata('message', 'Tablet Added Successfully!');
		redirect('addons', 'refresh');
	}
}