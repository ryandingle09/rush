<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Rush\Citolaravel\Helpers\MigrationHelper;

class Logout extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
        $this->load->library('session');
	}

	public function index()
	{
		MigrationHelper::deleteSessionfile(getenv('SESSION_SAVE_PATH').'/'.session_id());
  	MigrationHelper::deleteSessionfile(getenv('SESSION_SAVE_PATH').'/sess_'.$_COOKIE["PHPSESSID"]);
  	$this->session->sess_destroy();
		$user_data['is_admin'] = FALSE;
		$this->session->set_userdata($user_data);

		// redirect('../'.MigrationHelper::getOnboardingPrefix().'/sign-in');
		redirect( MigrationHelper::getServerurl() );
	}
}
