<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Rush\Citolaravel\Helpers\MigrationHelper;

class Promos extends CI_Controller
{
	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
        $this->load->library(array('session','cpanel'));
		$this->load->model('m_promo');

		if ($this->cpanel->checkpoint() == false) {
            redirect('../'.MigrationHelper::getOnboardingPrefix().'/sign-in');
		}
	}

	public function index() {
		if ($this->cpanel->is_loyalty()) {
			$settings = $this->m_promo->getSavedConversionSettings($this->cpanel->getId(),$this->cpanel->getPackageId());
			$settings['earningPoints'] = $this->cpanel->createOptionsRange(0,20,$settings['earningPoints']);
			$settings['redemptionPoints'] = $this->cpanel->createOptionsRange(0,20,$settings['redemptionPoints']);
			$this->cpanel->load_view('PromoLoyaltyView',array(
				'module' => 'promos',
				'earningPointsAmount' => $settings['earningPointsAmount'],
				'earningPoints' => $settings['earningPoints'],
				'redemptionPoints' => $settings['redemptionPoints'],
				'redemptionPointsAmount' => $settings['redemptionPointsAmount']
			));
		} else {
			$promo_mechanics = $this->m_promo->getSavedPromoMechanics($this->cpanel->getId(),$this->cpanel->getPackageId());

	        $this->cpanel->load_view('PromoView',array(
	        	'module' => 'promos',
	        	'promos' => $promo_mechanics,
	        	'message' => $this->session->flashdata('message') ? "<div class='alert alert-success' role='alert'><p>" . $this->session->flashdata('message') . "</p></div><style>.promo-list .new-punchcard{margin-bottom:-225px;}</style>" : null,
	        	'errors' => $this->session->flashdata('errors') ? "<div class='alert alert-warning' role='alert'><p>" . $this->session->flashdata('errors') . "</p></div><style>.promo-list .new-punchcard{margin-bottom:-225px;}</style>" : null
	        ));
	    }
	}

	public function savePromoMechanics() 
	{
		// Validation
		$this->load->library('form_validation');

		$validation_rules = [
			[
				'field'	=> 'promoTitle',
				'label' => 'Title',
				'rules' => 'required'
			],
			[
				'field'	=> 'durationStart',
				'label' => 'Duration start date',
				'rules' => 'required'
			],
			[
				'field'	=> 'durationEnd',
				'label' => 'Duration end date',
				'rules' => 'required'
			],
			[
				'field'	=> 'redemptionDurationStart',
				'label' => 'Redemption start date',
				'rules' => 'required'
			],
			[
				'field'	=> 'redemptionDurationEnd',
				'label' => 'Redemption end date',
				'rules' => 'required'
			],
			[
				'field'	=> 'amount',
				'label' => 'Amount',
				'rules' => 'required|numeric|greater_than[0]'
			]
		];

		$stampsCount = $this->getStampCount();

		for($i = 1; $i <= $stampsCount; $i++) {
			if($this->input->post('stamp_reward_description_'. $i) || $_FILES['stamp_reward_image_'. $i]['tmp_name']) {
				if(!$this->input->post('stamp_reward_description_'. $i) ) {
				    $validation_rules[] = [
						'field'	=> 'stamp_reward_description_'. $i,
						'label' => 'Reward '. $i . " description",
						'rules' => 'required'
					];
				}

				if(!$_FILES['stamp_reward_image_'. $i]['tmp_name']) {
					$validation_rules[] = [
						'field'	=> 'stamp_reward_image_'. $i,
						'label' => 'Reward '. $i . " image",
						'rules' => 'required'
					];
				}
			}
		}
		$this->form_validation->set_rules($validation_rules);
		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('errors', validation_errors() );
			$this->session->set_flashdata('promoTitle',  $this->input->post('promoTitle'));
			$this->session->set_flashdata('durationStart',  $this->input->post('durationStart'));
			$this->session->set_flashdata('durationEnd',  $this->input->post('durationEnd'));
			$this->session->set_flashdata('redemptionDurationStart',  $this->input->post('redemptionDurationStart'));
			$this->session->set_flashdata('redemptionDurationEnd',  $this->input->post('redemptionDurationEnd'));
			$this->session->set_flashdata('amount',  $this->input->post('amount'));
			redirect('promos/add');
			exit;
		} else {
			if( $this->m_promo->isPromoDurationValid($this->cpanel->getId(), $this->input->post('durationStart'), $this->input->post('durationEnd') ) == false ) {
				$this->session->set_flashdata('errors', "<p>Duration date conflict!</p>" );
				$this->session->set_flashdata('promoTitle',  $this->input->post('promoTitle'));
				$this->session->set_flashdata('durationStart',  $this->input->post('durationStart'));
				$this->session->set_flashdata('durationEnd',  $this->input->post('durationEnd'));
				$this->session->set_flashdata('redemptionDurationStart',  $this->input->post('redemptionDurationStart'));
				$this->session->set_flashdata('redemptionDurationEnd',  $this->input->post('redemptionDurationEnd'));
				$this->session->set_flashdata('amount',  $this->input->post('amount'));
				redirect('promos/add');
				exit;
			}
		}
		$files = $this->upload_rewards_image($stampsCount);
		$rewards = array();
		if( $files ) {
			foreach($files as $key => $file) {
				$reward_no_stamps = $file['stamp_no'];
				$reward_description = $this->input->post('stamp_reward_description_'. $reward_no_stamps);
				$reward_image_url = $file['image']['file_name'];
				$rewards[] = array(
						'no_stamps'	=> $reward_no_stamps,
						'reward'	=> $reward_description,
						'image_url'	=> $this->config->item("respository_url") . "merchant/rewards/" . $reward_image_url
					);
			}
		}

		$title = $this->input->post('promoTitle');
		$duration_promo_start = $this->input->post('durationStart');
		$duration_promo_end = $this->input->post('durationEnd');
		$redemption_promo_start = $this->input->post('redemptionDurationStart');
		$redemption_promo_end = $this->input->post('redemptionDurationEnd');
		$amount = $this->input->post('amount');
		$status = $this->input->post('status');

		$punchcard_id = $this->m_promo->savePromoMechanics(
			$this->cpanel->getId(),
			$this->cpanel->getPackageId(),
			$status,
			$title,
			$stampsCount,
			$duration_promo_start,
			$duration_promo_end,
			$redemption_promo_start,
			$redemption_promo_end,
			$amount,
			$rewards
		);
		$this->session->set_flashdata('message', $title .' punchcard has been added!');
		redirect('promos','refresh');
	}

	public function saveLoyaltyPromoMechanics() {
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$earningPeso = $this->input->post('earningPointsAmount',TRUE);
			$earningPoints = $this->input->post('earningPoints',TRUE);
			$redemptionPoints = $this->input->post('redemptionPoints',TRUE);
			$redemptionPeso = $this->input->post('redemptionPointsAmount',TRUE);

            $this->m_promo->saveConversionSettings($this->cpanel->getId(),$this->cpanel->getPackageId(),$earningPeso,$earningPoints,$redemptionPoints,$redemptionPeso);

            $this->session->set_flashdata('message', 'Promo Mechanics Updated Successfully!');
		}
		redirect('promos','refresh');
	}

	public function add()
	{
		$this->cpanel->load_view('PromoAddView', array(
			'module' => 'promos',
	        'errors'  => $this->session->flashdata('errors') ? "<div class='alert alert-danger'>" . $this->session->flashdata('errors') . "</div>" : null,
	        'promoTitle' => $this->session->flashdata('promoTitle') ? $this->session->flashdata('promoTitle') : null,
	        'durationStart' => $this->session->flashdata('durationStart') ? $this->session->flashdata('durationStart') : null,
	        'durationEnd' => $this->session->flashdata('durationEnd') ? $this->session->flashdata('durationEnd') : null,
	        'redemptionDurationStart' => $this->session->flashdata('redemptionDurationStart') ? $this->session->flashdata('durationEnd') : null,
	        'redemptionDurationEnd' => $this->session->flashdata('redemptionDurationEnd') ? $this->session->flashdata('redemptionDurationEnd') : null,
	        'amount' => $this->session->flashdata('amount') ? $this->session->flashdata('amount') : null,
        	'promoAddViewStamps' => $this->load->view('PromoAddViewStamps', array('n' => 3), true),
            'addRemoveButton' => $this->getAddRemoveButton()
		));
	}

	public function upload_rewards_image($stampsCount = 9)
	{
		$config['upload_path'] = '../repository/merchant/rewards/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';

        $stamps = array();
        for ($i=1; $i <= $stampsCount; $i++) {
            $stamps[] = 'stamp_reward_image_'.$i;
        }

		$uploaded_files = [];
		$i = 1;
		foreach($stamps as $stamp) {
			$config['file_name'] = $this->cpanel->getId() . "-" . time();
			$this->load->library('upload', $config);
			if ( $this->upload->do_upload($stamp) ){
				$uploaded_files[$stamp]['image'] = $this->upload->data();
				$uploaded_files[$stamp]['stamp_no'] = $i;
			}
			$i++;
		}

		return $uploaded_files;
	}

	public function delete($promoId)
	{
		$promo = $this->m_promo->getPromoMechanics($promoId);
		if($promo) {
			if ($promo['status'] == 1) {
				$this->session->set_flashdata('errors', "<p>Deleting a published punchcard is not allowed!</p>" );
			} else {
				$this->m_promo->deletePromoMechanics($this->cpanel->getId(), $promoId);
				$this->session->set_flashdata('message', $promo['promo_title'] . ' punchcard successfuly deleted!');
			}
		}
		redirect('promos','refresh');
	}

	public function update($promoId)
	{
		$promo = $this->m_promo->getPromoMechanics($promoId);
		if($promo) {
			$view_data = array(
				'module' => 'promos',
				'promoId' => $promo['id'],
				'promo_title' => $promo['promo_title'],
				'duration_from_date' => $promo['duration_from_date'],
				'duration_to_date' => $promo['duration_to_date'],
				'redemption_duration_from_date' => $promo['redemption_duration_from_date'],
				'redemption_duration_to_date' => $promo['redemption_duration_to_date'],
				'amount' => $promo['amount'],
				'status' => $promo['status'],
				'errors'  => $this->session->flashdata('errors') ? "<div class='alert alert-danger'>" . $this->session->flashdata('errors') . "</div>" : null,
	        	'promoUpdateViewStamps' => $this->load->view('PromoUpdateViewStamps', array('n' => $promo['num_stamps']), true),
                'addRemoveButton' => ($promo['status'] == 1) ? '' : $this->getAddRemoveButton($promo['num_stamps'], $promo['multiplier'])
			);


			//initial reward data
			for($i = 1; $i <= $promo['num_stamps']; $i++ ) {
				$view_data['reward_image_canvas_'. $i] = null;
				$view_data['reward_img_class_'. $i] = null;
				$view_data['reward_stamp_class_'. $i] = null;
				$view_data['reward_description_' . $i] = null;
			}
			//set existing reward data
			foreach($promo['rewards'] as $reward) {
				$view_data['reward_image_canvas_' . $reward['no_stamps']] = '<canvas width="500" height="100" style="background-image:url('. $reward['image_url'] .');background-size:cover;"></canvas>';
				$view_data['reward_img_class_'. $reward['no_stamps']] = "with-img";
				$view_data['reward_stamp_class_'. $reward['no_stamps']] = "with-stamp";
				$view_data['reward_description_' . $reward['no_stamps']] = $reward['reward'];
			}
			$this->cpanel->load_view('PromoUpdateView', $view_data);
		} else {
			redirect('promos','refresh');
		}
	}

	public function updatePromoMechanics($promoId)
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$title = $this->input->post('promoTitle');
			$duration_promo_start = $this->input->post('durationStart');
			$duration_promo_end = $this->input->post('durationEnd');
			$redemption_promo_start = $this->input->post('redemptionDurationStart');
			$redemption_promo_end = $this->input->post('redemptionDurationEnd');
			$amount = $this->input->post('amount');
			$status = $this->input->post('status');

			try {
				$promo = $this->m_promo->getPromoMechanics($promoId);
				if ($promo['status'] == '1' && ($promo['status'] != $status)) {
					throw new Exception("A published punchcard cannot be set to draft!");
				}
				if (!$promo) {
					throw new Exception("Punchcard requested to update does not exist!");
				}
				if ($promo['duration_from_date'] != $duration_promo_start && $promo['status'] == 1) {
					throw new Exception("Promo Duration start cannot be edited when punchcard is already published!");
				}
				if ($duration_promo_end < $duration_promo_start) {
					throw new Exception("Invalid promo Duration range!");
				}
				if ($redemption_promo_start < $duration_promo_start) {
					throw new Exception("Redemption may only start on or after the promo Duration start!");
				}
				if ($redemption_promo_end < $redemption_promo_start) {
					throw new Exception("Invalid promo Redemption range!");
				}
				if ($redemption_promo_end < $duration_promo_end) {
					throw new Exception("Redemption end should be on or after the Duration end!");
				}
				if ($this->m_promo->isPromoDurationValid(
						$this->cpanel->getId(),
						$duration_promo_start,
						$duration_promo_end,
						$promoId
					) == false ) {
					throw new Exception("Duration date conflict!");
				}

                $stampsCount = $this->getStampCount();
				$files = $this->upload_rewards_image($stampsCount);
				$rewards_image_changed = array();

				// update promo info
				$this->m_promo->updatePromoMechanics(
					$this->cpanel->getId(),
					$promoId,
					$status,
					$title,
					$duration_promo_start,
					$duration_promo_end,
					$redemption_promo_start,
					$redemption_promo_end,
					$amount,
					$stampsCount
				);

				// updating rewards
				for($i = 1; $i <= $stampsCount; $i++) {
					$isRewardChanged = $this->input->post('stamp_reward_change'. $i);
					//if reward has been changed
					if( $isRewardChanged ) {
						//check if it has value
						$reward_description = $this->input->post('stamp_reward_description_'. $i);
						$reward_image_url   = isset($files['stamp_reward_image_'. $i]) ? $this->config->item("respository_url") . "merchant/rewards/" . $files['stamp_reward_image_'. $i]['image']['file_name'] : false;

						$reward = $this->m_promo->getPromoReward($promoId, $i);
						if( $reward_description || $reward_image_url ) {
							// update or add stamp
							if( $reward ) {
								// reward exist so update this reward
								$this->m_promo->updatePromoReward($promoId, $i, $reward_description, $reward_image_url);
							}else{
								// reward doesn't exist so add this reward
								$this->m_promo->addPromoReward($promoId, $i, $reward_description, $reward_image_url, $this->cpanel->getId());
							}
						} else {
							//remove stamp
							$this->m_promo->removePromoReward($promoId, $i);
						}
					}
				}

				// delete stamps beyond the stampsCount
				$this->m_promo->removePromoRewardBeyondStampsCounts($promoId, $stampsCount);

				$this->session->set_flashdata('message', $title .' punchcard has been updated!');
			} catch (Exception $e) {
				$this->session->set_flashdata('errors', "<p>{$e->getMessage()}</p>" );
			} finally {
				if ($this->session->has_userdata('errors') ){
					redirect('promos/update/'. $promoId);
				}
				redirect('promos','refresh');
			}
		}
	}

    public function getStampCount()
    {
        $stampsCount = 0;
        foreach ($_POST as $key => $value) {
            if (strpos($key, 'stamp_reward_description_') !== false) {
                $stampsCount++;
            }
        }

        return $stampsCount;
    }

    public function getAddRemoveButton($max = 15, $multiplier = 3)
    {
        //MTODO: move this to view once we are using Laravel views
        $addRemoveButton =
        ' 	<button type="button" class="btn btn-default btn-sm hide" id="removeStampsBtn">
            <span class="glyphicon glyphicon-minus-sign"></span> Remove %d Stamps
            </button>
            <button type="button" class="btn btn-default btn-sm hide" id="addStampsBtn">
            <span class="glyphicon glyphicon-plus-sign"></span> Add %d Stamps
            </button>
            <input type="hidden" name="stampsMax" id="stampsMax" value="%d"/>
            <input type="hidden" name="stampsMultiplier" id="stampsMultiplier" value="%d"/>
        ';

        return sprintf($addRemoveButton, $multiplier, $multiplier, $max, $multiplier);
    }
}