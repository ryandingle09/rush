<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Rush\Citolaravel\Helpers\MigrationHelper;

class Management extends CI_Controller
{
	public function __construct()
    {
		parent::__construct();
		$this->load->helper('url');
        $this->load->helper('image_helper');
        $this->load->library(array('session', 'cpanel', 'upload'));
		$this->load->model('m_management', '', TRUE);

		if ($this->cpanel->checkpoint() == false) {
            redirect('../'.MigrationHelper::getOnboardingPrefix().'/sign-in');
		}
	}

	public function index()
    {
		$merchant_id = $this->cpanel->getId();

        $params = array(
            'module' => 'management',
            'branches' => $this->m_management->getBranches($merchant_id),
            'employees' => $this->m_management->getEmployees($merchant_id),
            'customers' => $this->m_management->getCustomer($merchant_id),
            'reports' => $this->m_management->getReports($merchant_id),
            'branchQrCodeLink' => ''
        );

        if (in_array($merchant_id, explode('|', getenv('SHOW_BRANCH_QRCODE_LINK')))) {
            $params['branchQrCodeLink'] = '<a target="_BLANK" href="'.MigrationHelper::getAppBaseUrl().'branch-qrcode">Download Branch QR Code</a>';
        }

        if (!$this->cpanel->is_loyalty()) {
            $viewTemplate = 'PunchcardManagementView';
            $params['stamps'] = $this->m_management->getStamps($merchant_id);
        } else {
            $viewTemplate = 'ManagementView';
            $params['points'] = $this->m_management->getPoints($merchant_id);
        }

        $this->cpanel->load_view($viewTemplate, $params);
	}

	public function addbranch()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $name = $this->input->post('branchName',TRUE);
            $streetName = $this->input->post('streetName',TRUE);
            $district = $this->input->post('district',TRUE);
            $city = $this->input->post('city',TRUE);
            $zipcode = $this->input->post('zipcode',TRUE);
            $branchLogo = $this->getBranchLogo();

			$data = array(
				'branchName' => $name,
				'street_name' => $streetName,
				'district' => $district,
				'city' => $city,
				'zipcode' => $zipcode,
				'longitude' => 0,
				'latitude' => 0,
				'status' => 1,
				'merchantId' => $this->cpanel->getId()
			);
            if ($branchLogo) {
                $data['branch_logo'] = $branchLogo;
            }

			$this->m_management->addBranch($data);
			$this->session->set_flashdata('message', 'Branch Added Successfully!');
		}

		redirect('management#branch', 'refresh');
	}

	public function editbranch()
    {
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$name = $this->input->post('branchName',TRUE);
			$streetName = $this->input->post('streetName',TRUE);
			$district = $this->input->post('district',TRUE);
			$city = $this->input->post('city',TRUE);
			$zipcode = $this->input->post('zipcode',TRUE);
			$id = $this->input->post("id",TRUE);
            $branchLogo = $this->getBranchLogo();

			$data = array(
				'branchName' => $name,
				'street_name' => $streetName,
				'district' => $district,
				'city' => $city,
				'zipcode' => $zipcode
			);
            if ($branchLogo) {
                $this->deleteBranchLogo($id);
                $data['branch_logo'] = $branchLogo;
            }

			$this->m_management->updateBranch($this->cpanel->getId(),$id,$data);
			$this->session->set_flashdata('message', 'Branch Updated Successfully!');
		}

		redirect('management#branch', 'refresh');
	}

	public function deletebranch($id)
    {
        $this->deleteBranchLogo($id);
		$this->m_management->deleteBranch($this->cpanel->getId(), $id);
		$this->session->set_flashdata('message', 'Branch Deleted Successfully!');

		redirect('management#branch', 'refresh');
	}

	public function addEmployee() {
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$employeeName = $this->input->post('employeeName',TRUE);
            $branch = $this->input->post('selectBranch',TRUE);
            $fourDigit = $this->input->post('fourDigit',TRUE);
            $requirePassword = $this->input->post('requirePassword',TRUE);

        	$employeeData = array(
        		'employeeName' => $employeeName,
        		'branchId' => $branch,
        		'fourDigit' => $fourDigit,
        		'requirePassword' => ($requirePassword == "on" ? 1 : 0),
        		'merchantId' => $this->cpanel->getId()
        	);
			$this->m_management->addEmployee($employeeData);
			$this->session->set_flashdata('message', 'Employee Added Successfully!');

		}
		redirect('management#employee', 'refresh');
	}

	public function editEmployee() {
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$employeeName = $this->input->post('employeeName',TRUE);
            $branch = $this->input->post('selectBranch',TRUE);
            $fourDigit = $this->input->post('fourDigit',TRUE);
            $employeeId = $this->input->post('employeeId',TRUE);
            $requirePassword = $this->input->post('requirePassword',TRUE);

        	$employeeData = array(
        		'employeeName' => $employeeName,
        		'branchId' => $branch,
        		'requirePassword' => ($requirePassword == "on" ? 1 : 0),
        		'fourDigit' => $fourDigit
        	);
			$this->m_management->updateEmployee($this->cpanel->getId(),$employeeId,$employeeData);
			$this->session->set_flashdata('message', 'Employee Updated Successfully!');
		}
		redirect('management#employee', 'refresh');
	}

	public function deleteemployee($id) {
		$this->m_management->deleteEmployee($this->cpanel->getId(),$id);
		$this->session->set_flashdata('message', 'Employee Deleted Successfully!');
		redirect('management#employee', 'refresh');
	}

    public function setBranchDefaults(&$branches)
    {
        foreach($branches AS $key => &$value) {
            $value['branch_logo'] = $value['branch_logo'] ?: './assets/images/addImage.jpg';
        }
    }

    public function getBranchLogo()
    {
        $branchLogo = null;
        if (has_upload_file('branch_logo')) {
            if (check_image_supported('branch_logo')) {
                $this->createBranchPath();
                $upload_data = do_upload(200, 200, '/merchant/branch', 'branch_logo');
                $branchLogo = create_uri('/branch/' . $upload_data['file_name']);
            }
        }

        return $branchLogo;
    }

    public function createBranchPath()
    {
        //MTODO: this is temporary and uses repository's relative path
        // consider next design for AWS S3
        $branchPath = FCPATH.'../repository/merchant/branch';
        if(!is_dir($branchPath)) {
            mkdir($branchPath , 0777);
        }
    }

    public function deleteBranchLogo($branchId)
    {
        $branchLogo = $this->m_management->getBranchLogo($branchId);
        $branchLogoFile = FCPATH.'..'.$branchLogo;
        if (is_file($branchLogoFile))
            unlink($branchLogoFile);
    }

    private function create_uri($file_path) {
        $path = '/repository/merchant' . $file_path;
        return $path;
    }
}