<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Rush\Citolaravel\Helpers\MigrationHelper;

class Achievementunlock extends CI_Controller
{
	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
        $this->load->library(array('session','cpanel'));
        $this->load->model('m_achievements');

		if ($this->cpanel->checkpoint() == false) {
            redirect('../'.MigrationHelper::getOnboardingPrefix().'/sign-in');
		}
	}

	public function index() {
		$achievements = $this->m_achievements->getSavedAchievementsUnlock($this->cpanel->getId());
        $this->cpanel->load_view('SoonView',array(   // AchievementView
        	'module' => 'achievementunlock',
        	'achievements' => $achievements
        ));
	}

	public function addAchievements() {
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$description = $this->input->post("description",TRUE);
			$points = $this->input->post("points",TRUE);
			$multiplier = $this->input->post("multiplier",TRUE);
			$fromDate = $this->input->post("fromDate",TRUE);
			$toDate = $this->input->post("toDate",TRUE);

            $last = count($sharePoints);
			$index = 0;
			$achievements = array();
			while ($index < $last) {
				$achievement = array(
					'description' => $sharePoints[$index],
					'points' => $points[$index],
					'multiplier' => $multiplier[$index],
					'from_date' => $from[$index],
					'to_date' => $to[$index]
				);
				$achievements[] = $achievement;
				$index++;
			}

			$this->m_achievements->saveAchievements($this->cpanel->getId(),$achievements);
	    }

		redirect('achievementunlock','refresh');
	}
}