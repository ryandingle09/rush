<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

use Rush\Citolaravel\Helpers\MigrationHelper;

class Transactionhistory extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library(array('session', 'cpanel'));
        $this->load->model('m_transactionhistory', '', true);

        if ($this->cpanel->checkpoint() == false) {
            redirect('../'.MigrationHelper::getOnboardingPrefix().'/sign-in');
        }
    }

    public function index()
    {
        $transactions = array();
        if ($this->cpanel->is_admin()) {
            $transactions = $this->m_transactionhistory->getAllMerchantTransactions();
        } else {
            if (! $this->cpanel->is_loyalty()) {
                $transactions = $this->m_transactionhistory->getInAppTransactions($this->cpanel->getId());
            } else {
                $transactions = $this->m_transactionhistory->getMerchantTransactions($this->cpanel->getId());
            }
        }

        $view = 'TransactionLoyaltyView';
        if (! $this->cpanel->is_loyalty()) {
            $view = 'TransactionView';
        }


        $this->cpanel->load_view($view, array(
            'module' => 'transactionhistory',
            'transactions' => $transactions
        ));
    }
}
