<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Rush\Citolaravel\Helpers\MigrationHelper;

class Settings extends CI_Controller
{

	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
        $this->load->helper('image_helper');
        $this->load->library(array('session', 'cpanel', 'upload'));
        $this->load->model('m_quicksetup');
        $this->load->model('m_design');

		if ($this->cpanel->checkpoint() == false) {
            redirect('../'.MigrationHelper::getOnboardingPrefix().'/sign-in');
		}
	}

	public function index() {
		$app_details = $this->m_quicksetup->getSavedApplicationDetails($this->cpanel->getId());
		$settings = $this->m_quicksetup->getSavedProgramSettings($this->cpanel->getId());

        if ($this->cpanel->is_loyalty()) {
            $stampPointsName = 'Points';
        } else {
            $stampPointsName = 'Stamp';
        }
        $this->cpanel->load_view('SettingsView',array(
            'module' => 'settings',
            'programName' => $settings['programName'],
            'pointsName' => $settings['pointsName'],
            'appName' => $app_details['appName'],
            'appDescription' => $app_details['appDescription'],
            'logo' => $app_details['appLogo'],
            'stampPointsName' => $stampPointsName,
            'message' => $this->session->flashdata('message') ? "<div class='alert alert-success' role='alert'><p>" . $this->session->flashdata('message') . "</p></div>" : null,
        ));
	}

    public function saveAppStoreLogo()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $upload_error = false;
            $upload_data = null;

            if (has_upload_file('logo')) {
                if (check_image_supported('logo')) {
                    $upload_data = do_upload(1024, 1024, '/merchant/logo', 'logo');
                    $appLogo = create_uri('/logo/' . $upload_data['file_name']);
                } else {
                    $upload_error = true;
                }
            } else {
                $upload_error = true;
            }

            if (!$upload_error) {
                $this->m_design->updateAppStoreLogo($this->cpanel->getId(), $appLogo);

                $settings = $this->m_quicksetup->getSavedProgramSettings($this->cpanel->getId());
                $programName = ($settings['programName']) ?: 'Program Name';

                $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.gmail.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'smtpuser',
                    'smtp_pass' => 'smtppass',
                    'mailtype'  => 'html',
                    'charset'   => 'iso-8859-1'
                );
                //$this->load->library('email', $config);
                $this->load->library('email');
                $this->email->set_newline("\r\n");

                $this->email->from('noreply.info@rush.com.ph', 'Rush');
                $this->email->to('support@rush.ph');
                $this->email->cc('mmanalang@yondu.com, rmcoronel@yondu.com');
                $this->email->subject($programName . ': Change Appstore Logo');
                $this->email->set_mailtype("html");
                $this->email->message($this->load->view(
                    'templates/saveAppStoreLogoEmailTemplate',
                    array('programName' => $programName),
                    true
                ));
                $this->email->attach($upload_data['full_path']);
                $this->email->send();

                $this->session->set_flashdata('message', 'We have received your request to change your appstore logo.<br>You will receive email confirmation from us within 3-5 business days.');
            }
        }

        redirect('settings', 'refresh');
    }
}
