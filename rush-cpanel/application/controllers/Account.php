<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Rush\Citolaravel\Helpers\MigrationHelper;

class Account extends CI_Controller
{

	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
        $this->load->library(array('session','cpanel'));
        $this->load->model('m_users');

		if ($this->cpanel->checkpoint() == false) {
            redirect('../'.MigrationHelper::getOnboardingPrefix().'/sign-in');
		}
	}

	public function index() {
        $this->cpanel->load_view('AccountView',array(
            'module' => 'account'
        ));
	}

	public function changepass() {
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $old_password = $this->input->post('old_password',TRUE);
            $new_password = $this->input->post('new_password',TRUE);
            $confirm_password = $this->input->post('confirm_password',TRUE);

            $new_password = trim($new_password);
            $old_password = trim($old_password);
            $confirm_password = trim($confirm_password);

            $password = $this->cpanel->is_admin() ? $this->m_users->getUserPassword($this->cpanel->getId()) : $this->m_users->getMerchantPassword($this->cpanel->getId());
            if ($password != null && $password == hash('sha512',$old_password)) {
                if ($new_password == $confirm_password)  {
                	$new_password_length = strlen($new_password);
                	if ($new_password_length > 6) {
	                	if ($this->cpanel->is_admin()) {
	                	    $this->m_users->updateUserPassword($this->cpanel->getId(),hash('sha512',$new_password));
	                    } else {
	                    	$this->m_users->updateMerchantPassword($this->cpanel->getId(),hash('sha512',$new_password));
	                    }
	                	$this->session->set_flashdata('message', 'Password has been changed successfully!');

                        // trigger email alert changePassword
                        $laravelEventEndpoint = MigrationHelper::getAppBaseUrl().'alert/changePassword';
                        MigrationHelper::postCURL($laravelEventEndpoint, ['merchantId' => $this->cpanel->getId(), 'newPassword' => $new_password]);
                    } else {
                    	$this->session->set_flashdata('message','Supplied password is too short! Minimum password length is 6.');
                    }
                } else {
                	$this->session->set_flashdata('message', 'Error changing password! new password and confirm password does not match!');
                }
            } else {
            	$this->session->set_flashdata('message', 'Error changing password! old password does not match current password');
            }
		}

		redirect('account','refresh');
	}
}
