<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Rush\Citolaravel\Helpers\MigrationHelper;

class Redemption extends CI_Controller
{
	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
        $this->load->library(array('session','cpanel','upload'));
		$this->load->model('m_redemption' ,'',TRUE);

		if ($this->cpanel->checkpoint() == false) {
            redirect('../'.MigrationHelper::getOnboardingPrefix().'/sign-in');
		}
	}

	public function index() {
        $this->cpanel->load_view('RedemptionView', array(
        	'module' => 'redemption',
        	'redemptions' => $this->m_redemption->getRedemptions($this->cpanel->getId()),
            'redeemRowTemplate' => $this->load->view('templates/redeemRowTemplate', array(), true),
            'redeemRowEditTemplate' => $this->load->view('templates/redeemRowEditTemplate', array(), true)
        ));
	}

	public function save() {
		$reedemIds = $this->input->post('redeemId',TRUE);
		$productName = $this->input->post("productName",TRUE);
		$productDescription = $this->input->post("productDescription",TRUE);
		$points = $this->input->post("points",TRUE);

        $options = array();
		$options['allowed_types'] = 'jpg|png';
		$options['max_width'] = 350;
		$options['max_height'] = 200;
		$options['max_size'] = 0;
		$options['remove_spaces'] = TRUE;
		$options['encrypt_name'] = TRUE;
		$options['upload_path'] = realpath(dirname(getcwd())) . '/repository/merchant/product';

		$images[] = $_FILES['productImage'];
		$images = $images[0];

        $items = array();

        $last = count($productName);
        $index = 0;
        while ($index < $last) {
        	$item = array();
        	$item['redeemItemId'] = $reedemIds[$index];
        	$item['name'] = $productName[$index];
        	$item['details'] = $productDescription[$index];
        	$item['pointsRequired'] = $points[$index];
        	$item['timestamp'] = date("Y-m-d H:i:s");
        	$item['merchantid'] = $this->cpanel->getId();

            if ($images['name'][$index] != "") {
	        	$_FILES['productImage']['name'] = $images['name'][$index];
	        	$_FILES['productImage']['type'] = $images['type'][$index];
	        	$_FILES['productImage']['tmp_name'] = $images['tmp_name'][$index];
	        	$_FILES['productImage']['error'] = $images['error'][$index];
	        	$_FILES['productImage']['size'] = $images['size'][$index];

			    $upload_data = $this->do_upload($options,'productImage');
			    $item['imageURL'] = '/repository/merchant/product/' . $upload_data['file_name'];
	     	}
		    $item['status'] = 1;
		    $items[] = $item;
            $index++;
        }

        $this->m_redemption->saveItemForRedeem($this->cpanel->getId(),$items);

        $this->session->set_flashdata('message', 'Record has been updated!');

        redirect('redemption','refresh');
	}

	public function delete() {
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$id = $this->input->post('redeemId',TRUE);
			$this->m_redemption->deleteItemForRedeem($this->cpanel->getId(),$id);
		}
	}

	private function do_upload($options,$name) {
		$this->upload->initialize($options);

		if (!$this->upload->do_upload($name)) {
            print $this->upload->display_errors();
		}
		return $this->upload->data();
	}	
}