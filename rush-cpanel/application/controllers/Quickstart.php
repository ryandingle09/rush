<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Rush\Citolaravel\Helpers\MigrationHelper;

class Quickstart extends CI_Controller
{

	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
        $this->load->library(array('session','cpanel'));
        $this->load->model('m_quicksetup');

		if ($this->cpanel->checkpoint() == false) {
            redirect('../'.MigrationHelper::getOnboardingPrefix().'/sign-in');
		}
	}

	public function index() {
		$result = $this->m_quicksetup->getCompletionState($this->cpanel->getId());
        if ($result->num_rows() > 0) {
        	$result = $result->result_array();
        	if ($result[0]['completed'] < 100) {
        		$this->m_quicksetup->shouldLaunchQuicksetup($this->cpanel->getId());
        	}
        }
        redirect("dashboard","refresh");
	}
}