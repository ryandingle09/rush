<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Rush\Citolaravel\Helpers\MigrationHelper;

class Merchant extends CI_Controller
{
	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
        $this->load->library(array('session','cpanel'));
		$this->load->model('m_merchant');

		if ($this->cpanel->checkpoint(2) == false) {
            redirect('../'.MigrationHelper::getOnboardingPrefix().'/sign-in');
		}
	}

	public function index() {
        $this->cpanel->load_view('MerchantView',array(
        	'module' => 'merchant',
        	'merchants' => $this->m_merchant->getAllMerchants()
        ));
	}	
}