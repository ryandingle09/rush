<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Rush\Citolaravel\Helpers\MigrationHelper;

class Quicksetup extends CI_Controller
{

	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
        $this->load->library(array('session','cpanel','upload'));
        $this->load->model('m_design');
        $this->load->model('m_quicksetup');
        $this->load->model('m_promo');
        $this->load->model('m_merchant');

		if ($this->cpanel->checkpoint() == false) {
            redirect('../'.MigrationHelper::getOnboardingPrefix().'/sign-in');
		}
	}

	public function index() {
        // redirect("dashboard","refresh");
	}

    public function setupProgram() {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $programName = $this->input->post('programName',TRUE);
            $pointsName = $this->input->post('pointsName',TRUE);

            $this->m_quicksetup->saveProgramSettings($this->cpanel->getId(),$programName,$pointsName);
            $this->m_quicksetup->trackProgress($this->cpanel->getId(),1);
        }
    }

    public function saveCardDesign() {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $cardDesignData = array();
            $upload_error = false;
            if ($this->has_upload_file('logo')) {
                if ($this->check_image_supported('logo')) {
                    $upload_data = $this->do_upload(200,200,'/merchant/logo','logo');
                    $cardDesignData['logoURL'] = $this->create_uri('/logo/' . $upload_data['file_name']);
                } else {
                    $upload_error = true;
                }
            }

            if ($this->has_upload_file('background')) {
                if ($this->check_image_supported('background')) {
                    $upload_data = $this->do_upload(1136,640,'/merchant/background','background');
                    $cardDesignData['backgroundURL'] = $this->create_uri('/background/' . $upload_data['file_name']);
                } else {
                    $upload_error = true;
                }
            }

            if ($upload_error == false) {
                $cardbgcolor = $this->input->post("cardbgcolor",TRUE);
                $cardDesignData['backgroundColorHEX'] = $cardbgcolor;

                $this->m_design->storeDesignDetails($this->cpanel->getId(),$this->cpanel->getPackageId(),2,$cardDesignData);
            }

            $this->m_quicksetup->trackProgress($this->cpanel->getId(),2);
        }
    }

    public function saveCustomerAppDesign() {
        $designData = array();

        $error = false;

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            if ($this->has_upload_file('logo')) {
                if ($this->check_image_supported('logo')) {
                    $upload_data = $this->do_upload(200,200,'/merchant/logo','logo');
                    $designData['logoURL'] = $this->create_uri('/logo/' . $upload_data['file_name']);
                } else {
                    $error = true;
                }
            }

            if ($this->has_upload_file('background')) {
                if ($this->check_image_supported('background')) {
                    $upload_data = $this->do_upload(640,1136,'/merchant/background','background');
                    $designData['backgroundURL'] = $this->create_uri('/background/' . $upload_data['file_name']);
                } else {
                    $error = true;
                }
            }

            if ($this->has_upload_file('stamp')) {
                if ($this->check_image_supported('stamp')) {
                    $upload_data = $this->do_upload(200,200,'/merchant/stamps','stamp');
                    $designData['stampURL'] = $this->create_uri('/stamps/' . $upload_data['file_name']);
                } else {
                    $error = true;
                }
            }

            if ($error == false) {
                $designData['merchantName'] = $this->input->post('merchantName',TRUE);

                $overlay = $this->input->post("overlayColor",TRUE);
                $designData['overlayColor'] = $overlay;

                $text = $this->input->post("textColor",TRUE);
                $designData['textColor'] = $text;

                $buttons = $this->input->post("buttonsColor",TRUE);
                $designData['buttonsColor'] = $buttons;

                $this->m_design->storeDesignDetails(
                    $this->cpanel->getId(),
                    $this->cpanel->getPackageId(),
                    3,
                    $designData
                );

                // save the stamp for MerchantDesign
                if (isset($designData['stampURL'])) {
                    $this->m_design->storeDesignDetails(
                        $this->cpanel->getId(),
                        $this->cpanel->getPackageId(),
                        1,
                        ['stampURL' => $designData['stampURL']]
                    );
                }

                $this->m_quicksetup->trackProgress($this->cpanel->getId(),2);
            }
        }
    }

    public function saveMerchantAppDesign() {
        $merchantAppData = array();
        $upload_error = false;
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            if ($this->has_upload_file('logo')) {
                if ($this->check_image_supported('logo')) {
                    $upload_data = $this->do_upload(200,200,'/merchant/logo','logo');
                    $merchantAppData['logoURL'] = $this->create_uri('/logo/' . $upload_data['file_name']);
                } else {
                    $upload_error = true;
                }
            }

            if ($this->has_upload_file('background')) {
                if ($this->check_image_supported('background')) {
                    $upload_data = $this->do_upload(1136,640,'/merchant/background','background');
                    $merchantAppData['backgroundURL'] = $this->create_uri('/background/' . $upload_data['file_name']);
                } else {
                    $upload_error = true;
                }
            }

            if ($this->has_upload_file('stamp')) {
                if ($this->check_image_supported('stamp')) {
                    $upload_data = $this->do_upload(1136,640,'/merchant/stamps','stamp');
                    $merchantAppData['stampURL'] = $this->create_uri('/stamps/' . $upload_data['file_name']);
                } else {
                    $upload_error = true;
                }
            }

            if ($upload_error == false) {
                // $overlay = $this->input->post("overlayColor",TRUE);
                // $merchantAppData['overlayColor'] = $overlay;

                $text = $this->input->post("textColor",TRUE);
                $merchantAppData['textColor'] = $text;

                $buttons = $this->input->post("buttonsColor",TRUE);
                $merchantAppData['buttonsColor'] = $buttons;

                $this->m_design->storeDesignDetails($this->cpanel->getId(),$this->cpanel->getPackageId(),1,$merchantAppData);

                $this->m_quicksetup->trackProgress($this->cpanel->getId(),2);
            }
        }
    }

    public function saveApplicationDetails() {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $upload_error = false;

            $appName = $this->input->post("appName", TRUE);
            $appDescription = $this->input->post("appDescription", TRUE);
            $appLogo = "";

            if ($this->has_upload_file('logo')) {
                if ($this->check_image_supported('logo')) {
                    $upload_data = $this->do_upload(1024, 1024, '/merchant/logo', 'logo');
                    $appLogo = $this->create_uri('/logo/' . $upload_data['file_name']);
                } else {
                    $upload_error = true;
                }
            }

            $this->m_quicksetup->saveApplicationDetails($this->cpanel->getId(), $appName, $appDescription, $appLogo);
            $this->m_quicksetup->trackProgress($this->cpanel->getId(), 3);
            $this->m_merchant->clearQuicksetupLaunchFlag($this->cpanel->getId());

            // set forceQuickSetup to 0
            $this->session->set_userdata(['forceQuickSetup' => 0]);

            // remove laravel session file
            MigrationHelper::deleteSessionfile(getenv('SESSION_SAVE_PATH').'/'.session_id());

            // trigger email alert
            $laravelEventEndpoint = MigrationHelper::getAppBaseUrl().'alert/quickstartCompletion';
            MigrationHelper::postCURL($laravelEventEndpoint, ['merchantId' => $this->cpanel->getId()]);
        }
    }

    private function check_image_supported($tag) {
        if (isset($_FILES) && isset($_FILES[$tag]['tmp_name'])) {
            $image_info = getimagesize($_FILES[$tag]['tmp_name']);
            $mime_type = $image_info['mime'];
            if (in_array($mime_type,array('image/jpeg','image/png'))) {
                return true;
            }
        }
        return false;
    }

    private function has_upload_file($tag) {
        if (isset($_FILES) && isset($_FILES[$tag]['tmp_name'])) {
            if (!empty($_FILES[$tag]['tmp_name'])) {
                return true;
            }
        }
        return false;
    }

    private function convert_grayscale($filename) {
            $image_info = getimagesize($filename);
            $mime_type = $image_info['mime'];
            $image = $mime_type == 'image/png' ? imagecreatefrompng($filename) : imagecreatefromjpeg($filename);
            if ($image) {
                imagefilter($image, IMG_FILTER_GRAYSCALE);
                if ($mime_type == 'image/png') {
                    imagepng($image);
                } else {
                    imagejpeg($image);
                }
            }
            imagedestroy($image);
    }

    private function do_upload($width,$height,$upload_path,$name) {
        $options = $this->get_fileupload_options($width,$height,$upload_path);

        $this->upload->initialize($options);

        if (!$this->upload->do_upload($name)) {
            print $this->upload->display_errors();
        }
        return $this->upload->data();
    }

    private function create_path($file_path) {
        $real_path = realpath(dirname(getcwd())) . '/repository' . $file_path;
        return $real_path;
    }

    private function create_uri($file_path) {
        $path = '/repository/merchant' . $file_path;
        return $path;
    }

    private function get_fileupload_options($width,$height,$upload_path) {
        $options = array();

        $options['allowed_types'] = 'jpg|png';
        $options['max_width'] = $width;
        $options['max_height'] = $height;
        $options['max_size'] = 0;
        $options['remove_spaces'] = TRUE;
        $options['encrypt_name'] = TRUE;
        $options['upload_path'] = $this->create_path($upload_path);

        return $options;
    }
}