<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use Rush\Citolaravel\Helpers\MigrationHelper;

class Cpanel {
	protected $user_type;
	protected $user_id;
	protected $login_name;
	protected $package_id;
	protected $quickstart;
	protected $notify;
	protected $message;

	protected $view;

	const MERCHANT = 1;
	const SUPER_ADMIN = 2;

    const QUICKSTART_HIDE = 0;
	const QUICKSTART_SHOW = 1;
	const QUICKSTART_NOTIFICATION = 2;

    const PACKAGE_LOYALTY = 1;
	const PACKAGE_PUNCHCARD = 2;

	const LOYALTY_STEPS_COUNT = 3;
	const PUNCHCARD_STEPS_COUNT = 3;

	public function __construct() {
		$this->user_type = 0;
		$this->user_id = 0;
		$this->package_id = 0;
		$this->login_name = '';

		$this->quickstart = CPanel::QUICKSTART_HIDE;

		$ci = &get_instance();

		$ci->load->library('view');
		$ci->load->model('m_quicksetup');

		$this->view = $ci->view;
	}

	public function checkpoint($access_level=0) {
		$checkpoint_passed = false;

		$ci = &get_instance();
		if ($ci->session->userdata('user_id') && $ci->session->userdata('user_type')) {
            $user_id = $ci->session->userdata('user_id');
            $user_type = $ci->session->userdata('user_type');

            $user_id = intval($user_id);
            $user_type = intval($user_type);
            if ($user_id > 0) {
            	if (($user_type >= CPanel::MERCHANT && $user_type <= CPanel::SUPER_ADMIN)) {
            		$this->user_id = $user_id;
            		$this->user_type = $user_type;

            		if ($ci->session->userdata('login_name')) {
            			$this->login_name = filter_var($ci->session->userdata('login_name'),FILTER_SANITIZE_STRING);
            		}

            		if ($user_type == CPanel::MERCHANT) {
                        $ci->load->model('m_merchant');
                        $result = $ci->m_merchant->getMerchantData($this->user_id);
                        $merchant_data = count($result) > 0 ? $result[0] : array();

                        if ($this->login_name != $merchant_data["businessName"]) {
                        	$this->login_name = filter_var($merchant_data["businessName"],FILTER_SANITIZE_STRING);
                        	$ci->session->set_userdata('login_name',$this->login_name);
                        }

                        // debug only
                        // $merchant_data['isfirstlogin'] = 1;
                        // $merchant_data["packageid"] = 2;
                        // end debug only

                        $this->package_id = $merchant_data["packageid"];

                        if ($merchant_data['isfirstlogin'] == 1 || $merchant_data['shouldLaunchQuicksetup'] == 1) {
                        	if ($merchant_data['requireChangePassword'] == 1) {
                        		if (!(uri_string() == "welcome/changepass")) {
                                    redirect('welcome/changepass');
                                }
                        	} else {
	                        	$this->quickstart = CPanel::QUICKSTART_SHOW;
	                        	$num_steps = $this->is_loyalty() ? CPanel::LOYALTY_STEPS_COUNT : CPanel::PUNCHCARD_STEPS_COUNT;
	                        	$ci->m_quicksetup->createQuicksetupProgressTrack($this->user_id, $this->package_id, $num_steps);
	                        	$ci->m_merchant->clearFirstLoginFlag($this->user_id);
                                $checkpoint_passed = true;
                        	}
                        } else {
		                    if ($this->needsQuickSetup()) {
			                    $this->quickstart = CPanel::QUICKSTART_NOTIFICATION;
		                    }
                        }
            		}

            		if ($access_level > 0) {
            			if ($user_type == $access_level) {
            				$checkpoint_passed = true;
            			}
            		} else {
            		    $checkpoint_passed = true;
            	    }
            	}
            }
		}

		if ($checkpoint_passed == false) {
			$this->expunge();
		}
		return $checkpoint_passed;
	}

	public function is_admin() {
		return $this->user_type == CPanel::SUPER_ADMIN;
	}

	public function is_loyalty() {
		return $this->package_id == CPanel::PACKAGE_LOYALTY;
	}

	public function get_login_name() {
		return $this->login_name;
	}

	public function getId() {
		return $this->user_id;
	}

	public function getPackageId() {
		return $this->package_id;
	}

	public function load_view($view, $data=array()) {
        $ci = &get_instance();

		$buttons = array(
			0 => array(
				'id' => 'btnTablet',
				'label' => 'Tablet'
			),
			1 => array(
				'id' => 'btnCard',
				'label' => 'Card'
			),
			2 => array(
				'id' => 'btnMobile',
				'label' => 'Mobile'
			)
		);

		$palettes = array(
			0 => array(
				"name" => "Palette 1",
				"r" => "#222532",
				"g" => "#009573",
				"b" => "#8CE994"
			),
			1 => array(
				"name" => "Palette 2",
				"r" => "#43062E",
				"g" => "#922648",
				"b" => "#F90047"
			),
			2 => array(
				"name" => "Palette 3",
				"r" => "#2F2F2C",
				"g" => "#005B67",
				"b" => "#00A8A0"
			),
			3 => array(
				"name" => "Palette 4",
				"r" => "#FFF7EA",
				"g" => "#00ECFF",
				"b" => "#00CBD6"
			)
		);

        isset($data['merchantAppPalette']) ?: $data['merchantAppPalette'] = $ci->load->view('templates/merchantAppPaletteTemplate', array('palettes' => $palettes), true);
        isset($data['customerAppPalette']) ?: $data['customerAppPalette'] = $ci->load->view('templates/customerAppPaletteTemplate', array('palettes' => $palettes), true);

		$quickstart_view = 'include/quickstart/loyalty';
		$notification_view = 'include/notification';
		if ($this->is_admin()) {
			$menu = 'include/menu/admin';
		} else {
            if (!$this->is_loyalty()) {
            	$menu = array('template' =>
                    $ci->load->view(
                        MigrationHelper::getVendorViewPath().'menu/punchcard',
                        array('serverUrl' => MigrationHelper::getServerUrl(), 'baseUrl' => MigrationHelper::getBaseUrl()),
                        true
                    )
                );
                $quickstart_view = 'include/quickstart/punchcard';
            } else {
                $menu = array('template' =>
                    $ci->load->view(
                        MigrationHelper::getVendorViewPath().'menu/loyalty',
                        array('serverUrl' => MigrationHelper::getServerUrl(), 'baseUrl' => MigrationHelper::getBaseUrl()),
                        true
                    )
                );
            }
		}

		if ($this->quickstart == CPanel::QUICKSTART_NOTIFICATION) {
            $quickstart_view = $notification_view;

            $data['quicksetup_notification'] = 'Quickstart not yet completed. Click here to launch quickstart';
		} else {
			if ($this->quickstart != CPanel::QUICKSTART_SHOW) {
				$quickstart_view = null;
			}
		}

		$modal = "include/modal";
		if (!$this->hasFlashMessage()) {
            $modal = null;
		} else {
			$data['flash_data_type'] = 'flash_message_success';
		    $data['flash_message'] = $this->message;
		}

		$contents = $this->view->merge_views(array(
            'include/header',
            $menu,
            $view,
            $quickstart_view,
            $modal,
            'include/footer'
		));

		$data['base_url'] = MigrationHelper::getBaseUrl();
		$data['login_name'] = stripslashes($this->get_login_name());

		if ($this->quickstart == CPanel::QUICKSTART_SHOW) {
			$data['quickstart'] = $this->is_loyalty() ? "loyalty" : "punchcard";

			$ci = &get_instance();
			$ci->load->model('m_quicksetup');
			$ci->load->model('m_design');

			$settings = $ci->m_quicksetup->getSavedProgramSettings($this->getId());
			$this->push_array_data($settings,$data);

			$settings = $ci->m_quicksetup->getSavedApplicationDetails($this->getId());
			$this->push_array_data($settings,$data);

			if ($data['quickstart'] == 'loyalty') {
				$settings = $ci->m_design->getSavedMerchantApplication($this->getId());
				$this->push_array_data($settings,$data,'merchant_app');

			    $settings = $ci->m_design->getSavedCustomerAppDesign($this->getId());
			    $this->push_array_data($settings,$data,'customer_app');
			} else {
				$settings = $ci->m_design->getSavedMerchantApplication($this->getId());
				$this->push_array_data($settings,$data,'merchant_app');

			    $settings = $ci->m_design->getSavedCustomerAppDesign($this->getId());
			    $this->push_array_data($settings,$data,'customer_app');
			}
		} else {
			$data['quickstart'] = 'none';
		}

		$this->view->render($contents,$data);
	}

	public function createOptionsRange($start,$end,$current) {
		$data = array();
        $index = $start;
        while ($index < $end+1) {
        	$value = array(
        		'value' => ($index == 0) ? "0" : "$index",
        		'selected' => ($index == $current) ? 'selected' : ''
        	);
        	$data[] = $value;
        	$index++;
        }
        return $data;
	}

	public function needsQuickSetup() {
		$ci = &get_instance();
        $result = $ci->m_quicksetup->getCompletionState($this->getId());
        if ($result->num_rows() > 0) {
        	$result = $result->result_array();
        	if ($result[0]['completed'] >= 100) {
        		return false;
        	}
        }
        return true;
	}

	public function hasFlashMessage() {
		$ci = &get_instance();
		if ($ci->session->flashdata('message')) {
			$this->message = $ci->session->flashdata('message');
			return true;
		} else {
			return false;
		}
	}

	public function is_post() {
		$ci = &get_instance();
		if ($ci->input->server('REQUEST_METHOD') == 'POST') {
			return true;
		} else {
			return false;
		}
	}

	public function push_array_data($srcData,&$destData,$prefix='') {
		if ($prefix != '') {
			$prefix = $prefix . '.';
		}
		foreach ($srcData as $key => $value) {
			$destData[$prefix.$key] = $value;
		}
	}

	public function expunge() {
        $ci = &get_instance();
        $ci->session->sess_destroy();
	}
}