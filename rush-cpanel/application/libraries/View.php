<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use Rush\Citolaravel\Helpers\MigrationHelper;

class View {
    public function __construct() {
    }

	public function render($contents,$data=array()) {
        $patterns = array();
        $values = array();

        foreach ($data as $key => $value) {
            if (!is_array($value)) {
                $patterns[] = "/{{\s*$key\s*}}/";
                $values[] = $value;
            }       
        }

        preg_match_all("/{%\s*include_once\s+\'(.+)'\s*%}/siU",$contents,$map,PREG_PATTERN_ORDER);
        if (count($map) > 0) {
            array_shift($map);
            $pats = array();
            $vals = array();
            foreach ($map[0] as $item) {
                $vals[] = $this->get_view('include/'.$item,'');
                $item = preg_replace("/(\/)/","\\/",$item);  
                $pats[] = "/{%\s*include_once\s+\'$item'\s*%}/siU";
            }
            $contents = preg_replace($pats,$vals,$contents);
        }

        preg_match_all("/{%\s*foreach\s*(\w+)\s+as\s+(\w+)\s*%}(.*){%\s*endforeach\s*%}/siU",$contents,$map,PREG_PATTERN_ORDER);
        if (count($map) >= 2) {
            array_shift($map);            
            foreach ($map[0] as $index => $key) {
                if (isset($data[$key]) && is_array($data[$key])) {
                    $var = $map[1][$index];
                    $replacement = "";
                    foreach ($data[$key] as $item) {
                        $content = $map[2][$index];
                        
                        $pats = array();
                        $vals = array();
                        foreach ($item as $name => $val) {
                            $pats[] = "/{{\s*$var\[$name\]\s*}}/";
                            if ($val == null || $val == "") {
                                $vals[] = "";
                            } else {
                                $vals[] = $val;
                            }
                        }
                        $content = preg_replace($pats,$vals,$content);
                        $replacement .= $content;
                    }
                    $contents = preg_replace("/{%\s*foreach\s*$key\s+as\s+$var\s*%}(.*){%\s*endforeach\s*%}/siU",$replacement,$contents);
                }
            }            
        }

        if (count($patterns) > 0) {
        	$contents = preg_replace($patterns, $values, $contents);
        }        

        echo $contents;
	}

	public function merge_views($views=array()) {
        $contents = '';
        foreach ($views as $view) {
            if (isset($view['template'])) {
                $contents .= $view['template'];
            }
            else if ($view != null) {
        	    $contents .= $this->get_view($view);
            }
        }
        return $contents;
	}

	public function get_view($view,$ext='.php') {
		$file_path = '/'.MigrationHelper::getCpanelPrefix().'/application/views/' . $view;

		$file_path = realpath(dirname(getcwd())) . $file_path . $ext;
        if (is_file($file_path)) {
            return file_get_contents($file_path);
        }
	}
}