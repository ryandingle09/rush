/**
 * Custom jQuery's
 * Created by PhpStorm.
 * User: G.ELLURANGGO
 */


//-- fittext for responsive text
$("#sidemenu #menu ul li a").fitText(1 , { minFontSize: '8px', maxFontSize: '14px' });
$(".count li h3").fitText( 0.2, { minFontSize: '30px', maxFontSize: '65px' });
//-- end

// $(window).load(function() {
//     if(localStorage.getItem('quickstart') != 'shown'){
//         window.setTimeout(function(){
//             $('.punchcard').show();
//             window.location.hash = "#step=1";
//             $("body").css("overflow","hidden");
//         }, 1000);
//         localStorage.setItem('quickstart','shown')
//     }
//     if($('.quickstart').is(':visible')){
//     }
// });

$(document).ready(function() {
    $(document).activeNavigation("#menu");

    //-- make sidemenu height equals to windows height
    $("#sidemenu").height($(window).height());
    //-- end

    //-- tabs stay active on page load
    $('#tabs a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });
    $("ul.nav-tabs > li > a").on("shown.bs.tab", function (e) {
        var tabId = $(e.target).attr("href").substr(1);
        window.location.hash = tabId;
    });
    var hash = window.location.hash;
    $('#tabs a[href="' + hash + '"]').tab('show');
    //-- end

    //-- remove bg image of siblings tab on load
    $(".nav-tabs li.active").prev("li").children("a").css("background-image","none");
    //-- end

    //-- stay add button of active tab
    if (window.location.hash == "#branch"){
        addBranch();
    }
    if (window.location.hash == "#employee"){
        addEmployee();
    }
    if (window.location.hash == "#customer"){
        addCustomer();
    }
    //-- end

    //-- stay tablet or mobile in modafuking design module
    if (window.location.hash == "#tablet"){
        designTablet($('#btnTablet'));
    }
    if (window.location.hash == "#mobile"){
        designMobile($('#btnmobile'));
    }
    if (window.location.hash == "#card"){
        designCard($('#btnCard'));
    }
    //-- end

    //- stay addProduct in promos module
    if (window.location.hash == "#addProduct"){
        apProd($(".apProd"));
    }
    if (window.location.hash == "#addAmount"){
        apAmnt($(".apAmnt"));
    }
    //-- end

    //

});

//-- remove bg image of prev tab
$(document).on("click", ".management .nav-tabs li", function(){
    $(".nav-tabs li a").css("background-image","url('images/navtabsborder.jpg')");
    $(".nav-tabs li.active a").css("background-image","none");
    $(".nav-tabs li.active").prev("li").children("a").css("background-image","none");

});
//-- end

// change add button on tab click
$(document).on("click", ".management .nav-tabs li a", function(){
    var tabHref = $(this).attr("href");

    if (tabHref == "#branch"){
        addBranch()
    }
    if (tabHref == "#employee"){
        addEmployee();
    }
    if (tabHref == "#customer"){
        addCustomer();
    }
});
//-- end

//--center modal
function centerModal() {
    $(this).css('display', 'block');
    var $dialog = $(this).find(".modal-dialog");
    var offset = ($(window).height() - $dialog.height()) / 2;
    // Center modal vertically in window
    $dialog.css("margin-top", offset);
}
$('.modal').on('show.bs.modal', centerModal);
//-- end

//-- button effects like android L
var button = document.querySelectorAll('.btn');
for (var i = 0; i < button.length; i++) {
    button[i].onmousedown = function(e) {

        var x = (e.offsetX == undefined) ? e.layerX : e.offsetX;
        var y = (e.offsetY == undefined) ? e.layerY : e.offsetY;
        var effect = document.createElement('div');
        effect.className = 'effect';
        effect.style.top = y + 'px';
        effect.style.left = x + 'px';
        e.srcElement.appendChild(effect);
        setTimeout(function() {
            e.srcElement.removeChild(effect);
        }, 1100);
    }
}
//-- end

//-- input file type
$(document).on('change', '.btn-file :file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
});
//-- end

//-- design module ==MOBILE==
function upLogo(input) {
    var getImagePath = URL.createObjectURL(event.target.files[0]);
    $('.uploadLogo .imgPrev').attr('src', getImagePath);
    $('.customLogo').attr('src', getImagePath);
}

function upBg(input) {
    var getImagePath = URL.createObjectURL(event.target.files[0]);
    $('.addBackImage .imgPrev').attr('src', getImagePath);
    $('.customMobileBgImg').css('background-image', 'url(' + getImagePath + ')');
}

function upStampColored(input) {
    var getImagePath = URL.createObjectURL(event.target.files[0]);
    $('.upStampC .imgPrev').attr('src', getImagePath);
    $('.stampWrap .c').css('background-image','url(' + getImagePath + ')');

    $('.upStampG .imgPrev').attr('src', getImagePath);
    $('.stampWrap .g').css('background-image','url(' + getImagePath + ')');
}
function upStampGrayscale(input) {
    var getImagePath = URL.createObjectURL(event.target.files[0]);
    $('.upStampG .imgPrev').attr('src', getImagePath);
    $('.stampWrap .g').css('background-image','url(' + getImagePath + ')');
}

function rUpProdImg(input) {
    var getImagePath = URL.createObjectURL(event.target.files[0]);
    $('.rProdImg .imgPrev').attr('src', getImagePath);
    $('.rProdImg .imgPrev').css("display","block");

}


$(".uploadLogo .btn-file :file").change(function(){
    upLogo(this);
});
$(".addBackImage .btn-file :file").change(function(){
    upBg(this);
});
$(".upStampC .btn-file :file").change(function(){
    upStampColored(this);
});
$(".upStampG .btn-file :file").change(function(){
    upStampGrayscale(this);
});




// redeemed module add product
$(".rProdImg .btn-file :file").change(function(){
    // rUpProdImg(this);
    var getImagePath = URL.createObjectURL(event.target.files[0]);
    var prodImg = $(this).parent().siblings(".imgPrev");
    // console.log(prodImg);
    prodImg.attr('src', getImagePath);
    prodImg.css("display","block");
    prodImg.siblings(".btn-file").css("display","none");

});
//-- end

//-- design module ==CARD

function upCLogo(input) {
    var getImagePath = URL.createObjectURL(event.target.files[0]);
    $('.cardUploadLogo .imgPrev').attr('src', getImagePath);
    $('.cardLogo').attr('src', getImagePath);
}
function upCBgImage(input) {
    var getImagePath = URL.createObjectURL(event.target.files[0]);
    $('.cardAddBackImage .imgPrev').attr('src', getImagePath);
    $('.cardWrap').css('background-image','url(' + getImagePath + ')');
}
function cardBgColorPrev(){
    var rgbaCol = 'rgba(' + parseInt(cardBgHex.slice(-6,-4),16)
        + ',' + parseInt(cardBgHex.slice(-4,-2),16)
        + ',' + parseInt(cardBgHex.slice(-2),16)
        +',0.4)';
    $('.cardWrap .overlay').css('background-color', rgbaCol);
}

$(".cardUploadLogo .btn-file :file").change(function(){
    upCLogo(this);
});
$(".cardAddBackImage .btn-file :file").change(function(){
    upCBgImage(this);
});
//-- end

//-- design module ==MOBILE==
function tabletUpLogo(input) {
    var getImagePath = URL.createObjectURL(event.target.files[0]);
    $('.tabletUploadLogo .imgPrev').attr('src', getImagePath);
    $('.tabletCustomLogo').attr('src', getImagePath);
}

function tabletUpBg(input) {
    var getImagePath = URL.createObjectURL(event.target.files[0]);
    $('.tabletAddBackImage .imgPrev').attr('src', getImagePath);
    $('.tabletLayout ').css('background-image', 'url(' + getImagePath + ')');
}

function tabletUpStampColored(input) {
    var getImagePath = URL.createObjectURL(event.target.files[0]);

}
function tabletUpStampGrayscale(input) {
    var getImagePath = URL.createObjectURL(event.target.files[0]);
}

$(".tabletUploadLogo .btn-file :file").change(function(){
    tabletUpLogo(this);
});
$(".tabletAddBackImage .btn-file :file").change(function(){
    tabletUpBg(this);
});
$(".tabletUpStampC .btn-file :file").change(function(){
    tabletUpStampColored(this);
});
$(".tabletUpStampG .btn-file :file").change(function(){
    tabletUpStampGrayscale(this);
});

//-- end

//-- Design Palette
$(document).ready( function() {
    var bgColor1 = '#00A8A4';
    var bgColor2 = '#00E6C5';
    var bgColor3 = '#8BFF6F';

    $('span.p1,span.tp1, .custPalette i:first-child').css('background-color',bgColor1);
    $('span.p2,span.tp2, .custPalette i:nth-child(2)').css('background-color',bgColor2);
    $('span.p3,span.tp3, .custPalette i:last-child').css('background-color',bgColor3);

    $("#tpOverlay").val(bgColor1);
    $("#tpText").val(bgColor2);
    $("#tpButtons").val(bgColor3);

    $("#pOverlay").val(bgColor1);
    $("#pText").val(bgColor2);
    $("#pButtons").val(bgColor3);

    $('#topPaneLabel').css('color',bgColor2);
    $('#topPaneLabel').text('Merchant Name');
    $('.redeem').css('color',bgColor2);

    $('#cardBgColor').val($('#cardBackgroundColor').text());
});

$('.prePaletteMnu li').on('click', function(e) {

    getSelectedPalette($(this));
    overlayHex = bgColor1;
    buttonHex = bgColor3;
    textHex = bgColor2;
    bgColorPrev();
    buttonColorPrev();
    textColorPrev();
    if (window.location.hash == "#mobile") {
        changeColorPalette();
    } else if (window.location.hash == "#tablet") {
        tChangeColorPalette();
    }
});

function bgColorPrev(){
    var rgbaCol = 'rgba(' + parseInt(overlayHex.slice(-6,-4),16)
        + ',' + parseInt(overlayHex.slice(-4,-2),16)
        + ',' + parseInt(overlayHex.slice(-2),16)
        +',0.4)';
    if (window.location.hash == "#mobile") {
        $('.customMobileBgColor').css('background-color', rgbaCol);
        $('#topPaneLabel').css('background-color', overlayHex);
    }
    if (window.location.hash == "#tablet") {
        $('.customTablet').css('background-color',rgbaCol);
    }
}
function buttonColorPrev(){
    if (window.location.hash == "#mobile") {
        $('.redeem').css('background-color', buttonHex);
    }
    if (window.location.hash == "#tablet") {
        $('.customButton').css('background-color',buttonHex);
    }
}
function textColorPrev(){
    if (window.location.hash == "#mobile") {
        // $('.customText').css('color', textHex);
        $('#topPaneLabel').css('color',textHex);
        $('.redeem').css('color',textHex);        
    }
    if (window.location.hash == "#tablet") {

    }
}


$("#pOverlay").on('move.spectrum', function() {
    var hexcolor = $("#pOverlay").spectrum('get').toHexString();
    //alert(hexcolor);
    $(this).siblings('.hex').html(hexcolor);
    $('span.p1').css('background-color',hexcolor);
    overlayHex = hexcolor;
    bgColorPrev();
});
$("#pText").on('move.spectrum', function() {
    var hexcolor = $("#pText").spectrum('get').toHexString();
    $(this).siblings('.hex').html(hexcolor);
    $('span.p2').css('background-color',hexcolor);
    textHex = hexcolor;
    textColorPrev();
});
$("#pButtons").on('move.spectrum', function() {
    var hexcolor = $("#pButtons").spectrum('get').toHexString();
    $(this).siblings('.hex').html(hexcolor);
    $('span.p3').css('background-color',hexcolor);
    buttonHex = hexcolor;
    buttonColorPrev();
});
$("#cardBgColor").on('move.spectrum', function() {
    var hexcolor = $("#cardBgColor").spectrum('get').toHexString();
    $(this).siblings('.hex').html(hexcolor);
    cardBgHex = hexcolor;
    cardBgColorPrev();
});
$("#tpOverlay").on('move.spectrum', function() {
    var hexcolor = $("#tpOverlay").spectrum('get').toHexString();
    $(this).siblings('.hex').html(hexcolor);
    $('span.tp1').css('background-color',hexcolor);
    overlayHex = hexcolor;
    bgColorPrev();
});
$("#tpText").on('move.spectrum', function() {
    var hexcolor = $("#tpText").spectrum('get').toHexString();
    $(this).siblings('.hex').html(hexcolor);
    $('span.tp2').css('background-color',hexcolor);
    textHex = hexcolor;
    textColorPrev();
});
$("#tpButtons").on('move.spectrum', function() {
    var hexcolor = $("#tpButtons").spectrum('get').toHexString();
    $(this).siblings('.hex').html(hexcolor);
    $('span.tp3').css('background-color',hexcolor);
    buttonHex = hexcolor;
    buttonColorPrev();
});
$("#pqs_pOverlay").on('move.spectrum', function() {
    var hexcolor = $("#tpOverlay").spectrum('get').toHexString();
    $(this).siblings('.hex').html(hexcolor);
    $('span.pqs_p1').css('background-color',hexcolor);
    overlayHex = hexcolor;
    bgColorPrev();
});
$("#pqs_pText").on('move.spectrum', function() {
    var hexcolor = $("#tpText").spectrum('get').toHexString();
    $(this).siblings('.hex').html(hexcolor);
    $('span.pqs_p2').css('background-color',hexcolor);
    textHex = hexcolor;
    textColorPrev();
});
$("#pqs_pButtons").on('move.spectrum', function() {
    var hexcolor = $("#tpButtons").spectrum('get').toHexString();
    $(this).siblings('.hex').html(hexcolor);
    $('span.pqs_p3').css('background-color',hexcolor);
    buttonHex = hexcolor;
    buttonColorPrev();
});
$("#pOverlay").spectrum({
    color: "#00A8A4",
    preferredFormat: "hex",
    showInput: true,
    showButtons: true,
    chooseText: "Select",
    cancelText: "Cancel"
});
$("#pText").spectrum({
    color: "#00E6C5",
    preferredFormat: "hex",
    showInput: true,
    showButtons: true,
    chooseText: "Select",
    cancelText: "Cancel"
});
$("#pButtons").spectrum({
    color: "#8BFF6F",
    preferredFormat: "hex",
    showInput: true,
    showButtons: true,
    chooseText: "Select",
    cancelText: "Cancel"
});

$("#cardBgColor").spectrum({
    color: "#E2E0DC",
    preferredFormat: "hex",
    showInput: true,
    showButtons: true,
    chooseText: "Select",
    cancelText: "Cancel"
});
$("#tpOverlay").spectrum({
    color: "#00A8A4",
    preferredFormat: "hex",
    showInput: true,
    showButtons: true,
    chooseText: "Select",
    cancelText: "Cancel"
});
$("#tpText").spectrum({
    color: "#00E6C5",
    preferredFormat: "hex",
    showInput: true,
    showButtons: true,
    chooseText: "Select",
    cancelText: "Cancel"
});
$("#tpButtons").spectrum({
    color: "#8BFF6F",
    preferredFormat: "hex",
    showInput: true,
    showButtons: true,
    chooseText: "Select",
    cancelText: "Cancel"
});

$("#pqs_pOverlay").spectrum({
    color: "#00A8A4",
    preferredFormat: "hex",
    showInput: true,
    showButtons: true,
    chooseText: "Select",
    cancelText: "Cancel"
});
$("#pqs_pText").spectrum({
    color: "#00E6C5",
    preferredFormat: "hex",
    showInput: true,
    showButtons: true,
    chooseText: "Select",
    cancelText: "Cancel"
});
$("pqs_pButtons").spectrum({
    color: "#8BFF6F",
    preferredFormat: "hex",
    showInput: true,
    showButtons: true,
    chooseText: "Select",
    cancelText: "Cancel"
});
//-- end

//Design tablet or mobile

$("#btnTablet").on('click',function(){
    designTablet($(this));
    window.location.hash = "tablet";
});
$("#btnCard").on('click',function(){
    designCard($(this));
    window.location.hash = "card";
});
$("#btnMobile").on('click',function(){
    designMobile($(this));
    window.location.hash = "mobile";
});

// bootstrap date time picker
$('#datetimepicker').datetimepicker({
    format: 'MM/DD/YYYY'
});

$('#datetimepicker2').datetimepicker({
    format: 'MM/DD/YYYY'
});

$('#promoDurationStart').datetimepicker({
    format: 'MM/DD/YYYY'
});
$('#promoDurationEnd').datetimepicker({
    format: 'MM/DD/YYYY'
});



// Promos amount or product

$(".apProd").on('click',function(){
    apProd($(this));
});
$(".apAmnt").on('click',function(){
    apAmnt($(this));
});

$(document).ready(function() {
    $('.skipQuickStart').on('click',function(){
        $('.punchcard').fadeOut();
        $('.loyalty').fadeOut();
        $("body").css("overflow","auto");
    });

    $('.qsOne .nextStep').on('click', function () {
        qsNextBack($(this));
        window.location.hash = "#step=2";
        $('.pc1').removeClass('active').addClass('done');
        $('.pc2').addClass('active');
        $('.qsOne').hide();
        $('.qsTwo').show();
    });
    $('.qsTwo .nextStep').on('click',function(){
        qsNextBack($(this));
        window.location.hash = "#step=3";
        $('.pc2').removeClass('active').addClass('done');
        $('.pc3').addClass('active');
        $('.qsTwo').hide();
        $('.qsThree').show();        
    });
    $('.qsThree .nextStep').on('click',function(){
        qsNextBack($(this));
        window.location.hash = "#step=4";
        $('.pc3').removeClass('active').addClass('done');
        $('.pc4').addClass('active');
        $('.qsThree').hide();
        $('.qsFour').show();        
    });
    $('.qsFour .nextStep').on('click',function(){
        qsNextBack($(this));
        window.location.hash = "#step=5";
        $('.pc4').removeClass('active').addClass('done');
        $('.pc5').addClass('active');
        $('.qsFour').hide();
        $('.qsFive').show();        
    });
    $('.qsFive .nextStep').on('click',function(){
        qsNextBack($(this));
        window.location.hash = "#step=6";
        $('.pc5').removeClass('active').addClass('done');
        $('.pc6').addClass('active');
        $('.qsFive').hide();
        $('.qsSix').show();        
    });
    $('.qsSix .nextStep').on('click',function(){
        qsNextBack($(this));
        window.location.hash = "#step=6";
        $('.pc6').removeClass('active').addClass('done');
        $('.pc7').addClass('active');
        $('.qsSix').hide();        
    });
	
	$('.qsOne .backStep').on('click', function () {
        qsBackNext($(this));
        window.location.hash = "#step=1";
        $('.pc2').removeClass('active').addClass('done');
        $('.pc1').addClass('active');
    });
    $('.qsTwo .backStep').on('click',function(){
        qsBackNext($(this));
        window.location.hash = "#step=2";
        $('.pc3').removeClass('active').addClass('done');
        $('.pc2').addClass('active');
    });
    $('.qsThree .backStep').on('click',function(){
        qsBackNext($(this));
        window.location.hash = "#step=3";
        $('.pc4').removeClass('active').addClass('done');
        $('.pc3').addClass('active');
    });
    $('.qsFour .backStep').on('click',function(){
        qsBackNext($(this));
        window.location.hash = "#step=4";
        $('.pc5').removeClass('active').addClass('done');
        $('.pc4').addClass('active'); 
    });
    $('.qsFive .backStep').on('click',function(){
        qsBackNext($(this));
        window.location.hash = "#step=5";
        $('.pc6').removeClass('active').addClass('done');
        $('.pc5').addClass('active');
    });
    $('.qsSix .backStep').on('click',function(){
        qsBackNext($(this));
        window.location.hash = "#step=6";
        $('.pc7').removeClass('active').addClass('done');
        $('.pc6').addClass('active');
    });

    $('#noOfStamps').on('change',function() {
        $("#stampCount").text($(this).val());
    });

    $('.branchEdit').on('click',function() {
        var $row = $(this).closest('tr');
        var $cell = $row.find('td');
        $("#ebranchId").val($row.data('id'));
        $("#ebranchName").val($cell.eq(0).text());
        $("#ebranchAddress").val($cell.eq(1).text());
    });

    $('.branchDelete').on('click',function() {
        var $row = $(this).closest('tr');
        var $cell = $row.find('td');
        $("#branchToDelete").text($cell.eq(1).text());
        $("#deleteBranchForm").attr('action',$(this).data('action'));
    });

    $('.employeeEdit').on('click',function() {
        var $row = $(this).closest('tr');
        $("#eEmployeeId").val($row.data('id'));
        $("#eFirstName").val($row.data('firstname'));
        $("#eLastName").val($row.data('lastname'));
        $("#eAddress").val($row.data('address'));
        $("#eMobileNumber").val($row.data('mobile'));
        $("#ePhoneNumber").val($row.data('phone'));
        $("#eEmail").val($row.data("email"));
        $("#selectBranch select").val($row.data('branch'));
    });

    $('.employeeDelete').on('click',function() {
        var $row = $(this).closest('tr');
        var $cell = $row.find('td');
        $("#employeeToDelete").text($cell.eq(0).text());
        $("#deleteEmployeeForm").attr('action',$(this).data('action'));
    });

    $('.smsDelete').on('click',function() {
        var $row = $(this).closest('tr');
        var $cell = $row.find('td');
        $("#smsToDelete").text($cell.eq(0).text());
        $("#deleteSmsForm").attr('action', $(this).data('action'));
    });

    $('.pushDelete').on('click',function() {
        var $row = $(this).closest('tr');
        var $cell = $row.find('td');
        $("#notifToDelete").text($cell.eq(0).text());
        $("#deleteNotifForm").attr('action', $(this).data('action'));
    });

    $('.emailDelete').on('click',function() {
        var $row = $(this).closest('tr');
        var $cell = $row.find('td');
        $("#emailToDelete").text($cell.eq(0).text());
        $("#deleteEmailForm").attr('action', $(this).data('action'));
    });

    $('.segmentDelete').on('click',function() {
        var $row = $(this).closest('tr');
        var $cell = $row.find('td');
        $("#segmentToDelete").text($cell.eq(0).text());
        $("#deleteSegmentForm").attr('action', $(this).data('action'));
    });

    $('.eventAdd').on('click',function() {
        var $row = $(this).closest('tr');
        var $cell = $row.find('td');
        $("#addEventForm").attr('action', $(this).data('action'));
    });

    var achivement_number = 1;

    $(".addAchievement").on('click',function(e) {
        var cloned = $('#achivement-row').clone();
        cloned.find('.number').text(""+(++achivement_number));
        cloned.find('.remove-achivement').on('click',function() {
            cloned.remove();
            achivement_number--;
        });
        cloned.find("#sharePoints").val("");
        cloned.find("#points").val("0");
        cloned.find("#aFrom").val("");
        cloned.find("#aTo").val("");
        cloned.appendTo('#achivements-row');
    });

    var raw_html_data = '<div class="itemsRedeemed"><div class="col-md-1"><div class="input-group"><span class="input-group-addon"><i class="number">0</i></span></div></div><div class="col-md-2"><div class="input-group"><input type="text" class="form-control rProdName" name="productName[]" id="rProdName" placeholder="e.g: Shampoo 150ml"></div></div><div class="col-md-2"><div class="input-group rProdImg"><img class="imgPrev" alt="" src="assets/images/addImage.jpg"><span class="btn btn-default btn-file">Browse <input type="file" name="productImage[]"></span></div></div><div class="col-md-3 "><div class="input-group"><textarea name="productDescription[]" rows="8" cols="40" class="form-control rDescription" id="rDescription"></textarea></div></div><div class="col-md-2 "><div class="input-group"><input type="text" class="form-control rPoints" name="points[]" id="rPoints"><span class="input-group-addon pts">pts</span></div></div><div class="col-md-2"><a href="" onclick="return false"><i class="fa edit fa-pencil edit-redeem"></i></a><a href="" onclick="return false"><i class="fa del fa-times remove-redeem"></i></a></div><input type="hidden" name="redeemId[]" class="rRedeemId" id="rRedeemId" value=""></div>';

    var redeem_number = 1;
    if ($('.itemsRedeemed').length == 1) {
        $('#redeem-rows').append(raw_html_data);
        var $row = $('.itemsRedeemed:last-child');
        $row.find('.number').text(""+($('.itemsRedeemed').length-1));
        $row.find('.remove-redeem').on('click',function() {
            $row.remove();
            $('.itemsRedeemed').each(function(index) {
                $(this).find('.number').text(index);
            });
        });
        $row.find('.edit-redeem').css('display','none');
        $row.find(".rProdName").val("");
        $row.find(".rDescription").val("");
        $row.find(".rPoints").val("0");
        $row.find(".rRedeemId").val("0");
        $row.find('.rProdImg .btn-file :file').change(function() {
            var file = $(this)[0].files[0];
            var getImagePath = URL.createObjectURL(file);
            var prodImg = $row.find('.imgPrev');
            prodImg.attr('src', getImagePath);
            prodImg.css("display","block");
            prodImg.siblings(".btn-file").css("display","none");
        });
    }

    $('.itemsRedeemed').each(function(index) {
        var $row = $(this);
        $row.find('.number').text(index);
        if (index > 1) {
            $row.find('.remove-redeem').on('click',function() {
                var id = $row.find('.rRedeemId').val();
                $.post(window.location.href+"/delete",{redeemId: id},function (data,status) {
                    $row.remove();
                    $('.itemsRedeemed').each(function(index) {
                        $(this).find('.number').text(index);
                    });                    
                });
            });
        }

        $(this).find('.edit-redeem').on('click',function() {
            var productName = $row.find('input[name="productName\[\]"]').val();
            var productDescription = $row.find('input[name="productDescription\[\]"]').val();
            var points = $row.find('input[name="points\[\]"]').val();
            var redeemId = $row.find('input[name="redeemId\[\]"]').val();
            var number = $row.find('.number').text();

            $row.html('<div class="col-md-1"><div class="input-group"><span class="input-group-addon"><i class="number">0</i></span></div></div><div class="col-md-2"><div class="input-group"><input type="text" class="form-control rProdName" name="productName[]" id="rProdName" placeholder="e.g: Shampoo 150ml"></div></div><div class="col-md-2"><div class="input-group rProdImg"><img class="imgPrev" alt="" src="assets/images/addImage.jpg"><span class="btn btn-default btn-file">Browse <input type="file" name="productImage[]"></span></div></div><div class="col-md-3 "><div class="input-group"><textarea name="productDescription[]" rows="8" cols="40" class="form-control rDescription" id="rDescription"></textarea></div></div><div class="col-md-2 "><div class="input-group"><input type="text" class="form-control rPoints" name="points[]" id="rPoints"><span class="input-group-addon pts">pts</span></div></div><div class="col-md-2"><a href="" onclick="return false"><i class="fa edit fa-pencil edit-redeem"></i></a><a href="" onclick="return false"><i class="fa del fa-times remove-redeem"></i></a></div><input type="hidden" name="redeemId[]" class="rRedeemId" id="rRedeemId" value="">');
            
            $row.find('input[name="productName\[\]"]').val(productName);
            $row.find('textarea[name="productDescription\[\]"]').text(productDescription);
            $row.find('input[name="points\[\]"]').val(points);
            $row.find('input[name="redeemId\[\]"]').val(redeemId);

            $row.find('.number').text(number);

            $row.find('.edit-redeem').css('display','none');

            $row.find('.remove-redeem').on('click',function() {
                $row.remove();
                var id = $row.find('.rRedeemId').val();
                $.post(window.location.href+"/delete",{redeemId: id},function (data,status) {});
                $('.itemsRedeemed').each(function(index) {
                    $(this).find('.number').text(index);
                });
            });
        });

        var $imgPrev = $('.rProdImg .imgPrev');
        if ($imgPrev.attr('src') == 'assets/images/addImage.jpg') {
            $imgPrev.css("display","none");
            $imgPrev.siblings(".btn-file").css("display","block");
        } else {
            $imgPrev.css("display","block");
            $imgPrev.siblings(".btn-file").css("display","none");            
        }
    });

    $(".addRedeemItem").on('click',function(e) {
        $('#redeem-rows').append(raw_html_data);
        var $row = $('.itemsRedeemed:last-child');
        $row.find('.number').text(""+($('.itemsRedeemed').length-1));
        $row.find('.remove-redeem').on('click',function() {
            $row.remove();
            $('.itemsRedeemed').each(function(index) {
                $(this).find('.number').text(index);
            });
        });
        $row.find('.edit-redeem').css('display','none');
        $row.find(".rProdName").val("");
        $row.find(".rDescription").val("");
        $row.find(".rPoints").val("0");
        $row.find(".rRedeemId").val("0");
        $row.find('.rProdImg .btn-file :file').change(function() {
            var file = $(this)[0].files[0];
            var getImagePath = URL.createObjectURL(file);
            var prodImg = $row.find('.imgPrev');
            prodImg.attr('src', getImagePath);
            prodImg.css("display","block");
            prodImg.siblings(".btn-file").css("display","none");
        });        
    });

    var customerAppPaletteCount = 1;

    $('#saveCustomerAppPalette').on('click',function() {
        var cloned = $('#customerAppPalette').find("li:last").clone(true);
        cloned.find("span").text("Custom Palette " + (customerAppPaletteCount++));

        cloned.find('i:first-child').css('background-color',$('#cThemeVal').text());
        cloned.find('i:nth-child(2)').css('background-color',$('#cTextVal').text());
        cloned.find('i:last-child').css('background-color',$('#cButtonsVal').text());

        cloned.appendTo('#customerAppPalette');

        console.log($('#cThemeVal').text());
        console.log($('#cTextVal').text());
        console.log($('#cButtonsVal').text());
    });

    var merchantAppPaletteCount = 1;

    $('#saveMerchantAppPalette').on('click',function() {
        var cloned = $('#merchantAppPalette').find("li:last").clone(true);
        cloned.find("span").text("Custom Palette " + (merchantAppPaletteCount++));

        cloned.find('i:first-child').css('background-color',$('#mThemeVal').text());
        cloned.find('i:nth-child(2)').css('background-color',$('#mTextVal').text());
        cloned.find('i:last-child').css('background-color',$('#mButtonsVal').text());

        cloned.appendTo('#merchantAppPalette');
    });

    $('.addReward').on('click',function() {
    });

    $(document).on("click", "#notes a", function(){
        $("#notes").slideUp("slow");
    });

    $(document).on("click", "#flash_message_success a", function(){
        $("#flash_message_success").slideUp("slow");
    });

    $(document).on("click", "#flash_message_error a", function(){
        $("#flash_message_success").slideUp("slow");
    });

    $(document).on("click", "#stamps ul li a", function(){
        $(this).next(".inputHere").slideToggle( "fast" );

    });
    $(document).on("click", "#stamps .btn", function(){
        $(this).parent().siblings("a").addClass("bordered");
        $(this).parent().siblings("a").next(".inputHere").hide();
    });

    $('#merchantName').on('keyup',function() {
        var value = $(this).val();
        if (value == "") {
            value = "Merchant Name";
        }
        $('.topPane').text(value);
    });
});
