/**
 * Custom jQuery's
 * Created by PhpStorm.
 * User: G.ELLURANGGO
 */



//--Global Variable
var addNew = $('.navTabsWrap .addNew');
var overlayHex;
var textHex;
var buttonHex;
var mLogo;
var mbgImg;
var mTheme;
var mText;
var mButton;
var mStampC;
var mStampG;

//-- end

//-Global function

function populateStorage(){
    localStorage.setItem('mLogo', mLogo);
}

function addBranch(){
    addNew.show();
    addNew.attr("href","#addBranch");
    addNew.html("<i class='fa fa-plus'></i> Add Branch");
}
function  addEmployee(){
    addNew.show();
    addNew.attr("href","#addEmployee");
    addNew.html("<i class='fa fa-plus'></i> Add Employee");
}
function addCustomer(){
    addNew.hide();
}

function designMobile(btnMobile){
    $(btnMobile).removeClass('btn-default').addClass('btn-primary active');
    $('#btnTablet,#btnCard').removeClass('btn-primary active').addClass('btn-default');
    //$('.cardFrame,.tabletFrame').hide();
    //$('.mobileFrame').show();
    $('.cardTab,.tabletTab,.stampIconTab').hide();
    $('.mobileTab').show();
}
function designTablet(btnTablet){
    $(btnTablet).removeClass('btn-default').addClass('btn-primary active');
    $('#btnMobile,#btnCard, #btnStampIcon').removeClass('btn-primary active').addClass('btn-default');
    //$('.mobileFrame,.cardFrame').hide();
    //$('.tabletFrame').show();
    $('.mobileTab,.cardTab,.stampIconTab').hide();
    $('.tabletTab').show();
}
function designStampIcon(btnStampIcon){
    $(btnStampIcon).removeClass('btn-default').addClass('btn-primary active');
    $('#btnMobile,#btnCard,#btnTablet').removeClass('btn-primary active').addClass('btn-default');
    //$('.mobileFrame,.cardFrame').hide();
    //$('.tabletFrame').show();
    $('.mobileTab,.cardTab,.tabletTab').hide();
    $('.stampIconTab').show();
}
function designCard(btnCard){
    $(btnCard).removeClass('btn-default').addClass('btn-primary active');
    $('#btnMobile,#btnTablet').removeClass('btn-primary active').addClass('btn-default');
    //$('.mobileFrame,.tabletFrame').hide();
    //$('.cardFrame').show();
    $('.mobileTab,.tabletTab').hide();
    $('.cardTab').show();
}
function apProd(dis){
    window.location.hash = "addProduct";
    $(dis).addClass('active');
    $(".apAmnt").removeClass('active');
    $(".earningReward1").slideUp('normal');
    $(".earningReward2").slideDown('normal');
}
function apAmnt(dis){
    window.location.hash = "addAmount";
    $(dis).addClass('active');
    $(".apProd").removeClass('active');
    $(".earningReward2").slideUp('normal');
    $(".earningReward1").slideDown('normal');
}

//convert hex to a rgb color
function rgb2hex(rgb) {
    var hexDigits = ["0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"];
    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    function hex(x) {
        return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
    }
    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

// Get Selected Palette
function getPBox1(bgColor){
    bgColor = bgColor.find("i:first-child").css('background-color');
    bgColor = rgb2hex(bgColor);
    bgColor1 = bgColor;

}
function getPBox2(bgColor){
    bgColor = bgColor.find("i:nth-child(2)").css('background-color');
    bgColor = rgb2hex(bgColor);
    bgColor2 = bgColor;
}
function getPBox3(bgColor){
    bgColor = bgColor.find("i:last-child").css('background-color');
    bgColor = rgb2hex(bgColor);
    bgColor3 = bgColor;
}
function getSelectedPalette(selected3Color){
    getPBox1(selected3Color);
    getPBox2(selected3Color);
    getPBox3(selected3Color);
}

function changeColorPalette(){
    $('span.p1').css('background-color',bgColor1);
    $('span.p2').css('background-color',bgColor2);
    $('span.p3').css('background-color',bgColor3);

    var palette = $('.customPaletteBox').children('div');
    palette.eq(0).find('.sp-preview-inner').css('background-color',bgColor1);
    palette.eq(1).find('.sp-preview-inner').css('background-color',bgColor2);
    palette.eq(2).find('.sp-preview-inner').css('background-color',bgColor3);

    $("#pOverlay").val(bgColor1);
    $("#pText").val(bgColor2);
    $("#pButtons").val(bgColor3);

    $("#cThemeVal").text(bgColor1);
    $("#cTextVal").text(bgColor2);
    $("#cButtonsVal").text(bgColor3);

    $('#topPaneLabel').css('color',bgColor2);
    $('.redeem').css('color',bgColor2);    
}

function tChangeColorPalette(){
    $('span.tp1').css('background-color',bgColor1);
    $('span.tp2').css('background-color',bgColor2);
    $('span.tp3').css('background-color',bgColor3);

    var palette = $('.customPaletteBox').children('div');
    palette.eq(3).find('.sp-preview-inner').css('background-color',bgColor1);
    palette.eq(4).find('.sp-preview-inner').css('background-color',bgColor2);
    palette.eq(5).find('.sp-preview-inner').css('background-color',bgColor3);    

    $("#tpOverlay").val(bgColor1);
    $("#tpText").val(bgColor2);
    $("#tpButtons").val(bgColor3);

    $("#mThemeVal").text(bgColor1);
    $("#mTextVal").text(bgColor2);
    $("#mButtonsVal").text(bgColor3);

    $('#topPaneLabel').css('color',bgColor2);
    $('.redeem').css('color',bgColor2);
}
//-- end


function saveLocalStorage(targetResult){
    var target = targetResult;
    if($('#btnMobile').hasClass('active')){
        console.log("mobile");
        $('.customLogo').attr('src', target);
    }
    if($('#btnTablet').hasClass('active')){
        console.log("tablet");
    }
    if($('#btnCard').hasClass('active')){
        console.log("card");
    }
}


//-- quickstart next back
function  qsNextBack(qs){
    $(qs).parent('.qsFooter').parent('.qsWrapper').hide();
    $(qs).parent('.qsFooter').parent('.qsWrapper').next('.qsWrapper').show();
}

function  qsBackNext(qs){
    $(qs).parent('.qsFooter').parent('.qsWrapper').hide();
    $(qs).parent('.qsFooter').parent('.qsWrapper').prev('.qsWrapper').show();
}


$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

//-- end
