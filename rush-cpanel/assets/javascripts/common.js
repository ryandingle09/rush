/* common */

(function() {
    //-- fittext for responsive text
    $("#sidemenu #menu ul li a").fitText(1 , { minFontSize: '8px', maxFontSize: '14px' });
    $(".count li h3").fitText( 0.2, { minFontSize: '30px', maxFontSize: '65px' });

    $(document).activeNavigation("#menu");

    //-- make sidemenu height equals to windows height
    $("#sidemenu").height($(window).height());
    //-- end

    //-- tabs stay active on page load
    $('#tabs a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });
    $("ul.nav-tabs > li > a").on("shown.bs.tab", function (e) {
        var tabId = $(e.target).attr("href").substr(1);
        window.location.hash = tabId;
    });
    var hash = window.location.hash;
    $('#tabs a[href="' + hash + '"]').tab('show');
    //-- end

    //-- remove bg image of siblings tab on load
    $(".nav-tabs li.active").prev("li").children("a").css("background-image","none");

    //-- remove bg image of prev tab
	$(document).on("click", ".management .nav-tabs li", function(){
	    $(".nav-tabs li a").css("background-image","url('images/navtabsborder.jpg')");
	    $(".nav-tabs li.active a").css("background-image","none");
	    $(".nav-tabs li.active").prev("li").children("a").css("background-image","none");

	});
	//-- end

	//-- button effects like android L
	// var button = document.querySelectorAll('.btn');
	// for (var i = 0; i < button.length; i++) {
	//     button[i].onmousedown = function(e) {

	//         var x = (e.offsetX == undefined) ? e.layerX : e.offsetX;
	//         var y = (e.offsetY == undefined) ? e.layerY : e.offsetY;
	//         var effect = document.createElement('div');

	//         effect.className = 'effect';
	//         effect.style.top = y + 'px';
	//         effect.style.left = x + 'px';
 //            // var target = e.target || e.srcElement;
	//         e.target.appendChild(effect);

	//         setTimeout(function() {
	//             e.target.removeChild(effect);
	//         }, 1100);
	//     }
	// }
	//-- end

	//--center modal
	function centerModal() {
	    $(this).css('display', 'block');
	    var $dialog = $(this).find(".modal-dialog");
	    var offset = ($(window).height() - $dialog.height()) / 2;
	    // Center modal vertically in window
	    $dialog.css("margin-top", offset);
	}
	$('.modal').on('show.bs.modal', centerModal);
	//-- end

	$(document).on("click", "#notes a", function(){
        $("#notes").slideUp("slow");
    });

    $(document).on("click", "#flash_message_success a", function(){
        $("#flash_message_success").slideUp("slow");
    });

    $(document).on("click", "#flash_message_error a", function(){
        $("#flash_message_success").slideUp("slow");
    });

    $(document).ready(function() {
        if ($("#flash_message_success")) {
        	setTimeout(function() {
                $("#flash_message_success").slideUp("slow");
        	},1200);
        }

        // Highlight Active Menu
        var activNav = /[^/]*$/.exec(window.location)[0];
        console.log(activNav);
        if (activNav == "" || activNav == "dashboard") {
            $("#sidemenu #menu ul li.das").addClass("activ");
        }
        if (activNav == "analytics") {
            $("#sidemenu #menu ul li.ana").addClass("activ");
        }
        if (activNav == "promos" || activNav == "add" || activNav == "punchcard") {
            $("#sidemenu #menu ul li.pro").addClass("activ");
        }
        if (activNav == "achievement-unlock") {
            $("#sidemenu #menu ul li.ach").addClass("activ");
        }
        if (activNav == "redemption" || activNav == "reports") {
            $("#sidemenu #menu ul li.rep").addClass("activ");
        }
        if (activNav == "design#tablet" || activNav == "design#mobile") {
            $("#sidemenu #menu ul li.des").addClass("activ");
        }
        if (activNav == "transactionhistory") {
            $("#sidemenu #menu ul li.tra").addClass("activ");
        }
        if (activNav == "management" || activNav == "management#branch" || activNav == "management#employee" || activNav == "management#customer" || activNav == "management#reports" || activNav == "management#feedback" || activNav == "management#checkins") {
            $("#sidemenu #menu ul li.man").addClass("activ");
        }
        if (activNav == "sms") {
            $("#sidemenu #menu ul li.sms").addClass("activ");
        }
        if (activNav == "addons" || activNav == "addons#design" || activNav == "addons#order-card" ) {
            $("#sidemenu #menu ul li.add").addClass("activ");
        }
        if (activNav == "help") {
            $("#sidemenu #menu ul li.hel").addClass("activ");
        }
        if (activNav == "settings") {
            $("#sidemenu #menu ul li.set").addClass("activ");
        }
        if (activNav == "merchant") {
            $("#sidemenu #menu ul li.mer").addClass("activ");
        }
		if (activNav == "users") {
			$("#sidemenu #menu ul li.usr").addClass("activ");
		}
		if (activNav == "billing" || activNav == "billing#account" || activNav == "billing#payment-history" || activNav == "billing#billing-history"|| activNav == "payment" || activNav == "billing" ) {
            $("#sidemenu #menu ul li.bil").addClass("activ");
        }
        if (activNav == "users" || activNav == "adduser" || activNav == "edituser" ) {
            $("#sidemenu #menu ul li.usr").addClass("activ");
        }
		if (activNav == "event-attendees") {
            $("#sidemenu #menu ul li.att").addClass("activ");
        }
        if (activNav == "package#package") {
			$("#sidemenu #menu ul li.cls").addClass("activ");
		}

        // Reduce menu height when list exceeds 9 to fit in view
        //console.log($("#sidemenu #menu ul li").length);
        // if($("#sidemenu #menu ul li").length > 9) {
            $("#sidemenu #menu ul li a span, #sidemenu #menu ul li a i").height("40px");

            $("#sidemenu #menu ul li .icon").each(function(index) {
                var pos = $(this).css("background-position").split(" ");
                var pos1 = parseInt(pos[1].replace("px",""));
                var newPos = pos1 - 10;

                $(this).css({
                    "background-position": pos[0] + " " + newPos + "px"
                });
            });
        // }


		//For Number
		$('[number]').on('keypress',function(event){
			return event.charCode >= 48 && event.charCode <= 57;
		})

		//For Currency field
		$('[currency]').on('keypress',function(evt){
			var charCode = (evt.which) ? evt.which : event.keyCode
			if (charCode > 31 && (charCode < 48 || charCode > 57) && !(charCode == 46 || charCode == 8)){
				return false;
			} else {
				var len = $(this).val().length;
				var index = $(this).val().indexOf('.');
				if (index > 0 && charCode == 46) {
					return false;
				}
				if (index > 0) {
					var CharAfterdot = (len + 1) - index;
					if (CharAfterdot > 3) {
						return false;
					}
				}
			}
			return true;
		})
    });
})();
