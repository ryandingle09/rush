/* Quicksetup */

var Quicksetup, quicksetupModule;
(function() {
    var bindings = [];
	var setAction = function(current, next, currentPc, nextPc, nextAction, backAction) {
        if ($.inArray(currentPc, bindings) != -1) {
            return false;
        } else {
            bindings.push(currentPc);
        }
    	$(current + ' .nextStep').on('click', function() {
            nextAction(function() {
                $(currentPc).removeClass('active').addClass('done');
                $(nextPc).addClass('active');
                $(current).hide();
                $(next).show();
            });
    	});

    	var backStep = $(current + ' .backStep');
    	if (typeof backStep != 'undefined') {
    		backStep.on('click', function() {
                backAction(); 
    		});
    	}
	};

	var dispose = function(current,prev,currentPc,prevPc) {
        $(currentPc).removeClass('active');
        $(prevPc).removeClass('done').addClass('active');
        $(current).hide();
        $(prev).show();
	};

	var submit = function(id,callback,success) {
        var form = $(id);
        var form_url = form.attr('action');
        var formData = new FormData(form[0]);

        $.ajax({
            type: 'POST',
            url: form_url,
                data: formData,
                processData: false,
                contentType: false,
                success: function (response) {
                    console.log(response);
                    success(response);
                    callback();
                },
                error: function(xhr,status,error) {
                    console.log(error);
                }
            });
	};

	var setHash = function(hash) {
        // window.location.hash = hash;
	}

    var quickSetupImageValidation = function(step, options = []) {
        var error = 0;
        var default_image = 'assets/images/addImage.jpg';
        var msgInvalidDimension = 'Image does not meet the image size requirement. Please check again.';
        var msgPleaseSupplyImage = 'Please upload the required image.';

        $.each($(step + ' :file'), function(key, value) {
            var imgDiv = $(this).parent().parent().parent();
            var errMsgSpan = imgDiv.find('.error-msg');
            if (options.indexOf('requireImage') >= 0
                && $(this).attr('validationstatus') == undefined
                && imgDiv.find('.imgPrev').attr('src') == default_image
            ) {
                errMsgSpan.text(msgPleaseSupplyImage);
                errMsgSpan.removeClass('hide');
                error++;
            }
            if ($(this).attr('validationstatus') == 'invalid') {
                errMsgSpan.text(msgInvalidDimension);
                errMsgSpan.removeClass('hide');
                error++;
            }
        });

        return error;
    }

    Quicksetup = {
    	Loyalty: function() {
            var loyaltyClass = {
                init: function() {
                    loyaltyClass.createProgram();
                },
                createProgram: function() {
                	setHash("#step=1");
                	setAction('.qsOne', '.qsTwo', '.pc1', '.pc2',
                        function(callback) {
                            var programName = $("#programName").val()
                            var pointsName = $("#pointsName").val()
                            var do_submit = true
                            if (programName.length < 3) {
                                $("#programNameLabel").css("color","red");
                                do_submit = false
                            }

                            if (pointsName.length < 3) {
                                $("#pointsNameLabel").css("color","red");
                                do_submit = false
                            }
                            if (do_submit) {
                                $("#programNameLabel").css("color","black");
                                $("#pointsNameLabel").css("color","black");
                                submit("#step1",callback, function(response) {
                                     loyaltyClass.designMerchantApp();
                                });
                            }
                    	}
                        , function() {}
                    );
                },
                designMerchantApp: function() {
                	setHash('#step=2');
                	setAction('.qsTwo', '.qsThree', '.pc2', '.pc3',
                        function(callback) {
                            var errors = quickSetupImageValidation('.qsTwo', ['requireImage']);
                            if (!errors) {
                                submit('#step2', callback, function(response) {
                                    loyaltyClass.designCustomerApp();
                                });
                            }
                    	}, function() {
                            dispose('.qsTwo', '.qsOne', '.pc2', '.pc1');
                            loyaltyClass.createProgram();
                    	}
                    );
                },
                designCustomerApp: function() {
                    setHash('#step=3');
                    setAction('.qsThree', '.qsFour', '.pc3', '.pc4',
                        function(callback) {
                            var errors = quickSetupImageValidation('.qsThree', []);
                            if (!errors) {
                                submit('#step3', callback, function(response) {
                                    loyaltyClass.appDetails();
                                });
                            }
                        }, function() {
                            dispose('.qsThree', '.qsTwo', '.pc3', '.pc2');
                            loyaltyClass.designMerchantApp();
                        }
                    );
                },
                appDetails: function() {
                	setHash('#step=4');
                	setAction('.qsFour', '.qsFinish', '.pc4', '.pc5',
                        function(callback) {
                            var appName = $('#appName').val();
                            var appDescription = $('#appDescription').val();
                            var errors = 0;

                            errors += quickSetupImageValidation('.qsFour', ['requireImage']);

                            if (appName.length < 3) {
                                $('#appNameLabel').css('color', 'red');
                                errors++;
                            }

                            if (appDescription.length < 3) {
                                $('#appDescriptionLabel').css('color', 'red');
                                errors++;
                            }

                            if (!errors) {
                                $('#appNameLabel').css('color', 'black');
                                $('#appDescriptionLabel').css('color', 'black');
                    		    submit('#step4', callback, function(response) {
                                    loyaltyClass.finishLoyalty();
                    		    });
                            }
                    	}, function() {
                            dispose('.qsFour', '.qsThree', '.pc4', '.pc3');
                            loyaltyClass.designCustomerApp();
                    	}
                    );
                },
                finishLoyalty: function() {
                	setHash("#step=5");
			    	var skipQuickstart = $('.skipQuickStart');
			    	if (typeof skipQuickstart != 'undefined') {
			    		skipQuickstart.on('click', function() {
                            var dashboard = "http://" + location.hostname + "/app/dashboard";
                            window.location.href = dashboard;
			    		});
			    	}
                }
            };
            return loyaltyClass;
    	},

    	PunchCard: function() {

            var punchcardClass = {
                init: function() {
                    punchcardClass.createProgram();
                },
                createProgram: function() {
                	setHash('#step=1');
                    setAction('.qsOne', '.qsTwo', '.pc1', '.pc2',
                        function(callback) {
                            var programName = $('#programName').val()
                            var pointsName = $('#pointsName').val()
                            var do_submit = true
                            if (programName.length < 3) {
                                $('#programNameLabel').css('color', 'red');
                                do_submit = false
                            }
                            if (pointsName.length < 3) {
                                $('#pointsNameLabel').css('color', 'red');
                                do_submit = false
                            }
                            if (do_submit) {
                                $('#programNameLabel').css('color', 'black');
                                $('#pointsNameLabel').css('color', 'black');
                                submit('#step1', callback, function(response) {
                                     punchcardClass.designMerchantApp();
                                });
                            }
                    	},
                        function() {}
                    );
                },
                designMerchantApp: function() {
                	setHash('#step=2');
                    setAction('.qsTwo', '.qsThree', '.pc2', '.pc3',
                        function(callback) {
                            var errors = 0;
                            errors += quickSetupImageValidation('.qsTwo', ['requireImage']);
                            if (!errors) {
                            	submit('#step2', callback, function(response) {
                                    punchcardClass.designCustomerApp();
                            	});
                            }
                        },
                        function() {
                        	dispose('.qsTwo', '.qsOne', '.pc2', '.pc1');
                            punchcardClass.createProgram();
                        }
                    );
                },
                designCustomerApp: function() {
                    setHash('#step=3');
                    setAction('.qsThree', '.qsFour', '.pc3', '.pc4', function(callback) {
                        var errors = quickSetupImageValidation('.qsThree', []);
                        if (!errors) {
                            submit('#step3', callback, function(response) {
                                punchcardClass.appDetails();
                            });
                        }
                    }, function() {
                        dispose('.qsThree', '.qsTwo', '.pc3', '.pc2');
                        punchcardClass.designMerchantApp();
                    });
                },
                appDetails: function () {
                	setHash('#step=4');
                	setAction('.qsFour', '.qsFinish', '.pc4', '.pc5',
                        function(callback) {
                            var appName = $('#appName').val();
                            var appDescription = $('#appDescription').val();
                            var errors = 0;
                            if (appName.length < 3) {
                                $('#appNameLabel').css('color', 'red');
                                errors++;
                            } else {
                                $('#appNameLabel').css('color', 'black');
                            }
                            if (appDescription.length < 3) {
                                $('#appDescriptionLabel').css('color', 'red');
                                errors++;
                            } else {
                                $('#appDescriptionLabel').css('color', 'black');
                            }
                            errors += quickSetupImageValidation('.qsFour', ['requireImage']);
                            if (!errors) {
                                $('#appNameLabel').css('color', 'black');
                                $('#appDescriptionLabel').css('color', 'black');
                                submit('#step4', callback, function(response) {
                                    punchcardClass.finishPunchcard();
                                });
                            }
                    	},
                        function() {
                            dispose('.qsFour', '.qsThree', '.pc4', '.pc3');
                            punchcardClass.designCustomerApp();
                    	}
                    );
                },
                finishPunchcard: function() {
                	setHash("#step=4");
			    	var skipQuickstart = $('.skipQuickStart');
			    	if (typeof skipQuickstart != 'undefined') {
			    		skipQuickstart.on('click', function() {
                            var dashboard = "http://" + location.hostname + "/app/dashboard";
                            window.location.href = dashboard;
			    		});
			    	}
                }
            };
            return punchcardClass;
    	}
    };

	$(document).ready(function() {
        quicksetupModule = Config.quicksetup;
        if (quicksetupModule == "loyalty") {
            Quicksetup.Loyalty().init();
        } else if (quicksetupModule == "punchcard") {
            // MerchantAppDesign.create(0);
            Quicksetup.PunchCard().init();
        }
	});
})();