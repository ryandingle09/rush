<?php

namespace Rush\Modules\Billing\Console\Commands;

use Illuminate\Console\Command;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class BillingSingleCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'billing:single-generate {id} {month}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Billing for One Merchant.';

    /**
     * BillingService
     * @var \Rush\Modules\Billing\Services\BillingService
     */
    protected $billingService = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct( \Rush\Modules\Billing\Services\BillingService $billingService ) {
        parent::__construct();
        $this->billingService = $billingService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $merchant_id = $this->argument('id');
      $month = $this->argument('month');

      $logger = new Logger('Billing Generation Logs');
      $logger->pushHandler(new StreamHandler(storage_path() . '/logs/billing_generation.log', Logger::INFO));
      $logger->addInfo("Running Single Billing Generation.");
      $this->billingService->generateBilling( $merchant_id, null, $month);
      $logger->addInfo("Closing Single Billing Generation.");
    }
}
