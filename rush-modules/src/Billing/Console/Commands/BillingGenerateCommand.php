<?php

namespace Rush\Modules\Billing\Console\Commands;

use Illuminate\Console\Command;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class BillingGenerateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'billing:schedule-generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Billing for Merchants.';

    /**
     * BillingService
     * @var \Rush\Modules\Billing\Services\BillingService
     */
    protected $billingService = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct( \Rush\Modules\Billing\Services\BillingService $billingService ) {
        parent::__construct();
        $this->billingService = $billingService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $logger = new Logger('Billing Generation Logs');
      $logger->pushHandler(new StreamHandler(storage_path() . '/logs/billing_generation.log', Logger::INFO));
      $logger->addInfo("Running Monthly Billing Generation.");
      $this->billingService->generateMonthly();
      $logger->addInfo("Closing Monthly Billing Generation.");
    }
}
