<?php
namespace Rush\Modules\Billing\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Rush\Modules\Billing\Models\PackageModel
 *
 * @property integer $id
 * @mixin \Eloquent
 */
class PackageModel extends Model
{
  protected $table = "Package";
  protected $primaryKey = "packageId";
}
