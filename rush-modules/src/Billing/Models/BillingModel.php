<?php
namespace Rush\Modules\Billing\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Rush\Modules\Billing\Models\BillingModel
 *
 * @property integer $id
 * @mixin \Eloquent
 */
class BillingModel extends Model
{
  use SoftDeletes;

  protected $table = "Billing";
  protected $primaryKey = "id";
  protected $dates = ['deleted_at'];

  public function package()
  {
    return $this->hasOne('Rush\Modules\Billing\Models\PackageModel', 'packageId', 'package_id');
  }

  public function getIsPaidTextAttribute()
  {
  	if ( $this->is_paid == 1 )
  	{
  		return "paid";
  	} else if ( $this->is_paid == 2) {
      return 'pending';
  	} else {
  		return 'unpaid';
  	}
  }

  public function getIsLatestAttribute()
  {
    $latest_billing = BillingModel::where( 'merchant_id', $this->merchant_id )->orderBy('created_at', 'desc')->first();
    if ( $this->id == $latest_billing->id ) {
      return true;
    } else {
      return false;
    }
  }

  public function getLatestBillingAttribute()
  {
    $latest_billing = BillingModel::where( 'merchant_id', $this->merchant_id )->orderBy('created_at', 'desc')->first();
    if ( count($latest_billing) > 0 ) {
      return $latest_billing;
    }
    return false;
  }

}
