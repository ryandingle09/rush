<?php
namespace Rush\Modules\Billing\Repositories;

use Rush\Modules\Repository\RepositoryInterface;

/**
 * Interface BillingInterface
 * @package Rush\Modules\Billing\Repositories
 */
interface BillingInterface extends RepositoryInterface
{
  
}
