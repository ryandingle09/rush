<?php
namespace Rush\Modules\Billing\Repositories;

use Rush\Modules\Billing\Models\BillingModel;
use Rush\Modules\Merchant\Models\NotificationSmsLogsModel;
use Rush\Modules\Repository\AbstractRepository;
use Carbon\Carbon;
use Milon\Barcode\DNS1D;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use DB;
use Event;
use Rush\Modules\Alert\Events\BillingGenerationEvent;
/**
 * Class BillingRepository
 * @package Rush\Modules\Billing\Repositories
 */
class BillingRepository extends AbstractRepository implements  BillingInterface
{
    protected $billing_tiers = [
        'tier1' => [ 'member_count' => '1 - 10,000', 'msf' => 15000, 'sms' => 2500, 'label' => 'Member: 1 - 10,000' ],
        'tier2' => [ 'member_count' => '10,001 - 25,000', 'msf' => 35000, 'sms' => 6250, 'label' => 'Member: 10,001 - 25,000' ],
        'tier3' => [ 'member_count' => '25,001 - 50,000', 'msf' => 60000, 'sms' => 12500, 'label' => 'Member: 25,000 - 50,000' ],
        'tier4' => [ 'member_count' => '50,001 - 100,000', 'msf' => 80000, 'sms' => 20000, 'label' => 'Member: 50,001 - 100,000' ],
        'tier5' => [ 'member_count' => '100,001 and up', 'msf' => 100000, 'sms' => 25000, 'label' => 'Member: 100,001 and up' ]
      ];

    /**
     * BillingRepository constructor.
     * @param BillingModel $BillingModel
     */
    public function __construct(BillingModel $billingModel)
    {
        parent::__construct($billingModel);
    }

    public function getById( $id )
    {
      return $this->model->where( ['id' => $id ] )->first();
    }

    public function getLatestBilling( $merchant_id )
    {
      // return $this->model->where( [ 'merchant_id' => $merchant_id, [ 'billing_period_start', '<', Carbon::now()->subMonth() ] , [ 'billing_period_end', '>=', Carbon::now()->subMonth() ] ] )->first();
      return $this->model->where( [ 'merchant_id' => $merchant_id ])->orderBy('id','DESC')->first();
    }

    public function getPreviousBilling( $merchant_id, $billing_id )
    {
      return $this->model->where( [ 'merchant_id' => $merchant_id, [ 'id', '<', $billing_id ] ] )->orderBy('id', 'DESC')->first();
    }

    public function getPayments( $merchant_id )
    {
      return $this->model->where( [ 'merchant_id' => $merchant_id, 'is_paid' => TRUE ] )->orderBy('id', 'DESC')->get();
    }

    public function getBillingHistory( $merchant_id )
    {
      return $this->model->where( [ 'merchant_id' => $merchant_id ] )->orderBy('id', 'DESC')->get();
    }

    public function generateBilling( $merchant, $start = null, $month = null )
    {
      //Check if the merchant has a packageId. return false if none
      if ( empty($merchant->packageId) ) return false;

      //Check if merchant account already has an account number generated for him/her
      if ( empty($merchant->account_number) ) {
        $this->generateAccountNumber( $merchant ); //generate if there isn't
      }

      //If Merchant Creation is same month as Billing it will return null
      if ( Carbon::parse( $merchant->timestamp )->month == Carbon::now()->month && Carbon::parse( $merchant->timestamp )->year == Carbon::now()->year ) { return false; }

      $billing = new BillingModel;
      $billing->account_number = $merchant->account_number;
      $billing->merchant_id = $merchant->merchantid;
      $billing->package_id = $merchant->packageId;
      $billing->status = ( $merchant->status ) ? $merchant->status : 'Trial';
      $billing->business_name = $merchant->businessName;
      $billing->business_type = $merchant->businessType;
      $billing->business_branches = $merchant->active_branches_count;
      $billing->location = $merchant->address;
      $billing->authorized_rep = $merchant->contactPerson;
      $billing->contact_number = $merchant->contactPersonNumber;

      $billing_start = new Carbon('first day of last month');
      $billing_start->hour = 00;
      $billing_start->minute = 00;
      $billing_start->second = 00;
      $billing_end = new Carbon('last day of last month');
      $billing_end->hour = 23;
      $billing_end->minute = 59;
      $billing_end->second = 59;

      if ( Carbon::now()->submonth()->month == Carbon::parse($merchant->trial_end)->month && Carbon::now()->submonth()->year == Carbon::parse($merchant->trial_end)->year && Carbon::parse($merchant->trial_end)->toDateString() != Carbon::parse($billing_end)->toDateString() ) {
        $billing_start = Carbon::parse($merchant->trial_end)->addDay(); 
      }

      if ( $start ) {
        $billing_start = $start;
      }

      if ( $month ) {
        $billing_start = new Carbon("first day of $month");
        $billing_start->hour = 00;
        $billing_start->minute = 00;
        $billing_start->second = 00;
        $billing_end = new Carbon("last day of $month");
        $billing_end->hour = 23;
        $billing_end->minute = 59;
        $billing_end->second = 59;
      }
      
      /* Check if there's a previous billing for this merchant */
      $billing_history = $this->getBillingHistory( $merchant->merchantid );
      $latest_billing = $billing_history->first();
      $billing_number = $billing_history->count() + 1;
      $billing->billing_number = $billing_number;

      $billing_statement = Carbon::parse( $billing_end )->addDay();
      $billing_due = Carbon::parse( $billing_statement );
      $billing_due->modify('last day of this month');

      $billing->billing_period_start = $billing_start;
      $billing->billing_period_end = $billing_end;
      $billing->statement_date = $billing_statement;
      $billing->due_date = $billing_due;
      $service_fee = $this->getServiceFee( $merchant, $billing_start, $billing_end );
      if ( $merchant->status == 'Trial' ) $service_fee['total_price'] = 0;
      $billing->service_fee = $service_fee['total_price'];
      $billing->service_breakdown = json_encode($service_fee);

      /* Check for Addons fee */
      $add_ons_fee = 0;
      $addons_id = explode('|', $merchant->PackageAddons);
      foreach ( $addons_id as $id )
      {
        switch( $id )
        {
          case 1: //Additional SMS Credit
            $get_fee = $this->additional_sms_credits( $merchant, $billing_start, $billing_end );
            $add_ons_fee += $get_fee['fee'];
          break;
          case 2: //App-Zero Rating
            $get_fee = $this->app_zero_rating( $merchant, $billing_start, $billing_end );
            $add_ons_fee += $get_fee['fee'];
          break;
          case 3: //White-labaled loyalty cards
            $get_fee = $this->white_labeled_card( $merchant, $billing_start, $billing_end);
            $add_ons_fee += $get_fee['fee'];
          break;
          default:
          break;
        }
      }

      $vat_add_ons = []; 
      $no_vat_add_ons = [];
      $vat_add_ons_fee = 0;
      $no_vat_add_ons_fee = 0;

      if( isset($merchant->billing_add_ons) ) {
        foreach( json_decode($merchant->billing_add_ons) as $add_on )
        {
          if ( $add_on->vat )
          {
            $vat_add_ons_fee += ( ( $add_on->amount * $add_on->quantity ) / 1.12 );
          } else {
            $no_vat_add_ons_fee += ( $add_on->amount * $add_on->quantity );
          }
        }
      
        $billing->billing_add_ons = $merchant->billing_add_ons;
      }

      $billing->add_ons_fee = $add_ons_fee + $vat_add_ons_fee + $no_vat_add_ons_fee;
      $vat_fee = ( $service_fee['total_price'] + $service_fee['additional_sms_transaction_fee'] + $service_fee['additional_admin_rewards_transactions'] + $service_fee['merchant_sms_broadcast_fee'] + $add_ons_fee + $vat_add_ons_fee ) * 0.12;
      $billing->vat_fee = $vat_fee;
      $total = $service_fee['total_price'] + $service_fee['additional_sms_transaction_fee'] + $service_fee['additional_admin_rewards_transactions'] + $service_fee['merchant_sms_broadcast_fee']  + $add_ons_fee + $vat_add_ons_fee + $no_vat_add_ons_fee + $vat_fee;
      $billing->total_current = $total;

      $outstanding_balance = 0;
      if ($merchant->settings && $merchant->settings->accrued_billing) {
        if ( !empty($latest_billing ) ) {
          $outstanding_balance = $latest_billing->total_amount - $latest_billing->payment_amount;
        }
      }
      
      $billing->outstanding_balance = $outstanding_balance;

      $total_amount = $total + $outstanding_balance;
      $billing->total_amount = $total_amount;

      /* Generate barcode file and assign in database field */
      $billing->barcode = $this->generateBarcode( $merchant->account_number );

      $billing->save();
      $logger = new Logger('Billing Generation Logs');
      $logger->pushHandler(new StreamHandler(storage_path() . '/logs/billing_generation.log', Logger::INFO));
      $logger->addInfo("Billing Data #" . $billing->id . " for Merchant # " . $merchant->merchantid . " generated.");
      // if( $merchant->status == 'Full' ) {
      //   Event::fire(new BillingGenerationEvent( $billing->id, $merchant->merchantid ));
      // }
    }

    public function getServiceFee( $merchant, $start, $end )
    {
      $fee = [];
      $fee['total_price'] = 0;
      $fee['additional_sms_transaction'] = 0;
      $fee['additional_sms_transaction_fee'] = 0;
      $fee['additional_admin_rewards_transactions'] = 0;
      $fee['merchant_sms_broadcast'] = 0;
      $fee['merchant_sms_broadcast_fee'] = 0;

      switch ( $merchant->packageId )
      {
        case 1: //if loyalty
          $fee = array_merge( $fee, $this->getLoyaltyMSF( $merchant, $start, $end ) );
          $fee = array_merge( $fee, $this->getSMSTransactions( $merchant, $start, $end ) );
          $fee = array_merge( $fee, $this->getSMSBroadcastFee( $merchant, $start, $end ) );
          $fee['additional_admin_rewards_transactions'] = $this->get_admin_rewards_transactions( $merchant, $start, $end);
        break;
        case 2: //if punchcard
          $fee = array_merge( $fee, $this->getPunchCardMSF( $merchant, $start, $end ) );
          $fee = array_merge( $fee, $this->getSMSTransactions( $merchant, $start, $end ) );
          $fee = array_merge( $fee, $this->getSMSBroadcastFee( $merchant, $start, $end ) );
        break;
        default:
        break;
      }
      return $fee;
    }

    public function getLoyaltyMSF( $merchant, $start, $end )
    {
      $fee = [];

      if ( $merchant->new_billing_calculation )
      {
        $customer_count = $merchant->customers->count();
        $service_fee = 0;
        $service_quantity = $customer_count;
        $merchant_billing_tiers = json_decode( $merchant->new_billing_tier_options );
        $service_fee_label = '';

        if ( $customer_count >= 0 && $customer_count <= 10000 ) {
          $service_fee = isset( $merchant_billing_tiers->tier1->msf ) ? $merchant_billing_tiers->tier1->msf : $this->billing_tiers['tier1']['msf'];
          $service_fee = ( $service_fee / 1.12);
          $service_fee_label = isset( $merchant_billing_tiers->tier1->label ) ?  $merchant_billing_tiers->tier1->label : $this->billing_tiers['tier1']['label']; }
        else if ( $customer_count > 10000 && $customer_count <= 25000 ) {
          $service_fee = isset( $merchant_billing_tiers->tier2->msf ) ? $merchant_billing_tiers->tier2->msf : $this->billing_tiers['tier2']['msf'];
          $service_fee = ( $service_fee / 1.12);
          $service_fee_label = isset( $merchant_billing_tiers->tier2->label ) ?  $merchant_billing_tiers->tier2->label : $this->billing_tiers['tier2']['label']; }
        else if ( $customer_count > 25000 && $customer_count <= 50000 ) {
          $service_fee = isset( $merchant_billing_tiers->tier3->msf ) ? $merchant_billing_tiers->tier3->msf : $this->billing_tiers['tier3']['msf'];
          $service_fee = ( $service_fee / 1.12);
          $service_fee_label = isset( $merchant_billing_tiers->tier3->label ) ?  $merchant_billing_tiers->tier3->label : $this->billing_tiers['tier3']['label']; }
        else if ( $customer_count > 50000 && $customer_count <= 100000 ) {
          $service_fee = isset( $merchant_billing_tiers->tier4->msf ) ? $merchant_billing_tiers->tier4->msf : $this->billing_tiers['tier4']['msf'];
          $service_fee = ( $service_fee / 1.12);
          $service_fee_label = isset( $merchant_billing_tiers->tier4->label ) ?  $merchant_billing_tiers->tier4->label : $this->billing_tiers['tier4']['label']; }
        else if ( $customer_count > 100000 ) {
          $service_fee = isset( $merchant_billing_tiers->tier5->msf ) ? $merchant_billing_tiers->tier5->msf : $this->billing_tiers['tier5']['msf'];
          $service_fee = ( $service_fee / 1.12);
          $service_fee_label = isset( $merchant_billing_tiers->tier5->label ) ?  $merchant_billing_tiers->tier5->label : $this->billing_tiers['tier5']['label']; }
        
        $fee['msf_type'] = 'fixed';
        $fee['total_price'] = round( $service_fee, 2 );
        $fee['service_fee_label'] = $service_fee_label;
        $fee['service_fee'] = round( $service_fee, 2 );
        $fee['service_quantity'] = 1;
      } else {
        $transaction_total_fee = 0;
        $transactional_gross_fee = 0;

        if ( $merchant->msf_type == 'fixed') {
          $msf = ( $merchant->monthly_service_fee ) ? $merchant->monthly_service_fee : 1299;
          $transaction_total_fee = $msf;
          $transactional_gross_fee = $transaction_total_fee;
        } else {
          $transactions = $merchant->getTransactions( $start, $end )->where('transactionType', 'earn')->get();
          foreach( $transactions as $transaction) {
            $transaction_total_fee += $transaction->amountPaidWithCash;
          }
          $transactional_gross_fee = $transaction_total_fee;
          $msf = ( $merchant->monthly_service_fee ) ? $merchant->monthly_service_fee : 1.5;
          $transaction_total_fee = $transaction_total_fee * ( $msf/100 );
        }
        $fee['msf_type'] = $merchant->msf_type;
        $fee['total_price'] = ( round( $transaction_total_fee, 2 ) / 1.12 );
        $fee['service_fee'] = ( round( $transactional_gross_fee, 2 ) / 1.12);
        $fee['service_quantity'] = ( $merchant->msf_type == 'fixed') ? 1 : $msf .'%';
      }
      return $fee;
    }

    public function getSMSTransactions( $merchant, $start, $end )
    {
      $fee = [];
      $charge = ( ( $merchant->sms_transactions_charge ) ? $merchant->sms_transactions_charge : "0.5" ) / 1.12;
      
      if ( $merchant->packageId == 1 ) {
        $sms_trans_total = count( $this->sms_customer_registration($merchant->merchantid, $start, $end) ) + count( $this->sms_points_inquiry( $merchant->merchantid, $start, $end) ) + count( $this->sms_points_transfer( $merchant->merchantid, $start, $end) );
      } else if ( $merchant->packageId == 2 ) {
        $sms_trans_total = count( $this->sms_customer_registration($merchant->merchantid, $start, $end) );
      }

      if ( $sms_trans_total > 0 ) {
        $fee['additional_sms_transaction'] = $sms_trans_total;
        $fee['additional_sms_transaction_charge'] = $charge = round( $charge, 2);
        $fee['additional_sms_transaction_fee'] =  $sms_trans_total * $charge ;
      }
      return $fee;
    }

    public function getPunchCardMSF( $merchant, $start, $end )
    {
      $fee = [];
      if ( $merchant->new_billing_calculation )
      {
        $customer_count = $merchant->customers->count();
        $service_fee = 0;
        $service_quantity = $customer_count;
        $merchant_billing_tiers = json_decode( $merchant->new_billing_tier_options );

        if ( $customer_count >= 0 && $customer_count <= 10000 ) {
          $service_fee = isset( $merchant_billing_tiers->tier1->msf ) ? $merchant_billing_tiers->tier1->msf : $this->billing_tiers['tier1']['msf'];
          $service_fee = ( $service_fee / 1.12);
          $service_fee_label = isset( $merchant_billing_tiers->tier1->label ) ?  $merchant_billing_tiers->tier1->label : $this->billing_tiers['tier1']['label']; }
        else if ( $customer_count > 10000 && $customer_count <= 25000 ) {
          $service_fee = isset( $merchant_billing_tiers->tier2->msf ) ? $merchant_billing_tiers->tier2->msf : $this->billing_tiers['tier2']['msf'];
          $service_fee = ( $service_fee / 1.12);
          $service_fee_label = isset( $merchant_billing_tiers->tier2->label ) ?  $merchant_billing_tiers->tier2->label : $this->billing_tiers['tier2']['label']; }
        else if ( $customer_count > 25000 && $customer_count <= 50000 ) {
          $service_fee = isset( $merchant_billing_tiers->tier3->msf ) ? $merchant_billing_tiers->tier3->msf : $this->billing_tiers['tier3']['msf'];
          $service_fee = ( $service_fee / 1.12);
          $service_fee_label = isset( $merchant_billing_tiers->tier3->label ) ?  $merchant_billing_tiers->tier3->label : $this->billing_tiers['tier3']['label']; }
        else if ( $customer_count > 50000 && $customer_count <= 100000 ) {
          $service_fee = isset( $merchant_billing_tiers->tier4->msf ) ? $merchant_billing_tiers->tier4->msf : $this->billing_tiers['tier4']['msf'];
          $service_fee = ( $service_fee / 1.12);
          $service_fee_label = isset( $merchant_billing_tiers->tier4->label ) ?  $merchant_billing_tiers->tier4->label : $this->billing_tiers['tier4']['label']; }
        else if ( $customer_count > 100000 ) {
          $service_fee = isset( $merchant_billing_tiers->tier5->msf ) ? $merchant_billing_tiers->tier5->msf : $this->billing_tiers['tier5']['msf'];
          $service_fee = ( $service_fee / 1.12);
          $service_fee_label = isset( $merchant_billing_tiers->tier5->label ) ?  $merchant_billing_tiers->tier5->label : $this->billing_tiers['tier5']['label']; }

        $fee['total_price'] = round( $service_fee, 2 );
        $fee['service_fee_label'] = $service_fee_label;
        $fee['service_fee'] = round( $service_fee, 2 );
        $fee['service_quantity'] = 1;
      } else {
        $msf = ( $merchant->monthly_service_fee ) ? $merchant->monthly_service_fee : 1299;
        $msf = $msf / 1.12;
        if ( isset($merchant->branch_count) && $merchant->branch_count > 0 ) {
          $branches = $merchant->branch_count;
        } else {
          $branches = $merchant->branches->where('is_deleted', 0)->count() ?: 1;
        }
        $fee['total_price'] = round( ( $msf * $branches ) + ( 199 / 1.12 ), 2);
        $fee['service_fee'] = $msf; //service fee * number branches for punchcard
        $fee['service_quantity'] = $branches; //service fee * number branches for punchcard
        if ( $merchant->settings && $merchant->settings->billing_cms_tool_fee ) {
          $fee['cms_tools_fee'] = 199; //additional fee for CMS Tools
        }
      }
      return $fee;
    }

    public function getSMSBroadcastFee( $merchant, $start, $end )
    {
      $fee = [];
      $charge = ( ( $merchant->sms_transactions_charge ) ? $merchant->sms_transactions_charge : "0.5" ) / 1.12;
      $sms_count = count( $this->get_sms_broadcast( $merchant->merchantid, $start, $end) );
      if ( $sms_count > 0 ) {
        $merchant_billing_tiers = json_decode( $merchant->new_billing_tier_options );

        if ( $merchant->new_billing_calculation ) {
          if ( $sms_count > 2500 ) {
            $customer_count = $merchant->customers->count();
            if ( $customer_count >= 0 && $customer_count <= 2500 ) {
              $merchant_sms = isset( $merchant_billing_tiers->tier1->sms ) ? $merchant_billing_tiers->tier1->sms : $this->billing_tiers['tier1']['sms'];
            } else if ( $customer_count > 2500 && $customer_count <= 25000 ) {
              $merchant_sms = isset( $merchant_billing_tiers->tier2->sms ) ? $merchant_billing_tiers->tier2->sms : $this->billing_tiers['tier2']['sms'];
            } else if ( $customer_count > 25000 && $customer_count <= 50000 ) {
              $merchant_sms = isset( $merchant_billing_tiers->tier3->sms ) ? $merchant_billing_tiers->tier3->sms : $this->billing_tiers['tier3']['sms'];
            } else if ( $customer_count > 50000 && $customer_count <= 100000 ) {
              $merchant_sms = isset( $merchant_billing_tiers->tier4->sms ) ? $merchant_billing_tiers->tier4->sms : $this->billing_tiers['tier4']['sms'];
            } else if ( $customer_count > 100000 ) {
              $merchant_sms = isset( $merchant_billing_tiers->tier5->sms ) ? $merchant_billing_tiers->tier5->sms : $this->billing_tiers['tier5']['sms'];
            }

            if ( $sms_count > $merchant_sms ) {
              $sms_count = $sms_count - $merchant_sms;
            }

            $fee['merchant_sms_broadcast'] = $sms_count;
            $fee['merchant_sms_broadcast_charge'] = $charge = round( $charge, 2);
            $fee['merchant_sms_broadcast_fee'] = $fee['merchant_sms_broadcast'] * $charge;
          }
        } else {
          $fee['merchant_sms_broadcast'] = $sms_count;
          $fee['merchant_sms_broadcast_charge'] = $charge = round( $charge, 2);
          $fee['merchant_sms_broadcast_fee'] = $fee['merchant_sms_broadcast'] * $charge;
        }
      }
      return $fee;
    }

    public function additional_sms_credits( $merchant, $start, $end )
    {
      $fee = 0;
      $sms_count = $merchant->notif_sms_logs()->whereBetween('created_at', [$start, $end])->count();
      $fee = $sms_count  * 0.5;
      return [ 'fee' => ( $fee / 1.12 ), 'qty' => ( $sms_count / 1.12 ) ];
    }

    public function app_zero_rating( $merchant, $start, $end )
    {
      $fee = 0;
      $transaction_count = $merchant->getTransactions( $start, $end )->count();
      $fee = $transaction_count * ( 6 / 1.12 );
      return [ 'fee' => $fee, 'qty' => ( $transaction_count * 3 ) . ' MB' ];
    }

    public function white_labeled_card( $merchant, $start, $end )
    {
      $fee = 0;
      $count = 0;
      $card_orders = $merchant->loyaltyCardOrder()->whereBetween('created_at', [$start, $end])->where('status','<>', 'Pending')->get();
      foreach( $card_orders as $order ) {
        $fee += $order->total;
        $count += $order->quantity;
      }
      return [ 'fee' => $fee , 'qty' => $count ];
    }

    public function generateBarcode($account_number)
    {
      $barcode = new DNS1D();
      $barcode_path = $barcode->getBarcodePNGPath($account_number , "C39", 1, 30);
      $barcode_path = str_replace(array('/', '\\'), '', $barcode_path);
      $old_path =  public_path() . '/' . $barcode_path;
      $new_path =  base_path().'/../repository/merchant/barcode/';
      if ( !file_exists( $new_path ) ) {
        mkdir( $new_path, 0777, true);
      }
      copy($old_path, $new_path . $barcode_path);
      unlink($old_path);
      return $barcode_path;
    }

    public function generateAccountNumber($merchant)
    {
      $account_number = sprintf('%04d', $merchant->merchantid );
      $account_number .= '-' . sprintf('%06d', rand(1,999999));
      $account_number .= '-' . Carbon::parse($merchant->timestamp)->format('my');
      $merchant->account_number = $account_number;
      $merchant->save();
    }

    public function generateORNumber( $billing )
    {
      $or_number = sprintf('%04d', $billing->merchant_id );
      $or_number .= "-" . sprintf('%03d', rand(1,999));
      $or_number .= sprintf('%04d', Carbon::now()->format('md') );
      $or_number .= "-" . sprintf('%04d', $billing->id );
      return $or_number;

    }

    public function payBilling($id, $data)
    {
      $billing = $this->model->find($id);
      $billing->is_paid = 2;
      $billing->payment_type = $data['paytMethod'];
      $billing->deposit_slip = '/repository/merchant/slip/' . $data['official_receipt'];
      $billing->reference_number = $data['refNo'];
      $billing->payment_amount = $data['amount'];
      $payment_date = Carbon::createFromFormat( 'm-d-Y', $data['payment_date'] );
      $billing->payment_date = $payment_date;
      $billing->or_number = $this->generateORNumber( $billing );
      $billing->save();
    }

    public function updateAmount($id, $amount)
    {
      $billing = $this->model->find($id);
      $billing->payment_amount = $amount;
      $billing->save();
    }

    public function get_admin_rewards_transactions( $merchant, $start, $end )
    {
      $sql = "SELECT `ct`.`pointsRedeem`, `mgr`.`name`, `mgr`.`pointsRequired`
              FROM `CustomerTransaction` as `ct`
              LEFT JOIN `Redeem` as `r`
              ON `ct`.`transactionReferenceCode` = `r`.`redeemReferenceCode`
              LEFT JOIN `redeemedItems` as `ri`
              ON `r`.`redeemId` = `ri`.`redeemId`
              LEFT JOIN `MerchantGlobeRewards` as `mgr`
              ON `ri`.`redeemItemId` = `mgr`.`redeemItemId`
              WHERE `ct`.`merchant_id` = ?
              AND `ct`.`transactionType` = ?
              AND `ct`.`amax_transaction_id` <> 0
              ";
      $transactions = DB::select( $sql, [$merchant->merchantid, 'redeem', ] );
      $total_amount = 0;
      $redemption_peso = ( $merchant->conversion_settings ) ? $merchant->conversion_settings->redemption_peso : 1;
      $redemption_points = ( $merchant->conversion_settings ) ? $merchant->conversion_settings->redemption_points : 1;
      foreach( $transactions as $transaction )
      {
        $amount = ( $transaction->pointsRedeem / $redemption_peso ) * $redemption_points;
        $total_amount += $amount;
      }
      return round( $total_amount / 1.12 , 2);
    }

    public function tagPayment( $id, $data )
    {
      $billing = $this->model->find($id);
      $billing->payment_type = $data['payment_type'];
      $billing->or_number = $data['or_number'];
      $billing->payment_date = Carbon::createFromFormat( 'm/d/Y', $data['payment_date'] );
      $billing->payment_amount = $data['payment_amount'];
      $billing->reference_number = $data['reference_number'];
      if ( isset($data['deposit_slip']) )
      {
        $billing->deposit_slip = $data['deposit_slip'];
      }
      $billing->is_paid = $data['is_paid'];
      return $billing->save();
    }

    public function sms_customer_registration( $merchantid, $start, $end )
    {
        return NotificationSmsLogsModel::where([ 'merchant_id' => $merchantid, 'transaction_type' => 'sms-service-customer-registration' ])->whereBetween('created_at', [$start, $end])->get();
    }

    public function sms_points_inquiry( $merchantid, $start, $end )
    {
        return NotificationSmsLogsModel::where([ 'merchant_id' => $merchantid, 'transaction_type' => 'sms-service-points-inquiry' ])->whereBetween('created_at', [$start, $end])->get();
    }

    public function sms_points_transfer( $merchantid, $start, $end )
    {
        return NotificationSmsLogsModel::where([ 'merchant_id' => $merchantid, 'transaction_type' => 'sms-service-points-transfer' ])->whereBetween('created_at', [$start, $end])->get();
    }

    public function get_sms_broadcast( $merchantid, $start, $end )
    {
        return NotificationSmsLogsModel::where([ 'merchant_id' => $merchantid, 'transaction_type' => 'Sms Broadcast' ])->whereBetween('created_at', [$start, $end])->get();
    }



}
