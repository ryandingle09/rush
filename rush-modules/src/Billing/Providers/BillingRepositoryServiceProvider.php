<?php
namespace Rush\Modules\Billing\Providers;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\ServiceProvider;
use Rush\Modules\Billing\Repositories\BillingInterface;
use Rush\Modules\Billing\Repositories\BillingRepository;

class BillingRepositoryServiceProvider extends  ServiceProvider
{
  protected $commands = [
      'Rush\Modules\Billing\Console\Commands\BillingGenerateCommand',
      'Rush\Modules\Billing\Console\Commands\BillingSingleCommand',
  ];

  public function boot()
  {
      // schedule commands internally when the app has booted
      $this->app->booted(function () {
          $schedule = $this->app->make(Schedule::class);
          $schedule->command('billing:schedule-generate')->monthlyOn(1, '00:30');
      });
  }

  /**
   * Register the service provider.
   *
   * @return void
   */
  public function register()
  {
      $this->commands($this->commands);

      $this->app->singleton(BillingInterface::class, BillingRepository::class);
  }
}
