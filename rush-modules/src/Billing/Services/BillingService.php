<?php
namespace  Rush\Modules\Billing\Services;

use Rush\Modules\Billing\Repositories\BillingInterface;
use Rush\Modules\Merchant\Repositories\MerchantInterface;
use PDF;

class BillingService
{
    /**
     * @var BillingInterface
     */
    protected $billing;
    protected $merchant;

    public function __construct(BillingInterface $billing, MerchantInterface $merchant)
    {
        $this->billing = $billing;
        $this->merchant = $merchant;
    }

    public function generateMonthly()
    {
      $merchants = $this->merchant->all();
      foreach( $merchants as $merchant )
      {
        $this->billing->generateBilling($merchant);
      }
    }

    public function getById( $billing_id )
    {
      return $this->billing->getById( $billing_id );
    }

    public function getLatestBilling( $merchant_id )
    {
      return $this->billing->getLatestBilling( $merchant_id );
    }

    public function getPreviousBilling( $merchant_id, $billing_id )
    {
      return $this->billing->getPreviousBilling( $merchant_id, $billing_id );
    }

    public function getPayments( $merchant_id )
    {
      return $this->billing->getPayments( $merchant_id );
    }

    public function getBillingHistory( $merchant_id )
    {
      return $this->billing->getBillingHistory( $merchant_id );
    }

    public function generateBilling( $merchant_id, $start = null, $month = null )
    {
      $merchant = $this->merchant->getById( $merchant_id );
      $this->billing->generateBilling( $merchant, $start, $month );
    }

    public function payBilling( $billing_id, $data )
    {
      $this->billing->payBilling( $billing_id , $data);
    }

    public function additional_sms_credits( $merchant, $start, $end )
    {
      return $this->billing->additional_sms_credits( $merchant, $start, $end );
    }

    public function app_zero_rating( $merchant, $start, $end )
    {
      return $this->billing->app_zero_rating( $merchant, $start, $end );
    }

    public function white_labeled_card( $merchant, $start, $end )
    {
      return $this->billing->white_labeled_card( $merchant, $start, $end );
    }

    public function updateAmount($billing_id, $amount )
    {
      $this->billing->updateAmount( $billing_id, $amount );
    }

    public function getBillingPdf( $billing, $merchant )
    {
      $data['merchant'] = $merchant;
      $data['billing'] = $billing;
      $data['previous_bill'] = $this->getPreviousBilling( $merchant->merchantid, $billing->id );
      $addons_id = explode('|', $merchant->PackageAddons);
      foreach ( $addons_id as $id )
      {
        if ( $id == 1 ) $data['additional_sms_credits'] = $this->additional_sms_credits( $merchant, $billing->billing_period_start, $billing->billing_period_end );
        if ( $id == 2 ) $data['app_zero_rating'] = $this->app_zero_rating( $merchant, $billing->billing_period_start, $billing->billing_period_end );
        if ( $id == 3 ) $data['white_labeled_card'] = $this->white_labeled_card( $merchant, $billing->billing_period_start, $billing->billing_period_end );
      }
      $pdf = PDF::loadView('account.statement', $data );
      return $pdf;
    }
}
