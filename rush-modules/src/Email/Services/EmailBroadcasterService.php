<?php

namespace Rush\Modules\Email\Services;

use Illuminate\Support\Facades\Mail;
use Rush\Modules\Alert\AlertHelper;
use Rush\Modules\Broadcasting\BroadcastHelper;
use Rush\Modules\Broadcasting\Contracts\BroadcasterInterface;
use Rush\Modules\Broadcasting\Models\MessageModel;
use Rush\Modules\Broadcasting\Services\RecipientService;
use Rush\Modules\Email\Library\Email;
use Rush\Modules\Email\Models\EmailModel;
use Rush\Modules\Email\Repositories\EmailLogInterface;
use Rush\Modules\Helpers\JobHelper;
use Rush\Modules\Merchant\Services\MerchantService;

class EmailBroadcasterService implements BroadcasterInterface
{
    /**
     * @var Rush\Modules\Email\Repositories\EmailLogRepository
     */
    protected $emailLogRepository;

    /**
     * @var Rush\Modules\Broadcasting\Services\RecipientService
     */
    protected $recipientService;

    /**
     * @var Rush\Modules\Merchant\Services\MerchantService
     */
    protected $merchantService;

    public function __construct(
        EmailLogInterface $emailLogRepository,
        RecipientService $recipientService,
        MerchantService $merchantService
    ) {
        $this->emailLogRepository = $emailLogRepository;
        $this->recipientService = $recipientService;
        $this->merchantService = $merchantService;
    }

    /**
     * @param  MessageModel $message
     */
    public function send(MessageModel $message)
    {
        try {
            if (!($message instanceof EmailModel)) {
                throw new \InvalidArgumentException("$message must be an instance of EmailModel");
            }
            JobHelper::start($message);

            $transactionType = 'Email Broadcast';
            $customers = $this->recipientService->getCustomers($message->merchant_id, $message->segment_id);
            $merchant = $this->merchantService->getById($message->merchant_id);
            $fromEmail = $this->getMerchantSenderEmail($merchant->contactPersonEmail);
            $fromName = $this->getMerchantSenderName($merchant->businessName);
            $subject = $this->getSubject($merchant->settings->program_name);

            //MTODO: implement dynamic SMTP configuration override

            // send email to each
            $sentCount = 0;
            foreach($customers AS $customer) {
                $subject = $message->subject;
                $result = Mail::send(
                    ['html' => 'preview.index'],
                    [   'subject' => $message->subject,
                        'emailMessage' => $message->message,
                        'banner' => $message->banner,
                        'salutation' => $customer->fullName,
                        'merchantLogo' => $merchant->merchant_design->logoURL,
                        'id' => $message->id,
                        'slug' => $message->url_slug
                    ],
                    function($email) use ($customer, $fromEmail, $fromName, $subject) {
                        $email->to($customer->email)
                            ->from($fromEmail, $fromName)
                            ->replyTo($fromEmail, $fromEmail)
                            ->subject($subject);
                    }
                );

                // log the Email send transaction
                $this->emailLogRepository->create([
                    'merchant_id' => $message->merchant_id,
                    'email_id' => $message->id,
                    'email_address' => $customer->email,
                    'transaction_type' => $transactionType
                ]);

                $sentCount++;
            }

            $message->status = BroadcastHelper::PROCESSED;
            $message->sent_count = $sentCount;
            $message->save();
        } catch(\Exception $e) {
            JobHelper::reactivate($message, $e);
            throw $e;
        }
    }

    public function getMerchantSenderEmail($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            return 'no-reply@'.substr(strrchr($email, "@"), 1);
        } else {
            return AlertHelper::getFrom();
        }
    }

    public function getMerchantSenderName($name)
    {
        return $name ?: 'Rush';
    }

    public function getSubject($subject)
    {
        return $subject ?: 'Rush Email Broadcast';
    }
}
