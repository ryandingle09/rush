<?php

namespace Rush\Modules\Email\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Mail;
use Rush\Modules\Broadcasting\BroadcastHelper;
use Rush\Modules\Broadcasting\Jobs\EmailBroadcastJob;
use Rush\Modules\Customer\Repositories\NonMemberTransactionRepository;
use Rush\Modules\Email\Models\EmailModel AS Email;
use Rush\Modules\Email\Repositories\EmailInterface;
use Rush\Modules\Helpers\Facades\StorageHelper;

/**
* Our EmailService, containing all useful methods for business logic around Email
* Service just hands off data it was given by the controller to the appropriate repositories
*/
class EmailService
{
    /**
     * EmailRepository
     * @var Rush\Modules\Email\Repositories\EmailRepository
     */
    protected $emailRepository;
    protected $non_member_transaction_repository;

    public function __construct( EmailInterface $emailRepository, NonMemberTransactionRepository $non_member_transaction_repository )
    {
        $this->emailRepository = $emailRepository;
        $this->non_member_transaction_repository = $non_member_transaction_repository;
    }

    public function all()
    {
        return $this->emailRepository->all();
    }

    public function store(array $input)
    {
        $emailInput = $input['email'];
        $emailInput['status'] = BroadcastHelper::INACTIVE;
        $emailInput['publish_date'] = date("Y-m-d H:i:s", strtotime("{$emailInput['emailPublishDate']} {$emailInput['emailPublishTime']}"));
        $emailInput['segment_id'] = $emailInput['targetSegment'];

        $validator = $this->validate(
            $emailInput,
            [   'status' => 'required',
                'emailPublishDate' => 'date_format:Y-m-d',
                'emailPublishTime' => 'date_format:h:i A',
                'publish_date' => 'required',
                'message' => 'required',
                'subject' => 'required',
                'merchant_id' => 'required',
                'imgbanner' => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:width=600,height=250'],
            [   'required' => 'The :attribute is required.',
                'emailPublishDate.date_format' => 'Select date.',
                'emailPublishTime.date_format' => 'Select time.']
        );
        if ($validator->fails()) {
            return $validator;
        }

        if (Input::hasFile('imgbanner')) {
            $emailInput['banner'] = StorageHelper::move('merchant/email', 'imgbanner');
        }
        $emailInput['url_slug'] = str_slug($emailInput['subject']);
        $this->emailRepository->create($emailInput);

        return true;
    }

    public function delete(array $input)
    {
        $rules = array(
            'id' => 'required|exists:email,id,status,'.BroadcastHelper::INACTIVE
        );
        $validator = \Validator::make($input, $rules);
        if ($validator->fails()) {
            return false;
        }

        $email = $this->emailRepository->getById($input['id']);
        StorageHelper::deleteFromRepository($email->banner);
        $email->delete();

        return true;
    }

    public function check()
    {
        $schedules = $this->emailRepository->getScheduledItems();

        foreach ($schedules as $schedule) {
            dispatch(new EmailBroadcastJob($schedule));
            $schedule->status = BroadcastHelper::ACTIVE;
            $schedule->save();
        }
    }

    private function validate($input, $rules, $messages = [])
    {
        return \Validator::make($input, $rules, $messages);
    }

    public function allWithSegment($merchantId)
    {
        return $this->emailRepository->allWithSegment($merchantId);
    }

    public function getBySegmentId($segment_id){
        return $this->emailRepository->getBySegmentId($segment_id);
    }

    public function getEmailBySlug($emailId, $slug)
    {
        return $this->emailRepository->getEmailBySlug($emailId, $slug);
    }

    private function send_email( $sender_email, $sender_name, $receiver_email, $receiver_name, $merchant_id, $flag, $data = [] ){
        
        // get the merchant's email message data from db based on $flag.
        // if there's no retrieved email message, get the generic email message.
        $email_message = $this->emailRepository->get_email_message( $merchant_id, $flag );
        if( empty($email_message) ) $email_message = $this->emailRepository->get_email_message( 0, $flag );

        // if there's still no retrieved email, log this incident.
        if( empty($email_message) ) return $this->log_email( $merchant_id, $flag, 'email message not found', "$sender_name <$sender_email>", "$receiver_name <$receiver_email>", null, null, $data );


        // replace keywords from subject and contents with values from $data.
        if( ! empty($data) ){
            foreach( $data as $key => $value ){
                $email_message->subject = str_replace("%$key%", $value, $email_message->subject);
                $email_message->message = str_replace("%$key%", $value, $email_message->message);
            }
        }


        // set the subject, contents, template, template data of the email to be sent.
        $subject = $email_message->subject;
        $contents = $email_message->message;
        $template = 'notification';
        $template_data = ['contents' => $contents];


        // if the retrieved merchant's email message has a template, 
        // override the contents, template, template data of the email to be sent.
        if( ! empty($email_message->template) ){
            $template = $email_message->template;
            $contents = view("email::{$template}", $data);
            $template_data = $data;
        }


        // send the email.
        Mail::send("email::{$template}", $template_data, function( $mail ) use( $sender_email, $sender_name, $receiver_email, $receiver_name, $subject ){
            $mail->from($sender_email, $sender_name);
            $mail->to($receiver_email, $receiver_name)->subject( $subject );
        });


        // log the email data.
        $this->log_email( $merchant_id, $flag, 'sent', "$sender_name <$sender_email>", "$receiver_name <$receiver_email>", $subject, $contents, $data );

    }

    private function log_email( $merchant_id, $type = null, $response = null, $sender = null, $receiver = null, $subject = null, $contents = null, $data = null ){

        $this->emailRepository->log_email( $merchant_id, $type, $response, $sender, $receiver, $subject, $contents, $data );

    }

    public function earn_non_member_points( $transaction ){

        $employee = $transaction->employee;
        $merchant = $employee->merchant;

        $sender_email = $merchant->no_reply_email;
        $sender_name = $merchant->settings->app_name;
        $receiver_email = $transaction->email;
        $receiver_name = $transaction->email;
        $merchant_id = $merchant->merchantid;
        $flag = 'non-member-points-earn';

        $datetime = Carbon::createFromTimestamp(strtotime($transaction->timestamp));
        $data['subject_app_name'] = $merchant->settings->app_name;
        $data['datetime'] = $datetime->format('m-d-Y, g:i:s A');
        $data['program_name'] = $employee->merchant->settings->program_name;
        $data['points'] = $employee->merchant->getEarningPointsOfAmount( $transaction->amountPaidWithCash );
        $data['points_name'] = $employee->merchant->settings->points_name;
        $data['amount'] = $transaction->amountPaidWithCash;
        $data['branch'] = $employee->branch->branchName;
        $data['app_name'] = $employee->merchant->settings->app_name;
        $data['current_points'] = $this->non_member_transaction_repository->getCurrentPointsByEmail( $transaction->email );
        $data['current_points'] = number_format( (float) $data['current_points'], 2 );
        $data['transaction_reference'] = $transaction->transactionReferenceCode;

        $this->send_email( $sender_email, $sender_name, $receiver_email, $receiver_name, $merchant_id, $flag, $data );

    }

    public function registration( $customer ){

        $merchant = $customer->merchant;

        $sender_email = $merchant->no_reply_email;
        $sender_name = $merchant->settings->app_name;
        $receiver_email = $customer->email;
        $receiver_name = $customer->fullName;
        $merchant_id = $merchant->merchantid;
        $flag = 'customer-register';

        $data['subject_app_name'] = $merchant->settings->app_name;
        $data['name'] = $customer->fullName;
        $data['points_name'] = $merchant->settings->points_name;
        $data['program_name'] = $merchant->settings->program_name;

        $this->send_email( $sender_email, $sender_name, $receiver_email, $receiver_name, $merchant_id, $flag, $data );
        
    }

    public function earnPoints( $transaction, $amount ){

        $merchant = $transaction->customer->merchant;
        $customer = $transaction->customer;

        $sender_email = $merchant->no_reply_email;
        $sender_name = $merchant->settings->app_name;
        $receiver_email = $customer->email;
        $receiver_name = $customer->fullName;
        $merchant_id = $merchant->merchantid;
        $flag = 'customer-points-earn';

        $datetime = Carbon::createFromTimestamp(strtotime($transaction->timestamp));
        $data['subject_app_name'] = $merchant->settings->app_name;
        $data['name'] = $customer->fullName;
        $data['datetime'] = $datetime->format('m-d-Y, g:i:s A');
        $data['balance_points'] = number_format( (float) $customer->points->current, 2);
        $data['program_name'] = $merchant->settings->program_name;
        $data['points_earned'] = number_format( (float) $transaction->pointsEarned, 2);
        $data['points_name'] = $merchant->settings->points_name;
        $data['amount'] = number_format( (float) $amount, 2);
        $data['branch_name'] = ( $transaction->employee->branch ) ? $transaction->employee->branch->branchName : 'system';
        $data['current_points'] = number_format( (float) $customer->points->current, 2);
        $data['transaction_reference'] = $transaction->transactionReferenceCode;
        $data['or_number'] = $transaction->receiptReferenceNumber;

        $this->send_email( $sender_email, $sender_name, $receiver_email, $receiver_name, $merchant_id, $flag, $data );

    }

    public function payPoints( $transaction ){

        $merchant = $transaction->customer->merchant;
        $customer = $transaction->customer;

        $sender_email = $merchant->no_reply_email;
        $sender_name = $merchant->settings->app_name;
        $receiver_email = $customer->email;
        $receiver_name = $customer->fullName;
        $merchant_id = $merchant->merchantid;
        $flag = 'customer-points-pay';

        $datetime = Carbon::createFromTimestamp(strtotime($transaction->timestamp));
        $data['subject_app_name'] = $merchant->settings->app_name;
        $data['name'] = $customer->fullName;
        $data['total_amount_purchased'] = number_format( (float) $transaction->amountPaidWithCash, 2 );
        $data['datetime'] = $datetime->format('m-d-Y, g:i:s A');
        $data['balance_points'] = number_format( (float) $customer->points->current, 2);
        $data['points_paid'] = number_format( (float) $transaction->amountPaidWithpoints, 2 );
        $data['points_name'] = $merchant->settings->points_name;
        $data['branch_name'] = $transaction->employee->branch->branchName;
        $data['transaction_reference'] = $transaction->transactionReferenceCode;
        $data['or_number'] = $transaction->receiptReferenceNumber;

        $this->send_email( $sender_email, $sender_name, $receiver_email, $receiver_name, $merchant_id, $flag, $data );
        
    }

    public function redeemPoints( $transaction ){

        $merchant = $transaction->customer->merchant;
        $customer = $transaction->customer;

        $sender_email = $merchant->no_reply_email;
        $sender_name = $merchant->settings->app_name;
        $receiver_email = $customer->email;
        $receiver_name = $customer->fullName;
        $merchant_id = $merchant->merchantid;
        $flag = 'customer-redeem';

        $datetime = Carbon::createFromTimestamp(strtotime($transaction->timestamp));
        $data['subject_app_name'] = $merchant->settings->app_name;
        $data['name'] = $customer->fullName;
        $data['points_redeem'] = (float) $transaction->pointsRedeem;
        $data['points_name'] = $merchant->settings->points_name;
        $data['quantity'] = $transaction->redeem->quantity;
        $data['reward_name'] = $transaction->reward['name'];
        $data['redeem_location'] = ( isset($transaction->employee) ? $transaction->employee->branch->branchName : $merchant->settings->program_name );;
        $data['datetime'] = $datetime->format('m-d-Y, g:i:s A');
        $data['balance_points'] = number_format( (float) $customer->points->current, 2);
        $data['transaction_reference'] = $transaction->transactionReferenceCode;

        $this->send_email( $sender_email, $sender_name, $receiver_email, $receiver_name, $merchant_id, $flag, $data );
        
    }

    public function claimReward( $transaction ){

        $merchant = $transaction->customer->merchant;
        $customer = $transaction->customer;
        $redeem = $transaction->redeem;

        $sender_email = $merchant->no_reply_email;
        $sender_name = $merchant->settings->app_name;
        $receiver_email = $customer->email;
        $receiver_name = $customer->fullName;
        $merchant_id = $merchant->merchantid;

        // full-claim message.
        if( $redeem->claimed == $redeem->quantity ) $flag = 'customer-reward-claim-full';

        // partial-claim message
        else $flag = 'customer-reward-claim-partial';

        $datetime = Carbon::createFromTimestamp(strtotime($transaction->timestamp));
        $data['subject_app_name'] = $merchant->settings->app_name;
        $data['name'] = $customer->fullName;
        $data['datetime'] = $datetime->format('m-d-Y, g:i:s A');
        $data['quantity'] = $redeem->quantity;
        $data['reward_name'] = $transaction->reward['name'];
        $data['branch_name'] = $transaction->employee->branch->branchName;
        $data['points_name'] = $merchant->settings->points_name;
        $data['transaction_reference'] = $transaction->transactionReferenceCode;
        $data['claimed'] = $redeem->claimed;

        $this->send_email( $sender_email, $sender_name, $receiver_email, $receiver_name, $merchant_id, $flag, $data );

    }

    public function claimPunchcardReward( $transaction ){

        $merchant = $transaction->customer->merchant;
        $customer = $transaction->customer;

        $sender_email = $merchant->no_reply_email;
        $sender_name = $merchant->settings->app_name;
        $receiver_email = $customer->email;
        $receiver_name = $customer->fullName;
        $merchant_id = $merchant->merchantid;
        $flag = 'customer-punchcard-reward-claim';

        $datetime = Carbon::createFromTimestamp(strtotime($transaction->timestamp));
        $data['subject_business_name'] = $merchant->settings->app_name;
        $data['name'] = $customer->fullName;
        $data['datetime'] = $datetime->format('m-d-Y, g:i:s A');
        $data['reward_name'] = $transaction->reward->name;
        $data['branch_name'] = $transaction->employee->branch->branchName;
        $data['points_name'] = $merchant->settings->points_name;
        $data['transaction_reference'] = $transaction->transaction_ref;

        $this->send_email( $sender_email, $sender_name, $receiver_email, $receiver_name, $merchant_id, $flag, $data );

    }

    public function voidEarn( $transaction ){

        $merchant = $transaction->customer->merchant;
        $customer = $transaction->customer;

        $sender_email = $merchant->no_reply_email;
        $sender_name = $merchant->settings->app_name;
        $receiver_email = $customer->email;
        $receiver_name = $customer->fullName;
        $merchant_id = $merchant->merchantid;
        $flag = 'customer-void-earn';

        $datetime = Carbon::createFromTimestamp(strtotime($transaction->timestamp));
        $data['subject_app_name'] = $merchant->settings->app_name;
        $data['name'] = $customer->fullName;
        $data['datetime'] = $datetime->format('m-d-Y, g:i:s A');
        $data['voided_points'] = $transaction->voided_points ? $transaction->voided_points : $transaction->pointsEarned;
        $data['voided_amount'] = $transaction->voided_amount ? $transaction->voided_amount : $transaction->amountPaidWithCash;
        $data['branch_name'] = $transaction->employee->branch->branchName;
        $data['balance_points'] = number_format( (float) $customer->points->current, 2);
        $data['points_name'] = $merchant->settings->points_name;
        $data['transaction_reference'] = $transaction->transactionReferenceCode;

        $this->send_email( $sender_email, $sender_name, $receiver_email, $receiver_name, $merchant_id, $flag, $data );

    }

    public function voidRedeem( $transaction ){

        $merchant = $transaction->customer->merchant;
        $customer = $transaction->customer;
        $redeem = $transaction->redeem;

        $sender_email = $merchant->no_reply_email;
        $sender_name = $merchant->settings->app_name;
        $receiver_email = $customer->email;
        $receiver_name = $customer->fullName;
        $merchant_id = $merchant->merchantid;
        $flag = 'customer-void-redeem';

        $datetime = Carbon::createFromTimestamp(strtotime($transaction->timestamp));
        $data['subject_app_name'] = $merchant->settings->app_name;
        $data['name'] = $customer->fullName;
        $data['datetime'] = $datetime->format('m-d-Y, g:i:s A');
        $data['voided_points'] = $transaction->pointsRedeem;
        $data['points_name'] = $merchant->settings->points_name;
        $data['quantity'] = $redeem->quantity;
        $data['reward_name'] = $transaction->reward['name'];
        $data['branch_name'] = $transaction->employee->branch->branchName;
        $data['balance_points'] = number_format( (float) $customer->points->current, 2);
        $data['transaction_reference'] = $transaction->transactionReferenceCode;

        $this->send_email( $sender_email, $sender_name, $receiver_email, $receiver_name, $merchant_id, $flag, $data );

    }

    public function voidPaypoints( $transaction ){

        $merchant = $transaction->customer->merchant;
        $customer = $transaction->customer;

        $sender_email = $merchant->no_reply_email;
        $sender_name = $merchant->settings->app_name;
        $receiver_email = $customer->email;
        $receiver_name = $customer->fullName;
        $merchant_id = $merchant->merchantid;
        $flag = 'customer-void-pay-points';

        $datetime = Carbon::createFromTimestamp(strtotime($transaction->timestamp));
        $data['subject_app_name'] = $merchant->settings->app_name;
        $data['name'] = $customer->fullName;
        $data['datetime'] = $datetime->format('m-d-Y, g:i:s A');
        $data['total_amount_purchased'] =  $transaction->amountPaidWithCash;
        $data['points_burn'] = $transaction->pointsBurn;
        $data['points_name'] = $merchant->settings->points_name;
        $data['branch_name'] = $transaction->employee->branch->branchName;
        $data['balance_points'] = number_format( (float) $customer->points->current, 2);
        $data['transaction_reference'] = $transaction->transactionReferenceCode;

        $this->send_email( $sender_email, $sender_name, $receiver_email, $receiver_name, $merchant_id, $flag, $data );

    }

    public function transferPoints( $transaction, $receiver ){

        $merchant = $transaction->customer->merchant;
        $customer = $transaction->customer;

        $sender_email = $merchant->no_reply_email;
        $sender_name = $merchant->settings->app_name;
        $receiver_email = $customer->email;
        $receiver_name = $customer->fullName;
        $merchant_id = $merchant->merchantid;
        $flag = 'customer-points-transfer';

        $datetime = Carbon::createFromTimestamp(strtotime($transaction->timestamp));
        $data['subject_app_name'] = $merchant->settings->app_name;
        $data['name'] = $customer->fullName;
        $data['datetime'] = $datetime->format('m-d-Y, g:i:s A');
        $data['points'] = number_format( (float) $transaction->pointsTransferred, 2);
        $data['points_name'] = $merchant->settings->points_name;
        $data['receiver_email'] = $receiver->email;
        $data['transaction_date'] = $datetime->format('m-d-Y, g:i:s A');
        $data['balance_points'] = number_format( (float) $customer->points->current, 2);;
        $data['transaction_reference'] = $transaction->transactionReferenceCode;

        $this->send_email( $sender_email, $sender_name, $receiver_email, $receiver_name, $merchant_id, $flag, $data );

    }

    public function transferPointsReceiverSms( $transaction, $receiver ){

        $merchant = $transaction->customer->merchant;
        $customer = $transaction->customer;

        $sender_email = $merchant->no_reply_email;
        $sender_name = $merchant->settings->app_name;
        $receiver_email = $receiver->email;
        $receiver_name = $receiver->fullName;
        $merchant_id = $merchant->merchantid;
        $flag = 'customer-points-receive';

        $datetime = Carbon::createFromTimestamp(strtotime($transaction->timestamp));
        $data['subject_app_name'] = $merchant->settings->app_name;
        $data['name'] = $receiver->fullName;
        $data['datetime'] = $datetime->format('m-d-Y, g:i:s A');
        $data['points'] = number_format( (float) $transaction->pointsTransferred, 2);
        $data['points_name'] = $merchant->settings->points_name;
        $data['sender_name'] = $customer->fullName;
        $data['transaction_date'] = $datetime->format('m-d-Y, g:i:s A');
        $data['receiver_balance_points'] = number_format( (float) $receiver->points->current, 2);;
        $data['transaction_reference'] = $transaction->transactionReferenceCode;

        $this->send_email( $sender_email, $sender_name, $receiver_email, $receiver_name, $merchant_id, $flag, $data );

    }

    public function redeemAmaxReward( $transaction, $promo ){

        // amax reward is only for customers using mobile.

    }

    public function redeemAmaxRewardFailed( $customer ){

        // amax reward is only for customers using mobile.

    }

    public function voidPackage( $transaction ){

        $merchant = $transaction->customer->merchant;
        $customer = $transaction->customer;
        $class_package = $transaction->class_package()->withTrashed()->first();

        $sender_email = $merchant->no_reply_email;
        $sender_name = $merchant->settings->app_name;
        $receiver_email = $customer->email;
        $receiver_name = $customer->fullName;
        $merchant_id = $merchant->merchantid;
        $flag = 'customer-void-package';

        $datetime = Carbon::createFromTimestamp(strtotime($transaction->timestamp));
        $data['subject_app_name'] = $merchant->settings->app_name;
        $data['name'] = $customer->fullName;
        $data['datetime'] = $datetime->format('m-d-Y, g:i:s A');
        $data['package_name'] = $class_package->package->name;
        $data['transaction_reference'] = $transaction->transactionReferenceCode;

        $this->send_email( $sender_email, $sender_name, $receiver_email, $receiver_name, $merchant_id, $flag, $data );

    }

    public function send_notification( $merchant, $customer, $flag, $data = [] ){

        $sender_email = $merchant->no_reply_email;
        $sender_name = $merchant->settings->app_name;
        $receiver_email = $customer->email;
        $receiver_name = $customer->fullName;
        $merchant_id = $merchant->merchantid;

        $this->send_email( $sender_email, $sender_name, $receiver_email, $receiver_name, $merchant_id, $flag, $data );

    }

    public function invite( $merchant, $customer, $mobile_numbers, $emails ){

        $sender_email = $merchant->no_reply_email;
        $sender_name = $merchant->settings->app_name;
        $merchant_id = $merchant->merchantid;
        $flag = 'customer-invite';

        $data['subject_app_name'] = $merchant->settings->app_name;
        $data['customer_name'] = $customer->fullName;
        $data['program_name'] = $merchant->settings->program_name;
        $data['app_name'] = $merchant->settings->app_name;
        $data['vanity_url'] = $merchant->settings->vanity_url;
        $data['referral_code'] = $customer->referral_code;
        $data['points_name'] = $merchant->settings->points_name;

        foreach( $emails as $email ){

            $receiver_email = $email;
            $receiver_name = $email;

            $this->send_email( $sender_email, $sender_name, $receiver_email, $receiver_name, $merchant_id, $flag, $data );
            
        }
        
    }
}
