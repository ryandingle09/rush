<?php

namespace Rush\Modules\Email\Repositories;

use Rush\Modules\Repository\RepositoryInterface;

/**
 * The EmailLogInterface contains unique method signatures related to EmailLog object
 */
interface EmailLogInterface extends RepositoryInterface
{
}
