<?php

namespace Rush\Modules\Email\Repositories;

use Rush\Modules\Broadcasting\Repositories\MessageRepository;
use Rush\Modules\Email\Models\EmailModel;
use Rush\Modules\Email\Models\NotificationEmailMessageModel;
use Rush\Modules\Email\Models\NotificationEmailLogModel;
use Rush\Modules\Email\Repositories\EmailInterface;

/**
 * A repository class would encapsulate each Eloquent Model class
 * and be responsible for CRUD operations on the database.
 */
class EmailRepository extends MessageRepository implements EmailInterface
{
    public function __construct(EmailModel $emailModel)
    {
        parent::__construct($emailModel);
    }

    /**
     * [getEmailBySlug description]
     * @param  int $emailId
     * @param  string $slug    url friendly string
     * @return mixed          Email object
     */
    public function getEmailBySlug($emailId, $slug)
    {
        return $this->model->where(['id' => $emailId, 'url_slug' => $slug])->first();
    }

    public function get_email_message( $merchant_id, $flag ){

        return NotificationEmailMessageModel
            ::where('merchant_id', $merchant_id)
            ->where('type', $flag)
            ->select('subject', 'message', 'template')
            ->first();

    }

    public function log_email( $merchant_id, $type = null, $response = null, $sender = null, $receiver = null, $subject = null, $contents = null, $data = [] ){

        $log = new NotificationEmailLogModel;
        $log->merchant_id = $merchant_id;
        $log->type = $type;
        $log->response = $response;
        $log->sender = $sender;
        $log->receiver = $receiver;
        $log->subject = $subject;
        $log->contents = $contents;
        $log->data = json_encode( $data );
        $log->save();

    }
}
