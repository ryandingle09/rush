<?php

namespace Rush\Modules\Email\Repositories;

use Rush\Modules\Broadcasting\Repositories\MessageInterface;

/**
 * The EmailInterface contains unique method signatures related to Email object
 */
interface EmailInterface extends MessageInterface
{
    public function getEmailBySlug($emailId, $slug);
}
