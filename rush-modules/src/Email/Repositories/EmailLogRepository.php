<?php

namespace Rush\Modules\Email\Repositories;

use Rush\Modules\Email\Models\EmailLogModel;
use Rush\Modules\Email\Repositories\EmailLogInterface;
use Rush\Modules\Repository\AbstractRepository;

/**
 * A repository class would encapsulate each Eloquent Model class
 * and be responsible for CRUD operations on the database.
 */
class EmailLogRepository extends AbstractRepository implements EmailLogInterface
{
    public function __construct(EmailLogModel $emailLogModel)
    {
        parent::__construct($emailLogModel);
    }
}
