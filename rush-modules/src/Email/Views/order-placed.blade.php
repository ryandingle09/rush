<div style="background-color: #eee; padding: 10px; text-align: center;">
    <img height="70px" src="{{ $logo_url }}" />
</div>
<br/>

<p>Hello, {{ $customer_name }}!</p>
<p>Thank you for your order from {{ $business_name }}. Your order confirmation is below.</p>
<p><b>Order #{{ $order_number }}</b></p>

<table width="50%">
    <tr>
        <td><b>Delivery Information:</b></td>
        <td><b>Special Instruction:</b></td>
    </tr>
    <tr>
        <td valign="top">
            {{ $delivery_name }}<br/>
            {{ $delivery_floor }}, {{ $delivery_building }}<br/>
            {{ $delivery_street }}<br/>
            {{ $delivery_mobile_number }}
        </td>
        <td valign="top">
            Change for {{ $payment_change_for }}<br/>
            {{ $payment_notes }}
        </td>
    </tr>
</table>
<br/><br/>

<table width="60%" style="border-collapse: collapse;">
    <tr style="background-color: #eee;">
        <td style="border: 1px #ddd solid; padding: 15px 25px;"><b>Item</b></td>
        <td style="border: 1px #ddd solid; padding: 15px 25px;" align="center" width="1%"><b>Qty</b></td>
        <td style="border: 1px #ddd solid; padding: 15px 25px;" align="center" width="1%"><b>Price</b></td>
        <td style="border: 1px #ddd solid; padding: 15px 25px;" align="center" width="1%"><b>Subtotal</b></td>
    </tr>

    @foreach( $order_items as $order_item )
        <tr>
            <td style="border: 1px #ddd solid; padding: 15px 25px;">{{ $order_item->item->name }}</td>
            <td style="border: 1px #ddd solid; padding: 15px 25px;" align="center">{{ $order_item->quantity }}</td>
            <td style="border: 1px #ddd solid; padding: 15px 25px;" align="center">{{ $order_item->amount }}</td>
            <td style="border: 1px #ddd solid; padding: 15px 25px;" align="right">{{ number_format($order_item->quantity * $order_item->amount, 2) }}</td>
        </tr>
    @endforeach

    <tr>
        <td style="border: 1px #ddd solid; padding: 15px 25px;" align="right" colspan="3"><b>Sub Total </b></td>
        <td style="border: 1px #ddd solid; padding: 15px 25px;" align="right">{{ number_format($total_amount, 2) }}</td>
    </tr>
    <tr>
        <td style="border: 1px #ddd solid; padding: 15px 25px;" align="right" colspan="3"><b>Promo code: ({{ $promo_code }})</b></td>
        <td style="border: 1px #ddd solid; padding: 15px 25px;" align="right">{{ number_format($promo_code_discount_amount, 2) }}</td>
    </tr>
    <tr>
        <td style="border: 1px #ddd solid; padding: 15px 25px;" align="right" colspan="3"><b>TOTAL</b></td>
        <td style="border: 1px #ddd solid; padding: 15px 25px;" align="right">{{ number_format($net_amount, 2) }}</td>
    </tr>
</table>
<br/><br/>

<p>This is a system generated message. PLEASE DO NOT REPLY TO THIS EMAIL. If you have any questions about your order, please call us at {{ $business_contact }}.</p>
<p>Thank you!</p>
<p><b>{{ $business_name }}</b></p>