<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width" />
</head>
<body>
    <table style="width: 100%;background-color: #f7f7f7;padding:0;">
        <tr>
            <td style="padding:30px 0;">
                <table id="th-ticket-template" style="margin: 0 auto;
                border-collapse: collapse;
                font-family: 'roboto', 'helvetica neue','helvetica','arial','sans-serif';
                color: #333;
                width: 640px;
                table-layout: fixed;">
                <tr style="background-color: #fff;">
                    <td colspan='4' style="padding: 0;">    
                        <img src="{{ url('app/assets/email/rush-email-header.jpg') }}" style="width:100%;"/>
                    </td>
                </tr>
                <tr style="background-color: #fff;">
                    <td colspan='4' style="padding: 0px 24px;font-size: 13px;line-height: 24px;">
                        <span>Your voucher codes for the item below are almost gone. Please replenish soon.</span>
                    </td>
                </tr>
                <tr style="background-color: #fff;">
                    <td colspan=4 style="padding: 12px 24px;font-size: 13px;line-height: 24px;">
                        <span style="padding-left:40px;"><b>Voucher Name: </b><span> {{ $voucher_name }}</span></span><br>
                        <span style="padding-left:40px;"><b>Branch: </b><span> {{ $branch_name }}</span></span>
                    </td>
                </tr>
                <tr style="background-color: #fff;">
                    <td colspan=4 style="padding: 12px 24px;font-size: 13px;line-height: 24px;">
                        <span style="padding-left:40px;"><a href="{{ env('APP_URL') }}">Login to the Merchant Dashboard</a></span>
                    </td>
                </tr>
                <tr style="background-color: #fff;">
                    <td colspan='4' style="padding: 12px 24px 32px;">
                        <p style="color:gray;font-style: italic;font-size:12px;">This is a system-generated email.</p>
                    </td>
                </tr>
                <tr style=""><td colspan="4">&nbsp;</td></tr>
                <tr>
                    <td colspan='4' style="text-align: center;">
                        <a href="https://www.facebook.com/rushrewardsph"><img src="{{ url('app/assets/email/fb-icon.png') }}" style="width:24px"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="{{ env('APP_URL') }}"><img src="{{ url('app/assets/email/link-icon.png') }}" style="width:24px;"></a>
                    </td>
                </tr>
                <tr style=""><td colspan="4">&nbsp;</td></tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>