<?php

namespace Rush\Modules\Email\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Email\Models\EmailLogModel;
use Rush\Modules\Email\Repositories\EmailLogRepository;

/**
* Register our Repository with Laravel
*/
class EmailLogRepositoryServiceProvider extends ServiceProvider
{
    /**
    * Registers the EmailLogInterface with Laravels IoC Container
    */
    public function register()
    {
        // Bind the returned class to the namespace 'Repositories\EmailLogInterface
        $this->app->bind(
            '\Rush\Modules\Email\Repositories\EmailLogInterface',
            function($app) {
                return new EmailLogRepository(new EmailLogModel());
            }
        );
    }
}
