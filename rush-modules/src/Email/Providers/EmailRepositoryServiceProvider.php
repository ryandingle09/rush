<?php

namespace Rush\Modules\Email\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Email\Models\EmailModel;
use Rush\Modules\Email\Repositories\EmailRepository;

/**
* Register our Repository with Laravel
*/
class EmailRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // register package views
        $this->loadViewsFrom(__DIR__.'/../Views', 'email');
    }

    /**
    * Registers the EmailInterface with Laravels IoC Container
    */
    public function register()
    {
        // Bind the returned class to the namespace 'Repositories\EmailInterface
        $this->app->bind(
            '\Rush\Modules\Email\Repositories\EmailInterface',
            function($app) {
                return new EmailRepository(new EmailModel());
            }
        );
    }
}
