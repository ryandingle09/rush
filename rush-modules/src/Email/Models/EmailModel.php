<?php

namespace Rush\Modules\Email\Models;

use Rush\Modules\Broadcasting\Models\MessageModel;
use Rush\Modules\Helpers\Facades\StorageHelper;


class EmailModel extends MessageModel
{
    protected $table = "email";

    protected $fillable = [
        'status',
        'publish_date',
        'message',
        'merchant_id',
        'segment_id',
        'banner',
        'url_slug',
        'subject'
    ];

    public function getBannerAttribute($value)
    {
        return StorageHelper::repositoryUrlForView($value);
    }
}