<?php

namespace Rush\Modules\Email\Models;

use Eloquent;

class EmailLogModel extends Eloquent
{
    protected $table = "email_logs";
    protected $primaryKey = "id";
    protected $fillable = array(
        'merchant_id',
        'email_id',
        'email_address',
        'transaction_type'
    );
}