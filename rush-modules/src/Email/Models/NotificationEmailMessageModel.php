<?php

namespace Rush\Modules\Email\Models;

use Illuminate\Database\Eloquent\Model;

class NotificationEmailMessageModel extends Model{
    protected $table = 'notification_email_messages';
}