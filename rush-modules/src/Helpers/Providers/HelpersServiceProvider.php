<?php

namespace Rush\Modules\Helpers\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Helpers\Facades\StorageHelper;
use Rush\Modules\Helpers\Storage\StorageHelperCloud;
use Rush\Modules\Helpers\Storage\StorageHelperLocal;
use Validator;

class HelpersServiceProvider extends ServiceProvider
{
    protected $commands = [
        'Rush\Modules\Helpers\Console\Commands\CopyLocalToCloudCommand',
    ];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('CmsHelper', 'Rush\Modules\Helpers\CmsHelper');
        $loader->alias('MenuHelper', 'Rush\Modules\Helpers\MenuHelper');
        $loader->alias('JobHelper', 'Rush\Modules\Helpers\JobHelper');
        $loader->alias('ToolHelper', 'Rush\Modules\Helpers\ToolHelper');
        $loader->alias('ClassManagementToolHelper', 'Rush\Modules\Helpers\ClassManagementToolHelper');

        Validator::extend('mobilenumber', 'Rush\Modules\Helpers\CustomValidator@validateMobileNumber');
        Validator::extend('uniqueBranchName', 'Rush\Modules\Helpers\CustomValidator@validateUniqueBranchName');
    }

    public function register()
    {
        $this->commands($this->commands);

        if (env('S3_DRIVER', 'local') == 'local') {
            $this->app->singleton('storageHelper', StorageHelperLocal::class);
            // $this->app->bind('storageHelper', StorageHelperLocal::class);
        } else {
            $this->app->singleton('storageHelper', StorageHelperCloud::class);
            // $this->app->bind('storageHelper', StorageHelperCloud::class);
        }
    }
}
