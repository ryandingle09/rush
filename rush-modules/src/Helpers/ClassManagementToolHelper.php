<?php

namespace Rush\Modules\Helpers;

use Rush\Modules\Merchant\Models\MerchantModel;

final class ClassManagementToolHelper
{
    const TOOL_PACKAGE_ID = 1;
    const TOOL_INSTRUCTOR_ID = 2;
    const TOOL_INSTRUCTOR_WITH_BRANCH_ID = 3;
    const TOOL_CLASS_ID = 4;
    const TOOL_ACTIVITY_ID = 5;
    const TOOL_SCHEDULE_ID = 6;
    const TOOL_SCHEDULE_LIST_ID = 7;

    public static function isAllowed($id, &$allowed)
    {
        $tools = [];
        if ($allowed instanceof MerchantModel) {
            $tools = $allowed->class_management_tools_allowed;
        } else if (is_array($allowed)) {
            $tools = $allowed;
        }

        return in_array($id, $tools);
    }
}