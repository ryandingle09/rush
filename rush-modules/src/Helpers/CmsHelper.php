<?php
namespace Rush\Modules\Helpers;

class CmsHelper
{
    // use this system ID when merchant object is not available
    const SYSTEM_MERCHANT_ID = 175;

    /**
     * Produces data-id="1" data-name="Juan"...
     * @param  array $fields
     * @return string
     */
    public static function getDomDataFormat($fields)
    {
        $domData = [];
        foreach ($fields as $key => $value) {
            $domData[] = "data-$key=\"$value\"";
        }

        return implode(' ', $domData);
    }

    public static function getArrayByKeys($sourceArray, $keys)
    {
        return array_intersect_key($sourceArray, array_flip($keys));
    }
}