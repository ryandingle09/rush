<?php

namespace Rush\Modules\Helpers;

use Rush\Modules\Merchant\Services\BranchService;

class CustomValidator
{
    protected $branchService;

    public function __construct(BranchService $branchService)
    {
        $this->branchService = $branchService;
    }

    public function validateMobileNumber($attribute, $value, $parameters, $validator)
    {
        if(preg_match('/^0\d{10}$/', $value))
            return true;

        return false;
    }

    /**
     * Validation to avoid duplicates in BranchNames
     * @param  string $attribute       the field being tested
     * @param  string $value           the submitted value subject to validation
     * @param  array $parameters       Requires only 1 parameter as merchantId (2nd parameter is for ignoring certain ids)
     * @param  Validator $validator    validator object
     * @return boolean                 return false if branchName exists
     */
    public function validateUniqueBranchName($attribute, $value, $parameters, $validator)
    {
        $this->requireParameterCount(1, $parameters, 'branchName');

        $branch = $this->branchService->findByBranchName($value, $parameters[0], isset($parameters[1]) ? $parameters[1] : null);
        if ($branch) {
            return false;
        }

        return true;
    }

    protected function requireParameterCount($count, $parameters, $rule)
    {
        if (count($parameters) < $count) {
            throw new InvalidArgumentException("Validation rule $rule requires at least $count parameters.");
        }
    }
}