<?php
namespace Rush\Modules\Helpers;

use Rush\Modules\Broadcasting\BroadcastHelper;
use Rush\Modules\Queue\Exception\JobProcessingException;

class JobHelper
{
    public static function start(&$job)
    {
        if ($job->status != BroadcastHelper::ACTIVE) {
            if ($job->status == BroadcastHelper::PROCESSING)
                throw new JobProcessingException("$job status is currently 'processing' wait for it to finish.");
            else
                throw new JobProcessingException("$job status is currently 'not available' to start.");
        } else {
            $job->status = BroadcastHelper::PROCESSING;
            $job->save();
        }
    }

    public static function reactivate(&$job, $e)
    {
        // set the job back to ACTIVE for non JobProcessingException
        if (!$e instanceof JobProcessingException) {
            $job->status = BroadcastHelper::ACTIVE;
            $job->save();
        }
    }
}