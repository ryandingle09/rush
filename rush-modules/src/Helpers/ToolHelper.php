<?php

namespace Rush\Modules\Helpers;

use Rush\Modules\Merchant\Models\MerchantModel;

final class ToolHelper
{
    const TOOL_SMS_ID = 1;
    const TOOL_PUSH_ID = 2;
    const TOOL_EMAIL_ID = 3;
    const TOOL_SEGMENT_ID = 4;
    const TOOL_EVENTS_PROMOS_ID = 5;
    const TOOL_ATTENDANCE_ID = 6;

    public static function isAllowed($id, &$allowed)
    {
        $tools = [];
        if ($allowed instanceof MerchantModel) {
            $tools = $allowed->broadcast_tools_allowed;
        } else if (is_array($allowed)) {
            $tools = $allowed;
        }

        return in_array($id, $tools);
    }
}