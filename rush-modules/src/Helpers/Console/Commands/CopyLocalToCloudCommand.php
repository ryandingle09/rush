<?php

namespace Rush\Modules\Helpers\Console\Commands;

use Illuminate\Console\Command;

class CopyLocalToCloudCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'storage:copy-local-to-cloud';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Copy Local to S3';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->moveFiles();
    }

    public function moveFiles()
    {
        $local = \Storage::disk('repository');
        $s3 = \Storage::disk('s3');

        try {
            $files = $local->allFiles();
            echo 'Local files count: ' . count($files) . PHP_EOL;
            $i = 0;
            foreach($files as $file) {
                $localFile = $local->get($file);
                $s3->put($file, $localFile, 'public');
                $i++;
            }
            echo 'Copied files count: ' . $i . PHP_EOL;
        } catch (Exception $e) {
            throw new Exception('Not able to store file, error details:' . $ex->getMessage());
        }
    }
}
