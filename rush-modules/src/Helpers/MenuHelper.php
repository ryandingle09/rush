<?php

namespace Rush\Modules\Helpers;

class MenuHelper
{

    public static function menu( $menu )
    {
        return view( $menu );
    }

    public static function generate()
    {
    	$merchant_id = self::getSession( 'user_id' );
    	$package_id = self::getSession( 'package_id' );
    	$user_type = self::getSession( 'user_type' );
    	$subuser_id = null;
    	$sql = "SELECT * FROM `modules` WHERE `package_id` = $package_id";
    	if ( $user_type == 1 )
        {
            $sql = 'SELECT m.id, m.code, m.name, m.url
                    FROM modules_merchant as mum
                    LEFT JOIN modules as m
                    ON mum.module_id = m.id
                    WHERE mum.merchant_id = ' . $merchant_id;
        }
        else if ( $user_type == 2 )
        {
    		$subuser_id = self::getSession( 'subuser_id' );
    		$sql = 'SELECT m.id, m.code, m.name, m.url
    				FROM merchant_user_modules as mum
    				LEFT JOIN modules as m
    				ON mum.modules_id = m.id
    				WHERE mum.user_id = ' . $subuser_id;
    	} else {

            $sql = 'SELECT m.id, m.code, m.name, m.url
                    FROM bcu_modules as mum
                    LEFT JOIN modules as m
                    ON mum.modules_id = m.id
                    WHERE mum.merchant_id = ' . $merchant_id;
        }

    	$conn = self::create_connection();
		
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}

		$result = $conn->query($sql);
		$modules = null;
        $view = '';
		if ( $result->num_rows > 0)
		{
		    while( $row = $result->fetch_assoc() ) {
		    	$modules[] = $row;
		    }
            foreach( $modules as $module ) {
		  	   $view .= self::create_menu( $module, $package_id );	
            }
        }

		echo $view;
    }

    private static function create_connection()
    {
    	$servername = getenv('DB_HOST');
    	$username = getenv('DB_USERNAME');
    	$password = getenv('DB_PASSWORD');
    	$dbname = getenv('DB_DATABASE');
    	$conn = new \mysqli($servername, $username, $password, $dbname);
    	return $conn;
    }

    

    private static function create_menu( $data = null, $package_id )
    {
        return '<li class="' . $data['code'] .'">
            <a href="' . url( $data['url'] ) . '">
                <i class="icon" data-toggle="tooltip" data-placement="right" title="'. $data['name'] .'"></i>
                <span>' . $data['name'] . '</span>
            </a>
            </li>
       ';
    }

    private static function getSession( $variable )
    {
    	if ( function_exists( "session" ) ) {
    		if ( !empty( session($variable) ) ) {
    			return session($variable);
    		} else  {
    			return null;
    		}
    	} else {

    	}
    }

    public static function admin_generate()
    {
        $admin_id = self::getSession('admin_id');
        $admin_type = self::getSession('admin_type');
        $sql = "SELECT * FROM `admin_modules`";
        if ( $admin_type != 1 ) {
            $sql = 'SELECT m.id, m.code, m.name, m.url
                    FROM bd_admin_modules as mum
                    LEFT JOIN admin_modules as m
                    ON mum.module_id = m.id
                    WHERE mum.bd_id = ' . $admin_id;
        }
        $conn = self::create_connection();
        
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        $result = $conn->query($sql);
        $modules = null;
        if ( $result->num_rows > 0)
        {
            while( $row = $result->fetch_assoc() ) {
                $modules[] = $row;
            }
        }

        $view = '';
        foreach( $modules as $module ) {
            $view .= self::create_admin_menu( $module ); 
        }

        echo $view;
    }

    private static function create_admin_menu( $data = null )
    {
        return '<li class="' . $data['code'] .'">
            <a href="' . url( 'admin/'. $data['url'] ) . '">
                <i class="icon" data-toggle="tooltip" data-placement="right" title="'. $data['name'] .'"></i>
                <span>' . $data['name'] . '</span>
            </a>
            </li>
       ';
    }
    
}