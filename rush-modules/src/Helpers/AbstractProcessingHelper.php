<?php

namespace Rush\Modules\Helpers;

abstract class AbstractProcessingHelper
{
    const IGNORED = 0;
    const INACTIVE = 1;
    const ACTIVE = 2;
    const PROCESSED = 3;
    const PROCESSING = 4;

    public static $status = [
        '0' => 'Ignored',
        '1' => 'Inactive',
        '2' => 'Active',
        '3' => 'Processed',
        '4' => 'Processing'
    ];
    public static function getStatus($statusId)
    {
        return self::$status[$statusId];
    }

    private function __construct(){
        throw new Exception("AbstractProcessingHelper not instantiable");
    }
}