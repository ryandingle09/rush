<?php
namespace Rush\Modules\Helpers;

use DB;

class LogArtisanCommandHelper
{
    private static $log_id;

    public static function start_log( $signature = '', $class = '' ){

        self::$log_id = DB::table('log_artisan_command')->insertGetId([
            'repo_env' => ENV('REPO_ENV', 'api'),
            'signature' => $signature,
            'class' => $class,
            'start_time' => date('Y-m-d H:i:s')
        ]);

    }

    public static function end_log( $note = null ){

        DB::table('log_artisan_command')
            ->where('id', self::$log_id)
            ->update(['end_time' => date('Y-m-d H:i:s'), 'note' => $note]);

    }
}