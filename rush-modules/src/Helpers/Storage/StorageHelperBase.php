<?php
namespace Rush\Modules\Helpers\Storage;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

/**
 * StorageHelper aims to consolidate all interaction with the 'repository' folder
 * with the intention to prepare Rush apps for adapting s3 as storage
 */
class StorageHelperBase
{
    /**
     * return Local Storage pointed at Rush Container
     * @return mixed
     */
    public static function getRushDisk()
    {
        return Storage::disk('rush');
    }

    /**
     * returns the path of the Rush container
     * @param  string $path transform path relative to rush_path
     * @return string
     */
    public static function rush_path($path = '')
    {
        return base_path('..').($path ? DIRECTORY_SEPARATOR.$path : $path);
    }

    /**
     * return local path
     * @param  string $path
     * @return string
     */
    public static function repository_path($path = '')
    {
        return rtrim(self::getRepositoryDisk()->getAdapter()->getPathPrefix(), '/\\').($path ? DIRECTORY_SEPARATOR.$path : $path);
    }

    /**
     * Generate CodeIgniter style filenames see set_filename of Upload library
     * @return string
     */
    public static function generateFilename()
    {
        return md5(uniqid(mt_rand()));
    }
}