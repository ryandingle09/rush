<?php
namespace Rush\Modules\Helpers\Storage;

/**
 * StorageHelper aims to consolidate all interaction with the 'repository' folder
 * with the intention to prepare Rush apps for adapting s3 as storage
 */
interface StorageHelperInterface
{
    /**
     * return disk for repository
     * @return Storage
     */
    public static function getRepositoryDisk();

    /**
     * return local path
     * @param  string $path
     * @return string
     */
    public static function repository_path($path = '');

    /**
     * Move uploaded Image from tmp location to destination file
     * @param  String $subPath         A path inside the repository folder
     * @param  String $fileName        Must match the POST field name of the uploaded file
     * @param  Illuminate\Http\UploadedFile $uploadedFile Optional parameter
     * @return String                  The repository url
     */
    public static function move($subPath, $fileName, $uploadedFile = null);

    /**
     * return the url path starting with '/repository/'
     * @param  String $file A file inside the repository folder
     * @return String       The repository url path
     */
    public static function repositoryUrl($file);

    /**
     * Use to rebase url paths stored in the DB which starts with /repository/
     * @param  String $path path to rebase
     * @return String       rebased path
     */
    public static function rebasePathToRepository($path);

    /**
     * Delete from repository using our Repository disk
     * @param  String $path Url Path from DB
     * @return boolean
     */
    public static function deleteFromRepository($path);

    /**
     * Provide the local path of repository URL paths
     * @param  String $url
     * @return String
     */
    public static function repositoryUrlToPath($url);

    public static function repositoryUrlForView($url, $full = false);

    public static function repositoryUrlToStream($url);

    public static function metadata($url, $key );
}