<?php
namespace Rush\Modules\Helpers\Storage;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Rush\Modules\Helpers\Storage\StorageHelperBase;
use Rush\Modules\Helpers\Storage\StorageHelperInterface;

/**
 * StorageHelper aims to consolidate all interaction with the 'repository' folder
 * with the intention to prepare Rush apps for adapting s3 as storage
 */
class StorageHelperLocal extends StorageHelperBase implements StorageHelperInterface
{
    /**
     * return disk for repository
     * @return Storage
     */
    public static function getRepositoryDisk()
    {
        return Storage::disk('repository');
    }

    /**
     * return local path
     * @param  string $path
     * @return string
     */
    public static function repository_path($path = '')
    {
        return rtrim(self::getRepositoryDisk()->getAdapter()->getPathPrefix(), '/\\').($path ? DIRECTORY_SEPARATOR.$path : $path);
    }

    /**
     * Move uploaded Image from tmp location to destination file
     * @param  String $subPath         A path inside the repository folder
     * @param  String $fileName        Must match the POST field name of the uploaded file
     * @param  Illuminate\Http\UploadedFile $uploadedFile Optional parameter
     * @return String                  The repository url
     */
    public static function move($subPath, $fileName, $uploadedFile = null)
    {
        $newFile = '';
        $file = false;
        if (get_class($uploadedFile) == 'Illuminate\Http\UploadedFile') {
            $file = $uploadedFile;
        } else if (Input::hasFile($fileName)) {
            $file = Input::file($fileName);
        }

        if ($file) {
            $repository = self::getRepositoryDisk();
            $fileExtension = $file->getClientOriginalExtension();

            if (!$uploadedFile) {
                while (($fileName = self::generateFilename().'.'.$fileExtension)
                    && $repository->exists($newFile = $subPath.'/'.$fileName)) {
                    // keep generating a fileName until we get one that does not exist
                }
            } else {
                $newFile = $subPath.'/'.$fileName;
            }

            $file->move($repository->getDriver()->getAdapter()->getPathPrefix().$subPath, $newFile);

            return self::repositoryUrl($newFile);
        } else {
            throw new \InvalidArgumentException("$fileName does not match an uploaded file.");
        }
    }

    /**
     * [repositoryUrl description]
     * @param  String $file A file inside the repository folder
     * @return String       The repository url path
     */
    public static function repositoryUrl($file)
    {
        return str_replace('/storage/', '/repository/', self::getRepositoryDisk()->url($file));
    }

    /**
     * Use to rebase url paths stored in the DB which starts with /repository/
     * @param  String $path path to rebase
     * @return String       rebased path
     */
    public static function rebasePathToRepository($path)
    {
        return str_replace('/repository/', '', $path);
    }

    /**
     * Delete from repository using our Repository disk
     * @param  String $path Url Path from DB
     * @return boolean
     */
    public static function deleteFromRepository($path)
    {
        self::getRepositoryDisk()->delete(self::rebasePathToRepository($path));
    }

    /**
     * Provide the local path of repository URL paths
     * @param  String $url
     * @return String
     */
    public static function repositoryUrlToPath($url)
    {
        return self::repository_path(self::rebasePathToRepository($url));
    }

    /**
     * Provide the url path for view
     * @param  String $url
     * @return String
     */
    public static function repositoryUrlForView($url, $full = false)
    {
        return (boolean) $full ? config('app.url').$url : $url;
    }

    public static function repositoryUrlToStream($url)
    {
        return self::getRepositoryDisk()->getDriver()->readStream(self::rebasePathToRepository($url));
    }

    public static function metadata($url, $key = null)
    {
        $metadata = [
            'extension' => pathinfo(self::rebasePathToRepository($url), PATHINFO_EXTENSION),
            'filename' => pathinfo(self::rebasePathToRepository($url), PATHINFO_FILENAME ),
        ];

        return array_key_exists($key, $metadata) ? $metadata[$key] : $metadata;
    }
}