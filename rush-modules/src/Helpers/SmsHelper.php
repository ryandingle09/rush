<?php
namespace Rush\Modules\Helpers;

use Ixudra\Curl\CurlService;

class SmsHelper
{
    protected $base_endpoint = "https://devapi.globelabs.com.ph";
    protected $send_endpoint;
    protected $curlService;
    public $shortcode;
    public $app_id;
    public $app_secret;
    public $passphrase;

    public function __construct($shortcode, $app_id, $app_secret, $passphrase)
    {
        $this->shortcode = $shortcode;
        $this->app_id = $app_id;
        $this->app_secret = $app_secret;
        $this->passphrase = $passphrase;

        $this->send_endpoint = $this->base_endpoint . "/smsmessaging/v1/outbound/{$this->shortcode}/requests?app_id={$this->app_id}&app_secret={$this->app_secret}";
        $this->curlService = new CurlService();
    }

    public function send($mobile_no, $message)
    {
        return $this->curlService->to($this->send_endpoint)
                ->withData(['address' => $mobile_no, 'message' => $message, 'passphrase' => $this->passphrase])
                ->post();
    }
}