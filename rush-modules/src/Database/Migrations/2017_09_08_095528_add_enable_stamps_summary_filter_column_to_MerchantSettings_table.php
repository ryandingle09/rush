<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEnableStampsSummaryFilterColumnToMerchantSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('MerchantSettings', function (Blueprint $table) {
            $table->dropColumn('rewards_stamps_redemption_conversion');
            $table->dropColumn('rewards_stamps_redemption_conversion_credit');
            $table->boolean('enable_stamps_summary_punchcard_filter')->default(false);
            $table->boolean('enable_stamps_summary_punchcard_redemption')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('MerchantSettings', function (Blueprint $table) {
            $table->dropColumn('enable_stamps_summary_punchcard_filter');
            $table->dropColumn('enable_stamps_summary_punchcard_redemption');
        });
    }
}