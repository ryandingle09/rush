<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CustomerReports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('merchant_id');
            $table->integer('customer_id');
            $table->string('mobile_number', 45);
            $table->string('type', 10);
            $table->text('message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('CustomerReports');
    }
}
