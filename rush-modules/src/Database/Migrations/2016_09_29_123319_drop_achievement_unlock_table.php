<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropAchievementUnlockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::drop('AchievementsUnlock');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('AchievementsUnlock', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('merchant_id');
            $table->char('name', 255);
            $table->integer('points');
            $table->integer('multiplier');
            $table->date('from_date');
            $table->date('to_date');
            $table->dateTime('created_at');
            $table->integer('created_by');
            $table->integer('modified_by');
            $table->integer('is_deleted');
            $table->integer('deleted_by');
            $table->dateTime('deleted_at');
        });
    }
}
