<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CustomerPointsLevel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_points_levels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('merchant_id');
            $table->string('level_name');
            $table->float('points');
            $table->string('points_type');
            $table->string('expiration');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customer_points_levels');
    }
}
