<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddScheduleTimeColumnOnScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('class_schedules', function (Blueprint $table) {
            $table->integer('start_time');
            $table->integer('end_time');
            $table->date('start_date');
            $table->date('end_date');
            $table->boolean('is_repeat');
            $table->string('repeats');
            $table->integer('repeat_every');
            $table->integer('repeat_occurence');
            $table->string('repeat_on');
            $table->string('repeat_selection');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('class_schedules', function (Blueprint $table) {
            $table->dropColumn(['start_time', 'end_time', 'start_date', 'end_date', 'is_repeat', 'repeats', 'repeat_every', 'repeat_occurence', 'repeat_on', 'repeat_selection']);
        });
    }
}
