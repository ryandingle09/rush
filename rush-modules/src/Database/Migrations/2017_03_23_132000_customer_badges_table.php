<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CustomerBadgesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_badges', function (Blueprint $table) {
            $table->increments('id');
            $table->string('transactionReferenceCode', 45);
            $table->integer('customer_id')->unsigned();
            $table->string('badge_group', 100);
            $table->integer('badge_id')->unsigned();
            $table->string('badge_name', 255);
            $table->datetime('validity_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customer_badges');
    }
}
