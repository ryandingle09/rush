<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrNumberInSmsMessages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('notification_sms_messages')->where('type', 'earn-points')->update(['message' => "You have earned %points_earned% %points_name% point(s) from %branch_name% on %datetime%. Your new balance is %balance_points% %points_name% point(s). Keep collecting %points_name% points to redeem exciting rewards. Ref. No. %reference%. OR No. %or_number%"]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('notification_sms_messages')->where('type', 'earn-points')->update(['message' => "You have earned %points_earned% %points_name% point(s) from %branch_name% on %datetime%. Your new balance is %balance_points% %points_name% point(s). Keep collecting %points_name% points to redeem exciting rewards. Ref. No. %reference%"]);
    }
}
