<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GmoviesSmsFeatureBypassMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $gmovies_id = DB::table('Merchant')->where('business_system_link', 'gmovies')->value('merchantid');

        DB::table('merchant_generic_feature_bypass')->insert([
            [
                'merchant_id' => $gmovies_id,
                'endpoint_name' => 'customer_register',
                'feature' => 'sms',
            ],
            [
                'merchant_id' => $gmovies_id,
                'endpoint_name' => 'customer_points_earn',
                'feature' => 'sms',
            ],
            [
                'merchant_id' => $gmovies_id,
                'endpoint_name' => 'customer_points_pay',
                'feature' => 'sms',
            ],
            [
                'merchant_id' => $gmovies_id,
                'endpoint_name' => 'customer_rewards_redeem',
                'feature' => 'sms',
            ],
            [
                'merchant_id' => $gmovies_id,
                'endpoint_name' => 'customer_rewards_redeem',
                'feature' => 'amax_sms',
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $gmovies_id = DB::table('Merchant')->where('business_system_link', 'gmovies')->value('merchantid');
        DB::table('merchant_generic_feature_bypass')->where('merchant_id', $gmovies_id)->delete();
    }
}
