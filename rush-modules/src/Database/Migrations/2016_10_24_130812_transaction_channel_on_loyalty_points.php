<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Rush\Modules\Customer\Models\CustomerTransactionsModel;

class TransactionChannelOnLoyaltyPoints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $TransactionChannelOnLoyaltyPoints_file = 'TransactionChannelOnLoyaltyPoints.csv';
        $TransactionChannelOnLoyaltyPoints_csv = [];
        $transactions = CustomerTransactionsModel::all();
        foreach($transactions as $transaction)
        {
            if($transaction->channel == '' &&
                (   $transaction->transactionType == 'earn' ||
                    $transaction->transactionType == 'redeem' ||
                    $transaction->transactionType == 'paypoints' ||
                    $transaction->transactionType == 'void-earn' ||
                    $transaction->transactionType == 'void-redeem' ||
                    $transaction->transactionType == 'void-earn' ||
                    $transaction->transactionType == 'void-paypoints'
                ) ) {
                $transaction->channel = 'merchantapp';
                $transaction->save();
                $TransactionChannelOnLoyaltyPoints_csv[] = $transaction->id;
            }
        }
        Storage::disk('local')->put($TransactionChannelOnLoyaltyPoints_file, implode(',',$TransactionChannelOnLoyaltyPoints_csv));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $TransactionChannelOnLoyaltyPoints_file = 'TransactionChannelOnLoyaltyPoints.csv';
        $transaction_ids = explode(',',Storage::disk('local')->get($TransactionChannelOnLoyaltyPoints_file));
        foreach($transaction_ids as $id) {
            $transaction = CustomerTransactionsModel::find($id);
            $transaction->channel = null;
            $transaction->save();
        }
    }
}
