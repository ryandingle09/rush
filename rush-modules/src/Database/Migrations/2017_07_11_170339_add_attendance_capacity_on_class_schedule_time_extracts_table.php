<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttendanceCapacityOnClassScheduleTimeExtractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('class_schedule_time_extracts', function (Blueprint $table) {
            $table->integer('attendance_capacity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('class_schedule_time_extracts', function (Blueprint $table) {
            $table->dropColumn('attendance_capacity');
        });
    }
}
