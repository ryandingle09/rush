<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('merchant_id');
            $table->integer('customer_id');
            $table->string('order_number');
            $table->string('status');
            $table->string('uuid');
            $table->integer('branch_id')->default(0);
            $table->string('delivery_name');
            $table->string('delivery_mobile_number');
            $table->string('delivery_email');
            $table->string('delivery_building');
            $table->string('delivery_floor');
            $table->string('delivery_street');
            $table->string('delivery_landmark');
            $table->string('payment_name');
            $table->string('payment_mobile_number');
            $table->string('payment_change_for');
            $table->string('payment_notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
