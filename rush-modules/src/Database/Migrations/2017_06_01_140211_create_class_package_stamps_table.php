<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassPackageStampsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_package_stamps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('package_id');
            $table->integer('stamp_no');
            $table->string('name');
            $table->text('details');
            $table->string('image_path');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('class_package_stamps');
    }
}
