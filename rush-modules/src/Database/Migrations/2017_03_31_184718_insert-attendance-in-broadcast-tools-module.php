<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Rush\Modules\Helpers\ToolHelper;

class InsertAttendanceInBroadcastToolsModule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('broadcast_tools_modules')) {
            DB::table('broadcast_tools_modules')->insert([
                ['id' => ToolHelper::TOOL_ATTENDANCE_ID, 'name'=>'Attendance']
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('broadcast_tools_modules')) {
            DB::table('broadcast_tools_modules')->where('id', '=', ToolHelper::TOOL_ATTENDANCE_ID)->delete();
        }
    }
}
