<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPunchcardMaxStampCardToMerchantPromoMechanicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('MerchantPromoMechanics', function (Blueprint $table) {
            $table->integer('max_stamp_card')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('MerchantPromoMechanics', function (Blueprint $table) {
            $table->dropColumn('max_stamp_card');
        });
    }
}
