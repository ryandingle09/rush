<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Rush\Modules\Admin\Models\ModulesModel;

class InsertModulesEventAttendees extends Migration
{
    protected $modulesTable;

    public function __construct()
    {
        $model = new ModulesModel();
        $this->modulesTable = $model->getTable();
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table($this->modulesTable)->insert(
            [   'code' => 'att',
                'name' => 'Event Attendees Management',
                'route_name' => 'event-attendees',
                'url' => 'event-attendees',
                'package_id' => '2',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table($this->modulesTable)->where(['code' => 'att'])->delete();
    }
}
