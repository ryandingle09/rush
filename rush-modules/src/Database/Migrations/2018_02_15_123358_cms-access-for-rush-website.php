<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmsAccessForRushWebsite extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('onboarding_cms_user');
        Schema::create('onboarding_cms_user', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname', 45)->nullable();
            $table->string('lastname', 45)->nullable();
            $table->string('email', 50)->nullable()->unique();
            $table->string('password', 255)->nullable();
            $table->tinyInteger('user_type')->default('1');
            $table->boolean('enabled')->default(true);
            $table->integer('created_by');
            $table->timestamp('created_at')->useCurrent();
            $table->integer('updated_by')->nullable();
            $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
            $table->softDeletes();
        });

        Schema::dropIfExists('onboarding_cms_user_type');
        Schema::create('onboarding_cms_user_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->nullable();
            $table->string('modules', 255)->nullable();
            $table->integer('created_by');
            $table->timestamp('created_at')->useCurrent();
            $table->integer('updated_by')->nullable();
            $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
            $table->softDeletes();
        });

        Schema::dropIfExists('onboarding_cms_permissions');
        Schema::create('onboarding_cms_permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->nullable();
            $table->text('url')->nullable();
            $table->text('other_details')->nullable();
            $table->integer('created_by');
            $table->string('type');
            $table->timestamp('created_at')->useCurrent();
            $table->integer('updated_by')->nullable();
            $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
            $table->softDeletes();
        });

        Schema::dropIfExists('onboarding_cms_news');
        Schema::create('onboarding_cms_news', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title')->nullable();
            $table->text('url')->nullable();
            $table->text('description')->nullable();
            $table->string('background', 255)->nullable();
            $table->string('logo', 255)->nullable();
            $table->string('app_image', 255)->nullable();
            $table->text('content')->nullable();
            $table->boolean('publish')->default(0);
            $table->timestamp('publish_date')->nullable();
            $table->boolean('visibility')->default(0);
            $table->integer('created_by');
            $table->timestamp('created_at')->useCurrent();
            $table->integer('updated_by')->nullable();
            $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
            $table->softDeletes();
        });

        Schema::dropIfExists('onboarding_cms_clientlogos');
        Schema::create('onboarding_cms_clientlogos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('logo', 255)->nullable();
            $table->string('client_name', 255)->nullable();
            $table->boolean('visibility')->default(0);
            $table->integer('created_by');
            $table->timestamp('created_at')->useCurrent();
            $table->integer('updated_by')->nullable();
            $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
            $table->softDeletes();
        });

        DB::table('onboarding_cms_user')->insert([
            ['firstname' => 'Onboarding', 'lastname' => 'Superadmin', 'email' => 'admin@rush.ph', 'password' => hash("sha512", '12345678') ]
        ]);
        DB::table('onboarding_cms_user_type')->insert([
            ['name' => 'Superadmin', 'modules' => json_encode([1,2,3])]
        ]);
        DB::table('onboarding_cms_permissions')->insert([
            ['id' => 1, 'name' => 'Onboarding CMS Dashboard', 'url' => 'onboarding-cms','type' => 'module', 'other_details' => json_encode(['label'=>'Home', 'class'=>'home'])]
        ]);
        DB::table('onboarding_cms_permissions')->insert([
            ['id' => 2, 'name' => 'News Module', 'url' => 'onboarding-cms/news', 'type' => 'module', 'other_details' => json_encode(['label'=>'News', 'class'=>'news'])]
        ]);
        DB::table('onboarding_cms_permissions')->insert([
            ['id' => 3, 'name' => 'Client Logos Module', 'url' => 'onboarding-cms/clientlogos', 'type' => 'module', 'other_details' => json_encode(['label'=>'Logos','class'=>'clientlogos'])]
        ]);

        DB::table('onboarding_cms_clientlogos')->insert([
            ['id' => 1, 'logo' => 'taf.svg', 'client_name' => 'The Athlete\'s Foot', 'visibility' => true],
            ['id' => 2, 'logo' => 'brownhause.svg', 'client_name' => 'Browhaus', 'visibility' => true],
            ['id' => 3, 'logo' => 'the-palace.svg', 'client_name' => 'The Palace', 'visibility' => true],
            ['id' => 4, 'logo' => 'auntie-annes.svg', 'client_name' => 'Auntie Anne\'s', 'visibility' => true],
            ['id' => 5, 'logo' => 'unilever.svg', 'client_name' => 'Unilever Food Solutions', 'visibility' => true],
            ['id' => 6, 'logo' => 'golds.svg', 'client_name' => 'Gold\'s Gym', 'visibility' => true],
            ['id' => 7, 'logo' => 'yoga.svg', 'client_name' => 'Yoga+', 'visibility' => true],
            ['id' => 8, 'logo' => 'the-common-space.png', 'client_name' => 'The Common Space', 'visibility' => true],
            ['id' => 9, 'logo' => 'blade.svg', 'client_name' => 'Blade', 'visibility' => true],
            ['id' => 10, 'logo' => 'stroke.png', 'client_name' => 'Strokes', 'visibility' => true],
            ['id' => 11, 'logo' => 'ashark.svg', 'client_name' => '', 'visibility' => true],
            ['id' => 12, 'logo' => 'pearl-farm.svg', 'client_name' => 'Pearl Farm', 'visibility' => true],
            ['id' => 13, 'logo' => 'tonton.png', 'client_name' => 'Tonton Prestige', 'visibility' => true],
            ['id' => 14, 'logo' => 'island-gas.png', 'client_name' => 'Island Gas', 'visibility' => true],
            ['id' => 15, 'logo' => 'SkinlineBeautyandWellnessSpa.png', 'client_name' => 'Skinline Beauty and Wellness', 'visibility' => true],
            ['id' => 16, 'logo' => 'harrys.png', 'client_name' => 'Harry\'s', 'visibility' => true],
            ['id' => 17, 'logo' => 'mathe-magis.png', 'client_name' => 'MatheMagis', 'visibility' => true],
        ]);
          
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('onboarding_cms_user');
        Schema::dropIfExists('onboarding_cms_user_type');
        Schema::dropIfExists('onboarding_cms_permissions');
        Schema::dropIfExists('onboarding_cms_news');
        Schema::dropIfExists('onboarding_cms_clientlogos');
    }
}
