<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStartDateColumnToCustomerClassPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_class_packages', function (Blueprint $table) {
            $table->dateTime('start_date');
        });

        $customer_class_packages = \Rush\Modules\Customer\Models\CustomerClassPackageModel::all();
        foreach ($customer_class_packages as $customer_class_package) {
            $customer_class_package->start_date = $customer_class_package->created_at->toDateString();
            $customer_class_package->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_class_packages', function (Blueprint $table) {
            $table->dropColumn('start_date');
        });
    }
}
