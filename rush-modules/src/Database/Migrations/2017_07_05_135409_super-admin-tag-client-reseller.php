<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SuperAdminTagClientReseller extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('Merchant')) {
            Schema::table('Merchant', function($table) {
                $table->integer('reseller_id')->nullable();
                $table->string('reseller_email')->nullable();
            });
        }

        if ( !Schema::hasTable('reseller') ) {
            Schema::create('reseller', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
            });
        }

        DB::table('reseller')->insert([
            ['name'=>'AdSpark'],
            ['name'=>'Globe EG'],
            ['name'=>'Globe SG'],
            ['name'=>'Avanza'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Merchant', function ($table) {
            if (Schema::hasColumn('Merchant', 'reseller_id')) {
                $table->dropColumn('reseller_id');
            }
        });

        Schema::dropIfExists('reseller');
    }
}
