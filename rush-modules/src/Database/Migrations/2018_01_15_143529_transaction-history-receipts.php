<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransactionHistoryReceipts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('MerchantSettings', 'transaction_receipts') )
        {
            Schema::table('MerchantSettings', function (Blueprint $table)
            {
                $table->boolean('transaction_receipts')->default(false);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('MerchantSettings', 'transaction_receipts') )
        {
            Schema::table('MerchantSettings', function (Blueprint $table)
            {
                $table->dropColumn('transaction_receipts');
            });
        }
    }
}
