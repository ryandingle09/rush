<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PnpExistingCustomers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pnp_existing_customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('card_number');
            $table->string('name');
            $table->date('birthdate')->nullable();
            $table->boolean('migrated')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pnp_existing_customers');
    }
}
