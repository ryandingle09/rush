<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassScheduleReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_schedule_reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('class_list_id');
            $table->integer('customer_id');
            $table->boolean('registration_type');
            $table->boolean('showed_up');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('class_schedule_reservations');
    }
}
