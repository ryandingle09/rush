<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BillingUpdatesMerchant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('Merchant', 'monthly_service_fee') )
        {
            Schema::table('Merchant', function (Blueprint $table)
            {
                $table->decimal('monthly_service_fee', 10, 2)->nullable();
                $table->boolean('auto_update_status')->default(false);
            });
        }
    }

    /**
     * Reverse the migrations.
     *s
     * @return void
     */
    public function down()
    {
      if ( Schema::hasColumn('Merchant', 'monthly_service_fee') )
        {
            Schema::table('Merchant', function (Blueprint $table)
            {
                $table->dropColumn('monthly_service_fee');
                $table->dropColumn('auto_update_status');
            });
        }
    }
}
