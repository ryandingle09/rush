<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerCustomerClassPackageRewardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_class_packages_rewards', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('class_package_punchcard_id')->unsigned();
            $table->integer('class_package_reward_id')->unsigned();
            $table->boolean('claimed')->default(true);
            $table->boolean('status')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customer_class_packages_rewards');
    }
}
