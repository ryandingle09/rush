<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StatementBillingAddress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Merchant', function (Blueprint $table) {
            $table->string('street1')->after('address')->nullable();
            $table->string('street2')->after('address')->nullable();
            $table->string('billingName')->after('businessName')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Merchant', function (Blueprint $table) {
            $table->dropColumn('street1');
            $table->dropColumn('street2');
            $table->dropColumn('billingName');
        }); 
    }
}
