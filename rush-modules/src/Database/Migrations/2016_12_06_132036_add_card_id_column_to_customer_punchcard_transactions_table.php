<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCardIdColumnToCustomerPunchcardTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('CustomerPunchCardTransactions', function (Blueprint $table) {
            $table->integer('card_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('CustomerPunchCardTransactions', function (Blueprint $table) {
            $table->dropColumn('card_id');
        });
    }
}
