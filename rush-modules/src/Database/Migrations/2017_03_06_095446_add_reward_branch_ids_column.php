<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRewardBranchIdsColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ItemsForRedeem', function (Blueprint $table) {
            $table->string('branch_ids', 255)->default('[0]');
        });

        Schema::table('MerchantGlobeRewards', function (Blueprint $table) {
            $table->string('branch_ids', 255)->default('[0]');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ItemsForRedeem', function (Blueprint $table) {
            $table->dropColumn('branch_ids');
        });

        Schema::table('MerchantGlobeRewards', function (Blueprint $table) {
            $table->dropColumn('branch_ids');
        });
    }
}
