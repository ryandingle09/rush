<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMessageTablesAddSegmentId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sms', function (Blueprint $table) {
            $table->integer('segment_id')->unsigned()->nullable();
        });

        Schema::table('push', function (Blueprint $table) {
            $table->integer('segment_id')->unsigned()->nullable();
        });

        Schema::table('email', function (Blueprint $table) {
            $table->integer('segment_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sms', function (Blueprint $table) {
            $table->dropColumn('segment_id');
        });

        Schema::table('push', function (Blueprint $table) {
            $table->dropColumn('segment_id');
        });

        Schema::table('email', function (Blueprint $table) {
            $table->dropColumn('segment_id');
        });
    }
}
