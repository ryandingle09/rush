<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCustomerClassPackagesPunchcardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_class_packages_punchcards', function (Blueprint $table) {
            $table->dropColumn('customer_id');
            $table->dropColumn('class_package_id');
            $table->integer('customer_class_package_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_class_packages_punchcards', function (Blueprint $table) {
            $table->integer('class_package_id')->unsigned();
            $table->integer('customer_id')->unsigned();
        });
    }
}
