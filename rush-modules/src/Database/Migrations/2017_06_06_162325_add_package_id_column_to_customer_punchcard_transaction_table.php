<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPackageIdColumnToCustomerPunchcardTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('CustomerPunchCardTransactions', function (Blueprint $table) {
            $table->integer('class_package_id')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('CustomerPunchCardTransactions', function (Blueprint $table) {
            $table->dropColumn('class_package_id');
        });
    }
}
