<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHexFieldsFaqModule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchant_posts', function (Blueprint $table) {
            $table->string('event_color1')->nullable()->default("#FFFFFF");
            $table->string('event_color2')->nullable()->default("#FFFFFF");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchant_posts', function (Blueprint $table) {
            $table->dropColumn('event_color1');
            $table->dropColumn('event_color2');
        });
    }
}
