<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\CustomerCards;

class ChangeCustomerCardsStatusDefault extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_cards', function (Blueprint $table) {
            $table->boolean('status')->default(1)->change();
        });

        $cards = CustomerCards::where('status', 0)->pluck('id')->toArray();
        if ($cards) {
          Storage::put('customer_cards_status_migration.csv', implode("\n", $cards));
        }

        CustomerCards::where('status', 0)
          ->update(['status' => 1]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_cards', function (Blueprint $table) {
            $table->boolean('status')->default(0)->change();
        });

        $cards = str_getcsv(Storage::get('customer_cards_status_migration.csv'), "\n");
        foreach($cards as $card_id) {
          CustomerCards::where('status', 1)
            ->where('id', $card_id)
            ->update(['status' => 0]);
        }
    }
}
