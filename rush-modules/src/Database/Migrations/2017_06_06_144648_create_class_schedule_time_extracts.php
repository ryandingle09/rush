<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassScheduleTimeExtracts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_schedule_time_extracts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('schedule_id');
            $table->integer('instructor_id');
            $table->string('instructor_status');
            $table->string('status');
            $table->date('start_date_time');
            $table->integer('start_time');
            $table->integer('end_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('class_schedule_time_extracts');
    }
}
