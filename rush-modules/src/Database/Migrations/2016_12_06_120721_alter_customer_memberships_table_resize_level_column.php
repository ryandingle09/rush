<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCustomerMembershipsTableResizeLevelColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_memberships', function (Blueprint $table) {
            // use boolean for smallint
            $table->boolean('level')->nullable(false)->unsigned()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_memberships', function (Blueprint $table) {
            $table->integer('level')->default(1)->change();
        });
    }
}
