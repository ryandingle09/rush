<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SubusersAlaCdeBcu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('Merchant', 'bcu_enabled') )
        {
            Schema::table('Merchant', function (Blueprint $table)
            {
                $table->boolean('bcu_enabled')->default(false);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('Merchant', 'bcu_enabled') )
        {
            Schema::table('Merchant', function (Blueprint $table)
            {
                $table->dropColumn('bcu_enabled');
            });
        }
    }
}
