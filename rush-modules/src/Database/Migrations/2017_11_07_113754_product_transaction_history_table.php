<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductTransactionHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_transaction_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_transaction_id');
            $table->integer('merchant_id');
            $table->integer('customer_id');
            $table->integer('product_id');
            $table->float('amount');
            $table->integer('quantity');
            $table->float('points');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_transaction_history');
    }
}
