<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PnpExistingCustomersBatch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pnp_existing_customers', function (Blueprint $table) {
            $table->integer('batch')->nullable()->after('migrated');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pnp_existing_customers', function (Blueprint $table) {
            $table->dropColumn('batch');
        });
    }
}
