<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVoidedColumsToCustomerTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('CustomerTransaction', function (Blueprint $table) {
            $table->float('voided_amount')->nullable()->after('transactionType');
            $table->float('voided_points')->nullable()->after('transactionType');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('CustomerTransaction', function (Blueprint $table) {
            $table->dropColumn('voided_amount');
            $table->dropColumn('voided_points');
        });
    }
}
