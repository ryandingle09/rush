<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SaveTafAndGlobeRewardsVoiceShortcode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('notification_sms_credentials')->where('merchant_id', '190')->update(['voice_shortcode' => '21586468']);
        DB::table('notification_sms_credentials')->where('merchant_id', '220')->update(['voice_shortcode' => '21580564']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('notification_sms_credentials')->where('merchant_id', '190')->update(['voice_shortcode' => '']);
        DB::table('notification_sms_credentials')->where('merchant_id', '220')->update(['voice_shortcode' => '']);
    }
}
