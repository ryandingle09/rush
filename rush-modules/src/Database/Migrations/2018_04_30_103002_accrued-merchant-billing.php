<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AccruedMerchantBilling extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		if (Schema::hasColumn('MerchantSettings', 'accrued_billing'));
		{
			Schema::table('MerchantSettings', function (Blueprint $table) {
				$table->boolean('accrued_billing')->default(true);
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		if (!Schema::hasColumn('MerchantSettings', 'accrued_billing'));
		{
			Schema::table('MerchantSettings', function (Blueprint $table) {
				$table->dropColumn('accrued_billing');
			});
		}
	}
}
