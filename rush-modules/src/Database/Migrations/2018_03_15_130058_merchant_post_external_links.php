<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MerchantPostExternalLinks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchant_posts', function( Blueprint $table ){
            $table->string('android_external_link')->nullable();
            $table->string('ios_external_link')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchant_posts', function( Blueprint $table ){
            $table->dropColumn('android_external_link');
            $table->dropColumn('ios_external_link');
        });
    }
}
