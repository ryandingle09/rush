<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchantPunchcardMilestonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_punchcard_milestones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('stamps');
            $table->uuid('uuid')->nullable();
            $table->integer('merchant_id');
            $table->boolean('enabled')->default(true);
            $table->timestamps();
        });

        Schema::table('CustomerPunchCardTransactions', function (Blueprint $table) {
            $table->string('remarks')->nullable();
            $table->integer('milestone_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('merchant_punchcard_milestones');
    }
}
