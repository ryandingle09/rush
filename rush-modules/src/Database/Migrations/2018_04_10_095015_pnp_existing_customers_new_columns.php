<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PnpExistingCustomersNewColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pnp_existing_customers', function (Blueprint $table) {
            $table->dropColumn('name');

            $table->float('points')->after('birthdate');
            $table->string('level')->nullable()->after('points');
            $table->date('enrollment_date')->nullable()->after('level');
            $table->date('last_sale_date')->nullable()->after('enrollment_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pnp_existing_customers', function (Blueprint $table) {
            $table->string('name')->after('card_number');

            $table->dropColumn('points');
            $table->dropColumn('level');
            $table->dropColumn('enrollment_date');
            $table->dropColumn('last_sale_date');
        });
    }
}
