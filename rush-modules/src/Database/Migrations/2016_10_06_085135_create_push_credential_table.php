<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePushCredentialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('push_credential', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('merchant_id');
            $table->string('android_key', '255');
            $table->string('ios_cert', '255');
            $table->string('ios_secret', '255');
            $table->string('ios_bundle', '255');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('push_credential');
    }
}
