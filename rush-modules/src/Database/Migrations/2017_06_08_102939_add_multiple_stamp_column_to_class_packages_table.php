<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMultipleStampColumnToClassPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('class_packages', function (Blueprint $table) {
            $table->boolean('multiple_stamp')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('class_packages', function (Blueprint $table) {
            $table->dropColumn('multiple_stamp');
        });
    }
}
