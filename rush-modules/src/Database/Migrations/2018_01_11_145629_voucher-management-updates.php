<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VoucherManagementUpdates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('voucher', 'discount') )
        {
            Schema::table('voucher', function (Blueprint $table)
            {
                $table->integer('discount')->nullable();
            });
        }

        if ( !Schema::hasColumn('voucher', 'discount_type') )
        {
            Schema::table('voucher', function (Blueprint $table)
            {
                $table->string('discount_type')->nullable();
            });
        }

        if ( !Schema::hasColumn('voucher', 'voucher_image') )
        {
            Schema::table('voucher', function (Blueprint $table)
            {
                $table->string('voucher_image')->nullable();
            });
        }

        if ( !Schema::hasColumn('voucher', 'file_upload') )
        {
            Schema::table('voucher', function (Blueprint $table)
            {
                $table->string('file_upload')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('voucher', 'discount') )
        {
            Schema::table('voucher', function (Blueprint $table)
            {
                $table->dropColumn('discount');
            });
        }

        if ( Schema::hasColumn('voucher', 'discount_type') )
        {
            Schema::table('voucher', function (Blueprint $table)
            {
                $table->dropColumn('discount_type');
            });
        }

        if ( Schema::hasColumn('voucher', 'voucher_image') )
        {
            Schema::table('voucher', function (Blueprint $table)
            {
                $table->dropColumn('voucher_image');
            });
        }

        if ( Schema::hasColumn('voucher', 'file_upload') )
        {
            Schema::table('voucher', function (Blueprint $table)
            {
                $table->dropColumn('file_upload');
            });
        }
    }
}
