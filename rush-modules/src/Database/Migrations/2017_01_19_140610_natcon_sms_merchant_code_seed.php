<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NatconSmsMerchantCodeSeed extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $merchant = DB::table('Merchant')->where('business_system_link', 'natcon-2017')->first();
        if( $merchant ){
            DB::table('MerchantCode')->insert([
                ['merchant_id' => $merchant->merchantid, 'code' => 'NATCON', 'enabled' => 1]
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $merchant = DB::table('Merchant')->where('business_system_link', 'natcon-2017')->first();
        if( $merchant ) DB::table('MerchantCode')->where('merchant_id', $merchant->merchantid)->delete();
    }
}
