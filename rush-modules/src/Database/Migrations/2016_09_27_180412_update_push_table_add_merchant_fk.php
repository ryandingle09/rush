<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePushTableAddMerchantFk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('push', function (Blueprint $table) {
            $table->foreign('merchant_id')
                ->references('merchantid')->on('Merchant')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('push', function (Blueprint $table) {
            $table->dropForeign(['merchant_id']);
        });
    }
}
