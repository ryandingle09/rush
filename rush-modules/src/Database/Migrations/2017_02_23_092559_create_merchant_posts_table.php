<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchantPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('merchant_id')->unsigned();
            $table->string('post_title');
            $table->string('post_slug');
            $table->longText('post_content');
            $table->longText('post_excerpt')->nullable();
            $table->string('post_thumbnail')->nullable();
            $table->string('post_type')->default('post');
            $table->string('post_status')->default('draft');
            $table->string('post_order')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('merchant_posts');
    }
}
