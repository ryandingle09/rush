<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIncludeColumnMerchantPunchcardRewardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('MerchantPunchCardRewards', function (Blueprint $table) {
            $table->boolean('include')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('MerchantPunchCardRewards', function (Blueprint $table) {
            $table->dropColumn('include');
        });
    }
}
