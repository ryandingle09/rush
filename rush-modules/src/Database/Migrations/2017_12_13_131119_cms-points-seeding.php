<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmsPointsSeeding extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('points_seeding_batch');
        Schema::create('points_seeding_batch', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('merchant_id');
            $table->integer('uploader_type');
            $table->integer('uploader_id');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::dropIfExists('points_seeding_data');
        Schema::create('points_seeding_data', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('transaction_date');
            $table->string('mobile_number');
            $table->string('or_number');
            $table->integer('transaction_amount');
            $table->integer('branch');
            $table->integer('status');
            $table->integer('batch_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('points_seeding_batch');
        Schema::dropIfExists('points_seeding_data');
    }
}
