<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MemberTierPricing extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('Merchant', 'new_billing_calculation') )
        {
            Schema::table('Merchant', function (Blueprint $table)
            {
                $table->boolean('new_billing_calculation')->default(true);
            });
        }

        $merchants = DB::table('Merchant')->get();

        foreach( $merchants as $merchant )
        {
            DB::table('Merchant')->where('merchantid', $merchant->merchantid )->update(['new_billing_calculation' => 0]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('Merchant', 'new_billing_calculation') )
        {
            Schema::table('Merchant', function (Blueprint $table)
            {
                $table->dropColumn('new_billing_calculation');
            });
        }
    }
}
