<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SuperAdminAddNewBdData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( Schema::hasTable('bd_accounts') ) {
            DB::table('bd_accounts')->insert([
                [ 'name'=>'Patricia Tejam', 'email'=>'ptejam@yondu.com' ]
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasTable('bd_accounts') ) {
            DB::table('bd_accounts')->where('email', '=', 'ptejam@yondu.com')->delete();
        }
    }
}
