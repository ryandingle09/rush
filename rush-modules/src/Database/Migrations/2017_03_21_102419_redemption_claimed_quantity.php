<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RedemptionClaimedQuantity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Redeem', function (Blueprint $table){
            $table->integer('claimed')->unsigned()->default(0);
        });

        DB::update('UPDATE Redeem SET claimed = quantity WHERE status = 1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Redeem', function (Blueprint $table) {
            $table->dropColumn('claimed');
        });
    }
}
