<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MidconSmsMessages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $midcon_merchant_id = DB::table('Merchant')->where('business_system_link', 'midcon-2017')->value('merchantid');

        if( $midcon_merchant_id ){

            DB::table('notification_sms_messages')->insert([
                ['merchant_id' => $midcon_merchant_id, 'type' => 'midcon-sms-service-registration-failed', 'message' => "Sorry, your request cannot be processed as of the moment or you may have entered an invalid keyword. Please make sure you've register with the following keyword: REG MIDCON, Name, 4-Digit PIN"],
                ['merchant_id' => $midcon_merchant_id, 'type' => 'midcon-sms-service-mobile-already-registered', 'message' => "Mobile number already registered with Globe 2017 Channel Management Mid-Year Conference!"],
                ['merchant_id' => $midcon_merchant_id, 'type' => 'midcon-sms-service-mobile-not-whitelisted', 'message' => "Sorry, the mobile number you are registering is not allowed on Globe 2017 Channel Management Mid-Year Conference"],
                ['merchant_id' => $midcon_merchant_id, 'type' => 'midcon-sms-service-customer-registration', 'message' => "Hi, %name%! Thank you for Activating Your Mobile Number. See you at the Globe CBG Natcon 2017, at the Marriott Grand Ballroom, on February 9."],
            ]);

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $midcon_merchant_id = DB::table('Merchant')->where('business_system_link', 'midcon-2017')->value('merchantid');

        if( $midcon_merchant_id ){
            DB::table('notification_sms_messages')
                ->where('merchant_id', $midcon_merchant_id)
                ->where('type', 'like', 'midcon-sms-service-%')
                ->delete();
        }
    }
}
