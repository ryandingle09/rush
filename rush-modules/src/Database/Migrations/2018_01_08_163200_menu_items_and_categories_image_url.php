<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MenuItemsAndCategoriesImageUrl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('menu_items', function (Blueprint $table) {
            $table->string('image_url')->after('description')->nullable();
        });

        Schema::table('menu_item_categories', function (Blueprint $table) {
            $table->string('image_url')->after('name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menu_items', function (Blueprint $table) {
            $table->dropColumn('image_url');
        });

        Schema::table('menu_item_categories', function (Blueprint $table) {
            $table->dropColumn('image_url');
        });
    }
}
