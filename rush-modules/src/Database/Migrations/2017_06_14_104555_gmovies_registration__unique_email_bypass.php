<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GmoviesRegistrationUniqueEmailBypass extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $gmovies_id = DB::table('Merchant')->where('business_system_link', 'gmovies')->value('merchantid');

        if( $gmovies_id ){
            DB::table('merchant_generic_validation_bypass')->insert([
                ['merchant_id' => $gmovies_id, 'endpoint_name' => 'customer_register', 'field' => 'email', 'rule' => 'unique']
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $gmovies_id = DB::table('Merchant')->where('business_system_link', 'gmovies')->value('merchantid');

        if( $gmovies_id ){
            DB::table('merchant_generic_validation_bypass')
                ->where('merchant_id', $gmovies_id)
                ->where('endpoint_name', 'customer_register')
                ->where('field', 'email')
                ->where('rule', 'unique')
                ->delete();
        }
    }
}
