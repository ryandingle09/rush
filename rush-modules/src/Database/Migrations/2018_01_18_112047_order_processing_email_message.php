<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderProcessingEmailMessage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('notification_email_messages')->insert([
            [
                'merchant_id' => 0,
                'type' => 'order-processing', 
                'subject' => '%subject_business_name% New Order %subject_order_number%', 
                'message' => '<div style="background-color: #eee; padding: 10px; text-align: center;"><img height="70px" src="%logo_url%" /></div><br/><p>Hello, %customer_name%!</p><p>Thank you for your order from %business_name%. <b>Your order with ID: %order_number% is now being processed.</b></p><p>If you have questions regarding your account or any other matter, please feel free to contact us through %business_email% or call us through %business_contact%.</p><p>Thank you!</p><p><b>%business_name%</b></p>'
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('notification_email_messages')->where('merchant_id', 0)->where('type', 'order-processing')->delete();
    }
}
