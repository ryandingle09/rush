<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MaxStampsPerDay extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('MerchantPromoMechanics', function (Blueprint $table) {
            $table->integer('max_stamps_per_day')->default(0)->after('branch_stamp_per_day');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('MerchantPromoMechanics', function (Blueprint $table) {
            $table->dropColumn('max_stamps_per_day');
        });
    }
}
