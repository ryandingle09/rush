<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SuperAdminRolesUserManagement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('admin_modules');
        Schema::create('admin_modules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('name');
            $table->string('route_name');
            $table->string('url');
            $table->integer('type_id')->default('2');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
        });

        if ( !Schema::hasColumn('bd_accounts', 'password') )
        {
            Schema::table('bd_accounts', function (Blueprint $table)
            {
                $table->string('password', 255)->nullable()->after('email');
            });
        }

        Schema::dropIfExists('bd_admin_modules');
        Schema::create('bd_admin_modules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('module_id');
            $table->integer('bd_id');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
        });

        DB::table('admin_modules')->insert([
            ['code' => 'merchant', 'name' => 'Merchant', 'route_name'=> 'admin-merchant', 'url' => 'merchant', 'type_id' => 2],
            ['code' => 'management', 'name' => 'Management', 'route_name'=> 'admin-management', 'url' => 'management', 'type_id' => 2],
            ['code' => 'rewards', 'name' => 'Rewards', 'route_name'=> 'admin-rewards', 'url' => 'rewards', 'type_id' => 2],
            ['code' => 'billing', 'name' => 'Billing', 'route_name'=> 'admin-billing', 'url' => 'billing', 'type_id' => 2],
            ['code' => 'prospects', 'name' => 'Prospects', 'route_name'=> 'admin-prospects', 'url' => 'prospects', 'type_id' => 2],
            ['code' => 'settings', 'name' => 'Settings', 'route_name'=> 'admin-settings', 'url' => 'settings', 'type_id' => 2]
        ]);

        $bd_accounts = DB::select('Select * from bd_accounts');
        $module = DB::table('admin_modules')->where([ 'code' => 'prospects' ])->first();

        foreach( $bd_accounts as $accounts )
        {
            DB::table('bd_admin_modules')->insert([ [ 'module_id' => $module->id, 'bd_id' => $accounts->id ] ]);
            DB::table('bd_accounts')->where([ 'id' => $accounts->id ] )->update([ 'password' => hash("sha512", '12345678') ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_modules');

        if ( Schema::hasColumn('bd_accounts', 'password') )
        {
            Schema::table('bd_accounts', function (Blueprint $table)
            {
                $table->dropColumn('password');
            });
        }

        Schema::dropIfExists('bd_admin_modules');
    }
}
