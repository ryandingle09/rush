<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmsHelpUpdatePdf extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

		DB::table('modules')->insert(['code' => 'hel', 'name' => 'Help', 'route_name' => 'help', 'url' => 'help', 'package_id' => 2]);

		$help_module = DB::table('modules')->where(['name' => 'Help', 'package_id' => 2])->first();

		$merchants = DB::table('Merchant')->where(['packageId' => 2])->get();
		if ($merchants) {
			foreach ($merchants as $merchant) {
				DB::table('modules_merchant')->insert(['merchant_id' => $merchant->merchantid, 'module_id' => $help_module->id]);
			}
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		$help_module = DB::table('modules')->where(['name' => 'Help', 'package_id' => 2])->first();

		DB::table('modules')->where(['name' => 'Help', 'package_id' => 2])->delete();

		DB::table('modules_merchant')->where(['module_id' => $help_module->id])->delete();
	}
}
