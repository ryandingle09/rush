<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class QrCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qr_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('merchant_id');
            $table->string('uuid');
            $table->string('name');
            $table->float('points')->default(0);
            $table->float('load')->default(-1);
            $table->integer('customer_scan_limit')->default(-1);
            $table->string('branch_ids')->default('[0]');
            $table->string('image_url');
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('qr_codes');
    }
}
