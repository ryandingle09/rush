<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchantUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('merchant_users');
        Schema::create('merchant_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('merchant_id');
            $table->integer('package_id');
            $table->string('name', 255)->nullable();
            $table->string('email', 50)->nullable()->unique();
            $table->string('password', 255)->nullable();
            $table->boolean('is_deleted')->default(false);
            $table->integer('created_by')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->integer('updated_by')->nullable();
            $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
        });

        Schema::dropIfExists('ModulesMerchant');
        Schema::dropIfExists('merchant_user_modules');
        Schema::create('merchant_user_modules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('modules_id');
        });

        Schema::dropIfExists('Modules');
        Schema::dropIfExists('modules');
        Schema::create('modules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('name');
            $table->string('route_name');
            $table->string('url');
            $table->integer('package_id')->default('1');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
        });

        $this->populate();   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchant_users');
        Schema::dropIfExists('merchant_user_modules');
        Schema::dropIfExists('Modules');
        Schema::dropIfExists('modules');
    }

    public function populate()
    {
        DB::table('modules')->insert([
          ['route_name' => 'analytics', 'url' =>'analytics', 'code' => 'ana', 'name' => 'Analytics', 'package_id' => 1],
          ['route_name' => 'promos', 'url' =>'promos', 'code' => 'pro', 'name' => 'Program Mechanics', 'package_id' => 1],
          ['route_name' => 'achievement-unlock', 'url' =>'achievement-unlock', 'code' => 'ach', 'name' => 'Achievement Unlock', 'package_id' => 1],
          ['route_name' => 'redemption', 'url' =>'redemption', 'code' => 'rep', 'name' => 'Reward Catalogue', 'package_id' => 1],
          ['route_name' => 'design', 'url' =>'design#tablet', 'code' => 'des', 'name' => 'Design Management', 'package_id' => 1],
          ['route_name' => 'transactionhistory', 'url' =>'transactionhistory', 'code' => 'tra', 'name' => 'Transaction History', 'package_id' => 1],
          ['route_name' => 'management', 'url' =>'management', 'code' => 'man', 'name' => 'Store & Customer Management', 'package_id' => 1],
          ['route_name' => 'broadcast', 'url' =>'broadcast/sms', 'code' => 'sms', 'name' => 'Broadcasting Tool', 'package_id' => 1],
          ['route_name' => 'addons', 'url' =>'addons', 'code' => 'add', 'name' => 'Add-Ons', 'package_id' => 1],
          ['route_name' => 'help', 'url' =>'help', 'code' => 'hel', 'name' => 'Help', 'package_id' => 1],
          ['route_name' => 'settings', 'url' =>'settings', 'code' => 'set', 'name' => 'Settings', 'package_id' => 1],
          ['route_name' => 'billing', 'url' =>'billing', 'code' => 'bil', 'name' => 'Billing', 'package_id' => 1],
          ['route_name' => 'users', 'url' =>'users', 'code' => 'usr', 'name' => 'Users', 'package_id' => 1],
          ['route_name' => 'analytics', 'url' =>'analytics', 'code' => 'ana', 'name' => 'Analytics', 'package_id' => 2],
          ['route_name' => 'promos', 'url' =>'punchcard', 'code' => 'pro', 'name' => 'Punchcard Management', 'package_id' => 2],
          ['route_name' => 'achievement-unlock', 'url' =>'achievement-unlock', 'code' => 'ach', 'name' => 'Achievement Unlock', 'package_id' => 2],
          ['route_name' => 'design', 'url' =>'design#tablet', 'code' => 'des', 'name' => 'Design Management', 'package_id' => 2],
          ['route_name' => 'transactionhistory', 'url' =>'transactionhistory', 'code' => 'tra', 'name' => 'Transaction History', 'package_id' => 2],
          ['route_name' => 'management', 'url' =>'management', 'code' => 'man', 'name' => 'Store & Customer Management', 'package_id' => 2],
          ['route_name' => 'broadcast', 'url' =>'broadcast/sms', 'code' => 'sms', 'name' => 'Broadcasting Tool', 'package_id' => 2],
          ['route_name' => 'settings', 'url' =>'settings', 'code' => 'set', 'name' => 'Settings', 'package_id' => 2],
          ['route_name' => 'billing', 'url' =>'billing', 'code' => 'bil', 'name' => 'Billing', 'package_id' => 2],
          ['route_name' => 'users', 'url' =>'users', 'code' => 'usr', 'name' => 'Users', 'package_id' => 2],
         ]);
    }
}
