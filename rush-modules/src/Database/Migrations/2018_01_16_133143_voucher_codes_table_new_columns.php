<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VoucherCodesTableNewColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('voucher_codes', function (Blueprint $table) {
            $table->integer('merchant_id')->after('id');
            $table->boolean('used')->after('send');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('voucher_codes', function (Blueprint $table) {
            $table->dropColumn('merchant_id')->after('id');
            $table->dropColumn('used')->after('send');
        });
    }
}
