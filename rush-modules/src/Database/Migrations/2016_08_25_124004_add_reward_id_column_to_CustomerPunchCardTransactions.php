<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRewardIdColumnToCustomerPunchCardTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('CustomerPunchCardTransactions', function (Blueprint $table) {
            $table->integer('reward_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('CustomerPunchCardTransactions', function (Blueprint $table) {
            $table->dropColumn('reward_id');
        });
    }
}
