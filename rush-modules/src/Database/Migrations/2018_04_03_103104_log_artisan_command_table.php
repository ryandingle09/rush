<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LogArtisanCommandTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_artisan_command', function (Blueprint $table) {
            $table->increments('id');
            $table->string('repo_env')->nullable();
            $table->string('signature')->nullable();
            $table->string('class')->nullable();
            $table->datetime('start_time')->nullable();
            $table->datetime('end_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('log_artisan_command');
    }
}
