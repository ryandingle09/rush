<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PointsTransferEmailMessages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('notification_email_messages')->insert([
            ['merchant_id' => 0, 'type' => 'customer-points-transfer', 'message' => "<p>Hi %name%,</p><p>You have successfully transferred %points% %points_name% points to email %receiver_email% on %transaction_date%. Your new balance is %balance_points%. Keep collecting %points_name% for every purchase to redeem exciting rewards! Ref. No %transaction_reference%</p><p>This is a system-generated e-mail. Please do not reply.</p>"],
            ['merchant_id' => 0, 'type' => 'customer-points-receive', 'message' => "<p>Hi %name%,</p><p>You have successfully received %points% %points_name% points from %sender_name% on %transaction_date%. Your new balance is %receiver_balance_points%. Keep collecting %points_name% points for every purchase to redeem exciting rewards! Ref. No %transaction_reference%</p><p>This is a system-generated e-mail. Please do not reply.</p>"],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('notification_email_messages')
            ->where('merchant_id', 0)
            ->whereIn('type', ['customer-points-transfer', 'customer-points-receive'])
            ->delete();
    }
}
