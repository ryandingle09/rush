<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GmoviesCustomerPointsPayValidationBypass extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $gmovies_merchant_id = DB::table('Merchant')->where('business_system_link', 'gmovies')->value('merchantid');

        if( $gmovies_merchant_id ){
            // remove previously unused bypass.
            DB::table('merchant_generic_validation_bypass')
                ->where('merchant_id', $gmovies_merchant_id)
                ->delete();

            // insert pay points bypass.
            DB::table('merchant_generic_validation_bypass')->insert([
                [
                    'merchant_id' => $gmovies_merchant_id,
                    'endpoint_name' => 'customer_points_pay',
                    'field' => 'pin',
                    'rule' => 'required'
                ]
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $gmovies_merchant_id = DB::table('Merchant')->where('business_system_link', 'gmovies')->value('merchantid');

        if( $gmovies_merchant_id ){
            DB::table('merchant_generic_validation_bypass')
                ->where('merchant_id', $gmovies_merchant_id)
                ->delete();
        }
    }
}
