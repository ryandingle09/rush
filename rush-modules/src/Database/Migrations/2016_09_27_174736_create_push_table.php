<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePushTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('push', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('status');
            $table->dateTime('publish_date');
            $table->string('campaign', 50);
            $table->string('description', 100);
            $table->text('message');
            $table->string('device', 7);
            $table->timestamps();
            $table->integer('merchant_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('push');
    }
}
