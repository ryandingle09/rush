<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LoyaltyCardOrderingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('LoyaltyCardOrder', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('merchant_id');
          $table->integer('quantity');
          $table->decimal('total', 15, 2);
          $table->string('status');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('LoyaltyCardOrder');
    }
}
