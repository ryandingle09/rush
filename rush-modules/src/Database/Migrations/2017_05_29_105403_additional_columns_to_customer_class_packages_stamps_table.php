<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdditionalColumnsToCustomerClassPackagesStampsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_class_packages_stamps', function (Blueprint $table) {
            $table->integer('transaction_id')->unsigned()->nullable();
            $table->integer('branch_id')->unsigned()->nullable();
            $table->boolean('void')->default(false);
            $table->boolean('redeem')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_class_packages_stamps', function (Blueprint $table) {
            $table->dropColumn('transaction_id');
            $table->dropColumn('branch_id');
            $table->dropColumn('void');
            $table->dropColumn('redeem');
        });
    }
}
