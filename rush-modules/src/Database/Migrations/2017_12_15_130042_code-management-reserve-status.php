<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CodeManagementReserveStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('client', 'reserve_customer') )
        {
            Schema::table('client', function (Blueprint $table)
            {
                $table->string('reserve_customer')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('client', 'reserve_customer') )
        {
            Schema::table('client', function (Blueprint $table)
            {
                $table->dropColumn('reserve_customer');
            });
        }
    }
}
