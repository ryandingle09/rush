<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCustomerPunchcardTransactionsTableAddIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('CustomerPunchCardTransactions', function (Blueprint $table) {
            $table->index(['customer_id', 'created_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('CustomerPunchCardTransactions', function (Blueprint $table) {
            $table->dropIndex(['customer_id', 'created_at']);
        });
    }
}
