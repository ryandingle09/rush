<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('merchant_id')->unsigned();
            $table->string('type');
            $table->string('name');
            $table->string('email');
            $table->string('home_address')->nullable();
            $table->string('home_phone_number')->nullable();
            $table->string('company_name')->nullable();
            $table->string('company_address')->nullable();
            $table->string('company_mobile_number')->nullable();
            $table->string('mobile_number')->nullable();
            $table->dateTime('start_datetime');
            $table->dateTime('end_datetime');
            $table->integer('passenger_no');
            $table->string('delivery_location')->nullable();
            $table->text('itinerary');
            $table->boolean('booked_before')->nullable();
            $table->string('vehicle_type');
            $table->string('customer_option')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('car_reservations');
    }
}
