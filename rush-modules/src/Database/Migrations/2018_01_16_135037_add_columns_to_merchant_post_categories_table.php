<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToMerchantPostCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchant_posts_categories', function (Blueprint $table) {
            $table->boolean('include')->default(true);
            $table->integer('order')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchant_posts_categories', function (Blueprint $table) {
            $table->dropColumn('include');
            $table->dropColumn('order');
        });
    }
}
