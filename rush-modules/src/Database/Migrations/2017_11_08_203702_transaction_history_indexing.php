<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransactionHistoryIndexing extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Redeem', function (Blueprint $table) {
            $table->index('redeemReferenceCode');
        });

        Schema::table('redeemedItems', function (Blueprint $table) {
            $table->index('redeemId');
        });

        Schema::table('CustomerTransaction', function (Blueprint $table) {
            $table->index('merchant_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Redeem', function (Blueprint $table) {
            $table->dropIndex('redeem_redeemreferencecode_index');
        });

        Schema::table('redeemedItems', function (Blueprint $table) {
            $table->dropIndex('redeemeditems_redeemid_index');
        });

        Schema::table('CustomerTransaction', function (Blueprint $table) {
            $table->dropIndex('customertransaction_merchant_id_index');
        });
    }
}
