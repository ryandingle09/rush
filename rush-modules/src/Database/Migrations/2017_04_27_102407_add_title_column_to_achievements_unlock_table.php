<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Rush\Modules\Achievements\Models\AchievementsModel;

class AddTitleColumnToAchievementsUnlockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('achievements_unlock', function (Blueprint $table) {
            $table->string('title');
        });

        $achievements = AchievementsModel::all();
        foreach ($achievements as $achievement) {
            switch ($achievement->name) {
                case 'complete_profile':
                    $achievement->title = 'Completed Profile';
                    break;
                case 'customer_app_activation':
                    $achievement->title = 'Customer App Activation';
                    break;
                case 'facebook_like':
                    $achievement->title = 'Facebook Like';
                    break;
                case 'members_referral':
                    $achievement->title = 'Member\'s Referral';
                    break;
                case 'transaction_no_with_minimum_amount':
                    $achievement->title = 'Transaction no. with minimum amount';
                    break;
                case 'redemption_no_with_minimum_points':
                    $achievement->title = 'Redemption no. with minimum points';
                    break;
                case 'purchase_no_within_year':
                    $achievement->title = 'Purchased no. within year';
                    break;
                case 'special_day_purchase':
                    $achievement->title = 'Special day purchase';
                    break;
                case 'double_points':
                    $achievement->title = 'Double points';
                    break;
                case 'attended_event':
                    $achievement->title = 'Attended event';
                    break;
                case 'answering_survey':
                    $achievement->title = 'Answering survey';
                    break;
                case 'redemption_no_with_minimum_stamps':
                    $achievement->title = 'Redemptiono no. with minimum stamps.';
                    break;
                case 'double_stamps':
                    $achievement->title = 'Double stamps';
                    break;
            }
            $achievement->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('achievements_unlock', function (Blueprint $table) {
            $table->dropColumn('title');
        });
    }
}
