<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BladeNotificationMessage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $blade_merchant_id = DB::table('Merchant')->where('business_system_link', 'blade-asia-inc')->value('merchantid');

        if( $blade_merchant_id ){
            DB::table('notification_sms_messages')->insert([[
                'merchant_id' => $blade_merchant_id,
                'type' => 'purchase-class-package',
                'message' => "Congratulations! Blade membership package has been assigned to your account on %datetime%, valid until %validity_datetime%. Please download Blade customer from %vanity_url% and input the membership activation code: %activation_code%. Thank you!",
            ]]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $blade_merchant_id = DB::table('Merchant')->where('business_system_link', 'blade-asia-inc')->value('merchantid');

        if( $blade_merchant_id ){
            DB::table('notification_sms_messages')
                ->where('merchant_id', $blade_merchant_id)
                ->where('type', 'purchase-class-package')
                ->delete();
        }
    }
}
