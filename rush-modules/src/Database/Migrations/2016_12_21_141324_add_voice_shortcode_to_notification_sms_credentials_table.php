<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVoiceShortcodeToNotificationSmsCredentialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_sms_credentials', function (Blueprint $table) {
            $table->string('voice_shortcode', 100)->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notification_sms_credentials', function (Blueprint $table) {
            $table->dropColumn('voice_shortcode');
        });
    }
}
