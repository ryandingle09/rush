<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVanityUrlColumnToMerchantSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('MerchantSettings', function (Blueprint $table) {
            $table->string('vanity_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('MerchantSettings', function (Blueprint $table) {
            $table->dropColumn('vanity_url');
        });
    }
}
