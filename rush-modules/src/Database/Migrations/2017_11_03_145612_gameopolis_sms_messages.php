<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GameopolisSmsMessages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $gameopolis_merchant_id = DB::table('Merchant')->where('business_system_link', 'globe-gameopolis')->value('merchantid');

        DB::table('notification_sms_messages')->insert([
            ['merchant_id' => $gameopolis_merchant_id, 'type' => 'earn-points',  'message' => "You have earned %points_earned% %points_name% point(s) from %branch_name% on %datetime%. Your new balance is %balance_points% %points_name% point(s). Keep collecting %points_name% points to redeem exciting rewards. Ref. No. %reference%"],
            ['merchant_id' => $gameopolis_merchant_id, 'type' => 'redeem-reward',  'message' => "You have redeemed %reward_name% from %branch_name% on %datetime%. Your new balance is %balance_points% %points_name% point(s). Keep collecting %points_name% points to redeem exciting rewards. Ref. No. %reference%"],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $gameopolis_merchant_id = DB::table('Merchant')->where('business_system_link', 'globe-gameopolis')->value('merchantid');

        DB::table('notification_sms_messages')
            ->where('merchant_id', $gameopolis_merchant_id)
            ->whereIn('type', ['earn-points', 'redeem-reward'])
            ->delete();
    }
}
