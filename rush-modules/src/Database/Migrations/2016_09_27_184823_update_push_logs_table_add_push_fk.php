<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePushLogsTableAddPushFk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('push_logs', function (Blueprint $table) {
            $table->foreign('push_id')
                ->references('id')->on('push')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('push_logs', function (Blueprint $table) {
            $table->dropForeign(['push_id']);
        });
    }
}
