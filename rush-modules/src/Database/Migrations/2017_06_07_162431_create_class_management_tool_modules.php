<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassManagementToolModules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_management_tool_modules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('enabled')->default(true);
            $table->timestamps();
        });

        DB::table('class_management_tool_modules')->insert([
            ['name'=>'Package Management'],
            ['name'=>'Instructor Management'],
            ['name'=>'Instructor (With Branch)'],
            ['name'=>'Class Management'],
            ['name'=>'Activity (Class)'],
            ['name'=>'Class Schedule Management'],
            ['name'=>'Class Schedule List'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('class_management_tool_modules');
    }
}
