<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCustomerPointsIndexCustomerId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customerPoints', function (Blueprint $table) {
            $table->index('customerId');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customerPoints', function (Blueprint $table) {
            $table->dropIndex('customerpoints_customerid_index');
        });
    }
}
