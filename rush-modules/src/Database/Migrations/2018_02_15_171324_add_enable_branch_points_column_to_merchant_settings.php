<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEnableBranchPointsColumnToMerchantSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('MerchantSettings', function (Blueprint $table) {
            $table->boolean('enable_branch_points')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('MerchantSettings', function (Blueprint $table) {
            $table->dropColumn('enable_branch_points');
        });
    }
}
