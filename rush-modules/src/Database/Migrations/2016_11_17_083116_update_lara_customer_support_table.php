<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateLaraCustomerSupportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('CustomerReports');

        Schema::table('lara_customer_support', function (Blueprint $table) {
            $table->integer('merchant_id');
            $table->string('type', 10)->default('text');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('CustomerReports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('merchant_id');
            $table->integer('customer_id');
            $table->string('mobile_number', 45);
            $table->string('type', 10);
            $table->text('message')->nullable();
            $table->timestamps();
        });

        Schema::table('lara_customer_support', function (Blueprint $table) {
            $table->dropColumn('merchant_id');
            $table->dropColumn('type');
        });
    }
}
