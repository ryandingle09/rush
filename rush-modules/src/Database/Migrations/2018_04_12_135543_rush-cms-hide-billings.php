<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RushCmsHideBillings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('Billing', 'deleted_at') )
        {
            Schema::table('Billing', function (Blueprint $table)
            {
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('Billing', 'deleted_at') )
        {
            Schema::table('Billing', function (Blueprint $table)
            {
                $table->dropSoftDeletes();
            });
        }
    }
}
