<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MerchantBillingTierOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('Merchant', 'new_billing_tier_options') )
        {
            Schema::table('Merchant', function (Blueprint $table)
            {
                $table->text('new_billing_tier_options')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('Merchant', 'new_billing_tier_options') )
        {
            Schema::table('Merchant', function (Blueprint $table)
            {
                $table->dropColumn('new_billing_tier_options');
            });
        }
    }
}
