<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GlobeSkusRewardManagement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('admin_rewards', 'status') )
        {
            Schema::table('admin_rewards', function (Blueprint $table)
            {
                $table->boolean('status')->default(false);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('admin_rewards', 'status') )
        {
            Schema::table('admin_rewards', function (Blueprint $table)
            {
                $table->dropColumn('status');
            });
        }
    }
}
