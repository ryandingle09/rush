<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NonMemberTransactionTableUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('NonMemberTransaction', function (Blueprint $table) {
            $table->string('mobileNumber')->nullable()->change();
            $table->string('email')->nullable()->after('mobileNumber');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('NonMemberTransaction', function (Blueprint $table) {
            $table->string('mobileNumber')->nullable(false)->change();
            $table->dropColumn('email');
        });
    }
}
