<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMaxBranchColumnToPromoMechanics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('MerchantPromoMechanics', function (Blueprint $table) {
            $table->integer('max_branch')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('MerchantPromoMechanics', function (Blueprint $table) {
            $table->dropColumn('max_branch');
        });
    }
}
