<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MerchantCheckinTabFlag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Merchant', function (Blueprint $table) {
            $table->boolean('checkin_tab')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Merchant', function (Blueprint $table) {
            $table->dropColumn('checkin_tab');
        });
    }
}
