<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmsAccessForRushWebsiteFe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('onboarding_cms_news', 'background_image')) {
            Schema::table('onboarding_cms_news', function (Blueprint $table) {
                $table->string('background_image', 255)->nullable();
            });
        }

        if (!Schema::hasColumn('onboarding_cms_news', 'banner_image')) {
            Schema::table('onboarding_cms_news', function (Blueprint $table) {
                $table->string('banner_image', 255)->nullable();
            });
        }

        if (!Schema::hasColumn('onboarding_cms_news', 'android_link')) {
            Schema::table('onboarding_cms_news', function (Blueprint $table) {
                $table->text('android_link')->nullable();
            });
        }

        if (!Schema::hasColumn('onboarding_cms_news', 'ios_link')) {
            Schema::table('onboarding_cms_news', function (Blueprint $table) {
                $table->text('ios_link')->nullable();
            });
        }

        if (!Schema::hasColumn('onboarding_cms_news', 'app_name')) {
            Schema::table('onboarding_cms_news', function (Blueprint $table) {
                $table->text('app_name')->nullable();
            });
        }

        if (!Schema::hasColumn('onboarding_cms_news', 'mobile_app_content')) {
            Schema::table('onboarding_cms_news', function (Blueprint $table) {
                $table->text('mobile_app_content')->nullable();
            });
        }

        if (Schema::hasColumn('onboarding_cms_news', 'app_image')) {
            Schema::table('onboarding_cms_news', function (Blueprint $table) {
                $table->dropColumn('app_image');
            });
        }

        if (!Schema::hasColumn('onboarding_cms_news', 'app_image1')) {
            Schema::table('onboarding_cms_news', function (Blueprint $table) {
                $table->text('app_image1')->nullable();
                $table->text('app_image1_label')->nullable();
            });
        }

        if (!Schema::hasColumn('onboarding_cms_news', 'app_image2')) {
            Schema::table('onboarding_cms_news', function (Blueprint $table) {
                $table->text('app_image2')->nullable();
                $table->text('app_image2_label')->nullable();
            });
        }

        if (!Schema::hasColumn('onboarding_cms_news', 'app_image3')) {
            Schema::table('onboarding_cms_news', function (Blueprint $table) {
                $table->text('app_image3')->nullable();
                $table->text('app_image3_label')->nullable();
            });
        }

        if (!Schema::hasColumn('onboarding_cms_news', 'app_image_top')) {
            Schema::table('onboarding_cms_news', function (Blueprint $table) {
                $table->text('app_image_top')->nullable();
            });
        }

        if (!Schema::hasColumn('onboarding_cms_news', 'merchant_name')) {
            Schema::table('onboarding_cms_news', function (Blueprint $table) {
                $table->text('merchant_name')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('onboarding_cms_news', 'background_image')) {
            Schema::table('onboarding_cms_news', function (Blueprint $table) {
                $table->dropColumn('background_image');
            });
        }

        if (Schema::hasColumn('onboarding_cms_news', 'banner_image')) {
            Schema::table('onboarding_cms_news', function (Blueprint $table) {
                $table->dropColumn('banner_image');
            });
        }

        if (Schema::hasColumn('onboarding_cms_news', 'android_link')) {
            Schema::table('onboarding_cms_news', function (Blueprint $table) {
                $table->dropColumn('android_link');
            });
        }

        if (Schema::hasColumn('onboarding_cms_news', 'ios_link')) {
            Schema::table('onboarding_cms_news', function (Blueprint $table) {
                $table->dropColumn('ios_link');
            });
        }

        if (Schema::hasColumn('onboarding_cms_news', 'app_name')) {
            Schema::table('onboarding_cms_news', function (Blueprint $table) {
                $table->dropColumn('app_name');
            });
        }
        
        if (Schema::hasColumn('onboarding_cms_news', 'mobile_app_content')) {
            Schema::table('onboarding_cms_news', function (Blueprint $table) {
                $table->dropColumn('mobile_app_content');
            });
        }

        if (Schema::hasColumn('onboarding_cms_news', 'app_image1')) {
            Schema::table('onboarding_cms_news', function (Blueprint $table) {
                $table->dropColumn('app_image1');
                $table->dropColumn('app_image1_label');
            });
        }

        if (Schema::hasColumn('onboarding_cms_news', 'app_image2')) {
            Schema::table('onboarding_cms_news', function (Blueprint $table) {
                $table->dropColumn('app_image2');
                $table->dropColumn('app_image2_label');
            });
        }

        if (Schema::hasColumn('onboarding_cms_news', 'app_image3')) {
            Schema::table('onboarding_cms_news', function (Blueprint $table) {
                $table->dropColumn('app_image3');
                $table->dropColumn('app_image3_label');
            });
        }

        if (Schema::hasColumn('onboarding_cms_news', 'app_image_top')) {
            Schema::table('onboarding_cms_news', function (Blueprint $table) {
                $table->dropColumn('app_image_top');
            });
        }

        if (Schema::hasColumn('onboarding_cms_news', 'merchant_name')) {
            Schema::table('onboarding_cms_news', function (Blueprint $table) {
                $table->dropColumn('merchant_name');
            });
        }


    }
}
