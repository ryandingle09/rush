<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PunchcardCustomerAppRedemptionEmailMessage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('notification_email_messages')->insert([
            [
                'merchant_id' => 0,
                'type' => 'redeem-stamp',
                'subject' => '%subject_business_name% Reward Redemption',
                'message' => '<p>Hello, %name%!</p><p>You have successfully redeemed %reward_name% on %datetime%.</p><p>Keep collecting %stamps_name% to redeem exciting freebies. Ref No. %reference%.</p><p>If you have questions regarding your account or any other matter, please feel free to contact us through %business_email% or call us through %business_contact%.</p><p>Thank you!</p><p>%business_name%</p>',
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('notification_email_messages')->where('merchant_id', 0)->where('type', 'redeem-stamp')->delete();
    }
}
