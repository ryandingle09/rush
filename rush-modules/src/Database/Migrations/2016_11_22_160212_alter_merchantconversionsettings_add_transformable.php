<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMerchantconversionsettingsAddTransformable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('MerchantConversionSettings', function (Blueprint $table) {
            $table->integer('transformable_id')->unsinged()->nullable();
            $table->string('transformable_type', 20)->nullable();
        });

        // fill up the new columns
        $conversions = Rush\Modules\Merchant\Models\MerchantConversionSettingModel::withTrashed()->get();
        foreach ($conversions as $conversion) {
            $conversion->forceFill([
                'transformable_id' => $conversion->merchant_id,
                'transformable_type' => 'Merchant'
            ]);
            $conversion->update();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('MerchantConversionSettings', function (Blueprint $table) {
            $table->dropColumn('transformable_id');
            $table->dropColumn('transformable_type');
        });
    }
}
