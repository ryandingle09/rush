<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMerchantsettingsAddMember extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('MerchantSettings', function (Blueprint $table) {
            $table->boolean('add_member')->default(false);
        });

        Schema::table('MerchantSettings', function (Blueprint $table) {
            $table->index('merchant_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('MerchantSettings', function (Blueprint $table) {
            $table->dropColumn('add_member');
        });

        Schema::table('MerchantSettings', function (Blueprint $table) {
            $table->dropIndex('merchantsettings_merchant_id_index');
        });
    }
}
