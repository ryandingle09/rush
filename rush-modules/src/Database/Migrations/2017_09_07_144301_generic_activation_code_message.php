<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenericActivationCodeMessage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $blade_merchant_id = DB::table('Merchant')->where('business_system_link', 'blade-asia-inc')->value('merchantid');

        if( $blade_merchant_id ){
            DB::table('notification_sms_messages')->insert([[
                'merchant_id' => $blade_merchant_id,
                'type' => 'generic-activation-code',
                'message' => "Your code is: %activation_code%. Thank you!",
            ]]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $blade_merchant_id = DB::table('Merchant')->where('business_system_link', 'blade-asia-inc')->value('merchantid');

        if( $blade_merchant_id ){
            DB::table('notification_sms_messages')
                ->where('merchant_id', $blade_merchant_id)
                ->where('type', 'generic-activation-code')
                ->delete();
        }
    }
}
