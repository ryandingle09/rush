<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBranchStampPerDayColumnPromoMechanics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('MerchantPromoMechanics', function (Blueprint $table) {
            $table->integer('branch_stamp_per_day')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('MerchantPromoMechanics', function (Blueprint $table) {
            $table->dropColumn('branch_stamp_per_day');
        });
    }
}
