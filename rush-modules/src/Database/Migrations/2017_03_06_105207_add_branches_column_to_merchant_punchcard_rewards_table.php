<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBranchesColumnToMerchantPunchcardRewardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('MerchantPunchCardRewards', function (Blueprint $table) {
            $table->string('branch_ids', 255)->default('[0]');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('MerchantPunchCardRewards', function (Blueprint $table) {
            $table->dropColumn('branch_ids');
        });
    }
}
