<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeneCustomerRsvpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('BeneCustomerRsvp', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('merchantId');
            $table->string('mobileNumber', 45);
            $table->string('PIN', 10);
            $table->string('email', 45);
            $table->mediumText('fullName');
            $table->string('registrationChannel', 255);
            $table->char('uuid', 36);
            $table->datetime('timestamp');
            $table->boolean('confirmed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('BeneCustomerRsvp');
    }
}
