<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EarnStampEmailMessage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('notification_email_messages')->insert([
            [
                'merchant_id' => 0,
                'type' => 'earn-stamp',
                'subject' => '%subject_business_name% Stamp Earn',
                'message' => '<p>Hi %customer_name%,</p>You have earned %transaction_stamps% %stamp_name% stamp/s for your transaction totaling PHP%amount% in %branch% on %datetime%. Your new balance is %current_stamps% %stamp_name% stamp/s for %promo%. Keep collecting %stamp_name% for every %business%\'s service availed to redeem exciting rewards! Ref. No %reference%.<p>This is a system-generated e-mail. Please do not reply.</p>',
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('notification_email_messages')->where('merchant_id', 0)->where('type', 'earn-stamp')->delete();
    }
}
