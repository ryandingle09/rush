<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AmaxApiCredentialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('AmaxApiCredentials', function (Blueprint $table) {
            $table->increments('id');
            $table->string('app_id');
            $table->string('app_secret');
            $table->string('rewards_token');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('AmaxApiCredentials');
    }
}
