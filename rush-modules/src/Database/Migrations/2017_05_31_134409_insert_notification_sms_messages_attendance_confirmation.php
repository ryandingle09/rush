<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Rush\Modules\Merchant\Models\NotificationSmsMessagesModel;

class InsertNotificationSmsMessagesAttendanceConfirmation extends Migration
{
    protected $notificationSmsMessagesTable;

    public function __construct()
    {
        $model = new NotificationSmsMessagesModel();
        $this->notificationSmsMessagesTable = $model->getTable();
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table($this->notificationSmsMessagesTable)->insert(
            [   'merchant_id' => '0',
                'type' => 'attendance-confirmation',
                'message' => 'Hi %name%! Welcome to %program%! Enjoy a day full of fun and learning experience.',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table($this->notificationSmsMessagesTable)->where([
            'merchant_id' => '0',
            'type' => 'attendance-confirmation'
        ])->delete();
    }
}
