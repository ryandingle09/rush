<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewCustomerPointsLevelsDasta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $pnp_merchant_id = DB::table('Merchant')->where('business_system_link', 'plains-and-prints')->value('merchantid');

        if( $pnp_merchant_id ){
            
            DB::table('customer_points_levels')->where('merchant_id', $pnp_merchant_id)->delete();

            DB::table('customer_points_levels')->insert([
                [
                    'merchant_id' => $pnp_merchant_id,
                    'code' => 'prestige',
                    'level_name' => 'Prestige Partner',
                    'points' => 10,
                    'points_type' => 'percent',
                    'expiration' => '1 year',
                ],
                [
                    'merchant_id' => $pnp_merchant_id,
                    'code' => 'elite',
                    'level_name' => 'Elite Partner',
                    'points' => 20,
                    'points_type' => 'percent',
                    'expiration' => '1 year',
                ],                
            ]);
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $pnp_merchant_id = DB::table('Merchant')->where('business_system_link', 'plains-and-prints')->value('merchantid');

        if( $pnp_merchant_id ){
            DB::table('customer_points_levels')->where('merchant_id', $pnp_merchant_id)->delete();
        }
    }
}
