<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGlobeNumbersPrefixTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('GlobeNumbersPrefix', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('prefix', 255);
            $table->string('company_name', 255);
            $table->string('brand', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('GlobeNumbersPrefix');
    }
}
