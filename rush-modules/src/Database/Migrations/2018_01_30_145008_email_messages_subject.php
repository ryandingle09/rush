<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmailMessagesSubject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('notification_email_messages')->where('merchant_id', 0)->where('type', 'generic-activation-code')->update(['subject' => '%subject_app_name% Activation Code']);
        DB::table('notification_email_messages')->where('merchant_id', 0)->where('type', 'customer-redeem')->update(['subject' => '%subject_app_name% Reward Redemption']);
        DB::table('notification_email_messages')->where('merchant_id', 0)->where('type', 'pre-registration')->update(['subject' => '%subject_app_name% Registration']);
        DB::table('notification_email_messages')->where('merchant_id', 0)->where('type', 'customer-points-earn')->update(['subject' => '%subject_app_name% Points Earn']);
        DB::table('notification_email_messages')->where('merchant_id', 0)->where('type', 'non-member-points-earn')->update(['subject' => '%subject_app_name% Non Member Points Earn']);
        DB::table('notification_email_messages')->where('merchant_id', 0)->where('type', 'customer-register')->update(['subject' => '%subject_app_name% Registration ']);
        DB::table('notification_email_messages')->where('merchant_id', 0)->where('type', 'customer-points-pay')->update(['subject' => '%subject_app_name% Points Pay']);
        DB::table('notification_email_messages')->where('merchant_id', 0)->where('type', 'customer-reward-claim-full')->update(['subject' => '%subject_app_name% Reward Claim']);
        DB::table('notification_email_messages')->where('merchant_id', 0)->where('type', 'customer-reward-claim-partial')->update(['subject' => '%subject_app_name% Reward Claim ']);
        DB::table('notification_email_messages')->where('merchant_id', 0)->where('type', 'customer-void-earn')->update(['subject' => '%subject_app_name% Void Earn']);
        DB::table('notification_email_messages')->where('merchant_id', 0)->where('type', 'customer-void-pay-points')->update(['subject' => '%subject_app_name% Void Pay Points']);
        DB::table('notification_email_messages')->where('merchant_id', 0)->where('type', 'customer-void-redeem')->update(['subject' => '%subject_app_name% Void Redeem']);
        DB::table('notification_email_messages')->where('merchant_id', 0)->where('type', 'customer-void-package')->update(['subject' => '%subject_app_name% Void Package']);
        DB::table('notification_email_messages')->where('merchant_id', 0)->where('type', 'customer-invite')->update(['subject' => '%subject_app_name% Invitation']);
        DB::table('notification_email_messages')->where('merchant_id', 0)->where('type', 'customer-points-transfer')->update(['subject' => '%subject_app_name% Transfer Points']);
        DB::table('notification_email_messages')->where('merchant_id', 0)->where('type', 'customer-points-receive')->update(['subject' => '%subject_app_name% Transfer Points']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('notification_email_messages')
            ->where('merchant_id', 0)
            ->whereIn('type', [
                'generic-activation-code',
                'customer-redeem',
                'pre-registration',
                'customer-points-earn',
                'non-member-points-earn',
                'customer-register',
                'customer-points-pay',
                'customer-reward-claim-full',
                'customer-reward-claim-partial',
                'customer-void-earn',
                'customer-void-pay-points',
                'customer-void-redeem',
                'customer-void-package',
                'customer-invite',
                'customer-points-transfer',
                'customer-points-receive'
            ])
            ->update(['subject' => null]);
    }
}
