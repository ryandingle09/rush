<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMerchantIdColumnToCustomerMembershipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_memberships', function (Blueprint $table) {
            $table->integer('merchant_id')->unsinged()->after('frequency_redemption');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_memberships', function (Blueprint $table) {
            $table->dropColumn('merchant_id');
        });
    }
}
