<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SuperadminProspectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('prospects');
        Schema::create('prospects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname', 255)->nullable();
            $table->string('firstname', 255)->nullable();
            $table->string('lastname', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('mobile', 255)->nullable();
            $table->string('company', 255)->nullable();
            $table->string('industry', 255)->nullable();
            $table->string('package', 255)->nullable();
            $table->string('objective', 255)->nullable();
            $table->string('message', 255)->nullable();
            $table->string('url', 255)->nullable();
            $table->string('ip_address', 255)->nullable();
            $table->string('bd_assigned', 255)->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
        });

        Schema::dropIfExists('bd_accounts');
        Schema::create('bd_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
        });

        DB::table('bd_accounts')->insert([
          ['name' => 'Catherine Cueno', 'email' =>'zcdcueno@globe.com.ph'],
          ['name' => 'Ana Denoga', 'email' =>'zajdenoga@globe.com.ph'],
          ['name' => 'Mervin Sanchez', 'email' =>'m.g.sanchez@icloud.com']
         ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prospects');
        Schema::dropIfExists('bd_accounts');
    }
}
