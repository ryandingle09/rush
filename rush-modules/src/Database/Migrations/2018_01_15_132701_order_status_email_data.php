<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderStatusEmailData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('notification_email_messages')->insert([
            [
                'merchant_id' => 0,
                'type' => "order-on-the-way",
                'subject' => "%subject_business_name% New Order %subject_order_number%",
                'message' => '<div style="background-color: #eee; padding: 10px; text-align: center;"><img height="70px" src="%logo_url%" /></div><br/><p>Hello, %customer_name%!</p><p>Thank you for your order from %business_name%. <b>Your order with ID: %order_number% is now on its way.</b></p><p>If you have questions regarding your account or any other matter, please feel free to contact us through %business_email% or call us through %business_contact%.</p><p>Thank you!</p><p><b>%business_name%</b></p>',
            ],
            [
                'merchant_id' => 0,
                'type' => "order-delivered",
                'subject' => "%subject_business_name% New Order %subject_order_number%",
                'message' => '<div style="background-color: #eee; padding: 10px; text-align: center;"><img height="70px" src="%logo_url%" /></div><br/><p>Hello, %customer_name%!</p><p>Thank you for your order from %business_name%. <b>Your order with ID: %order_number% has been delivered. Enjoy your meal!</b></p><p>If you have questions regarding your account or any other matter, please feel free to contact us through %business_email% or call us through %business_contact%.</p><p>Thank you!</p><p><b>%business_name%</b></p>',
            ],
            
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('notification_email_messages')
            ->where('merchant_id', 0)
            ->whereIn('type', ['order-on-the-way', 'order-delivered'])
            ->delete();
    }
}
