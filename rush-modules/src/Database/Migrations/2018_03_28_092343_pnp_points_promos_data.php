<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PnpPointsPromosData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $pnp_merchant_id = DB::table('Merchant')->where('business_system_link', 'plains-and-prints')->value('merchantid');

        if( $pnp_merchant_id ){
            DB::table('points_promos')->insert([
                'merchant_id' => $pnp_merchant_id, 
                'code' => 'birthday_double_points', 
                'status' => 1, 
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('points_promos')->truncate();
    }
}
