<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmsToolFeeBilling extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('MerchantSettings', 'billing_cms_tool_fee') )
        {
            Schema::table('MerchantSettings', function (Blueprint $table)
            {
                $table->boolean('billing_cms_tool_fee')->default(true);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('MerchantSettings', 'billing_cms_tool_fee') )
        {
            Schema::table('MerchantSettings', function (Blueprint $table)
            {
                $table->dropColumn('billing_cms_tool_fee');
            });
        }
    }
}
