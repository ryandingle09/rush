<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VoucherCms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('MerchantSettings', 'voucher_module') )
        {
            Schema::table('MerchantSettings', function (Blueprint $table)
            {
                $table->boolean('voucher_module')->default(false);
            });
        }

        if ( !Schema::hasColumn('voucher', 'description') )
        {
            Schema::table('voucher', function (Blueprint $table)
            {
                $table->text('description')->nullable()->after('name');
            });
        }

        if ( !Schema::hasColumn('voucher', 'description') )
        {
            Schema::table('voucher', function (Blueprint $table)
            {
                $table->text('description')->nullable()->after('name');
            });
        }

        if ( !Schema::hasColumn('voucher', 'enabled') )
        {
            Schema::table('voucher', function (Blueprint $table)
            {
                $table->boolean('enabled')->default(true);
            });
        }

        if ( !Schema::hasColumn('voucher', 'threshold') )
        {
            Schema::table('voucher', function (Blueprint $table)
            {
                $table->integer('threshold')->default(0);
            });
        }

        if ( !Schema::hasColumn('voucher', 'deleted_at') )
        {
            Schema::table('voucher', function (Blueprint $table)
            {
                $table->softDeletes();
            });
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('MerchantSettings', 'voucher_module') )
        {
            Schema::table('MerchantSettings', function (Blueprint $table)
            {
                $table->dropColumn('voucher_module');
            });
        }

        if ( Schema::hasColumn('voucher', 'enabled') )
        {
            Schema::table('voucher', function (Blueprint $table)
            {
                $table->dropColumn('enabled');
            });
        }

        if ( Schema::hasColumn('voucher', 'description') )
        {
            Schema::table('voucher', function (Blueprint $table)
            {
                $table->dropColumn('description');
            });
        }

        if ( Schema::hasColumn('voucher', 'threshold') )
        {
            Schema::table('voucher', function (Blueprint $table)
            {
                $table->dropColumn('threshold');
            });
        }

        if ( Schema::hasColumn('voucher', 'deleted_at') )
        {
            Schema::table('voucher', function (Blueprint $table)
            {
                $table->dropSoftDeletes();
            });
        }
        
    }
}
