<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GmoviesSmsCredentials extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $gmovies_id = DB::table('Merchant')->where('business_system_link', 'gmovies')->value('merchantid');

        if( $gmovies_id ){
            DB::table('notification_sms_credentials')->insert([[
                'merchant_id' => $gmovies_id,
                'shortcode' => '21586214',
                'app_id' => 'A6eRCdxxGEu67ce7rzTxxzu8o6XRCnxn',
                'app_secret' => 'd79ae4610d0f9bcf63e4f5ac166a6fcbe9b5043570527209bcdff64b5ac0e549',
                'passphrase' => 'QctllPdAMG',
            ]]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $gmovies_id = DB::table('Merchant')->where('business_system_link', 'gmovies')->value('merchantid');

        DB::table('notification_sms_credentials')->where('merchant_id', $gmovies_id)->delete();
    }
}
