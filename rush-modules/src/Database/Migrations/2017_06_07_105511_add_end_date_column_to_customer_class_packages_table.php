<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEndDateColumnToCustomerClassPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_class_packages', function (Blueprint $table) {
            $table->dateTime('end_date');
        });

        $customer_class_packages = \Rush\Modules\Customer\Models\CustomerClassPackageModel::all();
        /** @var \Rush\Modules\Customer\Services\CustomerService $customerService */
        $customerService = app()->make(\Rush\Modules\Customer\Services\CustomerService::class);
        foreach ($customer_class_packages as $customer_class_package) {
            /** @var \Rush\Modules\Customer\Models\CustomerClassPackageModel $customer_class_package */
            if ($customer_class_package->customer) {
                $expiration_date = $customerService->getClassPackageExpiration($customer_class_package->customer, $customer_class_package->id);
                $customer_class_package->end_date = $expiration_date->toDateTimeString();
                $customer_class_package->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_class_packages', function (Blueprint $table) {
            $table->dropColumn('end_date');
        });
    }
}
