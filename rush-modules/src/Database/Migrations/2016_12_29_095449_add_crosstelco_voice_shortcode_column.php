<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCrosstelcoVoiceShortcodeColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_sms_credentials', function (Blueprint $table) {
            $table->string('crosstelco_voice_shortcode', 100);
        });
        
        $merchant_id = DB::table('Merchant')->where('business_system_link', 'unilever-food-solutions-ph')->value('merchantid');
        if( $merchant_id ){
            DB::table('notification_sms_credentials')->where('merchant_id', $merchant_id)->update([
                'voice_shortcode' => '21584161',
                'crosstelco_voice_shortcode' => '29290584161'
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notification_sms_credentials', function (Blueprint $table) {
            $table->dropColumn('crosstelco_voice_shortcode');
        });
        
        $merchant_id = DB::table('Merchant')->where('business_system_link', 'unilever-food-solutions-ph')->value('merchantid');
        if( $merchant_id ) DB::table('notification_sms_credentials')->where('merchant_id', $merchant_id)->update(['voice_shortcode' => '21583960']);
    }
}
