<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVoucherCodeIdColumntCustomerPunchcardTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('CustomerPunchCardTransactions', function (Blueprint $table) {
            $table->integer('voucher_code_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('CustomerPunchCardTransactions', function (Blueprint $table) {
            $table->dropColumn('voucher_code_id');
        });
    }
}
