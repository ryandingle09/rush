<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MerchantSmsTransactionsCharges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('Merchant', 'sms_transactions_charge') )
        {
            Schema::table('Merchant', function (Blueprint $table)
            {
                $table->decimal('sms_transactions_charge', 10, 2)->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( !Schema::hasColumn('Merchant', 'sms_transactions_charge') )
        {
            Schema::table('Merchant', function (Blueprint $table)
            {
                $table->dropColumn('sms_transactions_charge');
            });
        }
    }
}
