<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransactionsLimitColumnsToMerchantSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('MerchantSettings', function (Blueprint $table) {
            $table->integer('earn_transaction_limit')->default(0);
            $table->float('earn_points_limit')->default(0);
            $table->integer('redemption_transaction_limit')->default(0);
            $table->integer('paypoint_transaction_limit')->default(0);
            $table->float('paypoint_points_limit')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('MerchantSettings', function (Blueprint $table) {
            $table->dropColumn('earn_transaction_limit');
            $table->dropColumn('earn_points_limit');
            $table->dropColumn('redemption_transaction_limit');
            $table->dropColumn('paypoint_transaction_limit');
            $table->dropColumn('paypoint_points_limit');
        });
    }
}
