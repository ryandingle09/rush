<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Rush\Modules\Customer\Models\CustomerModel;

class AlterCustomerAddAttendeeStatus extends Migration
{
    protected $customerTable;

    public function __construct()
    {
        $customer = new CustomerModel();
        $this->customerTable = $customer->getTable();
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->customerTable, function (Blueprint $table) {
            $table->boolean('attendee_status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->customerTable, function (Blueprint $table) {
            $table->dropColumn('attendee_status');
        });
    }
}
