<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SuperadminTableTwo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('admin_user');
        Schema::dropIfExists('Admin');
        Schema::create('Admin', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email', 50)->nullable()->unique();
            $table->string('password', 255)->nullable();
            $table->string('firstname', 45)->nullable();
            $table->string('lastname', 45)->nullable();
            $table->tinyInteger('admin_role')->default('0');
            $table->boolean('is_active')->default(false);
            $table->string('modules')->nullable();
            $table->integer('created_by');
            $table->timestamp('created_at')->useCurrent();
            $table->integer('updated_by')->nullable();
            $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
        });

        Schema::dropIfExists('admin_enable_disable_reason');
        Schema::create('admin_enable_disable_reason', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('merchant_id');
            $table->boolean('enabled');
            $table->text('reason')->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });

        if ( !Schema::hasColumn('Merchant', 'trial_end') )
        {
            Schema::table('Merchant', function (Blueprint $table)
            {
                $table->timestamp('trial_end')->nullable();
            });
        }

        if ( !Schema::hasColumn('Merchant', 'enabled') )
        {
            Schema::table('Merchant', function (Blueprint $table)
            {
                $table->boolean('enabled')->default(true);
            });
        }

        Schema::dropIfExists('Modules');
        Schema::create('Modules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('name');
            $table->integer('package_id')->default('1');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
        });

        Schema::dropIfExists('ModulesMerchant');
        Schema::create('ModulesMerchant', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('modules_id');
            $table->integer('merchant_id');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
        });

        Schema::dropIfExists('broadcast_tools_modules');
        Schema::create('broadcast_tools_modules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('enabled')->default(true);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
        });

        Schema::dropIfExists('broadcast_tools_management');
        Schema::create('broadcast_tools_management', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('broadcast_tools_id');
            $table->integer('merchant_id');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
        });

        $this->populate();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Admin');
        Schema::create('admin_user', function (Blueprint $table) {
            $table->increments('admin_id');
            $table->string('admin_name', 255)->nullable();
            $table->string('username', 20)->nullable();
            $table->string('password', 255)->nullable();
            $table->string('email', 50)->nullable();
            $table->tinyInteger('is_active')->nullable();
            $table->tinyInteger('admin_level')->nullable();
            $table->timestamp('createdat')->useCurrent();
            $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
            $table->string('firstname', 45)->nullable();
            $table->string('lastname', 45)->nullable();
            $table->string('remember_token', 255)->nullable();
            $table->integer('role_id')->default('0');
        });

        if ( !Schema::hasColumn('Merchant', 'trial_end') )
        {
            Schema::table('Merchant', function (Blueprint $table)
            {
                $table->dropColumn('trial_end');
            });
        }

        if ( !Schema::hasColumn('Merchant', 'enabled') )
        {
            Schema::table('Merchant', function (Blueprint $table)
            {
                $table->dropColumn('enabled');
            });
        }

        Schema::dropIfExists('Modules');
        Schema::dropIfExists('ModulesMerchant');
        Schema::dropIfExists('broadcast_tools_modules');
        Schema::dropIfExists('broadcast_tools_management');
        Schema::dropIfExists('admin_enable_disable_reason');
    }

    public function populate()
    {
      DB::table('Admin')->insert([
          'email' => 'admin@rush.ph',
          'password' => hash("sha512", '12345678'),
          'firstname' => 'Rush',
          'lastname' => 'Admin',
          'admin_role' => '0',
          'is_active' => '1',
          'created_by' => '1'
      ]);

      $merchants = DB::select('Select * from Merchant');

      foreach( $merchants as $merchant )
      {
        $trial_date_update = Carbon\Carbon::parse($merchant->timestamp)->addMonth();
        DB::table('Merchant')->where('merchantid', $merchant->merchantid )
                             ->update(['trial_end' => $trial_date_update]);
      }

      DB::table('Modules')->insert([
          ['code' => 'loyalty_dashboard', 'name' => 'Dashboard', 'package_id' => 1],
          ['code' => 'loyalty_program_mechanics', 'name' => 'Program Mechanics', 'package_id' => 1],
          ['code' => 'loyalty_achievement_unlock', 'name' => 'Achievement Unlock', 'package_id' => 1],
          ['code' => 'loyalty_reward_catalogue', 'name' => 'Reward Catalogue', 'package_id' => 1],
          ['code' => 'loyalty_design_management', 'name' => 'Design Management', 'package_id' => 1],
          ['code' => 'loyalty_transaction_history', 'name' => 'Transaction History', 'package_id' => 1],
          ['code' => 'loyalty_store_customer_management', 'name' => 'Store & Customer Management', 'package_id' => 1],
          ['code' => 'loyalty_broadcasting_tool', 'name' => 'Broadcasting Tool', 'package_id' => 1],
          ['code' => 'loyalty_add_ons', 'name' => 'Add-Ons', 'package_id' => 1],
          ['code' => 'loyalty_help', 'name' => 'Help', 'package_id' => 1],
          ['code' => 'loyalty_settings', 'name' => 'Settings', 'package_id' => 1],
          ['code' => 'loyalty_account', 'name' => 'Account', 'package_id' => 1],
          ['code' => 'punchcard_punchcard_management', 'name' => 'Punchcard Management', 'package_id' => 2],
          ['code' => 'punchcard_design_management', 'name' => 'Design Management', 'package_id' => 2],
          ['code' => 'punchcard_transaction_history', 'name' => 'Transaction History', 'package_id' => 2],
          ['code' => 'punchcard_store_customer_management', 'name' => 'Store & Customer Management', 'package_id' => 2],
          ['code' => 'punchcard_settings', 'name' => 'Settings', 'package_id' => 2],
          ['code' => 'punchcard_account', 'name' => 'Account', 'package_id' => 2],
      ]);

      DB::table('broadcast_tools_modules')->insert([
        ['name'=>'SMS Broadcast'],
        ['name'=>'Push Notifications'],
        ['name'=>'Email Broadcast'],
        ['name'=>'Target Segment Management'],
      ]);

      foreach( $merchants as $merchant )
      {
        $modules = DB::table('Modules')->where('package_id', $merchant->packageId)->get();
        foreach($modules as $module)
        {
          DB::table('ModulesMerchant')->insert([
            [ 'modules_id' => $module->id, 'merchant_id' => $merchant->merchantid ]
          ]);
        }

        $broadcast_modules = DB::table('broadcast_tools_modules')->where('enabled', TRUE)->get();
        foreach($broadcast_modules as $module)
        {
          DB::table('broadcast_tools_management')->insert([
            [ 'broadcast_tools_id' => $module->id, 'merchant_id' => $merchant->merchantid ]
          ]);
        }
      }

    }
}
