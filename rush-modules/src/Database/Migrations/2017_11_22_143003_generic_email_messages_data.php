<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenericEmailMessagesData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('notification_email_messages')->insert([
            ['merchant_id' => 0, 'type' => 'generic-activation-code', 'message' => "<p>Hi %name%,</p><p>Your Activation Code is: %activation_code%.</p><p>Thank you!</p><p>This is a system-generated e-mail. Please do not reply.</p>"],
            ['merchant_id' => 0, 'type' => 'customer-redeem', 'message' => "<p>Hi %name%,</p><p>You have redeemed %points_redeem% %points_name% points %quantity% %reward_name% from %redeem_location% on %datetime%. Your new balance is %balance_points% %points_name% points. Keep collecting %points_name% points every time you make a purchase to redeem exciting rewards! Ref. No %transaction_reference%.</p><p>This is a system-generated e-mail. Please do not reply.</p>"],
            ['merchant_id' => 0, 'type' => 'pre-registration', 'message' => "<p>Hi %name%,</p><p>Thank you for registering to the %program%'s Loyalty Program. Your One-time Activation Code (OTAC) is %otp%.</p><p>This is a system-generated e-mail. Please do not reply.</p>"],
            ['merchant_id' => 0, 'type' => 'customer-points-earn', 'message' => "<p>Hi %name%,</p><p>You have earned %points_earned% %points_name% points for your transaction totaling PHP%amount% in %branch_name% on %datetime%. Your new balance is %current_points% %points_name% points. Keep collecting %points_name% points for every purchase to redeem exciting rewards! Ref. No %transaction_reference%</p><p>This is a system-generated e-mail. Please do not reply.</p>"],
            ['merchant_id' => 0, 'type' => 'non-member-points-earn', 'message' => "<p>Thank you for your purchase our dear customer!</p><p>Register with %program_name% program for you to earn %points% %points_name% points from your transaction totaling PHP%amount% in %branch% on %datetime%. Download %app_name% from App Store and Google Play now. Ref. No %transaction_reference%.</p><p>This is a system-generated e-mail. Please do not reply.</p>"],
            ['merchant_id' => 0, 'type' => 'customer-register', 'message' => "<p>Hi %name%,</p><p>Thank you for signing up for %program_name%! Start earning %points_name% points every time you make a purchase to redeem exciting rewards!</p><p>This is a system-generated e-mail. Please do not reply.</p>"],
            ['merchant_id' => 0, 'type' => 'customer-points-pay', 'message' => "<p>Hi %name%,</p><p>You have paid %points_paid% %points_name% points for your transaction totalling PHP%total_amount_purchased% in %branch_name% on %datetime%. Your new balance is %balance_points%. Keep collecting %points_name% points for every purchase to redeem exciting rewards! Ref. No %transaction_reference%.</p><p>This is a system-generated e-mail. Please do not reply.</p>"],
            ['merchant_id' => 0, 'type' => 'customer-reward-claim-full', 'message' => "<p>Hi %name%,</p><p>You have claimed the redeemed %quantity% %reward_name% from %branch_name% on %datetime%. Keep collecting %points_name% points every time you make a purchase to redeem exciting rewards! Ref. No %transaction_reference%</p><p>This is a system-generated e-mail. Please do not reply.</p>"],
            ['merchant_id' => 0, 'type' => 'customer-reward-claim-partial', 'message' => "<p>Hi %name%,</p><p>You have claimed %claimed% of %quantity% redeemed %reward_name% from %branch_name% on %datetime%. Keep collecting %points_name% points every time you make a purchase to redeem exciting rewards! Ref. No %transaction_reference%</p><p>This is a system-generated e-mail. Please do not reply.</p>"],
            ['merchant_id' => 0, 'type' => 'customer-void-earn', 'message' => "<p>Hi %name%,</p><p>We have successfully reversed %voided_points% %points_name% points for your transaction totaling PHP%voided_amount% in %branch_name% on %datetime%. Your new balance is %balance_points% %points_name% points. Keep collecting %points_name% points for every purchase to redeem exciting rewards! Ref. No %transaction_reference%.</p><p>This is a system-generated e-mail. Please do not reply.</p>"],
            ['merchant_id' => 0, 'type' => 'customer-void-pay-points', 'message' => "<p>Hi %name%,</p><p>We have successfully reversed %points_burn% %points_name% points for your transaction totaling PHP%total_amount_purchased% in %branch_name% on %datetime%. Your new balance is %balance_points% %points_name% points. Keep collecting %points_name% points for every purchase to redeem exciting rewards! Ref. No %transaction_reference%.</p><p>This is a system-generated e-mail. Please do not reply.</p>"],
            ['merchant_id' => 0, 'type' => 'customer-void-redeem', 'message' => "<p>Hi %name%,</p><p>We have successfully reversed %voided_points% %points_name% points for your redemption of %quantity% %reward_name% from %branch_name% on %datetime%. Your new balance is %balance_points% %points_name% points. Keep collecting %points_name% points for every purchase to redeem exciting rewards! Ref. No %transaction_reference%.</p><p>This is a system-generated e-mail. Please do not reply.</p>"],
            ['merchant_id' => 0, 'type' => 'customer-void-package', 'message' => "<p>Hi %name%,</p><p>Your Package %package_name% has been voided. Ref. No %transaction_reference%.</p><p>This is a system-generated e-mail. Please do not reply.</p>"],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('notification_email_messages')->truncate();
    }
}
