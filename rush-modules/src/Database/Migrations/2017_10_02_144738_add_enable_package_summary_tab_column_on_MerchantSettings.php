<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEnablePackageSummaryTabColumnOnMerchantSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('MerchantSettings', function (Blueprint $table) {
            $table->boolean('enable_package_summary')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('MerchantSettings', function (Blueprint $table) {
            $table->dropColumn('enable_package_summary');
        });
    }
}
