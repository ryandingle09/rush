<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPushTableAddSentCount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('push', function (Blueprint $table) {
            $table->integer('sent_count')->default(0);
            $table->integer('failed_count')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('push', function (Blueprint $table) {
            $table->dropColumn('sent_count');
            $table->dropColumn('failed_count');
        });
    }
}
