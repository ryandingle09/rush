<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Billing', function (Blueprint $table) {
            $table->increments('id');
            $table->string('account_number');
            $table->integer('merchant_id');
            $table->string('business_name');
            $table->integer('package_id');
            $table->decimal('outstanding_balance', 15, 2);
            $table->string('status');
            $table->string('reference_number');
            $table->timestamp('payment_date')->nullable();
            $table->decimal('payment_amount', 15, 2);
            $table->string('payment_type');
            $table->string('official_receipt');
            $table->string('official_receipt_number');
            $table->string('barcode');
            $table->boolean('is_paid');
            $table->timestamp('billing_period_start');
            $table->timestamp('billing_period_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Billing');
    }
}
