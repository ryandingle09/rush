<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchantGlobeRewardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('MerchantGlobeRewards', function (Blueprint $table) {
            $table->integer('redeemItemId', true);
            $table->integer('merchantid')->nullable();
            $table->string('name', 255)->nullable();
            $table->string('details', 400)->default('none')->nullable();
            $table->integer('pointsRequired')->default(1)->nullable();
            $table->string('imageURL', 255)->default('none')->nullable();
            $table->string('status', 45)->default('none')->nullable();
            $table->datetime('timestamp', 45)->nullable();
            $table->char('uuid', 36);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('MerchantGlobeRewards');
    }
}
