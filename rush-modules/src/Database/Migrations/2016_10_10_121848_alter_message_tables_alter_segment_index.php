<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMessageTablesAlterSegmentIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // remove FK
        Schema::table('sms', function (Blueprint $table) {
            $table->dropForeign(['segment_id']);
        });

        Schema::table('push', function (Blueprint $table) {
            $table->dropForeign(['segment_id']);
        });

        Schema::table('email', function (Blueprint $table) {
            $table->dropForeign(['segment_id']);
        });

        // index segment_id
        Schema::table('sms', function (Blueprint $table) {
            $table->index('segment_id');
        });

        Schema::table('push', function (Blueprint $table) {
            $table->index('segment_id');
        });

        Schema::table('email', function (Blueprint $table) {
            $table->index('segment_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // drop index segment_id
        Schema::table('sms', function (Blueprint $table) {
            $table->dropIndex('segment_id');
        });

        Schema::table('push', function (Blueprint $table) {
            $table->dropIndex('segment_id');
        });

        Schema::table('email', function (Blueprint $table) {
            $table->dropIndex('segment_id');
        });

        // add segment FK
        Schema::table('sms', function (Blueprint $table) {
            $table->foreign('segment_id')
                ->references('id')->on('segments')
                ->onDelete('cascade');
        });

        Schema::table('push', function (Blueprint $table) {
            $table->foreign('segment_id')
                ->references('id')->on('segments')
                ->onDelete('cascade');
        });

        Schema::table('email', function (Blueprint $table) {
            $table->foreign('segment_id')
                ->references('id')->on('segments')
                ->onDelete('cascade');
        });
    }
}
