<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMessageTablesAddSegmentFk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sms', function (Blueprint $table) {
            $table->foreign('segment_id')
                ->references('id')->on('segments')
                ->onDelete('cascade');
        });

        Schema::table('push', function (Blueprint $table) {
            $table->foreign('segment_id')
                ->references('id')->on('segments')
                ->onDelete('cascade');
        });

        Schema::table('email', function (Blueprint $table) {
            $table->foreign('segment_id')
                ->references('id')->on('segments')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sms', function (Blueprint $table) {
            $table->dropForeign(['segment_id']);
        });

        Schema::table('push', function (Blueprint $table) {
            $table->dropForeign(['segment_id']);
        });

        Schema::table('email', function (Blueprint $table) {
            $table->dropForeign(['segment_id']);
        });
    }
}
