<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CustomerInviteEmailMessage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('notification_email_messages')->insert([
            ['merchant_id' => 0, 'type' => 'customer-invite', 'message' => "<p>Hi,</p><p>%customer_name% invited you to Join %program_name%! Download %app_name% via %vanity_url% and register using the referral code %referral_code% to earn instant %points_name% points!</p><p>This is a system-generated e-mail. Please do not reply.</p>"],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('notification_email_messages')->where('merchant_id', 0)->where('type', 'customer-invite')->delete();
    }
}
