<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CodeManagementSuperadmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('MerchantSettings', 'code_management') )
        {
            Schema::table('MerchantSettings', function (Blueprint $table)
            {
                $table->boolean('code_management')->default(false);
            });
        }

        if ( Schema::hasColumn('client', 'code') )
        {
            Schema::table('client', function ($table) {
                $table->string('code', 255)->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('MerchantSettings', 'code_management') )
        {
            Schema::table('MerchantSettings', function (Blueprint $table)
            {
                $table->dropColumn('code_management');
            });
        }

        if ( Schema::hasColumn('client', 'code') )
        {
            Schema::table('client', function ($table) {
                $table->string('code', 9)->change();
            });
        }
    }
}
