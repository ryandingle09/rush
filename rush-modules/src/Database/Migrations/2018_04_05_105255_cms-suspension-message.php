<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmsSuspensionMessage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('MerchantSettings', 'maintenance_text') )
        {
            Schema::table('MerchantSettings', function (Blueprint $table)
            {
                $table->string('maintenance_text', 60)->default("Please contact customer support.");
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('MerchantSettings', 'maintenance_text') )
        {
            Schema::table('MerchantSettings', function (Blueprint $table)
            {
                $table->dropColumn('maintenance_text');
            });
        }
    }
}
