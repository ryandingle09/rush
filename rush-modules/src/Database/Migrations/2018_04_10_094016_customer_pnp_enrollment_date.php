<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CustomerPnpEnrollmentDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Customer', function (Blueprint $table) {
            $table->datetime('pnp_enrollment_date')->nullable();
            $table->dropColumn('last_pnp_sync_datetime');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Customer', function (Blueprint $table) {
            $table->datetime('last_pnp_sync_datetime')->nullable();
            $table->dropColumn('pnp_enrollment_date');
        });
    }
}
