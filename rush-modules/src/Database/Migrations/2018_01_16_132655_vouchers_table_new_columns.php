<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VouchersTableNewColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if( !Schema::hasColumn('voucher', 'merchant_id') ){
            Schema::table('voucher', function (Blueprint $table){
                $table->integer('merchant_id')->after('id');
            });
        }

        if( !Schema::hasColumn('voucher', 'discount') ){
            Schema::table('voucher', function (Blueprint $table){
                $table->float('discount')->after('description')->default(0);
            });
        }

        if( !Schema::hasColumn('voucher', 'discount_type') ){
            Schema::table('voucher', function (Blueprint $table){
                $table->string('discount_type')->after('discount')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if( !Schema::hasColumn('voucher', 'merchant_id') ){
            Schema::table('voucher', function (Blueprint $table){
                $table->dropColumn('merchant_id');
            });
        }

        if( !Schema::hasColumn('voucher', 'discount') ){
            Schema::table('voucher', function (Blueprint $table){
                $table->dropColumn('discount');
            });
        }

        if( !Schema::hasColumn('voucher', 'discount_type') ){
            Schema::table('voucher', function (Blueprint $table){
                $table->dropColumn('discount_type');
            });
        }
    }
}
