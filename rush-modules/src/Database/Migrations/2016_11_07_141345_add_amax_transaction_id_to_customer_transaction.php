<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAmaxTransactionIdToCustomerTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('CustomerTransaction', function (Blueprint $table) {
            $table->integer('amax_transaction_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('CustomerTransaction', function (Blueprint $table) {
            $table->dropColumn('amax_transaction_id');
        });
    }
}
