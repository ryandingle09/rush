<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomerPackageIdColumnOnClassScheduleReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('class_schedule_reservations', function (Blueprint $table) {
            $table->integer('customer_package_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('class_schedule_reservations', function (Blueprint $table) {
            $table->dropColumn('customer_package_id');
        });
    }
}
