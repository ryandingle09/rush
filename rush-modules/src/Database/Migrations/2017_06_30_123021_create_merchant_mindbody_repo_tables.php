<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchantMindbodyRepoTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_mindbody_schedules', function (Blueprint $table) {
            $table->integer('id')->unsigned();
            $table->primary('id');
            $table->unique('id');
            $table->integer('class_id')->unsigned();
            $table->integer('instructor_id')->unsigned();
            $table->dateTime('start_datetime');
            $table->dateTime('end_datetime');
            $table->timestamps();
        });

        Schema::create('merchant_mindbody_classes', function (Blueprint $table) {
            $table->integer('id')->unsigned();
            $table->primary('id');
            $table->unique('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('merchant_mindbody_instructors', function (Blueprint $table) {
            $table->integer('id')->unsigned();
            $table->primary('id');
            $table->unique('id');
            $table->string('name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
