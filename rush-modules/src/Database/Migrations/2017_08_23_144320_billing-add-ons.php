<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BillingAddOns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('Merchant', 'billing_add_ons') )
        {
            Schema::table('Merchant', function (Blueprint $table)
            {
                $table->text('billing_add_ons')->nullable();
            });
        }

        if ( !Schema::hasColumn('Billing', 'billing_add_ons') )
        {
            Schema::table('Billing', function (Blueprint $table)
            {
                $table->text('billing_add_ons')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('Merchant', 'billing_add_ons') )
        {
            Schema::table('Merchant', function (Blueprint $table)
            {
                $table->dropColumn('billing_add_ons');
            });
        }

        if ( Schema::hasColumn('Billing', 'billing_add_ons') )
        {
            Schema::table('Billing', function (Blueprint $table)
            {
                $table->dropColumn('billing_add_ons');
            });
        }
    }
}
