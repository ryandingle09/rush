<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassScheduleTimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_schedule_time', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('schedule_id');
            $table->integer('start_time');
            $table->integer('end_time');
            $table->date('start_date');
            $table->date('end_date');
            $table->boolean('is_repeat');
            $table->string('repeats');
            $table->integer('repeat_every');
            $table->integer('repeat_occurence');
            $table->string('repeat_on');
            $table->string('repeat_selection');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('class_schedule_time');
    }
}
