<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CustomerInvitedEmailColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_invited', function (Blueprint $table) {
            $table->string('mobile_no')->nullable()->change();
            $table->string('email')->nullable()->after('mobile_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_invited', function (Blueprint $table) {
            $table->string('mobile_no')->nullable(false)->change();
            $table->dropColumn('email');
        });
    }
}
