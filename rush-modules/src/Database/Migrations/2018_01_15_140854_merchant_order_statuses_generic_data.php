<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MerchantOrderStatusesGenericData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('merchant_order_statuses')->insert([
            ['merchant_id' => 0, 'status' => 'Placed'],
            ['merchant_id' => 0, 'status' => 'Processing'],
            ['merchant_id' => 0, 'status' => 'On the way'],
            ['merchant_id' => 0, 'status' => 'Delivered'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('merchant_order_statuses')->truncate();
    }
}
