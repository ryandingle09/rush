<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPointsTransferFieldsToCustomerTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('CustomerTransaction', function (Blueprint $table) {
            $table->float('pointsTransferred');
            $table->integer('transferredFrom');
            $table->integer('transferredTo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('CustomerTransaction', function (Blueprint $table) {
            $table->dropColumn('pointsTransferred');
            $table->dropColumn('transferredFrom');
            $table->dropColumn('transferredTo');
        });
    }
}
