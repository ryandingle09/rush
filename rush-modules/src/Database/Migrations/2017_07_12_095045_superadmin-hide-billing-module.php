<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SuperadminHideBillingModule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('modules_merchant') ) {
            Schema::create('modules_merchant', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('module_id');
                $table->integer('merchant_id');
                $table->timestamps();
            });
        }

        $modules_punchcard = DB::table('modules')->select('id')->where(['package_id'=>2])->get();
        $modules_points = DB::table('modules')->select('id')->where(['package_id'=>1])->get();
        $merchants = DB::table('Merchant')->select('merchantid','packageId')->get();
        
        foreach( $merchants as $merchant )
        {
            $insert_array = [];
            if ( $merchant->packageId == 1 )
            {
                foreach( $modules_points as $module ) {
                    $insert_array[] = [ 'module_id' => $module->id, 'merchant_id' => $merchant->merchantid, 'created_at' => date('Y-m-d H:i:s') ];
                }
            } else {
                foreach( $modules_punchcard as $module ) {
                    $insert_array[] = [ 'module_id' => $module->id, 'merchant_id' => $merchant->merchantid, 'created_at' => date('Y-m-d H:i:s') ];
                }
            }
            DB::table('modules_merchant')->insert( $insert_array);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modules_merchant');
    }
}
