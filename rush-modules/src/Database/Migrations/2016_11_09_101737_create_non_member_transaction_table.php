<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNonMemberTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('NonMemberTransaction', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mobileNumber', 45);
            $table->string('transactionReferenceCode', 45);
            $table->integer('branchId');
            $table->string('transactionType', 45);
            $table->integer('conversionSettingsId');
            $table->integer('employee_id');
            $table->integer('userId');
            $table->decimal('pointsEarned', 10, 2);
            $table->double('amountPaidWithCash');
            $table->string('transactionStatus', 45);
            $table->string('receiptReferenceNumber', 45);
            $table->datetime('timestamp');
            $table->integer('merchant_id');
            $table->string('channel', 255);
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('NonMemberTransaction');
    }
}
