<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CleanUpMerchantList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('Merchant', 'deleted_at') )
        {
            Schema::table('Merchant', function (Blueprint $table)
            {
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('Merchant', 'deleted_at') )
        {
            Schema::table('Merchant', function (Blueprint $table)
            {
                $table->dropSoftDeletes();
            });
        }
    }
}
