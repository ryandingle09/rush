<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBeginPointsColumnToCustomerTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('CustomerTransaction', function (Blueprint $table) {
            $table->double('begin_points');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('CustomerTransaction', function (Blueprint $table) {
            $table->dropColumn('begin_points');
        });
    }
}
