<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SuperAdminCmsBranchCount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('Merchant', 'branch_count') )
        {
            if ( Schema::hasColumn('Merchant', 'msf_type') )
            {
                Schema::table('Merchant', function (Blueprint $table)
                {
                    $table->string('branch_count', 255)->nullable()->after('msf_type');
                });
            } else {
                Schema::table('Merchant', function (Blueprint $table)
                {
                    $table->string('branch_count', 255)->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('Merchant', 'branch_count') )
        {
            Schema::table('Merchant', function (Blueprint $table)
            {
                $table->dropColumn('branch_count');
            });
        }
    }
}
