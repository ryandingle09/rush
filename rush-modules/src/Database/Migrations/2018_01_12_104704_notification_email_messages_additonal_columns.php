<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NotificationEmailMessagesAdditonalColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_email_messages', function (Blueprint $table) {
            $table->string('subject')->after('type')->nullable();
            $table->string('template')->after('message')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notification_email_messages', function (Blueprint $table) {
            $table->dropColumn('subject');
            $table->dropColumn('template');
        });
    }
}
