<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GmoviesCustomerRegistrationValidationBypass extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $gmovies_merchant_id = DB::table('Merchant')->where('business_system_link', 'gmovies')->value('merchantid');

        if( $gmovies_merchant_id ){
            DB::table('merchant_generic_validation_bypass')->insert([
                ['merchant_id' => $gmovies_merchant_id, 'endpoint_name' => 'customer_register', 'field' => 'birthdate', 'rule' => 'required'],
                ['merchant_id' => $gmovies_merchant_id, 'endpoint_name' => 'customer_register', 'field' => 'gender', 'rule' => 'required'],
                ['merchant_id' => $gmovies_merchant_id, 'endpoint_name' => 'customer_register', 'field' => 'photo', 'rule' => 'required'],
                ['merchant_id' => $gmovies_merchant_id, 'endpoint_name' => 'customer_register', 'field' => 'fb_id', 'rule' => 'required'],
                ['merchant_id' => $gmovies_merchant_id, 'endpoint_name' => 'customer_register', 'field' => 'email_verified', 'rule' => 'required'],
                ['merchant_id' => $gmovies_merchant_id, 'endpoint_name' => 'customer_register', 'field' => 'mobile_verified', 'rule' => 'required'],
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $gmovies_merchant_id = DB::table('Merchant')->where('business_system_link', 'gmovies')->value('merchantid');

        if( $gmovies_merchant_id ){
            DB::table('merchant_generic_validation_bypass')
                ->where('merchant_id', $gmovies_merchant_id)
                ->where('endpoint_name', 'customer_register')
                ->delete();
        }
    }
}
