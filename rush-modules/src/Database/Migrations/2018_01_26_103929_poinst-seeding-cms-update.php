<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PoinstSeedingCmsUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( Schema::hasColumn('points_seeding_data', 'mobile_number') )
        {
            Schema::table('points_seeding_data', function (Blueprint $table)
            {
                $table->renameColumn('mobile_number', 'customer');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('points_seeding_data', 'customer') )
        {
            Schema::table('points_seeding_data', function (Blueprint $table)
            {
                $table->renameColumn('customer', 'mobile_number');
            });
        }
    }
}
