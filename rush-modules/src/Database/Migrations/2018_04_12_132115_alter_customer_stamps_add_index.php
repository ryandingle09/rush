<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCustomerStampsAddIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('CustomerStamps', function (Blueprint $table) {
            $table->index(['stamp_card_id', 'void']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('CustomerStamps', function (Blueprint $table) {
            $table->dropIndex(['stamp_card_id', 'void']);
        });
    }
}
