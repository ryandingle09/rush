<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAchievementUnlockTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('achievements_unlock_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('achievement_unlock_id');
            $table->integer('customer_id');
            $table->string('name');
            $table->text('options');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('achievements_unlock_transactions');
    }
}
