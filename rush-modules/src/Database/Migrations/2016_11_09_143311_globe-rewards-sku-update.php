<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GlobeRewardsSkuUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('admin_rewards');
        Schema::create('admin_rewards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->nullable();
            $table->string('details', 400)->nullable();
            $table->integer('pointsRequired');
            $table->string('imageURL', 255)->nullable();
            $table->timestamps();
        });

        if ( !Schema::hasColumn('MerchantGlobeRewards', 'admin_item_id') )
        {
            Schema::table('MerchantGlobeRewards', function (Blueprint $table)
            {
                $table->integer('admin_item_id')->nullable();
            });
        }

        if ( !Schema::hasColumn('ItemsForRedeem', 'admin_item_id') )
        {
            Schema::table('ItemsForRedeem', function (Blueprint $table)
            {
                $table->integer('admin_item_id')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_rewards');
        if ( Schema::hasColumn('MerchantGlobeRewards', 'admin_item_id') )
        {
            Schema::table('MerchantGlobeRewards', function (Blueprint $table)
            {
                $table->dropColumn('admin_item_id');
            });
        }

        if ( Schema::hasColumn('ItemsForRedeem', 'admin_item_id') )
        {
            Schema::table('ItemsForRedeem', function (Blueprint $table)
            {
                $table->dropColumn('admin_item_id');
            });
        }
    }
}
