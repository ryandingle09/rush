<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NotificationEmailLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_email_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('merchant_id');
            $table->string('type')->nullable();
            $table->string('response')->nullable();
            $table->string('sender')->nullable();
            $table->string('receiver')->nullable();
            $table->string('subject')->nullable();
            $table->text('contents')->nullable();
            $table->text('data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notification_email_logs');
    }
}
