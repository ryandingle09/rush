<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlacedOrderEmailDataMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('notification_email_messages')->insert([
            [
                'merchant_id' => 0,
                'type' => "order-placed",
                'subject' => "%subject_business_name% New Order %subject_order_number%",
                'message' => "",
                'template' => "order-placed",
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('notification_email_messages')
            ->where('merchant_id', 0)
            ->where('type', 'order-placed')
            ->delete();
    }
}
