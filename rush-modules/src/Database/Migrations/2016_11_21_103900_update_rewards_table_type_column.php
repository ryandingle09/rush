<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRewardsTableTypeColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ItemsForRedeem', function (Blueprint $table) {
            $table->dropColumn('type');
        });
        Schema::table('MerchantGlobeRewards', function (Blueprint $table) {
            $table->dropColumn('type');
        });
        Schema::table('admin_rewards', function (Blueprint $table) {
            $table->dropColumn('type');
        });


        Schema::table('ItemsForRedeem', function (Blueprint $table) {
            $table->string('type');
        });
        Schema::table('MerchantGlobeRewards', function (Blueprint $table) {
            $table->string('type');
        });
        Schema::table('admin_rewards', function (Blueprint $table) {
            $table->string('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ItemsForRedeem', function (Blueprint $table) {
            $table->dropColumn('type');
        });
        Schema::table('MerchantGlobeRewards', function (Blueprint $table) {
            $table->dropColumn('type');
        });
        Schema::table('admin_rewards', function (Blueprint $table) {
            $table->dropColumn('type');
        });


        Schema::table('ItemsForRedeem', function (Blueprint $table) {
            $table->tinyInteger('type')->default(0)->comment('0 = both, 1 = prepaid, 2 = postpaid');
        });
        Schema::table('MerchantGlobeRewards', function (Blueprint $table) {
            $table->tinyInteger('type')->default(0)->comment('0 = both, 1 = prepaid, 2 = postpaid');
        });
        Schema::table('admin_rewards', function (Blueprint $table) {
            $table->tinyInteger('type')->default(0)->comment('0 = both, 1 = prepaid, 2 = postpaid');
        });
    }
}
