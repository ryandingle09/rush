<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBillingTableForBreakdown extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('Billing', function($table)
      {
        $table->string('service_breakdown')->after('service_fee');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('Billing', function ($table) {
        if (Schema::hasColumn('Billing', 'service_breakdown')) {
          $table->dropColumn('service_breakdown');
        }
      });
    }
}
