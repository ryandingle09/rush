<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVarTypeColumnToBranchMetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('branches_meta', function (Blueprint $table) {
            $table->string('var_type')->default('string');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('branches_meta', function (Blueprint $table) {
            $table->dropColumn('var_type');
        });
    }
}
