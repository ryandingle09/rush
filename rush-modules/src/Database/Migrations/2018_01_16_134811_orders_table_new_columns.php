<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrdersTableNewColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('voucher_id')->after('status')->default(0);
            $table->string('promo_code')->after('voucher_id')->nullable();
            $table->float('promo_code_discount_amount')->after('total_amount')->default(0);
            $table->float('net_amount')->after('promo_code_discount_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('voucher_id');
            $table->dropColumn('promo_code');
            $table->dropColumn('promo_code_discount_amount');
            $table->dropColumn('net_amount');
        });
    }
}
