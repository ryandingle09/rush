<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerPromoCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_promo_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codes');
            $table->integer('customer_id')->unsigned()->nullable();
            $table->integer('merchant_id')->unsigned()->nullable();
            $table->string('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customer_promo_codes');
    }
}
