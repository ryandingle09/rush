<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmsFaqModule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('broadcast_tools_modules')->insert([
                ['id' => 5, 'name' => 'Events/Promos', 'enabled' => 1 ]
            ]);

        $merchants = DB::table('Merchant')->get();
        if( $merchants ){
            foreach( $merchants as $merchant) {
                DB::table('merchant_posts_categories')->insert([
                    ['merchant_id' => $merchant->merchantid, 'name' => 'Events/Promos']
                ]);
            }
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('broadcast_tools_modules')->where([ 'id' => 5 ])->delete();
        DB::table('merchant_posts_categories')->where([ 'name' => 'Events/Promos' ])->delete();
    }
}
