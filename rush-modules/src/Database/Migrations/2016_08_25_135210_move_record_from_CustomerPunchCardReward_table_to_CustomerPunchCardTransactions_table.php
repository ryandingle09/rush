<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MoveRecordFromCustomerPunchCardRewardTableToCustomerPunchCardTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $temp_rewards = Rush\Modules\Customer\Models\CustomerPunchCardRewardModel::all();
        DB::table('CustomerPunchCardTransactions')->update(['type' => 'earn']);
        $rewards = [];
        foreach ($temp_rewards as $reward) {
            $rewards[] = [
                'reward_id' => $reward->reward_id,
                'transaction_ref' => $reward->transaction_ref,
                'customer_id' => $reward->customer_id,
                'employee_id' => $reward->employee_id,
                'void' => $reward->void,
                'status' => $reward->status,
                'stamp_card_id' => $reward->stamp_card_id,
                'created_at' => $reward->created_at,
                'updated_at' => $reward->updated_at,
                'uuid' => $reward->uuid,
                'promo_id' => $reward->promo_id,
                'merchant_id' => isset($reward->customer->merchantId) ? $reward->customer->merchantId : null,
                'type' => 'redeem'
            ];
        }
        DB::table('CustomerPunchCardTransactions')->insert($rewards);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Rush\Modules\Customer\Models\CustomerPunchCardTransactionsModel::where('reward_id', '!=', 0)->delete();
        DB::table('CustomerPunchCardTransactions')->update(['type' => '']);
    }
}
