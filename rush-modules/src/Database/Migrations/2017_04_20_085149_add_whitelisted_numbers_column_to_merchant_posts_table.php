<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWhitelistedNumbersColumnToMerchantPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchant_posts', function (Blueprint $table) {
            $table->longText('whitelisted_numbers')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchant_posts', function (Blueprint $table) {
            $table->dropColumn('whitelisted_numbers');
        });
    }
}
