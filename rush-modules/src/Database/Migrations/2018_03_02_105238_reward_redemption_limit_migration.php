<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RewardRedemptionLimitMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ItemsForRedeem', function (Blueprint $table) {
            $table->integer('customer_redemption_limit')->default(-1)->after('pointsRequired');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ItemsForRedeem', function (Blueprint $table) {
            $table->dropColumn('customer_redemption_limit');
        });
    }
}
