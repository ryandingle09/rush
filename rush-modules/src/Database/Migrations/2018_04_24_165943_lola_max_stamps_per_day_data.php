<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LolaMaxStampsPerDayData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $lola_merchant_id = DB::table('Merchant')->where('business_system_link', 'la-lola-churreria')->value('merchantid');

        if( $lola_merchant_id ){
            DB::table('MerchantPromoMechanics')->where('merchant_id', $lola_merchant_id)->update(['max_stamps_per_day' => 2]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $lola_merchant_id = DB::table('Merchant')->where('business_system_link', 'la-lola-churreria')->value('merchantid');

        if( $lola_merchant_id ){
            DB::table('MerchantPromoMechanics')->where('merchant_id', $lola_merchant_id)->update(['max_stamps_per_day' => 0]);
        }
    }
}
