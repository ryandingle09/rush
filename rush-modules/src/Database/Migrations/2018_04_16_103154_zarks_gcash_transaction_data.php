<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ZarksGcashTransactionData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $zarks_merchant_id = DB::table('Merchant')->where('business_system_link', 'zarks-food-ventures-corporation')->value('merchantid');

        if( $zarks_merchant_id ){
            DB::table('MerchantSettings')
                ->where('merchant_id', $zarks_merchant_id)
                ->update(['gcash_transaction' => 1]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $zarks_merchant_id = DB::table('Merchant')->where('business_system_link', 'zarks-food-ventures-corporation')->value('merchantid');

        if( $zarks_merchant_id ){
            DB::table('MerchantSettings')
                ->where('merchant_id', $zarks_merchant_id)
                ->update(['gcash_transaction' => 0]);
        }
    }
}
