<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixBillingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('Billing');
        Schema::create('Billing', function (Blueprint $table) {
            $table->increments('id');
            $table->string('account_number');
            $table->integer('merchant_id');
            $table->integer('package_id');
            $table->string('status');
            $table->decimal('service_fee', 15, 2)->default('0.00');
            $table->decimal('add_ons_fee', 15, 2)->default('0.00');
            $table->decimal('vat_fee', 15, 2)->default('0.00');
            $table->decimal('total_current', 15, 2)->default('0.00');
            $table->decimal('outstanding_balance', 15, 2)->default('0.00');
            $table->decimal('total_amount', 15, 2)->default('0.00');
            $table->string('business_name')->nullable();
            $table->string('business_type')->nullable();
            $table->string('business_branches', 100)->nullable();
            $table->string('location')->nullable();
            $table->string('authorized_rep', 45)->nullable();
            $table->string('contact_number', 45)->nullable();
            $table->string('reference_number')->nullable();
            $table->string('barcode');
            $table->timestamp('billing_period_start')->nullable();
            $table->timestamp('billing_period_end')->nullable();
            $table->timestamp('statement_date')->nullable();
            $table->timestamp('due_date')->nullable();
            $table->string('deposit_slip')->nullable();
            $table->timestamp('payment_date')->nullable();
            $table->decimal('payment_amount', 15, 2)->default('0.00');
            $table->string('payment_type')->nullable();
            $table->string('or_number')->nullable();
            $table->boolean('is_paid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Billing');
        Schema::create('Billing', function (Blueprint $table) {
            $table->increments('id');
            $table->string('account_number');
            $table->integer('merchant_id');
            $table->string('business_name');
            $table->integer('package_id');
            $table->decimal('outstanding_balance', 15, 2);
            $table->string('status');
            $table->string('reference_number');
            $table->timestamp('payment_date')->nullable();
            $table->decimal('payment_amount', 15, 2);
            $table->string('payment_type');
            $table->string('official_receipt');
            $table->string('official_receipt_number');
            $table->string('barcode');
            $table->boolean('is_paid');
            $table->timestamp('billing_period_start');
            $table->timestamp('billing_period_date');
            $table->timestamps();
        });
    }
}
