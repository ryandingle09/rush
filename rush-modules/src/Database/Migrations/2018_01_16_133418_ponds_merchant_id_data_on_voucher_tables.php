<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PondsMerchantIdDataOnVoucherTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // ponds merchant id.
        $merchant_id = DB::table('Merchant')->where('business_system_link', 'unilever')->value('merchantid');

        DB::table('voucher')->update(['merchant_id' => $merchant_id]);
        DB::table('voucher_codes')->update(['merchant_id' => $merchant_id]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('voucher')->update(['merchant_id' => 0]);
        DB::table('voucher_codes')->update(['merchant_id' => 0]);
    }
}
