<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PunchcardIssueRewardEmailMessage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('notification_email_messages')->insert([
            [
                'merchant_id' => 0,
                'type' => 'customer-punchcard-reward-claim',
                'subject' => '%subject_business_name% Reward Claim',
                'message' => '<p>Hi %name%</p><p>You have claimed the redeemed %reward_name% from %branch_name% on %datetime%. Keep collecting %points_name% stamps every time you make a purchase to redeem exciting rewards! Ref. No %transaction_reference%</p><p>This is a system-generated e-mail. Please do not reply.</p>',
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('notification_email_messages')->where('merchant_id', 0)->where('type', 'customer-punchcard-reward-claim')->delete();
    }
}
