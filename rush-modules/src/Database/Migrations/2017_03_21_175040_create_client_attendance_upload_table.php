<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientAttendanceUploadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_attendance_upload', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('date_time')->index();
            $table->string('client_id', 15)->nullable(false)->index();
            $table->string('client', 60)->nullable(false);
            $table->string('visit_service_category', 15)->nullable(false);
            $table->string('visit_type', 15)->nullable(false);
            $table->string('type', 20)->nullable(false)->index();
            $table->string('pricing_option', 30)->nullable(false);
            $table->string('expiration_date', 30)->nullable(false);
            $table->string('visits_remaining', 30)->nullable(false);
            $table->string('staff', 60)->nullable(false);
            $table->string('visit_location', 20)->nullable(false)->index();
            $table->string('sale_location', 20)->nullable(false);
            $table->boolean('staff_paid')->default(1);
            $table->boolean('late_cancel')->default(1);
            $table->boolean('no_show')->default(1);
            $table->string('booking_method', 20)->nullable(false);
            $table->string('payment_service_category', 10)->nullable(false);
            $table->boolean('status')->default(1)->index;
            $table->timestamps();
            $table->integer('attendance_id')->unsigned()->nullable(false);
        });

        Schema::table('client_attendance_upload', function (Blueprint $table) {
            $table->foreign('attendance_id')
                ->references('id')->on('attendance')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('client_attendance_upload');
    }
}
