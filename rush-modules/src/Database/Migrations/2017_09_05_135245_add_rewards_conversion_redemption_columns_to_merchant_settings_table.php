<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRewardsConversionRedemptionColumnsToMerchantSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('MerchantSettings', function (Blueprint $table) {
            $table->boolean('rewards_stamps_redemption_conversion')->default(false);
            $table->integer('rewards_stamps_redemption_conversion_credit')->default(10);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('MerchantSettings', function (Blueprint $table) {
            $table->dropColumn('rewards_stamps_redemption_conversion');
            $table->dropColumn('rewards_stamps_redemption_conversion_credit');
        });
    }
}
