<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdjustRateCardBilling extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('Merchant', 'msf_type') )
        {
            Schema::table('Merchant', function (Blueprint $table)
            {
                $table->string('msf_type', 255)->default('fixed')->after('monthly_service_fee');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('Merchant', 'msf_type') )
        {
            Schema::table('Merchant', function (Blueprint $table)
            {
                $table->dropColumn('msf_type');
            });
        }
    }
}
