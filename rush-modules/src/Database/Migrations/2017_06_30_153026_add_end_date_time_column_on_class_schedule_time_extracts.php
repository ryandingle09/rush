<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEndDateTimeColumnOnClassScheduleTimeExtracts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('class_schedule_time_extracts', function (Blueprint $table) {
            $table->datetime('start_date_time')->change();
            $table->datetime('end_date_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('class_schedule_time_extracts', function (Blueprint $table) {
            $table->dropColumn(['start_date_time', 'end_date_time']);
        });
    }
}
