<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ShopmaniaQuizData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $shopmania_merchant_id = DB::table('Merchant')->where('business_system_link', 'shopmania')->value('merchantid');

        if( empty($shopmania_merchant_id) ) return;


        $question_id = DB::table('quiz_questions')->insertGetId(['merchant_id' => $shopmania_merchant_id, 'text' => 'Describe your perfect weekend?']);
        DB::table('quiz_question_choices')->insert([
            ['question_id' => $question_id, 'type' => 'Travel Junkie', 'text' => 'Enjoying a vacation out of town.'],
            ['question_id' => $question_id, 'type' => 'Health Buff', 'text' => 'Getting my sweats on.'],
            ['question_id' => $question_id, 'type' => 'Foodie', 'text' => 'Going on a food trip at the newest restaurant.'],
            ['question_id' => $question_id, 'type' => 'Style Star', 'text' => 'A shopping spree at the mall.'],
            ['question_id' => $question_id, 'type' => 'Techie', 'text' => 'A day to play video games at home.'],
        ]);


        $question_id = DB::table('quiz_questions')->insertGetId(['merchant_id' => $shopmania_merchant_id, 'text' => 'Which of these do you find enjoyable?']);
        DB::table('quiz_question_choices')->insert([
            ['question_id' => $question_id, 'type' => 'Travel Junkie', 'text' => 'Exploring new places.'],
            ['question_id' => $question_id, 'type' => 'Health Buff', 'text' => 'Feeling my muscles sore after a good workout.'],
            ['question_id' => $question_id, 'type' => 'Foodie', 'text' => 'Testing out new food places.'],
            ['question_id' => $question_id, 'type' => 'Style Star', 'text' => 'Shopping during sale season.'],
            ['question_id' => $question_id, 'type' => 'Techie', 'text' => 'Reading reviews of the latest gadgets.'],
        ]);


        $question_id = DB::table('quiz_questions')->insertGetId(['merchant_id' => $shopmania_merchant_id, 'text' => 'Where do you hang out with friends?']);
        DB::table('quiz_question_choices')->insert([
            ['question_id' => $question_id, 'type' => 'Travel Junkie', 'text' => 'A near cool place we can do a road trip to.'],
            ['question_id' => $question_id, 'type' => 'Health Buff', 'text' => 'Where I can get my muscles work.'],
            ['question_id' => $question_id, 'type' => 'Foodie', 'text' => 'An IG-worthy restaurant.'],
            ['question_id' => $question_id, 'type' => 'Style Star', 'text' => 'Where I can get my retail therapy fix.'],
            ['question_id' => $question_id, 'type' => 'Techie', 'text' => 'The tech outlet.'],
        ]);


        $question_id = DB::table('quiz_questions')->insertGetId(['merchant_id' => $shopmania_merchant_id, 'text' => 'What do you do to relax?']);
        DB::table('quiz_question_choices')->insert([
            ['question_id' => $question_id, 'type' => 'Travel Junkie', 'text' => 'Daydreaming of my next trip.'],
            ['question_id' => $question_id, 'type' => 'Health Buff', 'text' => 'Hustling for that muscle.'],
            ['question_id' => $question_id, 'type' => 'Foodie', 'text' => 'Counting the memories and the calories.'],
            ['question_id' => $question_id, 'type' => 'Style Star', 'text' => 'Shop, shop, shop!'],
            ['question_id' => $question_id, 'type' => 'Techie', 'text' => 'Geeking on tech reviews.'],
        ]);


        $question_id = DB::table('quiz_questions')->insertGetId(['merchant_id' => $shopmania_merchant_id, 'text' => 'What do you plan to buy on your next shopping day?']);
        DB::table('quiz_question_choices')->insert([
            ['question_id' => $question_id, 'type' => 'Travel Junkie', 'text' => 'Travel bag organizers.'],
            ['question_id' => $question_id, 'type' => 'Health Buff', 'text' => 'Running shoes.'],
            ['question_id' => $question_id, 'type' => 'Foodie', 'text' => 'A buffet coupon.'],
            ['question_id' => $question_id, 'type' => 'Style Star', 'text' => 'A head-to-toe outfit.'],
            ['question_id' => $question_id, 'type' => 'Techie', 'text' => 'New bluetooth speakers.'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('quiz_questions')->truncate();
        DB::table('quiz_question_choices')->truncate();
    }
}
