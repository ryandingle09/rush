<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrNumberOnEmailMessages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('notification_email_messages')
            ->where('merchant_id', 0)
            ->where('type', 'customer-points-earn')
            ->update(['message' => "<p>Hi %name%,</p><p>You have earned %points_earned% %points_name% points for your transaction totaling PHP%amount% in %branch_name% on %datetime%. Your new balance is %current_points% %points_name% points. Keep collecting %points_name% points for every purchase to redeem exciting rewards! Ref. No %transaction_reference%. OR No %or_number%</p><p>This is a system-generated e-mail. Please do not reply.</p>"]);

        DB::table('notification_email_messages')
            ->where('merchant_id', 0)
            ->where('type', 'customer-points-pay')
            ->update(['message' => "<p>Hi %name%,</p><p>You have paid %points_paid% %points_name% points for your transaction totalling PHP%total_amount_purchased% in %branch_name% on %datetime%. Your new balance is %balance_points%. Keep collecting %points_name% points for every purchase to redeem exciting rewards! Ref. No %transaction_reference%. OR No %or_number%</p><p>This is a system-generated e-mail. Please do not reply.</p>"]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('notification_email_messages')
            ->where('merchant_id', 0)
            ->where('type', 'customer-points-earn')
            ->update(['message' => "<p>Hi %name%,</p><p>You have earned %points_earned% %points_name% points for your transaction totaling PHP%amount% in %branch_name% on %datetime%. Your new balance is %current_points% %points_name% points. Keep collecting %points_name% points for every purchase to redeem exciting rewards! Ref. No %transaction_reference%</p><p>This is a system-generated e-mail. Please do not reply.</p>"]);

        DB::table('notification_email_messages')
            ->where('merchant_id', 0)
            ->where('type', 'customer-points-pay')
            ->update(['message' => "<p>Hi %name%,</p><p>You have paid %points_paid% %points_name% points for your transaction totalling PHP%total_amount_purchased% in %branch_name% on %datetime%. Your new balance is %balance_points%. Keep collecting %points_name% points for every purchase to redeem exciting rewards! Ref. No %transaction_reference%</p><p>This is a system-generated e-mail. Please do not reply.</p>"]);
    }
}
