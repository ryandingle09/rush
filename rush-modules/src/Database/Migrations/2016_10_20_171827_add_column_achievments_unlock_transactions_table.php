<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAchievmentsUnlockTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('achievements_unlock_transactions', function (Blueprint $table) {
            $table->string('options_date');
            $table->string('options_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('achievements_unlock_transactions', function (Blueprint $table) {
            $table->dropColumn('options_date');
            $table->dropColumn('options_name');
        });
    }
}
