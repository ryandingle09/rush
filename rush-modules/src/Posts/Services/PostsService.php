<?php

namespace Rush\Modules\Posts\Services;

use Rush\Modules\Posts\Repositories\PostsInterface;
use Rush\Modules\Posts\Repositories\PostsRepository;
use Rush\Modules\Helpers\Facades\StorageHelper;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;

class PostsService
{
    protected $postsRepository;

    public function __construct(PostsInterface $postsRepository)
    {
        $this->postsRepository = $postsRepository;
    }

    public function newPost($data)
    {
        return $this->postsRepository->newPost($data);
    }

    public function getPosts($args)
    {
        return $this->postsRepository->getPosts($args);
    }

    public function getCategory($cat, $merchant_id = null)
    {
        return $this->postsRepository->getCategory($cat, $merchant_id);
    }

    public function getAllCategory( $merchant_id = null )
    {
        return $this->postsRepository->getAllCategory( $merchant_id );
    }

    public function editPost( $data )
    {
        return $this->postsRepository->editPost( $data );
    }

    public function publishEvent( $flag, $id )
    {
        return $this->postsRepository->publishEvent( $flag, $id );
    }

    public function shiftUp($merchantId, $head, $tail)
    {
        return $this->postsRepository->shiftUp($merchantId, $head, $tail);
    }

    public function shiftDown($merchantId, $head, $tail)
    {
        return $this->postsRepository->shiftDown($merchantId, $head, $tail);
    }

    public function store(array $input)
    {
        $eventInput = $input;
    
        $validator = $this->validate(
            $eventInput,
            [   'post_title' => 'required',
                'post_content' => 'required',
                'merchant_id' => 'required',
                'post_thumbnail' => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:width=720,height=720',
                'url' => 'sometimes|url',
                'id' => 'sometimes|exists:'.$this->postsRepository->getTable().',id,merchant_id,'.$eventInput['merchant_id']],
            [   'required' => 'The :attribute is required.']
        );
        
        if ($validator->fails()) {
            return $validator;
        }
        
        $eventInput['post_slug'] = str_slug($eventInput['post_title']);
        $eventInput['post_type'] = $eventInput['post_type'] ? $eventInput['post_type'] : 'posts';
        
        if (!empty($eventInput['id']) && $event = $this->postsRepository->getById( $eventInput['id']) ) {

            if (Input::hasFile('eEventImg')) {
                StorageHelper::deleteFromRepository( $event->post_thumbnail );
                $eventInput['post_thumbnail'] = StorageHelper::move("merchant/posts/thumbnails", 'eEventImg');
            }

            // check for max order
            $lastEvent = $this->postsRepository->getByMerchantIdOrderLast($eventInput['merchant_id']);
            if ($lastEvent && $lastEvent->post_order + 1 == $eventInput['post_order']) {
                $eventInput['post_order'] = $lastEvent->post_order;
            }

            // shift the order accordingly
            if ($event->post_order < $eventInput['post_order']) {
                $shift = $this->shiftDown($eventInput['merchant_id'], $event->post_order, $eventInput['post_order']);
            } elseif ($event->post_order > $eventInput['post_order']) {
                $shift = $this->shiftUp($eventInput['merchant_id'],  $eventInput['post_order'], $event->post_order);
            }

            $event->update($eventInput);
        } else {

            if (Input::hasFile('aEventImg') ) {
            $eventInput['post_thumbnail'] = StorageHelper::move("merchant/posts/thumbnails", 'aEventImg');
        }   

            $shift = $this->shiftUp($eventInput['merchant_id'],  $eventInput['post_order'], null);
            $event = $this->postsRepository->fill($eventInput);
            $event->save();
        }

        $post_categories = $this->postsRepository->updatePostCategories( $event->id, $eventInput['category']);
        return true;
    }

    private function validate($input, $rules, $messages = [])
    {
        return \Validator::make($input, $rules, $messages);
    }
}
