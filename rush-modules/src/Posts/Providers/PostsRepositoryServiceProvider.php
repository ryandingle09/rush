<?php

namespace Rush\Modules\Posts\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Posts\Repositories\PostsInterface;
use Rush\Modules\Posts\Repositories\PostsRepository;

class PostsRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(PostsInterface::class, PostsRepository::class);
    }
}
