<?php

namespace Rush\Modules\Posts\Repositories;

use Rush\Modules\Posts\Models\PostsModel;
use Rush\Modules\Posts\Models\CategoryModel;
use Rush\Modules\Posts\Models\PostsCategoryModel;
use Illuminate\Database\Eloquent\Collection;
use Rush\Modules\Repository\AbstractRepository;

class PostsRepository extends AbstractRepository implements PostsInterface
{
    public function __construct(PostsModel $postsModel)
    {
        parent::__construct($postsModel);
    }

    public function newPost(array $data)
    {
        $post = new $this->model();
        $post->merchant_id = $data['merchant_id'];
        $post->post_title = $data['post_title'];
        $post->post_slug = str_slug($data['post_title']);
        $post->post_content = $data['post_content'];
        $post->post_excerpt = isset($data['post_excerpt']) ? $data['post_excerpt'] : null;
        $post->post_type = isset($data['post_type']) ? $data['post_type'] : 'posts';
        $post->post_status = isset($data['post_status']) ? $data['post_status'] : 'publish';
        $post->post_order = isset($data['post_order']) ? $data['post_order'] : 0;
        $post->post_thumbnail = isset($data['post_thumbnail']) ? $data['post_thumbnail'] : null;
        $post->post_sub_category = isset($data['post_sub_category']) ? $data['post_sub_category'] : null;
        $post->save();
        return $post;
    }

    public function editPost(array $data)
    {
        $post = $this->model->find( $data['id'] );
        if  ( isset($data['post_title']) ) $post->post_title = $data['post_title'];
        if  ( isset($data['post_content']) ) $post->post_content = $data['post_content'];
        if  ( isset($data['post_order']) ) $post->post_order = $data['post_order'];
        if  ( isset($data['post_thumbnail']) ) $post->post_thumbnail = $data['post_thumbnail'];
        if  ( isset($data['post_sub_category']) ) $post->post_thumbnail = $data['post_sub_category'];
        $post->save();
        return $post;
    }

    public function getPosts(array $args)
    {
        $category = null;
        $sub_category = null;
        if (isset($args['cat'])) {
            $category = isset($args['cat']) ? $this->getCategory($args['cat'], isset($args['merchant_id']) ? $args['merchant_id'] : null) : null;
            if (is_null($category)) {
                return new Collection;
            }

            if( isset($args['sub_cat']) ) $sub_category = $args['sub_cat'];
        }
        return $this->model->where(function($query) use ($args, $category, $sub_category) {
                            if (isset($args['merchant_id'])) {
                                $query->where('merchant_id', $args['merchant_id']);
                            }

                            if (isset($args['post_status'])) {
                                $query->where('post_status', $args['post_status']);
                            } else {
                                $query->where('post_status', 'draft');
                            }

                            if (isset($args['cat']) && $category) {
                                $query->whereHas('categories', function ($query) use ($category) {
                                    $query->where('category_id', $category->id);
                                });

                                if( $sub_category ) $query->where('post_sub_category', $sub_category);
                            }
                    })
                    ->get();
    }

    // TODO: create new category
    public function newCategory($title) {}

    // TODO: tag category to post
    public function tagCategory(PostModel $post, $id) {}

    public function getCategory($cat, $merchant_id = null)
    {
        if (is_string($cat)) {
            // try getting category via uuid
            $queryViaUuid = CategoryModel::where('uuid', $cat)->first();
            if (!is_null($queryViaUuid)) {
                return $queryViaUuid;
            }

            // try getting category via name and merchant 
            $query = CategoryModel::where('name', $cat);
            if ($merchant_id) {
                $query = $query->where('merchant_id', $merchant_id);
            }
            return $query->first();
        } else {
            $category =  CategoryModel::find($cat);

            return $category;
        }
    }

    public function getAllCategory($merchant_id = null)
    {
        if ( $merchant_id ) {
            return CategoryModel::where(['merchant_id' => $merchant_id])->get();
        } else {
            return CategoryModel::get();
        }
    }

    public function publishEvent( $flag, $id )
    {
        $post = $this->model->find( $id );
        if ( $flag == true) {
            $post->post_status = 'publish';
        } else {
            $post->post_status = 'draft';
        }
        return $post->save();
    }

    public function shiftUp($merchantId, $head, $tail = null)
    {
        if ($tail) {
            return $this->model
                ->where('merchant_id', $merchantId)
                ->where('post_order', '>=', $head)->where('post_order', '<', $tail)
                ->orderBy('post_order')->increment('post_order');
        } else {
            return $this->model
                ->where('merchant_id', $merchantId)
                ->where('post_order', '>=', $head)
                ->orderBy('post_order')->increment('post_order');
        }
    }

    public function shiftDown($merchantId, $head, $tail)
    {
        return $this->model
            ->where('merchant_id', $merchantId)
            ->where('post_order', '>', $head)
            ->where('post_order', '<=', $tail)
            ->orderBy('post_order')
            ->decrement('post_order');
    }

    public function getByMerchantIdOrderLast($merchantId)
    {
        return $this->model->where('merchant_id', $merchantId)->orderBy('post_order', 'DESC')->first();
    }

    public function updatePostCategories( $post_id, $category_id )
    {
        $post_mode = PostsCategoryModel::where('post_id', $post_id )->first();

        if ( count($post_mode) <= 0 )  {
            $post_mode = new PostsCategoryModel();
        }
        $post_mode->post_id = $post_id;
        $post_mode->category_id = $category_id;
        $post_mode->save();
    }
}
