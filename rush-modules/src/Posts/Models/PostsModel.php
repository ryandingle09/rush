<?php

namespace Rush\Modules\Posts\Models;

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;
use Logaretm\Transformers\Contracts\Transformable;
use Logaretm\Transformers\TransformableTrait;
use Rush\Modules\Helpers\Facades\StorageHelper;
use Rush\Modules\Posts\Models\PostsCategoryModel;

class PostsModel extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = "merchant_posts";
    protected $fillable = [
        'merchant_id',
        'post_title',
        'post_slug',
        'post_content',
        'post_excerpt',
        'post_thumbnail',
        'post_type',
        'post_status',
        'post_order',
        'created_at',
        'updated_at',
        'event_color1',
        'event_color2',
        'url',
        'post_sub_category',
    ];

    public function getCategories()
    {
        return $this->categories()->with('category')->get();
    }

    public function categories()
    {
        return $this->hasMany(PostsCategoryModel::class, 'post_id', 'id');
    }

    public function getCategoryNamesAttribute()
    {
        $category_names = [];

        foreach( $this->getCategories() as $category ){
            $category_names[] = $category->category->name;
        }

        return $category_names;
    }

    public function getPostThumbnailAttribute($value)
    {
        return StorageHelper::repositoryUrlForView($value);
    }

    public function getUuidAttribute( $value )
    {
        if( ! $value ) {
            $value = Uuid::uuid1()->toString();
            $this->uuid = $value;
            $this->save();
        }

        return $value;
    }
}
