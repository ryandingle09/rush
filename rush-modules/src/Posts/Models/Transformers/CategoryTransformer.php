<?php

namespace Rush\Modules\Posts\Models\Transformers;

use Logaretm\Transformers\Transformer;
use Rush\Modules\Helpers\Facades\StorageHelper;
use Rush\Modules\Posts\Models\CategoryModel;

class CategoryTransformer extends Transformer
{
    /**
     * @param  CategoryModel $category
     * @return mixed
     */
    public function getTransformation($category)
    {
        return [
            'id'  => $category->uuid,
            'name'  => $category->name,
            'image_url' => $category->image_url
        ];
    }
}
