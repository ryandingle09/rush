<?php

namespace Rush\Modules\Posts\Models\Transformers;

use Logaretm\Transformers\Transformer;
use Rush\Modules\Helpers\Facades\StorageHelper;
use Rush\Modules\Posts\Models\PostsModel;
use Rush\Modules\Customer\Services\CustomerService;

class PostsTransformer extends Transformer
{
    /**
     * @param  PostsModel $post
     * @return mixed
     */
    public function getTransformation($post)
    {
        $customer_id = request()->customer_id;
        $vote = 0;
        if ($customer_id) {
            /** @var $customerService \Rush\Modules\Customer\Services\CustomerService */
            $customerService = app(CustomerService::class);
            $customer = $customerService->find($customer_id);
            // if customer is pass
            if ($customer) {
                // get vote
                $voteQuery = $customer->votes->where('post_id', $post->id)->first();
                if ($voteQuery) {
                    $vote = $voteQuery->vote;
                }
            }
        }

        return [
            'id'  => (int) $post->id,
            'title' => $post->post_title,
            'content' => $post->post_content,
            'thumbnail_url' => $post->post_thumbnail ? StorageHelper::repositoryUrlForView($post->post_thumbnail, 1) : null,
            'event_color1' => $post->event_color1,
            'event_color2' => $post->event_color2,
            'url' => (string) $post->url,
            'categories' => $post->category_names,
            'sub_category' => (string) $post->post_sub_category,
            'uuid' => $post->uuid,
            'type' => $post->post_type == 'posts' ? 'text' : $post->post_type,
            'vote' => $vote,
            'android_external_link' => (string) $post->android_external_link,
            'ios_external_link' => (string) $post->ios_external_link,
        ];
    }
}
