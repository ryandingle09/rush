<?php

namespace Rush\Modules\Posts\Models;

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;
use Logaretm\Transformers\TransformableTrait;
use Logaretm\Transformers\Contracts\Transformable;
use Rush\Modules\Helpers\Facades\StorageHelper;

class CategoryModel extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = "merchant_posts_categories";
    
    public function getUuidAttribute( $value )
    {
        if( ! $value ) {
            $value = Uuid::uuid1()->toString();
            $this->uuid = $value;
            $this->save();
        }

        return $value;
    }

    public function getImageUrlAttribute( $value )
    {
        return StorageHelper::repositoryUrlForView($value);
    }
}
