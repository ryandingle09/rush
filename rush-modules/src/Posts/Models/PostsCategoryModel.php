<?php

namespace Rush\Modules\Posts\Models;

use Illuminate\Database\Eloquent\Model;
use Rush\Modules\Posts\Models\CategoryModel;

class PostsCategoryModel extends Model
{
    protected $table = "posts_categories";

    public function category()
    {
        return $this->hasOne(CategoryModel::class, 'id', 'category_id');
    }
}
