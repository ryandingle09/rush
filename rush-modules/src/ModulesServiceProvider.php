<?php namespace Rush\Modules;

use File;
use App;

class ModulesServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Will make sure that the required modules have been fully loaded
     * @return void
     */
    public function boot()
    {
        $modules = config("modules");
        while (list(, $module) = each($modules)) {
        }
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $modules = config("modules");
        while (list(, $module) = each($modules)) {
            // Register all providers
            if (file_exists(__DIR__ . '/' . $module . '/Providers/')) {
                $files = File::allFiles(__DIR__ . '/' . $module . '/Providers/');
                foreach ($files as $file) {
                    $provider = basename($file, '.php');
                    App::register('Rush\Modules\\'. ucwords($module) . '\Providers\\'. $provider);
                }
            }
        }
    }
}