<?php

namespace Rush\Modules\Repository;

use Rush\Modules\Repository\RepositoryInterface as Repository;
use Rush\Modules\Repository\RepositoryInterface;

abstract class Criteria
{
    protected $params;

    public function __construct($params = [])
    {
        $this->params = $params;
    }

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public abstract function apply($model, Repository $repository);
}