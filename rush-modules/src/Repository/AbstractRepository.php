<?php

namespace Rush\Modules\Repository;

/**
 * Performs the dynamic implementation of the different Eloquent methods
 */
abstract class AbstractRepository implements RepositoryInterface
{
    /**
     * @var \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    protected $model;

    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all()
    {
        return $this->model->all();
    }

    /**
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function getById($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param array $attribute
     * @return static
     */
    public function create(array $attribute)
    {
        return $this->model->create($attribute);
    }

    /**
     * @param int $id
     * @param array $attributes
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function update($id, array $attributes)
    {
        $temp = $this->getById($id);
        $temp->update($attributes);
        return $temp;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete($id)
    {
        $this->getById($id)->delete();
        return true;
    }

    /**
     * @param array $attribute
     * @return int
     */
    public function destroy(array $attribute)
    {
        return $this->model->destroy($attribute);
    }

    /**
     * @param  array  $attributes
     * @return mixed
     */
    public function fill(array $attributes)
    {
        return $this->model->fill($attributes);
    }

    /**
     * resets model with a new one, helpful in Objects Creation inside loops
     * @param  array  $attributes
     * @return mixed
     */
    public function newFill(array $attributes)
    {
        $model = get_class($this->model);
        $this->model = new $model();
        return $this->model->fill($attributes);
    }

    /**
     * automatically create a new entry in the DB if no match is found
     * @param  array  $attributes the attributes used for matching
     * @return model              eloquent model
     */
    public function firstOrCreate(array $attributes)
    {
        return $this->model->firstOrCreate($attributes);
    }

    /**
     * returns a new model instance if no match is found
     * requires save()
     * @param  array  $attributes the attributes used for matching
     * @return model              eloquent model
     */
    public function firstOrNew(array $attributes)
    {
        return $this->model->firstOrNew($attributes);
    }

    /**
     * Get the database table name
     * @return String
     */
    public function getTable()
    {
        return $this->model->getTable();
    }
}