<?php

namespace Rush\Modules\Repository;

/**
 * RepositoryInterface provides the standard functions
 * to be expected of ANY repository.
 */
interface RepositoryInterface
{
    public function all();

    public function create(array $data);

}