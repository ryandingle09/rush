<?php

namespace Rush\Modules\Repository;

use Rush\Modules\Repository\Criteria;

interface CriteriaInterface
{
    /**
     * @return mixed
     */
    public function getCriteria();

    /**
     * @param Criteria $criteria
     * @return $this
     */
    public function pushCriteria(Criteria $criteria);

    /**
     * @return $this
     */
    public function  applyCriteria();
}