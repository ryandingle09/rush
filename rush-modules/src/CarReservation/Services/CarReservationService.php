<?php

namespace Rush\Modules\CarReservation\Services;

use Rush\Modules\CarReservation\Repositories\CarReservationInterface;
use Mail;

class CarReservationService 
{
    protected $carReservationRepository;

    public function __construct(CarReservationInterface $carReservationRepository)
    {
        $this->carReservationRepository = $carReservationRepository;
    }

    public function saveReservation($merchant_id, array $data)
    {
        $reservation = $this->carReservationRepository->saveReservation($merchant_id, $data);
        $this->sendReservation($reservation);
        return $reservation;
    }

    public function sendReservation($reservation)
    {
        Mail::send('car_reservation::email.reservation', ['reservation' => $reservation],
            function ($message) use ($reservation) {
                $message->from('no-reply@rush.ph', 'RUSH')
                  ->to(getenv('CAR_RESERVATION_EMAIL_RECEIVER'))
                  ->subject("{$reservation->merchant->businessName} Booking via RUSH App");
        });
    }
}