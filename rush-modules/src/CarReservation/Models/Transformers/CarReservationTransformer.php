<?php

namespace Rush\Modules\CarReservation\Models\Transformers;

use Logaretm\Transformers\Transformer;
use Rush\Modules\CarReservation\Models\CarReservationModel;

class CarReservationTransformer extends Transformer
{
    /**
     * @param  CarReservationModel $customer
     * @return mixed
     */
    public function getTransformation($reservation)
    {
        return [
           'type' => $reservation->type,
           'name' => $reservation->name,
           'email' => $reservation->email,
           'home_address' => $reservation->home_address,
           'home_phone_number' => $reservation->home_phone_number,
           'company_name' => $reservation->company_name,
           'company_address' => $reservation->company_address,
           'company_mobile_number' => $reservation->company_mobile_number,
           'mobile_number' => $reservation->mobile_number,
           'start_date' => $reservation->start_datetime->format('Y-m-d'),
           'start_time' => $reservation->start_datetime->format('h:i A'),
           'end_date' => $reservation->end_datetime->format('Y-m-d'),
           'end_time' => $reservation->end_datetime->format('h:i A'),
           'passenger_no' => (int) $reservation->passenger_no,
           'delivery_location' => $reservation->delivery_location,
           'itinerary' => $reservation->itinerary,
           'booked_before' => $reservation->booked_before ? 'yes' : 'no',
           'vehicle_type' => $reservation->vehicle_type,
           'customer_option' => $reservation->customer_option,
           'voucher_code' => $reservation->voucher_code
        ];
    }
}