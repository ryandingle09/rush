<?php

namespace Rush\Modules\CarReservation\Models;

use Illuminate\Database\Eloquent\Model;
use Logaretm\Transformers\TransformableTrait;
use Logaretm\Transformers\Contracts\Transformable;

class CarReservationModel extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = "car_reservations";

    protected $primaryKey = "id";

    protected $dates = [
        'created_at',
        'updated_at',
        'start_datetime',
        'end_datetime'
    ];

    public  function merchant()
    {
        return $this->belongsTo('Rush\Modules\Merchant\Models\MerchantModel', 'merchant_id', 'merchantid');
    }
}