<p>Type: <b>{{ $reservation->type }}</b></p>
<p>Name: <b>{{ $reservation->name }}</b></p>
<p>Email: <b>{{ $reservation->email }}</b></p>
<p>Home Address: <b>{{ $reservation->home_address }}</b></p>
<p>Home Phone Number: <b>{{ $reservation->home_phone_number }}</b></p>
<p>Company Name: <b>{{ $reservation->company_name }}</b></p>
<p>Company Address: <b>{{ $reservation->company_address }}</b></p>
<p>Company Mobile Number: <b>{{ $reservation->company_mobile_number }}</b></p>
<p>Start Date Time: <b>{{ $reservation->start_datetime }}</b></p>
<p>End Date Time: <b>{{ $reservation->end_datetime }}</b></p>
<p>Passenger No.: <b>{{ $reservation->passenger_no }}</b></p>
<p>Delivery Location: <b>{{ $reservation->delivery_location }}</b></p>
<p>Itinerary: <b>{{ $reservation->itinerary }}</b></p>
<p>Booked Before: <b>{{ $reservation->booked_before }}</b></p>
<p>Vehicle Type: <b>{{ $reservation->vehicle_type }}</b></p>
<p>Customer Option: <b>{{ $reservation->customer_option }}</b></p>
<p>Voucher Code: <b>{{ $reservation->voucher_code }}</b></p>