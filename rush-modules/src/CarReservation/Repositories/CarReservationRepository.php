<?php

namespace Rush\Modules\CarReservation\Repositories;

use Rush\Modules\Repository\AbstractRepository;
use Rush\Modules\CarReservation\Models\CarReservationModel;

class CarReservationRepository extends AbstractRepository implements CarReservationInterface
{
    /**
     * CarReservationRepository constructor.
     * @param CarReservationModel $car_reservation_model
     */
    public function __construct(CarReservationModel $car_reservation_model)
    {
        parent::__construct($car_reservation_model);
    }

    public function saveReservation($merchant_id, array $data)
    {
        $this->model->merchant_id = $merchant_id;
        $this->model->type = $data['type'];
        $this->model->name = $data['name'];
        $this->model->email = $data['email'];
        $this->model->home_address = isset($data['home_address']) ? $data['home_address'] : null;
        $this->model->home_phone_number = isset($data['home_phone_number']) ? $data['home_phone_number'] : null;
        $this->model->company_name = isset($data['company_name']) ? $data['company_name'] : null;
        $this->model->company_address = isset($data['company_address']) ? $data['company_address'] : null;
        $this->model->company_mobile_number = isset($data['company_mobile_number']) ? $data['company_mobile_number'] : null;
        $this->model->mobile_number = isset($data['mobile_number']) ? $data['mobile_number'] : null;
        $this->model->start_datetime = $data['start_datetime'];
        $this->model->end_datetime = $data['end_datetime'];
        $this->model->passenger_no = $data['passenger_no'];
        $this->model->delivery_location = isset($data['delivery_location']) ? $data['delivery_location'] : null;
        $this->model->itinerary = $data['itinerary'];
        $this->model->booked_before = isset($data['booked_before']) ? $data['booked_before'] : null;
        $this->model->vehicle_type = $data['vehicle_type'];
        $this->model->customer_option = isset($data['customer_option']) ? $data['customer_option'] : null;
        $this->model->voucher_code = isset($data['voucher_code']) ? $data['voucher_code'] : null;
        $this->model->save();
        return $this->model;
    }
}