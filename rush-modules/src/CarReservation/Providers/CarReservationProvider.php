<?php

namespace Rush\Modules\CarReservation\Providers;

use Illuminate\Support\ServiceProvider;

// Repositories
use Rush\Modules\CarReservation\Repositories\CarReservationInterface;
use Rush\Modules\CarReservation\Repositories\CarReservationRepository;

class CarReservationProvider extends ServiceProvider
{
    public function boot()
    {
        // register package views
        $this->loadViewsFrom(__DIR__.'/../Views', 'car_reservation');
    }
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CarReservationInterface::class, CarReservationRepository::class);
    }
}