<?php
namespace Rush\Modules\Design\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Design\Repositories\DesignInterface;
use Rush\Modules\Design\Repositories\DesignRepository;

class DesignRepositoryServiceProvider extends  ServiceProvider
{
  public function register()
  {
    $this->app->singleton(DesignInterface::class, DesignRepository::class);
  }
}
