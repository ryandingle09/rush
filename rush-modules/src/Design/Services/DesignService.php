<?php
namespace Rush\Modules\Design\Services;

use Rush\Modules\Design\Repositories\DesignInterface;

class DesignService
{
  protected $designRepository;
  
  public function __construct(DesignInterface $designRepository )
  {
    $this->designRepository = $designRepository;
  }

  public function getSavedMerchantApplication( $merchant_id )
  {
    return $this->designRepository->getSavedMerchantApplication( $merchant_id );
  }

  public function getSavedLoyaltyCardDesign( $merchant_id )
  {
    return $this->designRepository->getSavedLoyaltyCardDesign( $merchant_id );
  }

  public function getSavedCustomerAppDesign( $merchant_id )
  {
    return $this->designRepository->getSavedCustomerAppDesign( $merchant_id );
  }
  
  public function getMerchantSettings( $merchant_id )
  {
    return $this->designRepository->getMerchantSettings( $merchant_id );
  }
  
  public function storeDesignDetails($merchantId, $packageId, $designType, $data)
  {
    return $this->designRepository->storeDesignDetails($merchantId, $packageId, $designType, $data);
  }

  public function updateMerchantStamp($merchantId, $stampURL)
  {
    return $this->designRepository->updateMerchantStamp($merchantId, $stampURL);
  }

}