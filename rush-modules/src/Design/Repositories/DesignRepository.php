<?php
namespace Rush\Modules\Design\Repositories;

use Carbon\Carbon;
use DB;
use File;
use Rush\Modules\Design\Models\MerchantPackageModel;
use Rush\Modules\Helpers\Facades\StorageHelper;
use Rush\Modules\Repository\AbstractRepository;

class DesignRepository extends AbstractRepository implements DesignInterface
{

  protected $merchantPackageModel;
  
  public function __construct(MerchantPackageModel $merchantPackageModel)
  {
    $this->merchantPackageModel = $merchantPackageModel;
  }

  public function getSavedMerchantApplication( $merchantId )
  {
    $data = array(
            'logo' => '',
            'background' => '',
            'overlayColor' => '',
            'textColor' => '',
            'buttonsColor' => '',
            'stamp' => ''
        );
    $query = DB::table('MerchantPackage')->select('logoURL','backgroundURL','stampURL','overlayColor','textColor','buttonsColor')->where( [ 'merchantId' => $merchantId, 'designType' => 1 ] );
    if ($query->count() > 0) {
        $result = $query->first();
        $data = array(
            'logo' => StorageHelper::repositoryUrlForView($result->logoURL),
            'background' => StorageHelper::repositoryUrlForView($result->backgroundURL),
            'overlayColor' => $result->overlayColor,
            'textColor' => $result->textColor,
            'buttonsColor' => $result->buttonsColor,
            'stamp' => ( $result->stampURL ) ? StorageHelper::repositoryUrlForView($result->stampURL) : 'assets/images/addImage.jpg',
        );
    }
    return $data;
  }

  public function getSavedLoyaltyCardDesign( $merchantId )
    {
        $data = array(
            'logo' => '',
            'background' => '',
            'cardbgcolor' => ''
        );
        $query = DB::table('MerchantPackage')->select('logoURL','backgroundURL','backgroundColorHEX')->where(['merchantId' => $merchantId, 'designType' => 2]);
        if ($query->count() > 0) {
            $result = $query->first();
            $data = array(
                'logo' => $result->logoURL,
                'background' => $result->backgroundURL,
                'cardbgcolor' => $result->backgroundColorHEX
            );
        }
        return $data;
    }

    public function getSavedCustomerAppDesign($merchantId)
    {
        $data = array(
            'logo' => '',
            'background' => '',
            'overlayColor' => '',
            'textColor' => '',
            'buttonsColor' => '',
            'stamp' => '',
            'name' => ''
        );
        $query = DB::table('MerchantPackage')->select('logoURL','backgroundURL','stampURL','overlayColor','textColor','buttonsColor','merchantName')->where(['merchantId' => $merchantId, 'designType' => 3]);
        if ($query->count() > 0) {
            $result = $query->first();
            $data = array(
                'logo' => StorageHelper::repositoryUrlForView($result->logoURL),
                'background' => StorageHelper::repositoryUrlForView($result->backgroundURL),
                'overlayColor' => $result->overlayColor,
                'textColor' => $result->textColor,
                'buttonsColor' => $result->buttonsColor,
                'stamp' => ( $result->stampURL ) ? StorageHelper::repositoryUrlForView($result->stampURL) : 'assets/images/addImage.jpg',
                'name' => $result->merchantName
            );
        }
        return $data;
    }

    public function getMerchantSettings($merchantId)
    {
        $settings = array();
        $query = DB::table('MerchantSettings')->select('program_name','points_name')->where( ["merchant_id" => $merchantId] );
        if ($query->count() > 0) {
            $settings = $query->first();
        }
        return $settings;
    }

    public function storeDesignDetails($merchantId, $packageId, $designType, $data)
    {
        $query = DB::table('MerchantPackage')->where( ['packageId' => $packageId, 'merchantId' => $merchantId, 'designType' => $designType ]);
        if ($query->count() > 0) {
            $result = $query->first();
            $id = $result->merchantPackageId;
            $data['updated_at'] = Carbon::now();

            if (isset($data['logoURL'])) {
                StorageHelper::deleteFromRepository($result->logoURL);
            }
            if (isset($data['backgroundURL'])) {
                StorageHelper::deleteFromRepository($result->backgroundURL);
            }
            if (isset($data['stampURL'])) {
                StorageHelper::deleteFromRepository($result->stampURL);
            }

            return DB::table('MerchantPackage')->where('merchantPackageId',$id)->update( $data );
        } else {
            $data['merchantId'] = $merchantId;
            $data['packageId'] = $packageId;
            $data['designType'] = $designType;
            $data['created_at'] = Carbon::now();
            return DB::table('MerchantPackage')->insert( $data );
        }
    }

    public function updateMerchantStamp($merchantId, $stampURL)
    {
        $data = ['stampURL' => $stampURL ];
        return DB::table('MerchantPackage')->where( [ 'merchantId' => $merchantId, 'designType' => 1 ] )->update( $data );
    }

 
}
