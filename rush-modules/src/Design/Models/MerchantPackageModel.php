<?php

namespace Rush\Modules\Design\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantPackageModel extends Model {
    
    protected $table = "MerchantPackage";
    protected $primaryKey = "merchantPackageId";
    
}
