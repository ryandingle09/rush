<?php
namespace Rush\Modules\Admin\Services;

use Rush\Modules\Admin\Repositories\PackageInterface;
use Rush\Modules\Admin\Repositories\BusinessTypeInterface;
use Rush\Modules\Admin\Repositories\ModulesInterface;
use Rush\Modules\Admin\Repositories\ModulesMerchantInterface;
use Rush\Modules\Admin\Repositories\AdminReasonInterface;
use Rush\Modules\Admin\Repositories\AdminUserInterface;
use Rush\Modules\Admin\Repositories\BroadcastToolsModulesInterface;
use Rush\Modules\Admin\Repositories\BroadcastToolsManagementInterface;
use Rush\Modules\Admin\Repositories\CustomerPointsInterface;
use Rush\Modules\Merchant\Repositories\MerchantInterface;
use Rush\Modules\Merchant\Repositories\BranchInterface;
use Rush\Modules\Merchant\Repositories\EmployeeInterface;
use Rush\Modules\Customer\Repositories\CustomerInterface;
use Rush\Modules\Admin\Repositories\AdminRewardsInterface;
use Rush\Modules\Billing\Repositories\BillingInterface;
use Rush\Modules\Helpers\Facades\StorageHelper;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Rush\Modules\Admin\Repositories\ProspectsInterface;
use Rush\Modules\ClassManagement\Repositories\ClassManagementToolsModulesInterface;
use Rush\Modules\ClassManagement\Repositories\ClassManagementToolsInterface;
use Rush\Modules\Admin\Repositories\ResellerInterface;

class AdminService
{
  protected $packageRepository;
  protected $businessTypeRepository;
  protected $modulesRepository;
  protected $merchantRepository;
  protected $modulesMerchantModel;
  protected $adminReasonRepository;
  protected $adminUserRepository;
  protected $branchRepository;
  protected $employeeRepository;
  protected $customerRepository;
  protected $broadcastToolsModulesRepository;
  protected $broadcastToolsManagementRepository;
  protected $customerPointsRepository;
  protected $adminRewardsRepository;
  protected $prospectsRepository;
  protected $classManagementToolsModulesRepository;
  protected $classManagementToolsRepository;
  protected $resellerRepository;

  public function __construct(PackageInterface $packageRepository, BusinessTypeInterface $businessTypeRepository, ModulesInterface $modulesRepository, MerchantInterface $merchantRepository, ModulesMerchantInterface $modulesMerchantRepository, AdminReasonInterface $adminReasonRepository, AdminUserInterface $adminUserRepository, BranchInterface $branchRepository, EmployeeInterface $employeeRepository, CustomerInterface $customerRepository, BroadcastToolsModulesInterface $broadcastToolsModulesRepository, BroadcastToolsManagementInterface $broadcastToolsManagementRepository, CustomerPointsInterface $customerPointsRepository, AdminRewardsInterface $adminRewardsRepository, BillingInterface $billingRepository, ProspectsInterface $prospectsRepository, ClassManagementToolsModulesInterface $classManagementToolsModulesRepository, ClassManagementToolsInterface $classManagementToolsRepository, ResellerInterface $resellerRepository)
  {
    $this->packageRepository = $packageRepository;
    $this->businessTypeRepository = $businessTypeRepository;
    $this->modulesRepository = $modulesRepository;
    $this->merchantRepository = $merchantRepository;
    $this->modulesMerchantRepository = $modulesMerchantRepository;
    $this->adminReasonRepository = $adminReasonRepository;
    $this->adminUserRepository = $adminUserRepository;
    $this->branchRepository = $branchRepository;
    $this->employeeRepository = $employeeRepository;
    $this->customerRepository = $customerRepository;
    $this->broadcastToolsModulesRepository = $broadcastToolsModulesRepository;
    $this->broadcastToolsManagementRepository = $broadcastToolsManagementRepository;
    $this->customerPointsRepository = $customerPointsRepository;
    $this->adminRewardsRepository = $adminRewardsRepository;
    $this->billingRepository = $billingRepository;
    $this->prospectsRepository = $prospectsRepository;
    $this->classManagementToolsModulesRepository = $classManagementToolsModulesRepository;
    $this->classManagementToolsRepository = $classManagementToolsRepository;
    $this->resellerRepository = $resellerRepository;
  }

  public function getAdminById($id)
  {
    return $this->adminUserRepository->getById($id);
  }

  public function getMerchantNameForDisplay($merchant_id)
  {
    $merchant = $this->merchantRepository->getById($merchant_id);
    return $merchant->businessName;
  }

  public function updateAdmin($id, $data, $admin_id)
  {
    return $this->adminUserRepository->updateAdmin($id, $data, $admin_id);
  }

  public function getMerchants()
  {
    return $this->merchantRepository->all();
  }

  public function merchantExist($id)
  {
    return $this->merchantRepository->checkIfExists($id);
  }

  public function getMerchantById($id)
  {
    return $this->merchantRepository->getById($id);
  }

  public function getMerchantPackageId($id)
  {
    $merchant = $this->merchantRepository->getById($id);
    return $merchant->packageId;
  }

  public function getPackages()
  {
    return $this->packageRepository->getPackages();
  }

  public function getBusinessTypes()
  {
    return $this->businessTypeRepository->getBusinessTypes();
  }

  public function getModulesByPackage($package_id)
  {
    return $this->modulesRepository->getModulesByPackage($package_id);
  }

  public function getModulesByMerchant($merchant_id)
  {
    return $this->modulesMerchantRepository->getModulesByMerchant($merchant_id);
  }

  public function updateMerchant($merchant_id, $data, $package_id, $admin_id = 1)
  {
    $merchant_update = $this->merchantRepository->adminUpdateMerchant( $merchant_id, $data );
    
    // $merchant_modules_update = $this->modulesMerchantRepository->updateByMerchant($merchant_id, $data['merchant_modules'], $package_id);
    $cms_modules = isset($data['cms_modules']) ? $data['cms_modules'] : [];
    $broadcast_tools_modules = isset($data['broadcast_tools_modules']) ? $data['broadcast_tools_modules'] : [];
    $class_management_tools_modules = isset($data['class_management_tools_modules']) ? $data['class_management_tools_modules'] : [];

    $cms_modules_update = $this->modulesMerchantRepository->updateByMerchant($merchant_id, $cms_modules);
    $broadcast_tools_modules_update = $this->broadcastToolsManagementRepository->updatebyMerchant( $merchant_id, $broadcast_tools_modules );
    $class_management_tools_modules_update = $this->classManagementToolsRepository->updatebyMerchant( $merchant_id, $class_management_tools_modules );

    if ( $data['enabled'] != "" ) $this->logStatusChange( $merchant_id, $data['reason'], $data['enabled'], $admin_id );
    if ( $merchant_update && $broadcast_tools_modules_update && $class_management_tools_modules_update) return TRUE;
  }

  public function disableMerchant( $merchant_id, $text = '')
  {
    return $this->merchantRepository->toggleMerchantEnableDisable( $merchant_id, FALSE );
  }

  public function enableMerchant( $merchant_id, $text = '' )
  {
    return $this->merchantRepository->toggleMerchantEnableDisable( $merchant_id, TRUE );
  }

  public function logStatusChange( $merchant_id, $text, $status, $admin_id )
  {
    return $this->adminReasonRepository->logStatusChange( $merchant_id, $text, $status, $admin_id );
  }

  public function getEnabledlogs( $merchant_id, $status = null )
  {
    return $this->adminReasonRepository->getEnabledlogs( $merchant_id, $status );
  }

  public function getAllBranches()
  {
    return $this->branchRepository->all();
  }

  public function getBranchesByMerchant( $merchant_id )
  {
    return $this->branchRepository->getBranchesByMerchant( $merchant_id );
  }

  public function getAllEmployees()
  {
    return $this->employeeRepository->all();
  }

  public function getEmployeesByMerchant( $merchant_id )
  {
    return $this->employeeRepository->getEmployeesByMerchant( $merchant_id );
  }

  public function getAllCustomers()
  {
    return $this->customerRepository->all();
  }

  public function getCustomersByMerchant($merchant_id)
  {
    return $this->customerRepository->getCustomersByMerchant( $merchant_id );
  }

  public function getBroadcastToolsModules()
  {
    return $this->broadcastToolsModulesRepository->getBroadcastToolsModules();
  }

  public function getClassManagementToolsModules()
  {
    return $this->classManagementToolsModulesRepository->getClassManagementToolsModules();
  }

  public function getPoints()
  {
    return $this->customerPointsRepository->all();
  }

  public function getPointsByMerchant( $merchant_id )
  {
    return $this->customerPointsRepository->getPointsByMerchant( $merchant_id );
  }

  public function getTransactionsByMerchant( $merchant_id )
  {
    return $this->merchantRepository->getMerchantTransactions( $merchant_id );
  }

  public function get_admin_rewards( $admin = false )
  {
    return $this->adminRewardsRepository->get_admin_rewards( $admin );
  }

  public function reward_save( $data, $id = null )
  {
    return $this->adminRewardsRepository->reward_save( $data, $id );
  }

  public function get_reward_info( $id )
  {
    return $this->adminRewardsRepository->get_reward_info( $id );
  }

  public function reward_status( $id )
  {
    if ( $id ) return $this->adminRewardsRepository->reward_status( $id );
    else return false;
  }

  public function getSubscriptionType()
  {
    return $this->adminRewardsRepository->getSubscriptionType();
  }

  public function getAdminRewardsTransactions()
  {
    return $this->adminRewardsRepository->getAdminRewardsTransactions();
  }

  public function getBillings()
  {
    $billings = $this->billingRepository->all();
    return $billings->sortByDesc('is_paid');
  }

  public function getBillingById( $id )
  {
    return $this->billingRepository->getById( $id );
  }

  public function tagPayment( $id, $data )
  {
    $validator = $this->validate(
        $data,
        [   'payment_type' => 'required',
            'or_number' => 'required',
            'payment_date' => 'required',
            'payment_amount' => 'required',
            'reference_number' => 'required',
            'deposit_slip' => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        [   'required' => 'The :attribute is required.']
        ]
    );

    if ( $validator->fails() ) {
        return $validator;
    } else {
        $billing = $this->billingRepository->getById( $id );

        if (Input::hasFile('deposit_slip')) {
            StorageHelper::deleteFromRepository( $billing->deposit_slip );
            $data['deposit_slip'] = StorageHelper::move("merchant/slip", 'deposit_slip');
        }

        $billing = $this->billingRepository->tagPayment( $id, $data );
    }

  }

  private function validate($input, $rules, $messages = [])
  {
      return \Validator::make($input, $rules, $messages);
  }

  public function login( $email, $password )
  {
    return $this->adminUserRepository->login( $email, $password );
  }

  public function getProspects( $id = null)
  {
    return $this->prospectsRepository->getProspects( $id );
  }

  public function getProspectsInfo( $id )
  {
    return $this->prospectsRepository->getProspectsInfo( $id );
  }

  public function getBDaccounts()
  {
    return $this->prospectsRepository->getBDaccounts();
  }

  public function assignBD( $prospect_id, $bd_id )
  {
    return $this->prospectsRepository->assignBD( $prospect_id, $bd_id );
  }

  public function getResellers()
  {
    return $this->resellerRepository->getResellers();
  }

  public function getResellersOptions()
  {
    return $this->resellerRepository->getResellersOptions();
  }

  public function getAdminModules( $type = 1, $id = null)
  {
    return $this->adminUserRepository->getAdminModules( $type, $id );
  }

  public function bd_login( $email, $password )
  {
    return $this->adminUserRepository->bd_login( $email, $password );
  }

  public function getAdminModulesRoute( $id, $array = null)
  {
      $modules = $this->adminUserRepository->getAdminModulesRoute( $id );
      if ( !$array )
      {
          return $modules;
      }
      else
      {
          if( count($modules) > 0 ) {
              $modulesArray = array('admin-home');
              foreach( $modules as $module )
              {
                  $modulesArray[] = $module->route_name;
              }
              return $modulesArray;
          }
          else
          {
              return null;
          }
      }
  }

  public function getBDaccountInfo( $id )
  {
    return $this->prospectsRepository->getBDaccountInfo( $id );
  }

  public function updateBDAccount( $id = null, $data )
  {
    return $this->prospectsRepository->updateBDAccount( $id, $data );
  }

  public function deleteBDAccount( $id )
  {
    return $this->prospectsRepository->deleteBDAccount( $id );
  }

}