<?php
namespace Rush\Modules\Admin\Repositories;

use Rush\Modules\Admin\Models\AdminRewardsModel;
use Rush\Modules\Merchant\Models\MerchantModel;
use Rush\Modules\Merchant\Models\MerchantGlobeRewardsModel;
use Rush\Modules\Customer\Models\GlobeNumbersPrefixModel;
use Rush\Modules\Repository\AbstractRepository;
use DB;

class AdminRewardsRepository extends AbstractRepository implements AdminRewardsInterface
{
  protected $adminRewardsModel;

  public function __construct(AdminRewardsModel $adminRewardsModel)
  {
    $this->adminRewardsModel = $adminRewardsModel;
  }

  public function get_admin_rewards( $admin = false )
  {
    if ( $admin ) {
      return $this->adminRewardsModel->all();
    } else {
      return AdminRewardsModel::where([ "status" => 1 ])->get();
    }
  }

  public function reward_save( $data, $id = null )
  {
	if ( $id ) $reward = AdminRewardsModel::find( $id );
	else $reward = new AdminRewardsModel;

	$reward->name = $data['name'];
	$reward->details = ( isset($data['details']) && !empty($data['details']) ) ? $data['details'] : null;
    $reward->pointsRequired = ( isset($data['pointsRequired']) && !empty($data['pointsRequired']) ) ? $data['pointsRequired'] : null;
	$reward->type = ( $data['type'] ) ? $data['type'] : null;
    if ( isset($data['imageURL']) ) {
		$reward->imageURL = $data['imageURL'];
	}
    $reward->status = $data['status'];
    $reward->save();
	return $this->update_admin_reward_merchant_table( $reward, $data['status']);
  }

  public function get_reward_info( $id )
  {
	return AdminRewardsModel::find( $id );
  }

  public function reward_status( $id )
  {
    $reward = AdminRewardsModel::find( $id );
    $status = $reward->status;
    if ( $status == 1 )
    {
      $reward->status = 0;
      $reward->save();
      return $this->update_admin_reward_merchant_table( $reward, 0);
    } else {
      $reward->status = 1;
      $reward->save();
      return $this->update_admin_reward_merchant_table( $reward, 1);
    }
  }

  public function update_admin_reward_merchant_table( $admin_reward, $status = 1)
  {
    $merchants = Merchantmodel::all();
    foreach( $merchants as $merchant ) {

      $reward = MerchantGlobeRewardsModel::where([ "admin_item_id" => $admin_reward->id, "merchantid" => $merchant->merchantid ]);

      if ( $reward->count() > 0 ) {
        $reward = $reward->first();
        $reward->name = $admin_reward->name;
        $reward->details = $admin_reward->details;
        $reward->status = $status;
        $reward->type = $admin_reward->type;
        $reward->save();
      } else {

        $reward = new MerchantGlobeRewardsModel();
        $reward->merchantid = $merchant->merchantid;
        $reward->name = $admin_reward->name;
        $reward->details = $admin_reward->details;
        $reward->type = $admin_reward->type;

        $points_calc = $admin_reward->pointsRequired;

        if ( $merchant->conversion_settings ) {
          $redemption_peso = ( $merchant->conversion_settings->redemption_peso ) ?: 1;
          $redemption_points = ( $merchant->conversion_settings->redemption_points ) ?: 1;
          $points_calc = $admin_reward->pointsRequired / $redemption_peso;
          $points_calc = round($points_calc * $redemption_points);
        }

        $reward->pointsRequired = $points_calc;
        $reward->imageURL = $admin_reward->imageURL;
        $reward->status = $status;
        $reward->timestamp = date("Y-m-d H:i:s");
        $reward->admin_item_id = $admin_reward->id;
        $reward->save();
      }
    }
    return true;
  }

  public function getSubscriptionType()
  {
    return GlobeNumbersPrefixModel::distinct()->select('brand')->get();
  }

  public function getAdminRewardsTransactions()
  {
    $sql ='SELECT ct.timestamp, m.businessName, c.mobileNumber, ct.pointsRedeem, ar.pointsRequired 
          FROM CustomerTransaction ct
          LEFT JOIN Redeem r
          ON r.redeemReferenceCode = ct.transactionReferenceCode
          LEFT JOIN redeemedItems ri
          ON r.redeemId = ri.redeemId
          LEFT JOIN MerchantGlobeRewards mgr
          ON ri.redeemItemId = mgr.redeemItemId
          LEFT JOIN admin_rewards ar
          ON mgr.admin_item_id = ar.id
          LEFT JOIN Merchant m
          ON ct.merchant_id = m.merchantid
          LEFT JOIN Customer c
          ON ct.customerId = c.customerId
          WHERE ct.amax_transaction_id <> 0
          AND ct.transactionType = "redeem"
    ';
    $rewards = DB::select( $sql );
    return $rewards;
  }
}
