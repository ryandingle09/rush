<?php
namespace Rush\Modules\Admin\Repositories;

use Rush\Modules\Admin\Models\PackageModel;
use Rush\Modules\Repository\AbstractRepository;

class PackageRepository extends AbstractRepository implements PackageInterface
{
  protected $packageModel;

  public function __construct(PackageModel $packageModel)
  {
    $this->packageModel = $packageModel;
  }

  public function getPackages()
  {
    return $this->packageModel->all();
  }
}
