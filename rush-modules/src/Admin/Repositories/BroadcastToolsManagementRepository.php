<?php
namespace Rush\Modules\Admin\Repositories;

use Rush\Modules\Admin\Models\BroadcastToolsManagementModel;
use Rush\Modules\Repository\AbstractRepository;

class BroadcastToolsManagementRepository extends AbstractRepository implements BroadcastToolsManagementInterface
{

  public function __construct(BroadcastToolsManagementModel $broadcastToolsManagementModel)
  {
    parent::__construct($broadcastToolsManagementModel);
  }

  public function updatebyMerchant( $merchant_id, $modules_id )
  {
    $data = $this->model->where(['merchant_id' => $merchant_id])->get();
    if ( $data->count() > 0 ) $this->model->where(['merchant_id' => $merchant_id])->delete();
    foreach($modules_id as $module) {
      $moduleMerchant = new BroadcastToolsManagementModel();
      $moduleMerchant->broadcast_tools_id = $module;
      $moduleMerchant->merchant_id = $merchant_id;
      $moduleMerchant->save();
    }
    return TRUE;
  }
}
