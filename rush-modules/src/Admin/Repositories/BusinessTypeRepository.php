<?php
namespace Rush\Modules\Admin\Repositories;

use Rush\Modules\Admin\Models\BusinessTypeModel;
use Rush\Modules\Repository\AbstractRepository;

class BusinessTypeRepository extends AbstractRepository implements BusinessTypeInterface
{
  protected $businessTypeModel;

  public function __construct(BusinessTypeModel $businessTypeModel)
  {
    $this->businessTypeModel = $businessTypeModel;
  }

  public function getBusinessTypes()
  {
    return $this->businessTypeModel->all();
  }
}
