<?php
namespace Rush\Modules\Admin\Repositories;

use Rush\Modules\Admin\Models\AdminReasonModel;
use Rush\Modules\Repository\AbstractRepository;

class AdminReasonRepository extends AbstractRepository implements AdminReasonInterface
{
  protected $adminReasonModel;

  public function __construct(AdminReasonModel $adminReasonModel)
  {
    $this->adminReasonModel = $adminReasonModel;
  }

  public function logStatusChange( $merchant_id, $text, $status, $admin_id )
  {
    $status = ( $status == "enabled" ) ? 0 : 1;
    $statusLog = new AdminReasonModel();
    $statusLog->merchant_id = $merchant_id;
    $statusLog->reason = $text;
    $statusLog->enabled = $status;
    $statusLog->created_by = $admin_id;
    return $statusLog->save();
  }

  public function getEnabledlogs( $merchant_id, $status = null )
  {
    $where = [ 'merchant_id' => $merchant_id ];
    if ( $status ) $where['enabled'] = $status;
    return $this->adminReasonModel->where( $where )->orderBy('id', 'desc')->get();
  }
}
