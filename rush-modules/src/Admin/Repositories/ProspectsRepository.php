<?php
namespace Rush\Modules\Admin\Repositories;

use Rush\Modules\Admin\Models\ProspectsModel;
use Rush\Modules\Admin\Models\BdaccountModel;
use Rush\Modules\Admin\Models\BdAdminModel;
use Rush\Modules\Repository\AbstractRepository;
use Illuminate\Http\Request;

class ProspectsRepository extends AbstractRepository implements ProspectsInterface
{
  protected $prospectsModel;
  protected $bdaccountModel;
  protected $adminModulesModel;
  protected $request;

  public function __construct(ProspectsModel $prospectsModel, Request $request, BdaccountModel $bdaccountModel, BdAdminModel $bdAdminModel)
  {
    $this->prospectsModel = $prospectsModel;
    $this->bdaccountModel = $bdaccountModel;
    $this->bdAdminModel = $bdAdminModel;
    $this->request = $request;
  }

  public function save( $data )
  {
    $fields = [ 'fullname' => 'name',
                'firstname' => 'fname',
                'lastname' => 'lname',
                'email' => 'email',
                'mobile' => 'mobile',
                'company' => 'companyname',
                'industry' => 'industry',
                'package' => 'package',
                'objective' => 'objective',
                'message' => 'request_message',
                'url' => 'url',
                'ip_address' => 'ip_address'
              ];

    $prospect = new ProspectsModel();

    foreach( $fields as $key => $value )
    {
      if ( isset( $data[$value] ) && !empty($data[$value]) ) {
        if ( in_array( $value, ['industry','objective'] ) ) {
          $prospect->$key = json_encode( $data[$value] );
        } else {
          $prospect->$key = $data[$value];
        }
      } 
    }
    return $prospect->save();
  }

  public function getProspects( $id )
  {
    if ( $id ) {
      $prospects = $this->prospectsModel->where([ "bd_assigned" => $id ])->get();
    } else {
      $prospects = $this->prospectsModel->all();
    }
    return $prospects;
  }

  public function getProspectsInfo( $id )
  {
    return $this->prospectsModel->find( $id );
  }

  public function getBDaccounts()
  {
    return $this->bdaccountModel->all();
  }

  public function assignBD( $prospect_id, $bd_id )
  {
    $prospect = ProspectsModel::find( $prospect_id );
    $prospect->bd_assigned = $bd_id;
    return $prospect->save();
  }

  public function getBDaccountInfo( $id )
  {
    return $this->bdaccountModel->where(['id' => $id])->first();
  }

  public function updateBDAccount( $id, $data)
  {
    $prospect = $this->bdaccountModel->find( $id );
    if( count($prospect) > 0 ) {
      
      $prospect->name = $data['name'];
      $prospect->save();

      if ( $this->bdAdminModel->where(['bd_id' => $id ])->count() > 0 ) {
        $this->bdAdminModel->where(['bd_id' => $id ])->delete();
      }

      $insert_arr = [];
      foreach( $data['admin_modules'] as $module )
      {
        $insert_arr[] = ['module_id' => $module, 'bd_id' => $id ];
      }

      $this->bdAdminModel->insert( $insert_arr );
    } else {
      $insert_data['name'] = $data['name'];
      $insert_data['email'] = $data['email'];
      $insert_data['password'] = hash("sha512", $data['password']);

      $id = $this->bdaccountModel->insertGetId( $insert_data );
      
      $insert_arr = [];
      foreach( $data['admin_modules'] as $module )
      {
        $insert_arr[] = ['module_id' => $module, 'bd_id' => $id ];
      }

      $this->bdAdminModel->insert( $insert_arr );
    }
    return $id;
  }

  public function deleteBDAccount( $id )
  {
    $data = $this->bdaccountModel->find( $id );
    if( count($data) > 0 ) {
      $this->bdaccountModel->where(['id' => $id ])->delete();
    }

    if ( $this->bdAdminModel->where(['bd_id' => $id ])->count() > 0 ) {
      $this->bdAdminModel->where(['bd_id' => $id ])->delete();
    }
  }

}
