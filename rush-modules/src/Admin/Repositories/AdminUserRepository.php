<?php
namespace Rush\Modules\Admin\Repositories;

use Rush\Modules\Admin\Models\AdminUserModel;
use Rush\Modules\Admin\Models\AdminModulesModel;
use Rush\Modules\Admin\Models\BdaccountModel;
use Rush\Modules\Repository\AbstractRepository;
use DB;

class AdminUserRepository extends AbstractRepository implements AdminUserInterface
{
  protected $adminUserModel;
  protected $adminModulesModel;
  protected $bdaccountModel;

  public function __construct(AdminUserModel $adminUserModel, AdminModulesModel $adminModulesModel, BdaccountModel $bdaccountModel)
  {
    $this->adminUserModel = $adminUserModel;
    $this->adminModulesModel = $adminModulesModel;
    $this->bdaccountModel = $bdaccountModel;
  }

  public function getById( $id )
  {
    return AdminUserModel::find($id)  ;
  }

  public function updateAdmin($id, $data, $admin_id)
  {
    $admin = AdminUserModel::find($id);
    $admin->firstname = $data['firstname'];
    $admin->lastname = $data['lastname'];
    $admin->updated_by = $admin_id;
    if( isset($data['password']) && $data['password'] != '' ) {
      $admin->password = hash("sha512", $data['password']);
    }
    return $admin->save();
  }

  public function login( $email, $password )
  {
    return AdminUserModel::where( [ 'email' => $email, 'password' => hash("sha512", $password) ] )->first();
  }

  public function getAdminModules( $type = 1, $id = null )
  {
    $modules = [];
    if ( $type != 1 )
    {
      $modules = $this->adminModulesModel->join('bd_admin_modules', 'admin_modules.id', '=', 'bd_admin_modules.module_id')->where(['bd_admin_modules.bd_id' => $id])->get();
    } else {
      $modules = $this->adminModulesModel->all();
    }
    return $modules;
  }

  public function bd_login( $email, $password )
  {
    return $this->bdaccountModel->where( [ 'email' => $email, 'password' => hash("sha512", $password) ] )->first();
  }

  public function getAdminModulesRoute( $id )
  {
    return $modules = $this->adminModulesModel->select('admin_modules.route_name')->join('bd_admin_modules', 'admin_modules.id', '=', 'bd_admin_modules.module_id')->where(['bd_admin_modules.bd_id' => $id])->get();
  }
  
}
