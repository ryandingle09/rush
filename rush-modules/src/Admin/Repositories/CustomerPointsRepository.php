<?php
namespace Rush\Modules\Admin\Repositories;

use Rush\Modules\Admin\Models\CustomerPointsModel;
use Rush\Modules\Repository\AbstractRepository;

class CustomerPointsRepository extends AbstractRepository implements CustomerPointsInterface
{
  public function __construct(CustomerPointsModel $customerPointsModel)
  {
    parent::__construct($customerPointsModel);  
  }
}
