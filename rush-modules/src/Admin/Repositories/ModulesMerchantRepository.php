<?php
namespace Rush\Modules\Admin\Repositories;

use Rush\Modules\Admin\Models\ModulesMerchantModel;
use Rush\Modules\Repository\AbstractRepository;

class ModulesMerchantRepository extends AbstractRepository implements ModulesMerchantInterface
{
  protected $modulesMerchantModel;

  public function __construct(ModulesMerchantModel $modulesMerchantModel)
  {
    $this->modulesMerchantModel = $modulesMerchantModel;
  }

  public function getModulesByMerchant($merchant_id)
  {
    return $this->modulesMerchantModel->where(['merchant_id' => $merchant_id])->get();
  }

  public function updateByMerchant( $merchant_id, $modules )
  {
    $data = $this->modulesMerchantModel->where(['merchant_id' => $merchant_id])->get();
    if ( $data->count() > 0 ) $this->modulesMerchantModel->where(['merchant_id' => $merchant_id])->delete();
    foreach($modules as $module) {
      $moduleMerchant = new ModulesMerchantModel();
      $moduleMerchant->module_id = $module;
      $moduleMerchant->merchant_id = $merchant_id;
      $moduleMerchant->save();
    }
    return TRUE;
  }

  public function getMerchantModulesRoute( $id )
  {
    return $this->modulesMerchantModel->where( ['modules_merchant.merchant_id' => $id ] )
                                      ->join('modules', 'modules_merchant.module_id', '=', 'modules.id')
                                      ->select('modules.route_name')
                                      ->get();
  }

  public function getMerchantModules( $id )
    {
      return $this->modulesMerchantModel->where( ['merchant_id' => $id ] )
                                        ->join('modules', 'modules_merchant.module_id', '=', 'modules.id')
                                        ->select('modules_merchant.id', 'modules_merchant.merchant_id', 'modules_merchant.module_id', 'modules.code', 'modules.name', 'modules.url')
                                        ->get();
    }

}
