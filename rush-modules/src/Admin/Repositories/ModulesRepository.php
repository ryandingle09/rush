<?php
namespace Rush\Modules\Admin\Repositories;

use Rush\Modules\Admin\Models\ModulesModel;
use Rush\Modules\Repository\AbstractRepository;

class ModulesRepository extends AbstractRepository implements ModulesInterface
{
  protected $modulesModel;

  public function __construct(ModulesModel $modulesModel)
  {
    $this->modulesModel = $modulesModel;
  }

  public function getModulesByPackage($package_id)
  {
    return $this->modulesModel->where(['package_id' => $package_id])->get();
  }

}
