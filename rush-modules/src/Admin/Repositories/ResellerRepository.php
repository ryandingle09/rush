<?php
namespace Rush\Modules\Admin\Repositories;

use Rush\Modules\Admin\Models\ResellerModel;
use Rush\Modules\Repository\AbstractRepository;
use Illuminate\Http\Request;

class ResellerRepository extends AbstractRepository implements ResellerInterface
{
  protected $resellerModel;
  protected $request;

  public function __construct(ResellerModel $resellerModel, Request $request)
  {
    $this->resellerModel = $resellerModel;
    $this->request = $request;
  }

  public function getResellers()
  {
    return $this->resellerModel->all();
  }

  public function getResellersOptions()
  {
    $resellers = $this->getResellers();
    foreach( $resellers as $reseller)
    {
      $data[$reseller->id] = $reseller->name;
    }
    return $data;
  }  

}
