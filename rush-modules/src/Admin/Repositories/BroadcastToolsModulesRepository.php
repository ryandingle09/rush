<?php
namespace Rush\Modules\Admin\Repositories;

use Rush\Modules\Admin\Models\BroadcastToolsModulesModel;
use Rush\Modules\Repository\AbstractRepository;

class BroadcastToolsModulesRepository extends AbstractRepository implements BroadcastToolsModulesInterface
{

  public function __construct(BroadcastToolsModulesModel $broadcastToolsModulesModel)
  {
    parent::__construct($broadcastToolsModulesModel);
  }

  public function getBroadcastToolsModules()
  {
    return $this->all();
  }
}
