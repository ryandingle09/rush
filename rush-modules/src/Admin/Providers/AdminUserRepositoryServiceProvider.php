<?php
namespace Rush\Modules\Admin\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Admin\Repositories\AdminUserInterface;
use Rush\Modules\Admin\Repositories\AdminUserRepository;

class AdminUserRepositoryServiceProvider extends  ServiceProvider
{
  public function register()
  {
    $this->app->singleton(AdminUserInterface::class, AdminUserRepository::class);
  }
}
