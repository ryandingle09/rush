<?php
namespace Rush\Modules\Admin\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Admin\Repositories\AdminRewardsInterface;
use Rush\Modules\Admin\Repositories\AdminRewardsRepository;

class AdminRewardsRepositoryServiceProvider extends  ServiceProvider
{
  public function register()
  {
    $this->app->singleton(AdminRewardsInterface::class, AdminRewardsRepository::class);
  }
}
