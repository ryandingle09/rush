<?php
namespace Rush\Modules\Admin\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Admin\Repositories\BroadcastToolsManagementInterface;
use Rush\Modules\Admin\Repositories\BroadcastToolsManagementRepository;

class BroadcastToolsManagementRepositoryServiceProvider extends  ServiceProvider
{
  public function register()
  {
    $this->app->singleton(BroadcastToolsManagementInterface::class, BroadcastToolsManagementRepository::class);
  }
}
