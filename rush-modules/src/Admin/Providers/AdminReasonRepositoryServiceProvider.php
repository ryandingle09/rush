<?php
namespace Rush\Modules\Admin\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Admin\Repositories\AdminReasonInterface;
use Rush\Modules\Admin\Repositories\AdminReasonRepository;

class AdminReasonRepositoryServiceProvider extends  ServiceProvider
{
  public function register()
  {
    $this->app->singleton(AdminReasonInterface::class, AdminReasonRepository::class);
  }
}
