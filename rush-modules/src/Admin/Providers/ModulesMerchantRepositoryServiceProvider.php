<?php
namespace Rush\Modules\Admin\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Admin\Repositories\ModulesMerchantInterface;
use Rush\Modules\Admin\Repositories\ModulesMerchantRepository;

class ModulesMerchantRepositoryServiceProvider extends  ServiceProvider
{
  public function register()
  {
    $this->app->singleton(ModulesMerchantInterface::class, ModulesMerchantRepository::class);
  }
}
