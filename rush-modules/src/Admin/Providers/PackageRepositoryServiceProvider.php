<?php
namespace Rush\Modules\Admin\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Admin\Repositories\PackageInterface;
use Rush\Modules\Admin\Repositories\PackageRepository;

class PackageRepositoryServiceProvider extends  ServiceProvider
{
  public function register()
  {
    $this->app->singleton(PackageInterface::class, PackageRepository::class);
  }
}
