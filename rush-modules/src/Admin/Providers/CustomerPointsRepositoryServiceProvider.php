<?php
namespace Rush\Modules\Admin\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Admin\Repositories\CustomerPointsInterface;
use Rush\Modules\Admin\Repositories\CustomerPointsRepository;

class CustomerPointsRepositoryServiceProvider extends  ServiceProvider
{
  public function register()
  {
    $this->app->singleton(CustomerPointsInterface::class, CustomerPointsRepository::class);
  }
}
