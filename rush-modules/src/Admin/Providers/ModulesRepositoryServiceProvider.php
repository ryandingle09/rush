<?php
namespace Rush\Modules\Admin\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Admin\Repositories\ModulesInterface;
use Rush\Modules\Admin\Repositories\ModulesRepository;

class ModulesRepositoryServiceProvider extends  ServiceProvider
{
  public function register()
  {
    $this->app->singleton(ModulesInterface::class, ModulesRepository::class);
  }
}
