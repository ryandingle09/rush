<?php
namespace Rush\Modules\Admin\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Admin\Repositories\BusinessTypeInterface;
use Rush\Modules\Admin\Repositories\BusinessTypeRepository;

class BusinessTypeRepositoryServiceProvider extends  ServiceProvider
{
  public function register()
  {
    $this->app->singleton(BusinessTypeInterface::class, BusinessTypeRepository::class);
  }
}
