<?php
namespace Rush\Modules\Admin\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Admin\Repositories\BroadcastToolsModulesInterface;
use Rush\Modules\Admin\Repositories\BroadcastToolsModulesRepository;

class BroadcastToolsModulesRepositoryServiceProvider extends  ServiceProvider
{
  public function register()
  {
    $this->app->singleton(BroadcastToolsModulesInterface::class, BroadcastToolsModulesRepository::class);
  }
}
