<?php
namespace Rush\Modules\Admin\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Admin\Repositories\ProspectsInterface;
use Rush\Modules\Admin\Repositories\ProspectsRepository;

class ProspectsRepositoryServiceProvider extends  ServiceProvider
{
  public function register()
  {
    $this->app->singleton(ProspectsInterface::class, ProspectsRepository::class);
  }
}
