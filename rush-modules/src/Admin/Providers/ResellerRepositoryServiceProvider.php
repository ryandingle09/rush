<?php
namespace Rush\Modules\Admin\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Admin\Repositories\ResellerInterface;
use Rush\Modules\Admin\Repositories\ResellerRepository;

class ResellerRepositoryServiceProvider extends  ServiceProvider
{
  public function register()
  {
    $this->app->singleton(ResellerInterface::class, ResellerRepository::class);
  }
}
