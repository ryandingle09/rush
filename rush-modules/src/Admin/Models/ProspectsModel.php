<?php
namespace Rush\Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class ProspectsModel extends Model
{
  protected $table = "prospects";
  protected $primaryKey = "id";

  public function bd_account()
  {
    return $this->belongsTo('Rush\Modules\Admin\Models\BdaccountModel', 'bd_assigned', 'id');
  }
}
