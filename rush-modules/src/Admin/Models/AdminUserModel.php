<?php
namespace Rush\Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class AdminUserModel extends Model
{
  protected $table = "Admin";
  protected $primaryKey = "id";
}
