<?php

namespace Rush\Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;
use Rush\Modules\Helpers\Facades\StorageHelper;

class AdminRewardsModel extends Model
{
    protected $table = "admin_rewards";
    protected $primaryKey = "id";

    public function getTypeToTextAttribute() {
	if ( $this->type ) {
	   $string = '';
           $types = json_decode( $this->type );
           $i = 0;
           foreach( $types as $type )
           {
            $string .= $type;
            $i++;
            if ( count($types) > $i ) $string .= ", ";
           }
           return $string;
	} else {
           return false;
        }
    }

    public function getStatusToTextAttribute() {
	if ( $this->status )
	{
		return "enabled";
	}
	return "disabled";
    }

    public function getImageURLAttribute($value)
    {
        return StorageHelper::repositoryUrlForView($value);
    }
}
