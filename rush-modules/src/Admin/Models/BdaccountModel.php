<?php
namespace Rush\Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class BdaccountModel extends Model
{
  protected $table = "bd_accounts";
  protected $primaryKey = "id";
}
