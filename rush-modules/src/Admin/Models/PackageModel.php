<?php
namespace Rush\Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class PackageModel extends Model
{
  protected $table = "Package";
  protected $primaryKey = "packageId";
}
