<?php
namespace Rush\Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class BdAdminModel extends Model
{
  protected $table = "bd_admin_modules";
  protected $primaryKey = "id";
}
