<?php
namespace Rush\Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessTypeModel extends Model
{
  protected $table = "BusinessType";
  protected $primaryKey = "id";
}
