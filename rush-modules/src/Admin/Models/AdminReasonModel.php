<?php
namespace Rush\Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;
use Rush\Modules\Admin\Models\AdminUserModel;

class AdminReasonModel extends Model
{
  protected $table = "admin_enable_disable_reason";
  protected $primaryKey = "id";

  public function admin()
  {
    return $this->hasOne('Rush\Modules\Admin\Models\AdminUserModel', 'id', 'created_by' );
  }

  public function getEnabledTextAttribute()
  {
    if ( $this->enabled == 1 ) return "Enabled";
    else return "Disabled";
  }
}
