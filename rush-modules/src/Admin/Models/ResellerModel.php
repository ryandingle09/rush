<?php
namespace Rush\Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class ResellerModel extends Model
{
  protected $table = "reseller";
  protected $primaryKey = "id";
}
