<?php
namespace Rush\Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class AdminModulesModel extends Model
{
  protected $table = "admin_modules";
  protected $primaryKey = "id";
}
