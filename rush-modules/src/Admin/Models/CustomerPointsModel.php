<?php
namespace Rush\Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerPointsModel extends Model
{
  protected $table = "customerPoints";
  protected $primaryKey = "customerpointsId";

  public function customer()
  {
    return $this->belongsTo('Rush\Modules\Customer\Models\CustomerModel', 'customerId', 'customerId');
  }
}
