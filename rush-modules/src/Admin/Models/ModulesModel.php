<?php
namespace Rush\Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class ModulesModel extends Model
{
  protected $table = "modules";
  protected $primaryKey = "id";
}
