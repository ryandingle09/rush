<?php
namespace Rush\Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class BroadcastToolsModulesModel extends Model
{
  protected $table = "broadcast_tools_modules";
  protected $primaryKey = "id";
}
