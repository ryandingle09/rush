<?php

namespace Rush\Modules\Sms\Models;

use Eloquent;

class SmsLogModel extends Eloquent
{
    protected $table = "notification_sms_logs";
    protected $primaryKey = "id";
    protected $fillable = array(
        'merchant_id',
        'message',
        'mobile_number',
        'response',
        'transaction_type'
    );
}