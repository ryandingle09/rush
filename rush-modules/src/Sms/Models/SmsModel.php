<?php

namespace Rush\Modules\Sms\Models;

use Rush\Modules\Broadcasting\Models\MessageModel;

class SmsModel extends MessageModel
{
    protected $table = "sms";
}