<?php

namespace Rush\Modules\Sms\Models;

use Illuminate\Database\Eloquent\Model;

class SmsCredentialModel extends Model
{
    protected $table  = 'notification_sms_credentials';
    public $timestamps = false;

    protected $guarded = ['id'];
}