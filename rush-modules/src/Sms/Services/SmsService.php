<?php

namespace Rush\Modules\Sms\Services;

use Rush\Modules\Broadcasting\BroadcastHelper;
use Rush\Modules\Broadcasting\Jobs\SmsBroadcastJob;
use Rush\Modules\Sms\Models\SmsModel AS Sms;
use Rush\Modules\Sms\Repositories\SmsInterface;
use Rush\Modules\Customer\Repositories\CustomerLoyaltySmsRepository;

/**
* Our SmsService, containing all useful methods for business logic around Sms
* Service just hands off data it was given by the controller to the appropriate repositories
*/
class SmsService
{
    /**
     * SmsRepository
     * @var Rush\Modules\Sms\Repositories\SmsRepository
     */
    protected $smsRepository;

    public function __construct(SmsInterface $smsRepository)
    {
        $this->smsRepository = $smsRepository;
    }

    public function all()
    {
        return $this->smsRepository->all();
    }

    public function store(array $input)
    {
        $smsInput = $input['sms'];
        $smsInput['status'] = BroadcastHelper::INACTIVE;
        $smsInput['publish_date'] = date("Y-m-d H:i:s", strtotime("{$smsInput['smsPublishDate']} {$smsInput['smsPublishTime']}"));
        $smsInput['segment_id'] = $smsInput['targetSegment'];

        $validator = $this->validate(
            $smsInput,
            [   'status' => 'required',
                'smsPublishDate' => 'date_format:Y-m-d',
                'smsPublishTime' => 'date_format:h:i A',
                'publish_date' => 'required|date_format:Y-m-d H:i:s',
                'message' => 'required',
                'merchant_id' => 'required'],
            [   'required' => 'The :attribute is required.',
                'smsPublishDate.date_format' => 'Select date.',
                'smsPublishTime.date_format' => 'Select time.']
        );
        if ($validator->fails()) {
            return $validator;
        }
        $this->smsRepository->create($smsInput);

        return true;
    }

    public function delete(array $input)
    {
        $rules = array(
            'id' => 'required|exists:sms,id,status,'.BroadcastHelper::INACTIVE
        );
        $validator = \Validator::make($input, $rules);
        if ($validator->fails()) {
            return false;
        }

        $this->smsRepository->delete($input['id']);

        return true;
    }

    public function check()
    {
        $schedules = $this->smsRepository->getScheduledItems();

        foreach ($schedules as $schedule) {
            dispatch(new SmsBroadcastJob($schedule));
            $schedule->status = BroadcastHelper::ACTIVE;
            $schedule->save();
        }
    }

    private function validate($input, $rules, $messages = [])
    {
        return \Validator::make($input, $rules, $messages);
    }

    public function getSmsSentAll($merchantId)
    {
        return $this->smsRepository->getSmsSentAll($merchantId);
    }

    public function getSmsSentThisMonth($merchantId)
    {
        return $this->smsRepository->getSmsSentThisMonth($merchantId);
    }

    public function allWithSegment($merchantId)
    {
        return $this->smsRepository->allWithSegment($merchantId);
    }

    public function getBySegmentId($segment_id){
        return $this->smsRepository->getBySegmentId($segment_id);
    }

    public function earn_non_member_points( $transaction ){

        $customerLoyaltySmsRepository = new CustomerLoyaltySmsRepository();
        $customerLoyaltySmsRepository->earnNonMemberPoints( $transaction );

    }

    public function registration( $customer ){

        $customer->sms->registration();

    }

    public function earnPoints( $transaction, $amount ){

        $customer = $transaction->customer;
        $customer->sms->earnPoints( $transaction, $amount );

    }

    public function payPoints( $transaction ){

        $customer = $transaction->customer;
        $customer->sms->payPoints( $transaction );

    }

    public function redeemPoints( $transaction ){

        $customer = $transaction->customer;
        $customer->sms->redeemPoints( $transaction );

    }

    public function claimReward( $transaction ){

        $customer = $transaction->customer;
        $customer->sms->claimReward( $transaction );  // uses LoyaltySms.

    }

    public function claimPunchcardReward( $transaction ){

        $customer = $transaction->customer;
        $customer->sms->claimReward( $transaction );  // uses PunchcardSms.

    }

    public function voidEarn( $transaction ){

        $customerLoyaltySmsRepository = new CustomerLoyaltySmsRepository();
        $customerLoyaltySmsRepository->voidEarn( $transaction );

    }

    public function voidRedeem( $transaction ){

        $customer = $transaction->customer;
        $customer->sms->voidRedeem( $transaction );

    }

    public function voidPaypoints( $transaction ){

        $customer = $transaction->customer;
        $customer->sms->voidPaypoints( $transaction );

    }

    public function invite( $merchant, $customer, $mobile_numbers, $emails ){

        $customer->sms->invite( $mobile_numbers );

    }

    public function transferPoints( $transaction, $receiver ){

        $customer = $transaction->customer;
        $customer->sms->transferPoints( $transaction, $receiver );

    }

    public function transferPointsReceiverSms( $transaction, $receiver ){

        $customer = $transaction->customer;
        $customer->sms->transferPointsReceiverSms( $transaction, $receiver );

    }

    public function redeemAmaxReward( $transaction, $promo ){

        $customerLoyaltySmsRepository = new CustomerLoyaltySmsRepository();
        return $customerLoyaltySmsRepository->redeemAmaxReward( $transaction, $promo );

    }

    public function redeemAmaxRewardFailed( $customer ){

        $customerLoyaltySmsRepository = new CustomerLoyaltySmsRepository();
        return $customerLoyaltySmsRepository->redeemAmaxRewardFailed( $customer );

    }

    public function voidPackage( $transaction ){

        $customerLoyaltySmsRepository = new CustomerLoyaltySmsRepository();
        $customerLoyaltySmsRepository->voidPackage( $transaction );

    }

    public function send_notification( $merchant, $customer, $flag, $data = [] ){

        $sms = sms()->instance($customer);
        $sms->sendNotification($flag, $data);

    }

}
