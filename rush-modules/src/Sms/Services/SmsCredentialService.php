<?php

namespace Rush\Modules\Sms\Services;

use Rush\Modules\Sms\Repositories\SmsCredentialInterface;

/**
* Our SmsCredentialService, containing all useful methods for business logic around SmsCredential
* Service just hands off data it was given by the controller to the appropriate repositories
*/
class SmsCredentialService
{
    /**
     * SmsCredentialRepository
     * @var Rush\Modules\Sms\Repositories\SmsCredentialRepository
     */
    protected $smsCredentialRepository;

    public function __construct(SmsCredentialInterface $smsCredentialRepository)
    {
        $this->smsCredentialRepository = $smsCredentialRepository;
    }

    public function getByMerchantId($merchantId)
    {
        $credential = $this->smsCredentialRepository->getByMerchantId($merchantId);
        if (!$credential) {
            $credential = $this->store([
                'merchant_id' => $merchantId,
                'shortcode' => '21585620',
                'app_id' => 'Bd6LInMXAzhMoc4BeRTXMKhB5d5LIKbk',
                'app_secret' => 'da571a2bb8c59e785195b128444dedea8357ce84f9382be374c4b6caf082d6eb',
                'passphrase' => 'hlFGlxfntT'
            ]);
        }

        return $credential;
    }

    public function store(array $input)
    {
        return $this->smsCredentialRepository->create($input);
    }

    public function update(array $input)
    {
        $input = $input['credential'];
        $credential = $this->getByMerchantId($input['merchant_id']);
        $this->smsCredentialRepository->update($credential->id, $input);

        return true;
    }
}
