<?php

namespace Rush\Modules\Sms\Services;

use Rush\Modules\Broadcasting\BroadcastHelper;
use Rush\Modules\Broadcasting\Contracts\BroadcasterInterface;
use Rush\Modules\Broadcasting\Models\MessageModel;
use Rush\Modules\Broadcasting\Services\RecipientService;
use Rush\Modules\Helpers\JobHelper;
use Rush\Modules\Sms\Library\Sms;
use Rush\Modules\Sms\Models\SmsModel;
use Rush\Modules\Sms\Repositories\SmsLogInterface;
use Rush\Modules\Sms\Services\SmsCredentialService;

class SmsBroadcasterService implements BroadcasterInterface
{
    /**
     * @var Rush\Modules\Sms\Repositories\SmsLogRepository
     */
    protected $smsLogRepository;

    /**
     * @var Rush\Modules\Broadcasting\Services\RecipientService
     */
    protected $recipientService;

    public function __construct(
        SmsLogInterface $smsLogRepository,
        RecipientService $recipientService,
        SmsCredentialService $smsCredentialService
    ) {
        $this->smsLogRepository = $smsLogRepository;
        $this->recipientService = $recipientService;
        $this->smsCredentialService = $smsCredentialService;
    }

    /**
     * @param  Rush\Modules\Sms\Models\SmsModel $message
     * @return none
     */
    public function send(MessageModel $message)
    {
        try {
            if (!($message instanceof SmsModel)) {
                throw new \InvalidArgumentException("$message must be an instance of SmsModel");
            }
            JobHelper::start($message);

            $transactionType = 'Sms Broadcast';
            $customers = $this->recipientService->getCustomers($message->merchant_id, $message->segment_id);
            $sms = new Sms($message->merchant_id, $this->smsCredentialService);

            // send sms to each
            $sentCount = 0;
            foreach($customers AS $customer) {
                $result = $sms->send($message->message, $customer->mobileNumber, $transactionType);

                // log the Sms send transaction
                $this->smsLogRepository->create([
                    'merchant_id' => $message->merchant_id,
                    'message' => $message->message,
                    'mobile_number' => $customer->mobileNumber,
                    'response' => $result,
                    'transaction_type' => $transactionType
                ]);

                $sentCount++;
            }

            $message->status = BroadcastHelper::PROCESSED;
            $message->sent_count = $sentCount;
            $message->save();
        } catch(\Exception $e) {
            JobHelper::reactivate($message, $e);
            throw $e;
        }
    }
}
