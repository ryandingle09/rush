<?php

namespace Rush\Modules\Sms\Library;

use Ixudra\Curl\CurlService;
use Rush\Modules\Sms\Services\SmsCredentialService;

class Sms
{
    public $credential;
    public $endpoint = "https://devapi.globelabs.com.ph";

    /**
     * @var Rush\Modules\Sms\Services\SmsCredentialService
     */
    protected $smsCredentialService;

    public function __construct($merchant_id, SmsCredentialService $smsCredentialService)
    {
        $this->smsCredentialService = $smsCredentialService;
        $this->credential = $this->smsCredentialService->getByMerchantId($merchant_id);
    }

    public function send($message, $mobile_number, $transaction_type = null)
    {
        if ($this->credential && $this->credential) {
            if (empty($this->credential->shortcode)
                || empty($this->credential->app_id)
                || empty($this->credential->app_secret)
                || empty($this->credential->passphrase)
            ) {
                throw new \Exception('SmsCredential not set for this Merchant');
            }

            $curlService = new CurlService();
            $send = $curlService->to($this->sendEnpoint())
                ->withData([
                    'address' => $mobile_number,
                    'message' => $message,
                    'passphrase' => $this->credential->passphrase])
                ->post();

            return $send;
        } else {
            throw new \Exception('No SmsCredential found for this Merchant');
        }

        return false;
    }

    public function sendEnpoint()
    {
        return $this->endpoint . "/smsmessaging/v1/outbound/{$this->credential->shortcode}/requests?app_id={$this->credential->app_id}&app_secret={$this->credential->app_secret}";
    }

    public function buildMessage($message, $data)
    {
        foreach ($data as $key => $value) {
            $message = str_replace("%". $key ."%", $value, $message);
        }

        return $message;
    }
}
