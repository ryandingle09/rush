<?php

namespace Rush\Modules\Sms\Repositories;

use Rush\Modules\Sms\Models\SmsCredentialModel;
use Rush\Modules\Sms\Repositories\SmsCredentialInterface;
use Rush\Modules\Repository\AbstractRepository;

/**
 * A repository class would encapsulate each Eloquent Model class
 * and be responsible for CRUD operations on the database.
 */
class SmsCredentialRepository extends AbstractRepository implements SmsCredentialInterface
{
    public function __construct(SmsCredentialModel $smsCredentialModel)
    {
        parent::__construct($smsCredentialModel);
    }

    public function getByMerchantId($merchantId)
    {
        return $this->model->where('merchant_id', $merchantId)->first();
    }
}
