<?php

namespace Rush\Modules\Sms\Repositories;

use Rush\Modules\Broadcasting\Repositories\MessageInterface;

/**
 * The SmsInterface contains unique method signatures related to Sms object
 */
interface SmsInterface extends MessageInterface
{
    public function getSmsSentAll($merchantId);

    public function getSmsSentThisMonth($merchantId);
}
