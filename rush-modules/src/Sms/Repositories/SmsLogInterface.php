<?php

namespace Rush\Modules\Sms\Repositories;

use Rush\Modules\Repository\RepositoryInterface;

/**
 * The SmsLogInterface contains unique method signatures related to SmsLog object
 */
interface SmsLogInterface extends RepositoryInterface
{
}
