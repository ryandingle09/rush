<?php

namespace Rush\Modules\Sms\Repositories;

use Rush\Modules\Repository\RepositoryInterface;

/**
 * The SmsCredentialInterface contains unique method signatures related to SmsCredential object
 */
interface SmsCredentialInterface extends RepositoryInterface
{
}
