<?php

namespace Rush\Modules\Sms\Repositories;

use Rush\Modules\Broadcasting\BroadcastHelper;
use Rush\Modules\Broadcasting\Repositories\MessageRepository;
use Rush\Modules\Sms\Models\SmsModel;
use Rush\Modules\Sms\Repositories\SmsInterface;

/**
 * A repository class would encapsulate each Eloquent Model class
 * and be responsible for CRUD operations on the database.
 */
class SmsRepository extends MessageRepository implements SmsInterface
{
    public function __construct(SmsModel $smsModel)
    {
        parent::__construct($smsModel);
    }

    public function getSmsSentAll($merchantId)
    {
        return $this->model
            ->where('merchant_id', $merchantId)
            ->where('status', BroadcastHelper::PROCESSED)
            ->sum('sent_count');
    }

    public function getSmsSentThisMonth($merchantId)
    {
        return $this->model
            ->where('merchant_id', $merchantId)
            ->where('status', BroadcastHelper::PROCESSED)
            ->whereYear('publish_date', '=', date('Y'))
            ->whereMonth('publish_date', '=', date('m'))
            ->sum('sent_count');
    }
}
