<?php

namespace Rush\Modules\Sms\Repositories;

use Rush\Modules\Repository\AbstractRepository;
use Rush\Modules\Sms\Models\SmsLogModel;
use Rush\Modules\Sms\Repositories\SmsLogInterface;

/**
 * A repository class would encapsulate each Eloquent Model class
 * and be responsible for CRUD operations on the database.
 */
class SmsLogRepository extends AbstractRepository implements SmsLogInterface
{
    public function __construct(SmsLogModel $smsLogModel)
    {
        parent::__construct($smsLogModel);
    }
}
