<?php

namespace Rush\Modules\Sms\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Sms\Models\SmsLogModel;
use Rush\Modules\Sms\Repositories\SmsLogRepository;

/**
* Register our Repository with Laravel
*/
class SmsLogRepositoryServiceProvider extends ServiceProvider
{
    /**
    * Registers the SmsLogInterface with Laravels IoC Container
    */
    public function register()
    {
        $this->app->bind(
            '\Rush\Modules\Sms\Repositories\SmsLogInterface',
            function($app) {
                return new SmsLogRepository(new SmsLogModel());
            }
        );
    }
}
