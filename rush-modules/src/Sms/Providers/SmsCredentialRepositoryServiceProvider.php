<?php

namespace Rush\Modules\Sms\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Sms\Models\SmsCredentialModel;
use Rush\Modules\Sms\Repositories\SmsCredentialRepository;

/**
* Register our Repository with Laravel
*/
class SmsCredentialRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // register package views
    }

    /**
    * Registers the SmsCredentialInterface with Laravels IoC Container
    */
    public function register()
    {
        // Bind the returned class to the namespace 'Repositories\SmsCredentialInterface
        $this->app->bind(
            '\Rush\Modules\Sms\Repositories\SmsCredentialInterface',
            function($app) {
                return new SmsCredentialRepository(new SmsCredentialModel());
            }
        );
    }
}
