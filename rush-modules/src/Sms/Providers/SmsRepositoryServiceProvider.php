<?php

namespace Rush\Modules\Sms\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Sms\Models\SmsModel;
use Rush\Modules\Sms\Repositories\SmsRepository;

/**
* Register our Repository with Laravel
*/
class SmsRepositoryServiceProvider extends ServiceProvider
{
    /**
    * Registers the SmsInterface with Laravels IoC Container
    */
    public function register()
    {
        // Bind the returned class to the namespace 'Repositories\SmsInterface
        $this->app->bind(
            '\Rush\Modules\Sms\Repositories\SmsInterface',
            function($app) {
                return new SmsRepository(new SmsModel());
            }
        );
    }
}
