<?php

namespace Rush\Modules\Push;

final class PushHelper
{
    const PLATFORM_IOS = 'ios';
    const PLATFORM_ANDROID = 'android';
    const PLATFORM_UNKNOWN = 'unknown';
    const PLATFORM_BOTH = 'both';

    public static function getPlatform($tag)
    {
        $androidPlatformTags = self::getAdroidPlatformTags();
        $iosPlatformTags = self::getIosPlatformTags();

        if (in_array($tag, $androidPlatformTags))
            return self::PLATFORM_ANDROID;
        if (in_array($tag, $iosPlatformTags))
            return self::PLATFORM_IOS;
        return self::PLATFORM_UNKNOWN;
    }

    public static function getPlatforms()
    {
        return [self::PLATFORM_IOS, self::PLATFORM_ANDROID];
    }

    private function __construct()
    {
        throw new Exception("PushHelper not instantiable");
    }

    public static function getAdroidPlatformTags()
    {
        return explode('|', env('BROADCASTING_ANDROID_PLATFORMS'));
    }

    public static function getIosPlatformTags()
    {
        return explode('|', env('BROADCASTING_IOS_PLATFORMS'));
    }

    public static function getPlatformTags()
    {
        return [
            self::PLATFORM_IOS => self::getIosPlatformTags(),
            self::PLATFORM_ANDROID => self::getAdroidPlatformTags()
        ];
    }
}