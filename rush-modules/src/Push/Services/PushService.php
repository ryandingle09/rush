<?php

namespace Rush\Modules\Push\Services;

use Rush\Modules\Broadcasting\BroadcastHelper;
use Rush\Modules\Broadcasting\Jobs\PushBroadcastJob;
use Rush\Modules\Push\Models\PushModel AS Push;
use Rush\Modules\Push\Repositories\PushInterface;

/**
* Our PushService, containing all useful methods for business logic around Push
* Service just hands off data it was given by the controller to the appropriate repositories
*/
class PushService
{
    /**
     * PushRepository
     * @var Rush\Modules\Push\Repositories\PushRepository
     */
    protected $pushRepository;

    public function __construct(PushInterface $pushRepository)
    {
        $this->pushRepository = $pushRepository;
    }

    public function all()
    {
        return $this->pushRepository->all();
    }

    public function store(array $input)
    {
        $pushInput = $input['push'];
        $pushInput['status'] = BroadcastHelper::INACTIVE;
        $pushInput['publish_date'] = date("Y-m-d H:i:s", strtotime("{$pushInput['notifRunDate']} {$pushInput['notifRunTime']}"));
        $pushInput['message'] = $pushInput['notifMessage'];
        $pushInput['campaign'] = $pushInput['notifCampaignName'];
        $pushInput['description'] = $pushInput['notifCampaignDesc'];
        $pushInput['device'] = isset($pushInput['notifDeviceType']) ? $pushInput['notifDeviceType'] : null;
        $pushInput['segment_id'] = $pushInput['targetSegment'];

        $validator = $this->validate(
            $pushInput,
            [   'status' => 'required',
                'notifRunDate' => 'date_format:Y-m-d',
                'notifRunTime' => 'date_format:h:i A',
                'publish_date' => 'required',
                'campaign' => 'required|unique:push,campaign,NULL,id,merchant_id,'.$pushInput['merchant_id'],
                'message' => 'required',
                'device' => 'required',
                'merchant_id' => 'required'],
            [   'required' => 'The :attribute is required.',
                'unique' => 'The :attribute has already been registered.',
                'notifRunDate.date_format' => 'Select date.',
                'notifRunTime.date_format' => 'Select time.']
        );
        if ($validator->fails()) {
            return $validator;
        }
        $this->pushRepository->create($pushInput);

        return true;
    }

    public function delete(array $input)
    {
        $rules = array(
            'id' => 'required|exists:push,id,status,'.BroadcastHelper::INACTIVE
        );
        $validator = \Validator::make($input, $rules);
        if ($validator->fails()) {
            return false;
        }

        $this->pushRepository->delete($input['id']);

        return true;
    }

    public function check()
    {
        $schedules = $this->pushRepository->getScheduledItems();

        foreach ($schedules as $schedule) {
            dispatch(new PushBroadcastJob($schedule));
            $schedule->status = BroadcastHelper::ACTIVE;
            $schedule->save();
        }
    }

    private function validate($input, $rules, $messages = [])
    {
        return \Validator::make($input, $rules, $messages);
    }

    public function allWithSegment($merchantId)
    {
        return $this->pushRepository->allWithSegment($merchantId);
    }

    public function getBySegmentId($segment_id){
        return $this->pushRepository->getBySegmentId($segment_id);
    }
}
