<?php

namespace Rush\Modules\Push\Services;

use Rush\Modules\Broadcasting\BroadcastHelper;
use Rush\Modules\Broadcasting\Contracts\BroadcasterInterface;
use Rush\Modules\Broadcasting\Models\MessageModel;
use Rush\Modules\Broadcasting\Services\RecipientService;
use Rush\Modules\Helpers\JobHelper;
use Rush\Modules\Push\Library\Push;
use Rush\Modules\Push\Models\PushModel;
use Rush\Modules\Push\PushHelper;
use Rush\Modules\Push\Repositories\PushLogInterface;
use Rush\Modules\Push\Services\PushCredentialService;

class PushBroadcasterService implements BroadcasterInterface
{
    /**
     * @var Rush\Modules\Push\Repositories\PushLogRepository
     */
    protected $pushLogRepository;

    /**
     * @var Rush\Modules\Broadcasting\Services\RecipientService
     */
    protected $recipientService;

    public function __construct(
        PushLogInterface $pushLogRepository,
        RecipientService $recipientService,
        PushCredentialService $pushCredentialService
    ) {
        $this->pushLogRepository = $pushLogRepository;
        $this->recipientService = $recipientService;
        $this->pushCredentialService = $pushCredentialService;
    }

    /**
     * @param  MessageModel $message
     */
    public function send(MessageModel $message)
    {
        try {
            if (!($message instanceof PushModel)) {
                throw new \InvalidArgumentException("$message must be an instance of PushModel");
            }
            JobHelper::start($message);

            $transactionType = 'Push Broadcast';
            $pushCredential = $this->pushCredentialService->getByMerchantId($message->merchant_id);
            if ($pushCredential) {
                $platformDevices = $this->recipientService->getCustomerDevices($message->merchant_id, $message->segment_id);

                $send_result = [
                    'success' => [],
                    'failed' => []
                ];
                foreach($platformDevices AS $platform => $devices) {
                    $platform_send_result = [];
                    if ($platform == PushHelper::PLATFORM_ANDROID
                        && ($message->device == PushHelper::PLATFORM_ANDROID || $message->device == PushHelper::PLATFORM_BOTH)
                    ) {
                        $platform_send_result = Push::sendAndroid($pushCredential, $devices, $message->message);
                    }
                    if ($platform == PushHelper::PLATFORM_IOS
                        && ($message->device == PushHelper::PLATFORM_IOS || $message->device == PushHelper::PLATFORM_BOTH)
                    ) {
                        $platform_send_result = Push::sendIos($pushCredential, $devices, $message->message);
                    }
                    $send_result = array_merge_recursive($send_result, $platform_send_result);
                }

                $this->pushLogRepository->createBatch(
                    [   'merchant_id' => $message->merchant_id,
                        'transaction_type' => $transactionType,
                        'devices' => $send_result,
                        'push_id' => $message->id
                    ]
                );

                // tag item as processed
                $message->status = BroadcastHelper::PROCESSED;
                $message->sent_count = count($send_result['success']);
                $message->failed_count = count($send_result['failed']);
                $message->save();
            } else {
                throw new \Exception('No PushCredential found for this Merchant');
            }
        } catch(\Exception $e) {
            JobHelper::reactivate($message, $e);
            throw $e;
        }
    }
}
