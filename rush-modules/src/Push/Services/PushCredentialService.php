<?php

namespace Rush\Modules\Push\Services;

use Rush\Modules\Push\Repositories\PushCredentialInterface;

/**
* Our PushCredentialService, containing all useful methods for business logic around PushCredential
* Service just hands off data it was given by the controller to the appropriate repositories
*/
class PushCredentialService
{
    /**
     * PushCredentialRepository
     * @var Rush\Modules\Push\Repositories\PushCredentialRepository
     */
    protected $pushCredentialRepository;

    public function __construct(PushCredentialInterface $pushCredentialRepository)
    {
        $this->pushCredentialRepository = $pushCredentialRepository;
    }

    public function getByMerchantId($merchantId)
    {
        $credential = $this->pushCredentialRepository->getByMerchantId($merchantId);
        if (!$credential) {
            $credential = $this->store(['merchant_id' => $merchantId]);
        }

        return $credential;
    }

    public function store(array $input)
    {
        return $this->pushCredentialRepository->create($input);
    }

    public function update(array $input)
    {
        $input = $input['credential'];
        $credential = $this->getByMerchantId($input['merchant_id']);
        $this->pushCredentialRepository->update($credential->id, $input);

        return true;
    }
}
