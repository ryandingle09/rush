<?php

namespace Rush\Modules\Push\Library;
use Exception;

class Push
{
    public static function validateUUID($id) {
        $parts = explode("-", $id);
        if (count($parts) == 5) {
            return (
                strlen($parts[0]) == 8 &&
                strlen($parts[1]) == 4 &&
                strlen($parts[2]) == 4 &&
                strlen($parts[3]) == 4 &&
                strlen($parts[4]) == 12
            );
        }

        return false;
    }

    public static function getBaseSendResult()
    {
        return [
            'success' => [],
            'failed' => []
        ];
    }

    /**
     * @param  Object $credential
     * @param  array  $tokens
     * @param  string $message
     * @return mixed
     */
    public static function sendAndroid($credential, $devices, $message)
    {
        $send_result = self::getBaseSendResult();
        $success_tokens = 0;
        $failed_tokens = 0;

        $url = env('BROADCASTING_ANDROID_PUSH_API_FCM');
        $server_key = $credential->android_key;

        $headers = array(
            'Authorization: key=' . $server_key,
            'Content-Type: application/json'
        );

        $payload = [
            //'delay_while_idle' => true,
            'data' => [
                'message' => $message,
            ]
        ];

        $batch = array_chunk($devices, 1000);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        foreach ($batch as $devices) {
            $payload['registration_ids'] = array_column($devices, 'token');

            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
            $result = curl_exec($ch);
            $result = json_decode($result);
            $sentCount = $result ? $result->success : 0;
            $failCount = $result ? $result->failure : count($devices);

            //MTODO: when FCM supports failed_registration_ids key
            // use this to specifically identify failed device_ids
            // OR push the messages per device_id if we can avoid FCM restrictions|limits
            // for now just tag success/failure based on order
            $deviceIds = array_column($devices, 'device_id');
            $ids = array_column($devices, 'id');

            foreach($deviceIds AS $index => $deviceId) {
                if ($sentCount) {
                    $send_result['success'][] = $deviceId ?: $ids[$index];
                    $sentCount--;
                    continue;
                }
                if ($failCount) {
                    $send_result['failed'][] = $deviceId ?: $ids[$index];
                    $sentCount--;
                    continue;
                }
            }

        }
        curl_close($ch);

        return $send_result;
    }

    public static function sendIos($credential, $devices, $message, $mode = 'default')
    {
        $send_result = self::getBaseSendResult();
        $url = env('BROADCASTING_IOS_PUSH_API_PROD');
        $certPath = env('BROADCASTING_IOS_PUSH_CERT_PATH');

        $payload = [
            "aps" => [
                'alert' => $message,
                'sound' => 'default',
                'badge' => 1
            ],
        ];
        if (defined('CURL_HTTP_VERSION_2_0')) {
            $pem_file = base_path($certPath . $credential->ios_cert);
            $pem_secret = $credential->ios_secret;
            $apns_topic = $credential->ios_bundle;
            $expiration = time() + (90 * 24 * 60 * 60);
            $payload = json_encode($payload);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2_0);
            curl_setopt($ch, CURLOPT_SSLCERT, $pem_file);
            curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $pem_secret);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $batch = array_chunk($devices, 500);
            foreach ($batch as $devices) {
                foreach ($devices as $device) {
                    $send_status = false;
                    $device_id = trim($device['device_id']);
                    $device_token = trim($device['token']);
                    $error = null;

                    if (self::validateUUID($device_id)) {
                        curl_setopt($ch, CURLOPT_URL, $url . $device_token);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, [
                            "apns-topic: {$apns_topic}",
                            "apns-id: {$device_id}",
                            "apns-expiration: {$expiration}"
                        ]);

                        $response = curl_exec($ch);
                        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                        if ($httpcode != 200) {
                            if (env('APP_ENV', 'production') != 'production' || $mode == 'debug') {
                                $error = curl_error($ch);
                            }
                        } else {
                            $send_status = true;
                        }
                    }

                    if ($send_status) {
                        $send_result['success'][] = $device_id;
                    } else {
                        $send_result['failed'][] = $device_id;
                        if ($error) {
                            $send_result['debug'][] = $error;
                        }
                    }
                }
            }

            curl_close($ch);
        }

        return $send_result;
    }
}