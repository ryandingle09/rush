<?php

namespace Rush\Modules\Push\Repositories;

use Rush\Modules\Push\Models\PushLogModel;
use Rush\Modules\Push\Repositories\PushLogInterface;
use Rush\Modules\Repository\AbstractRepository;

/**
 * A repository class would encapsulate each Eloquent Model class
 * and be responsible for CRUD operations on the database.
 */
class PushLogRepository extends AbstractRepository implements PushLogInterface
{
    public function __construct(PushLogModel $pushLogModel)
    {
        parent::__construct($pushLogModel);
    }

    public function createBatch($data)
    {
        $logs = [];
        foreach($data['devices']['success'] AS $deviceId) {
            $logs[] = [
                'device_id' => $deviceId,
                'transaction_type' => $data['transaction_type'],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'merchant_id' => $data['merchant_id'],
                'push_id' => $data['push_id'],
                'send_status' => 1
            ];
        }
        foreach($data['devices']['failed'] AS $deviceId) {
            $logs[] = [
                'device_id' => $deviceId,
                'transaction_type' => $data['transaction_type'],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'merchant_id' => $data['merchant_id'],
                'push_id' => $data['push_id'],
                'send_status' => 0
            ];
        }

        // insert per 300 rows
        $batches = array_chunk($logs, 300);
        foreach ($batches as $batch) {
            $this->model->insert($batch);
        }
    }
}
