<?php

namespace Rush\Modules\Push\Repositories;

use Rush\Modules\Broadcasting\Repositories\MessageRepository;
use Rush\Modules\Push\Models\PushModel;
use Rush\Modules\Push\Repositories\PushInterface;

/**
 * A repository class would encapsulate each Eloquent Model class
 * and be responsible for CRUD operations on the database.
 */
class PushRepository extends MessageRepository implements PushInterface
{
    public function __construct(PushModel $pushModel)
    {
        parent::__construct($pushModel);
    }
}
