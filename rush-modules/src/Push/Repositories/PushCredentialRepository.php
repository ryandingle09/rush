<?php

namespace Rush\Modules\Push\Repositories;

use Rush\Modules\Push\Models\PushCredentialModel;
use Rush\Modules\Push\Repositories\PushCredentialInterface;
use Rush\Modules\Repository\AbstractRepository;

/**
 * A repository class would encapsulate each Eloquent Model class
 * and be responsible for CRUD operations on the database.
 */
class PushCredentialRepository extends AbstractRepository implements PushCredentialInterface
{
    public function __construct(PushCredentialModel $pushCredentialModel)
    {
        parent::__construct($pushCredentialModel);
    }

    public function getByMerchantId($merchantId)
    {
        return $this->model->where('merchant_id', $merchantId)->first();
    }
}
