<?php

namespace Rush\Modules\Push\Repositories;

use Rush\Modules\Repository\RepositoryInterface;

/**
 * The PushLogInterface contains unique method signatures related to PushLog object
 */
interface PushLogInterface extends RepositoryInterface
{
}
