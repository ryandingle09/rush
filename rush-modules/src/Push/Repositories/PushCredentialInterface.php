<?php

namespace Rush\Modules\Push\Repositories;

use Rush\Modules\Repository\RepositoryInterface;

/**
 * The PushCredentialInterface contains unique method signatures related to PushCredential object
 */
interface PushCredentialInterface extends RepositoryInterface
{
}
