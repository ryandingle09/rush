<?php

namespace Rush\Modules\Push\Repositories;

use Rush\Modules\Broadcasting\Repositories\MessageInterface;

/**
 * The PushInterface contains unique method signatures related to Push object
 */
interface PushInterface extends MessageInterface
{
}
