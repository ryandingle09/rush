<?php

namespace Rush\Modules\Push\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Push\Models\PushLogModel;
use Rush\Modules\Push\Repositories\PushLogRepository;

/**
* Register our Repository with Laravel
*/
class PushLogRepositoryServiceProvider extends ServiceProvider
{
    /**
    * Registers the PushLogInterface with Laravels IoC Container
    */
    public function register()
    {
        // Bind the returned class to the namespace 'Repositories\PushLogInterface
        $this->app->bind(
            '\Rush\Modules\Push\Repositories\PushLogInterface',
            function($app) {
                return new PushLogRepository(new PushLogModel());
            }
        );
    }
}
