<?php

namespace Rush\Modules\Push\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Push\Models\PushModel;
use Rush\Modules\Push\Repositories\PushRepository;

/**
* Register our Repository with Laravel
*/
class PushRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('PushHelper', 'Rush\Modules\Push\PushHelper');
    }

    /**
    * Registers the PushInterface with Laravels IoC Container
    */
    public function register()
    {
        // Bind the returned class to the namespace 'Repositories\PushInterface
        $this->app->bind(
            '\Rush\Modules\Push\Repositories\PushInterface',
            function($app) {
                return new PushRepository(new PushModel());
            }
        );
    }
}
