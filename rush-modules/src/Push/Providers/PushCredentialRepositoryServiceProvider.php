<?php

namespace Rush\Modules\Push\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Push\Models\PushCredentialModel;
use Rush\Modules\Push\Repositories\PushCredentialRepository;

/**
* Register our Repository with Laravel
*/
class PushCredentialRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // register package views
    }

    /**
    * Registers the PushCredentialInterface with Laravels IoC Container
    */
    public function register()
    {
        // Bind the returned class to the namespace 'Repositories\PushCredentialInterface
        $this->app->bind(
            '\Rush\Modules\Push\Repositories\PushCredentialInterface',
            function($app) {
                return new PushCredentialRepository(new PushCredentialModel());
            }
        );
    }
}
