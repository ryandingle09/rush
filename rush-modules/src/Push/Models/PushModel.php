<?php

namespace Rush\Modules\Push\Models;

use Rush\Modules\Broadcasting\Models\MessageModel;

class PushModel extends MessageModel
{
    protected $table = "push";

    protected $fillable = [
        'status',
        'publish_date',
        'message',
        'merchant_id',
        'campaign',
        'description',
        'device',
        'segment_id'
    ];
}