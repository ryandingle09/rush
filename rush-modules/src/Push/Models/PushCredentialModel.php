<?php

namespace Rush\Modules\Push\Models;

use Eloquent;

class PushCredentialModel extends Eloquent
{
    protected $table = "push_credential";

    protected $guarded = ['id'];
}