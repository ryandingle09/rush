<?php

namespace Rush\Modules\Push\Models;

use Eloquent;

class PushLogModel extends Eloquent
{
    protected $table = "push_logs";
    protected $primaryKey = "id";
    protected $fillable = array(
        'merchant_id',
        'push_id',
        'device_id',
        'transaction_type'
    );
}