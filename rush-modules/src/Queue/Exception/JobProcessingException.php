<?php

namespace Rush\Modules\Queue\Exception;

class JobProcessingException extends \Exception
{
}