<?php

namespace Rush\Modules\Queue\Contracts;

/**
 * The MerchantAwareJobInterface
 */
interface MerchantAwareJobInterface
{
    /**
     * Provide a mechanism within the Job object to return the merchant_id
     * @return [int] The merchant_id
     */
    public function getMerchantId();
}
