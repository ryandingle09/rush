<?php
namespace Rush\Modules\ClassManagement\Models;

use Illuminate\Database\Eloquent\Model;

class ClassManagementToolsModulesModel extends Model
{
  protected $table = "class_management_tool_modules";
}
