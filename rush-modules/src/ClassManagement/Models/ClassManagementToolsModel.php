<?php
namespace Rush\Modules\ClassManagement\Models;

use Illuminate\Database\Eloquent\Model;

class ClassManagementToolsModel extends Model
{
  protected $table = "class_management_tools";
}
