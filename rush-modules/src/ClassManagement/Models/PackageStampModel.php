<?php

namespace Rush\Modules\ClassManagement\Models;

use Eloquent;
use Ramsey\Uuid\Uuid;

/**
 * Class PackageStampModel
 *
 * @property-read 	int 												$id
 * @property 		string												$uuid
 * @property 		string												$details
 * @property 		string												$image_path
 * @property-read 	int 												$stamp_no
 * @property 		string 												$name
 * @property-read 	\Rush\Modules\ClassManagement\Models\PackageModel $package
 */
class PackageStampModel extends Eloquent
{
	protected $table = "class_package_stamps";

	protected $fillable = ['name', 'details', 'image_path', 'stamp_no'];

	public function package()
	{
		return $this->belongsTo(PackageModel::class, 'package_id', 'id');
	}

	public function getUuidAttribute($value)
	{
		if (!$value) {
			$name = 'class_package_stamps' . '-' . $this->id;
			$value = Uuid::uuid3(Uuid::NAMESPACE_DNS, $name);
			$this->uuid = $value;
			$this->save();
		}

		return $value;
	}
}