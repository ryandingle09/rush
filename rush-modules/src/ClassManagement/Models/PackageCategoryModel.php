<?php
namespace Rush\Modules\ClassManagement\Models;

use Eloquent;
use Logaretm\Transformers\TransformableTrait;
use Logaretm\Transformers\Contracts\Transformable;

class PackageCategoryModel extends Eloquent implements  Transformable
{
    use TransformableTrait;

    protected $table = 'class_package_categories';
    protected $fillable = ['class_package_category_list_id'];
}