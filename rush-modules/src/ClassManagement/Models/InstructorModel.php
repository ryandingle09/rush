<?php

namespace Rush\Modules\ClassManagement\Models;

use Eloquent;
use Logaretm\Transformers\TransformableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Logaretm\Transformers\Contracts\Transformable;

class InstructorModel extends Eloquent implements Transformable
{

	use SoftDeletes, TransformableTrait;

	protected $table = "class_instructors";

	public function classes()
    {
        return $this->belongsToMany('Rush\Modules\ClassManagement\Models\ClassModel', 'class_instructor_class', 'instructor_id', 'class_id');
    }

    public function branches()
    {
        return $this->belongsToMany('Rush\Modules\Merchant\Models\MerchantBranchesModel', 'class_instructor_branch', 'instructor_id', 'branch_id');
    }
}
