<?php

namespace Rush\Modules\ClassManagement\Models\Transformers;

use Logaretm\Transformers\Transformer;
use Rush\Modules\ClassManagement\Models\PackageModel;

class PackageTransformer extends Transformer
{
    /**
     * @param  PackageModel $package
     * @return mixed
     */
    public function getTransformation($package)
    {
        return [
            'id'  => $package->uuid,
            'name' => $package->name,
            'type' => $package->type,
            'image_url' => config('app.url') . $package->image_path,
            'validity' => $package->validity,
            'no_of_visits' => (int) $package->no_of_visits,
            'categories' => $package->category_names
        ];
    }
}
