<?php

namespace Rush\Modules\ClassManagement\Models\Transformers;

use Logaretm\Transformers\Transformer;
use Rush\Modules\ClassManagement\Models\ClassModel;

class ClassTransformer extends Transformer
{
    /**
     * @param  ClassModel $class
     * @return mixed
     */
    public function getTransformation($class)
    {
        return [
            'id'  => $class->id,
            'name' => $class->name,
            'image_url' => config('app.url') . $class->image_path
        ];
    }
}
