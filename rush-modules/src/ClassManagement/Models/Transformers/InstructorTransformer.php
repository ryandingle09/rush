<?php

namespace Rush\Modules\ClassManagement\Models\Transformers;

use Logaretm\Transformers\Transformer;
use Rush\Modules\ClassManagement\Models\InstructorModel;

class InstructorTransformer extends Transformer
{
    /**
     * @param  InstructorModel $instructor
     * @return mixed
     */
    public function getTransformation($instructor)
    {
        return [
            'id'  => $instructor->id,
            'name' => $instructor->name,
            'image_url' => config('app.url') . $instructor->image_path
        ];
    }
}
