<?php

namespace Rush\Modules\ClassManagement\Models;

use Eloquent;

class ClassScheduleReservationModel extends Eloquent
{
	protected $table = "class_schedule_reservations";

    protected $fillable = ['class_list_id', 'customer_id', 'registration_type', 'showed_up', 'customer_package_id'];

    const REG_TYPE_CMS = 0;
    const REG_TYPE_ONLINE = 1;

    public function customer()
    {
    	return $this->belongsTo('Rush\Modules\Customer\Models\CustomerModel', 'customer_id', 'customerId');
    }

    public function class_list()
    {
    	return $this->belongsTo('Rush\Modules\ClassManagement\Models\ClassScheduleTimeExtractModel', 'class_list_id');
    }

    public function customer_package()
    {
        return $this->belongsTo('Rush\Modules\Customer\Models\CustomerClassPackageModel', 'customer_package_id', 'id');
    }
}