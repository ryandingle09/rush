<?php

namespace Rush\Modules\ClassManagement\Models;

use Eloquent;
use Logaretm\Transformers\TransformableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Logaretm\Transformers\Contracts\Transformable;

class ClassModel extends Eloquent implements Transformable
{

	use SoftDeletes, TransformableTrait;

	protected $table = "class";

	public function instructors()
	{
		return $this->belongsToMany('Rush\Modules\ClassManagement\Models\InstructorModel', 'class_instructor_class', 'class_id', 'instructor_id');
	}

	public function branches()
    {
        return $this->belongsToMany('Rush\Modules\Merchant\Models\MerchantBranchesModel', 'class_branch', 'class_id', 'branch_id');
    }

    public function schedules(){
        return $this->hasMany('Rush\Modules\ClassManagement\Models\ClassScheduleModel', 'class_id', 'id');
    }
}
