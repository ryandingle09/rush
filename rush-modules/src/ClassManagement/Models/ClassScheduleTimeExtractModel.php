<?php

namespace Rush\Modules\ClassManagement\Models;

use Eloquent;

class ClassScheduleTimeExtractModel extends Eloquent
{
	protected $table = "class_schedule_time_extracts";
	
	protected $dates = [
		'start_date_time'
	];

	protected $fillable = ['schedule_id', 'start_date_time', 'end_date_time', 'instructor_id', 'instructor_status', 'status','start_time', 'end_time', 'attendance_capacity'];

	public function schedule()
    {
    	return $this->belongsTo('Rush\Modules\ClassManagement\Models\ClassScheduleModel', 'schedule_id', 'id');
    }

    public function instructor()
    {
    	return $this->belongsTo('Rush\Modules\ClassManagement\Models\InstructorModel', 'instructor_id', 'id');
    }

    public function reservations()
    {
    	return $this->hasMany('Rush\Modules\ClassManagement\Models\ClassScheduleReservationModel', 'class_list_id');
    }

    public function getStartTimeLabelAttribute(){
		$suffix = ($this->start_time > 1159) ? 'PM' : 'AM';
		$start_time = ($this->start_time > 1200) ? $this->start_time - 1200 : $this->start_time;
		$start_time = str_pad($start_time, 4, 0, STR_PAD_LEFT);

		return substr($start_time , 0, 2) . ':' . substr($start_time, 2, 2) . ' ' . $suffix;
	}

	public function getEndTimeLabelAttribute(){
		$suffix = ($this->end_time > 1159) ? 'PM' : 'AM';
		$end_time = ($this->end_time > 1200) ? $this->end_time - 1200 : $this->end_time;
		$end_time = str_pad($end_time, 4, 0, STR_PAD_LEFT);

		return substr($end_time , 0, 2) . ':' . substr($end_time, 2, 2) . ' ' . $suffix;
	}
}