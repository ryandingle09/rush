<?php

namespace Rush\Modules\ClassManagement\Models;

use Eloquent;

class ClassScheduleModel extends Eloquent
{
	protected $table = "class_schedules";

    protected $dates = [
        'start_date',
        'end_date'
    ];

    protected $fillable = ['start_date', 'end_date', 'start_time', 'end_time','is_repeat', 'repeats', 'repeat_every', 'repeat_selection', 'repeat_occurence', 'repeat_on', 'branch_id', 'attendance_capacity', 'status'];

	public function instructor()
	{
		return $this->belongsTo('Rush\Modules\ClassManagement\Models\InstructorModel', 'instructor_id');
	}

	public function classes()
	{
		return $this->belongsTo('Rush\Modules\ClassManagement\Models\ClassModel', 'class_id');
	}

	public function branch()
    {
        return $this->belongsTo('Rush\Modules\Merchant\Models\MerchantBranchesModel', 'branch_id');
    }

    public function scheduleTimeExtract()
    {
    	return $this->hasMany('Rush\Modules\ClassManagement\Models\ClassScheduleTimeExtractModel', 'schedule_id');
    }

    public function getStartTimeLabelAttribute(){
        $suffix = ($this->start_time > 1159) ? 'PM' : 'AM';
        $start_time = ($this->start_time > 1200) ? $this->start_time - 1200 : $this->start_time;
        $start_time = str_pad($start_time, 4, 0, STR_PAD_LEFT);

        return substr($start_time , 0, 2) . ':' . substr($start_time, 2, 2) . ' ' . $suffix;
    }

    public function getEndTimeLabelAttribute(){
        $suffix = ($this->end_time > 1159) ? 'PM' : 'AM';
        $end_time = ($this->end_time > 1200) ? $this->end_time - 1200 : $this->end_time;
        $end_time = str_pad($end_time, 4, 0, STR_PAD_LEFT);

        return substr($end_time , 0, 2) . ':' . substr($end_time, 2, 2) . ' ' . $suffix;
    }
}