<?php

namespace Rush\Modules\ClassManagement\Models;

use Eloquent;
use Ramsey\Uuid\Uuid;
use Logaretm\Transformers\TransformableTrait;
use Logaretm\Transformers\Contracts\Transformable;

/**
 * Class PackageModel
 *
 * @property        boolean                                                                                                 $multiple_stamp
 * @property-read   integer                                                                                                 $id
 * @property        integer                                                                                                 $no_of_visits
 * @property        string                                                                                                  $image_path
 * @property        string                                                                                                  $name
 * @property        string                                                                                                  $validity
 * @property        string                                                                                                  $type
 * @property        string                                                                                                  $uuid
 * @property-read   null|\Illuminate\Database\Eloquent\Collection|\Rush\Modules\ClassManagement\Models\PackageStampModel[]  $stamps
 */
class PackageModel extends Eloquent implements Transformable
{
    use TransformableTrait;

    protected $table = "class_packages";

    protected $category_selection = [
        '1' => 'A',
        '2' => 'B',
        '3' => 'C',
        '4' => 'D',
    ];

    public function branches()
    {
        return $this->belongsToMany('Rush\Modules\Merchant\Models\MerchantBranchesModel', 'class_package_branch', 'class_package_id', 'branch_id');
    }

    public function stamps(){
    	return $this->hasMany('Rush\Modules\ClassManagement\Models\PackageStampModel', 'package_id');
    }

    public function categories(){
        return $this->hasMany('Rush\Modules\ClassManagement\Models\PackageCategoryModel', 'class_package_id');
    }

    public function getCategoryNamesAttribute(){
        $category_names = [];
        $package_category_ids = $this->categories()->pluck('class_package_category_list_id')->toArray();

        foreach($package_category_ids as $category_id){
            $category_names[] = $this->category_selection[$category_id];
        }

        return $category_names;
    }

    public function getUuidAttribute($value)
    {
        if (!$value) {
            $name = 'class_packages' . '-' . $this->id;
            $value = Uuid::uuid3(Uuid::NAMESPACE_DNS, $name);
            $this->uuid = $value;
            $this->save();
        }

        return $value;
    }
}
