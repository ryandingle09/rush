<?php

namespace Rush\Modules\ClassManagement\Services;

use Rush\Modules\ClassManagement\Repositories\ClassManagementInterface;
use Rush\Modules\ClassManagement\Repositories\WithBranchInterface;
use Rush\Modules\Push\PushHelper;
use Intervention\Image\ImageManager;
use Carbon\Carbon;

/**
* Strategy Entry Point
*/
class ClassManagementService
{
    protected $repository;

    public function __construct(ClassManagementInterface $repository)
    {
        $this->repository = $repository;
    }

    public function save($request)
    {
    	$inputs = $request->all();

        if(isset($inputs['image_path']) && !empty($inputs['image_path'])){
            $inputs['image_path'] = $this->processImagePath($inputs['image_path']);
        }

    	return $this->repository->save($inputs);
    }

    public function getAll($merchant_id)
    {
    	return $this->repository->getAll($merchant_id);
    }

    public function get($id)
    {
    	return $this->repository->get($id);
    }

    public function delete($id){
        return $this->repository->delete($id);
    }

    /*
    *   @todo : add image helper functionality when uploading new image, the old one will be deleted
    *   @todo : specify the image path to class/instructor/package
    */
    private function processImagePath($image)
    {
    	$image_path = '';
		
		if ( $this->checkImageSupported( $image->getMimeType() ) ) {
            $ext = $image->getClientOriginalExtension();
            $logo_filename =  Carbon::now()->timestamp . '_' . sprintf('%02d', rand(1,99)) . '.' . $ext;
            $logo_path = '/repository/merchant/logo/';
            $save_path = base_path() . '/..' . $logo_path . $logo_filename;

            $manager = new ImageManager();
            $image = $manager->make($image)->resize(200, 200)->save($save_path);
            $image_path = $logo_path . $logo_filename;
        }

        return $image_path;
    }

    private function checkImageSupported($mime_type)
    {
		return in_array( $mime_type, array('image/jpeg','image/png'));
    }

    public function getAllWithBranch($merchant_id)
    {
        if($this->repository instanceof WithBranchInterface){
            return $this->repository->getAllWithBranch($merchant_id);
        }
        return false;
    }
}
