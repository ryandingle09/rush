<?php

namespace Rush\Modules\ClassManagement\Services;

use Carbon\Carbon;
use Rush\Modules\ClassManagement\Repositories\ClassScheduleReservationInterface;
use Rush\Modules\Customer\Models\CustomerModel;
use Rush\Modules\Customer\Models\CustomerStamps;
use Rush\Modules\Customer\Repositories\CustomerPunchCardTransactionsInterface;
use Rush\Modules\Merchant\Services\EmployeeService;
use Rush\Modules\Merchant\Services\SmsNotificationService;
use Rush\Modules\Sms\Library\Sms;
use Rush\Modules\Sms\Repositories\SmsLogInterface;
use Rush\Modules\Sms\Services\SmsCredentialService;
use Rush\Modules\Customer\Services\CustomerService;
use Rush\Modules\Customer\Models\CustomerClassPackageModel;
use Rush\Modules\ClassManagement\Models\ClassScheduleReservationModel;
use Rush\Modules\ClassManagement\Models\ClassScheduleTimeExtractModel;
use Rush\Modules\ClassManagement\Services\CLassListService;

class CLassScheduleReservationService
{
    protected $classScheduleReservationRepository;

    protected $customerPunchCardTransactionsRepository;

    protected $employeeService;

    protected $smsNotificationService;

    protected $smsCredentialService;

    protected $smsLogRepository;

    protected $customerService;

    protected $classListService;

    public function __construct(
        ClassScheduleReservationInterface $classScheduleReservationRepository,
        CustomerPunchCardTransactionsInterface $customerPunchCardTransactionsRepository,
        EmployeeService $employeeService,
        SmsNotificationService $smsNotificationService,
        SmsCredentialService $smsCredentialService,
        SmsLogInterface $smsLogRepository,
        CustomerService $customerService,
        CLassListService $classListService
    ) {
        $this->classScheduleReservationRepository = $classScheduleReservationRepository;
        $this->customerPunchCardTransactionsRepository = $customerPunchCardTransactionsRepository;
        $this->employeeService = $employeeService;
        $this->smsNotificationService = $smsNotificationService;
        $this->smsCredentialService = $smsCredentialService;
        $this->smsLogRepository = $smsLogRepository;
        $this->customerService = $customerService;
        $this->classListService = $classListService;
    }

    public function registerCustomerToClass($merchant_id, $request)
    {
        $class_list_id = $request->class_list_id;
        $customer_package_id = $request->customer_package_id;

        $customer = CustomerModel::where( function ($query) use ($request) {
            $query->where('client_code', $request->client_data)
                ->orWhere('mobileNumber', $request->client_data);
            })
            ->where('merchantId', $merchant_id)
            ->first();

        $class_list_model = $this->classListService->getClassListById($class_list_id);

        //@todo check also if client has no client code
        if($customer == null){
            return ['message' => 'Client not found'];
        }

        if($request->multiple == "false"){
            if($this->classScheduleReservationRepository->checkIfClassIsCancelled($class_list_model)){
                return ['message' => 'Class is already cancelled'];
            }

            if($this->classScheduleReservationRepository->checkIfClassListIsFull($class_list_model)){
                return ['message' => 'Class is already full'];
            }

            if($this->classScheduleReservationRepository->checkIfCustomerAlreadySignedUp($customer, $class_list_id)){
                return ['message' => 'Client Code/Mobile Number already signed up'];
            }

            if($this->classScheduleReservationRepository->checkIfConflictOnCustomerSchedule($customer, $class_list_model)){
                return ['message' => 'Conflict on client\'s schedule'];
            }
        }

        if($customer->merchant->settings->enable_package == 1){
            if($customer_package_id == 0){
                $customer_packages = collect($this->customerService->getValidClassPackages($customer->customerId));

                $customer_packages = $customer_packages->filter(function($customer_package){
                    return $customer_package->package->no_of_visits > $customer_package->card->earned_stamps->count();
                });

                if($customer_packages->count() <= 0){
                    return ['message' => 'Client has no package'];
                } else if($customer_packages->count() == 1 && $request->multiple == "false"){

                    $customer_package_id = $customer_packages->first()->id;
                } else {
                    return ['message' => 'Select from client\'s packages', 'customer_packages' => $customer_packages->toArray()];
                }
            } else {
                $customer_package = CustomerClassPackageModel::where('id', $customer_package_id)->first();
                $class_list = ClassScheduleTimeExtractModel::where('id', $class_list_id)->first();
                $reservation_count = ClassScheduleReservationModel::where('customer_package_id', $customer_package->id)->count();

                if($customer_package->package->no_of_visits <= $reservation_count){
                    return ['message' => 'Customer package has reach its limit'];
                }

                if($class_list->start_date_time->gt($customer_package->end_date->endOfDay())){
                    return ['message' => 'Customer package will be expired on the date of reservation'];
                }
            }
        }

        $reservation = $this->classScheduleReservationRepository->registerCustomerToClassUsingCMS($customer, $class_list_id);
        $reservation->update(['customer_package_id' => $customer_package_id]);

        $merchant = $customer->merchant;

        if ($merchant->settings->enable_success_book_sms) {
            $extracted_schedule = $reservation->class_list;
            $schedule_datetime = $extracted_schedule->start_date_time->format('m/d/Y')
                . ' '
                . $extracted_schedule->start_time_label
                . ' - '
                . $extracted_schedule->end_time_label;
            $class = $extracted_schedule->schedule->classes;
            $instructor = $extracted_schedule->schedule->instructor;
            $branch = $extracted_schedule->schedule->branch;
            $data = [
                'customer_name' => $reservation->customer->name,
                'schedule_datetime' => $schedule_datetime,
                'class' => $class->name,
                'instructor' => $instructor->name,
                'branch' => $branch->name,
                'datetime' => $reservation->created_at->format('m-d-Y , g:i:s A')
            ];

            $sms = new Sms($customer->merchantId, $this->smsCredentialService);

            $transactionType = 'book-class';
            $message = $this->smsNotificationService->getMessage($transactionType, $customer->merchantId);
            $message = $sms->buildMessage($message, $data);
            $result = $sms->send($message, $customer->mobileNumber, $transactionType);

            $this->smsLogRepository->create([
                'merchant_id' => $customer->merchantId,
                'message' => $message,
                'mobile_number' => $customer->mobileNumber,
                'response' => $result,
                'transaction_type' => $transactionType
            ]);
        }

        return $reservation->with('customer', 'customer_package.package')->where('id', $reservation->id)->first();
    }

    public function multipleBookCustomerToClass($merchant_id, $request)
    {
        $response_data = [];
        $class_list_id = $request->class_list_id;
        $class_schedule = $this->classListService->getClassListById($class_list_id)->schedule;
        $customer_package_id = $request->customer_package_id;
        $reservation_dates = json_decode($request->reservation_dates);
        $success_counter = 0;
        $start_date = $reservation_dates[0];
        $end_date = $reservation_dates[count($reservation_dates) - 1];

        $customer = CustomerModel::where( function ($query) use ($request) {
            $query->where('client_code', $request->client_data)
                ->orWhere('mobileNumber', $request->client_data);
        })
        ->where('merchantId', $merchant_id)
        ->first();

        $merchant = $customer->merchant;

        if($customer == null){
            return ['message' => 'Client not found'];
        }

        $this->classListService->setBaseEndDate($reservation_dates[count($reservation_dates) - 1]);
        $this->classListService->updateScheduleTimeExtractWithId($merchant->id, $class_schedule->id);

        foreach($reservation_dates as $reservation_date){
            $class_list_model = $this->classListService->getClassListByDate($class_schedule->id, $reservation_date);

            if($this->classScheduleReservationRepository->checkIfClassIsCancelled($class_list_model)){
                $response_data['alert'][$reservation_date] = [
                    'status' => 'failed',
                    'message' => 'Class is already cancelled'
                ];
                continue;
            }

            if($this->classScheduleReservationRepository->checkIfClassListIsFull($class_list_model)){
                $response_data['alert'][$reservation_date] = [
                    'status' => 'failed',
                    'message' => 'Class is already full'
                ];
                continue;
            }

            if($this->classScheduleReservationRepository->checkIfCustomerAlreadySignedUp($customer, $class_list_model->id)){
                $response_data['alert'][$reservation_date] = [
                    'status' => 'failed',
                    'message' => 'Client Code/Mobile Number already signed up'
                ];
                continue;
            }

            if($this->classScheduleReservationRepository->checkIfConflictOnCustomerSchedule($customer, $class_list_model)){
                $response_data['alert'][$reservation_date] = [
                    'status' => 'failed',
                    'message' => 'Conflict on client\'s schedule'
                ];
                continue;
            }

            if($customer->merchant->settings->enable_package == 1){
                $customer_package = CustomerClassPackageModel::where('id', $customer_package_id)->first();
                $class_list = ClassScheduleTimeExtractModel::where('id', $class_list_id)->first();
                $reservation_count = ClassScheduleReservationModel::where('customer_package_id', $customer_package->id)->count();

                if($customer_package->package->no_of_visits <= $reservation_count){
                    $response_data['alert'][$reservation_date] = [
                        'status' => 'failed',
                        'message' => 'Customer package has reach its limit'
                    ];
                }

                if($class_list->start_date_time->gt($customer_package->end_date->endOfDay())){
                    $response_data['alert'][$reservation_date] = [
                        'status' => 'failed',
                        'message' => 'Customer package will be expired on the date of reservation'
                    ];
                }
            }

            $reservation = $this->classScheduleReservationRepository->registerCustomerToClassUsingCMS($customer, $class_list_model->id);
            $reservation->update(['customer_package_id' => $customer_package_id]);
            $success_counter++;

            $response_data['alert'][$reservation_date] = [
                'status' => 'success',
                'message' => 'Reservation success'
            ];

            if($class_list_id == $class_list_model->id){
                $response_data['display_data'] = $reservation->with('customer', 'customer_package.package')->where('id', $reservation->id)->first();
            }
        }

        if ($merchant->settings->enable_success_book_sms && !empty($reservation)) {
            $extracted_schedule = $reservation->class_list;
            $scheduletime = $extracted_schedule->start_time_label . ' - ' . $extracted_schedule->end_time_label;
            $class = $extracted_schedule->schedule->classes;
            $instructor = $extracted_schedule->schedule->instructor;
            $branch = $extracted_schedule->schedule->branch;
            $data = [
                'customer_name' => $reservation->customer->name,
                'schedulecount' => $success_counter,
                'scheduletime' => $scheduletime,
                'class' => $class->name,
                'instructor' => $instructor->name,
                'branch' => $branch->name,
                'datetime' => $reservation->created_at->format('m-d-Y , g:i:s A'),
                'dayselected' => implode(', ' ,$request->days),
                'start_date' => $start_date,
                'end_date' => $end_date
            ];

            $sms = new Sms($customer->merchantId, $this->smsCredentialService);

            $transactionType = 'book-class-multiple';
            $message = $this->smsNotificationService->getMessage($transactionType, $customer->merchantId);
            $message = $sms->buildMessage($message, $data);
            $result = $sms->send($message, $customer->mobileNumber, $transactionType);

            $this->smsLogRepository->create([
                'merchant_id' => $customer->merchantId,
                'message' => $message,
                'mobile_number' => $customer->mobileNumber,
                'response' => $result,
                'transaction_type' => $transactionType
            ]);
        }

        return $response_data;
    }

    public function getAllCustomerFromClass($class_list_id)
    {
        return $this->classScheduleReservationRepository->getAllCustomerFromClass($class_list_id);
    }

    public function deleteReservation($reservation_id)
    {
        //@todo check if early cancellation or not
        $reservation = ClassScheduleReservationModel::where('id', $reservation_id)->first();

        $customer = $reservation->customer;
        $merchant = $customer->merchant;

        if ($merchant->settings->enable_success_book_sms) {
            $extracted_schedule = $reservation->class_list;
            $schedule_datetime = $extracted_schedule->start_date_time->format('m/d/Y')
                . ' '
                . $extracted_schedule->start_time_label
                . ' - '
                . $extracted_schedule->end_time_label;
            $class = $extracted_schedule->schedule->classes;
            $instructor = $extracted_schedule->schedule->instructor;
            $branch = $extracted_schedule->schedule->branch;
            $now = Carbon::now();
            $data = [
                'customer_name' => $reservation->customer->name,
                'schedule_datetime' => $schedule_datetime,
                'class' => $class->name,
                'instructor' => $instructor->name,
                'branch' => $branch->name,
                'datetime' => $now->format('m-d-Y , g:i:s A')
            ];

            $sms = new Sms($customer->merchantId, $this->smsCredentialService);

            $transactionType = 'cancel-class';
            $message = $this->smsNotificationService->getMessage($transactionType, $customer->merchantId);
            $message = $sms->buildMessage($message, $data);
            $result = $sms->send($message, $customer->mobileNumber, $transactionType);

            $this->smsLogRepository->create([
                'merchant_id' => $customer->merchantId,
                'message' => $message,
                'mobile_number' => $customer->mobileNumber,
                'response' => $result,
                'transaction_type' => $transactionType
            ]);
        }

        return $this->classScheduleReservationRepository->deleteReservation($reservation_id);
    }

    public function confirmAttendanceReservation($reservation_id, $toggle)
    {
        $reservation = ClassScheduleReservationModel::where('id', $reservation_id)->first();

        if($reservation->customer_package->package->no_of_visits <= $reservation->customer_package->card->earned_stamps->count()){
            return ['message' => 'Client\'s package don\'t have enough remaining stamps'];
        }

        $result = $this->classScheduleReservationRepository->confirmAttendanceReservation($reservation_id, $toggle);

        if ($result) {
            if($reservation->customer->merchant->settings->enable_package == 1){

                $remarks = $reservation->class_list->start_date_time->format('l') . "\n ";
                $remarks .= $reservation->class_list->start_date_time->format('F d, Y') . ' ' . $reservation->class_list->start_time_label . "\n ";
                $remarks .= $reservation->class_list->schedule->classes->name . "\n ";
                $remarks .= $reservation->class_list->instructor->name . "\n";
                $remarks .= $reservation->class_list->schedule->branch->name;

                $this->customerService->addClassPackagePunchcardStamp($reservation->customer->customerId, $reservation->customer_package->card, null, $remarks);
            } else {
                $this->earnStamp($reservation_id);
            }
        }

        return $result;
    }

    public function earnStamp($reservation_id)
    {
        $reservation = $this->classScheduleReservationRepository->getById($reservation_id);
        $customer = $reservation->customer;
        $schedule = $reservation->class_list->schedule;

        $transaction = $this->customerPunchCardTransactionsRepository->getByOrNoAndCustomerId($reservation->customer_id, $schedule->id);

        if (!$transaction) {
            $in_api_customer = $reservation->customer;
            $card = $in_api_customer->card;
            $stamps = 1;

            // Check for maximum stamps required
            // Get promo max stamp
            $max_num_stamp = $card->punchcard->num_stamps;
            $current_no_stamp = count($card->stamps);

            if (($current_no_stamp == 0) && $stamps > $max_num_stamp) {
                $stamps = $max_num_stamp;
            } elseif (($current_no_stamp + $stamps) > $max_num_stamp) {
                $stamps = $max_num_stamp - $current_no_stamp;
            }

            $branch = $schedule->branch;
            $or_reference = $schedule->id;
            $employee = $this->employeeService->getMerchantBranchSystemEmployee($customer->merchantId, $branch->id);

            $transaction = $this->customerPunchCardTransactionsRepository->addTransaction(
                $employee,
                $card,
                0,
                0,
                false,
                'earn-attendance',
                'web',
                $stamps,
                null,
                $or_reference,
                $branch->id);

            //MTODO: convert direct model access to repository access
            for ($i = 1; $i <= $stamps; ++$i) {
                $stamp = new CustomerStamps();
                $stamp->transaction_id = $transaction->id;
                $stamp->stamp_card_id = $card->id;
                $stamp->branch_id = $branch->id;
                $stamp->void = 0;
                $stamp->save();
            }

            // custom sms
            $sms = new Sms($customer->merchantId, $this->smsCredentialService);

            $current_stamps = count($in_api_customer->card->stamps);
            $datetime = Carbon::createFromTimestamp($transaction->transaction_ref);
            $data = [
                'customer_name' => $customer->name,
                'transaction_stamps' => $stamps,
                'stamp_name' => $customer->merchant->settings->points_name,
                'amount' => $transaction->amount,
                'business' => $customer->merchant->businessName,
                'datetime' => $datetime->format('m-d-Y') .','. $datetime->format('g:i:s A'),
                'current_stamps' => $current_stamps,
                'reference' => $transaction->transaction_ref,
                'branch' => $branch ? $branch->branchName : null,
                'instructor' => $schedule->instructor->name,
                'class' => $schedule->classes->name
            ];
            $transactionType = 'earn-signedin-stamp';
            $message = $this->smsNotificationService->getMessage($transactionType, $customer->merchantId);
            $message = $sms->buildMessage($message, $data);
            $result = $sms->send($message, $customer->mobileNumber, $transactionType);

            $this->smsLogRepository->create([
                'merchant_id' => $customer->merchantId,
                'message' => $message,
                'mobile_number' => $customer->mobileNumber,
                'response' => $result,
                'transaction_type' => $transactionType
            ]);
        }
    }
}