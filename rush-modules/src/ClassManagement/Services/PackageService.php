<?php

namespace Rush\Modules\ClassManagement\Services;

use Rush\Modules\ClassManagement\Repositories\PackageInterface;
use Rush\Modules\Push\PushHelper;
use Intervention\Image\ImageManager;
use Carbon\Carbon;

class PackageService extends ClassManagementService
{
    public function __construct(PackageInterface $packageRepository)
    {
        $this->repository = $packageRepository;
    }

    //@todo temporary location (/repository/merchant/logo/) until storage helper applied
    public function savePackageStamps($request)
    {
        $package_stamps = json_decode($request->package_stamps);
        $package_stamps = ($package_stamps == null) ? [] : $package_stamps;
        $inputs['package_id'] = $request->package_id;


        foreach ($package_stamps as $stamp_no => $stamp) {
            if($stamp->action == 'add'){
                if(isset($stamp->fake_path)){
                    $logo_filename =  Carbon::now()->timestamp . '_' . sprintf('%02d', rand(1,10000)) . '.png';
                    $logo_path = '/repository/merchant/logo/';
                    $save_path = base_path() . '/..' . $logo_path . $logo_filename;

                    $manager = new ImageManager();
                    $image = $manager->make(file_get_contents($stamp->fake_path))->resize(350, 200)->save($save_path);
                    $input['image_path'] = $logo_path . $logo_filename;
                }

                $input['name'] = $stamp->name;
                $input['details'] = $stamp->details;
            }

            $input['stamp_no'] = $stamp_no;
            $input['package_id'] = $request->package_id;
            $input['action'] = $stamp->action;

            $this->repository->savePackageStamp($input);
        }
    }

    public function getPackageStamp($package_id, $stamp_id)
    {
        return $this->repository->getPackageStamp($package_id, $stamp_id);
    }

    /**
     * Get package stamp by specific column
     *
     * @param int $package_id
     * @param string $column
     * @param mixed $value
     * @return null|\Rush\Modules\ClassManagement\Models\PackageStampModel
     */
    public function getPackageStampBy($package_id, $column, $value)
    {
        return $this->repository->getPackageStampBy($package_id, $column, $value);
    }


    /**
     * Get Package Expiration by Date
     *
     * @param int                   $package_id
     * @param \Carbon\Carbon        $start_date
     * @return \Carbon\Carbon
     */
    public function getExpirationDate($package_id, $start_date)
    {
        $package = $this->repository->get($package_id);

        return $this->repository->getExpirationDate($package, $start_date);
    }

    /**
     * Get Package by Column/Value
     *
     * @param string        $column
     * @param string        $value
     * @return null|\Rush\Modules\ClassManagement\Models\PackageModel
     */
    public function getPackageBy($column, $value)
    {
        return $this->repository->getBy($column, $value);
    }
}
