<?php

namespace Rush\Modules\ClassManagement\Services;

use Rush\Modules\ClassManagement\Repositories\ClassScheduleInterface;

class ClassScheduleService extends ClassManagementService
{
    public function __construct(ClassScheduleInterface $classScheduleRepository)
    {
        $this->repository = $classScheduleRepository;
    }

    public function extractAllScheduleTime($merchant_id, $until = null)
    {
		return $this->repository->extractAllScheduleTime($merchant_id, $until);
    }

    public function validateClassScheduleList($request)
    {
		return $this->repository->validateClassScheduleList($request);
    }

    public function getClassListForToday()
    {

    }

    public function getClassScheduleList(){

        //while data count is lower than request and there is existing never ending schedule time
            // extract schedule time

    }

    public function filterClassScheduleList(){
        //get all schedule time that is below the end date
            //make extraction of schedule time
            //update schedule time

        //get all schedule time extracted
    }
}
