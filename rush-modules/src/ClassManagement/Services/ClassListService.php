<?php

namespace Rush\Modules\ClassManagement\Services;

use Rush\Modules\ClassManagement\Repositories\ClassScheduleTimeExtractInterface;
use Carbon\Carbon;

class CLassListService
{
	protected $classListRepository;

    public function __construct(ClassScheduleTimeExtractInterface $classListRepository)
    {
        $this->classListRepository = $classListRepository;
    }

    public function getClassListById($class_list_id)
    {
        return $this->classListRepository->getClassListById($class_list_id);
    }

    public function getClassListByDate($schedule_id, $date)
    {
        return $this->classListRepository->getClassListByDate($schedule_id, $date);
    }

    public function generateClassList($request)
    {
        $merchant_id = $request->merchant_id;
        if(!isset($request->start_date_rage) || !isset($request->end_date_rage)){
            return $this->getClassListDefault($merchant_id);
        } else {
            $this->classListRepository->base_start_date_range = Carbon::parse($request->start_date_rage);
            $this->classListRepository->base_end_date_range = Carbon::parse($request->end_date_rage);

            $this->classListRepository->updateScheduleTimeExtract($merchant_id);

            return $this->classListRepository->getClassScheduleList($merchant_id);
        }
    }

    public function getClassListDefault($merchant_id)
    {
        $this->classListRepository->base_start_date_range = Carbon::today();
        $this->classListRepository->base_end_date_range = $this->classListRepository->base_start_date_range->copy()->endOfMonth();

        $this->classListRepository->updateScheduleTimeExtract($merchant_id);

        return $this->classListRepository->getClassScheduleList($merchant_id);
    }

    public function getClassListForToday($merchant_id)
    {
    	$this->classListRepository->base_start_date_range = Carbon::today();
    	$this->classListRepository->base_end_date_range = $this->classListRepository->base_start_date_range->copy();

    	$this->classListRepository->updateScheduleTimeExtract($merchant_id);

    	return $this->classListRepository->getClassScheduleList($merchant_id);
    }

    public function updateClassListSchedule($request)
    {
        $update_type = $request->update_type;
        
        $input['class_list_id'] = $request->class_list_id;
        $input['instructor_id'] = $request->instructor_id;
        $input['attendance_capacity'] = $request->attendance_capacity;

        $class_list = $this->classListRepository->getClassListById($input['class_list_id']);

        if($class_list->schedule->instructor_id == $input['instructor_id']){
            $input['instructor_status'] = 'original';
        } else {
            $input['instructor_status'] = 'substitute';
        }

        $this->setProperDatesBasedOnRequest($request);
        $this->classListRepository->updateScheduleTimeExtract($request->merchant_id);
        
        switch ($update_type) {
            case "type_one":
                    $this->classListRepository->updateOneClassListSchedule($input);
                break;
            case "type_following":
                    $this->classListRepository->updateFollowingClassListSchedule($input);
                break;
            case "type_block":
                    $this->classListRepository->updateBlockClassListSchedule($input);
                break;
        }        
    }

    public function cancelClassListSchedule($request)
    {
        $action = $request->action;
        $class_list_id = $request->class_list_id;
        
        $this->setProperDatesBasedOnRequest($request);
        $this->classListRepository->updateScheduleTimeExtract($request->merchant_id);

        switch ($action) {
            case "type_one":
                    $this->classListRepository->cancelOneClassList($class_list_id);
                break;
            case "type_following":
                    $this->classListRepository->cancelFollowingClassList($class_list_id);
                break;
            case "type_block":
                    $this->classListRepository->cancelBlockOfClassList($class_list_id);
                break;
        }
    }

    private function setProperDatesBasedOnRequest($request){
        $this->classListRepository->base_start_date_range = isset($request->start_date_range) ? Carbon::parse($request->start_date_range) : Carbon::now();
        $this->classListRepository->base_end_date_range = isset($request->end_date_range) ? Carbon::parse($request->end_date_range)->endOfDay() : Carbon::now();

        if($this->classListRepository->base_start_date_range->isPast()){
            $this->classListRepository->base_start_date_range = Carbon::now();
        }

        if($this->classListRepository->base_end_date_range->isPast()){
            $this->classListRepository->base_end_date_range = Carbon::now()->endOfDay();
        }
    }

    public function getInstructorOnGoingClassScheduleList($request){
        return $this->classListRepository->getInstructorOnGoingClassScheduleList($request->instructor_id);
    }

    public function getClassOnGoingClassScheduleList($request){
        return $this->classListRepository->getClassOnGoingClassScheduleList($request->class_id);
    }

    public function getMultibookFormDates($request){
        $days = $this->classListRepository->getClassScheduleDays($request->class_list_id);
        $dates = $this->classListRepository->getMultibookDateOptions($request->class_list_id);

        return [
            'days' => $days,
            'dates' => $dates,
        ];
    }

    public function setBaseStartDate($date){
        $this->classListRepository->base_start_date_range = Carbon::parse($date);
    }

    public function setBaseEndDate($date){
        $this->classListRepository->base_end_date_range = Carbon::parse($date);
    }

    public function updateScheduleTimeExtractWithId($merchant_id, $schedule_id){
        $this->classListRepository->updateScheduleTimeExtract($merchant_id, $schedule_id);
    }
}
