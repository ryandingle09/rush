<?php

namespace Rush\Modules\ClassManagement\Providers;

use Illuminate\Container\Container;
use Illuminate\Support\ServiceProvider;
use Rush\Modules\ClassManagement\Models\ClassScheduleModel;
use Rush\Modules\ClassManagement\Repositories\ClassScheduleTimeExtractRepository;

/**
* Register our Repository with Laravel
*/
class ClassScheduleTimeExtractRepositoryServiceProvider extends ServiceProvider
{
    /**
    * Registers the ClassScheduleInterface with Laravels IoC Container
    */
    public function register()
    {
        // Bind the returned class to the namespace 'Repositories\ClassScheduleTimeExtractInterface
        $this->app->bind(
            '\Rush\Modules\ClassManagement\Repositories\ClassScheduleTimeExtractInterface',
            function($app) {
                return new ClassScheduleTimeExtractRepository();
            }
        );
    }
}
