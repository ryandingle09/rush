<?php

namespace Rush\Modules\ClassManagement\Providers;

use Illuminate\Container\Container;
use Illuminate\Support\ServiceProvider;
use Rush\Modules\ClassManagement\Models\ClassModel;
use Rush\Modules\ClassManagement\Repositories\ClassRepository;

/**
* Register our Repository with Laravel
*/
class ClassRepositoryServiceProvider extends ServiceProvider
{
    /**
    * Registers the ClassInterface with Laravels IoC Container
    */
    public function register()
    {
        // Bind the returned class to the namespace 'Repositories\ClassInterface
        $this->app->bind(
            '\Rush\Modules\ClassManagement\Repositories\ClassInterface',
            function($app) {
                return new ClassRepository();
            }
        );
    }
}
