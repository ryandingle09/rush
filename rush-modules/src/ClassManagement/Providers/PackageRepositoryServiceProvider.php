<?php

namespace Rush\Modules\ClassManagement\Providers;

use Illuminate\Container\Container;
use Illuminate\Support\ServiceProvider;
use Rush\Modules\ClassManagement\Models\PackageModel;
use Rush\Modules\ClassManagement\Repositories\PackageRepository;

/**
* Register our Repository with Laravel
*/
class PackageRepositoryServiceProvider extends ServiceProvider
{
    /**
    * Registers the PackageInterface with Laravels IoC Container
    */
    public function register()
    {
        // Bind the returned class to the namespace 'Repositories\PackageInterface
        $this->app->bind(
            '\Rush\Modules\ClassManagement\Repositories\PackageInterface',
            function($app) {
                return new PackageRepository();
            }
        );
    }
}
