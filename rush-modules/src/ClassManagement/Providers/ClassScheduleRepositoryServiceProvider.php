<?php

namespace Rush\Modules\ClassManagement\Providers;

use Illuminate\Container\Container;
use Illuminate\Support\ServiceProvider;
use Rush\Modules\ClassManagement\Models\ClassScheduleModel;
use Rush\Modules\ClassManagement\Repositories\ClassScheduleRepository;

/**
* Register our Repository with Laravel
*/
class ClassScheduleRepositoryServiceProvider extends ServiceProvider
{
    /**
    * Registers the ClassScheduleInterface with Laravels IoC Container
    */
    public function register()
    {
        // Bind the returned class to the namespace 'Repositories\ClassScheduleInterface
        $this->app->bind(
            '\Rush\Modules\ClassManagement\Repositories\ClassScheduleInterface',
            function($app) {
                return new ClassScheduleRepository();
            }
        );
    }
}
