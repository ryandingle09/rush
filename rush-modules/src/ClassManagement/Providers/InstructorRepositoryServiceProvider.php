<?php

namespace Rush\Modules\ClassManagement\Providers;

use Illuminate\Container\Container;
use Illuminate\Support\ServiceProvider;
use Rush\Modules\ClassManagement\Models\InstructorModel;
use Rush\Modules\ClassManagement\Repositories\InstructorRepository;

/**
* Register our Repository with Laravel
*/
class InstructorRepositoryServiceProvider extends ServiceProvider
{
    /**
    * Registers the InstructorInterface with Laravels IoC Container
    */
    public function register()
    {
        // Bind the returned class to the namespace 'Repositories\InstructorInterface
        $this->app->bind(
            '\Rush\Modules\ClassManagement\Repositories\InstructorInterface',
            function($app) {
                return new InstructorRepository();
            }
        );
    }
}
