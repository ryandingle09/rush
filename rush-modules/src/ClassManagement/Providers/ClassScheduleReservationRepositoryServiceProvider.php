<?php

namespace Rush\Modules\ClassManagement\Providers;

use Illuminate\Container\Container;
use Illuminate\Support\ServiceProvider;
use Rush\Modules\ClassManagement\Models\ClassScheduleReservationModel;
use Rush\Modules\ClassManagement\Repositories\ClassScheduleReservationRepository;

/**
* Register our Repository with Laravel
*/
class ClassScheduleReservationRepositoryServiceProvider extends ServiceProvider
{
    /**
    * Registers the ClassScheduleReservationInterface with Laravels IoC Container
    */
    public function register()
    {
        // Bind the returned class to the namespace 'Repositories\ClassScheduleReservationInterface
        $this->app->bind(
            '\Rush\Modules\ClassManagement\Repositories\ClassScheduleReservationInterface',
            function($app) {
                return new ClassScheduleReservationRepository();
            }
        );
    }
}
