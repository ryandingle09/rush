<?php
namespace Rush\Modules\ClassManagement\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\ClassManagement\Repositories\ClassManagementToolsModulesInterface;
use Rush\Modules\ClassManagement\Repositories\ClassManagementToolsModulesRepository;

class ClassManagementToolsModulesRepositoryServiceProvider extends  ServiceProvider
{
  public function register()
  {
    $this->app->singleton(ClassManagementToolsModulesInterface::class, ClassManagementToolsModulesRepository::class);
  }
}