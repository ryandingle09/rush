<?php
namespace Rush\Modules\ClassManagement\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\ClassManagement\Repositories\ClassManagementToolsInterface;
use Rush\Modules\ClassManagement\Repositories\ClassManagementToolsRepository;

class ClassManagementToolsRepositoryServiceProvider extends  ServiceProvider
{
  public function register()
  {
    $this->app->singleton(ClassManagementToolsInterface::class, ClassManagementToolsRepository::class);
  }
}