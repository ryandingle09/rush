<?php

namespace Rush\Modules\ClassManagement\Repositories;

use \stdClass;
use DB;
use Carbon\Carbon;
use Rush\Modules\ClassManagement\Models\ClassScheduleTimeExtractModel;
use Rush\Modules\ClassManagement\Models\ClassScheduleModel;
use Rush\Modules\Repository\AbstractRepository;

class ClassScheduleTimeExtractRepository extends AbstractRepository implements ClassScheduleTimeExtractInterface
{
	protected $connection = 'mysql';

	public $base_start_date_range;
	public $base_end_date_range;

    public function __construct()
    {
    	$this->base_start_date_range = Carbon::today();
    	$this->base_end_date_range = $this->base_start_date_range->copy();
        parent::__construct(new ClassScheduleTimeExtractModel);
    }

    public function getClassListById($class_list_id)
    {
        return $this->model->where('id', $class_list_id)->has('instructor')->whereHas('schedule', function($query){ return $query->has('classes'); })->first();
    }

    public function getClassListByDate($schedule_id, $date)
    {
        $base_date = Carbon::parse($date);

        return $this->model->whereDate('start_date_time', '=', $base_date->toDateString())
            ->whereHas('schedule', function($query) use ($schedule_id) {
                return $query->where('id', $schedule_id)->with(['classes' => function ($query) {
                    $query->withTrashed();
                }]);
            })->has('schedule')->with('schedule.classes', 'instructor')
            ->first();
    }

	public function getClassScheduleList($merchant_id)
    {
    	return $this->model->whereDate('start_date_time', '>=', $this->base_start_date_range->toDateString())->whereDate('start_date_time', '<=', $this->base_end_date_range->toDateString())->whereHas('schedule', function($query) use ($merchant_id){
    			return $query->where('merchant_id', $merchant_id)->with(['classes' => function($query){
                    $query->withTrashed();
                }]);
    		})->orderBy('start_date_time', 'asc')->orderBy('created_at', 'asc')
            ->with(['instructor' => function($query){
                $query->withTrashed();
            }])->get();
    }

    public function getInstructorOnGoingClassScheduleList($instructor_id, $limit = 10){
        return $this->model->whereDate('start_date_time', '>=', Carbon::today()->toDateString())
            ->where('instructor_id', $instructor_id)
            ->has('schedule')->with('schedule.classes', 'instructor')
            ->orderBy('start_date_time', 'asc')
            ->orderBy('created_at', 'asc')
            ->limit($limit)->get();
    }

    public function getClassOnGoingClassScheduleList($class_id, $limit = 10){
        return $this->model->whereDate('start_date_time', '>=', Carbon::today()->toDateString())
            ->whereHas('schedule', function($query) use ($class_id){
                $query->where('class_id', $class_id);
            })->with('schedule.classes', 'instructor')
            ->orderBy('start_date_time', 'asc')
            ->orderBy('created_at', 'asc')
            ->limit($limit)->get();
    }

    public function updateOneClassListSchedule($input)
    {
        $class_list_id = $input['class_list_id'];
        unset($input['class_list_id']);
        
        $this->model->where('id', $class_list_id)->update($input);
    }

    public function updateFollowingClassListSchedule($input)
    {
        $class_list_id = $input['class_list_id'];
        $schedule_list = $this->getClassListById($class_list_id);
        $schedule_id = $schedule_list->schedule->id;
        unset($input['class_list_id']);

        $schedule_new_data = $input;
        unset($schedule_new_data['instructor_status']);

        if($schedule_list->start_date_time->isFuture()){
            $this->base_start_date_range = $schedule_list->start_date_time;
        }

        $input['instructor_status'] = 'original';

        ClassScheduleModel::where('id', $schedule_id)->update($schedule_new_data);

        return $this->model->where('schedule_id', $schedule_id)->where('start_date_time', '>=', $this->base_start_date_range->toDateTimeString())->update($input);
    }

    public function updateBlockClassListSchedule($input)
    {
        $class_list_id = $input['class_list_id'];
        $schedule_id = $this->getClassListById($class_list_id)->schedule->id;
        unset($input['class_list_id']);

        return $this->model->where('schedule_id', $schedule_id)->where('start_date_time', '>=', $this->base_start_date_range->toDateTimeString())->where('start_date_time', '<=', $this->base_end_date_range->toDateTimeString())->update($input);
    }

    public function updateScheduleTimeExtract($merchant_id, $schedule_id = null)
    {
        if($schedule_id == null){
            $unupdated_schedules = ClassScheduleModel::where('end_date', '<', $this->base_end_date_range->toDateString())->where('is_repeat', '1')->where('merchant_id', $merchant_id)->has('classes')->get();
        } else {
            $unupdated_schedules = ClassScheduleModel::where('id', $schedule_id)
                ->where('end_date', '<', $this->base_end_date_range->toDateString())
                ->where('is_repeat', '1')->where('merchant_id', $merchant_id)
                ->has('classes')->get();
        }

		foreach ($unupdated_schedules as $schedule) {
			$updated_date = $this->base_end_date_range->copy();
			$counter_date = $updated_date->copy()->addDays(30);
			$base_date = $this->model->where('schedule_id', $schedule->id)->orderBy('start_date_time', 'desc')->first()->start_date_time->copy();
            $repeat_every = ($schedule->repeat_every <= 0) ?  1 : $schedule->repeat_every;
            $base_date->addDay();

			while($counter_date->gte($base_date)){
				$days = explode(',', $schedule->repeat_on);

				foreach ($days as $day) {
					$date = $this->getNextDateBasedOnDay($day, $base_date);

					$data['schedule_id'] = $schedule->id;
					$data['instructor_id'] = $schedule->instructor_id;
					$data['instructor_status'] = 'original';
					$data['status'] = 'active';
					$data['start_date_time'] = $this->addHrsAndMins($date, $schedule->start_time);
                    $data['end_date_time'] = $this->addHrsAndMins($date, $schedule->end_time);
					$data['start_time'] = $schedule->start_time;
					$data['end_time'] = $schedule->end_time;
                    $data['attendance_capacity'] = $schedule->attendance_capacity;
                    $data['status'] = $schedule->status;

					$updated_date = $date->copy();
					$this->model->create($data);
				}

                $base_date->addWeeks($repeat_every);
			}

			$schedule->update(['end_date' => $updated_date]);
		}
    }

    public function cancelOneClassList($class_list_id)
    {
        return $this->model->where('id', $class_list_id)->update(['status' => 'cancelled']);
    }

    public function cancelFollowingClassList($class_list_id)
    {
        $schedule_list = $this->getClassListById($class_list_id);
        $schedule_id = $schedule_list->schedule->id;

        if($schedule_list->start_date_time->isFuture()){
            $this->base_start_date_range = $schedule_list->start_date_time;
        }

        ClassScheduleModel::where('id', $schedule_id)->update(['status' => 'cancelled']);

        return $this->model->where('schedule_id', $schedule_id)->where('start_date_time', '>=', $this->base_start_date_range->toDateTimeString())->update(['status' => 'cancelled']);
    }

    public function cancelBlockOfClassList($class_list_id)
    {
        $schedule_id = $this->getClassListById($class_list_id)->schedule->id;

        return $this->model->where('schedule_id', $schedule_id)->where('start_date_time', '>=', $this->base_start_date_range->toDateTimeString())->where('start_date_time', '<=', $this->base_end_date_range->toDateTimeString())->update(['status' => 'cancelled']);
    }

    private function getNextDateBasedOnDay($day, $date)
    {
        switch ($day) {
            case 'sunday':
                if($date->dayOfWeek != Carbon::SUNDAY){
                    $date = $date->copy()->next(Carbon::SUNDAY);
                }
                break;
            case 'monday':
                if($date->dayOfWeek != Carbon::MONDAY){
                    $date = $date->copy()->next(Carbon::MONDAY);
                }
                break;
            case 'tuesday':
                if($date->dayOfWeek != Carbon::TUESDAY){
                    $date = $date->copy()->next(Carbon::TUESDAY);
                }
                break;
            case 'wednesday':
                if($date->dayOfWeek != Carbon::WEDNESDAY){
                    $date = $date->copy()->next(Carbon::WEDNESDAY);
                }
                break;
            case 'thursday':
                if($date->dayOfWeek != Carbon::THURSDAY){
                    $date = $date->copy()->next(Carbon::THURSDAY);
                }
                break;
            case 'friday':
                if($date->dayOfWeek != Carbon::FRIDAY){
                    $date = $date->copy()->next(Carbon::FRIDAY);
                }
                break;
            case 'saturday':
                if($date->dayOfWeek != Carbon::SATURDAY){
                    $date = $date->copy()->next(Carbon::SATURDAY);
                }
                break;
        }

        return $date;
    }

    public function addHrsAndMins($date, $time){
        return $date->copy()->startOfDay()->addHours((int)substr($time, 0, -2))->addMinutes((int)substr($time, -2));
    }

    public function getClassScheduleDays($class_list_id)
    {
        $days = [];
        $schedule_days = explode(',', $this->getClassListById($class_list_id)->schedule->repeat_on);

        foreach($schedule_days as $day){
            $days[$day] = ucfirst(substr($day, 0, 3));
        }

        return $days;
    }

    public function getMultibookDateOptions($class_list_id)
    {
        $dates = [];
        $class_list = $this->getClassListById($class_list_id);
        $class_schedule = $class_list->schedule;
        $base_date = $class_list->start_date_time;
        $repeat_every = ($class_schedule->repeat_every <= 0) ?  1 : $class_schedule->repeat_every;
        $counter_date = Carbon::now()->addMonths(6);

        if($class_schedule->is_repeat == 0 && $counter_date->gt($class_schedule->end_date)){
            $counter_date = $class_schedule->end_date->copy();
        }

        while($counter_date->gte($base_date)){
            $days = explode(',', $class_schedule->repeat_on);

            foreach ($days as $day) {
                $date = $this->getNextDateBasedOnDay($day, $base_date);

                $dates[$date->timestamp] = $date->format('l m/d/Y');
            }

            $base_date->addWeeks($repeat_every);
        }

        return $dates;
    }
}