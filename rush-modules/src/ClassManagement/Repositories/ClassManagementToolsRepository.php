<?php
namespace Rush\Modules\ClassManagement\Repositories;

use Rush\Modules\ClassManagement\Models\ClassManagementToolsModel;
use Rush\Modules\Repository\AbstractRepository;

class ClassManagementToolsRepository extends AbstractRepository implements ClassManagementToolsInterface
{

  public function __construct(ClassManagementToolsModel $classManagementToolsModel)
  {
    parent::__construct($classManagementToolsModel);
  }

  public function updatebyMerchant( $merchant_id, $modules_id )
  {
    $data = $this->model->where(['merchant_id' => $merchant_id])->get();
    if ( $data->count() > 0 ) $this->model->where(['merchant_id' => $merchant_id])->delete();
    foreach($modules_id as $module) {
      $moduleMerchant = new ClassManagementToolsModel();
      $moduleMerchant->class_management_tool_id = $module;
      $moduleMerchant->merchant_id = $merchant_id;
      $moduleMerchant->save();
    }
    return TRUE;
  }
}
