<?php

namespace Rush\Modules\ClassManagement\Repositories;

/**
 * The PackageInterface contains unique method signatures related to Package object
 */
interface PackageInterface extends ClassManagementInterface
{
	public function savePackageBranches($packageModel, $branches);
	public function savePackageStamp($inputs);
	public function getPackageStamp($package_id, $stamp_id);
}
