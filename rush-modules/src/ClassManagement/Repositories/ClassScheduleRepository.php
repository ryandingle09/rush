<?php

namespace Rush\Modules\ClassManagement\Repositories;

use \stdClass;
use DB;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Rush\Modules\ClassManagement\Models\ClassScheduleModel;
use Rush\Modules\ClassManagement\Models\ClassScheduleTimeExtractModel;
use Rush\Modules\Repository\AbstractRepository;

/**
 * A repository class would encapsulate each Eloquent Model class
 * and be responsible for CRUD operations on the database.
 */
class ClassScheduleRepository extends AbstractRepository implements ClassScheduleInterface
{
    protected $connection = 'mysql';

    public function __construct()
    {
        parent::__construct(new ClassScheduleModel);
    }

    public function save($inputs)
    {
    	$class_schedule = isset($inputs['schedule_id']) ? $this->model->where('id', $inputs['schedule_id'])->first() : $this->model;
        $inputs['start_time'] = ($inputs['start_period'] == 'AM' || ($inputs['start_hr'] == 12 && $inputs['start_period'] == 'PM')) ? $inputs['start_hr'] . $inputs['start_min'] : ($inputs['start_hr'] . $inputs['start_min']) + 1200;
        $inputs['end_time'] = ($inputs['end_period'] == 'AM' || ($inputs['end_hr'] == 12 && $inputs['end_period'] == 'PM')) ? $inputs['end_hr'] . $inputs['end_min'] : ($inputs['end_hr'] . $inputs['end_min']) + 1200;

        $class_schedule->class_id = $inputs['class_id'];
        $class_schedule->instructor_id = $inputs['instructor_id'];
        $class_schedule->merchant_id = $inputs['merchant_id'];
        $class_schedule->branch_id = $inputs['branch_id'];
        $class_schedule->attendance_capacity = $inputs['attendance_capacity'];
        $class_schedule->status = 'active';

        $class_schedule->start_date = $inputs['start_date'];
        $class_schedule->end_date = $inputs['repeat_end_date'];
        $class_schedule->start_time = $inputs['start_time'];
        $class_schedule->end_time = $inputs['end_time'];
        $class_schedule->is_repeat = (isset($inputs['is_repeat']) && $inputs['is_repeat'] == 'on') ? 1 : 0;
        $class_schedule->repeat_selection = 'once';

        if($class_schedule->is_repeat){
            $class_schedule->repeats = $inputs['repeats'];
            $class_schedule->repeat_every = $inputs['repeat_every'];
            $class_schedule->repeat_selection = $inputs['repeat_end_selection'];
            $class_schedule->repeat_occurence = isset($inputs['occurence']) ? $inputs['occurence'] : 0;
            $class_schedule->repeat_on = isset($inputs['repeat_on']) ? implode(',', $inputs['repeat_on']) : "";
        }

        if(isset($inputs['image_path'])){
            $class_schedule->image_path = $inputs['image_path'];
        }

        $class_schedule->save();

        $this->saveScheduleTimeExtract($class_schedule, $inputs);
    }

    public function saveScheduleTimeExtract($scheduleModel, $inputs)
    {
        $start_date = Carbon::parse($inputs['start_date']);
        $inputs['instructor_id'] = $scheduleModel->instructor_id;
        $data = [];

        if(isset($inputs['is_repeat'])){
            $data = $this->extractScheduleTime($inputs);
        } else {
            $info['start_date_time'] = $this->addHrsAndMins($start_date, $inputs['start_time']);
            $info['end_date_time'] = $this->addHrsAndMins($start_date, $inputs['end_time']);
            $info['start_time'] = $inputs['start_time'];
            $info['end_time'] = $inputs['end_time'];
            $info['instructor_id'] = $inputs['instructor_id'];
            $info['instructor_status'] = 'original';
            $info['status'] = 'active';
            $info['attendance_capacity'] = $inputs['attendance_capacity'];

            $data[] = $info;
        }

        $scheduleModel->update(['end_date' => end($data)['start_date_time']]);

        $scheduleModel->scheduleTimeExtract()->where('start_date_time', '>=', Carbon::now()->toDateTimeString())->delete();
        $scheduleModel->scheduleTimeExtract()->createMany($data);
    }

    public function extractScheduleTime($values){
        $start_date = Carbon::parse($values['start_date']);
        $counter_date = (empty($values['repeat_end_date'])) ? Carbon::now()->addDays(30) : Carbon::parse($values['repeat_end_date']);
        $start_time = $values['start_time'];
        $end_time = $values['end_time'];
        $repeat_end_selection = $values['repeat_end_selection'];
        $repeat_every = $values['repeat_every'];
        $attendance_capacity = $values['attendance_capacity'];
        $days = $values['repeat_on'];
        $return_values = [];

        while ($counter_date->gte($start_date)) {
            foreach ($days as $day) {
                $date = $this->getNextDateBasedOnDay($day, $start_date);
                $start_date_time = $this->addHrsAndMins($date, $start_time);
                $end_date_time = $this->addHrsAndMins($date, $end_time);
                
                if(!$start_date_time->isPast() || $start_date_time->isToday() ){
                    $data['start_date_time'] = $start_date_time;
                    $data['end_date_time'] = $end_date_time;
                    $data['start_time'] = $start_time;
                    $data['end_time'] = $end_time;
                    $data['instructor_id'] = $values['instructor_id'];
                    $data['instructor_status'] = 'original';
                    $data['status'] = 'active';
                    $data['attendance_capacity'] = $attendance_capacity;

                    $return_values[] = $data;
                }
            }

            $start_date->addWeeks($repeat_every);
        }

        return $return_values;
    }

    public function getAll($merchant_id)
    {
    	return $this->model->where('merchant_id', $merchant_id)->with(['instructor' => function($query){
            $query->withTrashed();
        },'classes' => function($query){
            $query->withTrashed();
        }], 'branch', 'scheduleTime')->get();
    }

    public function get($id)
    {
    	return $this->model->where('id', $id)->with(['instructor' => function($query){
            $query->withTrashed();
        },'classes' => function($query){
            $query->withTrashed();
        }], 'branch', 'scheduleTime')->first();
    }

    public function delete($id)
    {
    	return $this->model->where('id', $id)->delete();
    }

    //extract all schedule within 30 days if empty until
    // @var until Carbon Instance
    public function extractAllScheduleTime($merchant_id, $until = null, $instructor_id = null)
    {
        $schedules = $this->getAll($merchant_id);
        $counter_date = ($until == null) ? Carbon::now()->addDays(30) : $until;
        $data = [];
        
        foreach ($schedules as $schedule) {
            $start_date = $schedule->start_date;
            
            if($schedule->is_repeat){
                $end_date = ($schedule->repeat_selection == "never") ? $counter_date->copy() : $schedule->end_date;
                $days = explode(',', $schedule->repeat_on);

                while ($counter_date->gte($start_date)) {
                    foreach ($days as $day) {
                        $date = $this->getNextDateBasedOnDay($day, $start_date);

                        if(($date->isFuture() || $date->isToday()) && $counter_date->gte($date) && $end_date->gte($date)){
                            $data[] = $this->distributeScheduleColumnData($schedule, $date);
                        }
                    }

                    $start_date->addWeeks($schedule->repeat_every);
                }
            } else {
                $data[] = $this->distributeScheduleColumnData($schedule, $start_date);
            }
        }

        $data = array_values(array_sort($data, function ($value) {
            return $value['start_date_time']->timestamp;
        }));

        return $data;
    }

    public function validateClassScheduleTime($request)
    {
        $start_date = Carbon::parse($request->repeat_start_date);
        $start_time = ($request->start_period == 'AM' || ($request->start_hr == 12 && $request->start_period == 'PM')) ? $request->start_hr . $request->start_min : ($request->start_hr . $request->start_min) + 1200;
        $end_time = ($request->end_period == 'AM' || ($request->end_hr == 12 && $request->end_period == 'PM')) ? $request->end_hr . $request->end_min : ($request->end_hr . $request->end_min) + 1200;
        $repeat_on = ($request->is_repeat != 1) ? [strtolower($start_date->format('l'))] : $request->repeat_on;
        $data = [];
        $error = "No error";

        list($counter_date, $schedules) = $this->getCounterDateAndSchedules($request);

        if(!empty($schedules)){
            while ($counter_date->gte($start_date)) {
                foreach ($repeat_on as $day) {
                    $date = $this->getNextDateBasedOnDay($day, $start_date);
                    $start_time_date = $this->addHrsAndMins($date, $start_time);
                    $end_time_date = $this->addHrsAndMins($date, $end_time);

                    foreach ($schedules as $schedule) {
                        if($this->isConfictOnInstructorSchedule($schedule, $start_time_date, $end_time_date, $request)){
                            return $this->generateErrorMessageIfConflictOnInstructorSchedule($schedule);
                        }
                    }
                }

                $start_date->addWeeks($request->repeat_every);
            }
        }
        
        return $error;
    }

    public function validateClassScheduleList($request)
    {
        $error = "No error";
        $start_date = Carbon::parse($request->repeat_start_date);
        $end_date = Carbon::parse($request->repeat_end_date);
        $repeat_on = ($request->is_repeat != 1) ? [strtolower($start_date->format('l'))] : $request->repeat_on;
        $start_time = ($request->start_period == 'AM' || ($request->start_hr == 12 && $request->start_period == 'PM')) ? $request->start_hr . $request->start_min : ($request->start_hr . $request->start_min) + 1200;
        $end_time = ($request->end_period == 'AM' || ($request->end_hr == 12 && $request->end_period == 'PM')) ? $request->end_hr . $request->end_min : ($request->end_hr . $request->end_min) + 1200;
        
        $is_repeat = $request->is_repeat;
        $repeat_end_selection = $request->repeat_end_selection;
        $instructor_id = $request->instructor_id;
        
        if($is_repeat != 1){
            $counter_date = $start_date->copy(); 
        } else if($repeat_end_selection == 'on' || $repeat_end_selection == 'after'){
            $counter_date = $end_date->copy();
        } else {
            $schedule = ClassScheduleModel::orderBy('start_date', 'desc')->orderBy('end_date', 'desc')->first();
            $counter_date = null;

            if(!empty($schedule)){
                $counter_date = ($schedule->end_date->gte($schedule->start_date)) ? $schedule->end_date : $schedule->start_date;
            } else {
                $counter_date =Carbon::today()->addDays(30);
            }
        }

        while ($counter_date->gte($start_date)) {
            foreach ($repeat_on as $day) {
                $date = $this->getNextDateBasedOnDay($day, $start_date);
                $start_time_date = $this->addHrsAndMins($date, $start_time);
                $end_time_date = $this->addHrsAndMins($date, $end_time);

                $conflicted_class_list = ClassScheduleTimeExtractModel::where('instructor_id', $instructor_id)->where('status', 'active')
                    ->where(function($query) use ($start_time_date, $end_time_date){
                        $query->where('start_date_time', '<=', $start_time_date)
                        ->where('end_date_time', '>', $start_time_date)
                        ->orWhere('start_date_time', '<', $end_time_date)
                        ->where('end_date_time', '>', $end_time_date)
                        ->orWhereBetween('start_date_time', [$start_time_date, $end_time_date]);
                    })->has('schedule')->get();

                if($conflicted_class_list->count() > 0){
                    return $error = 'Conflict on instructor schedule at ' . $start_date->toDateString() . ' ' . $this->convertMilitaryTime($start_time) . ' - ' . $this->convertMilitaryTime($end_time);
                }
            }

            $start_date->addWeeks($request->repeat_every);
        }
        
        return $error;
    }

    public function convertMilitaryTime($time){
        $suffix = ($time > 1159) ? 'PM' : 'AM';
        $converted_time = ($time > 1200) ? $time - 1200 : $time;
        $converted_time = str_pad($converted_time, 4, 0, STR_PAD_LEFT);

        return substr($converted_time , 0, 2) . ':' . substr($converted_time, 2, 2) . ' ' . $suffix;
    }

    private function getCounterDateAndSchedules($request)
    {
        $merchant_id = $request->merchant_id;
        $repeat_selection = $request->repeat_end_selection;
        $repeat_start_date = $request->repeat_start_date;
        $repeat_end_date = $request->repeat_end_date;
        $end_time = (int) $request->end_time;
        
        if($request->is_repeat != 1){
            $counter_date = Carbon::parse($repeat_start_date);
            $schedules = $this->extractAllScheduleTime($merchant_id, $counter_date);
            $counter_date->addHours((int)substr($end_time, 0, -2));
        } else if($repeat_selection == 'on' || $repeat_selection == 'after'){
            $counter_date = Carbon::parse($repeat_end_date);
            $schedules = $this->extractAllScheduleTime($merchant_id, $counter_date);
        } else {
            //never end
            $schedule = ClassScheduleModel::orderBy('start_date', 'desc')->orderBy('end_date', 'desc')->first();
            $counter_date = null;

            if(!empty($schedule)){
                $counter_date = ($schedule->end_date->gte($schedule->start_date)) ? $schedule->end_date : $schedule->start_date;
            }
            
            $schedules = $this->extractAllScheduleTime($merchant_id, $counter_date);
        }

        return [$counter_date, $schedules];
    }

    private function isConfictOnInstructorSchedule($schedule, $start_time, $end_time, $request)
    {
        return ((($start_time->lte($schedule['start_date_time']) && $end_time->gt($schedule['start_date_time'])) ||
            ($start_time->lt($schedule['end_date_time']) && $end_time->gte($schedule['end_date_time']))) ||
            (($schedule['start_date_time']->lte($start_time) && $schedule['end_date_time']->gt($start_time)) ||
            ($schedule['start_date_time']->lt($end_time) && $schedule['end_date_time']->gte($end_time))) &&
            $request->schedule_time_id != $schedule['id'] && $request->instructor_id == $schedule['instructor_id']);
    }

    private function generateErrorMessageIfConflictOnInstructorSchedule($schedule)
    {
        return 'Instructor schedule conflict on ' . $schedule['class_name'] . ' class at ' . $schedule['start_date_time']->toDateString() . ' ' . $schedule['time'];
    }

    private function distributeScheduleColumnData($schedule, $date)
    {
        $data['id'] = $schedule->id;
        $data['start_date_time'] = $this->addHrsAndMins($date, $schedule->start_time);
        $data['end_date_time'] = $this->addHrsAndMins($date, $schedule->end_time);
        $data['time'] = $schedule->start_time_label . ' - ' . $schedule->end_time_label;
        $data['class_name'] = $schedule->classes->name;
        $data['class_description'] = $schedule->classes->description;
        $data['instructor_id'] = $schedule->instructor->id;
        $data['instructor'] = $schedule->instructor->name;
        $data['branch'] = $schedule->branch;

        return $data;
    }

    private function getNextDateBasedOnDay($day, $date)
    {
        switch ($day) {
            case 'sunday':
                if($date->dayOfWeek != Carbon::SUNDAY){
                    $date = $date->copy()->next(Carbon::SUNDAY);
                }
                break;
            case 'monday':
                if($date->dayOfWeek != Carbon::MONDAY){
                    $date = $date->copy()->next(Carbon::MONDAY);
                }
                break;
            case 'tuesday':
                if($date->dayOfWeek != Carbon::TUESDAY){
                    $date = $date->copy()->next(Carbon::TUESDAY);
                }
                break;
            case 'wednesday':
                if($date->dayOfWeek != Carbon::WEDNESDAY){
                    $date = $date->copy()->next(Carbon::WEDNESDAY);
                }
                break;
            case 'thursday':
                if($date->dayOfWeek != Carbon::THURSDAY){
                    $date = $date->copy()->next(Carbon::THURSDAY);
                }
                break;
            case 'friday':
                if($date->dayOfWeek != Carbon::FRIDAY){
                    $date = $date->copy()->next(Carbon::FRIDAY);
                }
                break;
            case 'saturday':
                if($date->dayOfWeek != Carbon::SATURDAY){
                    $date = $date->copy()->next(Carbon::SATURDAY);
                }
                break;
        }

        return $date;
    }

    public function addHrsAndMins($date, $time){
        return $date->copy()->addHours((int)substr($time, 0, -2))->addMinutes((int)substr($time, -2));
    }
}
