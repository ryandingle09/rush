<?php

namespace Rush\Modules\ClassManagement\Repositories;

interface WithBranchInterface
{
	public function getAllWithBranch($merchant_id);
}