<?php

namespace Rush\Modules\ClassManagement\Repositories;

use \stdClass;
use DB;
use Illuminate\Database\Eloquent\Model;
use Rush\Modules\ClassManagement\Models\PackageModel;
use Rush\Modules\ClassManagement\Repositories\PackageInterface;
use Rush\Modules\Repository\AbstractRepository;

/**
 * A repository class would encapsulate each Eloquent Model class
 * and be responsible for CRUD operations on the database.
 */
class PackageRepository extends AbstractRepository implements PackageInterface
{
    protected $connection = 'mysql';

    public function __construct()
    {
        parent::__construct(new PackageModel);
    }

    public function save($inputs)
    {
        $package = isset($inputs['package_id']) ? $this->model->where('id', $inputs['package_id'])->first() : $this->model;

    	$package->name = $inputs['name'];
    	$package->type = $inputs['type'];
    	$package->no_of_visits = isset($inputs['no_of_visits']) ? $inputs['no_of_visits'] : 0;
    	$package->validity = $inputs['validity'];
    	$package->merchant_id = $inputs['merchant_id'];
        $package->multiple_stamp = $inputs['multiple_stamp'];

        if(isset($inputs['image_path'])){
            $package->image_path = $inputs['image_path'];
        }

        $package->save();

        $this->savePackageCategories($package, $inputs['category_ids']);

        return $this->savePackageBranches($package, $inputs['branch_ids']);
    }

    public function savePackageCategories($package, $categories){
        $package->categories()->delete();

        foreach ($categories as $category){
            $package->categories()->create([
                'class_package_category_list_id' => $category
            ]);
        }
    }

    public function savePackageBranches($packageModel, $branches)
    {
    	return $packageModel->branches()->sync($branches);
    }

    public function savePackageStamp($inputs)
    {
        $packageModel = $this->model->where('id', $inputs['package_id'])->first();
        
        $packageModel->stamps()->where('stamp_no', $inputs['stamp_no'])->delete();

        if($inputs['action'] == 'add'){
            $data['name'] = $inputs['name'];
            $data['details'] = $inputs['details'];
            $data['stamp_no'] = $inputs['stamp_no'];
            
            if(isset($inputs['image_path'])){
                $data['image_path'] = $inputs['image_path'];    
            }
            
            $packageModel->stamps()->create($data);
        }
    }

    public function getPackageStamp($package_id, $stamp_id)
    {
        return $this->model->where('id', $package_id)->first()->stamps()->where('stamp_no', $stamp_id)->first();
    }

    /**
     * @param int $package_id
     * @param string $column
     * @param mixed $value
     * @return null|\Rush\Modules\ClassManagement\Models\PackageStampModel
     */
    public function getPackageStampBy($package_id, $column, $value)
    {
        return $this->model->where('id', $package_id)->first()->stamps()->where($column, $value)->first();
    }

    public function getAll($merchant_id)
    {
    	return $this->model->where('merchant_id', $merchant_id)->with('branches', 'stamps')->get();
    }

    public function get($id)
    {
        return $this->model->where('id', $id)->with('branches', 'stamps')->first();
    }

    /**
     * @param string        $column
     * @param string        $value
     * @return null|\Rush\Modules\ClassManagement\Models\PackageModel
     */
    public function getBy($column, $value)
    {
        return $this->model->where($column, $value)->with('branches', 'stamps')->first();
    }

    public function delete($id)
    {
        return $this->model->where('id', $id)->delete();
    }

    /**
     * @param PackageModel          $package
     * @param \Carbon\Carbon        $start_date
     * @return \Carbon\Carbon
     */
    public function getExpirationDate(PackageModel $package, $start_date)
    {
        $start_date->hour = 0;
        $start_date->minute = 0;
        $start_date->second = 0;
        switch ($package->validity) {
            case '1 day':
                $expiration_date = $start_date;
                break;
            case '2 weeks':
                $expiration_date = $start_date->addWeeks(2)->subDay();
                break;
            case '1 month':
                $expiration_date = $start_date->addMonth()->subDay();
                break;
            case '3 months':
                $expiration_date = $start_date->addMonths(3)->subDay();
                break;
            case '6 months':
                $expiration_date = $start_date->addMonths(6)->subDay();
                break;
            default:
                $expiration_date = $start_date->addMonths(12)->subDay();
        }

        return $expiration_date;
    }
}
