<?php

namespace Rush\Modules\ClassManagement\Repositories;

/**
 * The InstructorInterface contains unique method signatures related to Package object
 */
interface InstructorInterface extends ClassManagementInterface
{
	public function saveClasses($instructorModel, $classes);
}
