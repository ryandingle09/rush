<?php
namespace Rush\Modules\ClassManagement\Repositories;

use Rush\Modules\ClassManagement\Models\ClassManagementToolsModulesModel;
use Rush\Modules\Repository\AbstractRepository;

class ClassManagementToolsModulesRepository extends AbstractRepository implements ClassManagementToolsModulesInterface
{

  public function __construct(ClassManagementToolsModulesModel $classManagementToolsModulesModel)
  {
    parent::__construct($classManagementToolsModulesModel);
  }

  public function getClassManagementToolsModules()
  {
    return $this->all();
  }
}
