<?php

namespace Rush\Modules\ClassManagement\Repositories;

use \stdClass;
use DB;
use Illuminate\Database\Eloquent\Model;
use Rush\Modules\ClassManagement\Models\ClassModel;
use Rush\Modules\Repository\AbstractRepository;

/**
 * A repository class would encapsulate each Eloquent Model class
 * and be responsible for CRUD operations on the database.
 */
class ClassRepository extends AbstractRepository implements ClassInterface, WithBranchInterface
{
    protected $connection = 'mysql';

    public function __construct()
    {
        parent::__construct(new ClassModel);
    }

    public function save($inputs)
    {
    	$class = isset($inputs['class_id']) ? $this->model->where('id', $inputs['class_id'])->first() : $this->model;

    	$class->name = $inputs['name'];
    	$class->description = $inputs['description'];
    	$class->merchant_id = $inputs['merchant_id'];

        if(isset($inputs['image_path'])){
            $class->image_path = $inputs['image_path'];
        }

        $class->save();
        
        if(isset($inputs['branch_ids'])){
            $this->saveBranches($class, $inputs['branch_ids']);
        }
    }

    public function saveBranches($class, $branches)
    {
        return $class->branches()->sync($branches);
    }

    public function getAll($merchant_id)
    {
    	return $this->model->where('merchant_id', $merchant_id)->doesntHave('branches')->with(['instructors' => function($query){
            $query->withTrashed();
        }])->get();
    }

    public function getAllWithBranch($merchant_id)
    {
        return $this->model->where('merchant_id', $merchant_id)->has('branches')->get();
    }

    public function get($id)
    {
    	return $this->model->where('id', $id)->withTrashed()->with(['instructors' => function($query){
            $query->withTrashed();
        }])->first();
    }

    public function delete($id)
    {
    	return $this->model->where('id', $id)->delete();
    }
}
