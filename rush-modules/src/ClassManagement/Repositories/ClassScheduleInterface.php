<?php

namespace Rush\Modules\ClassManagement\Repositories;

/**
 * The PackageInterface contains unique method signatures related to Package object
 */
interface ClassScheduleInterface extends ClassManagementInterface
{
	public function extractAllScheduleTime($merchant_id);
	public function validateClassScheduleTime($request);
	public function validateClassScheduleList($request);
}
