<?php

namespace Rush\Modules\ClassManagement\Repositories;

use \stdClass;
use DB;
use Illuminate\Database\Eloquent\Model;
use Rush\Modules\ClassManagement\Models\InstructorModel;
use Rush\Modules\Repository\AbstractRepository;

/**
 * A repository class would encapsulate each Eloquent Model class
 * and be responsible for CRUD operations on the database.
 */
class InstructorRepository extends AbstractRepository implements InstructorInterface, WithBranchInterface
{
    protected $connection = 'mysql';

    public function __construct()
    {
        parent::__construct(new InstructorModel);
    }

    public function save($inputs)
    {
    	$instructor = isset($inputs['instructor_id']) ? $this->model->where('id', $inputs['instructor_id'])->first() : $this->model;

    	$instructor->name = $inputs['name'];
    	$instructor->merchant_id = $inputs['merchant_id'];
        $instructor->description = $inputs['description'];

        if(isset($inputs['image_path'])){
            $instructor->image_path = $inputs['image_path'];
        }

        $instructor->save();

        if(isset($inputs['class_ids'])){
            $this->saveClasses($instructor, $inputs['class_ids']);
        }
        
        if(isset($inputs['branch_ids'])){
            $this->saveBranches($instructor, $inputs['branch_ids']);
        }
    }

    public function saveClasses($instructorModel, $classes)
    {
    	return $instructorModel->classes()->sync($classes);
    }

    public function saveBranches($instructorModel, $branches)
    {
        return $instructorModel->branches()->sync($branches);
    }

    public function getAll($merchant_id)
    {
    	return $this->model->where('merchant_id', $merchant_id)->has('classes')->get();
    }

    public function getAllWithBranch($merchant_id)
    {
        return $this->model->where('merchant_id', $merchant_id)->has('branches')->doesntHave('classes')->get();
    }

    public function get($id)
    {
    	return $this->model->where('id', $id)->first();
    }

    public function delete($id)
    {
    	return $this->model->where('id', $id)->delete();
    }
}
