<?php

namespace Rush\Modules\ClassManagement\Repositories;

use Rush\Modules\Repository\RepositoryInterface;

/**
 * The PackageInterface contains unique method signatures related to Package object
 */
interface ClassManagementInterface extends RepositoryInterface
{
	public function save($inputs);
	public function getAll($merchant_id);
	public function get($id);
	public function delete($id);
}
