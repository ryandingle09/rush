<?php

namespace Rush\Modules\ClassManagement\Repositories;

use \stdClass;
use DB;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Rush\Modules\ClassManagement\Models\ClassScheduleReservationModel;
use Rush\Modules\ClassManagement\Models\ClassScheduleTimeExtractModel;
use Rush\Modules\Repository\AbstractRepository;
use Rush\Modules\Customer\Models\CustomerModel;

/**
 * A repository class would encapsulate each Eloquent Model class
 * and be responsible for CRUD operations on the database.
 */
class ClassScheduleReservationRepository extends AbstractRepository implements ClassScheduleReservationInterface
{
    protected $connection = 'mysql';

    public function __construct()
    {
        parent::__construct(new ClassScheduleReservationModel);
    }

    public function registerCustomerToClassUsingCMS(CustomerModel $customer, $class_list_id)
    {
    	$input['class_list_id'] = $class_list_id;
    	$input['customer_id'] = $customer->customerId;
    	$input['registration_type'] = ClassScheduleReservationModel::REG_TYPE_CMS;
    	$input['showed_up'] = false;

    	$id = $this->model->create($input)->id;

    	return $this->model->with('customer', 'customer_package.package')->where('id', $id)->first();
    }

    public function getAllCustomerFromClass($class_list_id)
    {
    	return $this->model->where('class_list_id', $class_list_id)->orderBy('id')->with('customer', 'customer_package.package')->get();
    }

    public function deleteReservation($reservation_id)
    {
    	return $this->model->where('id', $reservation_id)->delete();
    }

    public function confirmAttendanceReservation($reservation_id, $toggle)
    {
    	$this->model->where('id', $reservation_id)->update(['showed_up' => ($toggle == "true")]);

    	return ['showed_up' => $toggle];
    }

    public function checkIfCustomerAlreadySignedUp(CustomerModel $customer, $class_list_id)
    {
    	$reservation = $this->model->where('class_list_id', $class_list_id)->where('customer_id', $customer->customerId);

    	return ($reservation->count() > 0);
    }

    public function checkIfConflictOnCustomerSchedule(CustomerModel $customer, $class_list)
    {

    	$conflicted_class_list = $this->model->where('customer_id', $customer->customerId)->whereHas('class_list', function($query) use ($class_list) {
    		$query->where('start_date_time', '<=', $class_list->start_date_time)
    				->where('end_date_time', '>', $class_list->start_date_time)
    				->orWhere('start_date_time', '<', $class_list->end_date_time)
    				->where('end_date_time', '>', $class_list->end_date_time)
                    ->orWhereBetween('start_date_time', [$class_list->start_date_time, $class_list->end_date_time]);
    	})->get();

    	return ($conflicted_class_list->count() > 0);
    }

    public function checkIfClassListIsFull($class_list)
    {

    	return ($this->model->where('class_list_id', $class_list->id)->count() >= $class_list->attendance_capacity);
    }

    public function checkIfClassIsCancelled($class_list)
    {
        return ($class_list->status == 'cancelled');
    }
}
