<?php

namespace Rush\Modules\Merchant\Repositories;

use Rush\Modules\Merchant\Models\MembershipBranchModel;
use Rush\Modules\Merchant\Repositories\MembershipBranchInterface;
use Rush\Modules\Repository\AbstractRepository;

/**
 * A repository class would encapsulate each Eloquent Model class
 * and be responsible for CRUD operations on the database.
 */
class MembershipBranchRepository extends AbstractRepository implements MembershipBranchInterface
{
    public function __construct(MembershipBranchModel $membershipBranchModel)
    {
        parent::__construct($membershipBranchModel);
    }

    public function getByMembershipBranch($membershipId, $branchId)
    {
        return $this->model->where(['membership_id' => $membershipId, 'branch_id' => $branchId])->first();
    }
}
