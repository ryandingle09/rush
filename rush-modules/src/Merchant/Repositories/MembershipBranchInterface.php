<?php

namespace Rush\Modules\Merchant\Repositories;

use Rush\Modules\Repository\RepositoryInterface;

/**
 * The MembershipBranchInterface contains unique method signatures related to MembershipBranch object
 */
interface MembershipBranchInterface extends RepositoryInterface
{
    public function getByMembershipBranch($membershipId, $branchId);
}
