<?php

namespace Rush\Modules\Merchant\Repositories;

use Rush\Modules\Repository\RepositoryInterface;

interface EmployeeInterface extends RepositoryInterface
{
    public function getMerchantSystemEmployee($merchantId, $username);
}