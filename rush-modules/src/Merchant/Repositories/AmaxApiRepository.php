<?php
namespace Rush\Modules\Merchant\Repositories;

use Rush\Modules\Merchant\Models\AmaxApiCredentialsModel;
use Ixudra\Curl\CurlService;

class AmaxApiRepository
{
    
    private $credential;
    private $base_url = 'https://devapi.globelabs.com.ph/';
    private $redeem_uri = 'rewards/v1/transactions/send';
    
    public function __construct(){
        $this->credential = AmaxApiCredentialsModel::first();
    }
    
    public function redeemReward( $address, $promo )
    {
        if( ! $this->credential ) throw new \Exception('Amax Credential not set');
        
        $endpoint = $this->base_url . $this->redeem_uri;
        $curlService = new CurlService();
        
        $send = $curlService->to( $endpoint )
            ->withData([
                'outboundRewardRequest' => [
                    'app_id' => $this->credential->app_id,
                    'app_secret' => $this->credential->app_secret,
                    'rewards_token' => $this->credential->rewards_token,
                    'address' => $address,
                    'promo' => $promo
                ]
            ])
            ->asJson( true )
            ->post();

        return $send;
    }
}