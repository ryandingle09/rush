<?php

namespace Rush\Modules\Merchant\Repositories;

use Rush\Modules\Merchant\Models\MembershipModel;
use Rush\Modules\Merchant\Repositories\MembershipInterface;
use Rush\Modules\Repository\AbstractRepository;

/**
 * A repository class would encapsulate each Eloquent Model class
 * and be responsible for CRUD operations on the database.
 */
class MembershipRepository extends AbstractRepository implements MembershipInterface
{
    public function __construct(MembershipModel $membershipModel)
    {
        parent::__construct($membershipModel);
    }

    public function getByMerchant($merchantId)
    {
        return $this->model->where(['merchant_id' => $merchantId])->get();
    }

    public function getListByMerchant($merchantId)
    {
        return $this->model->where(['merchant_id' => $merchantId])->get()->lists('name', 'id');
    }

    public function getLevelsByMerchant($merchantId)
    {
        return $this->model->where(['merchant_id' => $merchantId])->get()->lists('level');
    }
}
