<?php

namespace Rush\Modules\Merchant\Repositories;

use Rush\Modules\Repository\RepositoryInterface;

/**
 * The MembershipInterface contains unique method signatures related to Membership object
 */
interface MembershipInterface extends RepositoryInterface
{
    public function getByMerchant($merchantId);

    public function getListByMerchant($merchantId);

    public function getLevelsByMerchant($merchantId);
}
