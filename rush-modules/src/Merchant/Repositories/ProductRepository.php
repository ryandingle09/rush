<?php

namespace Rush\Modules\Merchant\Repositories;

use Illuminate\Support\Facades\DB;
use Rush\Modules\Repository\AbstractRepository;
use Rush\Modules\Merchant\Models\ProductsModel;
use Rush\Modules\Merchant\Models\ProductStampModel;
use Rush\Modules\Merchant\Models\ProductTransactionHistoryModel;

class ProductRepository extends AbstractRepository implements ProductInterface
{
    protected $product_transaction_history_model;

    public function __construct(ProductsModel $products_model, ProductTransactionHistoryModel $product_transaction_history_model)
    {
        parent::__construct($products_model);
        $this->product_transaction_history_model = $product_transaction_history_model;
    }

    public function getAll($merchant_id){
        return $this->model->where('merchant_id', $merchant_id)->get();
    }

    public function store($product_data)
    {
        $product = new $this->model;

        $product->code = $product_data['code'];
        $product->name = $product_data['name'];
        $product->amount = $product_data['amount'];
        $product->points = $product_data['points'];
        $product->branch_ids = $product_data['branch_ids'];
        $product->merchant_id = $product_data['merchant_id'];
        $product->updated_by = $product_data['merchant_id'];

        $product->save();

        if(isset($product_data['stamp'])){
            $product->stamps()->delete();

            foreach($product_data['stamp']['name'] as $key => $value){
                $product->stamps()->create([
                    'stamp_name' => $product_data['stamp']['name'][$key],
                    'stamp_number' => $product_data['stamp']['number'][$key],
                ]);
            }
        }

        return true;
    }

    public function updateProduct($product_id, $product_data)
    {
        $product = $this->model->where('id', $product_id)->first();

        $product->code = $product_data['code'];
        $product->name = $product_data['name'];
        $product->amount = $product_data['amount'];
        $product->points = (int) $product_data['points'];
        $product->branch_ids = $product_data['branch_ids'];
        $product->merchant_id = $product_data['merchant_id'];
        $product->updated_by = $product_data['merchant_id'];

        $product->save();

        if(isset($product_data['stamp'])){
            $product->stamps()->delete();

            foreach($product_data['stamp']['name'] as $key => $value){
                $product->stamps()->create([
                    'stamp_name' => $product_data['stamp']['name'][$key],
                    'stamp_number' => $product_data['stamp']['number'][$key],
                ]);
            }
        }

        return true;
    }

    public function getAllProductTransaction($merchant_id = 0, $month = null)
    {
        if(!empty($month)){
            return $this->product_transaction_history_model->where('merchant_id', $merchant_id)
                ->whereMonth('created_at', '=', date('m', $month))
                ->whereYear('created_at', '=', date('Y', $month))->get();
        }

        return $this->product_transaction_history_model->where('merchant_id', $merchant_id)->get();
    }

    public function getProductTransactionOfCustomerAgeBracket($merchant_id = 0, $age_bracket = "not_set", $month = null){
        $data = [];

        if(!empty($month)){
            $production_transaction_history_model = $this->product_transaction_history_model
                ->where('merchant_id', $merchant_id)
                ->whereMonth('created_at', '=', date('m', $month))
                ->whereYear('created_at', '=', date('Y', $month));
        } else {
            $production_transaction_history_model = $this->product_transaction_history_model->where('merchant_id', $merchant_id);
        }

        switch ($age_bracket){
            case "not_set" :
                $male = $production_transaction_history_model
                    ->select('*', DB::raw('count(*) as total'))
                    ->whereHas('customer', function($query){
                        $query->whereNull('birthDate');
                    })->groupBy('product_id')
                    ->orderBy('total', 'desc')
                    ->get();
                $female = collect([]);

                $data['male'] = $male;
                $data['female'] = $female;
                break;

            case "18_24" :
                list($data['male'], $data['female']) = $this->queryTransactionAgeGroup(18, 24, $production_transaction_history_model);
                break;
            case "25_34" :
                list($data['male'], $data['female']) = $this->queryTransactionAgeGroup(25, 34, $production_transaction_history_model);
                break;
            case "35_44" :
                list($data['male'], $data['female']) = $this->queryTransactionAgeGroup(35, 44, $production_transaction_history_model);
                break;
            case "45_up" :
                list($data['male'], $data['female']) = $this->queryTransactionAgeGroup(45, 100, $production_transaction_history_model);
                break;
        }

        return $data;
    }

    private function queryTransactionAgeGroup($age_group_from = 18, $age_group_to = 24, $transaction_model){
        $production_transaction_male = $transaction_model;
        $production_transaction_female = clone $transaction_model;

        $male = $production_transaction_male
            ->select('*', DB::raw('count(*) as total'))
            ->whereHas('customer', function($query) use ($age_group_from, $age_group_to){
                $query->whereBetween(DB::raw('TIMESTAMPDIFF(YEAR, Customer.birthDate, CURDATE())'), [$age_group_from, $age_group_to])
                    ->whereIn('gender', ['Male', 'male', 'M','m']);
            })->groupBy('product_id')
            ->orderBy('total', 'desc')
            ->get();

        $female = $production_transaction_female
            ->select('*', DB::raw('count(*) as total'))
            ->whereHas('customer', function($query) use ($age_group_from, $age_group_to){
                $query->whereBetween(DB::raw('TIMESTAMPDIFF(YEAR, Customer.birthDate, CURDATE())'), [$age_group_from, $age_group_to])
                    ->whereIn('gender', ['Female', 'female', 'F','f']);
            })->groupBy('product_id')
            ->orderBy('total', 'desc')
            ->get();

        return [$male, $female];
    }

    public function delete($product_id){
        $product = $this->getById($product_id);

        $product->stamps()->delete();

        return $product->delete();
    }

    public function getProductStamp($stamp_id)
    {
        $stamp = ProductStampModel::where(function($query) use ($stamp_id) {
                        if (is_numeric($stamp_id)) {
                            $query->where('id', $stamp_id);
                        } else {
                            $query->where('uuid', $stamp_id);
                        }
                    })->first();
        return $stamp;
    }
}