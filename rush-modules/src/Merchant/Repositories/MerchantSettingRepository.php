<?php

namespace Rush\Modules\Merchant\Repositories;

use Rush\Modules\Merchant\Models\MerchantSettingModel;
use Rush\Modules\Merchant\Repositories\MerchantSettingInterface;
use Rush\Modules\Repository\AbstractRepository;

/**
 * A repository class would encapsulate each Eloquent Model class
 * and be responsible for CRUD operations on the database.
 */
class MerchantSettingRepository extends AbstractRepository implements MerchantSettingInterface
{
    public function __construct(MerchantSettingModel $merchantSettingModel)
    {
        parent::__construct($merchantSettingModel);
    }

    public function getByMerchantId($merchantId)
    {
        return $this->model->where('merchant_id', $merchantId)->first();
    }
}
