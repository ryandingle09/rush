<?php
namespace Rush\Modules\Merchant\Repositories;

use Carbon\Carbon;
use Config;
use DB;
use Rush\Modules\Admin\Models\AdminRewardsModel;
use Rush\Modules\Admin\Models\BroadcastToolsManagementModel;
use Rush\Modules\Admin\Models\BroadcastToolsModulesModel;
use Rush\Modules\Customer\Models\CustomerClassPackageModel;
use Rush\Modules\Customer\Models\CustomerModel;
use Rush\Modules\Customer\Models\CustomerStampsCardModel;
use Rush\Modules\Customer\Models\CustomerTransactionsModel;
use Rush\Modules\Helpers\Facades\StorageHelper;
use Rush\Modules\Merchant\Models\BcuModulesModel;
use Rush\Modules\Merchant\Models\ClientModel;
use Rush\Modules\Merchant\Models\LoyaltyCardOrderModel;
use Rush\Modules\Merchant\Models\MenuItemCategoryModel;
use Rush\Modules\Merchant\Models\MenuItemModel;
use Rush\Modules\Merchant\Models\MerchantBranchesModel;
use Rush\Modules\Merchant\Models\MerchantEmployeesModel;
use Rush\Modules\Merchant\Models\MerchantGlobeRewardsModel;
use Rush\Modules\Merchant\Models\MerchantModel;
use Rush\Modules\Merchant\Models\MerchantModules;
use Rush\Modules\Merchant\Models\MerchantPackageModel;
use Rush\Modules\Merchant\Models\MerchantRewardsFactoryModel;
use Rush\Modules\Merchant\Models\MerchantRewardsModel;
use Rush\Modules\Merchant\Models\MerchantSettingModel;
use Rush\Modules\Merchant\Models\MerchantSubUsersModel;
use Rush\Modules\Merchant\Models\MerchantSubUsersModulesModel;
use Rush\Modules\Merchant\Models\MerchantVotesModel;
use Rush\Modules\Merchant\Models\MindbodyClassModel;
use Rush\Modules\Merchant\Models\MindbodyInstructorModel;
use Rush\Modules\Merchant\Models\MindbodyScheduleModel;
use Rush\Modules\Merchant\Models\ModulesMerchantModel;
use Rush\Modules\Merchant\Models\OrderModel;
use Rush\Modules\Merchant\Models\PointsSeedingBatchModel;
use Rush\Modules\Merchant\Models\PointsSeedingDataModel;
use Rush\Modules\Merchant\Models\QRCodeModel;
use Rush\Modules\Merchant\Models\QuizQuestionChoiceModel;
use Rush\Modules\Merchant\Models\QuizQuestionModel;
use Rush\Modules\Merchant\Models\VoucherCodeModel;
use Rush\Modules\Merchant\Models\VoucherModel;
use Rush\Modules\Merchant\Models\PointsPromoModel;
use Rush\Modules\Merchant\Models\MerchantCodeModel;
use Rush\Modules\Merchant\Models\MerchantCityModel;
use Rush\Modules\Posts\Models\CategoryModel;
use Rush\Modules\Repository\AbstractRepository;
use Rush\Modules\Merchant\Models\MerchantPromoMechanicsModel;
use Rush\Modules\Merchant\Models\MerchantPunchCardRewardsModel;
use Rush\Modules\Merchant\Models\MerchantPromoAmountStampModel;
/**
 * Class MerchantRepository
 * @package Rush\Modules\Merchant\Repositories
 */
class MerchantRepository extends AbstractRepository implements MerchantInterface
{

    public $basicColumns = array(
        'merchantid as id',
        'businessName as business_name',
        'address',
        'personName as person_name',
        'personPosition as person_position',
        'contactPersonEmail as contact_person_email',
        'contactPersonNumber as contact_person_number',
    );

    /**
     * @param string $slug
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function getBySlug($slug)
    {
        return $this->model->where('business_system_link', $slug)->first();
    }

    /**
     * MerchantRepository constructor.
     * @param MerchantModel $merchantModel
     */
    public function __construct(MerchantModel $merchantModel)
    {
        parent::__construct($merchantModel);
    }

    public function getBasicDetails( $id ){

        $response['error_code'] = '0x0';
        $response['data'] = $this->model->select( $this->basicColumns )->find( $id );

        return $response;

    }

    public function returnBasicDetailsErrors( $errors ){

        $response['error_code'] = '0x1';
        $response['errors'] = $errors;

        return $response;

    }

    public function updateBasicDetails( $id, $request ){

        $merchant = MerchantModel::find( $id );
        $merchant->businessName = $request->business_name;
        $merchant->address = $request->address;
        $merchant->personName = $request->person_name;
        $merchant->personPosition = $request->person_position;
        $merchant->contactPersonEmail = $request->contact_person_email;
        $merchant->contactPersonNumber = $request->contact_person_number;
        $merchant->save();

        $response['error_code'] = '0x0';
        $response['data'] = $request->all();
        $response['data']['id'] = $id;

        return $response;

    }

    /**
     * @param  string $username
     * @return MerchantModel
     */
    public function getByUsername($username)
    {
        return $this->model->where('username', $username)->first();
    }

    public function getMerchantPackage($id, $type)
    {
        return MerchantPackageModel::where( [ 'merchantId' => $id, 'designType' => $type ] )->first();
    }

    public function updateMerchantPackage( $id, $data, $type )
    {
        $merchant_package = MerchantPackageModel::where( [ 'merchantId' => $id, 'designType' => $type ] );
        if ( $merchant_package && $merchant_package->count() > 0 ) {
            $merchant_package = $merchant_package->first();
            foreach( $data as $key => $value ) {
                $merchant_package->$key = $value;
            }
        } else {
            $merchant = $this->model->find($id);
            $merchant_package = new MerchantPackageModel();
            $merchant_package->logoURL = isset($data['logoURL']) ? $data['logoURL'] : null;
            $merchant_package->backgroundURL = isset($data['backgroundURL']) ? $data['backgroundURL'] : null;
            $merchant_package->backgroundColorHEX = isset($data['backgroundColorHEX']) ? $data['backgroundColorHEX'] : null;
            $merchant_package->designType = $type;
            $merchant_package->merchantId = $id;
            $merchant_package->packageId = $merchant->packageId;
        }

        return $merchant_package->save();
    }

    public function generateLoyaltyCardOrder( $id, $data )
    {
      $order = new LoyaltyCardOrderModel();
      $quantity = $data->input('quantity');
      $order->merchant_id = $id;
      $order->quantity = $data->quantity;
      $order->total = 50 * $quantity;
      $order->status = "Pending";
      return $order->save();
    }

    public function getPointsConversion( $merchant_id, $branch = null, $customer = null, $membership_branch = null )
    {
        // check customer
        if($customer) {
           // get customer membership card
           if($membership_card = $customer->membership_card) {
                // get conversion setting base on membership
                if($membership_card->membership->conversion_setting) {
                    // set conversion setting
                    $conversion_setting = $membership_card->membership->conversion_setting;
                }
           }
        }

        if($branch) {
            if(isset($customer) && isset($membership_card)) {
                // get membership branch conversion setting
                if($membership_branch) {
                    if($membership_branch->conversion_setting) {
                        $conversion_setting = $membership_branch->conversion_setting;
                    }
                } else {
                  // get branch conversion setting if any
                  if($branch->conversion_setting) {
                      $conversion_setting  = $branch->conversion_setting;
                  }
                }
            } else {
                // get branch conversion setting if any
                if($branch->conversion_setting) {
                    $conversion_setting  = $branch->conversion_setting;
                }
            }
        }

       // default to merchant setting if no specific conversion setting
        if(! isset($conversion_setting) ) {
            $merchant = $this->model->find($merchant_id);
            $conversion_setting = $merchant->conversion_setting;
        }

        return $conversion_setting;
    }

    public function getCatalog( $merchant_id, $customer_uuid = null ){

        $customer_mobile_network = '';

        if( ! is_null($customer_uuid) ) $customer = CustomerModel::where('uuid', $customer_uuid)->first();
        if( isset($customer) ) $customer_mobile_network = $customer->mobile_network;

        $rewards = [];
        $temp_rewards = MerchantRewardsModel::where('merchantid', $merchant_id)->where('status', 1)->orderBy('pointsRequired')->get();
        $globe_rewards = MerchantGlobeRewardsModel::where('merchantid', $merchant_id)->where('status', 1)->orderBy('pointsRequired')->get();

        foreach ($temp_rewards as $reward) {
            $rewards[] = [
                'id'                => $reward->uuid,
                'name'              => $reward->name,
                'details'           => $reward->details,
                'points_required'   => (float) $reward->pointsRequired,
                'image_url'         => $reward->imageURL ? StorageHelper::repositoryUrlForView($reward->imageURL, 1) : null
            ];
        }

        foreach ($globe_rewards as $reward) {
            if( in_array($customer_mobile_network, (array) json_decode($reward->type)) ){
                $rewards[] = [
                    'id'                => $reward->uuid,
                    'name'              => $reward->name,
                    'details'           => $reward->details,
                    'points_required'   => (float) $reward->pointsRequired,
                    'image_url'         => $reward->imageURL ? StorageHelper::repositoryUrlForView($reward->imageURL, 1) : null
                ];
            }
        }

        $response['error_code'] = '0x0';
        $response['data'] = $rewards;

        return $response;

    }

    public function getGenericCatalog( $merchant_id, $customer = null ){

        $rewards_factory = new MerchantRewardsFactoryModel( $merchant_id );
        $rewards_catalog = $rewards_factory->getRewardsCatalog( $customer );

        return $rewards_catalog;

    }

    public function adminUpdateMerchant( $merchant_id, $data )
    {
      $merchant = MerchantModel::find( $merchant_id );
      $merchant->status = $data['status'];
      $merchant->trial_end = Carbon::parse( $data['trial_date_end'] );
      $merchant->businessName = $data['businessName'];
      $merchant->personName = $data['contactPerson'];
      $merchant->contactPerson = $data['contactPerson'];
      $merchant->contactPersonNumber = $data['contactPersonNumber'];
      $merchant->contactPersonEmail = $data['contactPersonEmail'];
      $merchant->contactPersonEmail = $data['contactPersonEmail'];
      $merchant->packageId = $data['package_availed'];
      $merchant->address = $data['address'];
      $merchant->street1 = ( $data['street1'] != '' ? $data['street1'] : null ) ;
      $merchant->street2 = ( $data['street2'] != '' ? $data['street2'] : null );
      $merchant->TIN = ( $data['tin_number'] != '' ? $data['tin_number'] : null );
      $merchant->billingName = ( $data['billing_name'] != '' ? $data['billing_name'] : null );
      $fee = 0;
      if ( $merchant->packageId == 1 ) {
        $fee = 1.5;
      } else {
        $fee = 1299;
      }
      $merchant->auto_update_status = ( isset($data['auto_update_status']) ) ? TRUE : FALSE;
      if ( isset($data['monthly_service_fee']) ) {
        $msf = $data['monthly_service_fee'] ? $data['monthly_service_fee'] : $fee;
        $merchant->monthly_service_fee = $msf;
      }
      $merchant->enabled = $data['enabled'];

      if ( !isset($data['business_type']) && isset($data['business_type_other']) ) {
        $merchant->businessType = $data['business_type_other'];
      } else {
        $business_type = '';
        foreach( $data['business_type'] as $type ) {
          $business_type .= '|' . $type;
        }
        $merchant->businessType = $business_type;
      }
      $merchant->sms_transactions_charge = ( isset( $data['sms_transactions_charge'] ) && $data['sms_transactions_charge'] > 0 ) ? $data['sms_transactions_charge'] : "0.50";
      $merchant_settings = MerchantSettingModel::where('merchant_id', $merchant_id);
      if ( $merchant_settings->count() > 0 ) {
        $merchant_settings = $merchant_settings->first();
        $merchant_settings->vanity_url = $data['vanity_url'];
        $merchant_settings->allow_registration = ( isset($data['allow_registration']) ? $data['allow_registration'] : 1 );
        $merchant_settings->voucher_module = ( isset($data['voucher_module']) ? $data['voucher_module'] : 0 );
        $merchant_settings->code_management = ( isset($data['code_management']) ? $data['code_management'] : 0 );
        $merchant_settings->enable_feedback_category = ( isset($data['enable_feedback_category']) ? $data['enable_feedback_category'] : 0 );
        $merchant_settings->add_member = ( isset($data['add_member']) ? $data['add_member'] : 0 );
        $merchant_settings->billing_cms_tool_fee = ( isset($data['billing_cms_tool_fee']) ? $data['billing_cms_tool_fee'] : 0 );
        $merchant_settings->customer_login_param = ( isset($data['customer_login_param']) ? $data['customer_login_param'] : 'mobile' );
        $merchant_settings->maintenance = ( isset($data['maintenance']) ? $data['maintenance'] : false );
        $merchant_settings->maintenance_text = ( isset($data['maintenance_text']) ? $data['maintenance_text'] : "Please contact customer support." );
        $merchant_settings->toggle_branch_feature = ( isset($data['toggle_branch_feature']) ? $data['toggle_branch_feature'] : false );
        $merchant_settings->accrued_billing = ( isset($data['accrued_billing']) ? $data['accrued_billing'] : true );
        $merchant_settings->save();
      }

      $merchant->reseller_id = ( $data['reseller'] != '' ? $data['reseller'] : null ) ;
      $merchant->reseller_email = ( $data['reseller_email'] != '' ? $data['reseller_email'] : null );

      $merchant->msf_type = ( $data['msf_type'] != '' ? $data['msf_type'] : 'fixed' );
      $merchant->branch_count = ( isset($data['branch_count']) && $data['branch_count'] != 0 ? $data['branch_count'] : null );
      $merchant->bcu_enabled = ( isset($data['bcu_enabled']) ? $data['bcu_enabled'] : 0 );
      $merchant->new_billing_calculation = ( isset($data['new_billing_calculation']) ? $data['new_billing_calculation'] : 0 );

      if ( $data['new_billing_calculation'] ) {
        $tier1 = [ 'msf' => ( isset($data['msf_price_tier1']) && !empty($data['msf_price_tier1']) ? $data['msf_price_tier1'] : 15000 ), 'sms' => ( isset($data['sms_price_tier1']) && !empty($data['sms_price_tier1']) ? $data['sms_price_tier1'] : 2500 ), 'label' => ( isset($data['label_price_tier1']) && !empty($data['label_price_tier1']) ? $data['label_price_tier1'] : 'Member: 1 - 10,000' ) ];
        $tier2 = [ 'msf' => ( isset($data['msf_price_tier2']) && !empty($data['msf_price_tier2']) ? $data['msf_price_tier2'] : 35000 ), 'sms' => ( isset($data['sms_price_tier2']) && !empty($data['sms_price_tier2']) ? $data['sms_price_tier2'] : 6250 ), 'label' => ( isset($data['label_price_tier2']) && !empty($data['label_price_tier2']) ? $data['label_price_tier2'] : 'Member: 10,001 - 25,000' ) ];
        $tier3 = [ 'msf' => ( isset($data['msf_price_tier3']) && !empty($data['msf_price_tier3']) ? $data['msf_price_tier3'] : 60000 ), 'sms' => ( isset($data['sms_price_tier3']) && !empty($data['sms_price_tier3']) ? $data['sms_price_tier3'] : 12500 ), 'label' => ( isset($data['label_price_tier3']) && !empty($data['label_price_tier3']) ? $data['label_price_tier3'] : 'Member: 25,001 - 50,000' ) ];
        $tier4 = [ 'msf' => ( isset($data['msf_price_tier4']) && !empty($data['msf_price_tier4']) ? $data['msf_price_tier4'] : 80000 ), 'sms' => ( isset($data['sms_price_tier4']) && !empty($data['sms_price_tier4']) ? $data['sms_price_tier4'] : 20000 ), 'label' => ( isset($data['label_price_tier4']) && !empty($data['label_price_tier4']) ? $data['label_price_tier4'] : 'Member: 50,001 - 100,000' ) ];
        $tier5 = [ 'msf' => ( isset($data['msf_price_tier5']) && !empty($data['msf_price_tier5']) ? $data['msf_price_tier5'] : 100000 ), 'sms' => ( isset($data['sms_price_tier5']) && !empty($data['sms_price_tier5']) ? $data['sms_price_tier5'] : 25000 ), 'label' => ( isset($data['label_price_tier5']) && !empty($data['label_price_tier5']) ? $data['label_price_tier5'] : 'Member: 100,001 and up' ) ];

        $billing_tiers = [
            'tier1' => $tier1,
            'tier2' => $tier2,
            'tier3' => $tier3,
            'tier4' => $tier4,
            'tier5' => $tier5
          ];
        $merchant->new_billing_tier_options = json_encode( $billing_tiers );
      } else {
        $merchant->new_billing_tier_options = null;
      }

      if ( isset( $data['add-ons-desc'] ) ) {
        $billing_add_ons = [];
        $add_ons_count = count($data['add-ons-desc']);
        for( $x = 0; $x < $add_ons_count; $x++ ) {
          $billing_add_ons[] = array(
              'description' => $data['add-ons-desc'][$x],
              'quantity' => $data['add-ons-quantity'][$x],
              'amount' => $data['add-ons-amount'][$x],
              'vat' => $data['add-ons-vat'][$x]
            );
        }

        $merchant->billing_add_ons = json_encode( $billing_add_ons );
      }
      return $merchant->save();
    }

    public function toggleMerchantEnableDisable( $merchant_id, $value )
    {
      $merchant = MerchantModel::find( $merchant_id );
      $merchant->enabled = $value;
      return $merchant->save();
    }

    public function checkIfExists($merchant_id)
    {
      $merchant = MerchantModel::find( $merchant_id );

      if ( $merchant ) return TRUE;
      else return FALSE;
    }

    public function getPointsTransferSettings( $merchant_id ){

        $merchant = $this->model->find($merchant_id);

        $response['error_code'] = '0x0';
        $response['data'] = (int) $merchant->settings->points_transfer;

        return $response;

    }

    public function getPosSyncSettings( $merchant_id ){

        $merchant = $this->model->find($merchant_id);

        $response['error_code'] = '0x0';
        $response['data'] = $merchant->pos_sync_settings;

        return $response;

    }

    public function changeMerchantStatus( $merchant_id, $status = 'Full')
    {
      $merchant = MerchantModel::find( $merchant_id );
      $merchant->status = $status;
      return $merchant->save();
    }

    public function getMerchantGlobeReward( $uuid, $merchant_id )
    {
        $reward = MerchantGlobeRewardsModel::where('uuid', $uuid)->where('merchantid', $merchant_id)->first();
        return $reward;
    }

    public function saveMerchantRewards( $merchant_id, $item )
    {
      if ( $item['redeemItemId'] ) $reward = MerchantRewardsModel::find( $item['redeemItemId'] );
      else $reward = new MerchantRewardsModel();
      $reward->name = $item['name'];
      $reward->branch_ids = $item['branch_ids'];
      $reward->details = $item['details'];
      $reward->pointsRequired = $item['pointsRequired'];
      $reward->timestamp = $item['timestamp'];
      $reward->merchantid = $item['merchantid'];
      $reward->status = $item['status'];
      if (isset($item['imageURL'])) {
        if ( $reward->imageURL && file_exists( base_path() . '/..' . $reward->imageURL ) ) unlink( base_path() . '/..' . $reward->imageURL);
        $reward->imageURL = $item['imageURL'];
      }
      return $reward->save();
    }

    public function deleteItemForRedeem( $merchant_id, $item_id )
    {
      $reward = MerchantRewardsModel::where( 'redeemItemId', $item_id )->where('merchantid', $merchant_id)->first();
      if ( !$reward ) return false;
      $reward->status = 0;
      $reward->save();
      return true;
    }

    public function getMerchantTransactions( $merchant_id, $bcu_id = null )
    {

      $merchant = $this->model->find( $merchant_id );
      $transactions = array();
      $where_data = [ $merchant_id ];
      $branch_id = null;

      if ( $bcu_id ) {
        $branch_id = MerchantEmployeesModel::find( $bcu_id )->branchId;
      }

      if ( $merchant->packageId == 1 )
      {

        $sql = "SELECT
            ct.timestamp
            , CASE ct.transactionType
                WHEN 'void-earn' THEN 'void'
                WHEN 'void-paypoints' THEN 'void'
                WHEN 'void-redeem' THEN 'void'
                WHEN 'achievement-unlock' THEN
                      (CASE
                          WHEN aut.name = 'complete_profile' THEN 'Achievement: Profile Complete'
                          WHEN aut.name = 'customer_app_activation' THEN 'Achievement: App Activation'
                          WHEN aut.name = 'facebook_like' THEN 'Achievement: Facebook Like'
                          WHEN aut.name = 'members_referral' THEN 'Achievement: Successful Invite'
                          WHEN aut.name = 'transaction_no_with_minimum_amount' THEN 'Achievement: 1st Transactions Milestone'
                          WHEN aut.name = 'redemption_no_with_minimum_points' THEN 'Achievement: 1st Redemptions Milestone'
                          WHEN aut.name = 'purchase_no_within_year' THEN 'Achievement: Total Purchase Milestone'
                          WHEN aut.name = 'special_day_purchase' THEN CONCAT('Achievement: Special Day (', aut.options_date , ')')
                          WHEN aut.name = 'double_points' THEN 'Achievement: Double Points'
                          WHEN aut.name = 'attended_event' THEN CONCAT('Achievement: Event (', aut.options_name, ')')
                          WHEN aut.name = 'answering_survey' THEN CONCAT('Achievement: Survey (', aut.options_name, ')')
                      END)
                ELSE ct.transactionType
              END as transactionType
            , CASE ct.transactionType
                WHEN 'redeem' THEN ct.amountPaidWithCash
                WHEN 'void-redeem' THEN ct.amountPaidWithCash
                WHEN 'paypoints' THEN ct.amountPaidWithCash
                WHEN 'void-paypoints' THEN ct.amountPaidWithCash
                WHEN 'earn' THEN ct.amountPaidWithCash
                WHEN 'void-earn' THEN ct.voided_amount
                WHEN 'achievement-unlock' THEN ct.amountPaidWithCash
                WHEN 'transfer' THEN ct.amountPaidWithCash
              END as amountPaidWithCash
            , IFNULL(ct.receiptReferenceNumber, ct.transactionReferenceCode) AS receiptReferenceNumber
            , c.fullName AS customerName
            , CASE ct.transactionType
                WHEN 'redeem' THEN ct.pointsRedeem
                WHEN 'void-redeem' THEN CONCAT('+',ct.voided_points)
                WHEN 'paypoints' THEN ct.pointsBurn
                WHEN 'void-paypoints' THEN CONCAT('+',ct.pointsBurn)
                WHEN 'earn' THEN ct.pointsEarned
                WHEN 'void-earn' THEN CONCAT('-',ct.voided_points)
                WHEN 'achievement-unlock' THEN ct.pointsEarned
                WHEN 'transfer' THEN ct.pointsTransferred
              END as points
            , b.branchName
            , CASE ct.transactionType
                WHEN 'void-redeem' THEN
                  CASE ct.amax_transaction_id
                  WHEN 0 THEN CONCAT('(', ifr.name, ')')
                  ELSE CONCAT('(', mgr.name, ')')
                  END
                ELSE
                  CASE ct.amax_transaction_id
                  WHEN 0 THEN ifr.name
                  ELSE mgr.name
                  END
              END AS reward
            , ct.employee_id AS employeeNumber
            , r.quantity as quantity
            , r.claimed as claimed
            , ct.related_transactions_count AS related_transactions_count
            , ct.transactionReferenceCode AS transactionReferenceCode
            FROM CustomerTransaction AS ct
            LEFT JOIN Customer c
                ON ct.customerId = c.customerId
            LEFT JOIN Branches b
                ON ct.branchId = b.id
            LEFT JOIN Redeem r
                ON ct.transactionReferenceCode = r.redeemReferenceCode
            LEFT JOIN redeemedItems ri
                ON r.redeemId = ri.redeemId
            LEFT JOIN ItemsForRedeem ifr
                ON ri.redeemItemId = ifr.redeemItemId
            LEFT JOIN MerchantGlobeRewards mgr
                ON ri.redeemItemId = mgr.redeemItemId
            LEFT JOIN achievements_unlock_transactions aut
                ON ct.achievement_id = aut.id
            WHERE ct.merchant_id = ? ";
            if ( $branch_id ) { $sql .= " AND ct.branchId = ?"; $where_data[] = $branch_id; }
            $sql .= " ORDER BY timestamp DESC";
            $transactions = DB::select( $sql, $where_data );
      }  else {

        $sql =
        "   SELECT * FROM (
                SELECT
                cpct.date_punched AS timestamp
                , CONCAT(
                    IF (cpct.void = 1, 'void-', '')
                    , CASE
                        WHEN m.id IS NOT NULL THEN CONCAT(m.name, IF (cpct.remarks IS NULL OR cpct.remarks = '', '', CONCAT(': ', cpct.remarks)))
                        WHEN aut.id IS NOT NULL THEN
                            CONCAT('Achievement: ',
                                CASE aut.name
                                    WHEN 'complete_profile' THEN 'Profile Complete'
                                    WHEN 'customer_app_activation' THEN 'App Activation'
                                    WHEN 'facebook_like' THEN 'Facebook Like'
                                    WHEN 'members_referral' THEN 'Successful Invite'
                                    WHEN 'transaction_no_with_minimum_amount' THEN '1st Transactions Milestone'
                                    WHEN 'redemption_no_with_minimum_points' THEN '1st Redemptions Milestone'
                                    WHEN 'purchase_no_within_year' THEN 'Total Purchase Milestone'
                                    WHEN 'special_day_purchase' THEN CONCAT('Special Day (', aut.options_date , ')')
                                    WHEN 'double_points' THEN 'Double Points'
                                    WHEN 'attended_event' THEN CONCAT('Event (', aut.options_name, ')')
                                    WHEN 'answering_survey' THEN CONCAT('Survey (', aut.options_name, ')')
                                END
                            )
                        ELSE cpct.type
                        END
                    ) AS transactionType
                , IFNULL(cpct.or_no, cpct.transaction_ref) AS transaction_ref
                , c.fullName AS customerName
                , cpct.amount AS amountPaidWithCash
                , cpct.related_transactions_count AS related_transactions_count
                , CASE
                    WHEN (cpct.type = 'redeem' AND cpct.promo_id IS NOT NULL)
                        THEN (CASE WHEN cpct.void = 1 THEN CONCAT('(',mpcr.reward,')') ELSE mpcr.reward END)
                    WHEN (cpct.type = 'redeem' AND cpct.promo_id IS NULL)
                        THEN (CASE WHEN cpct.void = 1 THEN CONCAT('(',cps.name,')') ELSE cps.name END)
                    WHEN cpct.type = 'earn'
                        THEN (CASE WHEN cpct.void= 1 THEN CONCAT('-', cpct.stamps) ELSE cpct.stamps END)
                    WHEN cpct.type = 'achievement-unlock'
                        THEN cpct.stamps
                    WHEN cpct.type = 'earn-attendance'
                        THEN cpct.stamps
                  END AS stamps
                , b.branchName AS branchName
                , u.userId AS employeeNumber
                , ( CASE WHEN cpct.promo_id IS NOT NULL
                    THEN mpm.promo_title
                    ELSE cp.name
                    END ) AS promoTitle
                , cpct.id as id
                , c.uuid as customer_uuid
                , vc.code as voucher_code
                FROM CustomerPunchCardTransactions cpct
                LEFT JOIN Customer c ON cpct.customer_id = c.customerId
                LEFT JOIN Users u ON cpct.employee_id = u.userId
                LEFT JOIN Branches b ON u.branchId = b.id
                LEFT JOIN MerchantPromoMechanics mpm ON cpct.promo_id = mpm.id
                LEFT JOIN MerchantPunchCardRewards mpcr ON cpct.reward_id = mpcr.id
                LEFT JOIN customer_class_packages ccp ON cpct.class_package_id = ccp.id
                LEFT JOIN class_packages cp ON ccp.class_package_id = cp.id
                LEFT JOIN class_package_stamps cps ON cpct.reward_id = cps.id
                LEFT JOIN achievements_unlock_transactions aut ON cpct.achievement_transaction_id = aut.id
                LEFT JOIN merchant_punchcard_milestones m ON cpct.milestone_id = m.id
                LEFT JOIN voucher_codes vc ON cpct.voucher_code_id = vc.id
                WHERE cpct.merchant_id = ?  ";
                if ( $branch_id ) { $sql .= " AND b.id = ?"; $where_data[] = $branch_id; }
                $sql .= " GROUP BY cpct.id ) history";
                $transactions = DB::select( $sql, $where_data );
      }

      return $transactions;
    }

    public function countAllTransactions( $request )
    {
        $merchant = $this->model->find( $request->session()->get( 'user_id' ) );
        if ( $merchant->packageId == 1 ) /* Points */
        {
            $query = DB::table('CustomerTransaction')->where('merchant_id', $merchant->merchantid );
        } else { /* Punchcard */
            $query = DB::table('CustomerPunchCardTransactions')->where('merchant_id', $merchant->merchantid );
        }
        return $query->count();
    }

    public function countFilterTransactions( $request )
    {
        $merchant_id = $request->session()->get( 'user_id' );
        $merchant = $this->model->find( $merchant_id );
        if ( $merchant->packageId == 1 )
        {
            $query = $this->buildTransactionQueryPoints( $request );
        } else  {
            $query = $this->buildTransactionQueryPunchcard( $request );
        }
        return $query->count();
    }

    public function getMerchantTransactionsDaTaTable( $request )
    {
        $merchant_id = $request->session()->get( 'user_id' );
        $merchant = $this->model->find( $merchant_id );
        if ( $merchant->packageId == 1 )
        {
            $query = $this->buildTransactionQueryPoints( $request );
        } else  {
            $query = $this->buildTransactionQueryPunchcard( $request );
        }

        $limit = $request->length;

        if($limit == -1){
            $filteredCount = $query->count();
            $limit = ($filteredCount > 10) ? $filteredCount : $this->countAllTransactions( $request );
        }

        return $query->offset($request->start)->limit($limit)->get();
    }

    private function buildTransactionQueryPunchcard( $request )
    {
        $table_columns = array(
            'timestamp',
            'promoTitle',
            'transactionType',
            'fullName',
            'amount',
            'stamps',
            'transaction_ref',
            'branchName',
            'userId'
        );

        $query = DB::table('CustomerPunchCardTransactions')
                ->leftjoin('Customer', 'CustomerPunchCardTransactions.customer_id', '=', 'Customer.customerId')
                ->leftjoin('Users', 'CustomerPunchCardTransactions.employee_id', '=', 'Users.userId')
                ->leftjoin('Branches', 'Users.branchId', '=', 'Branches.id')
                ->leftjoin('branches_meta', function($join)
                    {
                        $join->on('Branches.id', '=', 'branches_meta.branch_id');
                        $join->on(DB::raw('branches_meta.name = "points"'), DB::raw(''), DB::raw(''));
                    })
                ->leftjoin('MerchantPromoMechanics', 'CustomerPunchCardTransactions.promo_id', '=', 'MerchantPromoMechanics.id')
                ->leftjoin('MerchantPunchCardRewards', 'CustomerPunchCardTransactions.reward_id', '=', 'MerchantPunchCardRewards.id')
                ->leftjoin('customer_class_packages', 'CustomerPunchCardTransactions.class_package_id', '=', 'customer_class_packages.id')
                ->leftjoin('class_packages', 'customer_class_packages.class_package_id', '=', 'class_packages.id')
                ->leftjoin('class_package_stamps', 'CustomerPunchCardTransactions.reward_id', '=', 'class_package_stamps.id')
                ->leftjoin('achievements_unlock_transactions', 'CustomerPunchCardTransactions.achievement_transaction_id', '=', 'achievements_unlock_transactions.id')
                ->leftjoin('merchant_punchcard_milestones', 'CustomerPunchCardTransactions.milestone_id', '=', 'merchant_punchcard_milestones.id')
                ->leftjoin('MerchantSettings', 'MerchantSettings.merchant_id', '=', 'Customer.merchantId')
                ->leftjoin('voucher_codes', 'CustomerPunchCardTransactions.voucher_code_id', '=', 'voucher_codes.id')
                ->where('CustomerPunchCardTransactions.merchant_id', $request->session()->get( 'user_id' ) )
                ->where(function($query) use ($request) {
                $query->where('CustomerPunchCardTransactions.date_punched', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('class_packages.name', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('CustomerPunchCardTransactions.type', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('Customer.fullName', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('CustomerPunchCardTransactions.amount', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('CustomerPunchCardTransactions.stamps', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('CustomerPunchCardTransactions.or_no', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('CustomerPunchCardTransactions.transaction_ref', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('Branches.branchName', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('Users.userId', 'like', '%' . $request->search['value'] . '%');
                });

                if($table_columns[$request->order[0]['column']] === 'promoTitle'){
                    $query->orderBy('MerchantPromoMechanics.promo_title', $request->order[0]['dir'])
                        ->orderBy('class_packages.name', $request->order[0]['dir']);
                } else if ($table_columns[$request->order[0]['column']] === 'stamps'){
                    $query->orderBy('MerchantPunchCardRewards.reward', $request->order[0]['dir'])
                        ->orderBy('class_package_stamps.name', $request->order[0]['dir'])
                        ->orderBy('CustomerPunchCardTransactions.stamps', $request->order[0]['dir']);
                } else if ($table_columns[$request->order[0]['column']] === 'transactionType'){
                    $query->orderBy('achievements_unlock_transactions.options_date', $request->order[0]['dir'])
                        ->orderBy('achievements_unlock_transactions.options_name', $request->order[0]['dir']);
                } else {
                    $query->orderBy($table_columns[$request->order[0]['column']], $request->order[0]['dir']);
                }

                $query->select('CustomerPunchCardTransactions.date_punched AS timestamp')
                ->addSelect( DB::raw("CONCAT(
                                    IF (CustomerPunchCardTransactions.void = 1, 'void-', '')
                                    , CASE
                                        WHEN merchant_punchcard_milestones.id IS NOT NULL THEN CONCAT(merchant_punchcard_milestones.name, IF (CustomerPunchCardTransactions.remarks IS NULL OR CustomerPunchCardTransactions.remarks = '', '', CONCAT(': ', CustomerPunchCardTransactions.remarks)))
                                        WHEN achievements_unlock_transactions.id IS NOT NULL THEN
                                        CONCAT('Achievement: ',
                                            CASE achievements_unlock_transactions.name
                                                WHEN 'complete_profile' THEN 'Profile Complete'
                                                WHEN 'customer_app_activation' THEN 'App Activation'
                                                WHEN 'facebook_like' THEN 'Facebook Like'
                                                WHEN 'members_referral' THEN 'Successful Invite'
                                                WHEN 'transaction_no_with_minimum_amount' THEN '1st Transactions Milestone'
                                                WHEN 'redemption_no_with_minimum_points' THEN '1st Redemptions Milestone'
                                                WHEN 'purchase_no_within_year' THEN 'Total Purchase Milestone'
                                                WHEN 'special_day_purchase' THEN CONCAT('Special Day (', achievements_unlock_transactions.options_date , ')')
                                                WHEN 'double_points' THEN 'Double Points'
                                                WHEN 'attended_event' THEN CONCAT('Event (', achievements_unlock_transactions.options_name, ')')
                                                WHEN 'answering_survey' THEN CONCAT('Survey (', achievements_unlock_transactions.options_name, ')')
                                            END
                                        )
                                    ELSE CustomerPunchCardTransactions.type
                                    END
                                ) AS transactionType"))
                ->addSelect( DB::raw('IFNULL(CustomerPunchCardTransactions.or_no, CustomerPunchCardTransactions.transaction_ref) AS transaction_ref' ) )
                ->addSelect('Customer.fullName AS customerName')
                ->addSelect('CustomerPunchCardTransactions.amount AS amountPaidWithCash')
                ->addSelect('CustomerPunchCardTransactions.related_transactions_count AS related_transactions_count')
                ->addSelect( DB::raw("CASE
                    WHEN (CustomerPunchCardTransactions.type = 'redeem' AND CustomerPunchCardTransactions.promo_id IS NOT NULL)
                        THEN (CASE WHEN CustomerPunchCardTransactions.void = 1 THEN CONCAT('(',MerchantPunchCardRewards.reward,')') ELSE MerchantPunchCardRewards.reward END)
                    WHEN (CustomerPunchCardTransactions.type = 'redeem' AND CustomerPunchCardTransactions.promo_id IS NULL)
                        THEN (CASE WHEN CustomerPunchCardTransactions.void = 1 THEN CONCAT('(',class_package_stamps.name,')') ELSE class_package_stamps.name END)
                    WHEN CustomerPunchCardTransactions.type = 'earn' AND MerchantSettings.enable_branch_points = '0'
                        THEN (CASE WHEN CustomerPunchCardTransactions.void= 1 THEN CONCAT('-', CustomerPunchCardTransactions.stamps) ELSE CustomerPunchCardTransactions.stamps END)
                    WHEN CustomerPunchCardTransactions.type = 'earn' AND MerchantSettings.enable_branch_points = '1'
                        THEN branches_meta.value
                    WHEN CustomerPunchCardTransactions.type = 'achievement-unlock'
                        THEN CustomerPunchCardTransactions.stamps
                    WHEN CustomerPunchCardTransactions.type = 'earn-attendance'
                        THEN CustomerPunchCardTransactions.stamps
                  END AS stamps") )
                ->addSelect('Branches.branchName AS branchName')
                ->addSelect('Users.userId AS employeeNumber')
                ->addSelect('CustomerPunchCardTransactions.id AS id')
                ->addSelect( DB::raw("( CASE WHEN CustomerPunchCardTransactions.promo_id IS NOT NULL
                    THEN MerchantPromoMechanics.promo_title
                    ELSE class_packages.name
                    END ) AS promoTitle") )
                ->addSelect('MerchantSettings.enable_branch_points as enable_branch_points')
                ->addSelect('branches_meta.value as branch_points')
                ->addSelect('voucher_codes.code as voucher_code')
                ->addSelect('Customer.uuid as customer_uuid')
                ->addSelect("Customer.mobileNumber as mobileNumber")
                ;
        if ( $request->minDate && $request->maxDate )
        {
            $query = $query->whereBetween('CustomerPunchCardTransactions.date_punched', [ $request->minDate, $request->maxDate]);
        }

        return $query;
    }

    private function buildTransactionQueryPoints( $request )
    {
        $table_columns = array(
            'CustomerTransaction.timestamp',
            'transactionType',
            'CustomerTransaction.transaction_method',
            'fullName',
            'amountPaidWithCash',
            'points',
            'quantity',
            'claimed',
            'reward',
            'receiptReferenceNumber',
            'branchName',
            'employee_id',
        );

        $query = DB::table('CustomerTransaction')
                ->leftjoin('Customer', 'CustomerTransaction.customerId', '=', 'Customer.customerId')
                ->leftjoin('Branches', 'CustomerTransaction.branchId', '=', 'Branches.id')
                ->leftjoin('Redeem', 'CustomerTransaction.transactionReferenceCode', '=', 'Redeem.redeemReferenceCode')
                ->leftjoin('redeemedItems', 'Redeem.redeemId', '=', 'redeemedItems.redeemId')
                ->leftjoin('ItemsForRedeem', 'redeemedItems.redeemItemId', '=', 'ItemsForRedeem.redeemItemId')
                ->leftjoin('MerchantGlobeRewards', 'redeemedItems.redeemItemId', '=', 'MerchantGlobeRewards.redeemItemId')
                ->leftjoin('achievements_unlock_transactions', 'CustomerTransaction.achievement_id', '=', 'achievements_unlock_transactions.id')
                ->where('CustomerTransaction.merchant_id', $request->session()->get( 'user_id' ) )
                ->where(function($query) use ($request) {
                $query->where('CustomerTransaction.timestamp', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('CustomerTransaction.transactionType', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('Customer.fullName', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('CustomerTransaction.amountPaidWithCash', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('CustomerTransaction.pointsEarned', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('Redeem.quantity', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('Redeem.claimed', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('ItemsForRedeem.name', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('MerchantGlobeRewards.name', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('CustomerTransaction.receiptReferenceNumber', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('CustomerTransaction.transactionReferenceCode', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('Branches.branchName', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('CustomerTransaction.employee_id', 'like', '%' . $request->search['value'] . '%')
                    ;
                })
                ->orderBy($table_columns[$request->order[0]['column']], $request->order[0]['dir'])
                ->select('CustomerTransaction.timestamp as timestamp')
                ->addSelect( DB::raw("CASE CustomerTransaction.transactionType
                                        WHEN 'void-earn' THEN 'void'
                                        WHEN 'void-paypoints' THEN 'void'
                                        WHEN 'void-redeem' THEN 'void'
                                        WHEN 'achievement-unlock' THEN
                                              (CASE
                                                  WHEN achievements_unlock_transactions.name = 'complete_profile' THEN 'Achievement: Profile Complete'
                                                  WHEN achievements_unlock_transactions.name = 'customer_app_activation' THEN 'Achievement: App Activation'
                                                  WHEN achievements_unlock_transactions.name = 'facebook_like' THEN 'Achievement: Facebook Like'
                                                  WHEN achievements_unlock_transactions.name = 'members_referral' THEN 'Achievement: Successful Invite'
                                                  WHEN achievements_unlock_transactions.name = 'transaction_no_with_minimum_amount' THEN 'Achievement: 1st Transactions Milestone'
                                                  WHEN achievements_unlock_transactions.name = 'redemption_no_with_minimum_points' THEN 'Achievement: 1st Redemptions Milestone'
                                                  WHEN achievements_unlock_transactions.name = 'purchase_no_within_year' THEN 'Achievement: Total Purchase Milestone'
                                                  WHEN achievements_unlock_transactions.name = 'special_day_purchase' THEN CONCAT('Achievement: Special Day (', achievements_unlock_transactions.options_date , ')')
                                                  WHEN achievements_unlock_transactions.name = 'double_points' THEN 'Achievement: Double Points'
                                                  WHEN achievements_unlock_transactions.name = 'attended_event' THEN CONCAT('Achievement: Event (', achievements_unlock_transactions.options_name, ')')
                                                  WHEN achievements_unlock_transactions.name = 'answering_survey' THEN CONCAT('Achievement: Survey (', achievements_unlock_transactions.options_name, ')')
                                              END)
                                        ELSE CustomerTransaction.transactionType
                                      END as transactionType"))
                ->addSelect( DB::raw("CASE CustomerTransaction.transactionType
                                        WHEN 'redeem' THEN CustomerTransaction.amountPaidWithCash
                                        WHEN 'void-redeem' THEN CustomerTransaction.amountPaidWithCash
                                        WHEN 'paypoints' THEN CustomerTransaction.amountPaidWithCash
                                        WHEN 'void-paypoints' THEN CustomerTransaction.amountPaidWithCash
                                        WHEN 'earn' THEN CustomerTransaction.amountPaidWithCash
                                        WHEN 'void-earn' THEN CustomerTransaction.voided_amount
                                        WHEN 'achievement-unlock' THEN CustomerTransaction.amountPaidWithCash
                                        WHEN 'transfer' THEN CustomerTransaction.amountPaidWithCash
                                      END as amountPaidWithCash"))
                ->addSelect( DB::raw("IFNULL(CustomerTransaction.receiptReferenceNumber, CustomerTransaction.transactionReferenceCode) AS receiptReferenceNumber"))
                ->addSelect("Customer.fullName AS customerName")
                ->addSelect( DB::raw("CASE CustomerTransaction.transactionType
                                        WHEN 'redeem' THEN CustomerTransaction.pointsRedeem
                                        WHEN 'void-redeem' THEN CONCAT('+',CustomerTransaction.voided_points)
                                        WHEN 'paypoints' THEN CustomerTransaction.pointsBurn
                                        WHEN 'void-paypoints' THEN CONCAT('+',CustomerTransaction.pointsBurn)
                                        WHEN 'earn' THEN CustomerTransaction.pointsEarned
                                        WHEN 'void-earn' THEN CONCAT('-',CustomerTransaction.voided_points)
                                        WHEN 'achievement-unlock' THEN CustomerTransaction.pointsEarned
                                        WHEN 'transfer' THEN CustomerTransaction.pointsTransferred
                                      END as points"))
                ->addSelect("Branches.branchName as branchName")
                ->addSelect( DB::raw("CASE CustomerTransaction.transactionType
                                        WHEN 'void-redeem' THEN
                                          CASE CustomerTransaction.amax_transaction_id
                                          WHEN 0 THEN CONCAT('(', ItemsForRedeem.name, ')')
                                          ELSE CONCAT('(', MerchantGlobeRewards.name, ')')
                                          END
                                        ELSE
                                          CASE CustomerTransaction.amax_transaction_id
                                          WHEN 0 THEN ItemsForRedeem.name
                                          ELSE MerchantGlobeRewards.name
                                          END
                                      END AS reward"))
                ->addSelect("CustomerTransaction.employee_id AS employeeNumber")
                ->addSelect("Redeem.quantity as quantity")
                ->addSelect("Redeem.claimed as claimed")
                ->addSelect("CustomerTransaction.related_transactions_count AS related_transactions_count")
                ->addSelect("CustomerTransaction.transactionReferenceCode AS transactionReferenceCode")
                ->addSelect("Customer.mobileNumber as mobileNumber")
                ->addSelect("CustomerTransaction.transaction_method as transaction_method")
                ;
        if ( $request->minDate && $request->maxDate )
        {
            $query = $query->whereBetween('CustomerTransaction.timestamp', [ $request->minDate, $request->maxDate]);
        }

        return $query;
    }

    public function toggleAdminRewards( $merchant_id, $reward_id, $enable )
    {
      $reward = MerchantGlobeRewardsModel::where([ 'merchantid' => $merchant_id, 'admin_item_id' => $reward_id]);
      if ( $reward->count() > 0 ) {
        $reward = $reward->first();
        $reward->status = $enable;
      } else {
        $admin_reward = AdminRewardsModel::find( $reward_id );

        $reward = new MerchantGlobeRewardsModel();
        $reward->merchantid = $merchant_id;
        $reward->name = $admin_reward->name;
        $reward->details = $admin_reward->details;
        $reward->type = $admin_reward->type;

        $points_calc = $admin_reward->pointsRequired;
        $merchant = MerchantModel::find( $merchant_id );
        if ( $merchant->conversion_settings ) {
          $redemption_peso = ( $merchant->conversion_settings->redemption_peso ) ?: 1;
          $redemption_points = ( $merchant->conversion_settings->redemption_points ) ?: 1;
          $points_calc = $admin_reward->pointsRequired / $redemption_peso;
          $points_calc = round($points_calc * $redemption_points);
        }

        $reward->pointsRequired = $points_calc;
        $reward->imageURL = $admin_reward->imageURL;
        $reward->status = $enable;
        $reward->timestamp = date("Y-m-d H:i:s");
        $reward->admin_item_id = $admin_reward->id;
      }
      $reward->save();
      return true;
    }

    public function getValidPunchcardIds($request){
        $punchcard_id = $request->punchcard_id;
        $merchant = $this->getById($request->merchant_id);
        if (!$merchant) {
            return false;
        }

        $punchcard_ids = [];
        if ($punchcard_id) {
            $punchcard = $merchant->punchcards->filter(function($punchcard) use ($punchcard_id) {
                return $punchcard->id == $punchcard_id;
            })->first();
            if (!$punchcard) {
                return false;
            }
            array_push($punchcard_ids, $punchcard->id);
        } else {
           $punchcard_ids = $merchant->punchcards->pluck('id')->flatten()->toArray();
        }

        return $punchcard_ids;
    }

    public function getPromoStampsSummary($merchant_id, $punchcard_id = null, $customer_ids = null)
    {
        $merchant = $this->getById($merchant_id);
        if (!$merchant) {
            return false;
        }
        $punchcard_ids = [];
        if ($punchcard_id) {
            $punchcard = $merchant->punchcards->filter(function($punchcard) use ($punchcard_id) {
                return $punchcard->id == $punchcard_id;
            })->first();
            if (!$punchcard) {
                return false;
            }
            array_push($punchcard_ids, $punchcard->id);
        } else {
           $punchcard_ids = $merchant->punchcards->pluck('id')->flatten()->toArray();
        }

        $customer_stamps_card_model = CustomerStampsCardModel::whereIn('punchcard_id', $punchcard_ids);

        if($customer_ids != null){
            $customer_stamps_card_model->whereIn('customer_id', $customer_ids);
        }

        return $customer_stamps_card_model->get();
    }


    public function buildPromoStampsSummaryQuery($request, $customer_ids = null)
    {
        $table_columns = array(
          'Customer.timestamp',
          'Customer.fullName',
          'Customer.mobileNumber',
          'MerchantPromoMechanics.promo_title',
          'MerchantPromoMechanics.duration_to_date',
          'status',
          'target',
          'accomplished',
          'balance'
        );

        $punchcard_ids = $this->getValidPunchcardIds($request);

        $customer_stamps_card_model = CustomerStampsCardModel::whereIn('punchcard_id', $punchcard_ids)
          ->groupBy('CustomerStampsCard.punchcard_id', 'CustomerStampsCard.customer_id')
          ->select(
            'CustomerStampsCard.id',
            'CustomerStampsCard.customer_id as customerId',
            'CustomerStampsCard.punchcard_id as punchCardId',
            'Customer.timestamp as timestamp',
            'Customer.fullName as fullName',
            'Customer.mobileNumber as mobileNumber',
            'MerchantPromoMechanics.promo_title as punchCard',
            'MerchantPromoMechanics.duration_to_date as validUntil'
            )->addSelect( DB::raw(
              'CASE
                  WHEN MerchantPromoMechanics.status = 0 THEN "Draft"
                    WHEN MerchantPromoMechanics.status = 1 AND MerchantPromoMechanics.duration_to_date < NOW() THEN "Expired"
                    WHEN MerchantPromoMechanics.status = 1 AND MerchantPromoMechanics.duration_to_date >= NOW() AND MerchantPromoMechanics.duration_from_date <= NOW() THEN "Active"
                    WHEN MerchantPromoMechanics.status = 1 THEN "Pending"
                END as status'
            ))
            ->addSelect( DB::raw('sum(MerchantPromoMechanics.num_stamps) as target') )
            ->addSelect( DB::raw('sum((SELECT count(cs.id) FROM CustomerStamps cs Where cs.stamp_card_id = CustomerStampsCard.id and cs.void = 0 )) as accomplished') )
            ->addSelect( DB::raw('sum(MerchantPromoMechanics.num_stamps) - sum((SELECT count(cs.id) FROM CustomerStamps cs Where cs.stamp_card_id = CustomerStampsCard.id and cs.void = 0 )) as balance') )
            ->addSelect( DB::raw('
              CASE
                WHEN (
                  SELECT count(CustomerPunchCardTransactions.id)
                      FROM CustomerPunchCardTransactions
                      WHERE CustomerPunchCardTransactions.stamp_card_id = CustomerStampsCard.id
                      AND CustomerPunchCardTransactions.type = "redeem" AND CustomerPunchCardTransactions.void = 0
                ) > 0 THEN  "Yes"
                  ELSE "No"
              END as redemption_status'
            ) )
            ->addSelect( DB::raw('count(distinct concat(CustomerStampsCard.punchcard_id, CustomerStampsCard.customer_id)) as temp_id') );

            if($table_columns[$request->order[0]['column']] == "target"){
              $customer_stamps_card_model->orderBy(DB::raw('sum(MerchantPromoMechanics.num_stamps)'), $request->order[0]['dir']);
            } elseif ($table_columns[$request->order[0]['column']] == "accomplished") {
              $customer_stamps_card_model->orderBy(DB::raw('sum((SELECT count(cs.id) FROM CustomerStamps cs Where cs.stamp_card_id = CustomerStampsCard.id and cs.void = 0 ))'), $request->order[0]['dir']);
            } elseif ($table_columns[$request->order[0]['column']] == "balance") {
              $customer_stamps_card_model->orderBy(DB::raw('sum(MerchantPromoMechanics.num_stamps) - sum((SELECT count(cs.id) FROM CustomerStamps cs Where cs.stamp_card_id = CustomerStampsCard.id and cs.void = 0 ))'), $request->order[0]['dir']);
            } else {
              $customer_stamps_card_model->orderBy($table_columns[$request->order[0]['column']], $request->order[0]['dir']);
            }

        $this->stampSummaryQueryApplyFilter($customer_stamps_card_model, $request);

        if($customer_ids != null){
            $customer_stamps_card_model->whereIn('CustomerStampsCard.customer_id', $customer_ids);
        }

        return $customer_stamps_card_model;
    }

    public function stampSummaryQueryApplyFilter($stamp_card_model, $request){
      return $stamp_card_model->leftJoin('MerchantPromoMechanics', 'CustomerStampsCard.punchcard_id', '=','MerchantPromoMechanics.id')
          ->leftJoin('Customer', 'CustomerStampsCard.customer_id', '=','Customer.customerId')->where(function($query) use ($request) {
          $query->where('Customer.timestamp', 'like', '%' . $request->search['value'] . '%')
              ->orWhere('Customer.fullName', 'like', '%' . $request->search['value'] . '%')
              ->orWhere('Customer.mobileNumber', 'like', '%' . $request->search['value'] . '%')
              ->orWhere('MerchantPromoMechanics.promo_title', 'like', '%' . $request->search['value'] . '%')
              ->orWhere('MerchantPromoMechanics.duration_to_date', 'like', '%' . $request->search['value'] . '%');
      });
    }

    public function getPromoStampSummaryData($query, $request)
    {
        $limit = $request->length;
        $query = clone $query;

        if($limit == -1){
            $filteredCount = $this->countFilterPromoStampSummaryData($query);
            $limit = ($filteredCount > 10) ? $filteredCount : $this->countAllPromoStampSummaryData($request);
        }

        return $query->offset($request->start)->limit($limit)->get();
    }

    public function countAllPromoStampSummaryData($request){
        $punchcard_ids = $this->getValidPunchcardIds($request);

        $query = CustomerStampsCardModel::whereIn('punchcard_id', $punchcard_ids)->select(DB::raw('count(DISTINCT concat(CustomerStampsCard.punchcard_id, CustomerStampsCard.customer_id)) as total'))->first();

        return $query->total;
    }

    public function countFilterPromoStampSummaryData($request){
        $punchcard_ids = $this->getValidPunchcardIds($request);

        $query = CustomerStampsCardModel::whereIn('punchcard_id', $punchcard_ids)->select(DB::raw('count(DISTINCT concat(CustomerStampsCard.punchcard_id, CustomerStampsCard.customer_id)) as total'));

        $this->stampSummaryQueryApplyFilter($query, $request);

        return $query->first()->total;
    }

    public function builPackageStampsSummaryQuery($request, $customer_ids, $package_id)
    {
        $table_columns = [
          'Customer.timestamp',
          'Customer.fullName',
          'Customer.mobileNumber',
          'class_packages.name',
          'customer_class_packages.end_date',
          'status',
          'class_packages.no_of_visits',
          'accomplished',
          'balance'
        ];

        $customer_class_packages = CustomerClassPackageModel::withTrashed()
        ->join('customer_class_packages_punchcards', 'customer_class_packages.id', '=', 'customer_class_packages_punchcards.customer_class_package_id')
        ->join('Customer', 'customer_class_packages.customer_id', '=', 'Customer.customerId')
        ->join('class_packages', 'customer_class_packages.class_package_id', '=', 'class_packages.id')
        ->where('class_packages.merchant_id', $request->merchant_id)
        ->where(function($query) use ($request) {
            $query->where('Customer.timestamp', 'like', '%' . $request->search['value'] . '%')
                ->orWhere('Customer.fullName', 'like', '%' . $request->search['value'] . '%')
                ->orWhere('Customer.mobileNumber', 'like', '%' . $request->search['value'] . '%')
                ->orWhere('class_packages.name', 'like', '%' . $request->search['value'] . '%')
                ->orWhere('customer_class_packages.end_date', 'like', '%' . $request->search['value'] . '%');
         })->select(
            'customer_class_packages.id as customerClassPackageId',
            'customer_class_packages_punchcards.id as punchCardId',
            'Customer.customerId as customerId',
            'class_packages.id as packageId',
            'Customer.timestamp as timestamp',
            'Customer.fullName as fullName',
            'Customer.mobileNumber as mobileNumber',
            'class_packages.name as punchCard',
            'customer_class_packages.end_date as validUntil',
            'class_packages.no_of_visits as target'
          )->addSelect( DB::raw('(SELECT count(*) FROM customer_class_packages_stamps WHERE customer_class_packages_stamps.class_package_punchcard_id = customer_class_packages_punchcards.id) as accomplished')
          )->addSelect( DB::raw('(class_packages.no_of_visits - (SELECT count(*) FROM customer_class_packages_stamps WHERE customer_class_packages_stamps.class_package_punchcard_id = customer_class_packages_punchcards.id)) as balance')
          )->addSelect( DB::raw('(CASE
            WHEN customer_class_packages.deleted_at IS NOT NULL THEN "Void"
              WHEN customer_class_packages.end_date < NOW() THEN "Expired" ELSE "Active"
            END) as status')
          );

        if($table_columns[$request->order[0]['column']] === "status"){
          $customer_class_packages->orderBy(DB::raw('(CASE
            WHEN customer_class_packages.deleted_at IS NOT NULL THEN "Void"
              WHEN customer_class_packages.end_date < NOW() THEN "Expired" ELSE "Active"
            END)'), $request->order[0]['dir']);
        } elseif ($table_columns[$request->order[0]['column']] === "accomplished" ) {
          $customer_class_packages->orderBy(DB::raw('(SELECT count(*) FROM customer_class_packages_stamps WHERE customer_class_packages_stamps.class_package_punchcard_id = customer_class_packages_punchcards.id)'), $request->order[0]['dir']);
        } elseif ($table_columns[$request->order[0]['column']] === "balance") {
          $customer_class_packages->orderBy(DB::raw('(class_packages.no_of_visits - (SELECT count(*) FROM customer_class_packages_stamps WHERE customer_class_packages_stamps.class_package_punchcard_id = customer_class_packages_punchcards.id))'), $request->order[0]['dir']);
        } else {
          $customer_class_packages->orderBy($table_columns[$request->order[0]['column']], $request->order[0]['dir']);
        }

        if($customer_ids != null){
            $customer_class_packages->whereIn('customer_class_packages.customer_id', $customer_ids);
        }

        return $customer_class_packages;
    }

    public function getPackageStampsSummaryData($query, $request){
        $limit = $request->length;
        $query = clone $query;

        if($limit == -1){
            $filteredCount = $this->countFilterPacakgeStampsSummaryData($query);
            $limit = ($filteredCount > 10) ? $filteredCount : $this->countAllPacakgeStampsSummaryData($request);
        }

        return $query->offset($request->start)->limit($limit)->get();
    }

    public function countFilterPackageStampsSummaryData($query){
        $query = clone $query;
        return $query->count();
    }

    public function countAllPackageStampsSummaryData($request, $package_id){
        $customer_class_packages = CustomerClassPackageModel::withTrashed()
        ->join('customer_class_packages_punchcards', 'customer_class_packages.id', '=', 'customer_class_packages_punchcards.customer_class_package_id')
        ->join('Customer', 'customer_class_packages.customer_id', '=', 'Customer.customerId')
        ->join('class_packages', 'customer_class_packages.class_package_id', '=', 'class_packages.id')
        ->where('class_packages.merchant_id', $request->merchant_id);

        return $customer_class_packages->count();
    }

    public function expireCustomerClassPackage($customer_package_id)
    {
        $customer_package_model = CustomerClassPackageModel::where('id', $customer_package_id)->first();

        $customer_package_model->end_date = Carbon::yesterday()->subDays(7);
        $customer_package_model->save();
    }

    public function getStamps($merchant_id)
    {
        $sql =
        "   SELECT
            c.timestamp
            , c.fullName
            , c.mobileNumber
            , ( CASE WHEN cpct.promo_id IS NOT NULL
                THEN mpm.promo_title
                ELSE cp.name
                END ) AS punchCard
            , ( CASE WHEN cpct.promo_id IS NOT NULL
                THEN DATE_FORMAT(mpm.duration_to_date, '%Y-%m-%d')
                ELSE DATE_FORMAT(ccp.end_date, '%Y-%m-%d')
                END ) AS validUntil
            , ( CASE WHEN cpct.promo_id IS NOT NULL
                THEN IF(DATE_FORMAT(mpm.duration_to_date, '%Y-%m-%d') < CURDATE(), 'Expired', 'Active')
                ELSE IF(DATE_FORMAT(ccp.end_date, '%Y-%m-%d') < CURDATE(), 'Expired', 'Active')
                END ) AS status
            , ( CASE WHEN cpct.promo_id IS NOT NULL
                THEN mpm.num_stamps
                ELSE cp.no_of_visits
                END ) AS target
            , SUM( CASE WHEN ((cpct.type='earn' OR cpct.type='earn-attendance' OR cpct.type='achievement-unlock') AND cpct.void != 1)
                THEN cpct.stamps
                ELSE 0 END
                ) AS accomplished
            FROM
            CustomerPunchCardTransactions cpct
            LEFT JOIN MerchantPromoMechanics mpm
                ON (cpct.promo_id = mpm.id)
            LEFT JOIN Customer c ON cpct.customer_id = c.customerId
            LEFT JOIN customer_class_packages ccp ON cpct.class_package_id = ccp.id
            LEFT JOIN class_packages cp ON ccp.class_package_id = cp.id
            WHERE
            cpct.merchant_id = ?
            AND cpct.void = 0
            GROUP BY
            cpct.customer_id
            , cpct.class_package_id
            , cpct.promo_id
            , cp.id
        ";

        $stamps = DB::select( $sql, [ $merchant_id ] );

        for ($i=0; $i < count($stamps); $i++) {
            $stamps[$i] = (array) $stamps[$i];
            $balance = $stamps[$i]['target'] - $stamps[$i]['accomplished'];
            $stamps[$i]['balance'] = ($balance > 0) ? $balance : '0';
        }
        return $stamps;
    }

    public function getSmsNotificationMessages($merchant_id, $type)
    {
        $merchant = $this->model->find($merchant_id);

        if (is_null($merchant)) {
            return false;
        }

        if ($type) {
            $messages = $merchant->sms_messages()->where('type', $type)->get();

            if ($messages->isEmpty()) {
                return null;
            }

            if ($messages->count() > 1) {
                return $messages;
            } else {
                if($messages->first()->message) {
                    return $messages->first();
                } else {
                    return null;
                }
            }
        }

        return $merchant->sms_notification_messages;
    }

    public function updateMerchantPassword( $merchant_id, $password )
    {
      $merchant = MerchantModel::find( $merchant_id );
      $merchant->password = $password;
      $merchant->requireChangePassword = false;
      return $merchant->save();
    }

    public function getModules( $package_id )
    {
      return MerchantModules::where( 'package_id', $package_id )->get();
    }

    public function saveUser( $data, $merchant_id, $package_id )
    {

      $user = MerchantSubUsersModel::where( ['email' => $data['email'], 'merchant_id' => $merchant_id, 'package_id' => $package_id ] );
      if ( $user->count() == 0 ) {
        $user = new MerchantSubUsersModel();
      }
      if ( isset($data['id']) && $data['id'] != null ) {
        $user = MerchantSubUsersModel::find( $data['id']);
      }
      $user->merchant_id = $merchant_id;
      $user->package_id = $package_id;
      $user->name = $data['name'];
      if ( isset($data->email) && $data->email != '' )  {
        $user->email = $data['email'];
      }
      if ( isset($data->password) && $data->password != '' )  {
        $user->password = hash("sha512", $data['password']);
      }

      $process = $user->save();
      if ( $process ) {
        $this->saveUserModules( $user->id, $data['modules'] );
        return TRUE;
      } else {
        return FALSE;
      }
    }

    public function disableUser( $id )
    {
      $user = MerchantSubUsersModel::find( $id );
      $user->is_deleted = 1;
      return $user->save();
    }

    public function getSubUser( $id )
    {
      return MerchantSubUsersModel::find( $id );
    }

    public function getUserModules( $id )
    {
      return MerchantSubUsersModulesModel::where( ['merchant_user_modules.user_id' => $id ] )
                                        ->join('modules', 'merchant_user_modules.modules_id', '=', 'modules.id')
                                        ->select('merchant_user_modules.id', 'merchant_user_modules.user_id', 'merchant_user_modules.modules_id', 'modules.code', 'modules.name', 'modules.url')
                                        ->get();
    }

    public function saveUserModules( $user_id, $modules )
    {
      if ( MerchantSubUsersModulesModel::where( [ 'user_id' => $user_id ])->count() > 0 ) {
        MerchantSubUsersModulesModel::where( [ 'user_id' => $user_id ])->delete();
      }

      foreach( $modules as $module) {
        $input = array(
            'user_id' => $user_id,
            'modules_id' => $module
          );
        MerchantSubUsersModulesModel::insert( $input );
      }
    }

    public function getUserModulesRoute( $id )
    {
      return MerchantSubUsersModulesModel::where( ['merchant_user_modules.user_id' => $id ] )
                                        ->join('modules', 'merchant_user_modules.modules_id', '=', 'modules.id')
                                        ->select('modules.route_name')
                                        ->get();
    }

    public function getSavedPromoMechanics( $merchant_id , $package_id )
    {
      $promo_mechanics = [];
      $query = DB::table('MerchantPromoMechanics')->select('id','promo_title','num_stamps','duration_from_date','duration_to_date','status')->where(['merchant_id' => $merchant_id,'package_id' => $package_id, 'deleted_at' => NULL])->orderBy("duration_from_date", "desc");
      if ($query->count() > 0) {
          $results = $query->get();
          for($i = 0; $i < count($results); $i++) {
             $promo_mechanics[$i]['id'] = $results[$i]->id;
             $promo_mechanics[$i]['title'] = $results[$i]->promo_title;
             $promo_mechanics[$i]['status'] = $this->formatStatus($results[$i]->status, $results[$i]->duration_from_date, $results[$i]->duration_to_date);
             $promo_mechanics[$i]['duration_from_date'] = date("M d, Y", strtotime($results[$i]->duration_from_date));
             $promo_mechanics[$i]['duration_to_date'] = date("M d, Y", strtotime($results[$i]->duration_to_date));
          }
      }
      return $promo_mechanics;
    }

    protected function formatStatus($status, $durationStart, $durationEnd)
    {
        $formatted_status = null;
        $now = strtotime(Date('Y-m-d'));
        $start_ts = strtotime($durationStart);
        $end_ts = strtotime($durationEnd);

        if($status == 1) { // published
            $formatted_status = "Pending";

            //check if expired
            if( $end_ts < $now )  {
                $formatted_status = "Expired";
                return $formatted_status;
            }

            // check if pending
            if(  $now >= $start_ts && $now <= $end_ts )  {
                $formatted_status = "Live";
            }
        } else {
            $formatted_status = "Draft";
        }

        return $formatted_status;
    }

    public function updateMerchantUserPassword( $user_id, $password )
    {
      $merchant = MerchantSubUsersModel::find( $user_id );
      $merchant->password = $password;
      return $merchant->save();
    }

    public function isPromoDurationValid($merchantId, $durationStart, $durationEnd, $promoId = null)
    {
        $return = true;
        if($durationStart) {
            if($promoId) {
              $where[] = array('id','!=', $promoId);
            }
            $where[] = array('merchant_id','=', $merchantId);
            $where[] = array('duration_from_date','<=', $durationStart);
            $where[] = array('duration_to_date','>=', $durationStart);
            $query = DB::table('MerchantPromoMechanics')->where( $where );
            if ($query->count() > 0) {
                $return = false;
            }

            unset( $where );

            if($promoId) {
                $where[] = array('id','!=', $promoId);
            }
            $where[] = array('merchant_id','=', $merchantId);
            $where[] = array('duration_from_date','<=', $durationEnd);
            $where[] = array('duration_to_date','>=', $durationEnd);
            $query = DB::table('MerchantPromoMechanics')->where( $where );
            if ($query->count() > 0) {
                $return = false;
            }
        }
        return $return;
    }

    public function savePromoMechanics( $data, $rewards )
    {
      $to_save = array(
          'promo_title' => $data['title'],
          'num_stamps' => $data['stampsCount'],
          'duration_from_date' => $data['duration_promo_start'],
          'duration_to_date' => $data['duration_promo_end'],
            'redemption_duration_from_date' => $data['redemption_promo_start'],
            'redemption_duration_to_date' => $data['redemption_promo_end'],
            'merchant_id' => $data['merchant_id'],
            'package_id'  => $data['package_id'],
            'created_at'  => date('Y-m-d H:i:s'),
            'created_by'  => $data['merchant_id'],
            'status'      => $data['status']
        );

      DB::table('MerchantPromoMechanics')->insert($to_save);
      $promo_mechanics_id = DB::getPdo()->lastInsertId();

      $amount_promo = array(
        'promo_id'  => $promo_mechanics_id,
        'amount'    => $data['amount'],
        'no_of_stamp' => 1
      );

      DB::table('MerchantPromoAmountStamp')->insert($amount_promo);

      if( $rewards ) {
        foreach($rewards as $reward) {

          $reward['promo_id'] = $promo_mechanics_id;
          $reward['merchant_id'] = $data['merchant_id'];

          DB::table('MerchantPunchCardRewards')->insert($reward);
        }
      }

      return $promo_mechanics_id;
    }

    public function getPromoMechanics( $id )
    {
        $result = [];
        $query = DB::table('MerchantPromoMechanics')->where(['id' => $id]);

        if ($query->count() > 0) {
          $result = (array) $query->first();

          //get amount
          $query = DB::table('MerchantPromoAmountStamp')->where(['promo_id' => $id]);
          $amount_result = (array) $query->first();

          $result = array_merge( $result, $amount_result);

          //get rewards
          $query =  DB::table('MerchantPunchCardRewards')->where(['promo_id' => $id]);
          $reward_results = $query->get();

          $result['rewards'] = array();

          foreach( $reward_results as $reward_result )
          {
            $result['rewards'][] = (array) $reward_result;
          }
        }
        return $result;
    }

    public function deletePromoMechanics( $merchant_id, $id )
    {
      $query = DB::table('MerchantPromoMechanics')->select('id')->where([ 'merchant_id' => $merchant_id,'id' => $id] );
      if ($query->count() > 0) {
        DB::table('MerchantPromoMechanics')->where(['id' => $id])->delete();
        DB::table('MerchantPunchCardRewards')->where(['promo_id' => $id])->delete();
        DB::table('MerchantPromoAmountStamp')->where(['promo_id' => $id])->delete();
        return true;
      } else {
        return false;
      }
    }

    public function deleteExpiredPromoMechanics( $merchant_id, $id )
    {
      $query = DB::table('MerchantPromoMechanics')->select('id')->where([ 'merchant_id' => $merchant_id,'id' => $id] );
      if ($query->count() > 0) {
        MerchantPromoMechanicsModel::where(['id' => $id])->delete();
        DB::table('MerchantPromoMechanics')->where('id', $id)->update(['deleted_by' => $merchant_id]);
        MerchantPunchCardRewardsModel::where(['promo_id' => $id])->delete();
        MerchantPromoAmountStampModel::where(['promo_id' => $id])->delete();
        return true;
      } else {
        return false;
      }
    }

    public function updatePromoMechanics( $merchantId, $promoId, $status, $promoTitle, $durationStart, $durationEnd, $redemptionStart, $redemptionEnd, $amount, $numStamps )
    {
        $data = array(
            'promo_title' => $promoTitle,
            'duration_from_date' => $durationStart,
            'duration_to_date' => $durationEnd,
            'redemption_duration_from_date' => $redemptionStart,
            'redemption_duration_to_date' => $redemptionEnd,
            'status' => $status,
            'num_stamps' => $numStamps
        );

        DB::table('MerchantPromoMechanics')->where(['id' => $promoId])->update($data);
        $amount_data = array(
                'amount' => $amount
            );
        DB::table('MerchantPromoAmountStamp')->where(['promo_id' => $promoId])->update($amount_data);
    }

    public function getPromoReward($promoId, $stampNo)
    {
        $result = [];
        $query = DB::table('MerchantPunchCardRewards')->where([ 'promo_id' => $promoId, 'no_stamps' => $stampNo ]);
        if ($query->count() > 0) {
           $result = (array) $query->first();
        }
        return $result;
    }

    public function updatePromoReward($promoId, $stampNo, $description, $imageUrl, $details, $branch)
    {
        $data = array(
            'reward' => $description,
            'details' => $details,
            'branch_ids' => $branch
        );
        if ($imageUrl) {
            $data = array_merge($data, array('image_url' => $imageUrl));
        }
        return DB::table('MerchantPunchCardRewards')->where([ 'promo_id' => $promoId, 'no_stamps' => $stampNo ])->update($data);
    }

    public function addPromoReward($promoId, $stampNo, $description, $imageUrl, $merchantId, $details, $branch)
    {
        $data = array(
            'promo_id' => $promoId,
            'no_stamps' => $stampNo,
            'reward' => $description,
            'merchant_id' => $merchantId,
            'image_url' => $imageUrl,
            'details' => $details,
            'branch_ids' => $branch
        );
        return DB::table('MerchantPunchCardRewards')->insert($data);
    }

    public function removePromoReward($promoId, $stampNo)
    {
        return DB::table('MerchantPunchCardRewards')->where([ 'promo_id' => $promoId, 'no_stamps' => $stampNo ])->delete();
    }

    public function enableUser( $id )
    {
      $user = MerchantSubUsersModel::find( $id );
      $user->is_deleted = 0;
      return $user->save();
    }

    public function getCheckIn( $merchant_id, $branch_id )
    {
      $sql =
        "   SELECT
            cpct.date_punched
            , c.fullName
            , c.mobileNumber
            FROM
            CustomerPunchCardTransactions cpct
            LEFT JOIN MerchantPromoMechanics mpm ON cpct.promo_id = mpm.id
            LEFT JOIN Customer c ON cpct.customer_id = c.customerId
            LEFT JOIN CustomerStamps cs ON cpct.id = cs.transaction_id
            WHERE
            cpct.merchant_id = ?
            AND cpct.void = 0
            AND cs.branch_id = ?
        ";
        $query = DB::select( $sql, [ $merchant_id, $branch_id ] );
        return $query;
    }

    public function getGenericValidationBypass( $merchant_id ){

        $merchant = $this->model->find( $merchant_id );
        return $merchant->generic_validation_bypass;

    }

    public function getGenericFeatureBypass( $merchant_id ){

        $merchant = $this->model->find( $merchant_id );
        return $merchant->generic_feature_bypass;

    }

    public function login( $email, $password )
    {
      return MerchantModel::where( [ 'username' => $email, 'password' => hash("sha512", $password) ] )->first();
    }

    public function subuserlogin( $email, $password )
    {
      return MerchantSubUsersModel::where( [ 'email' => $email, 'password' => hash("sha512", $password) ] )->first();
    }

    public function registerMerchant( $data )
    {
      $merchant = new MerchantModel();
      $merchant->packageId = $data['packageId'];
      $merchant->business_system_link = $data['business_system_link'];
      $merchant->businessName = $data['businessName'];
      $merchant->businessType = $data['businessType'];
      $merchant->businessProgramObjective = $data['businessProgramObjective'];
      $merchant->PackageAddons = $data['PackageAddons'];
      $merchant->business_branches = $data['business_branches'];
      $merchant->address = $data['address'];
      $merchant->averageCustomer = $data['averageCustomer'];
      $merchant->personName = $data['personName'];
      $merchant->personPosition = $data['personPosition'];
      $merchant->contactPerson = $data['contactPerson'];
      $merchant->contactPersonEmail = $data['contactPersonEmail'];
      $merchant->contactPersonNumber = $data['contactPersonNumber'];
      $merchant->username = $data['username'];
      $merchant->password = $data['password'];
      $merchant->timestamp = $data['timestamp'];
      $merchant->trial_end = $data['trial_end'];
      $merchant->status = $data['status'];
      $merchant->shouldLaunchQuicksetup = $data['shouldLaunchQuicksetup'];
      $merchant->requireChangePassword = 1;
      $merchant->account_number = $this->generateAccountNumber( $merchant );
      $merchant->save();

      $modules = $this->getModules( $data['packageId'] );
      if ( count($modules) > 0 ) {
        $insert_arr = [];
        foreach( $modules as $module )
        {
          if ( $module->route_name != "class" )
          {
            $insert_arr[] = ['module_id' => $module->id, 'merchant_id' => $merchant->merchantid, 'created_at' => date("Y-m-d H:i:s") ];
          }
        }

        ModulesMerchantModel::insert($insert_arr);
      }

      $broadcast_modules = BroadcastToolsModulesModel::get();
      if ( count($broadcast_modules) > 0 ) {
        $insert_arr = [];
        foreach( $broadcast_modules as $broadcast_module )
        {
          if ( $broadcast_module->name != "Attendance" )
          {
            $insert_arr[] = ['broadcast_tools_id' => $broadcast_module->id, 'merchant_id' => $merchant->merchantid, 'created_at' => date("Y-m-d H:i:s") ];
          }
        }
        BroadcastToolsManagementModel::insert($insert_arr);
      }


      $new_post_category = new CategoryModel();
      $new_post_category->merchant_id = $merchant->merchantid;
      $new_post_category->name = 'Events/Promos';
      $new_post_category->save();

      return $merchant;
    }

    public function generateAccountNumber( $merchant )
    {
      $account_number = sprintf('%04d', $merchant->merchantid );
      $account_number .= '-' . sprintf('%06d', rand(1,999999));
      $account_number .= '-' . Carbon::parse($merchant->timestamp)->format('my');
      $merchant->account_number = $account_number;
      $merchant->save();
    }

    public function checkEmail( $email )
    {
      $query = MerchantModel::where( [ 'username' => $email ] )->count();
      if ( $query > 0 ) {
        return 'merchant';
      } else {
        $query = MerchantSubUsersModel::where( [ 'email' => $email ] )->count();
        if ( $query > 0 ) {
          return 'subuser';
        }
      }
      return false;
    }

    public function forgotpassword( $email, $password, $type = 'merchant' )
    {
      if ( $type == 'merchant') {
        $merchant = MerchantModel::where( [ 'username' => $email ] )->first();
        $merchant->password = hash("sha512", $password);
        $merchant->save();
        return $merchant->personName;
      } else if ( $type == "subuser") {
        $subuser = MerchantSubUsersModel::where( [ 'email' => $email ] )->first();
        $subuser->password = hash("sha512", $password);
        $subuser->save();
        return $subuser->name;
      } else {
        return false;
      }
    }

    public function createQuicksetupProgressTrack( $merchant_id, $package_id, $steps )
    {
        $query = DB::table('Quicksetup')->where([ 'merchant_id' => $merchant_id ] );
        if ( $query->count() == 0 )
        {
            DB::table('Quicksetup')->insert(
                [ 'merchant_id' => $merchant_id, 'completed' => 0, 'package_id' => $package_id, 'num_steps' => $steps ]
            );
        }
    }

    public function clearFirstLoginFlag( $merchant_id )
    {
        $merchant = MerchantModel::find( $merchant_id );
        $merchant->isfirstlogin = 0;
        $merchant->save();
    }

    public function getCompletionState( $merchant_id )
    {
      $result = DB::table('Quicksetup')->select('completed')->where([ 'merchant_id' => $merchant_id ])->first();
      return $result;
    }

    public function getSavedProgramSettings( $merchant_id )
    {
      $data = [ 'programName' => '', 'pointsName' => '' ];
      $query = DB::table('MerchantSettings')->select('id','program_name','points_name')->where(['merchant_id' => $merchant_id]);
      if ($query->count() > 0) {
        $result = $query->first();
        $data = [
          'programName' => $result->program_name,
          'pointsName' => $result->points_name
        ];
      }
      return $data;
    }

    public function getSavedApplicationDetails( $merchant_id )
    {
      $data = [ 'appName' => '', 'appDescription' => '', 'appLogo' => '' ];
      $query = DB::table('MerchantSettings')->select('app_name','app_description','app_logo')->where(['merchant_id' => $merchant_id]);
      if ($query->count() > 0) {
        $result = $query->first();
        $data = [
          'appName' => $result->app_name,
          'appDescription' => $result->app_description,
          'appLogo' => $result->app_logo ? StorageHelper::repositoryUrlForView($result->app_logo) : 'assets/images/addImage.jpg'
          ];
      }
      return $data;
    }

    public function getSavedMerchantApplication( $merchant_id )
    {
      $data = [
            'logo' => '',
            'background' => '',
            'overlayColor' => '',
            'textColor' => '',
            'buttonsColor' => '',
            'stamp' => ''
            ];

      $query = DB::table('MerchantPackage')->select('logoURL','backgroundURL','stampURL','overlayColor','textColor','buttonsColor')->where(['merchantId' => $merchant_id, 'designType' => 1]);
      if ($query->count() > 0) {
      $result = $query->first();
      $data = [
        'logo' => $result->logoURL ? StorageHelper::repositoryUrlForView($result->logoURL) : '',
        'background' => $result->backgroundURL ? StorageHelper::repositoryUrlForView($result->backgroundURL) : '',
        'overlayColor' => $result->overlayColor,
        'textColor' => $result->textColor,
        'buttonsColor' => $result->buttonsColor,
        'stamp' => $result->stampURL ? StorageHelper::repositoryUrlForView($result->stampURL) : 'assets/images/addImage.jpg',
        ];
      }
      return $data;
    }

    public function getSavedCustomerAppDesign( $merchant_id )
    {
        $data = [
            'logo' => '',
            'background' => '',
            'overlayColor' => '',
            'textColor' => '',
            'buttonsColor' => '',
            'stamp' => '',
            'name' => ''
            ];
        $query = DB::table('MerchantPackage')->select('logoURL','backgroundURL','stampURL','overlayColor','textColor','buttonsColor','merchantName')->where(['merchantId' => $merchant_id, 'designType' => 3]);
        if ($query->count() > 0) {
          $result = $query->first();
          $data = [
              'logo' => $result->logoURL ? StorageHelper::repositoryUrlForView($result->logoURL) : '',
              'background' => $result->backgroundURL ? StorageHelper::repositoryUrlForView($result->backgroundURL) : '',
              'overlayColor' => $result->overlayColor,
              'textColor' => $result->textColor,
              'buttonsColor' => $result->buttonsColor,
              'stamp' => $result->stampURL ? StorageHelper::repositoryUrlForView($result->stampURL) : 'assets/images/addImage.jpg',
              'name' => $result->merchantName
            ];
        }
        return $data;
    }

    public function saveProgramSettings( $merchant_id, $programName, $pointsName )
    {
        $data = array(
            'program_name' => $programName,
            'points_name' => $pointsName
        );
        $query = DB::table('MerchantSettings')->select('id')->where(['merchant_id' => $merchant_id]);
        if ( $query->count() > 0) {
            $result = $query->first();
            DB::table('MerchantSettings')->where([ 'id' => $result->id ,'merchant_id' => $merchant_id ])->update( $data );
        } else {
            $data['merchant_id'] = $merchant_id;
            DB::table('MerchantSettings')->insert( $data );
        }
    }

    public function trackProgress($merchantId,$progress)
    {
        $query = DB::table('QuicksetupProgress')->select('id')->where(['merchant_id' => $merchantId,'flag' => $progress]);
        if ( !$query->count() > 0) {
            $query2 = DB::table('Quicksetup')->select('id')->where(['merchant_id' => $merchantId]);
            if ( $query2->count() > 0) {
                $result = $query2->first();
                $quicksetup_id = $result->id;
                DB::table('QuicksetupProgress')->insert([ 'merchant_id' => $merchantId,'quicksetup_id' => $quicksetup_id, 'flag' => $progress ]);
                $query3 = DB::table('Quicksetup')->select('completed', 'num_steps')->where(['merchant_id' => $merchantId]);
                if ($query3->count() > 0) {
                    $result2 = $query3->first();
                    $completed = ($result2->completed + (100 / $result2->num_steps));
                    if ($completed >= 99) {
                        $completed = 100;
                    }
                    DB::table('Quicksetup')->where(['merchant_id' => $merchantId])->update(['completed' => $completed]);
                }
            }
        }
    }

    public function saveApplicationDetails( $merchant_id, $appName, $appDescription, $appLogo )
    {
        $data = [
            'app_name' => $appName,
            'app_description' => $appDescription,
            'app_logo' => $appLogo
            ];
        $query = DB::table('MerchantSettings')->select('id')->where([ 'merchant_id' => $merchant_id ]);
        if ($query->count() > 0) {
            $result = $query->first();
            $merchant_settings_id = $result->id;
            DB::table('MerchantSettings')->where(['id' => $merchant_settings_id,'merchant_id' => $merchant_id])->update($data);
        } else {
            $data['merchant_id'] = $merchant_id;
            DB::table('MerchantSettings')->insert( $data );
        }
    }

    public function clearQuicksetupLaunchFlag( $merchant_id )
    {
        $merchant = MerchantModel::find( $merchant_id );
        $merchant->shouldLaunchQuicksetup = 0;
        $merchant->save();
    }

    public function getMerchantRewardByUuid( $merchant_id, $uuid ){

        $rewards_factory = new MerchantRewardsFactoryModel( $merchant_id );
        $reward = $rewards_factory->getRewardByUuid( $uuid );

        return $reward;

    }

    public function populateMindbodyRepoTables($data)
    {
        $is_schedule_exists = MindbodyScheduleModel::find($data['schedule']['id']);
        if (!$is_schedule_exists) {
            $schedule = new MindbodyScheduleModel();
            $schedule->id = $data['schedule']['id'];
            $schedule->class_id = $data['class']['id'];
            $schedule->instructor_id = $data['instructor']['id'];
            $schedule->start_datetime = $data['schedule']['start_datetime'];
            $schedule->end_datetime = $data['schedule']['end_datetime'];
            $schedule->save();
        }

        $is_class_exists = MindbodyClassModel::find($data['class']['id']);
        if (!$is_class_exists) {
            $class = new MindbodyClassModel();
            $class->id = $data['class']['id'];
            $class->name = $data['class']['name'];
            $class->save();
        }

        $is_instructor_exists = MindbodyInstructorModel::find($data['instructor']['id']);
        if (!$is_instructor_exists) {
            $instructor = new MindbodyInstructorModel();
            $instructor->id = $data['instructor']['id'];
            $instructor->name = $data['instructor']['name'];
            $instructor->save();
        }

        return;
    }

    /**
     * @param MerchantModel $merchant
     * @param array $args
     * @return \Rush\Modules\Merchant\Models\MerchantPunchCardMilestoneModel[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getPunchcardMilestone(MerchantModel $merchant, array $args)
    {
        return count($args) ? $merchant->punchcard_milestones()->where($args)->get() : $merchant->punchcard_milestones;
    }

    public function bculogin( $id, $pin )
    {
      return MerchantEmployeesModel::where( [ 'userId' => $id, 'fourDigit' => $pin ] )->first();
    }

    public function getModuleByCode( $code, $package_id )
    {
      if ( count($code) > 1 ) {
        return MerchantModules::where( [ 'package_id' => $package_id ] )->whereIn( 'code', $code )->get();
      } else {
        return MerchantModules::where( [ 'code' => $code, 'package_id' => $package_id ] )->get();
      }
    }

    public function memberTierStop( $id )
    {

        $merchant_settings = MerchantSettingModel::where('merchant_id', $id);
        if ($merchant_settings->count() > 0) {
            $merchant_settings = $merchant_settings->first();
            $merchant_settings->allow_registration = false;
            return $merchant_settings->save();
        }
        return false;
    }

    public function validateCustomerTierCount($merchant)
    {
        if ($merchant->new_billing_calculation) {
            $total  = $merchant->customers->count();
            $allowed = true;
            if ($total <= 10000) {
                if ($total === 10000 && !$merchant->settings->allow_registration) {
                    $allowed = false;
                }
            } elseif ($total <= 25000) {
                if ($total === 25000 && !$merchant->settings->allow_registration) {
                    $allowed = false;
                }
            } elseif ($total <= 50000) {
                if ($total === 50000 && !$merchant->settings->allow_registration) {
                    $allowed = false;
                }
            } elseif ($total <= 100000) {
                if ($total === 100000 && !$merchant->settings->allow_registration) {
                    $allowed = false;
                }
            }
            return $allowed;
        } else {
            return (boolean) $merchant->settings->allow_registration;
        }
    }

    public function insert_qr_code( $merchant_id, $branch_ids = "[0]", $name = "", $points = 0, $load = -1, $customer_scan_limit = -1 ){

        $qr_code_model = new QRCodeModel;
        $qr_code_model->merchant_id = $merchant_id;
        $qr_code_model->branch_ids = $branch_ids;
        $qr_code_model->name = $name;
        $qr_code_model->points = $points;
        $qr_code_model->load = $load;
        $qr_code_model->customer_scan_limit = $customer_scan_limit;
        $qr_code_model->save();

        return $qr_code_model;

    }

    public function get_merchant_branches_by_id( $merchant_id, $branch_ids_arr ){

        return MerchantBranchesModel
            ::where('merchantId', $merchant_id)
            ->whereIn('id', $branch_ids_arr)
            ->get();

    }

    public function get_qr_code_by_uuid( $qr_code_uuid ){

        return QRCodeModel
            ::where('active', 1)
            ->where('uuid', $qr_code_uuid)
            ->first();

    }

    public function getMerchantBcuModules( $id )
    {
      return BcuModulesModel::where( ['bcu_modules.merchant_id' => $id ] )
                                        ->join('modules', 'bcu_modules.modules_id', '=', 'modules.id')
                                        ->select('bcu_modules.id', 'bcu_modules.merchant_id', 'bcu_modules.modules_id', 'modules.code', 'modules.name', 'modules.url')
                                        ->get();
    }

    public function saveBcuOptions( $modules, $functions, $merchant_id )
    {
      if ( BcuModulesModel::where( [ 'merchant_id' => $merchant_id ])->count() > 0 ) {
        BcuModulesModel::where( [ 'merchant_id' => $merchant_id ])->delete();
      }

      foreach( $modules as $module) {
        $input = array(
            'merchant_id' => $merchant_id,
            'modules_id' => $module,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')

          );
        BcuModulesModel::insert( $input );
      }

      return true;
    }

    public function getMerchantBcuModulesRoute( $id )
    {
      return BcuModulesModel::where( ['bcu_modules.merchant_id' => $id ] )
                                        ->join('modules', 'bcu_modules.modules_id', '=', 'modules.id')
                                        ->select('modules.route_name')
                                        ->get();
    }

    public function getIssuedVoucherTransactions($employee, $args)
    {
        $merchant = $employee->merchant;
        $transactions = $merchant->transactions()
            ->where('employee_id', $employee->id)
            ->where('type', 'redeem')
            ->where('voucher_code_id', '!=', null)
            ->orderBy('created_at', 'desc')
            ->paginate($args['per_page'], ['*'], 'page', $args['paged']);
        return $transactions;
    }

    public function getVouchers( $branches )
    {
      $branch_ids = [];
      foreach( $branches as $branch )
      {
        $branch_ids[] = $branch['id'];
      }
      return VoucherModel::whereIn('branch_id', $branch_ids )->get();
    }

    public function addVoucher( $request )
    {
        $branches = $request->branch_ids;
        foreach($branches as $branch) {
          $voucher = new VoucherModel();
          $voucher->name = $request->{'voucher-name'};
          $voucher->description = $request->{'voucher-description'};
          $voucher->branch_id = $branch;
          $voucher->threshold = $request->threshold;
          $voucher->discount = $request->discount;
          $voucher->discount_type = $request->discount_type;
          $voucher->voucher_image = $request->voucher_image;
          $voucher->save();
        }
    }

    public function editVoucher( $request )
    {

      $voucher = VoucherModel::find( $request->voucher_id );
      $voucher->name = $request->{'evoucher-name'};
      $voucher->description = $request->{'evoucher-description'};
      $voucher->threshold = $request->ethreshold;
      $voucher->discount = $request->ediscount;
      $voucher->discount_type = $request->ediscount_type;
      $voucher->voucher_image = $request->evoucher_image;
      $voucher->save();
    }

    public function deleteVoucher( $id )
    {
      $voucher = VoucherModel::find( $id );
      $voucher->delete();
    }

    public function addClientCode( $merchant_id, $code )
    {
      $clientcode = new ClientModel();
      $clientcode->code = $code;
      $clientcode->merchant_id = $merchant_id;
      return $clientcode->save();
    }

    public function editClientCode( $merchant_id, $data )
    {
      $clientcode = ClientModel::find( $data['eclient-code-id'] );
      $clientcode->reserve_customer = $data['ecustomer'];
      return $clientcode->save();
    }

    public function importClientCode( $data )
    {
      foreach( $data as $item ){
        $clientcode = new ClientModel();
        $clientcode->code = $item['code'];
        $clientcode->merchant_id = $item['merchant_id'];
        $clientcode->save();
      }
      // DB::table('client')->insert( $data );
    }

    public function get_raw_menu_items( $merchant_id ){

        $menu_items = MenuItemModel
            ::where('merchant_id', $merchant_id)
            ->where('active', 1)
            ->get();

        return $menu_items;

    }

    public function get_raw_menu_item_categories( $merchant_id ){

        $menu_item_categories = MenuItemCategoryModel
            ::where('merchant_id', $merchant_id)
            ->where('active', 1)
            ->get();

        return $menu_item_categories;

    }

    public function get_customer_orders_by_date_time_range( $merchant_id, $start_date_time, $end_date_time, $status = null ){

         // the "id" column in select() is ESSENTIAL!
         $query = OrderModel
            ::where('merchant_id', $merchant_id)
            ->whereRaw("created_at BETWEEN '{$start_date_time}' AND '{$end_date_time}'")
            ->select('id', 'uuid', 'order_number', 'created_at', 'delivery_name', 'status');

         if( ! is_null($status) ) $query->where('status', $status);

         $orders = $query->get();

        return $orders;

    }

   public function get_status_updates_by_date_time_range( $merchant_id, $start_date_time, $end_date_time ){

         $query = OrderModel
            ::where('merchant_id', $merchant_id)
            ->whereRaw("updated_at BETWEEN '{$start_date_time}' AND '{$end_date_time}'")
            ->where('status', '!=', 'Placed')
            ->select('uuid', 'status');

         $order_statuses = $query->get();

        return $order_statuses;

    }

    public function get_order_by_uuid( $order_uuid ){

        $order = OrderModel::where('uuid', $order_uuid)->first();
        return $order;

    }

    public function order_status_update( $order, $status ){

        $order->status = $status;
        $order->save();

        return $order;

    }

    public function get_promo_code( $merchant_id, $promo_code ){

        $promo_code = VoucherCodeModel
            ::where('merchant_id', $merchant_id)
            ->where('code', $promo_code)
            ->where('used', 0)
            ->where('expiration', '>', date('Y-m-d H:i:s'))
            ->first();

        // check promo code voucher if enabled.
        if( $promo_code ){
            if( $promo_code->voucher->enabled == 0 ) return false;
        }

        return $promo_code;

    }

    public function set_promo_code_as_used( $promo_code ){

        if( ! empty($promo_code) ){

            $promo_code->used = 1;
            $promo_code->save();

        }

    }

    public function getPointsSeedingBatchByMerchant( $merchant_id )
    {
      return PointsSeedingBatchModel::where('merchant_id', $merchant_id )->get();
    }

    public function addPointsSeedingBatch( $data )
    {
      $batch = new PointsSeedingBatchModel();
      $batch->merchant_id = $data['merchant_id'];
      $batch->uploader_type = $data['user_type'];
      if ( $data['user_type'] == 1 )
      {
          $batch->uploader_id = $data['merchant_id'];
      } else if ( $data['user_type'] == 2 )
      {
          $batch->uploader_id = $data['subuser_id'];
      } else if ( $data['user_type'] == 3 ) {
          $batch->uploader_id = $data['bcu_id'];
      }
      $batch->save();

      return $batch;
    }

    public function getBranchIdByName( $name, $merchant_id )
    {
      $branch = MerchantBranchesModel::where( ['branchName' => $name, 'merchantId' => $merchant_id ])->select('id');
      if ( $branch->count() > 0 ) {
        $branch = $branch->first();
        return $branch->id;
      }
      return false;
    }

    public function checkSeedingDataStatus( $or_number, $merchant_id )
    {
      $count = CustomerTransactionsModel::where( [ 'receiptReferenceNumber' => $or_number, 'merchant_id' => $merchant_id ] )->count();
      if ( $count > 0 ) {
        return false;
      }
      return true;
    }

    public function buildPointsSeedingDataQuery($request)
    {
        $table_columns = array(
            'points_seeding_data.id',
            'points_seeding_data.transaction_date',
            'points_seeding_data.customer',
            'points_seeding_data.or_number',
            'points_seeding_data.transaction_amount',
            'Branches.branchName',
            'points_seeding_data.status'
        );

        return PointsSeedingDataModel::leftjoin('Branches', 'points_seeding_data.branch', '=', 'Branches.id')
            ->leftjoin('CustomerTransaction', function($query) {
                $query->on('points_seeding_data.or_number', '=', 'CustomerTransaction.receiptReferenceNumber')
                ->on('points_seeding_data.branch', '=', 'CustomerTransaction.branchId');
            })
            ->where('points_seeding_data.batch_id', $request->id)
            ->where(function($query) use ($request) {
                $query->where('points_seeding_data.id', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('points_seeding_data.transaction_date', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('points_seeding_data.customer', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('points_seeding_data.or_number', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('points_seeding_data.transaction_amount', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('Branches.branchName', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('points_seeding_data.status', 'like', '%' . $request->search['value'] . '%');
            })
            ->orderBy($table_columns[$request->order[0]['column']], $request->order[0]['dir'])
            ->select(
              'points_seeding_data.id',
              'points_seeding_data.transaction_date',
              'points_seeding_data.customer',
              'points_seeding_data.or_number',
              'points_seeding_data.transaction_amount',
              'Branches.branchName',
              'points_seeding_data.status'
            )
            ->addSelect( DB::raw('IFNULL(CustomerTransaction.id, NULL) AS transaction_id' ) )
            ;
    }

    public function getPointsSeedingData($query, $request)
    {
        $limit = $request->length;
        $query = clone $query;

        if($limit == -1){
            $filteredCount = $this->countFilterPointsSeedingData($query);
            $limit = ($filteredCount > 10) ? $filteredCount : $this->countAllPointsSeedingData($request);
        }

        return $query->offset($request->start)->limit($limit)->get();
    }

    public function countAllPointsSeedingData($request){
        return PointsSeedingDataModel::where([ 'batch_id' => $request->id ])->count();
    }

    public function countFilterPointsSeedingData($query){
        $query = clone $query;
        return $query->count();
    }

    public function getPointsSeedingDataById( $id )
    {
        return PointsSeedingDataModel::where([ 'id' => $id ])->first();
    }

    public function updateSeedingData( $id )
    {
        $data = PointsSeedingDataModel::find($id);
        $data->status = 0;
        return $data->save();
    }

    public function getVoucherItemData( $id )
    {
      return VoucherModel::where('id', $id )->first();
    }

    public function getVoucherCodes( $id )
    {
      return VoucherCodeModel::where('voucher_item_id', $id )->get();
    }

    public function getVoucherBranches($voucher_id)
    {
        return MerchantBranchesModel::where('merchantId',  $voucher_id)->get();
    }

    public function transferVoucherCode($id, $merchant_id, $voucher_id)
    {
        return VoucherCodeModel::where('id',  $id)->update(['voucher_item_id' => $voucher_id]);
    }


    public function getReceiptData( $id )
    {
      $sql = "SELECT
                  cpct.id as transaction_nunmber,
                  c.fullName as customerName,
                  c.mobileNumber as mobileNumber,
                  b.branchName as branchName,
                  u.employeeName as employeeName,
                  cpct.date_punched as date_punched,
                  CONCAT( 'repository/customers/photos/', c.uuid, '/vouchers/', vc.code ,'.png') as image_file
              FROM `CustomerPunchCardTransactions` cpct
              LEFT JOIN `Customer` c
                ON cpct.customer_id = c.customerId
              LEFT JOIN `Branches` b
                ON cpct.branch_id = b.id
              LEFT JOIN `Users` u
                ON cpct.employee_id = u.userId
              LEFT JOIN `voucher_codes` vc
                ON cpct.voucher_code_id = vc.id
              WHERE cpct.id = $id";
      $transaction = DB::select( $sql );
      return $transaction;
    }

    public function buildPointsSeedingBatchQuery($request)
    {
        $table_columns = array(
            'points_seeding_batch.id',
            'points_seeding_batch.created_at',
            'Merchant.businessName',
            'Users.employeeName',
            'merchant_users.name'
        );

        return PointsSeedingBatchModel::leftjoin('Merchant', 'points_seeding_batch.uploader_id', '=', 'Merchant.merchantid')
            ->leftjoin('Users', 'points_seeding_batch.uploader_id', '=', 'Users.userId')
            ->leftjoin('merchant_users', 'points_seeding_batch.uploader_id', '=', 'merchant_users.id')
            ->where('points_seeding_batch.merchant_id', $request->merchant_id)
            ->where(function($query) use ($request) {
                $query->where('points_seeding_batch.id', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('points_seeding_batch.created_at', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('Merchant.businessName', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('Users.employeeName', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('merchant_users.name', 'like', '%' . $request->search['value'] . '%');
            })
            ->orderBy($table_columns[$request->order[0]['column']], $request->order[0]['dir'])
            ->select(
              'points_seeding_batch.id',
              'points_seeding_batch.created_at'
            )
            ->addSelect( DB::raw( 'CASE points_seeding_batch.uploader_type
                                    WHEN 1 THEN Merchant.businessName
                                    WHEN 2 THEN Users.employeeName
                                    WHEN 3 THEN merchant_users.name
                                  END as customer_name') )
            ->addSelect( DB::raw( '( Select count(*) FROM points_seeding_data WHERE batch_id = points_seeding_batch.id AND status = 1) as valid_data'))
            ->addSelect( DB::raw( '( Select count(*) FROM points_seeding_data WHERE batch_id = points_seeding_batch.id AND status = 0) as invalid_data'))
            ;
    }

    public function countAllPointsSeedingBatch($request){
        return PointsSeedingBatchModel::where([ 'merchant_id' => $request->merchant_id ])->count();
    }

    public function buildClientCodeDataQuery($request)
    {
        $table_columns = array(
            'client.code',
            'Customer.fullName',
            'client.created_at',
            'client.updated_at'
        );

        return ClientModel::leftjoin('Merchant', 'client.merchant_id', '=', 'Merchant.merchantid')
            ->leftjoin('Customer', function($query) {
                $query->on('Customer.merchantId', '=', 'client.merchant_id')
                ->on('Customer.client_code', '=', 'client.code');
            })
            ->where('client.merchant_id', $request->merchant_id )
            ->where(function($query) use ($request) {
                $query->where('client.code', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('client.reserve_customer', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('Customer.fullName', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('client.created_at', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('client.updated_at', 'like', '%' . $request->search['value'] . '%');
            })
            ->orderBy($table_columns[$request->order[0]['column']], $request->order[0]['dir'])
            ->select(
              'client.id',
              'client.code',
              'client.created_at',
              'client.updated_at'
            )
            ->addSelect( DB::raw("CASE WHEN client.reserve_customer IS NOT NULL THEN 'Reserved'
                                  ELSE
                                    CASE WHEN Customer.customerId IS NOT NULL THEN 'Assigned'
                                    ELSE 'Available'
                                    END
                                  END as status"))
            ->addSelect( DB::raw("CASE WHEN client.reserve_customer IS NOT NULL THEN client.reserve_customer
                                  ELSE
                                    CASE WHEN Customer.customerId IS NOT NULL THEN Customer.fullName
                                    ELSE NULL
                                    END
                                  END as customer"))
            ;
    }

    public function getClientCodeData($query, $request)
    {
        $limit = $request->length;
        $query = clone $query;

        if($limit == -1){
            $filteredCount = $query->count();
            $limit = ($filteredCount > 10) ? $filteredCount : $this->countAllClientCodeData($request);
        }

        return $query->offset($request->start)->limit($limit)->get();
    }

    public function countAllClientCodeData($request){
        return ClientModel::where([ 'merchant_id' => $request->merchant_id ])->count();
    }

    public function buildBranchesDataQuery($request)
    {
        $table_columns = array(
            'Branches.branchName',
            'Branches.street_name',
        );

        if ( $request->user_type == 3 ) {
          $employee = MerchantEmployeesModel::where('userId', $request->session()->get('bcu_id'))->first();
          $query = MerchantBranchesModel::where('is_deleted', 0)
                  ->where('id', $employee->branchId );
        } else {
          $query = MerchantBranchesModel::where('is_deleted', 0);
        }

        $query = $query->where('merchantId', $request->merchant_id)
                    ->where(function($query) use ($request) {
                    $query->where('Branches.branchName', 'like', '%' . $request->search['value'] . '%')
                        ->orWhere('Branches.street_name', 'like', '%' . $request->search['value'] . '%')
                        ->orWhere('Branches.district', 'like', '%' . $request->search['value'] . '%')
                        ->orWhere('Branches.city', 'like', '%' . $request->search['value'] . '%')
                        ->orWhere('Branches.zipcode', 'like', '%' . $request->search['value'] . '%');
                      })
                    ->orderBy($table_columns[$request->order[0]['column']], $request->order[0]['dir'])
                    ->select(
                      'Branches.id',
                      'Branches.branchName',
                      'Branches.street_name',
                      'Branches.district',
                      'Branches.city',
                      'Branches.zipcode',
                      'Branches.branch_logo',
                      'Branches.show'
                    )
                    ->addSelect( DB::raw('CONCAT( Branches.street_name," ", Branches.district, " ", Branches.city, " ", Branches.zipcode) as address'));
        return $query;
    }

    public function getBranchesData($query, $request)
    {
        $limit = $request->length;
        $query = clone $query;

        if($limit == -1){
            $filteredCount = $query->count();
            $limit = ($filteredCount > 10) ? $filteredCount : $this->countAllBranchesData($request);
        }
        return $query->offset($request->start)->limit($limit)->get();
    }

    public function countAllBranchesData($request){
        return MerchantBranchesModel::where([ 'merchantId' => $request->merchant_id, 'is_deleted' => 0])->count();
    }

    public function buildEmployeesDataQuery($request)
    {
        $table_columns = array(
            'Users.userId',
            'Users.employeeName',
            'Branches.branchName',
        );

        if ( $request->user_type == 3 ) {
          $employee = MerchantEmployeesModel::where('userId', $request->session()->get('bcu_id'))->first();
          $query = MerchantEmployeesModel::where('Users.is_deleted', 0)
                  ->where('branchId', $employee->branchId );
        } else {
          $query = MerchantEmployeesModel::where('Users.is_deleted', 0);
        }

        $query = $query->leftjoin('Branches', 'Users.branchId', '=', 'Branches.id')
                    ->where('Users.merchantId', $request->merchant_id)
                    ->where(function($query) use ($request) {
                    $query->where('Users.userId', 'like', '%' . $request->search['value'] . '%')
                        ->orWhere('Users.employeeName', 'like', '%' . $request->search['value'] . '%')
                        ->orWhere('Branches.branchName', 'like', '%' . $request->search['value'] . '%')
                        ->orWhere('Branches.status', 'like', '%' . $request->search['value'] . '%');
                      })
                    ->orderBy($table_columns[$request->order[0]['column']], $request->order[0]['dir'])
                    ->select(
                      'Users.userId',
                      'Users.employeeName',
                      'Branches.branchName',
                      'Users.requirePassword',
                      'Users.fourDigit',
                      'Users.branchId'
                    )
                    ->addSelect( DB::raw('CASE Branches.status WHEN 1 THEN "Enabled" ELSE "Disabled" END as employee_status') );
        return $query;
    }

    public function getEmployeesData($query, $request)
    {
        $limit = $request->length;
        $query = clone $query;

        if($limit == -1){
            $filteredCount = $query->count();
            $limit = ($filteredCount > 10) ? $filteredCount : $this->countAllEmployeesData($request);
        }

        return $query->offset($request->start)->limit($limit)->get();
    }

    public function countAllEmployeesData($request){
        return MerchantEmployeesModel::where([ 'merchantId' => $request->merchant_id ])->count();
    }

    public function buildVotesDataQuery($request)
    {
        $table_columns = array(
            'votes.id',
            'votes.created_at',
            'Customer.fullName',
            'merchant_posts.post_title',
            'votes.vote'
        );

        return MerchantVotesModel::leftjoin('Customer', 'votes.customer_id', '=', 'Customer.customerId')
            ->leftjoin('merchant_posts', 'votes.post_id', '=', 'merchant_posts.id')
            ->where(function($query) use ($request) {
                $query->where('votes.created_at', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('Customer.fullName', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('merchant_posts.post_title', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('votes.vote', 'like', '%' . $request->search['value'] . '%');
            })
            ->orderBy($table_columns[$request->order[0]['column']], $request->order[0]['dir'])
            ->select(
                'votes.id',
                'votes.created_at',
                'Customer.fullName',
                'merchant_posts.post_title',
                'votes.vote'
            );
    }

    public function getVotesData($query, $request)
    {
        $limit = $request->length;
        $query = clone $query;

        if($limit == -1){
            $filteredCount = $this->countFilterVotesData($query);
            $limit = ($filteredCount > 10) ? $filteredCount : $this->countAllVotesData($request);
        }

        return $query->offset($request->start)->limit($limit)->get();
    }

    public function countFilterVotesData($query){
        $query = clone $query;
        return $query->count();
    }

    public function countAllVotesData($request){
        return MerchantVotesModel::where([ 'merchant_id' => $request->merchant_id ])->count();
    }

    public function getMerchantQuizQuestions( $merchant_id )
    {
        $questions = QuizQuestionModel::where('merchant_id', $merchant_id)->get();
        return $questions;
    }

    public function getQuizQuestionChoices( $questions )
    {
        $quiz_question_ids = [];

        foreach( $questions as $question ){
            $quiz_question_ids[] = $question->id;
        }

        $choices = QuizQuestionChoiceModel::whereIn('question_id', $quiz_question_ids)->get();
        return $choices;
    }

    public function getMerchantPointsPromos( $merchant_id )
    {
        return PointsPromoModel
            ::where('merchant_id', $merchant_id)
            ->where('status', 1)
            ->get();
    }

    public function getMerchantByMerchantCode( $merchant_code )
    {
        $merchant_code = MerchantCodeModel
            ::where('code', $merchant_code)
            ->where('enabled', 1)
            ->first();

        if( $merchant_code ) return $merchant_code->merchant;
    }

    public function getPointsSeedingDataOrm($batch_id){
        return PointsSeedingDataModel::where([ 'batch_id' => $batch_id ])->get();
    }

    public function getCities()
    {
        return MerchantCityModel::pluck('city');
    }
}
