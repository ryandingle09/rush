<?php
namespace Rush\Modules\Merchant\Repositories;

use Rush\Modules\Repository\RepositoryInterface;

/**
 * Interface MerchantInterface
 * @package Rush\Modules\Merchant\Repositories
 */
interface MerchantInterface extends RepositoryInterface
{

    /**
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|\Rush\Modules\Merchant\Models\MerchantModel
     */
    public function getById($id);

    public function getByUsername($username);
}
