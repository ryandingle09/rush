<?php

namespace Rush\Modules\Merchant\Repositories;

use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use Rush\Modules\Merchant\Models\MerchantModel;
use Rush\Modules\Merchant\Models\MerchantEmployeesModel;
use Rush\Modules\Repository\AbstractRepository;
use Rush\Modules\Merchant\Models\MerchantBranchesModel;
use Rush\Modules\Merchant\Models\MerchantBranchesDateQrModel;

/**
 * Class BranchRepository
 * @package Rush\Modules\Merchant\Repositories
 */
class BranchRepository extends AbstractRepository implements BranchInterface
{
    public $merchantModel;
    public $basicColumns = array(
        'uuid',
        'branchName as name',
        'address',
        'street_name',
        'district',
        'city',
        'zipcode',
        'longitude',
        'latitude',
    );

    public function __construct( MerchantBranchesModel $merchantBranchesModel, MerchantModel $merchantModel ){

        parent::__construct( $merchantBranchesModel );
        $this->merchantModel = $merchantModel;

    }

    public function getMerchantBranches( $merchant_id ){

        $merchant = MerchantModel::find( $merchant_id );

        $response['error_code'] = '0x0';
        $response['data'] = $merchant->branches()->select( $this->basicColumns )->get();

        return $response;

    }

    public function returnDetailsErrors( $errors ){

        $response['error_code'] = '0x1';
        $response['errors'] = $errors;

        return $response;

    }

    public function createBranch( $merchant_id, $request ){

        $this->model->merchantId = $merchant_id;
        $this->model->branchName = $request->branch_name;
        $this->model->street_name = $request->street_name;
        $this->model->district = $request->district;
        $this->model->city = $request->city;
        $this->model->zipcode = $request->zipcode;
        $this->model->longitude = $request->longitude;
        $this->model->latitude = $request->latitude;
        $this->model->address = $request->street_name . ', ' . $request->district . ', ' . $request->city . ', ' . $request->zipcode;
        $this->model->uuid = Uuid::uuid1()->toString();
        $this->model->save();

        $response['error_code'] = '0x0';
        $response['data'] = $request->all();
        $response['data']['address'] = $request->street_name . ', ' . $request->district . ', ' . $request->city . ', ' . $request->zipcode;

        return $response;

    }

    public function getSingleBranch( $uuid ){

        $response['error_code'] = '0x0';
        $response['data'] = $this->model->select( $this->basicColumns )->where('uuid', $uuid)->first();

        return $response;

    }

    /**
     *  Get branch by ID or UUID
     * @param  integer|string $id
     * @param  array $args
     * @return mixed|null
     */
    public function find($id, $args)
    {
        if (is_int($id)) {
            $args = array_merge($args, ['id' => $id]);
        } else {
            $args = array_merge($args, ['uuid' => $id]);
        }

        return $this->model->where($args)->first();
    }

    public function findByBranchName($branchName, $merchantId, $idToIgnore = null)
    {
        $whereData = [
            ['merchantId', $merchantId],
            ['branchName', $branchName],
            ['is_deleted', '0'],
        ];

        if ($idToIgnore) {
            array_push($whereData, ['id', '!=', $idToIgnore]);
        }

        return $this->model->where($whereData)->first();
    }

    public function updateBranch( $uuid, $request ){

        $branch = MerchantBranchesModel::where('uuid', $uuid)->first();
        $branch->branchName = $request->branch_name;
        $branch->street_name = $request->street_name;
        $branch->district = $request->district;
        $branch->city = $request->city;
        $branch->zipcode = $request->zipcode;
        $branch->longitude = $request->longitude;
        $branch->latitude = $request->latitude;
        $branch->address = $request->street_name . ', ' . $request->district . ', ' . $request->city . ', ' . $request->zipcode;
        $branch->save();

        $response['error_code'] = '0x0';
        $response['data'] = $request->all();
        $response['data']['address'] = $request->street_name . ', ' . $request->district . ', ' . $request->city . ', ' . $request->zipcode;

        return $response;

    }

    function deleteBranch( $uuid ){

        $branch = MerchantBranchesModel::where('uuid', $uuid)->first();
        $branch->is_deleted = 1;
        $branch->save();
        $branch->delete();

        $response['error_code'] = '0x0';

        return $response;

    }

    public function getBranchesByMerchant( $id )
    {
        return $this->model->where('merchantId', $id)->where('is_deleted','!=', 1)->get();
    }

    public function getListByMerchant($merchantId)
    {
        return $this->model->where(['merchantId' => $merchantId, 'is_deleted' => 0])->get()->lists('branchName', 'id');
    }

    public function addBranch( $data )
    {
        $this->model->branchName = $data['branchName'];
        $this->model->street_name = $data['street_name'];
        $this->model->district = $data['district'];
        $this->model->city = $data['city'];
        $this->model->zipcode = $data['zipcode'];
        $this->model->longitude = $data['longitude'];
        $this->model->latitude = $data['latitude'];
        $this->model->status = $data['status'];
        $this->model->merchantId = $data['merchantId'];
        if ( isset($data['branch_logo']) && !empty($data['branch_logo']) ) {
            $this->model->branch_logo = $data['branch_logo'];
        }
        $this->model->show = $data['show'];
        return $this->model->save();
    }

    public function getBranchLogo( $id )
    {
        $branchLogo = null;
        $branch = MerchantBranchesModel::where('id', $id)->select('branch_logo');
        if ( $branch->count() > 0 )
        {
            $result = $branch->first();
            $branchLogo = $result->branch_logo;
        }
        return $branchLogo;
    }

    public function update_branch($merchantId, $id, $data)
    {
        $branch = MerchantBranchesModel::where( ['id' => $id, 'merchantId' => $merchantId] )->first();
        $branch->branchName = $data['branchName'];
        $branch->street_name = $data['street_name'];
        $branch->district = $data['district'];
        $branch->city = $data['city'];
        $branch->zipcode = $data['zipcode'];
        if ( isset($data['branch_logo']) && !empty($data['branch_logo']) ) {
            $branch->branch_logo = $data['branch_logo'];
        }
        $branch->show = $data['show'];
        return $branch->save();
    }

    public function delete_branch($merchantId,$id)
    {
        $data = array(
            'is_deleted' => 1,
            'deleted_at' => date('Y-m-d H:is'),
            'deleted_by' => $merchantId,
            'branch_logo' => null
        );
        $branch = MerchantBranchesModel::where( ['id' => $id, 'merchantId' => $merchantId] )->first();
        $branch->is_deleted = 1;
        $branch->deleted_at = Carbon::now();
        $branch->deleted_by = $merchantId;
        $branch->branch_logo = null;
        return $branch->save();
    }

    public function getRedemptionBranch($merchant)
    {
        return $merchant->branches()->where('enable_redemption', 1)->get();
    }

    /**
     * @param \Rush\Modules\Merchant\Models\MerchantBranchesModel $branch
     * @param \Carbon\Carbon|bool $date
     * @param bool|false $create
     * @return mixed
     */
    public function getDateQrCode($branch, $date, $create = false)
    {
        if ($date == false) {
            $qr_code = $branch->date_qr_codes()->orderBy('id', 'desc')->first();
        } else {
            $qr_code = $branch->date_qr_codes()->where('date_generated', $date->format('Y-m-d'))->first();
        }

        if (!$qr_code && $create) {
            $name = 'branch' . '-' . $branch->id . '-' . $date->timestamp;
            $uuid = Uuid::uuid3(Uuid::NAMESPACE_DNS, $name);
            $qr_code = $branch->date_qr_codes()->create(['branch_id' => $branch->id, 'uuid' => $uuid->toString(), 'date_generated' => $date->format('Y-m-d')]);
        }

        return $qr_code;
    }

    /**
     * @param string $date_qr_uuid
     * @return \Rush\Modules\Merchant\Models\MerchantBranchesModel
     */
    public function getBranchByDateQrUuid($date_qr_uuid)
    {
        /** @var MerchantBranchesDateQrModel $date_qr_model */
        $date_qr_model =  MerchantBranchesDateQrModel::where('uuid', $date_qr_uuid)->first();

        return $date_qr_model ? $date_qr_model->branch : null;
    }

    public function getBranchEmployee( $branch_id ){

        return MerchantEmployeesModel::where('branchId', $branch_id)->first();

    }
}
