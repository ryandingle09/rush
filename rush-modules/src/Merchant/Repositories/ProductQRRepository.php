<?php
namespace Rush\Modules\Merchant\Repositories;

use Rush\Modules\Repository\AbstractRepository;
use Rush\Modules\Merchant\Models\QRCodeModel;

class ProductQRRepository extends AbstractRepository implements ProductQRInterface
{
    public function __construct(QRCodeModel $qr_code_model)
    {
        parent::__construct($qr_code_model);
    }

    public function getAll($merchant_id){
        return $this->model->where('merchant_id', $merchant_id)->orderBy('created_at', 'desc')->get();
    }

    public function storeProductQR($product_qr_data)
    {
        $product_qr = new $this->model;

        $product_qr->uuid = $product_qr_data['uuid'];
        $product_qr->name = $product_qr_data['name'];
        $product_qr->load = $product_qr_data['load'];
        $product_qr->points = $product_qr_data['points'];
        $product_qr->customer_scan_limit = $product_qr_data['customer_scan_limit'];
        $product_qr->branch_ids = '[' . implode( ',', $product_qr_data['branch_ids']) . ']';
        $product_qr->type = $product_qr_data['type'];
        $product_qr->active = $product_qr_data['active'];
        $product_qr->merchant_id = $product_qr_data['merchant_id'];

        return $product_qr->save();
    }

    public function updateProductQR($product_qr_id, $product_qr_data)
    {
        $product_qr = $this->model->where('id', $product_qr_id)->first();

        $product_qr->name = $product_qr_data['name'];
        $product_qr->load = $product_qr_data['load'];
        $product_qr->points = $product_qr_data['points'];
        $product_qr->customer_scan_limit = $product_qr_data['customer_scan_limit'];
        $product_qr->branch_ids = '[' . implode(',', $product_qr_data['branch_ids']) . ']';
        $product_qr->type = $product_qr_data['type'];
        $product_qr->active = $product_qr_data['active'];

        return $product_qr->save();
    }
}