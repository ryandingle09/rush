<?php

namespace Rush\Modules\Merchant\Repositories;

use Rush\Modules\Repository\RepositoryInterface;

interface SmsNotificationInterface extends RepositoryInterface
{
    public function getMessage($type, $merchantId = null);
}
