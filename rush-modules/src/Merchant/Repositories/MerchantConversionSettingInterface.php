<?php

namespace Rush\Modules\Merchant\Repositories;

use Rush\Modules\Repository\RepositoryInterface;

/**
 * The MerchantConversionSettingInterface contains unique method signatures related to MerchantConversionSetting object
 */
interface MerchantConversionSettingInterface extends RepositoryInterface
{
    public function getByMerchantId($merchantId);
}
