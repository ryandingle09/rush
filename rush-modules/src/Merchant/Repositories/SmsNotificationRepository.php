<?php

namespace Rush\Modules\Merchant\Repositories;

use Rush\Modules\Repository\AbstractRepository;
use Rush\Modules\Merchant\Models\NotificationSmsMessagesModel;
use Rush\Modules\Merchant\Repositories\SmsNotificationInterface;

class SmsNotificationRepository extends AbstractRepository implements SmsNotificationInterface
{
    public function __construct(NotificationSmsMessagesModel $notificationModel)
    {
        parent::__construct($notificationModel);
    }

    public function getDefaultMessage($type)
    {
        return $this->model->where('merchant_id', 0)->where('type', $type)->first();
    }

    public function getMessage($type, $merchantId = null)
    {
        $message = null;
        if ($merchantId) {
            $message = $this->model->where('merchant_id', $merchantId)->where('type', $type)->first();
        }
        if (!$message) {
            $message = $this->getDefaultMessage($type);
        }

        return ($message) ? $message->message: '';
    }
}
