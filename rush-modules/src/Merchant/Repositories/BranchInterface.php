<?php
namespace Rush\Modules\Merchant\Repositories;

use Rush\Modules\Repository\RepositoryInterface;

/**
 * Interface BranchInterface
 * @package Rush\Modules\Merchant\Repositories
 */
interface BranchInterface extends RepositoryInterface
{

    /**
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|\Rush\Modules\Merchant\Models\MerchantBranchesModel
     */
    public function getById($id);

    /**
     * @param  int $merchantId
     * @return array
     */
    public function getListByMerchant($merchantId);
}
