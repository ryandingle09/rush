<?php

namespace Rush\Modules\Merchant\Repositories;

use Rush\Modules\Repository\AbstractRepository;
use Rush\Modules\Merchant\Repositories\EmployeeInterface;
use Rush\Modules\Merchant\Models\MerchantEmployeesModel;
use Rush\Modules\Merchant\Models\MerchantEmployeeDeviceSessionsModel;
use Rush\Modules\Merchant\Models\MerchantModel;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;

class EmployeeRepository extends AbstractRepository implements EmployeeInterface
{

    public $basicColumns = array(
        'uuid',
        'merchantId as merchant_id',
        'branchId as branch_id',
        'employeeName as name',
        'fourDigit as pin',
        'requirePassword as require_password',
    );

    public function __construct( MerchantEmployeesModel $merchantEmployeesModel ){

        parent::__construct( $merchantEmployeesModel );

    }

    public function getMerchantEmployees( $merchant_id ){

        $merchant = MerchantModel::find( $merchant_id );

        $response['error_code'] = '0x0';
        $response['data'] = $merchant->employees()->select( $this->basicColumns )->get();

        return $response;

    }

    public function returnDetailsErrors( $errors ){

        $response['error_code'] = '0x1';
        $response['errors'] = $errors;

        return $response;

    }

    public function createEmployee( $merchant_id, $request ){

        $this->model->merchantId = $merchant_id;
        $this->model->branchId = $request->branch_id;
        $this->model->employeeName = $request->name;
        $this->model->fourDigit = $request->pin;
        $this->model->requirePassword = $request->require_password;
        $this->model->uuid = Uuid::uuid1()->toString();
        $this->model->save();

        $response['error_code'] = '0x0';
        $response['data'] = $request->all();

        return $response;

    }

    public function getSingleEmployee( $uuid ){

        $employee = MerchantEmployeesModel::select( $this->basicColumns )->where('uuid', $uuid)->first();

        $response['error_code'] = '0x0';
        $response['data'] = $employee;

        return $response;

    }

    public function findByUuid( $uuid ){

        return $this->model->where('uuid', $uuid)->first();

    }

    public function updateEmployee( $uuid, $request ){

        $emmployee = MerchantEmployeesModel::where('uuid', $uuid)->first();
        $emmployee->branchId = $request->branch_id;
        $emmployee->employeeName = $request->name;
        $emmployee->fourDigit = $request->pin;
        $emmployee->requirePassword = $request->require_password;
        $emmployee->save();

        $response['error_code'] = '0x0';
        $response['data'] = $request->all();

        return $response;

    }

    public function deleteEmployee( $uuid ){

        $emmployee = MerchantEmployeesModel::where('uuid', $uuid)->first();
        $emmployee->is_deleted = 1;
        $emmployee->save();
        $emmployee->delete();

        $response['error_code'] = '0x0';

        return $response;

    }

    public function getEmployeesByMerchant( $merchant_id )
    {
      return MerchantEmployeesModel::where([ 'merchantId' => $merchant_id ])->get();
    }


    public function addEmployee( $data ) {
        $this->model->employeeName = $data['employeeName'];
        $this->model->branchId = $data['branchId'];
        $this->model->fourDigit = $data['fourDigit'];
        $this->model->requirePassword = $data['requirePassword'];
        $this->model->merchantId = $data['merchantId'];
        return $this->model->save();
    }

    public function update_employee($merchantId,$employeeId,$data)
    {
        $employee = MerchantEmployeesModel::where( ['userId' => $employeeId, 'merchantId' => $merchantId] )->first();
        $employee->employeeName = $data['employeeName'];
        $employee->branchId = $data['branchId'];
        $employee->fourDigit = $data['fourDigit'];
        $employee->requirePassword = $data['requirePassword'];
        return $employee->save();
    }

    public function delete_employee($merchantId,$id)
    {
        $employee = MerchantEmployeesModel::where( ['userId' => $id, 'merchantId' => $merchantId] )->first();
        $employee->is_deleted = 1;
        $employee->deleted_at = Carbon::now();
        $employee->deleted_by = $merchantId;
        return $employee->save();
    }

    public function getMerchantSystemEmployee($merchantId, $username)
    {
        return $this->model->where(['merchantId' => $merchantId, 'username' => $username])->first();
    }

    public function saveDeviceSession($employee_id, $device_id)
    {
        $this->destroyDeviceSession($employee_id, $device_id);

        $session = new MerchantEmployeeDeviceSessionsModel;
        $session->employee_id = $employee_id;
        $session->device_id =  $device_id;
        $session->save();

        return $session;
    }

    public function getDeviceSession($employee_id, $device_id)
    {
        $session = MerchantEmployeeDeviceSessionsModel::where('employee_id', $employee_id)
                        ->where('device_id', $device_id)
                        ->first();

        return $session;
    }

    public function destroyDeviceSession($employee_id, $device_id)
    {
        $session = MerchantEmployeeDeviceSessionsModel::where('employee_id', $employee_id)->delete();

        return $session;
    }
}
