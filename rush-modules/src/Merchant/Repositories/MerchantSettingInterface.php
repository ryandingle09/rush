<?php

namespace Rush\Modules\Merchant\Repositories;

use Rush\Modules\Repository\RepositoryInterface;

/**
 * The MerchantSettingInterface contains unique method signatures related to MerchantSetting object
 */
interface MerchantSettingInterface extends RepositoryInterface
{
    public function getByMerchantId($merchantId);
}
