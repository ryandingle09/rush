<?php

namespace Rush\Modules\Merchant\Repositories;

use Rush\Modules\Merchant\Models\MerchantConversionSettingModel;
use Rush\Modules\Merchant\Repositories\MerchantConversionSettingInterface;
use Rush\Modules\Repository\AbstractRepository;

/**
 * A repository class would encapsulate each Eloquent Model class
 * and be responsible for CRUD operations on the database.
 */
class MerchantConversionSettingRepository extends AbstractRepository implements MerchantConversionSettingInterface
{
    public function __construct(MerchantConversionSettingModel $merchantConversionSettingModel)
    {
        parent::__construct($merchantConversionSettingModel);
    }

    public function getByMerchantId($merchantId)
    {
        return $this->model->where('merchant_id', $merchantId)->get();
    }
}
