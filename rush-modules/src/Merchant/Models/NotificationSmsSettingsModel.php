<?php

namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;
use Rush\Modules\Merchant\Models\MerchantModel;

class NotificationSmsSettingsModel extends Model
{
  protected $table = "notification_sms_settings";

  public function merchant()
  {
      return $this->belongsTo(MerchantModel::class, 'merchant_id', 'merchantid');
  }
}
