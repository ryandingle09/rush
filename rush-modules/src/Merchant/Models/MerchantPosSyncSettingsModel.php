<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantPosSyncSettingsModel extends Model
{
    protected $table = "pos_sync_settings";
}