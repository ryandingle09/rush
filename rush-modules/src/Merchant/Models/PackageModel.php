<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * Class MerchantModel
 * @package Rush\Modules\Merchant\Models
 *
 * @property integer $id
 * @property integer $packageId
 * @property \Rush\Modules\Merchant\Models\MerchantConversionSettingModel $conversion_setting
 * @property \Rush\Modules\Customer\Models\CustomerModel[] $customers
 *
 */
class PackageModel extends Model
{
    protected $table = "Package";
    protected $primaryKey = "packageId";
}
