<?php

namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;
use Rush\Modules\Helpers\Facades\StorageHelper;
use DB;

class MerchantRewardsFactoryModel extends Model
{
    private $rewardModelNamespace = 'Rush\Modules\Merchant\Models\\';
    private $rewardTables = ['ItemsForRedeem', 'MerchantGlobeRewards'];  // key will represent the reward type.
    private $rewardModels = ['MerchantRewardsModel', 'MerchantGlobeRewardsModel'];  // corresponding models of reward tables.
    private $merchant;

    public function __construct( $merchant_id ){

        $this->merchant = DB::table('Merchant')->where('merchantid', $merchant_id)->first();
        if( is_null($this->merchant) ) throw new \Exception('Merchant not found');

    }

    private function getRewardGroups(){

        $rewardGroups = [];

        foreach( $this->rewardModels as $model_name ){
            
            $model = $this->rewardModelNamespace . $model_name;

            $rewards = $model
                ::where('merchantid', $this->merchant->merchantid)
                ->where('status', 1)
                ->orderBy('pointsRequired')
                ->get();

            $rewardGroups[ $model_name ] = $rewards;

        }

        return $rewardGroups;

    }

    public function getRewardsCatalog( $customer = null ){

        $rewardGroups = [];  // will contain grouped results of rewards per table.
        $catalog = [];  // will contain the rewards catalog to be returned.
        $customer_mobile_network = ( isset($customer) ? $customer->mobile_network : '' );

        $rewardGroups = $this->getRewardGroups();

        // filter and format rewards as catalog.
        foreach( $rewardGroups as $model_name => $rewards ){
            foreach( $rewards as $reward ){

                $type = (int) array_search($model_name, $this->rewardModels);
                $image_url = ( $reward->imageURL ? StorageHelper::repositoryUrlForView($reward->imageURL) : '' );

                // filter for globe rewards.
                if( $model_name == 'MerchantGlobeRewardsModel' ){
                    // customer can be prepaid, postpaid, tm subscriber.
                    // return only the globe reward that is available to customer subscription.
                    if( ! in_array($customer_mobile_network, (array) json_decode($reward->type)) ) continue;
                }

                $catalog[] = [
                    'uuid' => (string) $reward->uuid,
                    'type' => (int) $type,
                    'name' => (string) $reward->name,
                    'details' => (string) $reward->details,
                    'points' => (float) $reward->pointsRequired,
                    'image_url' => $image_url,
                    'branches' => $reward->branches,
                ];

            }
        }

        return $catalog;

    }

    public function getRewardByUuid( $uuid ){

        $reward = null;

        foreach( $this->rewardModels as $reward_model ){

            $model = $this->rewardModelNamespace . $reward_model;

            $reward = $model
                ::where('merchantid', $this->merchant->merchantid)
                ->where('status', 1)
                ->where('uuid', $uuid)
                ->first();

            if( $reward ) break;

        }

        return $reward;

    }
}
