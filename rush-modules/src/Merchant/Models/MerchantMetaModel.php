<?php

namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantMetaModel extends Model
{
    protected $table = "merchant_meta";
}