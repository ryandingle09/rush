<?php

namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantCodeModel extends Model{
    
    protected $table = "MerchantCode";
    
    public function merchant(){
        
        return $this->belongsTo('Rush\Modules\Merchant\Models\MerchantModel', 'merchant_id', 'merchantid');
        
    }
    
}
