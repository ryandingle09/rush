<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class BcuModulesModel extends Model
{
    protected $table = "bcu_modules";
    protected $primaryKey = "id";
}
