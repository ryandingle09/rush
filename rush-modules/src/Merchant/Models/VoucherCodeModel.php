<?php

namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class VoucherCodeModel extends Model
{
    protected $table = 'voucher_codes';

    protected $dates = ['created_at', 'updated_at', 'expiration'];

    public function voucher()
    {
        return $this->belongsTo('Rush\Modules\Merchant\Models\VoucherModel', 'voucher_item_id');
    }
    public function getIssuedAttribute()
    {
        if ( $this->send ) {
        	return 'Issued';
        } else {
        	return '';
        }
    }
}