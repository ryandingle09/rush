<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class LoyaltyCardOrderModel
 * @package Rush\Modules\Merchant\Models
 *
 */
class LoyaltyCardOrderModel extends Model
{
    protected $table = "LoyaltyCardOrder";
    protected $primaryKey = "id";
}
