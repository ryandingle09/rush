<?php

namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class MembershipBranchModel extends Model
{
    protected $table = "membership_branch";
    public $timestamps = true;
    protected $fillable = [
        'membership_id',
        'branch_id'
    ];

    /**
     * defines the type string of this model when used in polymorphic relationships
     * @var string
     */
    protected $morphClass = 'MembershipBranch';

    /**
     * @return Rush\Modules\Merchant\Models\MerchantConversionSetting
     */
    public function conversion_setting()
    {
        return $this->morphOne('\Rush\Modules\Merchant\Models\MerchantConversionSettingModel', 'transformable');
    }

    public function membership()
    {
        return $this->belongsTo('\Rush\Modules\Merchant\Models\MembershipModel');
    }

    public function branch()
    {
        return $this->belongsTo('\Rush\Modules\Merchant\Models\MerchantBranchesModel');
    }
}
