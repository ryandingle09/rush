<?php

namespace Rush\Modules\Merchant\Models;

use Endroid\QrCode\QrCode;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Intervention\Image\ImageManager;
use Logaretm\Transformers\Contracts\Transformable;
use Logaretm\Transformers\TransformableTrait;
use Rush\Modules\Helpers\Facades\StorageHelper;
use Rush\Modules\Merchant\Models\Traits\Branch\AttributesTrait;
use Rush\Modules\Merchant\Models\Traits\Branch\RelationshipsTrait;

/**
 * Class MerchantBranchesModel
 *
 * @property-read    integer                                                            $id
 * @property         string                                                             $uuid
 * @property         string                                                             $name
 * @property         mixed                                                              $qr
 * @property-read    \Rush\Modules\Merchant\Models\MerchantBranchesDateQrModel          $date_qr_codes
 * @property-read    \Rush\Modules\Merchant\Models\MerchantModel                        $merchant
 * @property-read    \Rush\Modules\Merchant\Models\MerchantEmployeesModel               $employee
 * @property-read    integer                                                            $enable_earn_stamp
 */
class MerchantBranchesModel extends Model implements Transformable
{
    use TransformableTrait, AttributesTrait, RelationshipsTrait;
    use SoftDeletes;

    protected $table = "Branches";
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'merchantId',
        'branchName',
        'enable_earn_stamp'
    ];

    /**
     * defines the type string of this model when used in polymorphic relationships
     * @var string
     */
    protected $morphClass = 'Branch';

    public function getisDeletedTextAttribute()
    {
      if ( $this->is_deleted ) return "disabled";
      else return "enabled";
    }

    /**
     * @return Rush\Modules\Merchant\Models\MerchantConversionSetting
     */
    public function conversion_setting()
    {
        return $this->morphOne('\Rush\Modules\Merchant\Models\MerchantConversionSettingModel', 'transformable');
    }

    public function getStatusTextAttribute()
    {
        if ( $this->status == 1 ) return "Enabled";
        else return "Disabled";
    }


    public function generateQR()
    {
        $image_manager = new ImageManager(array('driver' => 'gd'));
        $disk = StorageHelper::getRepositoryDisk();

        $qr = new QrCode();
        $qr->setText($this->uuid);
        $qr->setSize(300);

        $path = "/branches/qrcode/" . $this->uuid . ".png";
        $image = $image_manager->make($qr->getDataUri());
        $disk->put($path, $image->stream()->__toString(), 'public');

        return '/repository'. $path;
    }

    protected function qrExists()
    {
        $disk = StorageHelper::getRepositoryDisk();
        $path = "/branches/qrcode/" . $this->uuid . ".png";

        return $disk->exists($path);
    }

    public function getQrCodeAttribute()
    {
        $disk = StorageHelper::getRepositoryDisk();
        $file_path = 'branches/qrcode/'. $this->uuid . '.png';

        if (!$disk->exists($file_path)) {
            $image_manager = new ImageManager(array('driver' => 'gd'));
            $qr_code = new QrCode();
            $qr_code->setText($this->uuid);
            $qr_code->setSize(300);
            $image = $image_manager->make($qr_code->getImage());
            $disk->put($file_path, $image->stream()->__toString(), 'public');
        }

        if (!$this->qrExists()) {
            $this->generateQR();
        }

        return StorageHelper::repositoryUrlForView($file_path);
    }

    public function __get($key)
    {
        $value =  $this->getAttribute($key);
        if (! $value) {
            // try checking meta
            $value = $this->metas()->where('name', $key)->value('value');
        }

        return $value;
    }

    public function classes()
    {
        return $this->belongsToMany(
            'Rush\Modules\ClassManagement\Models\ClassModel',
            'class_branch',
            'branch_id',
            'class_id'
        );

    }

    public function getBranchLogoAttribute($value)
    {
        return $value;
    }
}
