<?php

namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;
use Rush\Modules\Customer\Models\CustomerModel;

class ClientModel extends Model
{
    protected $table = "client";
    protected $primaryKey = "id";

    public function merchant()
    {    
        return $this->belongsTo('Rush\Modules\Merchant\Models\MerchantModel', 'merchant_id', 'merchantid');   
    }
}
