<?php

namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;
use Rush\Modules\Merchant\Models\MindbodyClassModel;
use Rush\Modules\Merchant\Models\MindbodyInstructorModel;

class MindbodyScheduleModel extends Model
{
    protected $table = 'merchant_mindbody_schedules';

    public function scheduled_class()
    {
        return $this->hasOne(MindbodyClassModel::class, 'id', 'class_id');
    }

    public function instructor()
    {
        return $this->hasOne(MindbodyInstructorModel::class, 'id', 'instructor_id');
    }
}
