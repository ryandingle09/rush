<?php

namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;
use Rush\Modules\Merchant\Models\MerchantModel;
use Rush\Modules\Merchant\Models\MerchantPunchCardRewardsModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class MerchantPromoMechanicsModel extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = "MerchantPromoMechanics";

    public function merchant()
    {
        return $this->belongsTo(MerchantModel::class, 'merchant_id', 'merchantid');
    }

    public function rewards()
    {
        return $this->hasMany('Rush\Modules\Merchant\Models\MerchantPunchCardRewardsModel', 'promo_id', 'id');
    }

    public function getDataAttribute()
    {
        $promo = [];
        $promo['id'] = $this->uuid;
        $promo['name'] = $this->promo_title;
        $promo['stamps'] = (int) $this->num_stamps;
        $promo['duration'] = [
            'start' => $this->duration_from_date,
            'end'   => $this->duration_to_date
        ];
        $promo['redemption'] = [
            'start' => $this->redemption_duration_from_date,
            'end'   => $this->redemption_duration_to_date
        ];
        $promo['multiplier'] = (int) $this->multiplier;
        $promo['amount'] = (float) $this->amount_per_stamp;
        $promo['rewards'] = $this->promo_rewards;
        $promo['status'] = $this->status;

        return $promo;
    }

    public function getAmountPerStampAttribute()
    {
       return  (float) MerchantPromoAmountStampModel::where('promo_id', $this->id)->pluck('amount')->first();
    }

    public function getPromoRewardsAttribute()
    {
        $temp_rewards = $this->rewards->filter(function($reward){
            return $reward->include == true;
        });
        $rewards = [] ;
        foreach($temp_rewards as $reward)
        {
            $rewards[] = $reward->data;
        }

        return $rewards;
    }

    public function getStatusTextAttribute()
    {
        $now = strtotime(Date('Y-m-d'));
        $start_ts = strtotime($this->duration_from_date);
        $end_ts = strtotime($this->duration_to_date);

        if($this->status == 1) { // published
            $status = "Pending";

            //check if expired
            if( $end_ts < $now )  {
                $status = "Expired";
                return $status;
            }

            // check if pending
            if(  $now >= $start_ts && $now <= $end_ts )  {
                $status = "Active";
            }
        } else {
            $status = "Draft";
        }

        return $status;
    }

    public function addRewardToStamps($stamp_no)
    {
        $reward = new MerchantPunchCardRewardsModel;
        $reward->merchant_id = $this->merchant_id;
        $reward->promo_id = $this->id;
        $reward->no_stamps = $stamp_no;
        $reward->reward = $this->merchant->default_reward_name;
        $reward->details = $this->merchant->default_reward_details;
        $reward->image_url = $this->merchant->default_reward_image;
        $reward->save();
        return $reward;
    }
}
