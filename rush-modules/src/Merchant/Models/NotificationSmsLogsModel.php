<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Rush\Modules\Merchant\Models\NotificationSmsLogModel
 *
 * @property integer $id
 * @mixin \Eloquent
 */
class NotificationSmsLogsModel extends Model
{
  protected $table = "notification_sms_logs";
  protected $primaryKey = "id";
}
