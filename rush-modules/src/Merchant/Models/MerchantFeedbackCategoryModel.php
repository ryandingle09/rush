<?php

namespace Rush\Modules\Merchant\Models;

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;
use Logaretm\Transformers\TransformableTrait;
use Logaretm\Transformers\Contracts\Transformable;

class MerchantFeedbackCategoryModel extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'merchant_feedback_categories';

    /**
     * Uuid property
     * 
     * Generate uuid if its null
     *
     * @param string $value
     * @return string 
     */
    public function getUuidAttribute($value)
    {
        if (!$value) {
            $value = Uuid::uuid1()->toString();
            $this->uuid = $value;
            $this->save();
        }

        return $value;
    }
}