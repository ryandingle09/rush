<?php

namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantSubUsersModel extends Model
{
    protected $table = "merchant_users";
    protected $primaryKey = "id";
    
    public function merchant()
    {
        return $this->belongsTo('Rush\Modules\Merchant\Models\MerchantModel', 'merchant_id', 'merchantid');
    }

    public function modules()
    {
    	return $this->hasMany('Rush\Modules\Customer\Models\MerchantSubUsersModulesModel', 'user_id', 'id');
    }

    public function getStatusAttribute()
    {
        if ( $this->is_deleted == 0 )
            return "enabled";
        else {
            return 'disabled';
        }
    }
}
