<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class AmaxApiCredentialsModel extends Model
{
    protected $table = "AmaxApiCredentials";
}
