<?php

namespace Rush\Modules\Merchant\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PointsSeedingDataModel extends Model
{
    use SoftDeletes;

    protected $table = "points_seeding_data";
    protected $primaryKey = "id";

    protected $dates = ['deleted_at'];

    public function batch()
    {    
        return $this->belongsTo('Rush\Modules\Merchant\Models\PointsSeedingBatchModel', 'batch_id', 'id');   
    }
}
