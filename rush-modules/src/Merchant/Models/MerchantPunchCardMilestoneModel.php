<?php

namespace Rush\Modules\Merchant\Models;

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;
use Logaretm\Transformers\TransformableTrait;
use Logaretm\Transformers\Contracts\Transformable;

/**
 * Class MerchantPunchCardMilestoneModel
 *
 * @property-read       integer             $id
 * @property            string              $name
 * @property            integer             $stamps
 * @property            string              $uuid
 * @property            boolean             $enabled
 * @property            integer             $merchant_id
 * @property            \Carbon\Carbon      $created_at
 * @property            \Carbon\Carbon      $updated_at
 */
class MerchantPunchCardMilestoneModel extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'merchant_punchcard_milestones';

    public function getUuidAttribute($value)
    {
        if (!$value) {
            $value = Uuid::uuid1()->toString();
            $this->uuid = $value;
            $this->save();
        }

        return $value;
    }
}