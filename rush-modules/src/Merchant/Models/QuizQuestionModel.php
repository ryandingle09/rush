<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class QuizQuestionModel extends Model
{
    protected $table = "quiz_questions";
}
