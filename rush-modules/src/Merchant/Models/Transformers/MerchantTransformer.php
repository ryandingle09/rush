<?php

namespace Rush\Modules\Merchant\Models\Transformers;

use Logaretm\Transformers\Transformer;
use Rush\Modules\Merchant\Models\MerchantModel;
use Rush\Modules\Achievements\Models\AchievementsModel;
use Rush\Modules\Merchant\Models\MerchantPromoMechanicsModel;

class MerchantTransformer extends Transformer
{
    /**
     * @param  MerchantModel $merchant
     * @return mixed
     */
    public function getTransformation($merchant)
    {
        $package_index = $merchant->packageId == 1 ? 'points_name' : 'stamp_name';
        $designs = $merchant->designs['customer'];
        $settings = $merchant->settings;

        $gcash_transaction_achievement = AchievementsModel
            ::where('merchant_id', $merchant->merchantid)
            ->where('name', 'gcash_transaction')
            ->first();

        $gcash_multiplier = ( isset($gcash_transaction_achievement->options->multiplier) ? $gcash_transaction_achievement->options->multiplier : 0 );

        $max_stamps_per_day = MerchantPromoMechanicsModel
            ::where('merchant_id', $merchant->merchantid)
            ->orderBy('id', 'desc')
            ->value('max_stamps_per_day');


        $data['name'] = $merchant->name;
        $data['program_name'] = $merchant->program_name;
        $data[ $package_index ] = $settings->points_name;
        $data['business_name'] = $merchant->businessName;
        $data['logo_url'] = (string) $designs['logo_url'];
        $data['background_url'] = (string) $designs['background_url'];
        $data['text_color'] = (string) $designs['text_color'];
        $data['buttons_color'] = (string) $designs['buttons_color'];
        $data['stamp_url'] = (string) $designs['stamp_url'];
        $data['stamp_gray_url'] = (string) $designs['stamp_gray_url'];
        $data['updated_at'] = (string) $designs['updated_at'];
        
        $data['settings'] = [
            'program_name' => (string) $settings->program_name,
            'points_name' => (string) $settings->points_name,
            'app_name' => (string) $settings->app_name,
            'app_description' => (string) $settings->app_description,
            'app_logo' => (string) $settings->app_logo,
            'points_transfer' => (boolean) $settings->points_transfer,
            'earn_transaction_limit' => (int) $settings->earn_transaction_limit,
            'earn_points_limit' => (float) $settings->earn_points_limit,
            'redemption_transaction_limit' => (int) $settings->redemption_transaction_limit,
            'paypoint_transaction_limit' => (int) $settings->paypoint_transaction_limit,
            'paypoint_points_limit' => (float) $settings->paypoint_points_limit,
            'gcash_multiplier' => (float) $gcash_multiplier,
            'api_callback_url' => (string) $settings->api_callback_url,
            'whitelisting_mobile_numbers' => (boolean) $settings->whitelisting_mobile_numbers,
            'enable_email' => (boolean) $settings->enable_email,
            'vanity_url' => (string) $settings->vanity_url,
            'rounding_off_points' => (boolean) $settings->rounding_off_points,
            'enable_package' => (boolean) $settings->enable_package,
            'enable_stamps_summary_punchcard_filter' => (boolean) $settings->enable_stamps_summary_punchcard_filter,
            'enable_stamps_summary_punchcard_redemption' => (boolean) $settings->enable_stamps_summary_punchcard_redemption,
            'enable_feedback_category' => (boolean) $settings->enable_feedback_category,
            'allow_multiple_email' => (boolean) $settings->allow_multiple_email,
            'enable_success_book_sms' => (boolean) $settings->enable_success_book_sms,
            'enable_cancel_book_sms' => (boolean) $settings->enable_cancel_book_sms,
            'enable_non_unique_customer_email' => (boolean) $settings->enable_non_unique_customer_email,
            'enable_extended_package_expiry' => (boolean) $settings->enable_extended_package_expiry,
            'allow_registration' => (boolean) $settings->allow_registration,
            'enable_package_summary' => (boolean) $settings->enable_package_summary,
            'display_live_punchcard_only' => (boolean) $settings->display_live_punchcard_only,
            'customer_login_param' => (string) $settings->customer_login_param,
            'voucher_module' => (boolean) $settings->voucher_module,
            'add_member' => (boolean) $settings->add_member,
            'code_management' => (boolean) $settings->code_management,
            'enable_referral_code' => (boolean) $settings->enable_referral_code,
            'max_stamps_per_day' => (int) $max_stamps_per_day,
        ];



        return $data;
    }
}
