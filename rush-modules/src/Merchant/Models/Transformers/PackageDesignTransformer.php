<?php

namespace Rush\Modules\Merchant\Models\Transformers;

use Logaretm\Transformers\Transformer;
use Rush\Modules\Merchant\Models\MerchantPackageModel;

class PackageDesignTransformer extends Transformer
{
    /**
     * @param  MerchantPackageModel $design
     * @return mixed
     */
    public function getTransformation($design)
    {
        return [
            'logo_url'          =>  $design->logo->url,
            'background_url'    =>  $design->background->url,
            'text_color'        =>  $design->colors->text,
            'buttons_color'     =>  $design->colors->button,
            'stamp_url'        =>   $design->stamp->url,
            'stamp_gray_url'    =>  $design->stamp_greyscale->url,
            'updated_at'        =>  $design->updated_at ? $design->updated_at->format('d-m-Y H:i:s') : ($design->created_at ? $design->created_at->format('d-m-Y H:i:s') : null)
        ];
    }
}
