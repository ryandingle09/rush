<?php

namespace Rush\Modules\Merchant\Models\Transformers;

use Logaretm\Transformers\Transformer;
use Rush\Modules\Merchant\Models\MerchantBranchesMetaModel;

class BranchMetaTransformer extends Transformer
{
    /**
     * @param  MerchantBranchesMetaModel $meta
     * @return mixed
     */
    public function getTransformation($meta)
    {
        $return = ['name' => $meta->name];
        $value = json_decode($meta->value);
        if (json_last_error() == JSON_ERROR_SYNTAX) {
            $value = $meta->value;
        };
        $return['value']  = $value;

        return $return;
    }
}
