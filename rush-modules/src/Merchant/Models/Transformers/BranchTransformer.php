<?php

namespace Rush\Modules\Merchant\Models\Transformers;

use Logaretm\Transformers\Transformer;
use Rush\Modules\Helpers\Facades\StorageHelper;
use Rush\Modules\Merchant\Models\MerchantBranchesModel;

class BranchTransformer extends Transformer
{
    /**
     * @param  MerchantBranchesModel $branch
     * @return mixed
     */
    public function getTransformation($branch)
    {
        $metas = $branch->metas->map(function($meta) {
                switch($meta->var_type) {
                    case 'integer':
                        $value = (int) $meta->value;
                        break;
                    case 'float':
                        $value = (float) $meta->value;
                        break;
                    case 'json':
                        $value = json_decode($meta->value);
                        break;
                    default:
                        $value = $meta->value;
                }
                return [
                    $meta->name => $value 
                ];
            })
            ->flatten(1)
            ->toArray();
        return [
            'id'  => $branch->uuid,
            'name' => $branch->name,
            'address' => $branch->street_name,
            'logo_url' => str_contains($branch->logo_url, config('app.url')) ? $branch->logo_url : ($branch->logo_url ? StorageHelper::repositoryUrlForView($branch->logo_url, true) : ''),
            'qr_code' => StorageHelper::repositoryUrlForView($branch->qr_code, true),
            'district' => $branch->district,
            'city' => $branch->city,
            'zipcode' => $branch->zipcode,
            'meta' => $metas
        ];
    }
}
