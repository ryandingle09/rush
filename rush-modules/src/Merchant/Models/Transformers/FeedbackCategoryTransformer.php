<?php

namespace Rush\Modules\Merchant\Models\Transformers;

use Logaretm\Transformers\Transformer;
use Rush\Modules\Merchant\Models\MerchantFeedbackCategoryModel;

class FeedbackCategoryTransformer extends Transformer
{
    /**
     * Get formatted object 
     *
     * @param  MerchantFeedbackCategoryModel $category
     * @return array 
     */
    public function getTransformation($category)
    {
        return [
            'id' => $category->uuid,
            'title' => $category->title
        ];
    }
}