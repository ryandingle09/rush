<?php

namespace Rush\Modules\Merchant\Models\Transformers;

use Logaretm\Transformers\Transformer;
use Rush\Modules\Merchant\Models\MerchantPunchCardMilestoneModel;

class PunchCardMilestonesTransformer extends Transformer
{
    /**
     * @param  MerchantPunchCardMilestoneModel $milestone
     * @return mixed
     */
    public function getTransformation($milestone)
    {
        return [
            'id' => $milestone->uuid,
            'name' => $milestone->name,
            'stamps' => $milestone->stamps
        ];
    }
}