<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantCityModel extends Model
{
    protected $table = "merchant_cities";
}
