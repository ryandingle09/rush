<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;
use Rush\Modules\Helpers\Facades\StorageHelper;

// Libraries
use Logaretm\Transformers\TransformableTrait;
use Logaretm\Transformers\Contracts\Transformable;

// Traits
use Rush\Modules\Merchant\Models\Traits\PackageDesign\AttributesTrait;

class MerchantPackageModel extends Model implements Transformable
{
    use AttributesTrait, TransformableTrait;

    protected $table = 'MerchantPackage';
    protected $primaryKey = 'merchantPackageId';

    public $assets_paths = [
        'stamp' => '/repository/merchant/stamps',
        'logo' => '/repository/merchant/logo',
        'background' => '/repository/merchant/logo'
    ];

    public function getLogoURLAttribute($value)
    {
        return StorageHelper::repositoryUrlForView($value);
    }

    public function getBackgroundURLAttribute($value)
    {
        return StorageHelper::repositoryUrlForView($value);
    }

    public function getStampURLAttribute($value)
    {
        return StorageHelper::repositoryUrlForView($value);
    }
}
