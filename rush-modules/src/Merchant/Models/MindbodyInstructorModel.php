<?php

namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class MindbodyInstructorModel extends Model
{
    protected $table = 'merchant_mindbody_instructors';
}
