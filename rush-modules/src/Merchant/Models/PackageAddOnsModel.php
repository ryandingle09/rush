<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;
use Rush\Modules\Merchant\Models\PackageAddOnsModel;
use Carbon\Carbon;
/**
 * Class MerchantModel
 * @package Rush\Modules\Merchant\Models
 *
 * @property integer $id
 * @property integer $packageId
 * @property \Rush\Modules\Merchant\Models\MerchantConversionSettingModel $conversion_setting
 * @property \Rush\Modules\Customer\Models\CustomerModel[] $customers
 *
 */
class PackageAddOnsModel extends Model
{
    protected $table = "PackageAddons";
    protected $primaryKey = "id";
}
