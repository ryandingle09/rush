<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class QuizQuestionChoiceModel extends Model
{
    protected $table = "quiz_question_choices";
}
