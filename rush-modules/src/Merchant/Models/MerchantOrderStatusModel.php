<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantOrderStatusModel extends Model
{
    protected $table = "merchant_order_statuses";
}
