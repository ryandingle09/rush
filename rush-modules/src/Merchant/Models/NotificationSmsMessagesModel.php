<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class NotificationSmsMessagesModel extends Model
{
    protected $table = "notification_sms_messages";
}
