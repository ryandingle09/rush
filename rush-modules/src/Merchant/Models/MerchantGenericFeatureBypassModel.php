<?php

namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantGenericFeatureBypassModel extends Model
{
    protected $table = "merchant_generic_feature_bypass";
}
