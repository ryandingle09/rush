<?php

namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;
use Rush\Modules\Merchant\Models\VoucherCodeModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class VoucherModel extends Model
{
    use SoftDeletes;

    protected $table = 'voucher';

    protected $dates = ['deleted_at'];

    public function codes()
    {
        return $this->hasMany(VoucherCodeModel::class, 'voucher_item_id', 'id');
    }

    public function branch()
    {
        return $this->hasOne( MerchantBranchesModel::class, 'id', 'branch_id');
    }

    public function getIssuedAttribute()
    {
        return VoucherCodeModel::where('voucher_item_id', $this->id )->where('send', 1)->count();
    }
    
    public function getAvailableAttribute()
    {
        return VoucherCodeModel::where('voucher_item_id', $this->id )->where('send', 0)->count();
    }

    public function getEnabledLabelAttribute()
    {
        if ( $this->enabled ) {
        	return 'Enabled';
        } else {
        	return 'Disabled';
        }
    }

    public function get_discount_amount( $total_amount ){

        if( $this->discount_type == 'amount' ) return (float) $this->discount;
        elseif( $this->discount_type == 'percent' ) return (float) ($total_amount * ($this->discount / 100));

    }

    public function getAvailablePromoCodeCountAttribute(){

        return VoucherCodeModel
            ::where('merchant_id', $this->merchant_id)
            ->where('voucher_item_id', $this->id)
            ->where('used', 0)
            ->where('expiration', '>', date('Y-m-d H:i:s'))
            ->count();

    }

    public function merchant(){

        return $this->belongsTo('Rush\Modules\Merchant\Models\MerchantModel', 'merchant_id');

    }

}