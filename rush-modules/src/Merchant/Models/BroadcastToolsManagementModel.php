<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class BroadcastToolsManagementModel extends Model
{
  protected $table = "broadcast_tools_management";
  protected $primaryKey = "id";
}
