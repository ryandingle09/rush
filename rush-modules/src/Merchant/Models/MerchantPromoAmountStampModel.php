<?php

namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MerchantPromoAmountStampModel extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = "MerchantPromoAmountStamp";

    public function getUpdatedAtColumn() {
	    return null;
	}
    
}
