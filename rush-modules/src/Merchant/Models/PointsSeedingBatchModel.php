<?php

namespace Rush\Modules\Merchant\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Rush\Modules\Merchant\Models\PointsSeedingDataModel;
use Rush\Modules\Merchant\Models\MerchantModel;
use Rush\Modules\Merchant\Models\MerchantEmployeesModel;
use Rush\Modules\Merchant\Models\MerchantSubUsersModel;

class PointsSeedingBatchModel extends Model
{
    use SoftDeletes;

    protected $table = "points_seeding_batch";
    protected $primaryKey = "id";

    protected $dates = ['deleted_at'];

    public function merchant()
    {    
        return $this->belongsTo('Rush\Modules\Merchant\Models\MerchantModel', 'merchant_id', 'merchantid');   
    }

    public function seeding_data()
    {
        return $this->hasMany('Rush\Modules\Merchant\Models\PointsSeedingDataModel', 'batch_id', 'id');      
    }

}
