<?php
namespace Rush\Modules\Merchant\Models;

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class ProductStampModel extends Model
{
    protected $table = "product_stamps";

    protected $fillable = ["stamp_name", "stamp_number"];

    public function getUuidAttribute( $value ){

        if( ! $value ){

            $value = Uuid::uuid1()->toString();
            $this->uuid = $value;
            $this->save();
        }

        return $value;

    }
}
