<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantUserModel extends Model
{
    protected $table = "lara_users";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function merchant()
    {
        return $this->hasOne('Rush\Modules\Merchant\Models\MerchantModel', 'merchantid', 'merchant_id');
    }

    public function apps()
    {
        return $this->hasMany('Rush\Modules\Merchant\Models\MerchantUserAppsModel','user_id', 'id');
    }

}



