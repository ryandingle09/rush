<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantIntegrationsFieldsModel extends Model
{
    protected $table = "merchant_integrations_fields";
}