<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MerchantSMSNotificationSettings
 * @package Rush\Modules\Merchant\Models
 *
 * @property integer $id
 * @property integer $merchant_id
 * @property integer $near_completion_enable
 * @property string $near_completion_message
 * @property integer $near_completion_no_days
 * @property integer $near_expiration_enable
 * @property string $near_expiration_message
 * @property integer $near_expiration_no_days
 */
class MerchantSMSNotificationSettingModel extends Model
{
    protected $table = "merchant_sms_notification_settings";
}