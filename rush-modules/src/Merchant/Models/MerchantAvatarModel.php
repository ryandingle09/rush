<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantAvatarModel extends Model
{
    protected $table = "merchant_avatars";
}
