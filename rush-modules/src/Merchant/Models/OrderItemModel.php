<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class OrderItemModel extends Model
{
    protected $table = "order_items";

    public function item(){

        return $this->belongsTo('Rush\Modules\Merchant\Models\MenuItemModel');

    }
}
