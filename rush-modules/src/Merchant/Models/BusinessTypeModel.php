<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * Class BusinessTypeModel
 * @package Rush\Modules\Merchant\Models
 *
 * @property integer $id
 * @property integer $packageId
 * @property \Rush\Modules\Merchant\Models\MerchantConversionSettingModel $conversion_setting
 * @property \Rush\Modules\Customer\Models\BusinessTypeModel[] $customers
 *
 */
class BusinessTypeModel extends Model
{
    protected $table = "BusinessType";
    protected $primaryKey = "id";

    public function getNameToTextsAttribute()
    {
      return $this->name;
    }
}
