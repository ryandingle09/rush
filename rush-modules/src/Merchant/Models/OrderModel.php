<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class OrderModel extends Model
{
    protected $table = "orders";

    public function getUuidAttribute( $value ){

        if( ! $value ){

            $value = Uuid::uuid1()->toString();
            $this->uuid = $value;
            $this->save();
        }

        return $value;
        
    }

    public function order_items(){

        return $this->hasMany('Rush\Modules\Merchant\Models\OrderItemModel', 'order_id');

    }

    public function status_history(){

        return $this->hasMany('Rush\Modules\Merchant\Models\OrderStatusHistoryModel', 'order_id');

    }

    public function customer(){

        return $this->belongsTo('Rush\Modules\Customer\Models\CustomerModel');

    }

    public function merchant(){

        return $this->belongsTo('Rush\Modules\Merchant\Models\MerchantModel');

    }
}
