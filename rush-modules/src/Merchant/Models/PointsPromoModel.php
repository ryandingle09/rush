<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class PointsPromoModel extends Model
{
    protected $table = "points_promos";

    public function getPromofiedPoints( $customer, $points )
    {
        // double the points if it's customer's birthday.
        if( $this->code == 'birthday_double_points' ){
            if( $customer->birthDate == date('Y-m-d') ) $points = $points * 2;
        }

        return $points;
    }
}
