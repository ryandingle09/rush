<?php
namespace Rush\Modules\Merchant\Models;

use Ramsey\Uuid\Uuid;
use Rush\Modules\Posts\Models\PostsModel;
use Illuminate\Database\Eloquent\Model;

class MerchantVotesModel extends Model
{
    protected $table = "votes";

    public function getUuidAttribute( $value )
    {
        if( ! $value ) {
            $value = Uuid::uuid1()->toString();
            $this->uuid = $value;
            $this->save();
        }

        return $value;
    }
    
    public function post()
    {
        return $this->belongsTo(PostsModel::class, 'post_id');
    }
}
