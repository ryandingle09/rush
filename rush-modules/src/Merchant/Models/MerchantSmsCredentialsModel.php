<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantSmsCredentialsModel extends Model
{
    protected $table = "notification_sms_credentials";
}
