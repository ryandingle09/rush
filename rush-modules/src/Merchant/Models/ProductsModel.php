<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class ProductsModel extends Model
{
    protected $table = "products";

    public function getUuidAttribute( $value ){

        if( ! $value ){

            $value = Uuid::uuid1()->toString();
            $this->uuid = $value;
            $this->save();
        }

        return $value;
        
    }

    public function stamps(){
        return $this->hasMany(ProductStampModel::class, 'product_id');
    }

    public function getBranchesNameAttribute(){
        $branch_names = [];

        if($this->branch_ids == "[0]"){
            return "All Branch";
        }

        $branch_ids = str_replace('[', '', $this->branch_ids);
        $branch_ids = str_replace(']', '', $branch_ids);
        $branch_ids = explode(',', $branch_ids);

        foreach ($branch_ids as $id){
            $branch_names[] = MerchantBranchesModel::where('id', trim($id))->first()->branchName;
        }

        return implode( ', ', $branch_names);
    }

    public function user_last_updated(){

        return $this->belongsTo('Rush\Modules\Merchant\Models\MerchantModel', 'updated_by');

    }
}
