<?php
namespace Rush\Modules\Merchant\Models;

use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Database\Eloquent\Model;
use Logaretm\Transformers\Contracts\Transformable;
use Logaretm\Transformers\TransformableTrait;
use Rush\Modules\Customer\Models\CustomerModel;
use Rush\Modules\Helpers\Facades\StorageHelper;
use Rush\Modules\Merchant\Models\BroadcastToolsManagementModel;
use Rush\Modules\Merchant\Models\MerchantBranchesModel;
use Rush\Modules\Merchant\Models\MerchantConversionSettingModel;
use Rush\Modules\Merchant\Models\MerchantGlobeRewardsModel;
use Rush\Modules\Merchant\Models\MerchantOrderStatusModel;
use Rush\Modules\Merchant\Models\MerchantPackageModel;
use Rush\Modules\Merchant\Models\MerchantPromoMechanicsModel;
use Rush\Modules\Merchant\Models\MerchantRewardsModel;
use Rush\Modules\Merchant\Models\MerchantSettingModel;
use Rush\Modules\Merchant\Models\MerchantSubUsersModel;
use Rush\Modules\Merchant\Models\ModulesMerchantModel;
use Rush\Modules\Merchant\Models\NotificationSmsLogsModel;
use Rush\Modules\Merchant\Models\PointsSeedingBatchModel;
use Rush\Modules\Merchant\Models\Traits\Merchant\AttributesTrait;
use Rush\Modules\Merchant\Models\Traits\Merchant\RelationshipsTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class MerchantModel
 *
 * @property integer                             $id
 * @property integer                             $packageId
 * @property string                              $business
 * @property CustomerModel[]|null                $customers
 * @property MerchantSettingModel                $settings
 * @property MerchantPunchCardMilestoneModel[]   $punchcard_milestones
 */
class MerchantModel extends Model implements Transformable
{
    use AttributesTrait, RelationshipsTrait, TransformableTrait;

    use SoftDeletes;

    protected $table = "Merchant";
    protected $primaryKey = "merchantid";
    protected $dates = ['deleted_at'];

    /**
     * defines the type string of this model when used in polymorphic relationships
     * @var string
     */
    protected $morphClass = 'Merchant';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Rush\Modules\Customer\Models\CustomerModel[]
     */
    public function customers()
    {
        return $this->hasMany('Rush\Modules\Customer\Models\CustomerModel', 'merchantId', 'merchantid');
    }

    /**
     * @return  \Rush\Modules\Customer\Models\CustomerTransactionsModel[]|\Illuminate\Database\Query\Builder
     */
    public function transactions()
    {
        if ($this->packageId == 1) {
            return $this->hasMany('Rush\Modules\Customer\Models\CustomerTransactionsModel', 'merchant_id', 'merchantid');
        } else {
            return $this->hasMany('Rush\Modules\Customer\Models\CustomerPunchCardTransactionsModel', 'merchant_id', 'merchantid');
        }
    }

    /**
     * @param string $datetime_start
     * @param string $datetime_end
     * @return mixed|null|\Rush\Modules\Customer\Models\CustomerTransactionsModel[]
     */
    public function getTransactions($datetime_start = null, $datetime_end = null)
    {
        if ($datetime_start === null && $datetime_end === null)
            return $this->transactions;

        $now = Carbon::now();
        if ($datetime_start !== null && $datetime_end === null) {
            $datetime_start = Carbon::createFromFormat('Y-m-d H:i:s', $datetime_start);
            return $this->transactions()->whereBetween('timestamp', [$datetime_start->toDateTimeString(), $now->toDateTimeString()])->get();
        } else if ($datetime_start === null && $datetime_end !== null) {
            $datetime_end = Carbon::createFromFormat('Y-m-d H:i:s', $datetime_end);
            return $this->transactions()->whereBetween('timestamp', [$now->toDateTimeString(), $datetime_end->toDateTimeString()])->get();
        }
        if ( $this->packageId == 1 ) {
          return $this->transactions()->whereBetween('timestamp', [$datetime_start, $datetime_end]);
        } else {
          return $this->transactions()->whereBetween('date_punched', [$datetime_start, $datetime_end]);
        }

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne|\Rush\Modules\Merchant\Models\MerchantConversionSetting
     */
    public function conversion_setting()
    {
        return $this->morphOne('\Rush\Modules\Merchant\Models\MerchantConversionSettingModel', 'transformable');
    }

    public function smss()
    {
        return $this->hasMany('Rush\Modules\Sms\Models\SmsModel', 'merchant_id', 'merchantid');
    }

    public function pushs()
    {
        return $this->hasMany('Rush\Modules\Push\Models\PushModel', 'merchant_id', 'merchantid');
    }

    public function emails()
    {
        return $this->hasMany('Rush\Modules\Email\Models\EmailModel', 'merchant_id', 'merchantid');
    }

    public function segments()
    {
        return $this->hasMany('Rush\Modules\Broadcasting\Models\SegmentModel', 'merchant_id', 'merchantid');
    }

    public function activeSegments()
    {
        return $this->segments()->where('status', '=', 1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne|\Rush\Modules\Merchant\Models\MerchantSMSNotificationSettingModel
     */
    public function sms_notification_setting()
    {
        return $this->hasOne('Rush\Modules\Merchant\Models\MerchantSMSNotificationSettingModel', 'merchant_id', 'merchantid');
    }

    /**
     * @return MerchantPackageModel
     */
    public function getLoyaltyCardDesignAttribute()
    {
        return MerchantPackageModel::where('designType', 2)->where('merchantId', $this->merchantid)->first();
    }

    public function getAddOnsAttribute()
    {
        $addons_id = explode('|', $this->PackageAddons);
        return PackageAddOnsModel::whereIn('id', $addons_id)->get();
    }

    public function getBusinessTypeNameAttribute()
    {
        $business_type_id = array_filter(explode('|', $this->businessType));
        return BusinessTypeModel::whereIn('id', $business_type_id)->get();
    }

    public function branches()
    {
        return $this->hasMany('Rush\Modules\Merchant\Models\MerchantBranchesModel', 'merchantId', 'merchantid');
    }

    public function get_branches()
    {
        $branches = [];

        $branches_from_db = MerchantBranchesModel::where('merchantId', $this->merchantid)->where('is_deleted', 0)->get();
        foreach ($branches_from_db as $branch) {
            $address = "";
            if($branch->street_name || $branch->district || $branch->city || $branch->zipcode) {
                $address = $branch->street_name . ", " . $branch->district . ", " . $branch->city . ", " . $branch->zipcode;
            }

            $merchant_logo = isset($this->merchant_app_design->logoURL) ? StorageHelper::repositoryUrlForView($this->merchant_app_design->logoURL, 1) : null;

            $branch_logo = $branch->branch_logo ? StorageHelper::repositoryUrlForView($branch->branch_logo, 1) : $merchant_logo;

            $branches[] = [
                'id' => $branch->uuid,
                'name' => $branch->branchName,
                'address' => $address,
                'logo_url' => $branch_logo,
                'qr_code' => $branch->qr_code
            ];
        }

        return $branches;
    }

    public function getActiveBranchesCountAttribute()
    {
      $branches = $this->branches->where('is_deleted', 0)->count();
      $branches = ( $branches > 0 ) ? $branches : 1;
      return $branches;
    }

    public function employees()
    {

        return $this->hasMany('Rush\Modules\Merchant\Models\MerchantEmployeesModel', 'merchantId', 'merchantid');

    }

    public function conversion_settings(){

        return $this->hasOne('Rush\Modules\Merchant\Models\MerchantConversionSettingModel', 'merchant_id', 'merchantid');

    }

    public function notif_sms_logs()
    {
        return $this->hasMany('Rush\Modules\Merchant\Models\NotificationSmsLogsModel', 'merchant_id', 'merchantid');
    }

    public function user()
    {
        return $this->belongsTo('Rush\Modules\Merchant\Models\MerchantUserModel', 'merchantid', 'merchant_id');
    }

    public function getPromoAttribute(){

        $promo = ['expired' => [], 'ongoing' => []];
        $now = strtotime(date('Y-m-d'));
        $ongoing = false;
        $ongoing_card = new \stdClass;
        $expired = false;
        $expired_card = new \stdClass; // expired punchcard but redemption is still active

        $punchcards = MerchantPromoMechanicsModel::where('merchant_id', $this->merchantid)->get();

        if ($punchcards) {
            // check if there's a ongoing promo
            foreach ($punchcards as $punchcard) {
                $duration_from_date = strtotime($punchcard->duration_from_date);
                $duration_to_date = strtotime($punchcard->duration_to_date);
                $redemption_from_date = strtotime($punchcard->redemption_duration_from_date);
                $redemption_to_date = strtotime($punchcard->redemption_duration_to_date);
                // get ongoing punchcard
                if ($now >= $duration_from_date && $now <= $duration_to_date) {
                    $ongoing = true;
                    $ongoing_card = $punchcard;
                }
                // get expired punchcard that has active redemption
                if (($now >= $redemption_from_date && $now <= $redemption_to_date) &&
                    ($now > $duration_from_date && $now > $duration_to_date)
                ) {
                    $expired = true;
                    $expired_card = $punchcard;
                }
            }
        }

        if ($expired) {
            $promo['expired'] = $expired_card;
        }

        if ($ongoing) {
            $promo['ongoing'] = $ongoing_card;
        }


        return (object)$promo;

    }

    public function facebook_app_credentials()
    {
        return $this->hasOne('Rush\Modules\Merchant\Models\MerchantFacebookAppCredentialsModel', 'merchant_id', 'merchantid');
    }

    public function loyaltyCardOrder()
    {
      return $this->hasMany('Rush\Modules\Merchant\Models\LoyaltyCardOrderModel', 'merchant_id', 'merchantid');
    }

    public function modules_merchant()
    {
      return $this->hasMany('Rush\Modules\Merchant\Models\ModulesMerchantModel', 'merchant_id', 'merchantid');
    }

    public function getStatusForAdminAttribute()
    {
      $status = $this->status;
      if ( $this->status == "Full" ) {
        return "Active - Full";
      } else  {
        $trial_expiration_date = Carbon::parse($this->timestamp)->addMonth();
        if ( Carbon::now() <= $trial_expiration_date )
        {
          return "Active - Trial";
        }
      }
      return "Inactive";
    }

    public function getEnabledForAdminAttribute()
    {
      $status = $this->enabled;
      if ( $this->enabled ) return "enabled";
      else return "disabled";
    }

    public function billings()
    {
        return $this->hasMany('Rush\Modules\Merchant\Models\BillingModel', 'merchant_id', 'merchantid');

    }

    public function broadcast_tools()
    {
        return $this->hasMany('Rush\Modules\Merchant\Models\BroadcastToolsManagementModel', 'merchant_id', 'merchantid');
    }

    public function class_management_tools()
    {
        return $this->hasMany('Rush\Modules\ClassManagement\Models\ClassManagementToolsModel', 'merchant_id', 'merchantid');
    }

    public function isPro()
    {
       if($this->packageId == 1)  {
           return true;
       }

       return false;
    }

    public function getPosSyncSettingsAttribute(){

        $settings = [];
        $raw_settings = MerchantPosSyncSettingsModel::where('merchant_id', $this->merchantid)->select('name', 'enabled')->get();

        foreach( $raw_settings as $row ){
            $settings[] = ['name' => $row->name, 'enabled' => (int) $row->enabled];
        }

        return $settings;

    }

    public function getBroadcastToolsAllowedAttribute()
    {
      $modules = array();
      foreach( $this->broadcast_tools as $tools )
      {
        $modules[] = $tools->broadcast_tools_id;
      }
      return $modules;
    }

    public function getClassManagementToolsAllowedAttribute()
    {
        $modules = array();
        foreach( $this->class_management_tools as $tools )
        {
            $modules[] = $tools->class_management_tool_id;
        }
        return $modules;
    }

    public function getSmsTransactRegisterAttribute()
    {
        return NotificationSmsLogsModel::where('merchant_id', $this->merchantid)->where('transaction_type', "sms-service-customer-registration")->get();
    }

    public function getSmsTransactPointsTransferAttribute()
    {
        return NotificationSmsLogsModel::where('merchant_id', $this->merchantid)->where('transaction_type', "sms-service-points-transfer")->get();
    }

    public function getSmsTransactPointsInquiryAttribute()
    {
        return NotificationSmsLogsModel::where('merchant_id', $this->merchantid)->where('transaction_type', "sms-service-points-inquiry")->get();
    }

    public function getEarningPointsOfAmount( $amount ){

        $dividend = floatval($amount) / floatval($this->conversion_settings->earning_peso);
        $points = intval($this->conversion_settings->earning_points) * $dividend;

        return $points;

    }

    public function getRewardsAttribute()
    {
        return MerchantRewardsModel::where('status', 1)->where('merchantid', $this->merchantid)->orderBy('pointsRequired','asc')->get();
    }

    public function getAdminRewardsAttribute()
    {
        return MerchantGlobeRewardsModel::where('status', 1)->where('merchantid', $this->merchantid)->get();
    }

    public function reports()
    {
        return $this->hasMany('Rush\Modules\Customer\Models\CustomerSupportModel', 'merchant_id', 'merchantid');
    }

    public function sub_users()
    {
        return $this->hasMany('Rush\Modules\Merchant\Models\MerchantSubUsersModel', 'merchant_id', 'merchantid');
    }

    public function getLoyaltyCatalogue( $customer_uuid = null )
    {
        $customer_mobile_network = '';

        if( ! is_null($customer_uuid) ) $customer = CustomerModel::where('uuid', $customer_uuid)->first();
        if( isset($customer) ) $customer_mobile_network = $customer->mobile_network;

        $regular_rewards = MerchantRewardsModel::where('status', 1)->where('merchantid', $this->merchantid)->orderBy('pointsRequired')->get();
        $globe_rewards = MerchantGlobeRewardsModel::where('status', 1)->where('merchantid', $this->merchantid)->orderBy('pointsRequired')->get();
        $catalogue = [];

        foreach( $regular_rewards as $reward ){
            $catalogue[] = [
                'uuid' => (string) $reward->uuid,
                'id' => (int) $reward->redeemItemId,
                'type' => 0,
                'name' => (string) $reward->name,
                'details' => (string) $reward->details,
                'points' => (float) $reward->pointsRequired,
                'image_url' => (string) $reward->imageURL,
                'branches' => (array) $reward->branches,
            ];
        }

        foreach ($globe_rewards as $reward) {
            if( in_array($customer_mobile_network, (array) json_decode($reward->type)) ){
                $catalogue[] = [
                    'uuid' => (string) $reward->uuid,
                    'id' => (int) $reward->redeemItemId,
                    'type' => 1,
                    'name' => (string) $reward->name,
                    'details' => (string) $reward->details,
                    'points' => (float) $reward->pointsRequired,
                    'image_url' => (string) $reward->imageURL,
                    'branches' => (array) $reward->branches,
                ];
            }
        }

        return $catalogue;
    }

    public function getDataAttribute()
    {
        $data = [];
        $data['name'] = $this->businessName;
        $data['program_name'] = $this->program_name;
        if($this->packageId == 1){
            $data['points_name'] = $this->settings->points_name;
        } else {
            $data['stamp_name'] = $this->settings->points_name;
        }
        return $data;
    }

    public function getProgramNameAttribute()
    {
        return $this->settings->program_name;
    }

    public function settings()
    {
        return $this->hasOne('Rush\Modules\Merchant\Models\MerchantSettingModel', 'merchant_id', 'merchantid');
    }

    public function getRewards( $customer_uuid = null )
    {
        $customer_mobile_network = '';

        if( ! is_null($customer_uuid) ) $customer = CustomerModel::where('uuid', $customer_uuid)->first();
        if( isset($customer) ) $customer_mobile_network = $customer->mobile_network;

        $rewards = [];
        $temp_rewards = MerchantRewardsModel::where('merchantid', $this->id)->where('status', 1)->orderBy('pointsRequired')->get();
        $globe_rewards = MerchantGlobeRewardsModel::where('merchantid', $this->id)->where('status', 1)->orderBy('pointsRequired')->get();

        foreach ($temp_rewards as $reward) {
            $rewards[] = [
                'id'                => $reward->uuid,
                'name'              => $reward->name,
                'details'           => $reward->details,
                'points_required'   => (float) $reward->pointsRequired,
                'image_url'         => $reward->imageURL ? StorageHelper::repositoryUrlForView($reward->imageURL, 1) : null
            ];
        }

        foreach ($globe_rewards as $reward) {
            if( in_array($customer_mobile_network, (array) json_decode($reward->type)) ){
                $rewards[] = [
                    'id'                => $reward->uuid,
                    'name'              => $reward->name,
                    'details'           => $reward->details,
                    'points_required'   => (float) $reward->pointsRequired,
                    'image_url'         => $reward->imageURL ? StorageHelper::repositoryUrlForView($reward->imageURL, 1) : null
                ];
            }
        }

        return $rewards;
    }

    public function whitelisted_mobile_numbers(){

        return $this->hasMany('Rush\Modules\Merchant\Models\WhitelistedNumberModel', 'merchant_id', 'merchantid');

    }

    public function posts()
    {
        return $this->hasMany('Rush\Modules\Posts\Models\PostsModel', 'merchant_id', 'merchantid');
    }

    public function getGenericValidationBypassAttribute(){

        $formatted_validation_bypasses = [];
        $validation_bypasses = MerchantGenericValidationBypassModel
            ::where('merchant_id', $this->merchantid)
            ->where('status', 1)
            ->get();

        foreach( $validation_bypasses as $bypass ){
            $formatted_validation_bypasses[ $bypass->endpoint_name ][ $bypass->field ][ $bypass->rule ] = true;
        }

        return $formatted_validation_bypasses;

    }

    public function getGenericFeatureBypassAttribute(){

        $formatted_feature_bypasses = [];
        $feature_bypasses = MerchantGenericFeatureBypassModel
            ::where('merchant_id', $this->merchantid)
            ->where('status', 1)
            ->get();

        foreach( $feature_bypasses as $bypass ){
            $formatted_feature_bypasses[ $bypass->endpoint_name ][ $bypass->feature ] = true;
        }

        return $formatted_feature_bypasses;
    }

    public function integration($name)
    {
        return $this->integrations()->where('name', $name)->first();
    }

    public function __get($key)
    {
        $value =  $this->getAttribute($key);
        if ( is_null($value) ) {
            // try checking meta
            $value = $this->metas()->where('name', $key)->value('value');
        }

        return $value;
    }

    public function avatars(){
        return $this->hasMany('Rush\Modules\Merchant\Models\MerchantAvatarModel', 'merchant_id', 'merchantid');
    }

    public function getNoReplyEmailAttribute(){

        $email_partials = explode('@', $this->contactPersonEmail);
        return 'no-reply@' . $email_partials[1];
    }
    
    public function client(){
        return $this->hasMany('Rush\Modules\Merchant\Models\ClientModel', 'merchant_id', 'merchantid');
    }

    public function getOrderStatusesAttribute(){

        $statuses = MerchantOrderStatusModel::where('merchant_id', $this->merchantid)->pluck('status');
        if( ! count($statuses) ) $statuses = MerchantOrderStatusModel::where('merchant_id', 0)->pluck('status');

        return $statuses->all();

    }

    public function points_seeding_batch()
    {
        return $this->hasMany('Rush\Modules\Merchant\Models\PointsSeedingBatchModel', 'merchant_id', 'merchantid');
    }

    public function getIsPnpAttribute()
    {
        return ( $this->business_system_link == 'plains-and-prints' ? true : false );
    }
    
}
