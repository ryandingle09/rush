<?php

namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantSubUsersModulesModel extends Model
{
    protected $table = "merchant_user_modules";
    protected $primaryKey = "id";
    
    public function sub_user()
    {
        return $this->belongsTo('Rush\Modules\Merchant\Models\MerchantSubUsersModel', 'user_id', 'id');
    }

    public function module()
    {
        return $this->hasOne('Rush\Modules\Merchant\Models\MerchantModules', 'modules_id', 'id');
    }
}
