<?php

namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class MindbodyClassModel extends Model
{
    protected $table = 'merchant_mindbody_classes';
}
