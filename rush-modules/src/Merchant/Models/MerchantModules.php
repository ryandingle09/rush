<?php

namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantModules extends Model
{
    protected $table = "modules";
    protected $primaryKey = "id";
   
}
