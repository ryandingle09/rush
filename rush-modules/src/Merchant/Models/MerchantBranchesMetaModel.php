<?php

namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;
use Logaretm\Transformers\TransformableTrait;
use Logaretm\Transformers\Contracts\Transformable;

class MerchantBranchesMetaModel extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = "branches_meta";
}
