<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;
use Rush\Modules\Helpers\Facades\StorageHelper;

/**
 * Class MerchantSetting
 *
 * @property string $stamp_name
 * @property string $points_name
 * @property string $app_name
 * @property string $app_description
 * @property string $app_logo
 * @property string $program_name
 *
 * @mixin \Eloquent
 */
class MerchantSettingModel extends Model
{
    protected $table = "MerchantSettings";

    protected $guarded = ['id'];

    protected $fillable = [
        'program_name',
        'points_name',
        'app_name',
        'app_description',
        'app_logo',
        'points_transfer',
        'earn_transaction_limit',
        'earn_points_limit',
        'redemption_transaction_limit',
        'paypoint_transaction_limit',
        'paypoint_points_limit'
    ];

    public $timestamps = false;

    public function getStampNameAttribute()
    {
        return $this->points_name;
    }

    public function getEarnTransactionLimitAttribute( $value ){

        if( is_null($value) ) return (int) env('EARN_TRANSACTION_LIMIT');
        else return (int) $value;

    }

    public function getEarnPointsLimitAttribute( $value ){

        if( is_null($value) ) return (float) env('EARN_POINTS_LIMIT');
        else return (float) $value;

    }

    public function getPaypointTransactionLimitAttribute( $value ){

        if( is_null($value) ) return (int) env('PAYPOINT_TRANSACTION_LIMIT');
        else return (int) $value;

    }

    public function getPaypointPointsLimitAttribute( $value ){

        if( is_null($value) ) return (float) env('PAYPOINT_POINTS_LIMIT');
        else return (float) $value;

    }

    public function getRedemptionTransactionLimitAttribute( $value ){

        if( is_null($value) ) return (int) env('REDEMPTION_TRANSACTION_LIMIT');
        else return (int) $value;

    }

    public function getAppLogoAttribute($value)
    {
        return StorageHelper::repositoryUrlForView($value);
    }
}