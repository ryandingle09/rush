<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;
use Rush\Modules\Merchant\Models\MerchantIntegrationsFieldsModel;

class MerchantIntegrationsModel extends Model
{
    protected $table = "merchant_integrations";

    public function fields()
    {
        return $this->hasMany(MerchantIntegrationsFieldsModel::class, 'integration_id', 'id');
    }
}