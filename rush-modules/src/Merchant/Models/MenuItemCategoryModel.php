<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class MenuItemCategoryModel extends Model
{
    protected $table = "menu_item_categories";
}
