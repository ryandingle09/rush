<?php

namespace Rush\Modules\Merchant\Models;

use Config;
use Illuminate\Database\Eloquent\Model;
use Rush\Modules\Helpers\Facades\StorageHelper;
use Rush\Modules\Merchant\Models\MerchantBranchesModel;
use UUID;

class MerchantRewardsModel extends Model
{
    protected $table = "ItemsForRedeem";
    protected $primaryKey = "redeemItemId";
    
    public function getDataAttribute()
    {
        $data = [];
        $data['id'] = $this->uuid;
        $data['uuid'] = $this->uuid;
        $data['name'] = $this->name;
        $data['details'] = $this->details;
        $data['points_required'] = (float) $this->pointsRequired;
        $data['image_url'] = $this->imageURL ? StorageHelper::repositoryUrlForView($this->imageURL, 1) : null;
        $data['branches'] = $this->branches;
        $data['branch_ids'] = json_decode($this->branch_ids);

        return $data;
    }
    
    public function getUuidAttribute($value)
    {
        if(! $value ) {
            $value = UUID::generate();
            $this->uuid = $value;
            $this->save();
        }
        return $value;
    }

    public function getBranchInfoAttribute()
    {
        $branches = json_decode( $this->branch_ids );

        if( in_array( 0, $branches) )
        {
            return array( ["id" => 0, 'branchName' => 'All'] );
        } else {
            $var = '';
            foreach( $branches as $branch ) 
            {
                $var_b = MerchantBranchesModel::find( $branch );
                $var[] = array('id' => $branch, 'branchName' => $var_b->branchName);
            }
            return $var;
        }
    }
    
    public function getBranchesAttribute( $value ){

        $branches = [];
        $branch_ids = json_decode($this->branch_ids);
        $raw_branches = MerchantBranchesModel::whereIn('id', $branch_ids )->pluck('branchName');
        
        foreach( $raw_branches as $branch ){
            $branches[] = $branch;
        }

        if( in_array(0, $branch_ids) ) $branches[] = 'all';

        return $branches;

    }

    public function getImageURLAttribute($value)
    {
        return StorageHelper::repositoryUrlForView($value);
    }
}
