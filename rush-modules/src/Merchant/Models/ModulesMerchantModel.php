<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class ModulesMerchantModel extends Model
{
    protected $table = "modules_merchant";
    protected $primaryKey = "id";
}
