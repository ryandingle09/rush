<?php

namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Ramsey\Uuid\Uuid;

/**
 * Class MerchantEmployeesModel
 *
 * @property-read   integer                                               $id
 * @property-read   \Rush\Modules\Merchant\Models\MerchantBranchesModel   $branch
 */
class MerchantEmployeesModel extends Model{

    use SoftDeletes;

    protected $primaryKey = 'userId';
    protected $table = "Users";
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'merchantId',
        'username',
        'employeeName',
        'branchId'
    ];

    public function merchant()
    {
        return $this->belongsTo('Rush\Modules\Merchant\Models\MerchantModel', 'merchantId', 'merchantid');
    }

    public function branch()
    {
        return $this->belongsTo('Rush\Modules\Merchant\Models\MerchantBranchesModel', 'branchId', 'id');
    }

    public function getIsDeletedTextAttribute()
    {
        if ( $this->is_deleted == 1 ) return "disabled";
        else return "enabled";
    }

    public function getIdAttribute()
    {
        return $this->userId;
    }
    
    public function getUuidAttribute( $value )
    {
        if( ! $value ) {
            $value = Uuid::uuid1()->toString();
            $this->uuid = $value;
            $this->save();
        }

        return $value;
    }
}
