<?php

namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class WhitelistedNumberModel extends Model
{
    protected $table = "whitelisted_mobile_numbers";
    protected $primaryKey = "id";
}
