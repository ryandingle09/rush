<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class ProductTransactionHistoryModel extends Model
{
    protected $table = "product_transaction_history";

    public function customer()
    {
        return $this->belongsTo('Rush\Modules\Customer\Models\CustomerModel', 'customer_id', 'customerId');
    }

    public function product()
    {
        return $this->belongsTo('Rush\Modules\Merchant\Models\ProductsModel', 'product_id');
    }

    public function customer_transaction()
    {
        return $this->belongsTo('Rush\Modules\Customer\Models\CustomerTransactionsModel', 'customer_transaction_id');
    }

    public function getTotalAmountAttribute()
    {
        return $this->amount * $this->quantity;
    }

    public function getTotalPointsAttribute()
    {
        return $this->points * $this->quantity;
    }
}
