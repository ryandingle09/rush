<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class OrderStatusHistoryModel extends Model
{
    protected $table = "order_status_history";
}
