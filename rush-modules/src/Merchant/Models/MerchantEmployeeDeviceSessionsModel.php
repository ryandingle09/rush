<?php

namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MerchantEmployeeDeviceSessionsModel extends Model
{
    use SoftDeletes;

    protected $table = "employee_device_sessions";
    protected $dates = ['created_at', 'deleted_at'];

    public function employee()
    {
        return $this->belongsTo(MerchantEmployeesModel::class, 'userId', 'id');
    }
}

