<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class MenuItemModel extends Model
{
    protected $table = "menu_items";

    public function getUuidAttribute( $value ){

        if( ! $value ){

            $value = Uuid::uuid1()->toString();
            $this->uuid = $value;
            $this->save();

        }

        return $value;
    }

    public function category(){

        return $this->belongsTo('Rush\Modules\Merchant\Models\MenuItemCategoryModel');

    }
}
