<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class QRCodeModel extends Model
{
    protected $table = "qr_codes";

    public function getUuidAttribute( $value ){

        if( ! $value ){

            $value = Uuid::uuid1()->toString();
            $this->uuid = $value;
            $this->save();
        }

        return $value;
    }
    
    public function getImageUrlAttribute( $value ){

        if( ! $value ){

            $value = '/repository/merchant/qr_codes/' . $this->uuid . '.jpg';
            $this->image_url = $value;
            $this->save();
        }

        return env('APP_URL') . $value;

    }

    public function getBranchesAttribute(){

        $branches = [];
        $branch_ids = json_decode($this->branch_ids);
        $raw_branches = MerchantBranchesModel::whereIn('id', $branch_ids )->pluck('branchName');
        
        foreach( $raw_branches as $branch ){
            $branches[] = $branch;
        }

        if( in_array(0, $branch_ids) ) $branches[] = 'all';

        return $branches;

    }

    public function getStatusLabelAttribute(){
        return ($this->active == 1) ? 'Enabled' : 'Disabled';
    }
}
