<?php

namespace Rush\Modules\Merchant\Models\Traits\Branch;

use Ramsey\Uuid\Uuid;
use Rush\Modules\Helpers\Facades\StorageHelper;

trait AttributesTrait
{
    public function getLogoUrlAttribute()
    {
        return $this->branch_logo;
    }

    public function setLogoUrlAttribute($value)
    {
        $this->attributes['branch_logo'] = $value;
    }

    public function getNameAttribute()
    {
        return $this->branchName;
    }

    public function setNameAttribute($value)
    {
        $this->attributes['branchName'] = $value;
    }

    public function getUuidAttribute($value)
    {
        if (!$value) {
            $value = Uuid::uuid1()->toString();
            $this->uuid = $value;
            $this->save();
        }

        return $value;
    }
}
