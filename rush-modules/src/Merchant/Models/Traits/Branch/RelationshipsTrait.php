<?php

namespace Rush\Modules\Merchant\Models\Traits\Branch;

use Rush\Modules\Merchant\Models\VoucherModel;
use Rush\Modules\Merchant\Models\MerchantModel;
use Rush\Modules\Merchant\Models\MerchantEmployeesModel;
use Rush\Modules\Merchant\Models\MerchantBranchesMetaModel;
use Rush\Modules\ClassManagement\Models\ClassScheduleModel;
use Rush\Modules\Merchant\Models\MerchantBranchesDateQrModel;

trait RelationshipsTrait
{
    public function employee()
    {
        return $this->belongsTo(MerchantEmployeesModel::class, 'id', 'branchId');
    }

    public function merchant()
    {
        return $this->belongsTo(MerchantModel::class, 'merchantId', 'merchantid');
    }

    public function metas()
    {
        return $this->hasMany(MerchantBranchesMetaModel::class, 'branch_id');
    }

    public function date_qr_codes()
    {
        return $this->hasMany(MerchantBranchesDateQrModel::class, 'branch_id', 'id');
    }

    public function class_schedules()
    {
        return $this->hasMany(ClassScheduleModel::class, 'branch_id', 'id');
    }

    public function vouchers()
    {
        return $this->hasMany(VoucherModel::class, 'branch_id', 'id');
    }
}
