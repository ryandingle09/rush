<?php

namespace Rush\Modules\Merchant\Models\Traits\Merchant;

use Rush\Modules\Merchant\Models\Transformers\PackageDesignTransformer;

trait AttributesTrait
{
    public function getIdAttribute()
    {
        return $this->merchantid;
    }

    public function getNameAttribute()
    {
        return $this->businessName;
    }

    public function getCustomerDesignAttribute()
    {
        return $this->package_designs()->where('designType', 3)->first();
    }

    public function getMerchantDesignAttribute()
    {
        return $this->package_designs()->where('designType', 1)->first();
    }

    public function getDesignsAttribute()
    {
        $transformer = new PackageDesignTransformer;
        $customer = $transformer->transform($this->customer_design);
        $merchant = $transformer->transform($this->merchant_design);
        $customer['stamp_url'] = $merchant['stamp_url'];
        $customer['stamp_gray_url'] = $merchant['stamp_gray_url'];
        return  [
            'customer' => $customer,
            'merchant' => $merchant
        ];
    }

    public function getIsLoyaltyAttribute()
    {
        return $this->packageId == 1 ? true : false;
    }

    public function getIsBasicAttribute()
    {
        return $this->packageId == 2 ? true : false;
    }

    public function getBusinessAttribute()
    {
        return $this->businessName;
    }
}
