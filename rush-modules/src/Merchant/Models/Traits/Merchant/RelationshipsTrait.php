<?php

namespace Rush\Modules\Merchant\Models\Traits\Merchant;

use Rush\Modules\Merchant\Models\PackageModel;
use Rush\Modules\Merchant\Models\ProductsModel;
use Rush\Modules\Merchant\Models\MerchantMetaModel;
use Rush\Modules\Merchant\Models\MerchantPackageModel;
use Rush\Modules\Merchant\Models\MerchantSettingModel;
use Rush\Modules\Achievements\Models\AchievementsModel;
use Rush\Modules\Merchant\Models\WhitelistedNumberModel;
use Rush\Modules\Merchant\Models\MerchantIntegrationsModel;
use Rush\Modules\Merchant\Models\MerchantPromoMechanicsModel;
use Rush\Modules\Merchant\Models\NotificationSmsMessagesModel;
use Rush\Modules\Merchant\Models\NotificationSmsSettingsModel;
use Rush\Modules\Merchant\Models\MerchantFeedbackCategoryModel;
use Rush\Modules\Merchant\Models\MerchantPunchCardMilestoneModel;
use Rush\Modules\ClassManagement\Models\PackageModel as ClassPackageModel;

trait RelationshipsTrait
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne|\Rush\Modules\Merchant\Models\MerchantSetting
     */
    public function settings()
    {
        return $this->hasOne(MerchantSettingModel::class, 'merchant_id', 'merchantid');
    }

    public function whitelisted_numbers()
    {
        return $this->hasMany(WhitelistedNumberModel::class, 'merchant_id', 'merchantid');
    }

    public function package()
    {
        return $this->hasOne(PackageModel::class, 'packageId', 'packageId');
    }

    public function package_designs()
    {
        return $this->hasMany(MerchantPackageModel::class, 'merchantId', 'merchantid');
    }

    public function sms_credentials()
    {
        return $this->hasOne('Rush\Modules\Merchant\Models\MerchantSmsCredentialsModel', 'merchant_id', 'merchantid');
    }

    public function sms_settings()
    {
        return $this->hasOne(NotificationSmsSettingsModel::class, 'merchant_id', 'merchantid');
    }

    public function sms_messages()
    {
        return $this->hasMany(NotificationSmsMessagesModel::class, 'merchant_id', 'merchantid');
    }

    public function integrations()
    {
        return $this->hasMany(MerchantIntegrationsModel::class, 'merchant_id', 'merchantid');
    }

    public function achievements()
    {
        return $this->hasMany(AchievementsModel::class, 'merchant_id', 'merchantid');
    }

    public function punchcard_milestones()
    {
        return $this->hasMany(MerchantPunchCardMilestoneModel::class, 'merchant_id', 'merchantid');
    }

    public function feedback_categories()
    {
        return $this->hasMany(MerchantFeedbackCategoryModel::class, 'merchant_id', 'merchantid');
    }

    public function punchcards()
    {
        return $this->hasMany(MerchantPromoMechanicsModel::class, 'merchant_id', 'merchantid');
    }

    public function metas()
    {
        return $this->hasMany(MerchantMetaModel::class, 'merchant_id', 'merchantid');
    }

    public function class_packages()
    {
        return $this->hasMany(ClassPackageModel::class, 'merchant_id', 'merchantid');
    }

    public function products()
    {
        return $this->hasMany(ProductsModel::class, 'merchant_id', 'merchantid');
    }
}
