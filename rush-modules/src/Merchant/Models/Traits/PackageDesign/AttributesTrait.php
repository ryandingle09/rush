<?php

namespace Rush\Modules\Merchant\Models\Traits\PackageDesign;

use Rush\Modules\Helpers\Facades\StorageHelper;
use Intervention\Image\ImageManager;

trait AttributesTrait
{
    public function getStampGreyscaleAttribute()
    {
        $disk = StorageHelper::getRepositoryDisk();
        $stamp_path = StorageHelper::rebasePathToRepository($this->stampURL);
        if ($disk->exists($stamp_path)) {
            $gray_stamp_filename = str_replace('.png', '', basename($stamp_path)) . '-g.png';
            $gray_stamp_path = StorageHelper::rebasePathToRepository($this->assets_paths['stamp'] . '/' . $gray_stamp_filename);
            if (!$disk->exists($gray_stamp_path)) {
                $stamp = $disk->get($stamp_path);
                $image_manager = new ImageManager(array('driver' => 'gd'));
                $image = $image_manager->make($disk->get($stamp_path))->greyscale();
                $disk->put($gray_stamp_path, $image->stream()->__toString(), 'public');
            }
        }

        $obj = new \stdClass();
        $obj->url = $disk->exists($stamp_path) ? StorageHelper::repositoryUrlForView('/repository/' . $gray_stamp_path, true) : null;
        $obj->path = $disk->exists($stamp_path) ? StorageHelper::rebasePathToRepository($gray_stamp_path) : null;

        return $obj;
    }

    public function getLogoAttribute()
    {
        $disk = StorageHelper::getRepositoryDisk();
        $logo_path = StorageHelper::rebasePathToRepository($this->logoURL);
        $obj = new \stdClass();
        $obj->url = $disk->exists($logo_path) ? StorageHelper::repositoryUrlForView($this->logoURL, true) : null;
        $obj->path = $disk->exists($logo_path) ? $this->logoURL : null;

        return $obj;
    }

    public function getBackgroundAttribute()
    {
        $disk = StorageHelper::getRepositoryDisk();
        $background_path = StorageHelper::rebasePathToRepository($this->backgroundURL);

        $obj = new \stdClass();
        $obj->url = $disk->exists($background_path) ? StorageHelper::repositoryUrlForView($this->backgroundURL, true) : null;
        $obj->path = $disk->exists($background_path) ? $this->backgroundURL : null;

        return $obj;
    }

    public function getStampAttribute()
    {
        $disk = StorageHelper::getRepositoryDisk();
        $stamp_path = StorageHelper::rebasePathToRepository($this->stampURL);

        $obj = new \stdClass();
        $obj->url = $disk->exists($stamp_path) ? StorageHelper::repositoryUrlForView($this->stampURL, true) : null;
        $obj->path = $disk->exists($stamp_path) ? $this->stampURL : null;

        return $obj;
    }

    public function getColorsAttribute()
    {
        $obj = new \stdClass();
        $obj->text = $this->textColor;
        $obj->button = $this->buttonsColor;
        $obj->background = $this->backgroundColorHEX;

        return $obj;
    }
}
