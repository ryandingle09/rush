<?php

namespace Rush\Modules\Merchant\Models;

use Endroid\QrCode\QrCode;
use Intervention\Image\ImageManager;
use Illuminate\Database\Eloquent\Model;
use Rush\Modules\Helpers\Facades\StorageHelper;

/**
 * Class MerchantBranchesDateQrModel
 *
 * @property-read   integer                 $id
 * @property        string                  $uuid
 * @property        \Carbon\Carbon          $date_generated
 * @property        MerchantBranchesModel   $branch
 */
class MerchantBranchesDateQrModel extends Model
{
    protected $table = "branches_date_qr";

    protected $dates = ['created_at', 'date_generated'];

    protected $fillable = ['uuid', 'date_generated', 'branch_id'];

    public function getQrAttribute()
    {
        $disk = StorageHelper::getRepositoryDisk();
        $file_path = 'branches/qrcode/'. $this->uuid . '.png';
        if (!$disk->exists($file_path)) {
            $image_manager = new ImageManager(array('driver' => 'gd'));
            $qr_code = new QrCode();
            $qr_code->setText($this->uuid);
            $qr_code->setSize(300);
            $image = $image_manager->make($qr_code->getImage());
            $disk->put($file_path, $image->stream());
        }

        $obj = new \stdClass();
        $obj->path = '/repository/' . $file_path;
        $obj->url = config('app.url') . '/repository/' . $file_path;

        return $obj;
    }

    public function branch()
    {
        return $this->belongsTo(MerchantBranchesModel::class, 'branch_id', 'id');
    }
}
