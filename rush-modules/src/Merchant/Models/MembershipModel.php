<?php

namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class MembershipModel extends Model
{
    protected $table = "customer_memberships";
    public $timestamps = true;
    protected $fillable = [
        'name',
        'level',
        'frequency_redemption',
        'merchant_id'
    ];

    /**
     * defines the type string of this model when used in polymorphic relationships
     * @var string
     */
    protected $morphClass = 'Membership';

    /**
     * @return Rush\Modules\Merchant\Models\MerchantConversionSetting
     */
    public function conversion_setting()
    {
        return $this->morphOne('\Rush\Modules\Merchant\Models\MerchantConversionSettingModel', 'transformable');
    }
}
