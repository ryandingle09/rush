<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MerchantModel
 * @package Rush\Modules\Merchant\Models
 *
 * @property integer $id
 * @property integer $packageId
 * @property \Rush\Modules\Merchant\Models\MerchantConversionSettingModel $conversion_setting
 * @property \Rush\Modules\Customer\Models\CustomerModel[] $customers
 *
 */
class BillingModel extends Model
{
    protected $table = "Billing";
    protected $primaryKey = "id";
}
