<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class MerchantConversionSetting
 * @package Rush\Modules\Merchant\Models
 *
 * @property int    $redemption_points
 * @property float  $redemption_peso
 * @property int    $earning_points
 * @property float  $earning_peso
 */
class MerchantConversionSettingModel extends Model
{
    use SoftDeletes;

    const UPDATED_AT = 'modified_at';

    protected $table = "MerchantConversionSettings";

    protected $fillable = [
        'merchant_id',
        'package_id',
        'redemption_points',
        'redemption_peso',
        'earning_peso',
        'earning_points',
        'created_by',
        'modified_by',
    ];

    /**
     * Removed from toArray() since it produces unecessary
     * probing query just to build this attributes. These
     * attributes are only usable in CRUD but not during
     * Transactions like Earning or Redeeming.
     * @var array
     */
    /*protected $appends = [
        'branch_id',
        'branch_name',
        'membership_id',
        'membership_name'
    ];*/

    public function getRedemptionPointsAttribute($value)
    {
       return (int) $value;
    }

    public function getRedemptionPesoAttribute($value)
    {
        return (float) $value;
    }

    public function getEarningPointsAttribute($value)
    {
        return (int) $value;
    }

    public function getEarningPesoAttribute($value)
    {
        return (float) $value;
    }

    /**
     * This resolves to the owning Object
     */
    public function transformable()
    {
        return $this->morphTo();
    }

    // Custom Attributes
    protected $branch = null;
    protected $membership = null;

    public function branch()
    {
        if (!$this->branch) {
            $attr = ['key' => '', 'name' => ''];
            if ($this->transformable->getMorphClass() == 'MembershipBranch') {
                $attr['key'] = $this->transformable->branch->getKey();
                $attr['name'] = $this->transformable->branch->branchName;
            } else if ($this->transformable->getMorphClass() == 'Branch') {
                $attr['key'] = $this->transformable->getKey();
                $attr['name'] = $this->transformable->branchName;
            }
            $this->branch = (Object) $attr;
        }

        return $this->branch;
    }

    public function membership()
    {
        if (!$this->membership) {
            $attr = ['key' => '', 'name' => ''];
            if ($this->transformable->getMorphClass() == 'MembershipBranch') {
                $attr['key'] = $this->transformable->membership->getKey();
                $attr['name'] = $this->transformable->membership->name;
            } else if ($this->transformable->getMorphClass() == 'Membership') {
                $attr['key'] = $this->transformable->getKey();
                $attr['name'] = $this->transformable->name;
            }
            $this->membership = (Object) $attr;
        }

        return $this->membership;
    }

    public function getBranchIdAttribute($value)
    {
        return $this->branch()->key;
    }

    public function getBranchNameAttribute($value)
    {
        return $this->branch()->name;
    }

    public function getMembershipIdAttribute($value)
    {
        return $this->membership()->key;
    }

    public function getMembershipNameAttribute($value)
    {
        return $this->membership()->name;
    }
}