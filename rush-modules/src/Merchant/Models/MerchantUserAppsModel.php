<?php
namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantUserAppsModel extends Model
{
    protected $table = "lara_user_apps";

    public function user()
    {
        return $this->belongsTo('Rush\Modules\Merchant\Models\MerchantUserModel');
    }

}