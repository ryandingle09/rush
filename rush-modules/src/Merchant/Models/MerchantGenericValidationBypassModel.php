<?php

namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantGenericValidationBypassModel extends Model
{
    protected $table = "merchant_generic_validation_bypass";
}
