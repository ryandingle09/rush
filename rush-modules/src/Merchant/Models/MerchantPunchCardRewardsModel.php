<?php

namespace Rush\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;
use Rush\Modules\Helpers\Facades\StorageHelper;
use Illuminate\Database\Eloquent\SoftDeletes;

class MerchantPunchCardRewardsModel extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    protected $table = "MerchantPunchCardRewards";
    
    public function getDataAttribute()
    {
        $reward = [];
        $reward['id'] = $this->uuid;
        $reward['name'] = $this->reward;
        $reward['stamps'] = (int) $this->no_stamps;
        $reward['image_url'] = $this->image_url;

        return $reward;
    }
    
    public function getNameAttribute()
    {
        return $this->reward;
    }
    
    public function getUuidAttribute($value)
    {
        if( ! $value ) {
            $value = Uuid::uuid1()->toString();
            $this->uuid = $value;
            $this->save();
        }
        return $value;
    }

    public function getImageUrlAttribute($value)
    {
        return StorageHelper::repositoryUrlForView($value);
    }
}
