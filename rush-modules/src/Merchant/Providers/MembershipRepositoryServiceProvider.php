<?php

namespace Rush\Modules\Merchant\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Merchant\Models\MembershipModel;
use Rush\Modules\Merchant\Repositories\MembershipRepository;

/**
* Register our Repository with Laravel
*/
class MembershipRepositoryServiceProvider extends ServiceProvider
{
    /**
    * Registers the MembershipInterface with Laravels IoC Container
    */
    public function register()
    {
        $this->app->bind(
            'Rush\Modules\Merchant\Repositories\MembershipInterface',
            function($app) {
                return new MembershipRepository(new MembershipModel());
            }
        );
    }
}
