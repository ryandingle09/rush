<?php

namespace Rush\Modules\Merchant\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Merchant\Models\MembershipBranchModel;
use Rush\Modules\Merchant\Repositories\MembershipBranchRepository;

/**
* Register our Repository with Laravel
*/
class MembershipBranchRepositoryServiceProvider extends ServiceProvider
{
    /**
    * Registers the MembershipBranchInterface with Laravels IoC Container
    */
    public function register()
    {
        $this->app->bind(
            'Rush\Modules\Merchant\Repositories\MembershipBranchInterface',
            function($app) {
                return new MembershipBranchRepository(new MembershipBranchModel());
            }
        );
    }
}
