<?php

namespace Rush\Modules\Merchant\Providers;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;
use Rush\Modules\Merchant\Repositories\SmsNotificationInterface;
use Rush\Modules\Merchant\Repositories\SmsNotificationRepository;

/**
 * Class MerchantRepositoryServiceProvider
 * @package Rush\Modules\Merchant\Providers
 */
class SmsNotificationRepositoryServiceProvider extends ServiceProvider
{
    public function boot()
    {
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(SmsNotificationInterface::class, SmsNotificationRepository::class);
    }
}
