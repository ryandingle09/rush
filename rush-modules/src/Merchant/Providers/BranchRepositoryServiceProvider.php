<?php
namespace Rush\Modules\Merchant\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Merchant\Repositories\BranchInterface;
use Rush\Modules\Merchant\Repositories\BranchRepository;

/**
 * Class MerchantRepositoryServiceProvider
 * @package Rush\Modules\Merchant\Providers
 */
class BranchRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(BranchInterface::class, BranchRepository::class);
    }
}
