<?php

namespace Rush\Modules\Merchant\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Merchant\Models\MerchantSettingModel;
use Rush\Modules\Merchant\Repositories\MerchantSettingRepository;

/**
* Register our Repository with Laravel
*/
class MerchantSettingRepositoryServiceProvider extends ServiceProvider
{
    /**
    * Registers the MerchantSettingInterface with Laravels IoC Container
    */
    public function register()
    {
        $this->app->bind(
            'Rush\Modules\Merchant\Repositories\MerchantSettingInterface',
            function($app) {
                return new MerchantSettingRepository(new MerchantSettingModel());
            }
        );
    }
}
