<?php
namespace Rush\Modules\Merchant\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Merchant\Repositories\ProductQRInterface;
use Rush\Modules\Merchant\Repositories\ProductQRRepository;

class ProductQRRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ProductQRInterface::class, ProductQRRepository::class);
    }
}