<?php

namespace Rush\Modules\Merchant\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Merchant\Models\MerchantConversionSettingModel;
use Rush\Modules\Merchant\Repositories\MerchantConversionSettingRepository;

/**
* Register our Repository with Laravel
*/
class MerchantConversionSettingRepositoryServiceProvider extends ServiceProvider
{
    /**
    * Registers the MerchantSettingInterface with Laravels IoC Container
    */
    public function register()
    {
        $this->app->bind(
            'Rush\Modules\Merchant\Repositories\MerchantConversionSettingInterface',
            function($app) {
                return new MerchantConversionSettingRepository(new MerchantConversionSettingModel());
            }
        );
    }
}
