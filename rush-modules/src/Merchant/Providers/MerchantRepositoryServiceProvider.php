<?php

namespace Rush\Modules\Merchant\Providers;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;
use Rush\Modules\Merchant\Models\MerchantModel;
use Rush\Modules\Merchant\Repositories\MerchantInterface;
use Rush\Modules\Merchant\Repositories\MerchantRepository;

/**
 * Class MerchantRepositoryServiceProvider
 * @package Rush\Modules\Merchant\Providers
 */
class MerchantRepositoryServiceProvider extends ServiceProvider
{
    protected $commands = [
      'Rush\Modules\Merchant\Console\Commands\MerchantUpdateStatusCommand',
      'Rush\Modules\Merchant\Console\Commands\HtpMigrate',
      'Rush\Modules\Merchant\Console\Commands\HtpMigrateRollback',
    ];

    public function boot()
    {
        // schedule commands internally when the app has booted
        $this->app->booted(function () {
            $schedule = $this->app->make(Schedule::class);
            $schedule->command('merchant-update:status')->daily();
        });

        Relation::morphMap([
            'Merchant' => \Rush\Modules\Merchant\Models\MerchantModel::class,
            'Branch' => \Rush\Modules\Merchant\Models\MerchantBranchesModel::class,
            'Membership' => \Rush\Modules\Merchant\Models\MembershipModel::class,
            'MembershipBranch' => \Rush\Modules\Merchant\Models\MembershipBranchModel::class,
        ]);
    }
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands($this->commands);

        $this->app->bind(
            'Rush\Modules\Merchant\Repositories\MerchantInterface',
            function($app) {
                return new MerchantRepository(new MerchantModel());
            }
        );
    }
}
