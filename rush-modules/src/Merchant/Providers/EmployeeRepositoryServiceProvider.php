<?php
namespace Rush\Modules\Merchant\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Merchant\Repositories\EmployeeInterface;
use Rush\Modules\Merchant\Repositories\EmployeeRepository;

/**
 * Class MerchantRepositoryServiceProvider
 * @package Rush\Modules\Merchant\Providers
 */
class EmployeeRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(EmployeeInterface::class, EmployeeRepository::class);
    }
}
