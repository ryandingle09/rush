<?php

namespace Rush\Modules\Merchant\Services;

use Rush\Modules\Sms\Services\SmsService;
use Rush\Modules\Email\Services\EmailService;

class NotificationService
{
    protected $sms_service;
    protected $email_service;

    public function __construct( SmsService $sms_service, EmailService $email_service ){

        $this->sms_service = $sms_service;
        $this->email_service = $email_service;

    }

    public function get_merchant_notification_service( $merchant ){

        if( $merchant->settings->customer_login_param == 'email' ) return $this->email_service;
        else return $this->sms_service;

    }

    public function earn_non_member_points( $merchant, $transaction ){

        $notification_service = $this->get_merchant_notification_service( $merchant );
        $notification_service->earn_non_member_points( $transaction );

    }

    public function registration( $merchant, $customer ){

        $notification_service = $this->get_merchant_notification_service( $merchant );
        $notification_service->registration( $customer );

    }

    public function earnPoints( $merchant, $transaction, $amount ){

        $notification_service = $this->get_merchant_notification_service( $merchant );
        $notification_service->earnPoints( $transaction, $amount );

    }

    public function payPoints( $merchant, $transaction ){

        $notification_service = $this->get_merchant_notification_service( $merchant );
        $notification_service->payPoints( $transaction );

    }

    public function redeemPoints( $merchant, $transaction ){

        $notification_service = $this->get_merchant_notification_service( $merchant );
        $notification_service->redeemPoints( $transaction );

    }

    // claim loyalty reward.
    public function claimReward( $merchant, $transaction ){

        $notification_service = $this->get_merchant_notification_service( $merchant );
        $notification_service->claimReward( $transaction );

    }

    // claim puchcard reward.
    public function claimPunchcardReward( $merchant, $transaction ){

        $notification_service = $this->get_merchant_notification_service( $merchant );
        $notification_service->claimPunchcardReward( $transaction );

    }

    public function voidEarn( $merchant, $transaction ){

        $notification_service = $this->get_merchant_notification_service( $merchant );
        $notification_service->voidEarn( $transaction );

    }

    public function voidRedeem( $merchant, $transaction ){

        $notification_service = $this->get_merchant_notification_service( $merchant );
        $notification_service->voidRedeem( $transaction );

    }

    public function voidPaypoints( $merchant, $transaction ){

        $notification_service = $this->get_merchant_notification_service( $merchant );
        $notification_service->voidPaypoints( $transaction );

    }

    public function invite( $merchant, $customer, $mobile_numbers, $emails ){

        $notification_service = $this->get_merchant_notification_service( $merchant );
        $notification_service->invite( $merchant, $customer, $mobile_numbers, $emails );

    }

    public function transferPoints( $merchant, $transaction, $receiver ){

        $notification_service = $this->get_merchant_notification_service( $merchant );
        $notification_service->transferPoints( $transaction, $receiver );

    }

    public function transferPointsReceiverSms( $merchant, $transaction, $receiver ){

        $notification_service = $this->get_merchant_notification_service( $merchant );
        $notification_service->transferPointsReceiverSms( $transaction, $receiver );

    }

    public function redeemAmaxReward( $merchant, $transaction, $promo ){

        $notification_service = $this->get_merchant_notification_service( $merchant );
        return $notification_service->redeemAmaxReward( $transaction, $promo );

    }

    public function redeemAmaxRewardFailed( $merchant, $customer ){

        $notification_service = $this->get_merchant_notification_service( $merchant );
        return $notification_service->redeemAmaxRewardFailed( $customer );

    }

    public function voidPackage( $merchant, $transaction ){

        $notification_service = $this->get_merchant_notification_service( $merchant );
        $notification_service->voidPackage( $transaction );

    }

    public function send_notification( $merchant, $customer, $flag, $data = [] ){

        $notification_service = $this->get_merchant_notification_service( $merchant );
        $notification_service->send_notification( $merchant, $customer, $flag, $data );

    }
}
