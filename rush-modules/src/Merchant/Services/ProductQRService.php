<?php
namespace Rush\Modules\Merchant\Services;

use Rush\Modules\Merchant\Repositories\ProductQRInterface;
use Ramsey\Uuid\Uuid;
use Endroid\QrCode\QrCode;
use Intervention\Image\ImageManager;
use Rush\Modules\Helpers\Facades\StorageHelper;

class ProductQRService
{
    protected $product_qr_repository;

    public function __construct(ProductQRInterface $product_qr_repository)
    {
        $this->product_qr_repository = $product_qr_repository;
    }

    public function getAll($merchant_id = null)
    {
        if($merchant_id == null){
            return $this->product_qr_repository->all();
        }

        return $this->product_qr_repository->getAll($merchant_id);
    }

    public function storeProductQR($product_qr_data){

        $product_qr_data['uuid'] = Uuid::uuid1()->toString();

        // generate qr code.
        $qr_code_uuid = $product_qr_data['uuid'];
        $qr_code = new QrCode( $qr_code_uuid );
        $qr_code->setSize(300);


        // generate image of the qr code.
        $image_manager = new ImageManager(array('driver' => 'gd'));
        $image = $image_manager->make( $qr_code->getImage() );

        // save qr code image in the file system.
        $disk = StorageHelper::getRepositoryDisk();
        $file_path = 'merchant/qr_codes/'. $qr_code_uuid . '.jpg';

        if( ! $disk->exists($file_path) ){
            $disk->put( $file_path, $image->stream() );
        }

        return $this->product_qr_repository->storeProductQR($product_qr_data);
    }

    public function updateProductQR($product_qr_id, $product_data){
        return $this->product_qr_repository->updateProductQR($product_qr_id, $product_data);
    }

    public function delete($id){
        return $this->product_qr_repository->delete($id);
    }
}