<?php

namespace Rush\Modules\Merchant\Services;

use Rush\Modules\Merchant\Repositories\MembershipRepository;

/**
* Our MembershipService, containing all useful methods for business logic around Membership
* Service just hands off data it was given by the controller to the appropriate repositories
*/
class MembershipService
{
    /**
     * @var Rush\Modules\Merchant\Repositories\MembershipRepository
     */
    protected $membershipRepository;

    public function __construct(MembershipRepository $membershipRepository)
    {
        $this->membershipRepository = $membershipRepository;
    }

    public function getByMerchant($merchantId)
    {
        return $this->membershipRepository->getByMerchant($merchantId);
    }

    public function getListByMerchant($merchantId)
    {
        return $this->membershipRepository->getListByMerchant($merchantId);
    }

    /**
     * Returns associative array of unassigned levels for the current Merchant
     * @param  int $merchantId
     * @return array
     */
    public function getLevelsByMerchant($merchantId)
    {
        $maxLevel = 10;
        $freeLevels = array_combine(range(1,$maxLevel), range(1,$maxLevel));
        $levels = $this->membershipRepository->getLevelsByMerchant($merchantId);
        foreach ($levels as $level) {
            unset($freeLevels[$level]);
        }

        return $freeLevels;
    }

    public function store(array $input)
    {
        $membershipInput = $input['input'];

        $validator = $this->validate(
            $membershipInput,
            [   'name' => 'required|between:1,20',
                'level' => 'required|integer',
                'frequency_redemption' => 'sometimes|integer',
                'membership_id' => 'sometimes|exists:'.$this->membershipRepository->getTable().',id,merchant_id,'.$membershipInput['merchant_id']],
            []
        );

        if ($validator->fails()) {
            return $validator;
        }

        if (!empty($membershipInput['membership_id'])) {
            $membership = $this->membershipRepository->update($membershipInput['membership_id'], $membershipInput);
        } else {
            $membership = $this->membershipRepository->fill($membershipInput);
            $membership->save();
        }

        return true;
    }

    private function validate($input, $rules, $messages = [])
    {
        return \Validator::make($input, $rules, $messages);
    }
}
