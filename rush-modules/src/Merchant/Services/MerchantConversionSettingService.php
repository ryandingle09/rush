<?php

namespace Rush\Modules\Merchant\Services;

use Rush\Modules\Merchant\Repositories\BranchInterface;
use Rush\Modules\Merchant\Repositories\MembershipInterface;
use Rush\Modules\Merchant\Repositories\MerchantConversionSettingInterface;
use Rush\Modules\Merchant\Repositories\MerchantInterface;

/**
* Our MerchantConversionSettingService, containing all useful methods for business logic around MerchantConversionSetting
* Service just hands off data it was given by the controller to the appropriate repositories
*/
class MerchantConversionSettingService
{
    /**
     * @var Rush\Modules\Merchant\Repositories\MerchantConversionSettingRepository
     */
    protected $conversionRepository;

    /**
     * @var Rush\Modules\Merchant\Repositories\MerchantRepository
     */
    protected $merchantRepository;

    /**
     * @var Rush\Modules\Merchant\Repositories\BranchRepository
     */
    protected $branchRepository;

    /**
     * @var Rush\Modules\Merchant\Repositories\MembershipRepository
     */
    protected $membershipRepository;

    /**
     * @var Rush\Modules\Merchant\Services\MembershipBranchService
     */
    protected $membershipBranchService;

    public function __construct(
        MerchantConversionSettingInterface $conversionRepository,
        MerchantInterface $merchantRepository,
        BranchInterface $branchRepository,
        MembershipInterface $membershipRepository,
        MembershipBranchService $membershipBranchService
    ) {
        $this->conversionRepository = $conversionRepository;
        $this->merchantRepository = $merchantRepository;
        $this->branchRepository = $branchRepository;
        $this->membershipRepository = $membershipRepository;
        $this->membershipBranchService = $membershipBranchService;
    }

    public function getById($id)
    {
        return $this->conversionRepository->getById($id);
    }

    public function getByMerchantId($merchantId)
    {
        return $this->conversionRepository->getByMerchantId($merchantId);
    }

    public function store(array $input)
    {
        $conversionInput = $input['input'];

        $validator = $this->validate(
            $conversionInput,
            [   'earning_peso' => 'required|integer|min:1',
                'earning_points' => 'required|integer|min:1',
                'redemption_points' => 'required|integer|min:1',
                'redemption_peso' => 'required|integer|min:1'],
            []
        );

        if ($validator->fails()) {
            return $validator;
        }

        if (!empty($conversionInput['branch']) && !empty($conversionInput['membership'])) {
            $transformable = $this->membershipBranchService->getByMembershipBranch($conversionInput['membership'], $conversionInput['branch']);
        } else if (!empty($conversionInput['branch'])) {
            $transformable = $this->branchRepository->getById($conversionInput['branch']);
        } else if (!empty($conversionInput['membership'])) {
            $transformable = $this->membershipRepository->getById($conversionInput['membership']);
        } else {
            $transformable = $this->merchantRepository->getById($conversionInput['merchant_id']);
        }

        $previousConversion = $transformable->conversion_setting;
        if ($previousConversion) {
            $previousConversion->delete();
        }

        $newConversion = $this->conversionRepository->fill($conversionInput);
        $newConversion->save();
        $transformable->conversion_setting()->save($newConversion);

        return true;
    }

    private function validate($input, $rules, $messages = [])
    {
        return \Validator::make($input, $rules, $messages);
    }
}
