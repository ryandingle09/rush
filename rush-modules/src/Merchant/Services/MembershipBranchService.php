<?php

namespace Rush\Modules\Merchant\Services;

use Rush\Modules\Merchant\Repositories\MembershipBranchRepository;

/**
* Our MembershipBranchService, containing all useful methods for business logic around MembershipBranch
* Service just hands off data it was given by the controller to the appropriate repositories
*/
class MembershipBranchService
{
    /**
     * @var Rush\Modules\Merchant\Repositories\MembershipBranchRepository
     */
    protected $membershipBranchRepository;

    public function __construct(MembershipBranchRepository $membershipBranchRepository)
    {
        $this->membershipBranchRepository = $membershipBranchRepository;
    }

    public function getByMembershipBranch($membershipId, $branchId)
    {
        $membershipBranch = $this->membershipBranchRepository->getByMembershipBranch($membershipId, $branchId);
        if (!$membershipBranch) {
            $membershipBranch = $this->store(['membership_id' => $membershipId, 'branch_id' => $branchId]);
        }

        return $membershipBranch;
    }

    public function store(array $input)
    {
        return $this->membershipBranchRepository->create($input);
    }
}
