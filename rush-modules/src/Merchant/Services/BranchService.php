<?php

namespace Rush\Modules\Merchant\Services;

use Validator;
use Rush\Modules\Merchant\Repositories\BranchInterface;
use Rush\Modules\ClassManagement\Repositories\ClassInterface;
use Rush\Modules\ClassManagement\Repositories\ClassRepository;
use Rush\Modules\ClassManagement\Repositories\InstructorInterface;
use Rush\Modules\ClassManagement\Repositories\InstructorRepository;

class BranchService
{
    /** @var \Rush\Modules\Merchant\Repositories\BranchRepository */
    protected $branchRepository;
    protected $instructorRepository;
    protected $classRepository;

    public function __construct(
        BranchInterface $branchRepository,
        InstructorInterface $instructorRepository,
        ClassInterface $classRepository) {
        $this->branchRepository = $branchRepository;
        $this->instructorRepository = $instructorRepository;
        $this->classRepository = $classRepository;
    }

    public function getBranches( $merchant_id ){

        return $this->branchRepository->getMerchantBranches( $merchant_id );

    }

    public function createBranch( $merchant_id, $request ){

        // validate branch inputs.
        $errors = $this->validateBranchDetails( $request );

        // return formatted validation errors.
        if( count($errors) ) return $this->branchRepository->returnDetailsErrors( $errors );

        // create merchant branch.
        else return $this->branchRepository->createBranch( $merchant_id, $request );

    }

    public function validateBranchDetails( $request ){

        $validator = Validator::make($request->all(), [
            'branch_name' => 'required',
            'street_name' => 'required',
            'district' => 'required',
            'city' => 'required',
            'zipcode' => 'required',
            'longitude' => 'required',
            'latitude' => 'required',
        ]);

        return $validator->errors();

    }

    public function getSingleBranch( $uuid ){

        return $this->branchRepository->getSingleBranch( $uuid );

    }

    public function updateBranch( $uuid, $request ){

        $errors = $this->validateBranchDetails( $request );

        if( count($errors) ) return $this->branchRepository->returnDetailsErrors( $errors );

        else return $this->branchRepository->updateBranch( $uuid, $request );

    }

    public function deleteBranch( $uuid ){

        return $this->branchRepository->deleteBranch( $uuid );

    }

    public function getListByMerchant($merchantId)
    {
        return $this->branchRepository->getListByMerchant($merchantId);
    }

    /**
     *  Get branch by ID or UUID
     * @param  integer|string $id
     * @param  array $args
     * @return mixed|null
     */
    public function find($id, $args = [])
    {
        return $this->branchRepository->find($id, $args);
    }

    public function findByBranchName($branchName, $merchantId, $idToIgnore = null) {
        return $this->branchRepository->findByBranchName($branchName, $merchantId, $idToIgnore);
    }

    public function getRedemptionBranch($merchant)
    {
        return $this->branchRepository->getRedemptionBranch($merchant);
    }

    public function getBranchesByMerchantId($merchant_id)
    {
        return $this->branchRepository->getBranchesByMerchant($merchant_id);
    }

    /**
     * @param \Rush\Modules\Merchant\Models\MerchantBranchesModel $branch
     * @param \Carbon\Carbon|bool $date
     * @param bool|false $create
     * @return mixed
     */
    public function getDateQrCode($branch, $date, $create = false)
    {
        return $this->branchRepository->getDateQrCode($branch, $date, $create);
    }

    /**
     * @param string $date_qr_uuid
     * @return \Rush\Modules\Merchant\Models\MerchantBranchesModel
     */
    public function getBranchByDateQrUuid($date_qr_uuid)
    {
        return $this->branchRepository->getBranchByDateQrUuid($date_qr_uuid);
    }

    public function firstOrNew($attributes)
    {
        return $this->branchRepository->firstOrNew($attributes);
    }

    public function getBranchInstructors($branch_id)
    {
        $branch = $this->find($branch_id);

        if (! $branch) {
            return false;
        }

        $instructors = $this->instructorRepository->getAllWithBranch($branch->merchantId);

        if (!$instructors->isEmpty()) {
            $instructors = $instructors->filter(function($instructor) use ($branch_id) {
                $search = $instructor->branches->search(function($branch) use ($branch_id) {
                    return $branch->id == $branch_id;
                });

                return $search !== false;
            })->values();
        }

        return $instructors;
    }

    public function getBranchActivities($branch_id)
    {
        $branch = $this->find($branch_id);

        if (!$branch) {
            return false;
        }

        $activities = $this->classRepository->getAllWithBranch($branch->merchantId);

        if (!$activities->isEmpty()) {
            $activities = $activities->filter(function ($activity) use ($branch_id) {
                $search = $activity->branches->search(function ($branch) use ($branch_id) {
                    return $branch->id == $branch_id;
                });

                return $search !== false;
            })->values();
        }

        return $activities;
    }

    public function getBranchEmployee( $branch_id ){

        return $this->branchRepository->getBranchEmployee( $branch_id );

    }
}
