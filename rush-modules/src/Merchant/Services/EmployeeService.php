<?php

namespace Rush\Modules\Merchant\Services;

use Rush\Modules\Merchant\Repositories\EmployeeInterface;
use Validator;

class EmployeeService{

    public $employeeRepository;

    public function __construct( EmployeeInterface $employeeRepository ){

        $this->employeeRepository = $employeeRepository;

    }

    public function getEmployees( $merchant_id ){

       return $this->employeeRepository->getMerchantEmployees( $merchant_id );

    }

    public function createEmployee( $merchant_id, $request ){

        $errors = $this->validateEmployeeDetails( $request );

        if( count($errors) ) return $this->employeeRepository->returnDetailsErrors( $errors );

        else return $this->employeeRepository->createEmployee( $merchant_id, $request );

    }

    public function validateEmployeeDetails( $request ){

        $messages = [
            'pin.regex' => 'The :attribute must be a four-digit numer.'
        ];

        $validator = Validator::make($request->all(), [
            'branch_id' => 'required|numeric',
            'name' => 'required',
            'pin' => 'required|regex:/^\d{4}$/',
            'require_password' => 'required|in:0,1',
        ], $messages);

        return $validator->errors();

    }

    public function getSingleEmployee( $uuid ){

        return $this->employeeRepository->getSingleEmployee( $uuid );

    }

    public function updateEmployee( $uuid, $request ){

        $errors = $this->validateEmployeeDetails( $request );

        if( count($errors) ) return $this->employeeRepository->returnDetailsErrors( $errors );

        else return $this->employeeRepository->updateEmployee( $uuid, $request );

    }

    public function deleteEmployee( $uuid ){

        return $this->employeeRepository->deleteEmployee( $uuid );

    }

    public function getMerchantSystemEmployee($merchantId)
    {
        $username = 'system_employee_'.$merchantId;
        $merchantSystemEmployee = $this->employeeRepository->getMerchantSystemEmployee($merchantId, $username);

        if (!$merchantSystemEmployee) {
            // create the MerchantSystemEmployee
            $merchantSystemEmployee = $this->employeeRepository->fill([
                'username' => $username,
                'employeeName' => $username,
                'merchantId' => $merchantId
            ]);
            $merchantSystemEmployee->save();
        }

        return $merchantSystemEmployee;
    }

    public function getMerchantBranchSystemEmployee($merchantId, $branchId)
    {
        $username = 'system_employee_'.$merchantId.'_branch_'.$branchId;
        $merchantBranchSystemEmployee = $this->employeeRepository->getMerchantSystemEmployee($merchantId, $username);

        if (!$merchantBranchSystemEmployee) {
            // create the MerchantBranchSystemEmployee
            $merchantBranchSystemEmployee = $this->employeeRepository->fill([
                'username' => $username,
                'employeeName' => $username,
                'merchantId' => $merchantId,
                'branchId' => $branchId
            ]);
            $merchantBranchSystemEmployee->save();
        }

        return $merchantBranchSystemEmployee;
    }

    public function firstOrNew($attributes)
    {
        return $this->employeeRepository->firstOrNew($attributes);
    }

    public function findByUuid($uuid)
    {
        return $this->employeeRepository->findByUuid($uuid);
    }

    public function saveDeviceSession($employee_id, $device_id)
    {
        return $this->employeeRepository->saveDeviceSession($employee_id, $device_id);
    }

    public function getDeviceSession($employee_id, $device_id)
    {
        return $this->employeeRepository->getDeviceSession($employee_id, $device_id);
    }

    public function destroyDeviceSession($employee_id, $device_id)
    {
        return $this->employeeRepository->destroyDeviceSession($employee_id, $device_id);
    }
}