<?php

namespace Rush\Modules\Merchant\Services;

use Rush\Modules\Merchant\Repositories\ProductInterface;

class ProductService
{
    protected $product_repository;

    public function __construct(ProductInterface $product_repository)
    {
        $this->product_repository = $product_repository;
    }

    public function getById($product_id){
        return $this->product_repository->getById($product_id);
    }

    public function getAll($merchant_id = null)
    {
        if($merchant_id == null){
            return $this->product_repository->all();
        }

        return $this->product_repository->getAll($merchant_id);
    }

    public function store($product_data){
        return $this->product_repository->store($product_data);
    }

    public function update($product_id, $product_data){
        return $this->product_repository->updateProduct($product_id, $product_data);
    }

    public function getAllProductTransaction($merchant_id)
    {
        return $this->product_repository->getAllProductTransaction($merchant_id);
    }

    public function delete($id){
        return $this->product_repository->delete($id);
    }

    /**
     * @param integer|string $stamp_id
     * @return \Rush\Modules\Merchant\Models\ProductStampModel|null
     */
    public function getProductStamp($stamp_id)
    {
        return $this->product_repository->getProductStamp($stamp_id);
    }
}