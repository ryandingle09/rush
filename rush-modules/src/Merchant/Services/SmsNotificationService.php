<?php

namespace Rush\Modules\Merchant\Services;

use Rush\Modules\Merchant\Repositories\SmsNotificationInterface;
use Rush\Modules\Merchant\Repositories\SmsNotificationRepository;

class SmsNotificationService
{
    protected $notificationRepository;

    public function __construct(SmsNotificationInterface $notificationRepository)
    {
        $this->notificationRepository = $notificationRepository;
    }

    public function getMessage($type, $merchantId = null)
    {
        return $this->notificationRepository->getMessage($type, $merchantId);
    }
}
