<?php

namespace Rush\Modules\Merchant\Services;

use Event;
use Illuminate\Support\Facades\Input;
use Rush\Modules\Alert\Events\ChangeAppstoreLogoEvent;
use Rush\Modules\Helpers\Facades\StorageHelper;
use Rush\Modules\Merchant\Repositories\MerchantRepository;
use Rush\Modules\Merchant\Repositories\MerchantSettingInterface;

/**
* Our MerchantSettingService, containing all useful methods for business logic around MerchantSetting
* Service just hands off data it was given by the controller to the appropriate repositories
*/
class MerchantSettingService
{
    /**
     * @var Rush\Modules\Merchant\Repositories\MerchantSettingRepository
     */
    protected $merchantSettingRepository;

    /**
     * @var Rush\Modules\Merchant\Repositories\MerchantRepository
     */
    protected $merchantRepository;

    public function __construct(
        MerchantSettingInterface $merchantSettingRepository,
        MerchantRepository $merchantRepository
    ) {
        $this->merchantSettingRepository = $merchantSettingRepository;
        $this->merchantRepository = $merchantRepository;
    }

    /**
     * @param $merchantId
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function getByMerchantId($merchantId)
    {
        return $this->merchantSettingRepository->getByMerchantId($merchantId);
    }

    public function store(array $input)
    {
        $settingInput = $input['input'];
        $merchantSetting = $this->merchantSettingRepository->getByMerchantId($settingInput['merchant_id']);

        $validator = $this->validate(
            $settingInput,
            [   'program_name' => 'required',
                'points_name' => 'required',
                'app_name' => 'required',
                'app_description' => 'required',
                'logo' => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:width=1024,height=1024'],
            []
        );

        if ($validator->fails()) {
            return $validator;
        }

        if (Input::hasFile('logo')) {
            $settingInput['app_logo'] = StorageHelper::move('merchant/logo', 'logo');
            StorageHelper::deleteFromRepository($merchantSetting->app_logo);
        }
        $this->merchantSettingRepository->update($merchantSetting->id, $settingInput);

        if (isset($settingInput['app_logo'])) {
            $merchant = $this->merchantRepository->getById($settingInput['merchant_id']);
            Event::fire(new ChangeAppstoreLogoEvent($merchant));
        }

        return true;
    }

    public function storeCapping(array $input)
    {
        $settingInput = $input['input'];
        $merchantSetting = $this->merchantSettingRepository->getByMerchantId($settingInput['merchant_id']);

        $validator = $this->validate(
            $settingInput,
            [   'points_transfer' => 'required|boolean',
                'earn_transaction_limit' => 'required|numeric',
                'earn_points_limit' => 'required|numeric',
                'redemption_transaction_limit' => 'required|numeric',
                'paypoint_transaction_limit' => 'required|numeric',
                'paypoint_points_limit' => 'required|numeric'],
            []
        );
        if ($validator->fails()) {
            return $validator;
        }

        $this->merchantSettingRepository->update($merchantSetting->id, $settingInput);

        return true;
    }

    private function validate($input, $rules, $messages = [])
    {
        return \Validator::make($input, $rules, $messages);
    }
}
