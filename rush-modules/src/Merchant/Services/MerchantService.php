<?php
namespace Rush\Modules\Merchant\Services;

use Rush\Modules\Merchant\Repositories\MerchantInterface;
use Rush\Modules\Merchant\Repositories\MerchantRepository;
use Rush\Modules\Customer\Repositories\CustomerInterface;
use Rush\Modules\Customer\Repositories\CustomerRepository;
use Rush\Modules\Merchant\Repositories\BranchRepository;
use Rush\Modules\Merchant\Repositories\BranchInterface;
use Rush\Modules\Merchant\Repositories\MembershipBranchInterface;
use Rush\Modules\Merchant\Repositories\MembershipBranchRepository;
use Rush\Modules\Merchant\Repositories\EmployeeRepository;
use Rush\Modules\Merchant\Repositories\SmsNotificationInterface;
use Rush\Modules\Merchant\Repositories\SmsNotificationRepository;
use Rush\Modules\Admin\Repositories\ModulesMerchantInterface;
use Rush\Modules\Admin\Repositories\ModulesMerchantRepository;
use Rush\Modules\Broadcasting\Repositories\SegmentInterface;
use Rush\Modules\Customer\Repositories\CustomerPunchCardTransactionsRepository;
use Rush\Modules\Customer\Repositories\CustomerTransactionRepository;
use Rush\Modules\Merchant\Services\NotificationService;

use Validator;
use Carbon\Carbon;
use Ixudra\Curl\CurlService;
use Endroid\QrCode\QrCode;
use Intervention\Image\ImageManager;
use Rush\Modules\Helpers\Facades\StorageHelper;

/**
 * Class MerchantService
 * @package Rush\Modules\Merchant\Services
 */
class MerchantService
{
    /**
     * @var MerchantRepository
     */
    protected $merchantRepository;

    protected $customerRepository;

    protected $branchRepository;

    protected $membershipBranchRepository;

    protected $employeeRepository;

    protected $smsNotificationRepository;

    protected $modulesMerchantRepository;

    protected $segmentRepository;

    protected $customerPunchcardTransactionRepository;

    protected $customerTransactionRepository;

    protected $notification_service;

    /**
     * MerchantService constructor.
     * @param MerchantInterface $merchantRepository
     */
    public function __construct(
        MerchantInterface $merchantRepository,
        CustomerInterface $customerRepository,
        BranchInterface $branchRepository,
        MembershipBranchInterface $membershipBranchRepository,
        EmployeeRepository $employeeRepository,
        SmsNotificationInterface $smsNotificationRepository,
        ModulesMerchantInterface $modulesMerchantRepository,
        SegmentInterface $segmentRepository,
        CustomerPunchCardTransactionsRepository $customerPunchcardTransactionRepository,
        CustomerTransactionRepository $customerTransactionRepository,
        NotificationService $notification_service
    ) {
        $this->merchantRepository = $merchantRepository;
        $this->customerRepository = $customerRepository;
        $this->branchRepository = $branchRepository;
        $this->membershipBranchRepository = $membershipBranchRepository;
        $this->employeeRepository = $employeeRepository;
        $this->smsNotificationRepository = $smsNotificationRepository;
        $this->modulesMerchantRepository = $modulesMerchantRepository;
        $this->segmentRepository = $segmentRepository;
        $this->customerPunchcardTransactionRepository = $customerPunchcardTransactionRepository;
        $this->customerTransactionRepository = $customerTransactionRepository;
        $this->notification_service = $notification_service;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model[]
     */
    public function all()
    {
        return $this->merchantRepository->all();
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|\Rush\Modules\Merchant\Models\MerchantModel
     */
    public function getById($id)
    {
        return $this->merchantRepository->getById($id);
    }

    /**
     * @param string $slug
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function getBySlug($slug)
    {
        return $this->merchantRepository->getBySlug($slug);
    }

    public function getBasicDetails( $id ){

        return $this->merchantRepository->getBasicDetails( $id );

    }

    public function updateBasicDetails( $id, $request ){

        // validate inputs.
        $errors = $this->validateMerchantDetails( $request );

        // return formatted validation errors.
        if( count($errors) ) return $this->merchantRepository->returnBasicDetailsErrors( $errors );

        // update merchant data and return updated data.
        else return $this->merchantRepository->updateBasicDetails( $id, $request );

    }

    public function validateMerchantDetails( $request ){

        $validator = Validator::make($request->all(), [
            'business_name' => 'required',
            'address' => 'required',
            'person_name' => 'required',
            'person_position' => 'required',
            'contact_person_email' => 'required|email',
            'contact_person_number' => 'required',
        ]);

        return $validator->errors();

    }

    /*
     * @param $username
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function getByUsername($username)
    {
        return $this->merchantRepository->getByUsername($username);
    }

    public function getMerchantPackage( $merchant_id, $type )
    {
        return $this->merchantRepository->getMerchantPackage($merchant_id, $type);
    }

    public function updateMerchantPackage( $merchant_id, $data, $type )
    {
        return $this->merchantRepository->updateMerchantPackage($merchant_id, $data, $type);
    }

    public function generateLoyaltyCardOrder($id, $data)
    {
      return $this->merchantRepository->generateLoyaltyCardOrder($id,$data);
    }

    public function getPointsConversionObject($merchant_id, $branch_id = null, $customer_id = null)
    {
        $membership_branch =  null;
        $branch = $branch_id ? $this->branchRepository->getById($branch_id) : null;
        $customer = $customer_id ? $this->customerRepository->getById($customer_id) : null;

        if(isset($customer->membership_card)){
            $membership_branch = $this->membershipBranchRepository->getByMembershipBranch($customer->membership_card->membership->id, $branch_id);
        }


        $conversion_setting = $this->merchantRepository->getPointsConversion( $merchant_id, $branch, $customer, $membership_branch );

        return $conversion_setting;
    }

    public function getPointsConversion( $merchant_id, $branch_id = null, $customer_id = null )
    {
        $branch = $branch_id ? $this->branchRepository->getById($branch_id) : null;
        $customer = $customer_id ? $this->customerRepository->getById($customer_id) : null;
        $membership_branch =  null;

        if(isset($customer->membership_card)){
            $membership_branch = $this->membershipBranchRepository->getByMembershipBranch($customer->membership_card->membership->id, $branch_id);
        }

        $conversion_setting = $this->merchantRepository->getPointsConversion( $merchant_id, $branch, $customer, $membership_branch );

        $data = [
            'redemption_points' => (float) $conversion_setting->redemption_points,
            'redemption_peso' => (float) $conversion_setting->redemption_peso,
            'earning_points' => (float) $conversion_setting->earning_points,
            'earning_peso' => (float) $conversion_setting->earning_peso,
        ];

        $response['error_code'] = '0x0';
        $response['data'] = $data;

        return $response;
    }

    public function getCatalog( $merchant_id, $customer_uuid = null ){

        return $this->merchantRepository->getCatalog( $merchant_id, $customer_uuid );

    }

    public function getGenericCatalog( $merchant_id, $customer_uuid = null ){

        $customer = $this->customerRepository->getCustomerByUuid( $customer_uuid );
        if( ! is_null($customer_uuid) && ! $customer ) return ['error_code' => '0x1', 'message' => 'Customer not found.'];

        $data = $this->merchantRepository->getGenericCatalog( $merchant_id, $customer );
        return ['error_code' => '0x0', 'data' => $data];

    }

    public function getPointsTransferSettings( $merchant_id ){

        return $this->merchantRepository->getPointsTransferSettings( $merchant_id );

    }

    public function getPosSyncSettings( $merchant_id ){

        return $this->merchantRepository->getPosSyncSettings( $merchant_id );

    }

    public function autoUpdateMerchantStatus()
    {
        foreach( $this->all() as $merchant )
        {
          if ( Carbon::parse( $merchant->trial_end ) < date('Y-m-d H:i:s') && $merchant->auto_update_status == 1 ) {
            $this->merchantRepository->changeMerchantStatus( $merchant->merchantid );
          }
        }
    }

    public function saveMerchantRewards( $merchant_id, $data )
    {
      return $this->merchantRepository->saveMerchantRewards( $merchant_id, $data );
    }

    public function deleteItemForRedeem( $merchant_id, $item_id )
    {
      return $this->merchantRepository->deleteItemForRedeem( $merchant_id, $item_id );
    }

    public function getMerchantTransactions( $merchant_id, $bcu_id = null )
    {
        return $this->merchantRepository->getMerchantTransactions( $merchant_id, $bcu_id );
    }

    public function countAllTransactions( $request )
    {
        return $this->merchantRepository->countAllTransactions( $request );
    }

    public function countFilterTransactions( $request )
    {
        return $this->merchantRepository->countFilterTransactions( $request );
    }

    public function getMerchantTransactionsDaTaTable( $request )
    {
        return $this->merchantRepository->getMerchantTransactionsDaTaTable( $request );
    }

    public function toggleAdminRewards( $merchant_id, $reward_id, $enable )
    {
      return $this->merchantRepository->toggleAdminRewards( $merchant_id, $reward_id, $enable );
    }

    public function addBranch($branchInput)
    {
        $validator = $this->validate($branchInput,
            [   'branchName' => 'required|uniqueBranchName:'.$branchInput['merchantId'],
                'street_name' => 'required',
                'district' => 'required',
                'city' => 'required',
                'zipcode' => 'required'],
            [   'branchName.uniqueBranchName' => 'The :attribute has already been registered.']
        );
        if ($validator->fails()) {
            return $validator;
        }
        $this->branchRepository->addBranch($branchInput);

        return true;
    }

    public function getBranchLogo( $id )
    {
      return $this->branchRepository->getBranchLogo( $id );
    }

    public function updateBranch($merchantId, $id, $branchInput)
    {
        $validator = $this->validate($branchInput,
            [   'branchName' => 'required|uniqueBranchName:'.$merchantId.','.$branchInput['id']],
            [   'branchName.uniqueBranchName' => 'The :attribute has already been registered.']
        );
        if ($validator->fails()) {
            return $validator;
        }
        $this->branchRepository->update_branch($merchantId, $id, $branchInput);

        return true;
    }

    public function deleteBranch($merchantId,$id)
    {
      return $this->branchRepository->delete_branch($merchantId,$id);
    }

    public function addEmployee( $data )
    {
      return $this->employeeRepository->addEmployee( $data );
    }

    public function updateEmployee($merchantId,$employeeId,$employee)
    {
      return $this->employeeRepository->update_employee($merchantId,$employeeId,$employee);
    }

    public function deleteEmployee($merchantId,$id)
    {
      return $this->employeeRepository->delete_employee($merchantId,$id);
    }

    public function getBCUCustomerIds($branch_id){
        $customer_ids = null;

        if(!empty($branch_id)){

            if($package_id == 1){
                $customer_ids = $this->customerTransactionRepository->getByBranchId($branch_id)->pluck('customerId')->toArray();
            } else {
                $customer_ids = $this->customerPunchcardTransactionRepository->getByBranchId($branch_id)->pluck('customer_id')->toArray();
            }


            if(empty($customer_ids)){
                $customer_ids = [0];
            }
        }
    }

    public function getPromoStampsSummary($merchant_id, $punchcard_id = null, $branch_id = null)
    {
        $customer_ids = null;

        if(!empty($branch_id)){
            $customer_ids = $this->customerTransactionRepository->getByBranchId($branch_id)->pluck('customerId')->toArray();

            if(empty($customer_ids)){
                $customer_ids = [0];
            }
        }

      return $this->merchantRepository->getPromoStampsSummary($merchant_id, $punchcard_id, $customer_ids);
    }

    public function getPromoStampsSummaryDT($request, $branch_id = null, $package_id)
    {
        $customer_ids = $this->getBCUCustomerIds($branch_id);

        $stamp_summary_query = $this->merchantRepository->buildPromoStampsSummaryQuery($request, $customer_ids);

        $response_data['data'] = $this->merchantRepository->getPromoStampSummaryData($stamp_summary_query, $request);
        $response_data['draw'] = $request->draw;
        $response_data['recordsFiltered'] = $this->merchantRepository->countFilterPromoStampSummaryData($request);
        $response_data['recordsTotal'] = $this->merchantRepository->countAllPromoStampSummaryData($request);

        return $response_data;
    }

    public function getPackageStampsSummaryDT($request, $branch_id = null, $package_id)
    {
        $customer_ids = $this->getBCUCustomerIds($branch_id);

        $package_summary_query = $this->merchantRepository->builPackageStampsSummaryQuery($request, $customer_ids, $package_id);

        $response_data['data'] = $this->merchantRepository->getPackageStampsSummaryData($package_summary_query, $request);
        $response_data['draw'] = $request->draw;
        $response_data['recordsFiltered'] = $this->merchantRepository->countFilterPackageStampsSummaryData($package_summary_query);
        $response_data['recordsTotal'] = $this->merchantRepository->countAllPackageStampsSummaryData($request, $package_id);

        return $response_data;
    }

    public function expireCustomerClassPackage($customer_package_id)
    {
        return $this->merchantRepository->expireCustomerClassPackage($customer_package_id);
    }

    public function getStamps($merchant_id)
    {
      return $this->merchantRepository->getStamps($merchant_id);
    }

    public function getSmsNotificationMessages($merchant_id, $type = null)
    {
        if ($merchant_id === 0) {
            return $this->smsNotificationRepository->getDefaultMessage($type);
        }
        return $this->merchantRepository->getSmsNotificationMessages($merchant_id, $type);
    }

    public function getCheckIn( $merchant_id, $branch_id)
    {
        return $this->merchantRepository->getCheckIn( $merchant_id, $branch_id );
    }

    public function updateMerchantPassword( $merchant_id, $password )
    {
        return $this->merchantRepository->updateMerchantPassword( $merchant_id, $password );
    }

    public function getModules( $package_id )
    {
        return $this->merchantRepository->getModules( $package_id );
    }

    public function saveUser( $data, $merchant_id, $package_id )
    {
        return $this->merchantRepository->saveUser( $data, $merchant_id, $package_id );
    }

    public function disableUser( $id )
    {
        return $this->merchantRepository->disableUser( $id );
    }

    public function getSubUser( $id )
    {
        return $this->merchantRepository->getSubUser( $id );
    }

    public function updateMerchantUserPassword( $user_id, $password )
    {
        return $this->merchantRepository->updateMerchantUserPassword( $user_id, $password );
    }

    public function getUserModules( $id )
    {
        return $this->merchantRepository->getUserModules( $id );
    }

    public function getUserModulesRoute( $id, $array = null)
    {
        $modules = $this->merchantRepository->getUserModulesRoute( $id );
        if ( !$array )
        {
            return $modules;
        }
        else
        {
            if( count($modules) > 0 ) {
                $modulesArray = array('home');
                foreach( $modules as $module )
                {
                    $modulesArray[] = $module->route_name;
                }
                return $modulesArray;
            }
            else
            {
                return null;
            }
        }
    }

    public function getSavedPromoMechanics( $merchant_id , $package_id )
    {
        return $this->merchantRepository->getSavedPromoMechanics( $merchant_id , $package_id );
    }

    public function isPromoDurationValid($merchantId, $durationStart, $durationEnd, $promoId = null)
    {
        return $this->merchantRepository->isPromoDurationValid($merchantId, $durationStart, $durationEnd, $promoId);
    }

    public function getPromoMechanics( $id )
    {
        return $this->merchantRepository->getPromoMechanics( $id );
    }

    public function savePromoMechanics( $data, $rewards )
    {
        return $this->merchantRepository->savePromoMechanics( $data, $rewards );
    }

    public function deletePromoMechanics( $merchant_id, $id )
    {
        return $this->merchantRepository->deletePromoMechanics( $merchant_id, $id );
    }

    public function deleteExpiredPromoMechanics( $merchant_id, $id )
    {
        return $this->merchantRepository->deleteExpiredPromoMechanics( $merchant_id, $id );
    }

    public function updatePromoMechanics( $merchantId, $promoId, $status, $promoTitle, $durationStart, $durationEnd, $redemptionStart, $redemptionEnd, $amount, $numStamps )
    {
        return $this->merchantRepository->updatePromoMechanics( $merchantId, $promoId, $status, $promoTitle, $durationStart, $durationEnd, $redemptionStart, $redemptionEnd, $amount, $numStamps );
    }

    public function getPromoReward($promoId, $stampNo)
    {
        return $this->merchantRepository->getPromoReward($promoId, $stampNo);
    }

    public function updatePromoReward($promoId, $stampNo, $description, $imageUrl, $details, $branch)
    {
        return $this->merchantRepository->updatePromoReward($promoId, $stampNo, $description, $imageUrl, $details, $branch);
    }

    public function addPromoReward($promoId, $stampNo, $description, $imageUrl, $merchantId, $details, $branch)
    {
        return $this->merchantRepository->addPromoReward($promoId, $stampNo, $description, $imageUrl, $merchantId, $details, $branch);
    }

    public function removePromoReward($promoId, $stampNo)
    {
        return $this->merchantRepository->removePromoReward($promoId, $stampNo);
    }

    public function enableUser( $id )
    {
        return $this->merchantRepository->enableUser( $id );
    }

    public function login( $email, $password )
    {
        return $this->merchantRepository->login( $email, $password );
    }

    public function subuserlogin( $email, $password )
    {
        return $this->merchantRepository->subuserlogin( $email, $password );
    }

    public function registerMerchant( $data )
    {
        return $this->merchantRepository->registerMerchant( $data );
    }

    public function checkEmail( $email )
    {
        return $this->merchantRepository->checkEmail( $email );
    }

    public function forgotpassword( $email, $password, $type = 'merchant' )
    {
        return $this->merchantRepository->forgotpassword( $email, $password, $type );
    }

    public function createQuicksetupProgressTrack( $merchant_id, $package_id, $steps )
    {
        return $this->merchantRepository->createQuicksetupProgressTrack( $merchant_id, $package_id, $steps );
    }

    public function clearFirstLoginFlag( $merchant_id )
    {
        return $this->merchantRepository->clearFirstLoginFlag( $merchant_id );
    }

    public function getCompletionState( $merchant_id )
    {
        return $this->merchantRepository->getCompletionState( $merchant_id );
    }

    public function getSavedProgramSettings( $merchant_id )
    {
        return $this->merchantRepository->getSavedProgramSettings( $merchant_id );
    }

    public function getSavedApplicationDetails( $merchant_id )
    {
        return $this->merchantRepository->getSavedApplicationDetails( $merchant_id );
    }

    public function getSavedMerchantApplication( $merchant_id )
    {
        return $this->merchantRepository->getSavedMerchantApplication( $merchant_id );
    }

    public function getSavedCustomerAppDesign( $merchant_id )
    {
        return $this->merchantRepository->getSavedCustomerAppDesign( $merchant_id );
    }

    public function saveProgramSettings( $merchant_id, $programName, $pointsName )
    {
        return $this->merchantRepository->saveProgramSettings( $merchant_id, $programName, $pointsName );
    }

    public function trackProgress( $merchant_id, $progress )
    {
        return $this->merchantRepository->trackProgress( $merchant_id, $progress );
    }

    public function saveApplicationDetails( $merchant_id , $appName, $appDescription, $appLogo )
    {
        return $this->merchantRepository->saveApplicationDetails( $merchant_id , $appName, $appDescription, $appLogo );
    }

    public function clearQuicksetupLaunchFlag( $merchant_id )
    {
        return $this->merchantRepository->clearQuicksetupLaunchFlag( $merchant_id );
    }

    public function populateMindbodyRepoTables($data)
    {
        return $this->merchantRepository->populateMindbodyRepoTables($data);
    }

    /**
     * @param integer $merchant_id
     * @param array $args
     * @return bool|\Rush\Modules\Merchant\Models\MerchantPunchCardMilestoneModel[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getPunchcardMilestone($merchant_id, array $args)
    {
        $merchant = $this->getById($merchant_id);
        if (!$merchant)  {
            return false;
        }
        return $this->merchantRepository->getPunchcardMilestone($merchant, $args);
    }

    public function getModulesByMerchant($merchant_id)
    {
        return $this->modulesMerchantRepository->getModulesByMerchant( $merchant_id );
    }

    public function getMerchantModulesRoute( $id, $array = null)
    {
        $modules = $this->modulesMerchantRepository->getMerchantModulesRoute( $id );
        if ( !$array )
        {
            return $modules;
        }
        else
        {
            $modulesArray = array('home');
            if( count($modules) > 0 ) {
                foreach( $modules as $module )
                {
                    $modulesArray[] = $module->route_name;
                }

            }
            return $modulesArray;
        }
    }

    public function getMerchantModules( $id )
    {
        return $this->modulesMerchantRepository->getMerchantModules( $id );
    }

    public function bculogin( $id, $pin )
    {
        return $this->merchantRepository->bculogin( $id, $pin );
    }

    public function getModuleByCode( $code, $package_id )
    {
        return $this->merchantRepository->getModuleByCode( $code, $package_id );
    }

    private function validate($input, $rules, $messages = [])
    {
        return \Validator::make($input, $rules, $messages);
    }

    public function memberTierStop( $id )
    {
        return $this->merchantRepository->memberTierStop( $id );
    }

    public function validateCustomerTierCount($merchant)
    {
        return $this->merchantRepository->validateCustomerTierCount($merchant);
    }

    public function qr_code_generate( $merchant_id, $branch_ids = "[0]", $name = "", $points = 0, $load = -1, $customer_scan_limit = -1 ){

        $qr_code_model = $this->merchantRepository->insert_qr_code( $merchant_id, $branch_ids, $name, $points, $load, $customer_scan_limit );


        // generate qr code.
        $qr_code_uuid = $qr_code_model->uuid;
        $qr_code = new QrCode( $qr_code_uuid );
        $qr_code->setSize(300);


        // generate image of the qr code.
        $image_manager = new ImageManager(array('driver' => 'gd'));
        $image = $image_manager->make( $qr_code->getImage() )->stream();

        // save qr code image in the file system.
        $disk = StorageHelper::getRepositoryDisk();
        $file_path = 'merchant/qr_codes/'. $qr_code_uuid . '.jpg';

        if( ! $disk->exists($file_path) ){
            $disk->put( $file_path, $image->__toString(), 'public' );
        }


        return $qr_code_model;

    }

    public function validate_merchant_branch_ids( $merchant_id, $branch_ids_arr ){

        // remove the branch id 0 in $branch_ids_arr because it doesn't exist in db.
        $zero_branch_key = array_search(0, $branch_ids_arr);
        unset( $branch_ids_arr[$zero_branch_key] );

        $branches = $this->merchantRepository->get_merchant_branches_by_id( $merchant_id, $branch_ids_arr );
        return ( count($branch_ids_arr) == count($branches) ? true : false );

    }

    public function get_qr_code_by_uuid( $qr_code_uuid ){

        return $this->merchantRepository->get_qr_code_by_uuid( $qr_code_uuid );

    }

    public function getMerchantBcuModules( $id )
    {
        return $this->merchantRepository->getMerchantBcuModules( $id );
    }

    public function saveBcuOptions( $modules, $functions, $id )
    {
        return $this->merchantRepository->saveBcuOptions( $modules, $functions, $id );
    }

        public function getMerchantBcuModulesRoute( $id, $array = null)
    {
        $modules = $this->merchantRepository->getMerchantBcuModulesRoute($id);
        if (!$array) {
            return $modules;
        } else {
            if (count($modules) > 0) {
                $modulesArray = array('home');
                foreach ($modules as $module) {
                    $modulesArray[] = $module->route_name;
                }
                return $modulesArray;
            }
            else {
                return null;
            }
        }
    }

    public function getIssuedVoucherTransactions($employee, $args)
    {
        return $this->merchantRepository->getIssuedVoucherTransactions($employee, $args);
    }

    public function getVouchers( $branches )
    {
        return $this->merchantRepository->getVouchers( $branches );
    }

    public function addVoucher( $request )
    {
        return $this->merchantRepository->addVoucher( $request );
    }

    public function editVoucher( $request )
    {
        return $this->merchantRepository->editVoucher( $request );
    }

    public function deleteVoucher( $id )
    {
        return $this->merchantRepository->deleteVoucher( $id );
    }

    public function addClientCode( $merchant_id, $code )
    {
        return $this->merchantRepository->addClientCode( $merchant_id, $code );
    }

    public function editClientCode( $merchant_id, $data )
    {
        return $this->merchantRepository->editClientCode( $merchant_id, $data );
    }

    public function importClientCode( $data )
    {
        return $this->merchantRepository->importClientCode( $data );
    }

    public function getSegmentByScopeString($merchant_id, $string){
        return $this->segmentRepository->getSegmentByScopeString($merchant_id, $string);
    }

    public function get_raw_menu_items( $merchant_id ){

        return $this->merchantRepository->get_raw_menu_items( $merchant_id );

    }

    public function get_raw_menu_item_categories( $merchant_id ){

        return $this->merchantRepository->get_raw_menu_item_categories( $merchant_id );

    }

    public function get_categorized_menu_items( $raw_menu_item_categories, $raw_menu_items ){

        $grouped_menu_items = [];
        $grouped_sub_categories = [];
        $categorized_menu_items = [];


        // group items per category for quick retrieval.
        if( count($raw_menu_items) ){
            foreach( $raw_menu_items as $item ){

                $grouped_menu_items[ $item->category_id ][] = $item;

            }
        }


        // group sub categories per parent category for quick retrieval.
        if( count($raw_menu_item_categories) ){
            foreach( $raw_menu_item_categories as $category ){
                if( $category->parent_category_id != 0 ){

                    $grouped_sub_categories[ $category->parent_category_id ][] = $category;

                }
            }
        }


        // loop through raw categories.
        // if the category has no sub categories, get its items.
        // if the category has sub categories, get its sub categories,
        // then, the sub categories' items will be retrieved.
        if( count($raw_menu_item_categories) ){
            foreach( $raw_menu_item_categories as $category ){
                if( $category->parent_category_id == 0 ){

                    $categorized_menu_items[] = $this->get_sub_categories_or_items( $grouped_menu_items, $grouped_sub_categories, $category );

                }
            }
        }

        return $categorized_menu_items;

    }

    private function get_sub_categories_or_items( $grouped_menu_items, $grouped_sub_categories, $category ){

        $category_id = $category->id;
        $parent_category_id = $category->parent_category_id;


        // if the category has sub categories, return the sub categories.
        // then, the sub categories' items will be retrieved.
        if( array_key_exists($category_id, $grouped_sub_categories) ){

            $sub_categories = [];

            foreach( $grouped_sub_categories[ $category_id ] as $sub_category ){

                $sub_categories[] = $this->get_sub_categories_or_items( $grouped_menu_items, $grouped_sub_categories, $sub_category );

            }

            return [
                'category_name' => (string) $category->name,
                'category_note' => (string) $category->note,
                'image_url' => ( $category->image_url ? StorageHelper::repositoryUrlForView($category->image_url) : '' ),
                'sub_categories' => $sub_categories,
                'items' => [],
            ];


        // if the category has NO sub categories, return the items of the category.
        // including the category details.
        }else{

            $items = [];

            if( isset($grouped_menu_items[ $category_id ]) ){
                foreach( $grouped_menu_items[ $category_id ] as $item ){

                    $items[] = [
                        'uuid' => (string) $item->uuid,
                        'name' => (string) $item->name,
                        'description' => (string) $item->description,
                        'amount' => (float) $item->amount,
                        'image_url' => ( $item->image_url ? StorageHelper::repositoryUrlForView($item->image_url) : '' ),
                    ];

                }
            }

            return [
                'category_name' => (string) $category->name,
                'category_note' => (string) $category->note,
                'image_url' => ( $category->image_url ? StorageHelper::repositoryUrlForView($category->image_url) : '' ),
                'sub_categories' => [],
                'items' => $items,
            ];

        }

    }

    public function get_customer_orders_by_date_time_range( $merchant_id, $start_date_time, $end_date_time, $status = null ){

        return $this->merchantRepository->get_customer_orders_by_date_time_range( $merchant_id, $start_date_time, $end_date_time, $status );

    }

    public function group_orders_by_status( $raw_customer_orders ){

        $group_orders = [];

        if( count($raw_customer_orders) ){
            foreach( $raw_customer_orders as $order ){

                $status = str_replace(' ', '_', strtolower($order->status));

                $group_orders[ $status ][] = [
                    'uuid' => (string) $order->uuid,
                    'order_number' => (string) $order->order_number,
                    'date' => $order->created_at->format('Y-m-d H:i:s'),
                    'delivery_name' => (string) $order->delivery_name,
                    'status' => (string) $order->status,
                ];

            }
        }

        return $group_orders;

    }

    public function get_formatted_customer_orders( $raw_customer_orders ){

        $customer_orders = [];


        if( count($raw_customer_orders) ){
            foreach( $raw_customer_orders as $order ){

                $items = [];

                foreach( $order->order_items as $order_item ){

                    $items[] = [
                        'name' => (string) $order_item->item->name,
                        'amount' => (float) $order_item->amount,
                        'quantity' => (int) $order_item->quantity,
                    ];

                }

                $customer_orders[] = [
                    'uuid' => (string) $order->uuid,
                    'order_number' => (string) $order->order_number,
                    'status' => (string) $order->status,
                    'total_amount' => (float) $order->total_amount,
                    'promo_code' => (string) $order->promo_code,
                    'promo_code_discount_amount' => (float) $order->promo_code_discount_amount,
                    'net_amount' => (float) $order->net_amount,
                    'date' => $order->created_at->format('Y-m-d H:i:s'),

                    'delivery_name' => (string) $order->delivery_name,
                    'delivery_mobile_number' => (string) $order->delivery_mobile_number,
                    'delivery_email' => (string) $order->delivery_email,
                    'delivery_building' => (string) $order->delivery_building,
                    'delivery_floor' => (string) $order->delivery_floor,
                    'delivery_street' => (string) $order->delivery_street,
                    'delivery_landmark' => (string) $order->delivery_landmark,

                    'payment_name' => (string) $order->payment_name,
                    'payment_mobile_number' => (string) $order->payment_mobile_number,
                    'payment_change_for' => (string) $order->payment_change_for,
                    'payment_notes' => (string) $order->payment_notes,

                    'items' => $items,
                ];

            }
        }

        return $customer_orders;

    }

    public function get_formatted_order_details( $order ){

        $items = [];

        foreach( $order->order_items as $order_item ){

            $items[] = [
                'name' => (string) $order_item->item->name,
                'amount' => (float) $order_item->amount,
                'quantity' => (int) $order_item->quantity,
            ];

        }

        $order_details = [
            'uuid' => (string) $order->uuid,
            'order_number' => (string) $order->order_number,
            'status' => (string) $order->status,
            'total_amount' => (float) $order->total_amount,
            'promo_code' => (string) $order->promo_code,
            'promo_code_discount_amount' => (float) $order->promo_code_discount_amount,
            'net_amount' => (float) $order->net_amount,
            'date' => $order->created_at->format('Y-m-d H:i:s'),

            'delivery_name' => (string) $order->delivery_name,
            'delivery_mobile_number' => (string) $order->delivery_mobile_number,
            'delivery_email' => (string) $order->delivery_email,
            'delivery_building' => (string) $order->delivery_building,
            'delivery_floor' => (string) $order->delivery_floor,
            'delivery_street' => (string) $order->delivery_street,
            'delivery_landmark' => (string) $order->delivery_landmark,

            'payment_name' => (string) $order->payment_name,
            'payment_mobile_number' => (string) $order->payment_mobile_number,
            'payment_change_for' => (string) $order->payment_change_for,
            'payment_notes' => (string) $order->payment_notes,

            'items' => $items,
        ];

        return $order_details;

    }

    public function get_status_updates_by_date_time_range( $merchant_id, $start_date_time, $end_date_time ){

        return $this->merchantRepository->get_status_updates_by_date_time_range( $merchant_id, $start_date_time, $end_date_time );

    }

    public function get_order_by_uuid( $order_uuid ){

        return $this->merchantRepository->get_order_by_uuid( $order_uuid );

    }

    public function order_status_update( $order, $status, $channel, $user_id ){

        if( ! in_array($status, $order->merchant->order_statuses) ) return false;

        try{

            $order = \DB::transaction(function() use($order, $status, $channel, $user_id){

                $this->merchantRepository->order_status_update( $order, $status );
                $this->customerRepository->save_order_status_history( $order->id, $status, $channel, $user_id );

                return $order;

            });

        }catch( \Exception $e ){

            return false;

        }

        return $order;

    }

    public function send_order_status_update_notification( $order ){

        $customer = $order->customer;
        $merchant = $customer->merchant;
        $status = $order->status;

        if( $status == 'Processing' ) $flag = 'order-processing';
        elseif( $status == 'On the way' ) $flag = 'order-on-the-way';
        elseif( $status == 'Delivered' ) $flag = 'order-delivered';
        else $flag = "non-existent-flag. order_status: {$status}";  // this will be logged.

        $data['subject_business_name'] = $merchant->businessName;
        $data['subject_order_number'] = $order->order_number;
        $data['logo_url'] = \Config::get('app.url') . $merchant->settings->app_logo;
        $data['customer_name'] = $customer->fullName;
        $data['business_name'] = $merchant->businessName;
        $data['order_number'] = $order->order_number;
        $data['business_email'] = $merchant->contactPersonEmail;
        $data['business_contact'] = $merchant->contactPersonNumber;

        $this->notification_service->send_notification( $merchant, $customer, $flag, $data );

    }

    public function get_promo_code( $merchant_id, $promo_code ){

        return $this->merchantRepository->get_promo_code( $merchant_id, $promo_code );

    }

    public function send_voucher_threshold_met_notification( $voucher ){

        $merchant = $voucher->merchant;
        $flag = 'voucher-threshold-met';

        // hack to send the email to merchant.
        $customer = new \stdClass;
        $customer->email = $merchant->contactPersonEmail;
        $customer->fullName = $merchant->contactPerson;

        $data['voucher_name'] = $voucher->name;
        $data['branch_name'] = $voucher->branch->branchName;

        $this->notification_service->send_notification( $merchant, $customer, $flag, $data );

    }

    public function getPointsSeedingBatchByMerchant( $merchant_id )
    {
        return $this->merchantRepository->getPointsSeedingBatchByMerchant( $data );
    }

    public function addPointsSeedingBatch( $data )
    {
        return $this->merchantRepository->addPointsSeedingBatch( $data );
    }

    public function getBranchIdByName( $name, $merchant_id )
    {
        return $this->merchantRepository->getBranchIdByName( $name, $merchant_id );
    }

    public function checkSeedingDataStatus( $or_number, $merchant_id )
    {
        return $this->merchantRepository->checkSeedingDataStatus( $or_number, $merchant_id );
    }

    public function gatherPointsSeedingDataTableData( $request )
    {
        $customerQuery = $this->merchantRepository->buildPointsSeedingDataQuery($request);
        $response_data['draw'] = $request->draw;
        $response_data['data'] = $this->merchantRepository->getPointsSeedingData($customerQuery, $request);
        $response_data['recordsFiltered'] = $this->merchantRepository->countFilterPointsSeedingData($customerQuery);
        $response_data['recordsTotal'] = $this->merchantRepository->countAllPointsSeedingData($request);
        return $response_data;
    }

    public function gatherVotesDtAjaxTableData( $request )
    {
        $votesQuery = $this->merchantRepository->buildVotesDataQuery($request);
        $response_data['draw'] = $request->draw;
        $response_data['data'] = $this->merchantRepository->getVotesData($votesQuery, $request);
        $response_data['recordsFiltered'] = $this->merchantRepository->countFilterVotesData($votesQuery);
        $response_data['recordsTotal'] = $this->merchantRepository->countAllVotesData($request);
        return $response_data;
    }

    public function gatherPointsSeedingBatchTableData( $request )
    {
        $query = $this->merchantRepository->buildPointsSeedingBatchQuery($request);
        $response_data['draw'] = $request->draw;
        $response_data['data'] = $this->merchantRepository->getPointsSeedingData($query, $request);
        $response_data['recordsFiltered'] = $this->merchantRepository->countFilterPointsSeedingData($query);
        $response_data['recordsTotal'] = $this->merchantRepository->countAllPointsSeedingBatch($request);
        return $response_data;
    }

    public function getPointsSeedingDataById( $id )
    {
        return $this->merchantRepository->getPointsSeedingDataById( $id );
    }

    public function updateSeedingData( $id )
    {
        return $this->merchantRepository->updateSeedingData( $id );
    }

    public function getVoucherItemData( $id )
    {
        return $this->merchantRepository->getVoucherItemData( $id );
    }

    public function getVoucherCodes( $id )
    {
        return $this->merchantRepository->getVoucherCodes( $id );
    }

    public function getVoucherBranches( $id )
    {
        return $this->merchantRepository->getVoucherBranches( $id );
    }

    public function transferVoucherCode($id, $merchant_id, $voucher_id)
    {
        return $this->merchantRepository->transferVoucherCode($id, $merchant_id, $voucher_id);
    }

    public function getReceiptData( $id )
    {
        return $this->merchantRepository->getReceiptData( $id );
    }

    public function getClientCode( $request )
    {
        $merchant_id = $request->session()->get('user_id');
        $request->request->add(['merchant_id' => $merchant_id]);

        $query = $this->merchantRepository->buildClientCodeDataQuery($request);
        $response_data['draw'] = $request->draw;
        $response_data['data'] = $this->merchantRepository->getClientCodeData( $query, $request);
        $response_data['recordsFiltered'] = $query->count();
        $response_data['recordsTotal'] = $this->merchantRepository->countAllClientCodeData($request);
        return $response_data;
    }

    public function getBranches( $request )
    {
        $merchant_id = $request->session()->get('user_id');
        $request->request->add(['merchant_id' => $merchant_id]);
        $user_type = $request->session()->get( 'user_type' );
        $request->request->add(['user_type' => $user_type]);

        $query = $this->merchantRepository->buildBranchesDataQuery($request);
        $response_data['draw'] = $request->draw;
        $response_data['data'] = $this->merchantRepository->getBranchesData( $query, $request);
        if ( $user_type !== 3 ) {
            foreach( $response_data['data'] as $data ) {
                $data->action = '<a href="#" data-toggle="modal" data-target="#deleteBranch" data-action="' . url('management/deletebranch/' . $data->id ) . '" onclick="return false" class="delete branchDelete"><i class="fa fa-trash"></i>Delete</a>
                            <a href="#" data-toggle="modal" data-target="#editBranch" onclick="return false" class="edit branchEdit"><i class="fa fa-pencil"></i>Edit</a>';
            }
        }
        foreach( $response_data['data'] as $data ) {
                $data->branch_logo = StorageHelper::repositoryUrlForView($data->branch_logo);
            }
        $response_data['recordsFiltered'] = $query->count();
        $response_data['recordsTotal'] = $this->merchantRepository->countAllBranchesData($request);
        $response_data['user_type'] = $user_type;
        return $response_data;
    }

    public function getEmployees( $request )
    {
        $merchant_id = $request->session()->get('user_id');
        $request->request->add(['merchant_id' => $merchant_id]);
        $user_type = $request->session()->get( 'user_type' );
        $request->request->add(['user_type' => $user_type]);

        $query = $this->merchantRepository->buildEmployeesDataQuery($request);
        $response_data['draw'] = $request->draw;
        $response_data['data'] = $this->merchantRepository->getEmployeesData( $query, $request);
        if ( $user_type !== 3 ) {
            foreach( $response_data['data'] as $data ) {
                $data->action = '<a href="#" data-toggle="modal" data-target="#deleteEmployee" class="delete employeeDelete" data-action="'. url('management/deleteEmployee/' . $data->userId ) . '"><i class="fa fa-trash"></i> Delete</a>
                            <a href="#" data-toggle="modal" data-target="#editEmployee" class="edit employeeEdit"  onclick="return false"><i class="fa fa-pencil"></i> Edit</a>';
            }
        }
        $response_data['recordsFiltered'] = $query->count();
        $response_data['recordsTotal'] = $this->merchantRepository->countAllEmployeesData($request);
        return $response_data;
    }

    public function getMerchantQuiz( $merchant_id )
    {
        $questions = $this->merchantRepository->getMerchantQuizQuestions( $merchant_id );
        $question_choices = $this->merchantRepository->getQuizQuestionChoices( $questions );
        $quiz = $this->composeQuiz( $questions, $question_choices );

        return $quiz;
    }

    public function composeQuiz( $questions, $question_choices )
    {
        $quiz = [];
        $grouped_choices = [];


        // group first the choices per question.
        foreach( $question_choices as $choice ){
            $grouped_choices[ $choice->question_id ][] = $choice;
        }


        // compose the quiz.
        foreach( $questions as $question ){

            $choices = [];

            if( array_key_exists($question->id, $grouped_choices) ){
                foreach( $grouped_choices[ $question->id ] as $choice ){
                    $choices[] = [
                        'type' => (string) $choice->type,
                        'choice_text' => (string) $choice->text,
                    ];
                }
            }

            $quiz[] = [
                'question_id' => (int) $question->id,
                'question' => (string) $question->text,
                'choices' => $choices,
            ];

        }


        return $quiz;
    }

    public function collectQuestionIds( $questions )
    {
        $quiz_question_ids = [];

        foreach( $questions as $question ){
            $quiz_question_ids[] = $question->id;
        }

        return $quiz_question_ids;
    }

    public function getGroupedQuestionChoiceTypes( $questions )
    {
        $grouped_choice_types = [];
        $question_choices = $this->merchantRepository->getQuizQuestionChoices( $questions );

        if( count($question_choices) ){
            foreach( $question_choices as $choice ){
                $grouped_choice_types[ $choice->question_id ][] = $choice->type;
            }
        }

        return $grouped_choice_types;
    }

    public function getMerchantPointsPromos( $merchant_id )
    {
        return $this->merchantRepository->getMerchantPointsPromos( $merchant_id );
    }
    public function getPointsSeedingDataOrm($batch_id)
    {
        return $this->merchantRepository->getPointsSeedingDataOrm($batch_id);
    }

    public function getCities()
    {
        return $this->merchantRepository->getCities();
    }

}
