<?php

namespace Rush\Modules\Merchant\Console\Commands;

use Illuminate\Console\Command;

class MerchantUpdateStatusCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'merchant-update:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Status for Merchants with flag on Auto Update field.';

    protected $merchantService = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct( \Rush\Modules\Merchant\Services\MerchantService $merchantService ) {
        parent::__construct();
        $this->merchantService = $merchantService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $this->merchantService->autoUpdateMerchantStatus();
    }
}
