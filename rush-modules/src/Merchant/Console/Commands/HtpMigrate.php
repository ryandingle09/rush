<?php

namespace Rush\Modules\Merchant\Console\Commands;

use Illuminate\Console\Command;
use DB;

class HtpMigrate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'htp:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate htp punchcard data to points data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // get merchant ids of htp merchants.
        $htp_punchcard_merchant_id = (int) DB::table('Merchant')->where('business_system_link', 'htp-clothing')->value('merchantid');
        $htp_points_merchant_id = (int) DB::table('Merchant')->where('business_system_link', 'htp-clothing-points')->value('merchantid');



        /* customer migration */

        // migrate the htp punchcard customers to htp points.
        $this->migrate_htp_customers( $htp_punchcard_merchant_id, $htp_points_merchant_id );



        /* points migration */

        // get htp points customer ids.
        $customer_ids = $this->get_htp_points_customer_ids( $htp_points_merchant_id );

        // get customer stamps to be converted to points.
        $raw_customer_stamps = $this->get_raw_customer_stamps( $customer_ids );

        // get the customer stamps count.
        $customer_stamp_count = $this->get_customer_stamp_count( $raw_customer_stamps );

        // convert the customer stamps count to points.
        $this->convert_customer_stamps_to_points( $customer_stamp_count );



        /* rewards migration */

        // get htp punchcard rewards.
        $punchcard_rewards = $this->get_htp_punchcard_rewards( $htp_punchcard_merchant_id );

        // convert htp punchcard rewards to points rewards.
        // this returns a cross-reward binding array.
        // it's an associative array that uses punchcard reward id as key
        // and points reward id as value.
        // this will be used later to determine the redeemed rewards.
        $cross_reward_bindings = $this->convert_punchcard_rewards_to_points_rewards( $htp_points_merchant_id, $punchcard_rewards );



        /* transactions migration */

        // get htp punchcard transactions.
        $punchcard_transactions = $this->get_htp_punchcard_transactions( $htp_punchcard_merchant_id );

        // convert htp punchcard transactions to points transactions.
        // this returns a redeemed punchcard reward ids array
        // that uses transaction_ref as key and reward_id as value.
        // this will be used later together with $cross_reward_bindings
        // to convert the redeemed punchcard rewards to redeemed points rewards.
        $redeemed_punchcard_reward_ids = $this->convert_punchcard_transactions_to_points_transactions( $htp_points_merchant_id, $punchcard_transactions );



        /* redemption migration */

        // get redeem transactions.
        $redeem_transactions = $this->get_htp_points_redeem_transactions( $htp_points_merchant_id );
        
        // add redemption record for every redeem transaction.
        // use $redeemed_punchcard_reward_ids and $cross_reward_bindings
        // to identify the points rewards counter part of the redeemed punchcard rewards.
        $this->add_redemption_for_redeem_transactions( $redeem_transactions, $redeemed_punchcard_reward_ids, $cross_reward_bindings );

    }

    public function migrate_htp_customers( $htp_punchcard_merchant_id, $htp_points_merchant_id ){

        DB::table('Customer')->where('merchantId', $htp_punchcard_merchant_id)->update(['merchantId' => $htp_points_merchant_id]);

    }

    public function get_htp_points_customer_ids( $htp_points_merchant_id ){

        $customer_ids = DB::table('Customer')->where('merchantId', $htp_points_merchant_id)->pluck('customerId');
        return $customer_ids;

    }

    public function get_raw_customer_stamps( $customer_ids ){

        $raw_customer_stamps = DB::table('Customer AS C')
            ->join('CustomerStampsCard AS CSC', 'C.customerId', '=', 'CSC.customer_id')
            ->join('CustomerStamps AS CS', 'CSC.id', '=', 'CS.stamp_card_id')
            ->whereIn('C.customerId', $customer_ids)
            ->where('CS.void', 0)
            ->where('CS.redeem', 0)
            ->select('C.customerId', 'CS.id')
            ->get();

        return $raw_customer_stamps;

    }

    public function get_customer_stamp_count( $raw_customer_stamps ){

        $grouped_customer_stamps = [];
        $customer_stamp_count = [];

        if( count($raw_customer_stamps) ){
            foreach( $raw_customer_stamps as $customer_stamp ){
                $grouped_customer_stamps[ $customer_stamp->customerId ][] = $customer_stamp->id;
            }
        }

        if( count($grouped_customer_stamps) ){
            foreach( $grouped_customer_stamps as $customer_id => $customer_stamps ){
                $customer_stamp_count[ $customer_id ] = count($customer_stamps);
            }
        }

        return $customer_stamp_count;

    }

    public function convert_customer_stamps_to_points( $customer_stamp_count ){

        $customer_points_data = [];

        if( count($customer_stamp_count) ){
            foreach( $customer_stamp_count as $customer_id => $stamp_count ){

                $customer_points_data[] = [
                    'customerId' => $customer_id,
                    'totalPoints' => $stamp_count,
                    'currentpoints' => $stamp_count,
                    'usedPoints' => 0,
                    'status' => 'active',
                ];

            }
        }

        if( count($customer_points_data) ) DB::table('customerPoints')->insert( $customer_points_data );

    }

    public function get_htp_punchcard_rewards( $htp_punchcard_merchant_id ){

        $punchcard_rewards = DB::table('MerchantPunchCardRewards')->where('merchant_id', $htp_punchcard_merchant_id)->get();

        return $punchcard_rewards;

    }

    public function convert_punchcard_rewards_to_points_rewards( $htp_points_merchant_id, $punchcard_rewards ){

        $cross_reward_bindings = [];

        if( count($punchcard_rewards) ){
            foreach( $punchcard_rewards as $reward ){

                $points_rewards_data['merchantid'] = $htp_points_merchant_id;
                $points_rewards_data['name'] = $reward->reward;
                $points_rewards_data['details'] = $reward->details;
                $points_rewards_data['pointsRequired'] = $reward->no_stamps;
                $points_rewards_data['imageURL'] = $reward->image_url;
                $points_rewards_data['status'] = 1;
                $points_rewards_data['timestamp'] = $reward->updated_at;
                $points_rewards_data['uuid'] = $reward->uuid;
                $points_rewards_data['created_at'] = $reward->updated_at;
                $points_rewards_data['updated_at'] = $reward->updated_at;
                $points_rewards_data['admin_item_id'] = null;
                $points_rewards_data['type'] = "";
                $points_rewards_data['branch_ids'] = $reward->branch_ids;

                $redeem_item_id = DB::table('ItemsForRedeem')->insertGetId( $points_rewards_data );
                
                $cross_reward_bindings[ $reward->id ] = $redeem_item_id;

            }
        }

        return $cross_reward_bindings;

    }

    public function get_htp_punchcard_transactions( $htp_punchcard_merchant_id ){

        $punchcard_transactions = DB::table('CustomerPunchCardTransactions')->where('merchant_id', $htp_punchcard_merchant_id)->get();

        return $punchcard_transactions;

    }

    public function convert_punchcard_transactions_to_points_transactions( $htp_points_merchant_id, $punchcard_transactions ){

        $points_transactions_data = [];
        $redeemed_punchcard_reward_ids = [];
        $htp_points_branch_id = (int) DB::table('Branches')->where('merchantId', $htp_points_merchant_id)->value('id');
        $htp_points_conversion_settings_id = (int) DB::table('MerchantConversionSettings')->where('merchant_id', $htp_points_merchant_id)->value('id');
        $htp_points_employee_id = (int) DB::table('Users')->where('merchantId', $htp_points_merchant_id)->value('userId');


        if( count($punchcard_transactions) ){
            foreach( $punchcard_transactions as $transaction ){

                $points_transactions_data[] = [
                    'transactionReferenceCode' => $transaction->transaction_ref,
                    'branchId' => $htp_points_branch_id,
                    'conversionSettingsId' => $htp_points_conversion_settings_id,
                    'customerId' => $transaction->customer_id,
                    'userId' => $htp_points_employee_id,
                    'amountPaidWithCash' => (float) $transaction->amount,
                    'pointsEarned' => ( $transaction->type != 'redeem' ? (int) $transaction->stamps : 0 ),
                    'pointsRedeem' => ( $transaction->type == 'redeem' ? (int) $transaction->stamps : 0 ),
                    'transactionStatus' => "done",
                    'receiptReferenceNumber' => $transaction->or_no,
                    'transactionType' => $transaction->type,
                    'timestamp' => $transaction->created_at,
                    'merchant_id' => $htp_points_merchant_id,
                    'employee_id' => $htp_points_employee_id,
                    'created_at' => $transaction->created_at,
                    'updated_at' => $transaction->updated_at,
                    'channel' => $transaction->channel,
                    'achievement_id' => $transaction->achievement_transaction_id,
                ];

                if( $transaction->type == 'redeem' ) $redeemed_punchcard_reward_ids[ $transaction->transaction_ref ] = $transaction->reward_id;

            }
        }


        if( count($points_transactions_data) ) DB::table('CustomerTransaction')->insert( $points_transactions_data );


        return $redeemed_punchcard_reward_ids;

    }

    public function get_htp_points_redeem_transactions( $htp_points_merchant_id ){

        $redeem_transactions = DB::table('CustomerTransaction')
            ->where('merchant_id', $htp_points_merchant_id)
            ->where('transactionType', 'redeem')
            ->select('transactionReferenceCode', 'customerId', 'timestamp')
            ->get();

        return $redeem_transactions;

    }

    public function add_redemption_for_redeem_transactions( $redeem_transactions, $redeemed_punchcard_reward_ids, $cross_reward_bindings ){

        if( count($redeem_transactions) ){
            foreach( $redeem_transactions as $transaction ){

                $redeem_data['redeemReferenceCode'] = $transaction->transactionReferenceCode;
                $redeem_data['customerId'] = $transaction->customerId;
                $redeem_data['status'] = 1;
                $redeem_data['timestamp'] = $transaction->timestamp;
                $redeem_data['quantity'] = 1;
                $redeem_data['claimed'] = 1;
                $redeem_id = DB::table('Redeem')->insertGetId( $redeem_data );


                $punchcard_reward_id = $redeemed_punchcard_reward_ids[ $transaction->transactionReferenceCode ];
                $redeem_item_id = $cross_reward_bindings[ $punchcard_reward_id ];

                $redeemed_items_data['redeemId'] = $redeem_id;
                $redeemed_items_data['redeemItemId'] = $redeem_item_id;
                DB::table('redeemedItems')->insert( $redeemed_items_data );

            }
        }

    }
}