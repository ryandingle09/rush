<?php

namespace Rush\Modules\Merchant\Console\Commands;

use Illuminate\Console\Command;
use DB;

class HtpMigrateRollback extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'htp:migrate-rollback';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete the migrated htp points data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $htp_punchcard_merchant_id = (int) DB::table('Merchant')->where('business_system_link', 'htp-clothing')->value('merchantid');
        $htp_points_merchant_id = (int) DB::table('Merchant')->where('business_system_link', 'htp-clothing-points')->value('merchantid');


        // get htp points customer ids.
        $customer_ids = $this->get_htp_points_customer_ids( $htp_points_merchant_id );

        // delete migrated customer points.
        $this->delete_migrated_points( $customer_ids );

        // delete migrated htp points rewards.
        $this->delete_migrated_rewards( $htp_points_merchant_id );

        // delete migrated transactions.
        $this->delete_migrated_transactions( $htp_points_merchant_id );

        // delete migrated redemptions.
        $this->delete_migrated_redemptions( $customer_ids );

        // unmigrate htp points customers to htp punchcard.
        $this->unmigrate_htp_customers( $htp_points_merchant_id, $htp_punchcard_merchant_id );
    }

    public function get_htp_points_customer_ids( $htp_points_merchant_id ){

        $customer_ids = DB::table('Customer')->where('merchantId', $htp_points_merchant_id)->pluck('customerId');
        return $customer_ids;

    }

    public function delete_migrated_points( $customer_ids ){

        DB::table('customerPoints')->whereIn('customerId', $customer_ids)->delete();

    }

    public function delete_migrated_rewards( $htp_points_merchant_id ){

        DB::table('ItemsForRedeem')->where('merchantid', $htp_points_merchant_id)->delete();

    }

    public function delete_migrated_transactions( $htp_points_merchant_id ){
        
        DB::table('CustomerTransaction')->where('merchant_id', $htp_points_merchant_id)->delete();

    }

    public function delete_migrated_redemptions( $customer_ids ){

        $redeem_ids = DB::table('Redeem')->whereIn('customerId', $customer_ids)->pluck('redeemId');

        DB::table('Redeem')->whereIn('customerId', $customer_ids)->delete();
        DB::table('redeemedItems')->whereIn('redeemId', $redeem_ids)->delete();

    }

    public function unmigrate_htp_customers( $htp_points_merchant_id, $htp_punchcard_merchant_id ){

        DB::table('Customer')->where('merchantId', $htp_points_merchant_id)->update(['merchantId' => $htp_punchcard_merchant_id]);

    }
}
