<?php

namespace Rush\Modules\Achievements\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Achievements\Repositories\AchievementsInterface;
use Rush\Modules\Achievements\Repositories\AchievementsRepository;

/**
 * Class MerchantRepositoryServiceProvider
 * @package Rush\Modules\Merchant\Providers
 */
class AchievementsRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(AchievementsInterface::class, AchievementsRepository::class);
    }
}
