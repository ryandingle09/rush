<?php
namespace Rush\Modules\Achievements\Services;

use Rush\Modules\Achievements\Models\AchievementsLogsModel;
use Rush\Modules\Achievements\Repositories\AchievementsInterface;
use Rush\Modules\Customer\Services\CustomerService;
use Rush\Modules\Achievements\Repositories\Loyalty\Customer as LoyaltyAchievementCustomerRepository;
use Rush\Modules\Achievements\Repositories\Punchcard\Customer as PunchcardAchievementCustomerRepository;
use Carbon\Carbon;

class AchievementsService
{
    /**
     * @var \Rush\Modules\Achievements\Repositories\AchievementsRepository
     */
    protected $achievementsRepository;

    protected $customerService;

    protected $achievementOptionsLists;
    protected $achievementSmsRepository;

    protected $loyaltyAchievementCustomerRepository;
    protected $punchcardAchievementCustomerRepository;

    /**
     * AchievementsService constructor.
     * @param AchievementsInterface $achievementsRepository
     */
    public function __construct(
        AchievementsInterface $achievementsRepository,
        CustomerService $customerService,
        LoyaltyAchievementCustomerRepository $loyaltyAchievmentCustomerRepository,
        PunchcardAchievementCustomerRepository $punchcardAchievementCustomerRepository)
    {
        $this->achievementsRepository = $achievementsRepository;
        $this->customerService = $customerService;
        $this->achievementOptionsLists = $this->achievementsRepository->all();
        $this->loyaltyAchievementCustomerRepository = $loyaltyAchievmentCustomerRepository;
        $this->punchcardAchievementCustomerRepository = $punchcardAchievementCustomerRepository;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model[]|\Rush\Modules\Achievements\Models\AchievementsModel[]|\Illuminate\Database\Eloquent\Collection
     */
    public function all()
    {
        return $this->achievementsRepository->all();
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function getById($id)
    {
        return $this->achievementsRepository->getById($id);
    }

    /**
     * @param $merchant_id
     * @return array|\Rush\Modules\Achievements\Models\AchievementsModel[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllByMerchantId($merchant_id)
    {
        return $this->achievementsRepository->getAllByMerchantId($merchant_id);
    }

    /**
     * @param $name
     * @param $merchant_id
     * @return mixed|\Rush\Modules\Achievements\Models\AchievementsModel
     */
    public function getByName($name, $merchant_id)
    {
        return $this->achievementsRepository->getByNameMerchant($name, $merchant_id);
    }

    /**
     * @param $data
     * @param $merchant_id
     * @return array|\Illuminate\Database\Eloquent\Collection|\Rush\Modules\Achievements\Models\AchievementsModel[]
     */
    public function updateAchievementsList($data, $merchant_id)
    {
        $achievements = $data['achievements'];
        return $this->achievementsRepository->updateAchievementsList($achievements, $merchant_id);
    }

    public function addCustomerAchievement($achievement_name, $args)
    {
        $customer = isset($args['customer_id']) && $args['customer_id']  ? $this->customerService->getById($args['customer_id']) : null;

        if(isset($args['customer_referrer_id'])) {
            $customer =  $this->customerService->getById($args['customer_referrer_id']);
        }

        $achievement = $this->achievementsRepository->getByNameMerchant($achievement_name, $args['merchant_id']);

        if (!$achievement || !isset($achievement->options->enabled) || !$achievement->options->enabled) {
            return false;
        }

        if ((! $achievement->options->repeat) && $this->achievementsRepository->isCustomerAchievementUnlocked($achievement->id, $customer->customerId)) {
            return false;
        }

        $customers = $this->customerService->getAllByNumbers($this->getCustomerFromAchievement($achievement), $args['merchant_id']);
        if(! $customers->isEmpty()) {
            foreach($customers as $customer) {
                if($customer->merchant->packageId == 1) {
                    // loyalty achievements
                    $achievementCustomerRepository = $this->loyaltyAchievementCustomerRepository;
                } else {
                    // punchcard achievements
                    $achievementCustomerRepository = $this->punchcardAchievementCustomerRepository;
                }
                $achievementCustomerRepository->setUp($customer, $achievement);
                $achievementCustomerRepository->check($args);
            }
            $this->achievementsRepository->removeCustomers($achievement);
        } else {
            if($customer->merchant->packageId == 1) {
                // loyalty achievements
                $achievementCustomerRepository = $this->loyaltyAchievementCustomerRepository;
            } else {
                // punchcard achievements
                $achievementCustomerRepository = $this->punchcardAchievementCustomerRepository;
            }
            $achievementCustomerRepository->setUp($customer, $achievement);
            $achievementCustomerRepository->check($args);
        }
    }

    public function updateAchievement($achievement_id, $data)
    {
        return $this->achievementsRepository->update($achievement_id, $data);
    }

    protected function getCustomerFromAchievement($achievement)
    {
        if(isset($achievement->options->customers) && $achievement->options->customers) {
            return explode("\r\n", $achievement->options->customers);
        }

        return false;
    }
}