<?php
namespace Rush\Modules\Achievements\Repositories;

use Rush\Modules\Customer\Models\CustomerTransactionsModel;
use Rush\Modules\Helpers\SmsHelper;
use Rush\Modules\Customer\Helpers\SmsLogHelper;
use Carbon\Carbon;

/**
 * Class AchievementSmsRepository
 * @package Rush\Modules\Achievements\Repositories
 */
class AchievementSmsRepository
{
    /**
     * @var $smsHelper SmsHelper
     */
    protected $smsHelper;

    private function setCredential($customer)
    {
        $this->smsHelper = new SmsHelper(
            $customer->merchant->sms_credentials->shortcode,
            $customer->merchant->sms_credentials->app_id,
            $customer->merchant->sms_credentials->app_secret,
            $customer->merchant->sms_credentials->passphrase
        );
    }

    /**
     * @param \Rush\Modules\Customer\Models\CustomerModel  $customer
     * @param float $points
     * @param \Rush\Modules\Customer\Models\CustomerTransactionsModel $transaction
     * @return mixed|string
     */
    public function completedProfile($customer, $points, $transaction)
    {
        $this->setCredential($customer);
        $balance_points = number_format((float) $customer->points->currentpoints, 2);
        $message = "Hi, {$customer->fullName}! You have earned {$points} {$customer->merchant->settings->points_name} point(s) for completing your profile at {$customer->merchant->settings->program_name}! Your new balance is {$balance_points} {$customer->merchant->settings->points_name} point(s). Keep collecting {$customer->merchant->settings->points_name} points for every purchase to redeem exciting rewards! Ref. No {$transaction->transactionReferenceCode}.";

        $response = $this->smsHelper->send($customer->mobileNumber, $message);
        SmsLogHelper::log($customer->mobileNumber, $message, $response, $customer->merchantId, 'profile-completion-achievement');

        return $response;
    }

    /**
     * @param \Rush\Modules\Customer\Models\CustomerModel $customer
     * @param float $points
     * @param \Rush\Modules\Customer\Models\CustomerTransactionsModel $transaction
     * @return mixed|string
     */
    public function customerAppActivation($customer, $points, $transaction)
    {
        $this->setCredential($customer);
        $balance_points = number_format((float) $customer->points->currentpoints, 2);
        $message = "Hi, {$customer->fullName}! You have earned {$points} {$customer->merchant->settings->points_name} point(s) for activating your account at {$customer->merchant->settings->program_name}! Your new balance is {$balance_points} {$customer->merchant->settings->points_name} point(s). Keep collecting {$customer->merchant->settings->points_name} points for every purchase to redeem exciting rewards! Ref. No {$transaction->transactionReferenceCode}.";

        $response = $this->smsHelper->send($customer->mobileNumber, $message);
        SmsLogHelper::log($customer->mobileNumber, $message, $response, $customer->merchantId, 'customer-app-activation-achievement');

        return $response;
    }

    /**
     * @param \Rush\Modules\Customer\Models\CustomerModel $customer
     * @param float $points
     * @param float $points_earned
     * @param integer $no_of_transaction
     * @param \Rush\Modules\Customer\Models\CustomerTransactionsModel $transaction
     * @return mixed|string
     */
    public function transactionNoWithMinimumAmount($customer, $points, $points_earned, $no_of_transaction, $transaction)
    {
        $this->setCredential($customer);
        $current_points =  number_format( (float) ($customer->points->currentpoints + $points_earned),2);
        $message = "Hi, {$customer->fullName}! You have earned {$points} {$customer->merchant->settings->points_name} point(s) for successfully completing first {$no_of_transaction} transactions with a minimum amount required! Your new balance is {$current_points} {$customer->merchant->settings->points_name} point(s). Keep collecting {$customer->merchant->settings->points_name} points for every purchase to redeem exciting rewards! Ref. No {$transaction->transactionReferenceCode}.";

        $response = $this->smsHelper->send($customer->mobileNumber, $message);
        SmsLogHelper::log($customer->mobileNumber, $message, $response, $customer->merchantId, 'transaction-no-with-minimum-amount-achievement');

        return $response;
    }

    /**
     * @param \Rush\Modules\Customer\Models\CustomerModel $customer
     * @param float $points
     * @param float $redeemed_points
     * @param integer $redemption_no
     * @param \Rush\Modules\Customer\Models\CustomerTransactionsModel $transaction
     * @return mixed|string
     */
    public function redemptionNoWithMinimumPoints($customer, $points, $redeemed_points, $redemption_no, $transaction)
    {
        $this->setCredential($customer);
        $current_points = number_format( (float) ($customer->points->currentpoints - $redeemed_points), 2);
        $message = "Hi, {$customer->fullName}! You have earned {$points} {$customer->merchant->settings->points_name} point(s) for successfully completing first {$redemption_no} redemptions with a minimum points required to redeem! Your new balance is {$current_points} {$customer->merchant->settings->points_name} point(s). Keep collecting {$customer->merchant->settings->points_name} points for every purchase to redeem exciting rewards! Ref. No {$transaction->transactionReferenceCode}.";

        $response = $this->smsHelper->send($customer->mobileNumber, $message);
        SmsLogHelper::log($customer->mobileNumber, $message, $response, $customer->merchantId, 'redemption-no-with-mininmum-points');
        return $response;
    }

    /**
     * @param \Rush\Modules\Customer\Models\CustomerModel $customer
     * @param float $points
     * @param integer $year
     * @param \Rush\Modules\Customer\Models\CustomerTransactionsModel $transaction
     * @return mixed|string
     */
    public function purchaseNoWithinYear($customer, $points, $year, $transaction)
    {
        $this->setCredential($customer);
        $current_points = number_format( (float) $customer->points->currentpoints, 2);
        $message = "Hi, {$customer->fullName}! You have earned {$points} {$customer->merchant->settings->points_name} point(s) for successfully completing the total minimum amount purchase within {$year} year! Your new balance is {$current_points} {$customer->merchant->settings->points_name} point(s). Keep collecting {$customer->merchant->settings->points_name} points for every purchase to redeem exciting rewards! Ref. No {$transaction->transactionReferenceCode}.";

        $response = $this->smsHelper->send($customer->mobileNumber, $message);
        SmsLogHelper::log($customer->mobileNumber, $message, $response, $customer->merchantId, 'purchase-no-within-year-achievement');
        return $response;
    }

    /**
     * @param  \Rush\Modules\Customer\Models\CustomerModel $customer
     * @param float $points
     * @param float $points_earned
     * @param string $date
     * @param \Rush\Modules\Customer\Models\CustomerTransactionsModel $transaction
     * @return mixed
     */
    public function specialDayPurchase($customer, $points, $points_earned, $date, $transaction)
    {
        $this->setCredential($customer);
        $current_points = number_format( (float) $customer->points->currentpoints + $points_earned, 2);
        $message = "Hi, {$customer->fullName}! You have earned {$points} {$customer->merchant->settings->points_name} point(s) for your purchase on our special day on {$date}! Your new balance is {$current_points} {$customer->merchant->settings->points_name}. Your new balance is {$customer->points->currentpoints} {$customer->merchant->settings->points_name} point(s). Keep collecting {$customer->merchant->settings->points_name} points for every purchase to redeem exciting rewards! Ref. No {$transaction->transactionReferenceCode}.";

        $response = $this->smsHelper->send($customer->mobileNumber, $message);
        SmsLogHelper::log($customer->mobileNumber, $message, $response, $customer->merchantId, 'special-day-purchase-achievement');
        return $response;
    }

    /**
     * @param \Rush\Modules\Customer\Models\CustomerModel $customer
     * @param  float $points
     * @param  float $amount
     * @param  \Rush\Modules\Customer\Models\CustomerTransactionsModel $transaction
     * @return mixed
     */
    public function doublePoints($customer, $points, $amount, $transaction)
    {
        $this->setCredential($customer);

        $datetime = Carbon::createFromTimestamp(strtotime($transaction->timestamp));
        $current_points = number_format( (float) ($customer->points->currentpoints + $points), 2);
        $message = "Hi, {$customer->fullName}! You have earned additional {$points} {$customer->merchant->settings->points_name} point(s) for your transaction totaling PHP {$amount} in {$customer->merchant->businessName} on {$datetime->format('m-d-Y')}, {$datetime->format('g:i:s A')}. Your new balance is {$current_points} {$customer->merchant->settings->points_name} point(s). Keep collecting {$customer->merchant->settings->points_name} points for every purchase to redeem exciting rewards! Ref. No {$transaction->transactionReferenceCode}";

        $response = $this->smsHelper->send($customer->mobileNumber, $message);
        SmsLogHelper::log($customer->mobileNumber, $message, $response, $customer->merchantId, 'double-points-achievement');
        return $response;
    }

    /**
     * @param \Rush\Modules\Customer\Models\CustomerModel  $customer
     * @param float $points
     * @param string $event
     * @param string $date
     * @param \Rush\Modules\Customer\Models\CustomerTransactionsModel $transaction
     * @return mixed|string
     */
    public function attendedEvent($customer, $points, $event, $date, $transaction)
    {
        $this->setCredential($customer);
        $balance_points = number_format( (float)$customer->points->currentpoints, 2);
        $message = "Hi, {$customer->fullName}! You have earned {$points} {$customer->merchant->settings->points_name} point(s) for attending {$event} on {$date}! Your new balance is {$balance_points} {$customer->merchant->settings->points_name} point(s). Keep collecting {$customer->merchant->settings->points_name} points by attending our scheduled events to redeem exciting rewards! Ref. No $transaction->transactionReferenceCode.";

        $response = $this->smsHelper->send($customer->mobileNumber, $message);
        SmsLogHelper::log($customer->mobileNumber, $message, $response, $customer->merchantId, 'attended-event-achievement');

        return $response;
    }

    /**
     * @param \Rush\Modules\Customer\Models\CustomerModel  $customer
     * @param float $points
     * @param string $survey
     * @param string $date
     * @param \Rush\Modules\Customer\Models\CustomerTransactionsModel $transaction
     * @return mixed
     */
     public function answeringSurvey($customer, $points, $survey, $date, $transaction)
    {
        $this->setCredential($customer);
        $balance_points = number_format( (float)$customer->points->currentpoints, 2);
        $message = "Hi, {$customer->fullName}! You have earned {$points} {$customer->merchant->settings->points_name} point(s) for answering {$survey} survery on {$date}! Your new balance is {$balance_points} {$customer->merchant->settings->points_name} point(s). Keep collecting {$customer->merchant->settings->points_name} point(s) by answering our survey to redeem exciting rewards! Ref. No {$transaction->transactionReferenceCode}.";

        $response = $this->smsHelper->send($customer->mobileNumber, $message);
        SmsLogHelper::log($customer->mobileNumber, $message, $response, $customer->merchantId, 'answering-survey-achievement');

        return $response;
    }

    /**
     * @param \Rush\Modules\Customer\Models\CustomerModel  $customer_referrer
     * @param \Rush\Modules\Customer\Models\CustomerModel $customer_referred
     * @param float $points
     * @param \Rush\Modules\Customer\Models\CustomerTransactionsModel $transaction
     * @return mixed
     */
    public function membersReferral($customer_referrer, $customer_referred, $points, $transaction)
    {
        $this->setCredential($customer_referrer);
        $balance_points = number_format( (float)$customer_referrer->points->currentpoints, 2);
        $message = "Hi, {$customer_referrer->fullName}! You have earned {$points} {$customer_referrer->merchant->settings->points_name} point(s) for successfully inviting your friend: {$customer_referred->fullName} to join {$customer_referrer->merchant->settings->program_name}! Your new balance is {$balance_points} {$customer_referrer->merchant->settings->points_name} point(s). Keep collecting {$customer_referrer->merchant->settings->points_name} points for every successful invite and redeem exciting rewards! Ref. No {$transaction->transactionReferenceCode}.";

        $response = $this->smsHelper->send($customer_referrer->mobileNumber, $message);
        SmsLogHelper::log($customer_referrer->mobileNumber, $message, $response, $customer_referrer->merchantId, 'members-referral-achievement');

        return $response;
    }

    /**
     * @param \Rush\Modules\Customer\Models\CustomerModel $customer
     * @param float $points
     * @param string $facebook_page
     * @param \Rush\Modules\Customer\Models\CustomerTransactionsModel $transaction
     * @return mixed
     */
    public function likeFacebookPage($customer, $points, $facebook_page, $transaction)
    {
        $this->setCredential($customer);
        $balance_points = number_format( (float)$customer->points->currentpoints, 2);
        $message = "Hi, {$customer->fullName}! You have earned {$points} {$customer->merchant->settings->points_name} point(s) for liking our facebook page  {$facebook_page}! Your new balance is {$balance_points} {$customer->merchant->settings->points_name} point(s). Keep collecting {$customer->merchant->settings->points_name} point(s) by answering our survey to redeem exciting rewards! Ref. No {$transaction->transactionReferenceCode}.";

        $response = $this->smsHelper->send($customer->mobileNumber, $message);
        SmsLogHelper::log($customer->mobileNumber, $message, $response, $customer->merchantId, 'answering-survey-achievement');

        return $response;
    }
}