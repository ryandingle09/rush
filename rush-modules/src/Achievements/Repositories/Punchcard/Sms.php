<?php

namespace Rush\Modules\Achievements\Repositories\Punchcard;

use Rush\Modules\Achievements\Models\AchievementsModel;
use Rush\Modules\Customer\Models\CustomerModel;
use Rush\Modules\Helpers\SmsHelper;
use Rush\Modules\Customer\Helpers\SmsLogHelper;
use Carbon\Carbon;

class Sms {

    /**
     * @var \Rush\Modules\Customer\Models\CustomerModel
     */
    protected $customer;

    protected $achievement;

    /**
     * @var $smsHelper SmsHelper
     */
    protected $smsHelper;

    public function __construct($customer, $achievement)
    {
        $this->customer = $customer;
        $this->achievement = $achievement;
        $this->_setCredential();
    }

    protected function _setCredential()
    {
        $this->smsHelper = new SmsHelper(
            $this->customer->merchant->sms_credentials->shortcode,
            $this->customer->merchant->sms_credentials->app_id,
            $this->customer->merchant->sms_credentials->app_secret,
            $this->customer->merchant->sms_credentials->passphrase
        );
    }

    public function completedProfile($args)
    {
        $current_stamps = count($this->customer->card->stamps);
        $message = "Hi, {$this->customer->fullName}! You have earned {$args['stamps_earned']} {$this->customer->merchant->settings->stamp_name} stamp(s) for completing your profile at {$this->customer->merchant->settings->program_name}! Your new balance is {$current_stamps} {$this->customer->merchant->settings->stamp_name} stamp(s). Keep collecting {$this->customer->merchant->settings->stamp_name} stamps for every purchase to redeem exciting rewards! Ref. No {$args['transaction']->transaction_ref}.";

        $response = $this->smsHelper->send($this->customer->mobileNumber, $message);
        SmsLogHelper::log($this->customer->mobileNumber, $message, $response, $this->customer->merchantId, 'punchcard-customer-achievement:'. $this->achievement->name);

        return $response;
    }

    public function accountActivation($args)
    {
        $current_stamps = count($this->customer->card->stamps);
        $message = "Hi, {$this->customer->fullName}! You have earned {$args['stamps_earned']} {$this->customer->merchant->settings->stamp_name} stamp(s) for activating your account at {$this->customer->merchant->settings->program_name}! Your new balance is {$current_stamps} {$this->customer->merchant->settings->stamp_name} stamp(s). Keep collecting {$this->customer->merchant->settings->stamp_name} stamps for every purchase to redeem exciting rewards! Ref. No {$args['transaction']->transaction_ref}.";

        $response = $this->smsHelper->send($this->customer->mobileNumber, $message);
        SmsLogHelper::log($this->customer->mobileNumber, $message, $response, $this->customer->merchantId, 'punchcard-customer-achievement:'. $this->achievement->name);

        return $response;
    }

    public function customerReferrals($args)
    {
        $current_stamps = count($this->customer->card->stamps);
        $message = "Hi, {$this->customer->fullName}! You have earned {$args['stamps_earned']} {$this->customer->merchant->settings->stamp_name} stamp(s) for successfully inviting your friend: {$args['customer_referred']->fullName} to join {$this->customer->merchant->settings->program_name}! Your new balance is {$current_stamps} {$this->customer->merchant->settings->stamp_name} stamp(s). Keep collecting {$this->customer->merchant->settings->stamp_name} stamps for every successful invite and redeem exciting rewards! Ref. No {$args['transaction']->transaction_ref}.";

        $response = $this->smsHelper->send($this->customer->mobileNumber, $message);
        SmsLogHelper::log($this->customer->mobileNumber, $message, $response, $this->customer->merchantId, 'punchcard-customer-achievement:'. $this->achievement->name);

        return $response;
    }

    public function transactionNoWithMinimumAmount($args)
    {
        $current_stamps = count($this->customer->card->stamps);
        $message = "Hi, {$this->customer->fullName}! You have earned {$args['stamps_earned']} {$this->customer->merchant->settings->stamp_name} stamp(s) for successfully completing first {$this->achievement->transaction_no} transactions with a minimum amount required! Your new balance is {$current_stamps} {$this->customer->merchant->settings->stamp_name} stamp(s). Keep collecting {$this->customer->merchant->settings->stamp_name} stamps for every purchase to redeem exciting rewards! Ref. No {$args['transaction']->transaction_ref}.";

        $response = $this->smsHelper->send($this->customer->mobileNumber, $message);
        SmsLogHelper::log($this->customer->mobileNumber, $message, $response, $this->customer->merchantId, 'punchcard-customer-achievement:'. $this->achievement->name);

        return $response;
    }

    public function redemptionNoWithMinimumStamps($args)
    {
        $current_stamps = count($this->customer->card->stamps);
        $message = "Hi, {$this->customer->fullName}! You have earned {$args['stamps_earned']} {$this->customer->merchant->settings->stamp_name} stamps(s) for successfully completing first {$this->achievement->redemption_no} redemptions with a minimum stamps required to redeem! Your new balance is {$current_stamps} {$this->customer->merchant->settings->stamp_name} stamp(s). Keep collecting {$this->customer->merchant->settings->stamp_name} stamps for every purchase to redeem exciting rewards! Ref. No {$args['transaction']->transaction_ref}.";

        $response = $this->smsHelper->send($this->customer->mobileNumber, $message);
        SmsLogHelper::log($this->customer->mobileNumber, $message, $response, $this->customer->merchantId, 'punchcard-customer-achievement:'. $this->achievement->name);

        return $response;
    }

    public function purchaseNoWithinYear($args)
    {
        $current_stamps = count($this->customer->card->stamps);
        $message = "Hi, {$this->customer->fullName}! You have earned {$args['stamps_earned']} {$this->customer->merchant->settings->stamp_name} stamp(s) for successfully completing the total minimum amount purchase within {$this->achievement->options->year} year! Your new balance is {$current_stamps} {$this->customer->merchant->settings->stamp_name} stamp(s). Keep collecting {$this->customer->merchant->settings->stamp_name} stamps for every purchase to redeem exciting rewards! Ref. No {$args['transaction']->transaction_ref}.";

        $response = $this->smsHelper->send($this->customer->mobileNumber, $message);
        SmsLogHelper::log($this->customer->mobileNumber, $message, $response, $this->customer->merchantId, 'punchcard-customer-achievement:'. $this->achievement->name);

        return $response;
    }

    public function specialDayPurchase($args)
    {
        $current_stamps = count($this->customer->card->stamps);
        $message = "Hi, {$this->customer->fullName}! You have earned {$args['stamps_earned']} {$this->customer->merchant->settings->stamp_name} stamp(s) for your purchase on our special day on {$this->achievement->options->date}! Your new balance is {$current_stamps}. Keep collecting {$this->customer->merchant->settings->stamp_name} stamps for every purchase to redeem exciting rewards! Ref. No {$args['transaction']->transaction_ref}.";

        $response = $this->smsHelper->send($this->customer->mobileNumber, $message);
        SmsLogHelper::log($this->customer->mobileNumber, $message, $response, $this->customer->merchantId, 'punchcard-customer-achievement:'. $this->achievement->name);

        return $response;
    }

    public function doubleStamps($args)
    {
        $datetime = Carbon::createFromTimestamp($args['transaction']->transaction_ref);
        $current_stamps = count($this->customer->card->stamps) + $args['stamps_earned'];
        $message = "Hi, {$this->customer->fullName}! You have earned additional {$args['stamps_earned']} {$this->customer->merchant->settings->stamp_name} stamp(s) for your transaction totaling {$args['amount']} in {$this->customer->merchant->settings->program_name} on {$datetime->format('m-d-Y')}, {$datetime->format('g:i:s A')}. Your new balance is {$current_stamps} {$this->customer->merchant->settings->stamp_name} stamp(s). Keep collecting {$this->customer->merchant->settings->stamp_name} stamps for every purchase to redeem exciting rewards! Ref. No {$args['transaction']->transaction_ref}.";

        $response = $this->smsHelper->send($this->customer->mobileNumber, $message);
        SmsLogHelper::log($this->customer->mobileNumber, $message, $response, $this->customer->merchantId, 'punchcard-customer-achievement:'. $this->achievement->name);

        return $response;
    }

    public function attendedEvent($args)
    {
       $current_stamps = count($this->customer->card->stamps);
       $message = "Hi, {$this->customer->fullName}! You have earned {$args['stamps_earned']} {$this->customer->merchant->settings->stamp_name} stamp(s) for attending {$this->achievement->options->name} on {$this->achievement->options->date}! Your new balance is {$current_stamps} {$this->customer->merchant->settings->stamp_name} stamp(s). Keep collecting {$this->customer->merchant->settings->stamp_name} stamp by attending our scheduled events to redeem exciting rewards! Ref. No {$args['transaction']->transaction_ref}.";

        $response = $this->smsHelper->send($this->customer->mobileNumber, $message);
        SmsLogHelper::log($this->customer->mobileNumber, $message, $response, $this->customer->merchantId, 'punchcard-customer-achievement:'. $this->achievement->name);

        return $response;
    }

    public function answeringSurvey($args)
    {
        $current_stamps = count($this->customer->card->stamps);
        $message = "Hi, {$this->customer->fullName}! You have earned {$args['stamps_earned']} {$this->customer->merchant->settings->stamp_name} stamp(s) for answering {$this->achievement->options->name} survery on {$this->achievement->options->date}! Your new balance is {$current_stamps} {$this->customer->merchant->settings->stamp_name} stamp(s). Keep collecting {$this->customer->merchant->settings->stamp_name} by answering our survey to redeem exciting rewards! Ref. No {$args['transaction']->transaction_ref}.";

        $response = $this->smsHelper->send($this->customer->mobileNumber, $message);
        SmsLogHelper::log($this->customer->mobileNumber, $message, $response, $this->customer->merchantId, 'punchcard-customer-achievement:'. $this->achievement->name);

        return $response;
    }

}