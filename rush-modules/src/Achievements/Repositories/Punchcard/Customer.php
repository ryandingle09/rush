<?php

namespace Rush\Modules\Achievements\Repositories\Punchcard;

use Rush\Modules\Achievements\Models\AchievementsModel;
use Rush\Modules\Achievements\Models\AchievementsLogsModel;
use Rush\Modules\Achievements\Models\AchievementsTransactionsModel;
use Rush\Modules\Customer\Repositories\CustomerRepository;
use Rush\Modules\Customer\Models\CustomerModel;
use Carbon\Carbon;
use Rush\Modules\Achievements\Repositories\Punchcard\Sms;
use Rush\Modules\Customer\Models\CustomerPunchCardTransactionsModel;
use Rush\Modules\Customer\Models\CustomerStamps;


class Customer {

    protected $customer;
    protected $customerRepository;
    /**
     * @var \Rush\Modules\Achievements\Repositories\Punchcard\Sms
     */
    protected $sms;
    protected $achievement;

    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    public function setUp(CustomerModel $customer, AchievementsModel $achievement)
    {
        $this->customer = $customer;
        $this->achievement = $achievement;
        $this->sms = new Sms($this->customer, $this->achievement);
    }

    public function check($args)
    {
        if(! $this->customer->card ) {
            return;
        }

        $args['stamps'] = isset($args['stamps']) && $args['stamps'] ? $args['stamps'] : 0;
        switch ($this->achievement->name) {
            case 'complete_profile': {
                if ($this->_verifyProfile()) {
                    $args['stamps_earned'] = $this->achievement->options->stamps;
                    $args['transaction'] = $this->addCustomerTransaction($args);
                    if($this->getEarnedStamps($args['stamps_earned'])) {
                        $this->sms->completedProfile($args);
                    }
                }
                break;
            }
            case 'customer_app_activation': {
                $args['stamps_earned'] = $this->achievement->options->stamps;
                $args['transaction'] = $this->addCustomerTransaction($args);
                if($this->getEarnedStamps($args['stamps_earned'])) {
                    $this->sms->accountActivation($args);
                }
                break;
            }
            case 'members_referral': {
                $args['stamps_earned'] = $this->achievement->options->stamps;
                $args['customer_referred'] = $this->customerRepository->getById($args['customer_referred_id']);
                $args['transaction'] = $this->addCustomerTransaction($args);

                if (isset($this->achievement->options->referee_stamps) && $this->achievement->options->referee_stamps) {
                   $this->addCustomerRefereeTransaction($args['customer_referred'], $args);
                }

                $this->customerRepository->saveMemberReferral(['customer_referrer_id' => $this->customer->customerId, 'customer_referred_id' => $args['customer_referred']->customerId]);

                if($this->getEarnedStamps($args['stamps_earned'])) {
                    $this->sms->customerReferrals($args);
                }

                break;
            }
            case 'transaction_no_with_minimum_amount': {
                if ($this->_verifyCustomerMinimumEarnTransaction()) {
                    $args['stamps_earned'] = $this->achievement->options->stamps;
                    $args['transaction'] = $this->addCustomerTransaction($args);
                    if($this->getEarnedStamps($args['stamps_earned'])) {
                        $this->sms->transactionNoWithMinimumAmount($args);
                    }
                }
                break;
            }
            case 'redemption_no_with_minimum_stamps': {
                if ($this->_verifyCustomerMinimumRedeemedTransaction()) {
                    $args['stamps_earned'] = $this->achievement->options->stamps;
                    $args['transaction'] = $this->addCustomerTransaction($args);
                    if($this->getEarnedStamps($args['stamps_earned'])) {
                        $this->sms->redemptionNoWithMinimumStamps($args);
                    }
               }
               break;
            }
            case 'purchase_no_within_year': {
                if ($this->_verifyCustomerEarnTransactionForYear()) {
                    $args['stamps_earned'] = $this->achievement->options->stamps;
                    $args['transaction'] = $this->addCustomerTransaction($args);
                    if($this->getEarnedStamps($args['stamps_earned'])) {
                        $this->sms->purchaseNoWithinYear($args);
                    }
                }
                break;
            }
            case 'special_day_purchase': {
                if ($this->_verifySpecialDayofPurchase()) {
                    $args['stamps_earned'] = $this->achievement->options->stamps;
                    $args['transaction'] = $this->addCustomerTransaction($args);
                    if($this->getEarnedStamps($args['stamps_earned'])) {
                        $this->sms->specialDayPurchase($args);
                    }
                }
                break;
            }
            case 'double_stamps': {
                if ($this->_verifyDoubleStamps() && $args['stamps']) {
                    $args['stamps_earned'] = $args['stamps'];
                    $args['transaction'] = $this->addCustomerTransaction($args);
                    if($this->getEarnedStamps($args['stamps_earned'])) {
                        $this->sms->doubleStamps($args);
                    }
                }
                break;
            }
            case 'attended_event': {
                $args['stamps_earned'] = $this->achievement->options->stamps;
                $args['transaction'] = $this->addCustomerTransaction($args);
                if($this->getEarnedStamps($args['stamps_earned'])) {
                    $this->sms->attendedEvent($args);
                }
                break;
            }
            case 'answering_survey': {
                    $args['stamps_earned'] = $this->achievement->options->stamps;
                    $args['transaction'] = $this->addCustomerTransaction($args);
                    if($this->getEarnedStamps($args['stamps_earned'])) {
                       $this->sms->answeringSurvey($args);
                    }
                    break;
                }
        }
    }

    private function addAchievementLog()
    {
        $unlocked_achievement = new AchievementsLogsModel;
        $unlocked_achievement->customer_id = $this->customer->customerId;
        $unlocked_achievement->achievement_unlock_id = $this->achievement->id;

        return $unlocked_achievement->save();
    }

    private function addAchievementTransaction()
    {
        // add achievment transaction log
        $achievement_transaction  = new AchievementsTransactionsModel;
        $achievement_transaction->name = $this->achievement->name;
        $achievement_transaction->options = json_encode($this->achievement->options);
        $achievement_transaction->customer_id = $this->customer->customerId;
        $achievement_transaction->achievement_unlock_id = $this->achievement->id;
        $achievement_transaction->options_name = isset($this->achievement->options->name) ? $this->achievement->options->name : '';
        $achievement_transaction->options_date = isset($this->achievement->options->date) ? $this->achievement->options->date : '';
        $achievement_transaction->save();


        return $achievement_transaction;
    }

    private function addCustomerTransaction($args)
    {
        $achievement_transaction = $this->addAchievementTransaction();
        $achievement_log = $this->addAchievementLog();

        // add customer transaction
        $reference = time();
        $transaction = new CustomerPunchCardTransactionsModel;
        $transaction->customer_id = $this->customer->customerId;
        $transaction->employee_id = isset($args['employee_id']) ? $args['employee_id'] : null;
        $transaction->merchant_id = $this->customer->merchantId;
        $transaction->date_punched = date('Y-m-d G:i:s', $reference);
        $transaction->promo_id = $this->customer->card->punchcard->id;
        $transaction->amount = null;
        $transaction->stamps = $args['stamps_earned'];
        $transaction->reward_id = null;
        $transaction->transaction_ref = $reference;
        $transaction->stamp_card_id = $this->customer->card->id;
        $transaction->achievement_transaction_id = $achievement_transaction->id;
        $transaction->void = 0;
        $transaction->status = 0;
        $transaction->type = 'achievement-unlock';
        $transaction->save();

        // Add stamps
        // Check for maximum stamps required
        // Get promo max stamp
        $stamps = $this->getEarnedStamps($args['stamps_earned']);

        for ($i = 1; $i <= $stamps; $i++) {
            $stamp = new CustomerStamps();
            $stamp->transaction_id = $transaction->id;
            $stamp->stamp_card_id = $this->customer->card->id;
            $stamp->void = 0;
            $stamp->save();
        }

        return $transaction;
    }

    protected function getEarnedStamps($stamps)
    {
        $max_num_stamp = $this->customer->card->punchcard->num_stamps;
        $current_no_stamp = count($this->customer->card->stamps);

        if (($current_no_stamp == 0) && $stamps > $max_num_stamp) {
            $stamps = $max_num_stamp;
        } elseif (($current_no_stamp + $stamps) > $max_num_stamp) {
            $stamps = $max_num_stamp - $current_no_stamp;
        } elseif($current_no_stamp == $max_num_stamp) {
            $stamps = 0;
        }

       return $stamps;
    }

    protected function _verifyProfile()
    {
        if ($this->customer->fullName &&
            $this->customer->gender &&
            $this->customer->mobileNumber &&
            $this->customer->birthDate &&
            $this->customer->email) {
            return true;
        }

        return false;
    }

    protected function _verifyCustomerMinimumEarnTransaction()
    {
        $minimum_amount = $this->achievement->options->amount;
        $no_of_transactions = $this->achievement->options->transaction_no;
        $transactions = $this->customer->transactions()->where('type', 'earn')->where('amount', '>=', $minimum_amount)->get();

        if ($transactions->count() == (int)$no_of_transactions) {
            return true;
        }

        return false;
    }

    protected function _verifyCustomerMinimumRedeemedTransaction()
    {
        $stamps = $this->achievement->options->redeemed_stamps;
        $no_of_redemptions = $this->achievement->options->redemption_no;

        $transactions = $this->customer
                            ->transactions()
                            ->where('type', 'redeem')
                            ->whereHas('reward', function($query) use ($stamps) {
                                $query->where('no_stamps', '>=', $stamps);
                            })
                            ->get();


        if ($transactions->count() >= $no_of_redemptions) {
            return true;
        }

        return false;
    }

    protected function _verifyCustomerEarnTransactionForYear()
    {
        $year = $this->achievement->options->year;
        $amount = $this->achievement->amount;
        $registration_date = Carbon::createFromFormat('Y-m-d H:i:s', $this->customer->timestamp);
        $current_date = Carbon::now();
        $customer_year = $registration_date->diffInYears($current_date);

        if ($customer_year <= $year) {
            $total_amount = (float) $this->customer->transactions()->where('type', 'earn')->sum('amount');
            if ($total_amount >= $amount) {
                return true;
            }
            return false;
        }
        return false;
    }

    protected function _verifySpecialDayofPurchase()
    {
        $special_purchase_date = $this->achievement->options->date;
        if ($special_purchase_date == date('Y-m-d')) {
            return true;
        }

        return false;
    }
    protected function _verifyDoubleStamps()
    {
        $achievement_date = $this->achievement->options->date;
        if ($achievement_date == date('Y-m-d')) {
            return true;
        }

        return false;
    }

    private function addCustomerRefereeTransaction($customer, $args)
    {
        $this->customer = $customer;
        $achievement_transaction = $this->addAchievementTransaction();
        $achievement_log = $this->addAchievementLog();

        // add customer transaction
        $reference = time();
        $transaction = new CustomerPunchCardTransactionsModel;
        $transaction->customer_id = $this->customer->customerId;
        $transaction->employee_id = isset($args['employee_id']) ? $args['employee_id'] : null;
        $transaction->merchant_id = $this->customer->merchantId;
        $transaction->date_punched = date('Y-m-d G:i:s', $reference);
        $transaction->promo_id = $this->customer->card->punchcard->id;
        $transaction->amount = null;
        $transaction->stamps = $this->achievement->options->referee_stamps;
        $transaction->reward_id = null;
        $transaction->transaction_ref = $reference;
        $transaction->stamp_card_id = $this->customer->card->id;
        $transaction->achievement_transaction_id = $achievement_transaction->id;
        $transaction->void = 0;
        $transaction->status = 0;
        $transaction->type = 'achievement-unlock';
        $transaction->save();

        // Add stamps
        // Check for maximum stamps required
        // Get promo max stamp
        $stamps = $this->getEarnedStamps($this->achievement->options->referee_stamps);

        for ($i = 1; $i <= $stamps; $i++) {
            $stamp = new CustomerStamps();
            $stamp->transaction_id = $transaction->id;
            $stamp->stamp_card_id = $this->customer->card->id;
            $stamp->void = 0;
            $stamp->save();
        }

        return $transaction;
    }
}
