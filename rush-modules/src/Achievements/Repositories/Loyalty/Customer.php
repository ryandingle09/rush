<?php

namespace Rush\Modules\Achievements\Repositories\Loyalty;

use Rush\Modules\Achievements\Models\AchievementsModel;
use Rush\Modules\Achievements\Models\AchievementsLogsModel;
use Rush\Modules\Achievements\Models\AchievementsTransactionsModel;
use Rush\Modules\Customer\Models\CustomerModel;
use Rush\Modules\Customer\Models\CustomerTransactionsModel;
use Rush\Modules\Achievements\Repositories\Loyalty\Sms;
use Rush\Modules\Customer\Models\CustomerTransactions;
use Rush\Modules\Customer\Repositories\CustomerRepository;
use Carbon\Carbon;

class Customer {

    protected $customer;
    protected $customerRepository;
    protected $sms;
    protected $achievement;

    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    public function setUp(CustomerModel $customer, AchievementsModel $achievement)
    {
        $this->customer = $customer;
        $this->achievement = $achievement;
        $this->sms = new Sms($this->customer, $this->achievement);
    }

    public function check($args)
    {
        $args['points'] = isset($args['points']) && $args['points'] ? $args['points'] : 0;
        switch ($this->achievement->name) {
            case 'complete_profile': {
                if ($this->_verifyProfile()) {
                    $args['points_earned'] = $this->achievement->options->points;
                    $args['transaction'] = $this->addCustomerTransaction($args);
                    $this->sms->completedProfile($args);
                }
                break;
            }
            case 'customer_app_activation': {
                $args['points_earned'] = $this->achievement->options->points;
                $args['transaction'] = $this->addCustomerTransaction($args);
                $this->sms->accountActivation($args);
                break;
            }
            case 'members_referral': {
                $args['points_earned'] = $this->achievement->options->points;
                $args['customer_referred'] = $this->customerRepository->getById($args['customer_referred_id']);
                $args['transaction'] = $this->addCustomerTransaction($args);
                if (isset($this->achievement->options->referee_points) && $this->achievement->options->referee_points) {
                    $this->addCustomerRefereeTransaction($args['customer_referred'], $args);
                }
                $this->sms->customerReferee($args['customer_referred'], $args);
                $this->customerRepository->saveMemberReferral(['customer_referrer_id' => $this->customer->customerId, 'customer_referred_id' => $args['customer_referred']->customerId]);
                $this->sms->customerReferrals($args);
                break;
            }
            case 'transaction_no_with_minimum_amount': {
                if ($this->_verifyCustomerMinimumEarnTransaction()) {
                    $args['points_earned'] = $this->achievement->options->points;
                    $args['transaction'] = $this->addCustomerTransaction($args);
                    $this->sms->transactionNoWithMinimumAmount($args);
                }
                break;
            }
            case 'redemption_no_with_minimum_points': {
                if ($this->_verifyCustomerMinimumRedeemedTransaction()) {
                    $args['points_earned'] = $this->achievement->options->points;
                    $args['transaction'] = $this->addCustomerTransaction($args);
                    $this->sms->redemptionNoWithMinimumPoints($args);
               }
               break;
            }
            case 'purchase_no_within_year': {
                if ($this->_verifyCustomerEarnTransactionForYear()) {
                    $args['points_earned'] = $this->achievement->options->points;
                    $args['transaction'] = $this->addCustomerTransaction($args);
                    $this->sms->purchaseNoWithinYear($args);
                }
                break;
            }
            case 'special_day_purchase': {
                if ($this->_verifySpecialDayofPurchase()) {
                    $args['points_earned'] = $this->achievement->options->points;
                    $args['transaction'] = $this->addCustomerTransaction($args);
                    $this->sms->specialDayPurchase($args);
                }
                break;
            }
            case 'double_points': {
                if ($this->_verifyDoublePoints()) {
                    $args['points_earned'] = $args['points'];
                    $args['transaction'] = $this->addCustomerTransaction($args);
                    $this->sms->doublePoints($args);
                }
                break;
            }
            case 'attended_event': {
                $args['points_earned'] = $this->achievement->options->points;
                $args['transaction'] = $this->addCustomerTransaction($args);
                $this->sms->attendedEvent($args);
                break;
            }
            case 'answering_survey': {
                $args['points_earned'] = $this->achievement->options->points;
                $args['transaction'] = $this->addCustomerTransaction($args);
                $this->sms->answeringSurvey($args);
                break;
            }
        }
    }

    private function addAchievementLog()
    {
        $unlocked_achievement = new AchievementsLogsModel;
        $unlocked_achievement->customer_id = $this->customer->customerId;
        $unlocked_achievement->achievement_unlock_id = $this->achievement->id;

        return $unlocked_achievement->save();
    }

    private function addAchievementTransaction()
    {
        // add achievment transaction log
        $achievement_transaction  = new AchievementsTransactionsModel;
        $achievement_transaction->name = $this->achievement->name;
        $achievement_transaction->options = json_encode($this->achievement->options);
        $achievement_transaction->customer_id = $this->customer->customerId;
        $achievement_transaction->achievement_unlock_id = $this->achievement->id;
        $achievement_transaction->options_name = isset($this->achievement->options->name) ? $this->achievement->options->name : '';
        $achievement_transaction->options_date = isset($this->achievement->options->date) ? $this->achievement->options->date : '';
        $achievement_transaction->save();


        return $achievement_transaction;
    }

    private function addCustomerTransaction($args)
    {
        $achievement_transaction = $this->addAchievementTransaction();
        $achievement_log = $this->addAchievementLog();

        // add customer transaction
        $conversion_id = $this->customer->merchant->conversion_setting->id;
        $transaction = new CustomerTransactionsModel;
        $transaction->transactionReferenceCode = str_random(12);
        $transaction->branchId = null;
        $transaction->transactionType = 'achievement-unlock';
        $transaction->conversionSettingsId = $conversion_id;
        $transaction->employee_id = isset($args['employee_id']) ? $args['employee_id'] : null;
        $transaction->customerId = $this->customer->customerId;
        $transaction->userId = $this->customer->customerId;
        $transaction->pointsEarned = $args['points_earned'];
        $transaction->pointsRedeem = 0;
        $transaction->amountPaidWithpoints = 0;
        $transaction->amountPaidWithCash = 0;
        $transaction->transactionStatus = 'done';
        $transaction->receiptReferenceNumber = null;
        $transaction->timestamp = date("Y-m-d H:i:s", time());
        $transaction->merchant_id = $this->customer->merchant->merchantid;
        $transaction->branchId = isset($args['branch_id']) ? $args['branch_id'] : null;
        $transaction->achievement_id = $achievement_transaction->id;
        $transaction->begin_points = $this->customer->points->currentpoints;
        $transaction->save();

        // update customer points
        $customer_points = $this->customer->points;
        $current_points = (float)$customer_points->currentpoints + (float) $args['points_earned'];
        $customer_points->currentpoints = $current_points;
        $customer_points->totalPoints = (float)$args['points_earned'] + $customer_points->totalPoints;
        $customer_points->save();

        return $transaction;
    }


    protected function _verifyProfile()
    {
        if ($this->customer->fullName &&
            $this->customer->gender &&
            $this->customer->mobileNumber &&
            $this->customer->birthDate &&
            $this->customer->email) {
            return true;
        }

        return false;
    }

    protected function _verifyCustomerMinimumEarnTransaction()
    {
        $minimum_amount = $this->achievement->options->amount;
        $conversion_setting = $this->customer->merchant->conversion_setting;
        $no_of_transactions = $this->achievement->options->transaction_no;
        $points = $minimum_amount / $conversion_setting->earning_peso;
        $transactions = $this->customer->transactions()->where('transactionType', 'earn')->where('pointsEarned', '>=', $points)->get();

        if ($transactions->count() == (int)$no_of_transactions) {
            return true;
        }

        return false;
    }

    protected function _verifyCustomerMinimumRedeemedTransaction()
    {
        $points = $this->achievement->options->redeemed_points;
        $no_of_redemptions = $this->achievement->options->redemption_no;

        $transactions = $this->customer->transactions()
                                ->whereHas('redeemedItem',
                                    function($query){
                                        $query->where('status', 1);
                                    })
                                ->where('transactionType', 'redeem')->where('pointsRedeem', '>=', $points)->get();

        if ($transactions->count() >= $no_of_redemptions) {
            return true;
        }

        return false;
    }

    protected function _verifyCustomerEarnTransactionForYear()
    {
        $year = $this->achievement->options->year;
        $amount = $this->achievement->options->amount;
        $registration_date = Carbon::createFromFormat('Y-m-d H:i:s', $this->customer->timestamp);
        $current_date = Carbon::now();
        $customer_year = $registration_date->diffInYears($current_date);

        if ($customer_year <= $year) {
            $total_amount= (float)$this->customer->transactions()->where('transactionType', 'earn')->sum('amountPaidWithCash');
            if ($total_amount >= $amount) {
                return true;
            }
            return false;
        }
        return false;
    }

    protected function _verifySpecialDayofPurchase()
    {
        $special_purchase_date = $this->achievement->options->date;
        if ($special_purchase_date == date('Y-m-d')) {
            return true;
        }

        return false;
    }

    protected function _verifyDoublePoints()
    {
        $double_points_date = $this->achievement->options->date;
        if ($double_points_date == date('Y-m-d')) {
            return true;
        }

        return false;
    }

    public function addCustomerRefereeTransaction($customer, $args)
    {
        $this->customer = $customer;
        $achievement_transaction = $this->addAchievementTransaction();
        $achievement_log = $this->addAchievementLog();

        // add customer transaction
        $conversion_id = $this->customer->merchant->conversion_setting->id;
        $transaction = new CustomerTransactionsModel;
        $transaction->transactionReferenceCode = str_random(12);
        $transaction->branchId = null;
        $transaction->transactionType = 'achievement-unlock';
        $transaction->conversionSettingsId = $conversion_id;
        $transaction->employee_id = isset($args['employee_id']) ? $args['employee_id'] : null;
        $transaction->customerId = $this->customer->customerId;
        $transaction->userId = $this->customer->customerId;
        $transaction->pointsEarned = $this->achievement->options->referee_points;
        $transaction->pointsRedeem = 0;
        $transaction->amountPaidWithpoints = 0;
        $transaction->amountPaidWithCash = 0;
        $transaction->transactionStatus = 'done';
        $transaction->receiptReferenceNumber = null;
        $transaction->timestamp = date("Y-m-d H:i:s", time());
        $transaction->merchant_id = $this->customer->merchant->merchantid;
        $transaction->branchId = isset($args['branch_id']) ? $args['branch_id'] : null;
        $transaction->achievement_id = $achievement_transaction->id;
        $transaction->save();

        // update customer points
        $customer_points = $this->customer->points;
        $current_points = (float)$customer_points->currentpoints + (float) $this->achievement->options->referee_points;
        $customer_points->currentpoints = $current_points;
        $customer_points->totalPoints = (float)$this->achievement->options->referee_points + $customer_points->totalPoints;
        $customer_points->save();

        return $transaction;

    }
}
