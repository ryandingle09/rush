<?php

namespace Rush\Modules\Achievements\Repositories\Loyalty;

use Rush\Modules\Achievements\Models\AchievementsModel;
use Rush\Modules\Customer\Models\CustomerModel;
use Rush\Modules\Helpers\SmsHelper;
use Rush\Modules\Customer\Helpers\SmsLogHelper;
use Carbon\Carbon;

class Sms {

    protected $customer;

    protected $achievement;

    /**
     * @var $smsHelper SmsHelper
     */
    protected $smsHelper;

    public function __construct($customer, $achievement)
    {
        $this->customer = $customer;
        $this->achievement = $achievement;
        $this->_setCredential();
    }

    protected function _setCredential()
    {
        $this->smsHelper = new SmsHelper(
            $this->customer->merchant->sms_credentials->shortcode,
            $this->customer->merchant->sms_credentials->app_id,
            $this->customer->merchant->sms_credentials->app_secret,
            $this->customer->merchant->sms_credentials->passphrase
        );
    }

    public function completedProfile($args)
    {
        $current_points = number_format((float) $this->customer->points->currentpoints, 2);
        $message = "Hi, {$this->customer->fullName}! You have earned {$this->achievement->options->points} {$this->customer->merchant->settings->points_name} point(s) for completing your profile at {$this->customer->merchant->settings->program_name}! Your new balance is {$current_points} {$this->customer->merchant->settings->points_name} point(s). Keep collecting {$this->customer->merchant->settings->points_name} points for every purchase to redeem exciting rewards! Ref. No {$args['transaction']->transactionReferenceCode}.";

        $response = $this->smsHelper->send($this->customer->mobileNumber, $message);
        SmsLogHelper::log($this->customer->mobileNumber, $message, $response, $this->customer->merchantId, 'loyalty-customer-achievement:'. $this->achievement->name);

        return $response;
    }

    public function accountActivation($args)
    {
        $current_points = number_format((float) $this->customer->points->currentpoints, 2);
        $message = "Hi, {$this->customer->fullName}! You have earned {$this->achievement->options->points} {$this->customer->merchant->settings->points_name} point(s) for activating your account at {$this->customer->merchant->settings->program_name}! Your new balance is {$current_points} {$this->customer->merchant->settings->points_name} point(s). Keep collecting {$this->customer->merchant->settings->points_name} points for every purchase to redeem exciting rewards! Ref. No {$args['transaction']->transactionReferenceCode}.";

        $response = $this->smsHelper->send($this->customer->mobileNumber, $message);
        SmsLogHelper::log($this->customer->mobileNumber, $message, $response, $this->customer->merchantId, 'loyalty-customer-app-activation-achievement');

        return $response;
    }

    public function customerReferrals($args)
    {
        $current_points = number_format( (float)$this->customer->points->currentpoints, 2);
        $message = "Hi, {$this->customer->fullName}! You have earned {$this->achievement->options->points} {$this->customer->merchant->settings->points_name} point(s) for successfully inviting your friend: {$args['customer_referred']->fullName} to join {$this->customer->merchant->settings->program_name}! Your new balance is {$current_points} {$this->customer->merchant->settings->points_name} point(s). Keep collecting {$this->customer->merchant->settings->points_name} points for every successful invite and redeem exciting rewards! Ref. No {$args['transaction']->transactionReferenceCode}.";

        $response = $this->smsHelper->send($this->customer->mobileNumber, $message);
        SmsLogHelper::log($this->customer->mobileNumber, $message, $response, $this->customer->merchantId, 'loyalty-members-referral-achievement');

        return $response;
    }

    public function transactionNoWithMinimumAmount($args)
    {
        $current_points =  number_format( (float) ($this->customer->points->currentpoints + $args['points']),2);
        $message = "Hi, {$this->customer->fullName}! You have earned {$this->achievement->options->points} {$this->customer->merchant->settings->points_name} point(s) for successfully completing first {$this->achievement->options->transaction_no} transactions with a minimum amount required! Your new balance is {$current_points} {$this->customer->merchant->settings->points_name} point(s). Keep collecting {$this->customer->merchant->settings->points_name} points for every purchase to redeem exciting rewards! Ref. No {$args['transaction']->transactionReferenceCode}.";

        $response = $this->smsHelper->send($this->customer->mobileNumber, $message);
        SmsLogHelper::log($this->customer->mobileNumber, $message, $response, $this->customer->merchantId, 'loyalty-transaction-no-with-minimum-amount-achievement');

        return $response;
    }

    public function redemptionNoWithMinimumPoints($args)
    {
        $current_points = number_format( (float) ($this->customer->points->currentpoints - $args['redeemed_points']), 2);
        $message = "Hi, {$this->customer->fullName}! You have earned {$this->achievement->options->points} {$this->customer->merchant->settings->points_name} point(s) for successfully completing first {$this->achievement->options->redemption_no} redemptions with a minimum points required to redeem! Your new balance is {$current_points} {$this->customer->merchant->settings->points_name} point(s). Keep collecting {$this->customer->merchant->settings->points_name} points for every purchase to redeem exciting rewards! Ref. No {$args['transaction']->transactionReferenceCode}.";

        $response = $this->smsHelper->send($this->customer->mobileNumber, $message);
        SmsLogHelper::log($this->customer->mobileNumber, $message, $response, $this->customer->merchantId, 'loyalty-redemption-no-with-mininmum-points');

        return $response;
    }

    public function purchaseNoWithinYear($args)
    {
        $current_points = number_format( (float) $this->customer->points->currentpoints + $args['points'], 2);
        $message = "Hi, {$this->customer->fullName}! You have earned {$this->achievement->options->points} {$this->customer->merchant->settings->points_name} point(s) for successfully completing the total minimum amount purchase within {$this->achievement->options->year} year! Your new balance is {$current_points} {$this->customer->merchant->settings->points_name} point(s). Keep collecting {$this->customer->merchant->settings->points_name} points for every purchase to redeem exciting rewards! Ref. No {$args['transaction']->transactionReferenceCode}.";

        $response = $this->smsHelper->send($this->customer->mobileNumber, $message);
        SmsLogHelper::log($this->customer->mobileNumber, $message, $response, $this->customer->merchantId, 'loyalty-purchase-no-within-year-achievement');
        return $response;
    }

    public function specialDayPurchase($args)
    {
        $current_points = number_format( (float) $this->customer->points->currentpoints + $args['points'], 2);
        $message = "Hi, {$this->customer->fullName}! You have earned {$this->achievement->options->points} {$this->customer->merchant->settings->points_name} point(s) for your purchase on our special day on {$this->achievement->options->date}! Your new balance is {$current_points} {$this->customer->merchant->settings->points_name} point(s). Keep collecting {$this->customer->merchant->settings->points_name} points for every purchase to redeem exciting rewards! Ref. No {$args['transaction']->transactionReferenceCode}.";

        $response = $this->smsHelper->send($this->customer->mobileNumber, $message);
        SmsLogHelper::log($this->customer->mobileNumber, $message, $response, $this->customer->merchantId, 'loyalty-special-day-purchase-achievement');
        return $response;
    }

    public function doublePoints($args)
    {
        $datetime = Carbon::createFromTimestamp(strtotime($args['transaction']->timestamp));
        $current_points = number_format( (float) ($this->customer->points->currentpoints + $args['points']), 2);
        $message = "Hi, {$this->customer->fullName}! You have earned additional {$args['points']} {$this->customer->merchant->settings->points_name} point(s) for your transaction totaling PHP {$args['amount']} in {$this->customer->merchant->businessName} on {$datetime->format('m-d-Y')}, {$datetime->format('g:i:s A')}. Your new balance is {$current_points} {$this->customer->merchant->settings->points_name} point(s). Keep collecting {$this->customer->merchant->settings->points_name} points for every purchase to redeem exciting rewards! Ref. No {$args['transaction']->transactionReferenceCode}";

        $response = $this->smsHelper->send($this->customer->mobileNumber, $message);
        SmsLogHelper::log($this->customer->mobileNumber, $message, $response, $this->customer->merchantId, 'loyalty-double-points-achievement');
        return $response;
    }

    public function attendedEvent($args)
    {
        $current_points = number_format( (float)$this->customer->points->currentpoints, 2);
        $message = "Hi, {$this->customer->fullName}! You have earned {$this->achievement->options->points} {$this->customer->merchant->settings->points_name} point(s) for attending {$this->achievement->options->name} on {$this->achievement->options->date}! Your new balance is {$current_points} {$this->customer->merchant->settings->points_name} point(s). Keep collecting {$this->customer->merchant->settings->points_name} points by attending our scheduled events to redeem exciting rewards! Ref. No {$args['transaction']->transactionReferenceCode}.";

        $response = $this->smsHelper->send($this->customer->mobileNumber, $message);
        SmsLogHelper::log($this->customer->mobileNumber, $message, $response, $this->customer->merchantId, 'loyalty-attended-event-achievement');

        return $response;
    }

    public function answeringSurvey($args)
    {
        $current_points = number_format( (float)$this->customer->points->currentpoints, 2);
        $message = "Hi, {$this->customer->fullName}! You have earned {$this->achievement->options->points} {$this->customer->merchant->settings->points_name} point(s) for answering {$this->achievement->options->name} survery on {$this->achievement->options->date}! Your new balance is {$current_points} {$this->customer->merchant->settings->points_name} point(s). Keep collecting {$this->customer->merchant->settings->points_name} point(s) by answering our survey to redeem exciting rewards! Ref. No {$args['transaction']->transactionReferenceCode}.";

        $response = $this->smsHelper->send($this->customer->mobileNumber, $message);
        SmsLogHelper::log($this->customer->mobileNumber, $message, $response, $this->customer->merchantId, 'loyalty-answering-survey-achievement');

        return $response;
    }

    public function customerReferee($customer, $args)
    {
        if ($this->achievement->options->referee_points) {
            $message = "Hi {$customer->fullName}! You have earned {$this->achievement->options->referee_points} {$customer->merchant->settings->points_name} point(s) for successfully registering with {$customer->merchant->settings->program_name} using the referral code {$this->customer->referral_code} from {$this->customer->fullName}. Start earning {$customer->merchant->settings->points_name} points every time you make a purchase to redeem exciting rewards!";
        } else {
            $message = "Hi {$customer->fullName}!! You have successfully registered with {$customer->merchant->settings->program_name} using the referral code {$this->customer->referral_code} from {$this->customer->fullName}. We are happy to have you!";
        }

        $response = $this->smsHelper->send($customer->mobileNumber, $message);
        SmsLogHelper::log($this->customer->mobileNumber, $message, $response, $this->customer->merchantId, 'loyalty-referral-achievement');
    }
}
