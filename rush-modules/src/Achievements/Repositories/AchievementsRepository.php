<?php
namespace Rush\Modules\Achievements\Repositories;

use Rush\Modules\Achievements\Models\AchievementsLogsModel;
use Rush\Modules\Achievements\Models\AchievementsModel;
use Rush\Modules\Achievements\Models\AchievementsTransactionsModel;
use Rush\Modules\Customer\Models\CustomerPointsModel;
use Rush\Modules\Customer\Models\CustomerModel;
use Rush\Modules\Repository\AbstractRepository;
use Rush\Modules\Customer\Models\CustomerTransactionsModel;
use Rush\Modules\Customer\Models\CustomerPunchCardTransactionsModel;

class AchievementsRepository extends AbstractRepository implements AchievementsInterface
{
    /**
     * AchievementsRepository constructor.
     * @param AchievementsModel $achievementsModel
     */
    public function __construct(AchievementsModel $achievementsModel)
    {
        parent::__construct($achievementsModel);
    }

    public function getAllByMerchantId($merchant_id)
    {
        $temp_achievements = $this->model->where('merchant_id', $merchant_id)->get();
        $achievements = [];
        foreach($temp_achievements as $achievement) {
            $achievements[$achievement->name] = $achievement;
        }

        return $achievements;
    }

    public function getByNameMerchant($name, $merchant_id)
    {
        return $this->model->where('name', $name)->where('merchant_id', $merchant_id)->first();
    }

    public function updateAchievementsList($achievements, $merchant_id)
    {
        foreach ($achievements as $achievement) {
            foreach ($achievement as $key => $options) {
                $temp = $this->model->where('name', $key)->where('merchant_id', $merchant_id)->first();
                if ($temp) {
                    $this->update($temp->id, ['options' => json_encode($options)]);
                } else {
                    $option = new $this->model;
                    $option->name = $key;
                    $option->options = json_encode($options);
                    $option->merchant_id = $merchant_id;
                    $option->save();
                }
                if ($key == 'members_referral') {
                    $achievement_model = $this->model->where('name', $key)->where('merchant_id', $merchant_id)->first();
                    $settings = $achievement_model->merchant->settings;
                    if (isset($options['enabled']) &&  $options['enabled'] == 'on') {
                        $settings->enable_referral_code = 1;
                    } else {
                        $settings->enable_referral_code = 0;
                    }
                    $settings->save();
                }
            }
        }

        return $this->getAllByMerchantId($merchant_id);
    }

    public function removeCustomers($achievement)
    {
       $temp = [
           'name' => $achievement->name,
           'description' => $achievement->description,
           'options' => $achievement->options,
           'merchant_id' => $achievement->merchant_id
       ];

       $temp['options']->customers = '';
       $temp['options'] = json_encode($temp['options']);

       $this->update($achievement->id, $temp);
    }

    public function isCustomerAchievementUnlocked($achievement_id, $customer_id)
    {
        return AchievementsLogsModel::where('achievement_unlock_id', $achievement_id)->where('customer_id', $customer_id)->first();
    }
}
