<?php
namespace Rush\Modules\Achievements\Models;

use Illuminate\Database\Eloquent\Model;
use Rush\Modules\Merchant\Models\MerchantModel;

class AchievementsModel extends  Model {
    protected $table = "achievements_unlock";

    protected $fillable = ['name', 'description','options'];


    public function getOptionsAttribute($value)
    {
        if($value) {
            $value = json_decode($value);
        }

        return $value;
    }

    public function merchant()
    {
        return $this->belongsTo(MerchantModel::class, 'merchant_id', 'merchantid');
    }
}
