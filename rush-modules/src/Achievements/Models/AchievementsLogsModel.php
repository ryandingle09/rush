<?php
namespace Rush\Modules\Achievements\Models;

use Illuminate\Database\Eloquent\Model;

class AchievementsLogsModel extends  Model {

    protected $table = "achievements_unlock_logs";

    protected $fillable = ['customer_id', 'achievement_unlock_id'];

}
