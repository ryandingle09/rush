<?php
namespace Rush\Modules\Achievements\Models;

use Illuminate\Database\Eloquent\Model;

class AchievementsTransactionsModel extends  Model {
    protected $table = "achievements_unlock_transactions";

    protected $fillable = ['name','options', 'achievement_unlock_id', 'customer_id'];

    public function getOptionsAttribute($value)
    {
        if($value) {
            $value = json_decode($value);
        }

        return $value;
    }

}
