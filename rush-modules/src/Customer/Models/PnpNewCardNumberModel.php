<?php

namespace Rush\Modules\Customer\Models;

use Illuminate\Database\Eloquent\Model;

class PnpNewCardNumberModel extends Model
{
    protected $table = "client";

    public function getIsExistingCardNumberAttribute()
    {
        return false;
    }
}
