<?php

namespace Rush\Modules\Customer\Models;

// Libraries
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Rush\Modules\Customer\Models\CustomerQrCodesModel;
use Rush\Modules\Customer\Models\CustomerSmsLogModel;
use Rush\Modules\Customer\Models\GlobeNumbersPrefixModel;

// Loyalty Specific Models
use Rush\Modules\Customer\Models\CustomerPointsModel;
use Rush\Modules\Customer\Models\CustomerTransactionsModel;
use Rush\Modules\Customer\Models\RedeemModel;
use Rush\Modules\Customer\Models\RedeemedItemsModel;
use Rush\Modules\Customer\Models\CustomerBadgeModel;

// Punchcard Specific Models
use Rush\Modules\Customer\Models\CustomerPunchCardRewardModel;
use Rush\Modules\Customer\Models\CustomerPunchCardTransactionsModel;
use Rush\Modules\Customer\Models\CustomerStamps;
use Rush\Modules\Customer\Models\CustomerStampsCardModel;

// Traits
use Rush\Modules\Customer\Models\Traits\Customer\AttributesTrait;
use Rush\Modules\Customer\Models\Traits\Customer\RelationshipsTrait;
use Logaretm\Transformers\TransformableTrait;

// Contracts
use Logaretm\Transformers\Contracts\Transformable;

use Carbon\Carbon;

/**
 * Class CustomerModel
 *
 * @property-read     integer                                           $id
 * @property          string                                            $name
 * @property          string                                            $uuid
 * @property          string                                            $email
 * @property          string                                            $grade_level              Mathemagis grade level
 * @property          string                                            $mobile_no
 * @property          integer                                           $merchantId
 * @property          \Rush\Modules\Customer\Models\CustomerPointsModel $points
 * @property-read     \Rush\Modules\Merchant\Models\MerchantModel       $merchant
 * @property          boolean                                           $email_verified
 * @property          boolean                                           $mobile_verified
 * @property          string                                            $birthday
 * @property          string                                            $profile_id
 * @property          string                                            $gender
 * @property          boolean                                           $membership_status        Non-Regular/Regular, default: Non-Regular
 */
class CustomerModel extends Model implements Transformable
{
    use AttributesTrait,
        RelationshipsTrait,
        TransformableTrait;

    protected $table = "Customer";
    protected $primaryKey = "customerId";

    public function getPointsAttribute(){
        $points = CustomerPointsModel::where('customerId', $this->customerId)->first();
        if( ! $points ){

            $points = new CustomerPointsModel;
            $points->customerId = $this->customerId;
            $points->totalPoints = 0;
            $points->currentpoints = 0;
            $points->usedPoints = 0;
            $points->status = "active";
            $points->save();

        }

        return $points;

    }

    public function merchant(){

        return $this->belongsTo('Rush\Modules\Merchant\Models\MerchantModel', 'merchantId', 'merchantid');

    }

    public function branch() {

        return $this->belongsTo('Rush\Modules\Merchant\Models\MerchantBranchesModel', 'branch_id', 'id');

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Rush\Modules\Customer\Models\CustomerPunchCardTransactionsModel|\Rush\Modules\Customer\Models\CustomerTransactionsModel
     */
    public function transactions(){
        if( $this->is_basic ) {
            return $this->hasMany('Rush\Modules\Customer\Models\CustomerPunchCardTransactionsModel', 'customer_id', 'customerId');
        } else {
            return $this->hasMany('Rush\Modules\Customer\Models\CustomerTransactionsModel', 'customerId', 'customerId');
        }
    }

    public function getIsBasicAttribute(){

        if( $this->merchant->packageId == 2 ) return true;

        return false;

    }

    public function getCardAttribute(){

        return CustomerStampsCardModel::getCard( $this );

    }

    public function sms_logs()
    {
        return $this->hasMany('Rush\Modules\Customer\Models\CustomerSmsLogModel', 'mobile_number', 'mobileNumber');
    }

    /**
     * @return bool|null
     */
    public function forceDelete()
    {
        if ($this->merchant->isPro()) {
            $this->loyaltyDelete();
        } else {
            $this->punchcardDelete();
        }

        // both punch card and loyalty models
        CustomerSmsLogModel::where('merchant_id', $this->merchantId)->where('mobile_number', $this->mobileNumber)->delete();

        return parent::forceDelete();
    }

    /**
     * @return void|null
     */
    protected function loyaltyDelete()
    {
        // remove customer points
        CustomerPointsModel::where('customerId', $this->customerId)->delete();

        CustomerQrCodesModel::where('QRCode', $this->uuid)->delete();

        // remove all customer transactions
        CustomerTransactionsModel::where('customerId', $this->customerId)->delete();

        $redeemed_items = RedeemModel::select('redeemId')->where('customerId', $this->customerId)->pluck('redeemId');

        // remove all redeemed items
        RedeemedItemsModel::whereIn('redeemId', $redeemed_items)->delete();
        RedeemModel::where('customerId', $this->customerId)->delete();
    }

    /**
     * @return bool|null
     */
    protected function punchcardDelete()
    {
        // remove customer rewards
        CustomerPunchCardRewardModel::where('customer_id', $this->customerId)->delete();

        // remove customer transactions
        CustomerPunchCardTransactionsModel::where('customer_id', $this->customerId)->delete();

        // fetch all customer stamp card
        $stamp_cards = CustomerStampsCardModel::where('customer_id', $this->customerId)->get();
        foreach ($stamp_cards as $card) {
            //  remove customer stamps
            CustomerStamps::where('stamp_card_id', $card->id)->delete();
        }

        CustomerStampsCardModel::where('customer_id', $this->customerId)->delete();
    }

    public function getMobileNumberPrefixAttribute(){

        return substr($this->mobileNumber, 1, 5);

    }

    public function getIsGlobeSubscriberAttribute(){

        $prefix = GlobeNumbersPrefixModel::where('prefix', $this->mobile_number_prefix)->first();

        return ( $prefix ? true : false );

    }

    public function getIsPrepaidGlobeSubscriberAttribute(){

        $prefix = GlobeNumbersPrefixModel::where('prefix', $this->mobile_number_prefix)->where('brand', '!=', 'POST')->first();

        return ( $prefix ? true : false );

    }

    public function getMobileNetworkAttribute(){

        $brand = GlobeNumbersPrefixModel::where('prefix', $this->mobile_number_prefix)->value('brand');

        return $brand;

    }

    // get customer's total transactions count for the day.
    public function getDayEarnTransactionsCountAttribute(){

        $count = CustomerTransactionsModel::where('customerId', $this->customerId)
            ->where('transactionType', 'earn')
            ->whereRaw('DATE(timestamp) = CURDATE()')
            ->count();

        return (int) $count;

    }

    // get customer's total points earned for the day.
    public function getDayPointsEarnedTotalAttribute(){

        $sum = CustomerTransactionsModel::where('customerId', $this->customerId)
            ->where('transactionType', 'earn')
            ->whereRaw('DATE(timestamp) = CURDATE()')
            ->sum('pointsEarned');

        return (float) $sum;

    }

    // get customer's pay points transactions count for today.
    public function getDayPayPointsTransactionsCountAttribute(){

        $count = CustomerTransactionsModel::where('customerId', $this->customerId)
            ->where('transactionType', 'paypoints')
            ->whereRaw('DATE(timestamp) = CURDATE()')
            ->count();

        return (int) $count;

    }

    // get customer's total points paid for today.
    public function getDayPointsPaidTotalAttribute(){

        $sum = CustomerTransactionsModel::where('customerId', $this->customerId)
            ->where('transactionType', 'paypoints')
            ->whereRaw('DATE(timestamp) = CURDATE()')
            ->sum('amountPaidWithpoints');

        return (float) $sum;

    }

    // get customer's redeem transactions count for today.
    public function getDayRedemptionTransactionsCountAttribute(){

        $count = CustomerTransactionsModel::where('customerId', $this->customerId)
            ->where('transactionType', 'redeem')
            ->whereRaw('DATE(timestamp) = CURDATE()')
            ->count();

        return (int) $count;

    }

    public function membership_cards()
    {
       return $this->hasMany('Rush\Modules\Customer\Models\CustomerMembershipCardModel', 'customer_id', 'customerId');
    }

    public function getMembershipCardAttribute()
    {
      return $this->membership_cards()->where('status', 1)->first();
    }

    public function getUnclaimedRedemptionsAttribute(){

        $redemptions = RedeemModel::where('customerId', $this->customerId)->where('status', 0)->get();

        return $redemptions;

    }

    public function getEarnedBadgeIdsAttribute(){

        $badge_ids = [];
        $raw_badge_ids = CustomerBadgeModel::where('customer_id', $this->customerId)->pluck('badge_id');

        foreach( $raw_badge_ids as $id ){
            $badge_ids[] = $id;
        }

        return array_unique($badge_ids);

    }

    public function has_activation_code( $type ){

        $code_count = CustomerActivationCodesModel
            ::where('customer_id', $this->customerId)
            ->where('type', $type)
            ->where('status', 1)
            ->count();

        return $code_count > 0 ? true : false;
    }

    public function syncUpdateCallback()
    {
       $merchant_callback_url = $this->merchant->settings->api_callback_url;
       if($merchant_callback_url) {
           $response = [
                'action' => 'update_customer',
                'customer' => [
                    'id' => $this->uuid,
                    'profile_id' => $this->profile_id,
                    'name' => $this->fullName,
                    'email' => $this->email,
                    'mobile_no' => $this->mobileNumber,
                    'birthdate' => $this->birthDate,
                    'gender' => $this->gender,
                    'qr' => $this->qr,
                    'photo' => $this->profileImage ? $this->profileImage : null,
                    'registration_date' => $this->timestamp
                ]
           ];

           $curlService = new \Ixudra\Curl\CurlService();

           $request = $curlService->to($merchant_callback_url)
                        ->withData($response)
                        ->returnResponseObject()
                        ->post();
       }
    }

    public function getDeliveryDetailAttribute(){

        $delivery_detail = CustomerDeliveryDetailModel::where('customer_id', $this->customerId)->first();

        if( ! $delivery_detail ){

            $delivery_detail = new CustomerDeliveryDetailModel;
            $delivery_detail->customer_id = $this->customerId;
            $delivery_detail->name = '';
            $delivery_detail->mobile_number = '';
            $delivery_detail->email = '';
            $delivery_detail->building = '';
            $delivery_detail->floor = '';
            $delivery_detail->street = '';
            $delivery_detail->landmark = '';
            $delivery_detail->save();

        }

        return $delivery_detail;

    }

    public function getPaymentDetailAttribute(){

        $payment_detail = CustomerPaymentDetailModel::where('customer_id', $this->customerId)->first();

        if( ! $payment_detail ){

            $payment_detail = new CustomerPaymentDetailModel;
            $payment_detail->customer_id = $this->customerId;
            $payment_detail->name = '';
            $payment_detail->mobile_number = '';
            $payment_detail->save();

        }

        return $payment_detail;

    }

    public function setBirthDateAttribute( $value )
    {
        if( $value ) {
            $birthdate = new Carbon($value);
            $this->attributes['birthDate'] = $birthdate->toDateString();
        }
    }

}
