<?php
namespace Rush\Modules\Customer\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerDeliveryDetailModel extends Model
{
    protected $table = "customer_delivery_details";
}
