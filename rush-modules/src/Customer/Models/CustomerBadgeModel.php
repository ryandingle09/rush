<?php

namespace Rush\Modules\Customer\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerBadgeModel extends Model
{
    protected $table = "customer_badges";
}
