<?php

namespace Rush\Modules\Customer\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerActivationCodesModel extends Model
{
    protected $fillable = ['status'];

    protected $table = "customer_activation_codes";
}
