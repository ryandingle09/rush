<?php

namespace Rush\Modules\Customer\Models;

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;
use Logaretm\Transformers\TransformableTrait;
use Logaretm\Transformers\Contracts\Transformable;

/**
 * Class CustomerClassPackagePunchcardModel
 *
 * @property      string                                                                                    $uuid
 * @property-read int                                                                                       $id
 * @property-read null|\Illuminate\Database\Eloquent\Collection|CustomerPunchCardTransactionsModel[]        $claimed_redeem_transactions
 * @property-read CustomerClassPackageModel                                                                 $class_package
 * @property-read null|\Illuminate\Database\Eloquent\Collection|CustomerPunchCardTransactionsModel[]        $redeem_transactions
 * @property-read \Illuminate\Database\Eloquent\Collection|CustomerClassPackagePunchcardStampsModel[]       $stamps
 * @property-read \Illuminate\Database\Eloquent\Collection|CustomerClassPackagePunchcardStampsModel[]       $earned_stamps
 * @property-read null|\Illuminate\Database\Eloquent\Collection|CustomerPunchCardTransactionsModel[]        $transactions
 * @property-read null|\Illuminate\Database\Eloquent\Collection|CustomerPunchCardTransactionsModel[]        $un_claimed_redeem_transactions
 * @property-read null|\Illuminate\Database\Eloquent\Collection|CustomerClassPackagePunchcardStampsModel[]  $un_redeemed_stamps
 */
class CustomerClassPackagePunchcardModel extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'customer_class_packages_punchcards';

    protected $fillable = ['customer_class_package_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Query\Builder
     */
    public function stamps()
    {
        return $this->hasMany(CustomerClassPackagePunchcardStampsModel::class, 'class_package_punchcard_id', 'id');
    }

    public function getEarnedStampsAttribute()
    {
        return $this->stamps()->where('void', 0)->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Query\Builder
     */
    public function class_package()
    {
        return $this->belongsTo(CustomerClassPackageModel::class, 'customer_class_package_id', 'id');
    }

    public function getUnRedeemedStampsAttribute()
    {
        return $this->stamps()->where('redeem', 0)->where('void', 0)->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Query\Builder
     */
    public function transactions()
    {
        return $this->hasMany(CustomerPunchCardTransactionsModel::class, 'class_package_stamp_card_id', 'id');
    }

    public function getRedeemTransactionsAttribute()
    {
        return $this->transactions()->where(['type' => 'redeem', 'void' => 0])->get();
    }

    public function getUnClaimedRedeemTransactionsAttribute()
    {
        return $this->redeem_transactions->filter(function ($transaction) {
            return $transaction->status == 0;
        });
    }

    public function getClaimedRedeemTransactionsAttribute()
    {
        return $this->redeem_transactions->filter(function ($transaction) {
            return $transaction->status == 1;
        });
    }

    public function getUuidAttribute($value)
    {
        if (!$value) {
            $name = 'customer_class_packages_punchcards' . '-' . $this->id;
            $value = Uuid::uuid3(Uuid::NAMESPACE_DNS, $name);
            $this->uuid = $value;
            $this->save();
        }

        return $value;
    }
}
