<?php

namespace Rush\Modules\Customer\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerSmsLogModel extends Model
{

    protected $table = "notification_sms_logs";
    use SoftDeletes;
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
     
    public function customer()
    {
        return $this->belongsTo('Rush\Modules\Customer\Models\CustomerModel', 'mobile_number', 'mobileNumber');
    }
}
