<?php

namespace Rush\Modules\Customer\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerSupportModel extends Model
{
    protected $table = "lara_customer_support";
    protected $primaryKey = "id";

    public function customer()
    {
        return $this->belongsTo('Rush\Modules\Customer\Models\CustomerModel', 'customer_id', 'customerId');
    }

    public function category()
    {
        return $this->hasOne('Rush\Modules\Merchant\Models\MerchantFeedbackCategoryModel', 'id', 'category_id');
    }

    public function getClassAttribute()
    {
    	return ( $this->type == "text" ) ? 'fa-file-o' : 'fa-play';
    }
}
