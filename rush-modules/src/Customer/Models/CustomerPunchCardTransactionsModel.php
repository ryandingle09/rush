<?php

namespace Rush\Modules\Customer\Models;

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Rush\Modules\Customer\Models\CustomerModel;
use Rush\Modules\Customer\Models\CustomerStamps;
use Rush\Modules\Merchant\Models\VoucherCodeModel;
use Rush\Modules\Merchant\Models\MerchantEmployeesModel;
use Rush\Modules\ClassManagement\Models\PackageStampModel;
use Rush\Modules\Customer\Models\CustomerClassPackageModel;
use Rush\Modules\Merchant\Models\MerchantPunchCardRewardsModel;
use Rush\Modules\Customer\Models\CustomerClassPackagePunchcardStampsModel;

/**
 * Class CustomerPunchCardTransactionsModel
 *
 * @property        string                                                 $transaction_ref
 * @property        string                                                 $uuid
 * @property        string                                                 $remarks
 * @property        integer                                                $void
 * @property        integer                                                $status
 * @property        integer                                                $reward_id
 * @property-read   CustomerModel                                          $customer
 * @property-read   MerchantEmployeesModel                                 $employee
 * @property-read   CustomerClassPackageModel                              $class_package
 * @property-read   CustomerClassPackagePunchcardModel                     $class_package_punchcard
 * @property-read   PackageStampModel                                      $class_package_reward
 * @property        string|\Carbon\Carbon                                  $date_punched
 * @property-read   CustomerClassPackagePunchcardStampsModel[]|Collection  $class_package_stamps
 */
class CustomerPunchCardTransactionsModel extends Model
{
    protected $table = "CustomerPunchCardTransactions";
    protected $primaryKey = "id";
    protected $dates = ['date_punched'];

    public function reward(){

        return $this->hasOne('Rush\Modules\Merchant\Models\MerchantPunchCardRewardsModel', 'id', 'reward_id');

    }

    public function customer_stamps(){

        return $this->hasMany('Rush\Modules\Customer\Models\CustomerStamps', 'transaction_id', 'id');

    }

    public function employee(){
        return $this->belongsTo('Rush\Modules\Merchant\Models\MerchantEmployeesModel', 'employee_id', 'userId');
    }

    public function customer()
    {
        return $this->belongsTo('Rush\Modules\Customer\Models\CustomerModel', 'customer_id', 'customerId');
    }

    public function branch()
    {
        return $this->belongsTo('Rush\Modules\Merchant\Models\MerchantBranchesModel', 'branch_id');
    }

    public static function addTransaction($employee, $card, $amount = null, $reward_id = null, $status = false, $type = 'earn', $channel = null, $reference = null, $stamps = 0)
    {
        $channel = ( ! is_null($channel) ? strtolower($channel) : 'merchantapp' );

        $reference = $reference ? $reference : time();
        $transaction = new self;
        $transaction->customer_id = $card->customer->customerId;
        $transaction->employee_id = $employee ? $employee->userId : null;
        $transaction->merchant_id = $card->customer->merchant->merchantid;
        $transaction->date_punched = date('Y-m-d G:i:s', time());
        $transaction->promo_id = $card->punchcard->id;
        $transaction->amount = $amount;
        $transaction->reward_id = $reward_id;
        $transaction->transaction_ref = $reference;
        $transaction->stamp_card_id = $card->id;
        $transaction->void = 0;
        $transaction->status = $status;
        $transaction->type = $type;
        $transaction->channel = $channel;
        $transaction->stamps = $stamps;
        $transaction->branch_id = $employee ? $employee->branch->id : null;
        $transaction->save();

        return $transaction;
    }

    public function void(){
        switch ($this->type){
            case 'purchase-package':
                if($this->class_package){
                    $this->class_package->delete();
                }

                $this->type = 'void-purchase-package';
                $this->amount = -$this->amount;
                $this->save();

                break;

        }
    }

    public function voidStamp( $card )
    {
        $current_stamps = count($card->stamps);
        // 1st: void those stamps
        foreach ($this->customer_stamps as $stamp) {
            $stamp->void = 1;
            $current_stamps--;
            $stamp->save();
        }

        $this->void = 1;
        $this->save();

        // void redeemed items
        foreach ($card->rewards as $redeem) {
            // customer has redeemed this reward
            if ( $redeem->reward->no_stamps > $current_stamps ) {
                    // void the redeemed reward
                    $redeem->void = 1;
                    $redeem->save();
                    // $card->customer->sms->voidRedeemedReward($redeem);
            }
        }

        // $card->customer->sms->voidStamps($this);

        return $this->void;
    }

    public function getUuidAttribute($value)
    {
        $uuid = $value;
        if (!$uuid) {
            $uuid = Uuid::uuid1()->toString();
            $this->uuid = $uuid;
            $this->save();
        }

        return $uuid;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Query\Builder
     */
    public function class_package()
    {
        return $this->hasOne(CustomerClassPackageModel::class, 'id', 'class_package_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Query\Builder
     */
    public function class_package_stamps()
    {
        return $this->hasMany(CustomerClassPackagePunchcardStampsModel::class, 'transaction_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Query\Builder
     */
    public function class_package_punchcard()
    {
        return $this->hasOne(CustomerClassPackagePunchcardModel::class, 'id', 'class_package_stamp_card_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Query\Builder
     */
    public function class_package_reward()
    {
        return $this->hasOne(PackageStampModel::class, 'id', 'reward_id');
    }

    public function promo_punchcard()
    {
        return $this->belongsTo('Rush\Modules\Merchant\Models\MerchantPromoMechanicsModel', 'promo_id', 'id');
    }

    public function voucher_code()
    {
        return $this->hasOne(VoucherCodeModel::class, 'id', 'voucher_code_id');
    }
}
