<?php
namespace Rush\Modules\Customer\Models;

use Illuminate\Database\Eloquent\Model;

class NonMemberTransactionModel extends Model
{
    protected $table = "NonMemberTransaction";

    // get non member's total transactions count for the day.
    public static function getDayEarnTransactionsCount( $mobile_no, $merchant_id )
    {
        $count = self::where('mobileNumber', $mobile_no)
            ->where('merchant_id', $merchant_id)
            ->where('status', 0)
            ->where('transactionType', 'earn')
            ->whereRaw('DATE(timestamp) = CURDATE()')
            ->count();

        return (int) $count;
    }

    // get non member's total points earned for the day.
    public static function getDayPointsEarnedTotal( $mobile_no, $merchant_id )
    {
        $sum = self::where('mobileNumber', $mobile_no)
            ->where('merchant_id', $merchant_id)
            ->where('status', 0)
            ->whereRaw('DATE(timestamp) = CURDATE()')
            ->sum('pointsEarned');

        return (float) $sum;
    }

    public static function earnPoints( $employee, $data, $channel = null )
    {
        $points = (float) $employee->merchant->getEarningPointsOfAmount( $data['amount'] );

        $transaction = new self();
        $transaction->mobileNumber = $data['mobile_no'];
        $transaction->transactionReferenceCode = str_random(12);
        $transaction->branchId = $employee->branchId;
        $transaction->transactionType = 'earn';
        $transaction->conversionSettingsId = $employee->merchant->conversion_settings->id;
        $transaction->employee_id = $employee->userId;
        $transaction->userId = $employee->userId;
        $transaction->pointsEarned = $points;
        $transaction->amountPaidWithCash = $data['amount'];
        $transaction->transactionStatus = 'done';
        $transaction->receiptReferenceNumber = $data['or_no'];
        $transaction->timestamp = date("Y-m-d H:i:s", time());
        $transaction->merchant_id = $employee->merchant->merchantid;
        $transaction->channel = ( ! is_null($channel) ? strtolower($channel) : 'merchantapp' );
        $transaction->status = 0;
        $transaction->save();

        return $transaction;
    }

    public function branch()
    {
        return $this->belongsTo('Rush\Modules\Merchant\Models\MerchantBranchesModel', 'branchId', 'id');
    }

    public function employee(){
        return $this->hasOne('Rush\Modules\Merchant\Models\MerchantEmployeesModel', 'userId', 'employee_id');
    }
}