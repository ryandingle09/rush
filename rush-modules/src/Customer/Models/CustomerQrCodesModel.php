<?php

namespace Rush\Modules\Customer\Models;

// Libraries
use Endroid\QrCode\QrCode;
use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManager;
use Rush\Modules\Helpers\Facades\StorageHelper;

// Traits
use Rush\Modules\Customer\Models\Traits\Qr\AttributesTrait;
use Rush\Modules\Customer\Models\Traits\Qr\RelationshipsTrait;

class CustomerQrCodesModel extends Model
{
    use AttributesTrait, RelationshipsTrait;

    /**
     * @var string
     */
    protected $table = "QRCode";

    /**
     * @var string
     */
    protected $primaryKey = 'qrid';

    /**
     * @var boolean
     */
    public $timestamps = false;

    public $disk;

    public $imageManager;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->disk = StorageHelper::getRepositoryDisk();
        $this->imageManager = new ImageManager(array('driver' => 'gd'));
    }

    public function generate()
    {
        $qr = new QrCode();
        $qr->setText($this->customer->uuid)
            ->setSize(300)
            ->setImageType(QrCode::IMAGE_TYPE_PNG);
        $image = $this->imageManager->make($qr->getImage());
        $path = "/customers/qrcode/" . $this->customer->uuid . ".png";
        $this->disk->put($path, $image->stream()->__toString(), 'public');
        $image_path = "/repository". $path;
        $this->qrImageURL = $image_path;
        $this->save();
        return $image_path;
    }

    protected function qrExists()
    {
        $path = "/customers/qrcode/" . $this->customer->uuid . ".png";

        return $this->disk->exists($path);
    }
}
