<?php

namespace Rush\Modules\Customer\Models;

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;
use Rush\Modules\Merchant\Models\MerchantBranchesModel;
use Rush\Modules\Customer\Models\CustomerPunchCardTransactionsModel;

/**
 * Class CustomerClassPackagePunchcardStampsModel
 *
 * @property-read integer                                        $id
 * @property      string                                         $uuid
 * @property-read MerchantBranchesModel                          $branch
 * @property-read CustomerPunchCardTransactionsModel             $transaction
 * @property      \Carbon\Carbon                                 $created_at
 */
class CustomerClassPackagePunchcardStampsModel extends Model
{
    protected $table = 'customer_class_packages_stamps';

    protected $fillable = ['class_package_punchcard_id', 'status'];

    public function branch()
    {
        return $this->belongsTo(MerchantBranchesModel::class, 'branch_id', 'id');
    }

    public function transaction()
    {
        return $this->belongsTo(CustomerPunchCardTransactionsModel::class, 'transaction_id', 'id');
    }

    public function getUuidAttribute($value)
    {
        if (!$value) {
            $value = Uuid::uuid1()->toString();
            $this->uuid = $value;
            $this->save();
        }

        return $value;
    }
}