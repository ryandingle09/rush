<?php

namespace Rush\Modules\Customer\Models;

use Illuminate\Database\Eloquent\Model;
use RushModulesHelper;

class NonMemberTransaction extends Model
{
    protected $table = 'NonMemberTransaction';

    public static function getCurrentPoints($mobile_no)
    {
       return self::where('mobileNumber', $mobile_no)->sum('pointsEarned');
    }
}
