<?php

namespace Rush\Modules\Customer\Models;

use Illuminate\Database\Eloquent\Model;

class RedeemedItemsModel extends Model
{
    protected $table = "redeemedItems";
}
