<?php
namespace Rush\Modules\Customer\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Rush\Modules\Customer\Models\CustomerTransactionsModel
 *
 * @property string $transactionReferenceCode
 * @property string $timestamp
 * @mixin \Eloquent
 */
class CustomerTransactionsModel extends Model
{
    protected $table = "CustomerTransaction";
    protected $primaryKey = "id";
    
    public function getRewardAttribute()
    {
        $redeem = RedeemModel::where('redeemReferenceCode', $this->transactionReferenceCode)->first();
        if ($redeem) {
            return $redeem->reward;
        }

        return null;
    }

    public function redeemedItem()
    {
        return $this->hasOne('Rush\Modules\Customer\Models\RedeemModel','redeemReferenceCode', 'transactionReferenceCode');
    }
    
    public function customer()
    {
        return $this->belongsTo('Rush\Modules\Customer\Models\CustomerModel', 'customerId', 'customerId');
    }

    public function branch()
    {
        return $this->belongsTo('Rush\Modules\Merchant\Models\MerchantBranchesModel', 'branchId');
    }

    public function void()
    {
        $transaction = new self();
        $transaction->branchId = $this->branchId;
        $transaction->transactionReferenceCode = $this->transactionReferenceCode;
        $transaction->conversionSettingsId = $this->conversionSettingsId;
        $transaction->employee_id = $this->employee_id;
        $transaction->customerId = $this->customerId;
        $transaction->userId = $this->employee_id;
        $transaction->pointsEarned = $this->pointsEarned;
        $transaction->voided_points = $this->pointsEarned;
        $transaction->pointsRedeem = $this->pointsRedeem;
        $transaction->pointsBurn = $this->pointsBurn;
        $transaction->pointsRedeem = $this->pointsRedeem;
        $transaction->amountPaidWithpoints = $this->amountPaidWithpoints;
        $transaction->voided_amount = $this->amountPaidWithCash;
        $transaction->amountPaidWithCash = $this->amountPaidWithCash;
        $transaction->transactionStatus = 'done';
        $transaction->receiptReferenceNumber = $this->receiptReferenceNumber;
        $transaction->timestamp = date("Y-m-d H:i:s", time());
        $transaction->merchant_id = $this->merchant_id;
        $transaction->channel = $this->channel;
        $transaction->card_id = $this->card_id;

        $points = $this->customer->points;
        switch ($this->transactionType) {
            case 'earn':
                $points->currentpoints = $this->customer->points->currentpoints - $this->pointsEarned;
                $transaction->transactionType = 'void-earn';
                $transaction->save();
                $points->save();
                $this->related_transactions_count++;
                $this->save();
                break;
            case 'redeem':
                $points->currentpoints = $this->customer->points->currentpoints + $this->pointsRedeem;
                $transaction->transactionType = 'void-redeem';
                $transaction->save();
                $points->save();
                break;
            case 'paypoints':
                $points->currentpoints = $this->customer->points->currentpoints + $this->pointsBurn;
                $transaction->transactionType = 'void-paypoints';
                $transaction->save();
                $points->save();
                break;

            case 'purchase-package':
                $points->currentpoints = $this->customer->points->currentpoints + $this->pointsBurn;
                $this->transactionType = 'void-purchase-package';
                $this->transactionReferenceCode = 'V:' . $this->transactionReferenceCode . ":" . time();
                $this->receiptReferenceNumber = 'V:' . $this->receiptReferenceNumber . ":" . time();
                $this->class_package()->withTrashed()->delete();
                $this->save();
                $points->save();
                break;
        }

        return $transaction;
    }
    
    public function employee(){
        return $this->hasOne('Rush\Modules\Merchant\Models\MerchantEmployeesModel', 'userId', 'employee_id');
    }

    public function redeem(){

        return $this->hasOne('Rush\Modules\Customer\Models\RedeemModel', 'redeemReferenceCode', 'transactionReferenceCode');

    }

    public function class_package(){

        return $this->belongsTo('Rush\Modules\Customer\Models\CustomerClassPackageModel', 'class_package_id');

    }

}
