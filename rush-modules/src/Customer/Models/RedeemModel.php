<?php

namespace Rush\Modules\Customer\Models;

use Illuminate\Database\Eloquent\Model;
use Rush\Modules\Merchant\Models\MerchantRewardsModel;
use Rush\Modules\Customer\Models\RedeemedItemsModel;
use UUID;

class RedeemModel extends Model
{
    protected $table = "Redeem";
    protected $primaryKey = "redeemId";
    
    public function getRewardAttribute()
    {
        $reward_item = RedeemedItemsModel::where('redeemId', $this->redeemId)->first();
        return MerchantRewardsModel::where('redeemItemId', $reward_item->redeemItemId)->first()->data;
    }

    public function getUuidAttribute($value)
    {
        if(! $value ) {
            $value = UUID::generate();
            $this->uuid = $value;
            $this->save();
        }
        return $value;
    }

    public function transaction()
    {
        return $this->belongsTo('Rush\Modules\Customer\Models\CustomerTransactionsModel', 'redeemReferenceCode', 'transactionReferenceCode');
    }
    
}
