<?php
namespace Rush\Modules\Customer\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerMembershipCardModel extends Model
{
    protected $table = "customer_cards";

    public function membership()
    {
        return $this->hasOne('Rush\Modules\Merchant\Models\MembershipModel', 'id', 'membership_id');
    }
}
