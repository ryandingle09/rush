<?php

namespace Rush\Modules\Customer\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CustomerPointsModel
 *
 * @property integer $customerpointsId
 * @property integer $customerId
 * @property float $totalPoints
 * @property float $current
 * @property float $currentpoints
 * @property float $usedPoints
 * @property string $status Active, Inactive
 * @property string $timestamp
 * @property \App\Models\Customer $customer
 */
class CustomerPointsModel extends Model
{

    protected $table = "customerPoints";
    protected $primaryKey = "customerpointsId";

    public function customer()
    {
        return $this->belongsTo('Rush\Modules\Customer\Models\CustomerModel', 'customerId', 'customerId');
    }
}
