<?php

namespace Rush\Modules\Customer\Models\Traits\CustomerClassPackages;

use Ramsey\Uuid\Uuid;
use Rush\Modules\Customer\Models\CustomerClassPackagePunchcardModel;

trait AttributesTrait
{
    public function getCardAttribute()
    {
        return CustomerClassPackagePunchcardModel::firstOrCreate(['customer_class_package_id' => $this->id]);
    }

    public function getUuidAttribute($value)
    {
        if (!$value) {
            $name = 'customer_class_packages' . '-' . $this->customer->merchant->id . '-' . $this->id;
            $value = Uuid::uuid3(Uuid::NAMESPACE_DNS, $name);
            $this->uuid = $value;
            $this->save();
        }

        return $value;
    }

    public function getBranchIdsAttribute($value)
    {
        if ($value) {
            return explode(',', $value);
        }

        return null;
    }
}