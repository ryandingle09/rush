<?php

namespace Rush\Modules\Customer\Models\Traits\CustomerClassPackages;

use Rush\Modules\Customer\Models\CustomerModel;
use Rush\Modules\ClassManagement\Models\PackageModel;
use Rush\Modules\Customer\Models\CustomerPunchCardTransactionsModel;

trait RelationshipsTrait
{
    public function package()
    {
        return $this->hasOne(PackageModel::class, 'id', 'class_package_id');
    }

    public function customer()
    {
        return $this->belongsTo(CustomerModel::class, 'customer_id', 'customerId');
    }

    public function transaction()
    {
       return $this->belongsTo(CustomerPunchCardTransactionsModel::class, 'id', 'class_package_id');
    }
}
