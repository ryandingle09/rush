<?php

namespace Rush\Modules\Customer\Models\Traits\Qr;

use Rush\Modules\Helpers\Facades\StorageHelper;

trait AttributesTrait
{
    public function getImageUrlAttribute()
    {
        if ($this->qrExists()) {
            return $this->qrImageURL;
        } else {
            $this->qrImageURL = $this->generate();
            $this->save();

            return $this->qrImageURL;
        }
    }

    public function setImageUrlAttribute($value)
    {
        $this->attributes['qrImageURL'] = $value;
    }

    public function getImageUriAttribute()
    {
        $image_url = StorageHelper::rebasePathToRepository($this->image_url);
        return base64_encode($this->disk->get($image_url));
    }
}
