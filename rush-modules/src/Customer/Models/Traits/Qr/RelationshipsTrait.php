<?php

namespace Rush\Modules\Customer\Models\Traits\Qr;

use Rush\Modules\Customer\Models\CustomerModel;

trait RelationshipsTrait {

    public function customer()
    {
        return $this->belongsTo(CustomerModel::class, 'QRCode', 'uuid');
    }
}
