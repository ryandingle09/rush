<?php

namespace Rush\Modules\Customer\Models\Traits\Customer;

use Carbon\Carbon;
use Intervention\Image\ImageManager;
use Rush\Modules\Achievements\Models\AchievementsLogsModel;
use Rush\Modules\Achievements\Models\AchievementsModel;
use Rush\Modules\Helpers\Facades\StorageHelper;

trait AttributesTrait
{
    /** @var \Rush\Modules\Customer\Models\CustomerModel $this */

    public function getIdAttribute()
    {
        return $this->customerId;
    }

    public function setIdAttribute($value)
    {
        $this->attributes['customerId'] = $value;
    }

    public function getNameAttribute()
    {
        return $this->fullName;
    }

    public function setNameAttribute($value)
    {
        $this->attributes['fullName'] = $value;
    }

    public function getMerchantId()
    {
        return $this->merchantId;
    }

    public function setMerchantId($value)
    {
        $this->attributes['merchantId'] = $value;
    }

    public function getMobileNoAttribute()
    {
        return $this->mobileNumber;
    }

    public function setMobileNoAttribute($value)
    {
        $this->attributes['mobileNumber'] = $value;
    }

    public function getBirthdayAttribute()
    {
        $value = $this->birthDate;
        if ($value) {
            $temp = new Carbon($value);
            $value = $temp->format('m/d/Y');
        }

        return $value;
    }

    public function setBirthdayAttribute($value)
    {
        if ($value) {
            $temp = new carbon($value);
            $value = $temp->format('m/d/Y');
        }
        $this->attributes['birthDate'] = $value;
    }

    public function getGenderAttribute($value)
    {
        switch ($value) {
            case "m":
                $value = "Male";
                break;
            case "f":
                $value = "Female";
                break;
            default:
                $value = null;
                break;
        }

        return $value;
    }

    public function setGenderAttribute($value)
    {
        switch ($value) {
            case "m":
                $value = "Male";
                break;
            case "f":
                $value = "Female";
                break;
            default:
                $value = null;
                break;
        }
        $this->attributes['gender'] = $value;
    }

    public function getMpinAttribute()
    {
        return $this->PIN;
    }

    public function setMpinAttribute($value)
    {
        $this->attributes['PIN'] = $value;
    }

    public function getPhotoAttribute()
    {
        if ($this->profileImage) {
            $file = 'photos/'. $this->uuid . '/' . basename($this->profileImage);
            $path = public_path($file);
            $repository_path = 'customers/photos/' . $this->uuid . '/'. basename($this->profileImage);
            $disk = StorageHelper::getRepositoryDisk();
            if (file_exists($path) && (! $disk->exists($repository_path))) {
                $image_manager = new ImageManager(array('driver' => 'gd'));
                $disk->put($repository_path, $image_manager->make($path)->stream()->__toString(), 'public');
                unlink($path);
                $this->profileImage = StorageHelper::repositoryUrl($repository_path);
                $this->save();
            }
        }

        return $this->profileImage;
    }

    public function setPhotoAttribute($data_uri)
    {
        $disk = StorageHelper::getRepositoryDisk();
        $image_manager = new ImageManager(array('driver' => 'gd'));
        $data_uri = str_replace(" ", "+", $data_uri); // base64 image data
        $repository_path = '/customers/photos/' . $this->uuid . '/'. md5(time()) . ".png";
        $image = $image_manager->make($data_uri)->fit(200)->encode('png')->stream();
        $disk->put($repository_path, $image->stream()->__toString(), 'public');
        $this->attributes['profileImage'] = '/repository' . $repository_path;
    }

    public function getPhotoUriAttribute()
    {
        $disk = StorageHelper::getRepositoryDisk();
        $image_url = StorageHelper::rebasePathToRepository($this->profileImage);

        return base64_encode($disk->get($image_url));
    }


    public function getAchievementsAttribute()
    {
        $logs = AchievementsLogsModel::where('customer_id', $this->id)->get()->pluck('achievement_unlock_id');
        $achievements = AchievementsModel::whereIn('id', $logs->toArray())->get();

        return $achievements;
    }

    public function getMembershipStatusAttribute($value)
    {
        return (bool) $value;
    }

    public function setMembershipStatusAttribute($value)
    {
       $this->attributes['membership_status'] = (int) $value;
    }
}
