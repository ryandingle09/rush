<?php

namespace Rush\Modules\Customer\Models\Traits\Customer;

use Rush\Modules\Merchant\Models\MerchantVotesModel;
use Rush\Modules\Customer\Models\CustomerActivationCodesModel;
use Rush\Modules\Customer\Models\CustomerQrCodesModel;
use Rush\Modules\Customer\Models\CustomerClassPackageModel;

trait RelationshipsTrait
{
    public function qr()
    {
        return $this->hasOne(CustomerQrCodesModel::class, 'QrCode', 'uuid');
    }

    public function class_packages()
    {
        return $this->hasMany(CustomerClassPackageModel::class, 'customer_id', 'customerId')->with('package');
    }

    public function reservations()
    {
    	return $this->hasMany('Rush\Modules\ClassManagement\Models\ClassScheduleReservationModel', 'customer_id', 'customerId');
    }

    public function activations_codes()
    {
        return $this->hasMany(CustomerActivationCodesModel::class, 'customer_id', 'customerId');
    }
    
    public function votes()
    {
        return $this->hasMany(MerchantVotesModel::class, 'customer_id', 'customerId');
    }
}
