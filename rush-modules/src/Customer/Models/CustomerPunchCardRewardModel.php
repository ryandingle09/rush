<?php

namespace Rush\Modules\Customer\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerPunchCardRewardModel extends Model
{
    protected $table = "CustomerPunchCardReward";

    public function customer()
    {
        return $this->belongsTo('Rush\Modules\Customer\Models\CustomerModel', 'customer_id', 'customerId');
    }
}
