<?php

namespace Rush\Modules\Customer\Models\Transformers;

use Logaretm\Transformers\Transformer;
use Rush\Modules\Customer\Models\CustomerClassPackageModel;

class CustomerClassPackageTransformer extends Transformer
{
    /**
     * @param  CustomerClassPackageModel $class_package
     * @return mixed
     */
    public function getTransformation($class_package)
    {
        return [
            'id'  => (int) $class_package->id,
            'name' => $class_package->package->name,
            'no_of_visits' => $class_package->package->no_of_visits,
            'image_url' => $class_package->package->image_path
        ];
    }
}
