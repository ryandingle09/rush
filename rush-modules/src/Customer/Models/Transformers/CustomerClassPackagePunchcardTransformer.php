<?php

namespace Rush\Modules\Customer\Models\Transformers;

use Logaretm\Transformers\Transformer;
use Rush\Modules\Helpers\Facades\StorageHelper;
use Rush\Modules\Customer\Services\CustomerService;
use Rush\Modules\Customer\Models\CustomerClassPackagePunchcardModel;

class CustomerClassPackagePunchcardTransformer extends Transformer
{
    /**
     * @var CustomerService
     */
    protected $customerService;

    public function __construct(CustomerService $customerService)
    {
        $this->customerService = $customerService;
    }

    /**
     * @param  CustomerClassPackagePunchcardModel $punchcard
     * @return mixed
     */
    public function getTransformation($punchcard)
    {
        $duration_end = $this->customerService->getClassPackageExpiration($punchcard->class_package->customer_id, $punchcard->class_package->id);
        $redemption_end = $this->customerService->getClassPackageRedemptionExpiration($punchcard->class_package->customer_id, $punchcard->class_package->id);
        $package_rewards_collection = $punchcard->class_package->package->stamps;
        $package_rewards = $package_rewards_collection->map(function ($reward) {
            /** @var \Rush\Modules\ClassManagement\Models\PackageStampModel $reward */
            return [
                    'id' => $reward->uuid,
                    'name' => $reward->name,
                    'details' => $reward->details,
                    'image_url' => config('app.url') . $reward->image_path,
                    'stamps' => $reward->stamp_no,
                ];
        })->sortBy(function ($reward) {
            return $reward['stamps'];
        })->values();

        $card_rewards = $punchcard->redeem_transactions->map(function ($transaction) {
            /** @var \Rush\Modules\Customer\Models\CustomerPunchcardTransactionsModel $transaction */
            return [
                'redeem_id' => $transaction->uuid,
                'reward_id' => $transaction->class_package_reward->uuid,
                'reward_name' => $transaction->class_package_reward->name,
                'reward_image_url' => $transaction->class_package_reward->image_path,
                'stamps' => (int) $transaction->class_package_reward->stamp_no,
                'status' => (int) $transaction->status,
                'date' => $transaction->date_punched->toDateTimeString()
            ];
        });

        $activities = $punchcard->earned_stamps->map(function ($stamp) {
            /** @var  \Rush\Modules\Customer\Models\CustomerClassPackagePunchcardStampsModel $stamp */
            if ($stamp->branch) {
                $item = [
                    'id' => $stamp->branch->uuid,
                    'name' => $stamp->branch->name,
                    'logo_url' => StorageHelper::repositoryUrlForView($stamp->branch->logo_url),
                    'qr_code' => StorageHelper::repositoryUrlForView($stamp->branch->qr_code)
                ];
            } else {
                $item = [
                    'id' => '',
                    'name' => '',
                    'logo_url' => $stamp->transaction->customer->merchant->merchant_design->stamp->url,
                    'qr_code' => ''
                ];
            }
            return $item;
        })->toArray();

        return [
            'id'  => $punchcard->uuid,
            'package' => [
                'id' => $punchcard->class_package->uuid,
                'name' => $punchcard->class_package->package->name,
                'stamps'=> $punchcard->class_package->package->no_of_visits,
                'duration' => [
                    'start' => $punchcard->class_package->created_at->format('Y-m-d'),
                    'end' => $punchcard->class_package->end_date->format('Y-m-d')
                ],
                'redemption' => [
                    'start' => $punchcard->class_package->created_at->format('Y-m-d'),
                    'end' =>  $redemption_end->format('Y-m-d')
                ],
                'rewards' => $package_rewards
            ],
            'rewards' => $card_rewards,
            'stamps' => $punchcard->earned_stamps->count(),
            'stamps_object' => $punchcard->earned_stamps->map(function ($stamp) {
                                /** @var \Rush\Modules\Customer\Models\CustomerClassPackagePunchcardStampsModel $stamp */
                                $package = $stamp->transaction->class_package_punchcard->class_package->package;
                                return [
                                    'id' => $stamp->uuid,
                                    'datetime_issued' => $stamp->created_at->toDateTimeString(),
                                    'remarks' => (string) $stamp->transaction->remarks,
                                    'image_url' => $package->image_path
                                ];
                            }),
            'activities' => $activities
        ];
    }
}
