<?php

namespace Rush\Modules\Customer\Models\Transformers;

use Logaretm\Transformers\Transformer;
use Rush\Modules\Helpers\Facades\StorageHelper;
use Rush\Modules\Customer\Models\CustomerModel;
use Rush\Modules\Customer\Models\CustomerQrCodesModel;

class CustomerTransformer extends Transformer
{
    /**
     * @param  CustomerModel $customer
     * @return mixed
     */
    public function getTransformation($customer)
    {
        $qr = $customer->qr;
        if (is_null($qr)) {
            $qr = new CustomerQrCodesModel;
            $qr->QRCode = $customer->uuid;
            $qr->save();
            $qr->generate();
        }

        $delivery_detail = $customer->delivery_detail;
        $payment_detail = $customer->payment_detail;

        return [
            'id'  => $customer->uuid,
            'uuid'  => $customer->uuid,
            'profile_id' => $customer->profile_id,
            'name' => $customer->name,
            'email' => $customer->email,
            'mobile_no' => $customer->mobile_no,
            'birthdate' => $customer->birthday,
            'gender' => $customer->gender,
            'qr' => $qr ? StorageHelper::repositoryUrlForView($qr->image_url, 1) : null,
            'qr_base64' => $qr ? $qr->image_uri : null,
            'photo' => $customer->photo ? StorageHelper::repositoryUrlForView($customer->photo, 1) : null,
            'photo_base64' => $customer->photo ? $customer->photo_uri : null,
            'fb_id' => $customer->fbId,
            'email_verified' => $customer->email_verified ? true : false,
            'mobile_verified' => $customer->mobile_verified ? true : false,
            'registration_date' => $customer->timestamp,
            'client_id' => $customer->client_code,
            'membership_status' => $customer->membership_status,
            'grade_level' => $customer->grade_level,
            'customer_account_code_sent' => $customer->has_activation_code('customer_account'),
            'first_name' => $customer->firstName,
            'last_name' => $customer->lastName,
            'address' => $customer->address,
            'delivery_detail' => [
                'name' => $delivery_detail->name,
                'mobile_number' => $delivery_detail->mobile_number,
                'email' => $delivery_detail->email,
                'building' => $delivery_detail->building,
                'floor' => $delivery_detail->floor,
                'street' => $delivery_detail->street,
                'landmark' => $delivery_detail->landmark,
            ],
            'payment_detail' => [
                'name' => $payment_detail->name,
                'mobile_number' => $payment_detail->mobile_number,
            ],
        ];
    }
}
