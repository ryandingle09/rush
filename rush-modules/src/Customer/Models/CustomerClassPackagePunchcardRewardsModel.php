<?php

namespace Rush\Modules\Customer\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerClassPackageRewardsModel extends Model
{
    protected $table = 'customer_class_packages_rewards';
}
