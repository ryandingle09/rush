<?php

namespace Rush\Modules\Customer\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerStamps extends Model
{
    protected $table = "CustomerStamps";
}
