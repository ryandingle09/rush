<?php

namespace Rush\Modules\Customer\Models;

use Illuminate\Database\Eloquent\Model;

class GlobeNumbersPrefixModel extends Model
{
    protected $table = "GlobeNumbersPrefix";
}
