<?php

namespace Rush\Modules\Customer\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerStampsCardModel extends Model
{
    protected $table = "CustomerStampsCard";

    public function customer()
    {
        return $this->belongsTo('Rush\Modules\Customer\Models\CustomerModel', 'customer_id', 'customerId');
    }

    public static function getCard( $customer )
    {
        $merchant = $customer->merchant;
        $promo = $merchant->promo;
        if ($promo) {
            if ($promo->expired && !$customer->merchant->settings->display_live_punchcard_only) {
                // check if customer has expired stamp card
                $card = self::where('customer_id', $customer->id)->where('punchcard_id', $promo->expired->id)->orderBy('id', 'desc')->first();
                if ($card) {
                    // check if customer has stamps that need to redeem
                    if (count($card->stamps) > 0) {
                        if ((count($card->punchcard->rewards) != 0) && (count($card->punchcard->rewards) > count($card->claimed_rewards))) {
                            return $card;
                        }
                    }
                }
            }
            if ($promo->ongoing) {
                // check if customer already has stamps from ongoing promo
                $card = self::where('customer_id', $customer->id)->where('punchcard_id', $promo->ongoing->id)->orderBy('id', 'desc')->first();
                if ($card) {
                    // check if customer have all the stamps and max out the redeem
                    if ((count($card->stamps) == $promo->ongoing->num_stamps)) {
                        $renew = false;
                        if (count($promo->ongoing->rewards) == 0 && $promo->ongoing->max_stamp_card == 0) {
                            $renew = true;
                        }
                        if (count($promo->ongoing->rewards) &&
                            count($promo->ongoing->rewards) == count($card->claimed_rewards) &&
                            $promo->ongoing->max_stamp_card == 0) {
                            $renew = true;
                        }

                        if ($promo->ongoing->max_stamp_card !== 0) {
                            $customer_current_no_stampcards = self::where('customer_id', $customer->id)
                                ->where('punchcard_id', $promo->ongoing->id)->count();
                            if ($customer_current_no_stampcards < $promo->ongoing->max_stamp_card) {
                                $renew = true;
                            }
                        }

                        if ($renew) {
                            $card = new self;
                            $card->customer_id = $customer->id;
                            $card->punchcard_id = $promo->ongoing->id;
                            $card->save();
                            return $card;
                        } else {
                            return $card;
                        }
                    } else {
                        return $card;
                    }
                } else {
                    // if customer don't have card yet
                    // give new card
                    $card = new self;
                    $card->customer_id = $customer->id;
                    $card->punchcard_id = $promo->ongoing->id;
                    $card->save();

                    return $card;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function getStampsAttribute()
    {
        return CustomerStamps::where('stamp_card_id', $this->id)->where('void', 0)->get();
    }

    public function getStampsEarnedTodayAttribute()
    {
        return CustomerStamps
            ::where('stamp_card_id', $this->id)
            ->where('void', 0)
            ->whereRaw('DATE(created_at) = CURDATE()')
            ->get();
    }

    public function punchcard()
    {
        return $this->hasOne('Rush\Modules\Merchant\Models\MerchantPromoMechanicsModel', 'id', 'punchcard_id');
    }

    public function getRewards($claimed = null)
    {
        $rewards = CustomerPunchCardTransactionsModel::where('stamp_card_id', $this->id)->where('type', 'redeem')->where('void', 0);
        if ($claimed === false || $claimed === true) {
            $rewards->where('status', $claimed);
        }

        return $rewards->get();
    }

    public function getClaimedRewardsAttribute()
    {
        return $this->getRewards(true);
    }

    public function getDataAttribute()
    {
        $card = [];
        $card['id'] = $this->uuid;
        $card['promo'] = $this->punchcard->data;
        /**
         * Format rewards
         */
        $rewards = [];
        foreach($this->rewards as $redeem_reward) {
            $rewards[] = [
                'redeem_id' => $redeem_reward->uuid,
                'reward_id' => $redeem_reward->reward->uuid,
                'reward_name' => $redeem_reward->reward->reward,
                'reward_image_url' => $redeem_reward->reward->image_url,
                'status' => (int) $redeem_reward->status,
                'date' => $redeem_reward->updated_at ? $redeem_reward->updated_at->format('Y-m-d H:i:s') : ($redeem_reward->created_at ? $redeem_reward->created_at->format('Y-m-d H:i:s') : null)
            ];
        }
        $card['rewards'] = $rewards;
        $card['stamps'] = count($this->stamps);
        return $card;
    }

    public function getRewardsAttribute()
    {
        return $this->getRewards();
    }

    public function earnStamps( $employee, $amount, $channel = null ){

        $transaction = CustomerPunchCardTransactionsModel::addTransaction($employee, $this, $amount, null, false, 'earn', $channel);
        $stamps = (int)($amount / $this->punchcard->amount_per_stamp);

        // Check for maximum stamps required
        // Get promo max stamp
        $max_num_stamp = $this->punchcard->num_stamps;
        $current_no_stamp = count($this->stamps);

        if (($current_no_stamp == 0) && $stamps > $max_num_stamp) {
            $stamps = $max_num_stamp;
        } elseif (($current_no_stamp + $stamps) > $max_num_stamp) {
            $stamps = $max_num_stamp - $current_no_stamp;
        }
        for ($i = 1; $i <= $stamps; ++$i) {
            $stamp = new CustomerStamps();
            $stamp->transaction_id = $transaction->id;
            $stamp->stamp_card_id = $this->id;
            $stamp->void = 0;
            $stamp->save();
        }

        // $this->customer->sms->earnStamps($transaction);
        return $transaction;

    }

    public function isRewardClaimed($reward_id)
    {
        return CustomerPunchCardTransactionsModel::where('stamp_card_id', $this->id)->where('void', 0)->where('reward_id', $reward_id)->count();
    }


    public function redeemStamps($employee, $reward, $status)
    {
        foreach ($this->stamps as $stamp) {
            $stamp->redeem = 1;
            $stamp->save();
        }

        $transaction = CustomerPunchCardTransactionsModel::addTransaction($employee, $this, null, $reward->id, $status, 'redeem');
        // $this->customer->sms->redeemStamps($transaction);

        return $transaction;
    }

    public function issueReward($reward_id, $employee)
    {
        foreach ($this->unclaimed_rewards as $redeem) {
            if ($redeem->reward->uuid == $reward_id) {
                $redeem->status = 1;
                $redeem->employee_id = $employee->userId;
                $redeem->save();
                return $redeem->transaction_ref;
            }
        }

        return false;
    }

    public function getUnclaimedRewardsAttribute()
    {
        return $this->getRewards(false);
    }

    public function earnClientStamps($employee, $reference){

        $stamps = $this->punchcard->upload_stamps_count;
        $transaction = CustomerPunchCardTransactionsModel::addTransaction($employee, $this, 1, null, false, 'earn', 'upload', $reference, $stamps);

        // Check for maximum stamps required
        // Get promo max stamp
        $max_num_stamp = $this->punchcard->num_stamps;
        $current_no_stamp = count($this->stamps);

        if (($current_no_stamp == 0) && $stamps > $max_num_stamp) {
            $stamps = $max_num_stamp;
        } elseif (($current_no_stamp + $stamps) > $max_num_stamp) {
            $stamps = $max_num_stamp - $current_no_stamp;
        }
        for ($i = 1; $i <= $stamps; ++$i) {
            $stamp = new CustomerStamps();
            $stamp->transaction_id = $transaction->id;
            $stamp->stamp_card_id = $this->id;
            $stamp->branch_id = $employee->branch->id;
            $stamp->void = 0;
            $stamp->save();
        }

        return $transaction;
    }

    public function getRedemptionStatusAttribute()
    {
        if ($this->stamps->count()) {
            if ($this->getRewards()->count()) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
}
