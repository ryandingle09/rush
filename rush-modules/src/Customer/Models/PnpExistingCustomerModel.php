<?php

namespace Rush\Modules\Customer\Models;

use Illuminate\Database\Eloquent\Model;
use Rush\Modules\Customer\Models\CustomerPointsLevelModel;

class PnpExistingCustomerModel extends Model
{
    protected $table = "pnp_existing_customers";

    public function getIsExistingCardNumberAttribute()
    {
        return true;
    }

    public function rush_customer()
    {
        return $this->belongsTo('Rush\Modules\Customer\Models\CustomerModel', 'card_number', 'firstName');
    }

    public function getPointsLevelIdAttribute()
    {
        $points_level_id = 0;
        $rush_customer = $this->rush_customer;

        // pnp's migrated levels are not consistent.
        // get the points levels of the rush customer's merchant
        // and check if the level's code text exists in the pnp's level text
        // to get the points level id.
        if( $rush_customer ){
            $points_levels = CustomerPointsLevelModel::where('merchant_id', $rush_customer->merchantId)->get();

            foreach( $points_levels as $level ){
                if( preg_match("/$level->code/i", $this->level) ){
                    $points_level_id = $level->id;
                    break;
                }
            }
        }

        return $points_level_id;
    }
}
