<?php

namespace Rush\Modules\Customer\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Logaretm\Transformers\TransformableTrait;
use Logaretm\Transformers\Contracts\Transformable;
use Rush\Modules\Customer\Models\Traits\CustomerClassPackages\RelationshipsTrait;
use Rush\Modules\Customer\Models\Traits\CustomerClassPackages\AttributesTrait;

/**
 * Class CustomerClassPackageModel
 *
 * @property-read   int                                                                      $id
 * @property        string[]                                                                 $branch_ids
 * @property        int                                                                      $class_package_id
 * @property        int                                                                      $customer_id
 * @property-read   string                                                                   $uuid
 * @property-read   \Rush\Modules\Customer\Models\CustomerClassPackagePunchcardModel         $card
 * @property-read   \Rush\Modules\Customer\Models\CustomerModel                              $customer
 * @property-read   \Rush\Modules\ClassManagement\Models\PackageModel                        $package
 * @property-read   \Rush\Modules\Customer\Models\CustomerPunchcardTransactionsModel         $transaction
 * @property        \Carbon\Carbon                                                           $start_date
 * @property        \Carbon\Carbon                                                           $end_date
 * @property-read   \Carbon\Carbon                                                           $created_at
 * @property-read   \Carbon\Carbon                                                           $updated_at
 * @method          static   CustomerClassPackageModel[]                                     all
 */
class CustomerClassPackageModel extends Model implements Transformable
{
    use AttributesTrait,
        RelationshipsTrait,
        TransformableTrait,
        SoftDeletes;

    protected $dates = ['start_date', 'end_date'];

    protected $table = 'customer_class_packages';

    public function isExpired()
    {
        $now = Carbon::now();
        $now->hour = 0;
        $now->minute = 0;
        $now->second = 0;
        return (boolean) $this->end_date->lt($now);
    }
}
