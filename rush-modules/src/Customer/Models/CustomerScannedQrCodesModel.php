<?php
namespace Rush\Modules\Customer\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerScannedQrCodesModel extends Model
{
    protected $table = "customer_scanned_qr_codes";
}