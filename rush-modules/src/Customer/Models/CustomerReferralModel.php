<?php
namespace Rush\Modules\Customer\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Rush\Modules\Customer\Models\CustomerReferralModel
 *
 * @property integer $customer_referred_id
 * @property integer $customer_referrer_id
 */
class CustomerReferralModel extends Model
{
    protected $table = "customer_referral";
}

