<?php
namespace Rush\Modules\Customer\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerPaymentDetailModel extends Model
{
    protected $table = "customer_payment_details";
}
