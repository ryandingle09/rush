<?php
namespace Rush\Modules\Customer\Models;

use Illuminate\Database\Eloquent\Model;

class BeneCustomerRsvpModel extends Model
{
    protected $table = "BeneCustomerRsvp";
}