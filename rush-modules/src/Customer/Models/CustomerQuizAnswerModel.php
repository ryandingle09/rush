<?php

namespace Rush\Modules\Customer\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerQuizAnswerModel extends Model
{
    protected $table = "customer_quiz_answers";
}
