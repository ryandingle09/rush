<?php

namespace Rush\Modules\Customer\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerPointsLevelModel extends Model
{
    protected $table = "customer_points_levels";

    public function getEarningPoints( $amount )
    {
        $points = $this->points;

        if( $this->points_type == 'percent' ){
            $points = (float) $amount * ( (float) $this->points / 100 );
        }
        
        return $points;
    }
}
