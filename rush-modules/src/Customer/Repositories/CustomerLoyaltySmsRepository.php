<?php
namespace Rush\Modules\Customer\Repositories;

use Rush\Modules\Helpers\SmsHelper;
use Rush\Modules\Customer\Helpers\SmsLogHelper;
use Carbon\Carbon;
use Rush\Modules\Sms\Models\SmsCredentialModel;
use Rush\Modules\Customer\Models\CustomerModel;
use Rush\Modules\Merchant\Models\MerchantCodeModel;
use Rush\Modules\Merchant\Models\MerchantEmployeesModel;
use Rush\Modules\Customer\Models\NonMemberTransaction; // temporary

class CustomerLoyaltySmsRepository
{
    protected $smsHelper;

    // 0 is the merchant id of the generic shortcode.
    private $genericMerchantId = 0;

    private function setCredential($merchant_id = 0)
    {
        $merchant_credentials = SmsCredentialModel::where('merchant_id', $merchant_id)->first();

        $this->smsHelper = new SmsHelper(
            $merchant_credentials->shortcode,
            $merchant_credentials->app_id,
            $merchant_credentials->app_secret,
            $merchant_credentials->passphrase
        );
    }

    public function forgotPin($customer)
    {
        $message = "Hey, {$customer->fullName}! You have chosen to retrieve your MPIN. You may now continue to access your {$customer->merchant->program_name} account using the retrieved M-PIN: {$customer->PIN}. Also we have sent a copy of your PIN to your email registered to our program.";

        $this->setCredential($customer->merchantId);
        $response = $this->smsHelper->send($customer->mobileNumber, $message);
        SmsLogHelper::log($customer->mobileNumber, $message, $response, $customer->merchantId, 'customer-forgot-pin');

        return $response;
    }

    public function earnPoints( $transaction )
    {
        $customer = $transaction->customer;
        $datetime = Carbon::createFromTimestamp(strtotime($transaction->timestamp));
        $points_earned = $transaction->pointsEarned;
        $points_name = $transaction->customer->merchant->settings->points_name;
        $amount = $transaction->amountPaidWithCash;
        $amount_phrase = ( $transaction->amountPaidWithCash != 0 ? "totalling PHP{$transaction->amountPaidWithCash}" : '' );
        // $earn_location = ( $transaction->employee ? $transaction->employee->branch->branchName : $transaction->customer->merchant->settings->program_name );
        if ( $transaction->employee ) {
            if ( $transaction->employee->branch ) {
                $earn_location = $transaction->employee->branch->branchName;
            } else {
                $earn_location = $transaction->branch->branchName;
            }
        } else {
            $earn_location = $transaction->customer->merchant->settings->program_name;
        }
        
        $earn_datetime = $datetime->format('m-d-Y, g:i:s A');
        $balance_points = number_format((float) $transaction->customer->points->currentpoints, 2);
        $transaction_reference = $transaction->transactionReferenceCode;


        // Specific message for Power Plant Mall merchant
        if ($customer->merchant->merchantid == 206) {
            $message = "You have earned {$points_earned} {$points_name} points for your movie ticket transaction totalling PHP{$amount} in {$earn_location} on {$earn_datetime}. Your new balance is {$balance_points} {$points_name} points. Keep collecting {$points_name} points for every purchase to redeem exciting rewards! Ref. No {$transaction_reference}";
        } else {
            $message = "You have earned {$points_earned} {$points_name} points for your transaction {$amount_phrase} in {$earn_location} on {$earn_datetime}. Your new balance is {$balance_points} {$points_name} points. Keep collecting {$points_name} points for every purchase to redeem exciting rewards! Ref. No {$transaction_reference}";
        }

        $this->setCredential($customer->merchantId);
        $response = $this->smsHelper->send($customer->mobileNumber, $message);
        SmsLogHelper::log($customer->mobileNumber, $message, $response, $customer->merchantId, 'customer-earn-points');

        return $response;
    }

    public function payPoints($transaction)
    {
        $customer = $transaction->customer;
        $datetime = Carbon::createFromTimestamp(strtotime($transaction->timestamp));
        $amount_paid_with_points = $transaction->amountPaidWithpoints;
        $points_name = $customer->merchant->settings->points_name;
        $amount = $transaction->amountPaidWithCash;
        $amount_phrase = ( $transaction->amountPaidWithCash != 0 ? "totalling PHP{$amount}" : '' );
        $paypoints_location = ( $transaction->employee ? $transaction->employee->branch->branchName : $transaction->customer->merchant->settings->program_name );
        $paypoints_datetime = $datetime->format('m-d-Y, g:i:s A');
        $balance_points = number_format((float)$customer->points->currentpoints, 2);
        $transaction_reference = $transaction->transactionReferenceCode;

        $message = "You have paid {$amount_paid_with_points} {$points_name} points for your transaction {$amount_phrase} in {$paypoints_location} on {$paypoints_datetime}. Your new balance is {$balance_points} {$points_name}. Keep collecting {$points_name} points for every purchase to redeem exciting rewards! Ref. No {$transaction_reference}.";

        $this->setCredential($customer->merchantId);
        $response = $this->smsHelper->send($customer->mobileNumber, $message);
        SmsLogHelper::log($customer->mobileNumber, $message, $response, $customer->merchantId, 'customer-pay-points');

        return $response;
    }

    public function redeemReward( $transaction )
    {
        $datetime = Carbon::createFromTimestamp(strtotime($transaction->timestamp));
        $customer = $transaction->customer;
        $points_redeem = $transaction->pointsRedeem;
        $points_name = $customer->merchant->settings->points_name;
        $quantity = (int) $transaction->redeem->quantity;
        $reward_name = $transaction->reward['name'];
        $redeem_location = ( $transaction->employee ? $transaction->employee->branch->branchName : $transaction->customer->merchant->settings->program_name );
        $redeem_datetime = $datetime->format('m-d-Y, g:i:s A');;
        $balance_points = number_format((float)$customer->points->currentpoints, 2);
        $transaction_reference = $transaction->transactionReferenceCode;

        $message = "You have redeemed {$points_redeem} {$points_name} points {$quantity} {$reward_name} in {$redeem_location} on {$redeem_datetime}. Your new balance is {$balance_points} {$points_name} points. Keep collecting {$points_name} points every time you make a purchase to redeem exciting rewards! Ref. No {$transaction_reference}.";

        $this->setCredential($customer->merchantId);
        $response = $this->smsHelper->send($customer->mobileNumber, $message);
        SmsLogHelper::log($customer->mobileNumber, $message, $response, $customer->merchantId, 'customer-redeem-reward');

        return $response;
    }

    public function claimReward( $transaction ){

        $datetime = Carbon::createFromTimestamp(strtotime($transaction->timestamp));
        $customer = $transaction->customer;
        $claimed = $transaction->redeem->claimed;
        $quantity = $transaction->redeem->quantity;
        $reward_name = $transaction->reward['name'];
        $claim_location = ( $transaction->employee ? $transaction->employee->branch->branchName : $transaction->customer->merchant->settings->program_name );
        $claim_datetime = $datetime->format('m-d-Y, g:i:s A');
        $points_name = $customer->merchant->settings->points_name;
        $transaction_reference = $transaction->transactionReferenceCode;

        $message = "You have claimed {$claimed} of {$quantity} redeemed {$reward_name} from {$claim_location} on {$claim_datetime}. Keep collecting {$points_name} points every time you make a purchase to redeem exciting rewards! Ref. No {$transaction_reference}";

        $this->setCredential($customer->merchantId);
        $response = $this->smsHelper->send($customer->mobileNumber, $message);
        SmsLogHelper::log($customer->mobileNumber, $message, $response, $customer->merchantId, 'customer-claim-reward');

        return $response;

    }

    public function sendInvalidKeywordNotif($mobile_no)
    {
        $message = 'Sorry, your request cannot be processed as of the moment due to invalid keyword. TEXT KEYWORD [MerchantCode] to get the complete list of keywords.';

        $this->setCredential($this->genericMerchantId);
        $response = $this->smsHelper->send($mobile_no, $message);
        SmsLogHelper::log($mobile_no, $message, $response, $this->genericMerchantId, 'sms-service-invalid-keyword');

        return $response;
    }

    public function sendUnregisteredNumberNotif($mobile_no, $partials)
    {
        $message = "Sorry, your mobile number is not yet registered with the program. To register please text REG {$partials['merchant_code']}, Name, Email, 4-Digit PIN";

        $this->setCredential($this->genericMerchantId);
        $response = $this->smsHelper->send($mobile_no, $message);
        SmsLogHelper::log($mobile_no, $message, $response, $this->genericMerchantId, 'sms-service-sender-not-member');

        return $response;
    }

    public function sendKeywordsList($mobile_no, $partials)
    {
        $message_first_part = "Registration : Text REG {$partials['merchant_code']}, [Name], [Email address], [4-Digit PIN]\nPoints Inquiry : Text POINTS {$partials['merchant_code']}";
        $message_second_part = "Points Transfer : Text TRANSFER {$partials['merchant_code']}, [recipient's 11-digit mobile number], [Points], [PIN]";

        $this->setCredential($this->genericMerchantId);

        $response = $this->smsHelper->send($mobile_no, $message_first_part);
        SmsLogHelper::log($mobile_no, $message_first_part, $response, $this->genericMerchantId, 'sms-service-keywords-list');

        $response = $this->smsHelper->send($mobile_no, $message_second_part);
        SmsLogHelper::log($mobile_no, $message_second_part, $response, $this->genericMerchantId, 'sms-service-keywords-list');

        return $response;
    }

    public function sendRegistrationFailedNotif($mobile_no)
    {
        $message = "Sorry, your request cannot be processed as of the moment or you may have entered an invalid keyword. Please make sure you've register with the following keyword: REG [Merchant Code], Name, Email, 4-Digit PIN";

        $this->setCredential($this->genericMerchantId);
        $response = $this->smsHelper->send($mobile_no, $message);
        SmsLogHelper::log($mobile_no, $message, $response, $this->genericMerchantId, 'sms-service-registration-failed');

        return $response;
    }

    public function sendCustomerUpdateFailedNotif($mobile_no)
    {
        $message = "Sorry, your request cannot be processed as of the moment due to invalid keyword. To update your details please text UPDATE [Merchant Code], [Name], [Email], [4-Digit PIN], [Card#], [Birthdate: mm/dd/yyyy], [City]";

        $this->setCredential($this->genericMerchantId);
        $response = $this->smsHelper->send($mobile_no, $message);
        SmsLogHelper::log($mobile_no, $message, $response, $this->genericMerchantId, 'sms-service-registration-failed');

        return $response;
    }

    public function sendDuplicateContactNotif($mobile_no, $partials)
    {
        $message = "Sorry, mobile number already registered to this program: " . $partials['merchant_code'];

        $this->setCredential($this->genericMerchantId);
        $response = $this->smsHelper->send($mobile_no, $message);
        SmsLogHelper::log($mobile_no, $message, $response, $this->genericMerchantId, 'sms-service-duplicate-contact');

        return $response;
    }

    public function sendDuplicateEmailNotif($mobile_no, $partials)
    {
        $message = "Sorry, email address already registered to this program: " . $partials['merchant_code'];

        $this->setCredential($this->genericMerchantId);
        $response = $this->smsHelper->send($mobile_no, $message);
        SmsLogHelper::log($mobile_no, $message, $response, $this->genericMerchantId, 'sms-service-duplicate-email');

        return $response;
    }

    public function sendRegistrationSuccessNotif($customer)
    {
        $sender_name = $customer->fullName;
        $program_name = $customer->merchant->settings->program_name;
        $points_name = $customer->merchant->settings->points_name;

        $message = "Hi, {$sender_name}! Thank you for signing up for {$program_name}! Start earning {$points_name} points every time you make a purchase to redeem exciting rewards!";

        $this->setCredential($customer->merchantId);
        $response = $this->smsHelper->send($customer->mobileNumber, $message);
        SmsLogHelper::log($customer->mobileNumber, $message, $response, $customer->merchantId, 'sms-service-customer-registration');

        return $response;
    }

    public function sendPointsInquiryFailedNotif($mobile_no)
    {
        $message = "Sorry, your request cannot be processed as of the moment or you may have entered an invalid keyword. To know your available points, please reply with POINTS [Merchant Code]";

        $this->setCredential($this->genericMerchantId);
        $response = $this->smsHelper->send($mobile_no, $message);
        SmsLogHelper::log($mobile_no, $message, $response, $this->genericMerchantId, 'sms-service-points-inquiry-failed');

        return $response;
    }

    public function sendCustomerPointsSms($mobile_no, $partials)
    {
        $merchant_code = MerchantCodeModel::where('code', $partials['merchant_code'])->first();
        $customer = CustomerModel::where('mobileNumber', $mobile_no)->where('merchantId', $merchant_code->merchant_id)->first();
        $customer_name = $customer->fullName;
        $total_points = number_format((float)$customer->points->currentpoints, 2);
        $transaction_date = date('m-d-y, h:i A');
        $points_name = $customer->merchant->settings->points_name;

        $message = "Hi, {$customer_name}! You have {$total_points} points as of {$transaction_date}. Keep collecting {$points_name} points for every purchase to redeem exciting rewards!";

        $this->setCredential($customer->merchantId);
        $response = $this->smsHelper->send($customer->mobileNumber, $message);
        SmsLogHelper::log($customer->mobileNumber, $message, $response, $customer->merchantId, 'sms-service-points-inquiry');

        return $response;
    }
    
    public function sendTransferPointsFailedNotif($mobile_no)
    {
        $message_part_1 = "Sorry, your request cannot be processed as of the moment due to invalid keyword. Please make sure you have enter the correct keyword: ";
        $message_part_2 = "Transfer [Merchant Code], [recipient's 11-digit mobile number], [Points], [4-Digit PIN]";

        $this->setCredential($this->genericMerchantId);

        $response = $this->smsHelper->send($mobile_no, $message_part_1);
        SmsLogHelper::log($mobile_no, $message_part_1, $response, $this->genericMerchantId, 'sms-service-transfer-points-failed');

        $response = $this->smsHelper->send($mobile_no, $message_part_2);
        SmsLogHelper::log($mobile_no, $message_part_2, $response, $this->genericMerchantId, 'sms-service-transfer-points-failed');

        return $response;
    }

    public function sendZeroPointsTransferNotif($mobile_no)
    {
        $message = "Sorry, points cannot be in 0 value";

        $this->setCredential($this->genericMerchantId);
        $response = $this->smsHelper->send($mobile_no, $message);
        SmsLogHelper::log($mobile_no, $message, $response, $this->genericMerchantId, 'sms-service-zero-points-transfer');

        return $response;
    }

    public function sendNegativePointsTransferNotif($mobile_no)
    {
        $message = "Sorry, points cannot be in negative value";

        $this->setCredential($this->genericMerchantId);
        $response = $this->smsHelper->send($mobile_no, $message);
        SmsLogHelper::log($mobile_no, $message, $response, $this->genericMerchantId, 'sms-service-negative-points-transfer');

        return $response;
    }

    public function sendTransferPointsInvalidRecipientNotif($mobile_no, $partials)
    {
        $message = "Sorry, your request cannot be processed as of the moment due to invalid recipient. Please make sure you are transferring points to a mobile number registered with {$partials['merchant_code']}";

        $this->setCredential($this->genericMerchantId);
        $response = $this->smsHelper->send($mobile_no, $message);
        SmsLogHelper::log($mobile_no, $message, $response, $this->genericMerchantId, 'sms-service-receiver-not-member');

        return $response;
    }

    public function sendInsufficientPointsNotif($mobile_no, $partials)
    {
        $message = "Sorry, your request cannot be processed as of the moment due to insufficient points. To know your available points, please reply with POINTS {$partials['merchant_code']}";

        $this->setCredential($this->genericMerchantId);
        $response = $this->smsHelper->send($mobile_no, $message);
        SmsLogHelper::log($mobile_no, $message, $response, $this->genericMerchantId, 'sms-service-insufficient-points');

        return $response;
    }

    public function sendTransferPointsInvalidPinNotif($mobile_no, $partials)
    {
        $message = "Sorry, your request cannot be processed as of the moment due to invalid 4-Digit PIN. Please try again";

        $this->setCredential($this->genericMerchantId);
        $response = $this->smsHelper->send($mobile_no, $message);
        SmsLogHelper::log($mobile_no, $message, $response, $this->genericMerchantId, 'sms-service-invalid-pin');

        return $response;
    }

    public function sendTransferPointsSuccessNotif($mobile_no, $partials, $transaction_reference)
    {
        $merchant_code = MerchantCodeModel::where('code', $partials['merchant_code'])->first();
        $customer = CustomerModel::where('mobileNumber', $mobile_no)->where('merchantId', $merchant_code->merchant_id)->first();
        $points = $partials['points'];
        $points_name = $customer->merchant->settings->points_name;
        $receiver = $partials['receiver'];
        $transaction_date = date('m-d-y, h:i A');
        $current_points = number_format((float)$customer->points->currentpoints, 2);

        $message = "You have successfully transferred {$points} {$points_name} points to mobile number {$receiver} on {$transaction_date}. Your new balance is {$current_points}. Keep collecting {$points_name} points for every purchase to redeem exciting rewards! Ref. No {$transaction_reference}";

        $this->setCredential($customer->merchantId);
        $response = $this->smsHelper->send($customer->mobileNumber, $message);
        SmsLogHelper::log($customer->mobileNumber, $message, $response, $customer->merchantId, 'sms-service-points-transfer');

        return $response;
    }
    
    public function sendReceivePointsSuccessNotif( $sender_mobile_no, $partials, $transaction_reference ){
        
        $merchant_code = MerchantCodeModel::where('code', $partials['merchant_code'])->first();
        $sender = CustomerModel::where('mobileNumber', $sender_mobile_no)->where('merchantId', $merchant_code->merchant_id)->first();
        $receiver = CustomerModel::where('mobileNumber', $partials['receiver'])->where('merchantId', $merchant_code->merchant_id)->first();
        
        $points = $partials['points'];
        $points_name = $sender->merchant->settings->points_name;
        $sender_name = $sender->fullName;
        $transaction_date = date('m-d-y, h:i A');
        $receiver_balance_points = number_format( (float) $receiver->points->currentpoints, 2 );
        
        $message = "You have successfully received {$points} {$points_name} points from {$sender_name} on {$transaction_date}. Your new balance is {$receiver_balance_points}. Keep collecting {$points_name} points for every purchase to redeem exciting rewards! Ref. No {$transaction_reference}";
        
        $this->setCredential($receiver->merchantId);
        $response = $this->smsHelper->send($receiver->mobileNumber, $message);
        SmsLogHelper::log($receiver->mobileNumber, $message, $response, $receiver->merchantId, 'sms-service-points-receive');

        return $response;
        
    }

    public function sendDisabledPointsTransferNotif($mobile_no, $partials)
    {
        $message = "Sorry, transferring of {$partials['merchant_code']} points are currently not allowed by the program.";

        $this->setCredential($this->genericMerchantId);
        $response = $this->smsHelper->send($mobile_no, $message);
        SmsLogHelper::log($mobile_no, $message, $response, $this->genericMerchantId, 'sms-service-points-transfer-disabled');

        return $response;
    }

    public function sendTransferPointsToSelfNotif($mobile_no)
    {
        $message = "Sorry, your request to transfer points to the same number cannot be processed. Please select other recipient";

        $this->setCredential($this->genericMerchantId);
        $response = $this->smsHelper->send($mobile_no, $message);
        SmsLogHelper::log($mobile_no, $message, $response, $this->genericMerchantId, 'sms-service-points-transfer-to-self');

        return $response;
    }

    public function sendInvalidReferralCodeNotif( $mobile_no ){

        $message = "Sorry, your request cannot be processed as of the moment due to invalid referral code. Please try again";

        $this->setCredential( $this->genericMerchantId );
        $response = $this->smsHelper->send($mobile_no, $message);
        SmsLogHelper::log($mobile_no, $message, $response, $this->genericMerchantId, 'sms-service-invalid-referral-code');
        return $response;

    }

    public function redeemAmaxReward( $transaction, $promo )
    {
        $datetime = Carbon::createFromTimestamp(strtotime($transaction->timestamp));
        $message = "Congratulations! You have successfully availed {$promo} on {$datetime->format('m-d-y, g:i:s A')}. Ref# {$transaction->transactionReferenceCode}.";
        $customer = $transaction->customer;

        $this->setCredential($customer->merchantId);
        $response = $this->smsHelper->send($customer->mobileNumber, $message);
        SmsLogHelper::log($customer->mobileNumber, $message, $response, $customer->merchantId, 'customer-redeem-amax-reward');

        return $response;
    }

    public function voidEarn( $transaction ){

        $customer = $transaction->customer;
        $datetime = Carbon::createFromTimestamp(strtotime($transaction->timestamp));
        $balance_points = number_format( (float) $customer->points->currentpoints, 2);
        $voided_points = $transaction->voided_points ? $transaction->voided_points : $transaction->pointsEarned;
        $voided_amount = $transaction->voided_amount ? $transaction->voided_amount : $transaction->amountPaidWithCash;
        $points_name = $transaction->customer->merchant->settings->points_name;
        $branch_name = $transaction->employee->branch->branchName;
        $earn_datetime = $datetime->format('m-d-Y, g:i:s A');
        $transaction_reference = $transaction->transactionReferenceCode;

        $message = "We have successfully reversed {$voided_points} {$points_name} points for your transaction totaling PHP{$voided_amount} in {$branch_name} on {$earn_datetime}. Your new balance is {$balance_points} {$points_name} points. Keep collecting {$points_name} points for every purchase to redeem exciting rewards! Ref. No {$transaction_reference}.";

        $this->setCredential($customer->merchantId);
        $response = $this->smsHelper->send($customer->mobileNumber, $message);
        SmsLogHelper::log($customer->mobileNumber, $message, $response, $customer->merchantId, 'customer-void-earn');

        return $response;
    }

    public function voidPackage($transaction){
        $customer = $transaction->customer;
        $class_package = $transaction->class_package()->withTrashed()->first();
        $transaction_reference = $transaction->transactionReferenceCode;

        $message = "Your Package {$class_package->package->name} has been voided. Ref. No {$transaction_reference}.";

        $this->setCredential($customer->merchantId);
        $response = $this->smsHelper->send($customer->mobileNumber, $message);
        SmsLogHelper::log($customer->mobileNumber, $message, $response, $customer->merchantId, 'customer-void-purchase-package');

        return $response;
    }

    public function voidPaypoints( $transaction ){

        $customer = $transaction->customer;
        $datetime = Carbon::createFromTimestamp(strtotime($transaction->timestamp));
        $points_burn = $transaction->pointsBurn;
        $points_name = $transaction->customer->merchant->settings->points_name;
        $total_amount_purchased =  $transaction->amountPaidWithCash;
        $branch_name = $transaction->employee->branch->branchName;
        $paypoints_datetime = $datetime->format('m-d-Y, g:i:s A');
        $balance_points = number_format( (float)$customer->points->currentpoints, 2);
        $transaction_reference = $transaction->transactionReferenceCode;

        $message = "We have successfully reversed {$points_burn} {$points_name} points for your transaction totaling PHP{$total_amount_purchased} in {$branch_name} on {$paypoints_datetime}. Your new balance is {$balance_points} {$points_name} points. Keep collecting {$points_name} points for every purchase to redeem exciting rewards! Ref. No {$transaction_reference}.";

        $this->setCredential($customer->merchantId);
        $response = $this->smsHelper->send($customer->mobileNumber, $message);
        SmsLogHelper::log($customer->mobileNumber, $message, $response, $customer->merchantId, 'customer-void-pay-points');

        return $response;
    }

    public function voidRedeem( $transaction ){

        $customer = $transaction->customer;
        $datetime = Carbon::createFromTimestamp(strtotime($transaction->timestamp));
        $reward_name = $transaction->reward['name'];
        $business_name = $customer->merchant->businessName;
        $redemption_datetime = $datetime->format('m-d-Y, g:i:s A');
        $balance_points = number_format( (float) $customer->points->currentpoints, 2);
        $points_name = $customer->merchant->settings->points_name;
        $transaction_reference = $transaction->transactionReferenceCode;

        $message = "We have successfully reversed your redemption of {$reward_name} from {$business_name} on {$redemption_datetime}. Your new balance is {$balance_points} {$points_name} points. Keep collecting {$points_name} points for every purchase to redeem exciting rewards! Ref. No {$transaction_reference}.";

        $this->setCredential($customer->merchantId);
        $response = $this->smsHelper->send($customer->mobileNumber, $message);
        SmsLogHelper::log($customer->mobileNumber, $message, $response, $customer->merchantId, 'customer-void-redeem');

        return $response;

    }

    public function redeemAmaxRewardFailed( $customer ){

        $message = "Sorry your request to redeem Globe product cannot be processed at this time. Redemption has been reversed and points are credited to your account. Please try again later.";

        $this->setCredential($customer->merchantId);
        $response = $this->smsHelper->send($customer->mobileNumber, $message);
        SmsLogHelper::log($customer->mobileNumber, $message, $response, $customer->merchantId, 'customer-void-redeem-amax-reward');

        return $response;

    }

    public function earnNonMemberPoints( $transaction ){

        $employee = $transaction->employee;
        $merchant_id = $employee->merchant->id;
        $program_name = $employee->merchant->settings->program_name;
        $points = $employee->merchant->getEarningPointsOfAmount( $transaction->amountPaidWithCash );
        $points_name = $employee->merchant->settings->points_name;
        $amount = $transaction->amountPaidWithCash;
        $branch = $employee->branch->branchName;
        $datetime = Carbon::createFromTimestamp(strtotime($transaction->timestamp));
        $app_name = $employee->merchant->settings->app_name;
        $current_points = NonMemberTransaction::getCurrentPoints( $transaction->mobileNumber );
        $current_points = number_format((float)$current_points, 2);
        $or_number = $transaction->receiptReferenceNumber;

        $message = "Thank you for your purchase our dear customer! Register with {$program_name} program for you to earn {$points} {$points_name} points from your transaction totaling PHP{$amount} in {$branch} on {$datetime->format('m-d-Y')}, {$datetime->format('g:i:s A')}. Download {$app_name} from App Store and Google Play now. Ref. No {$transaction->transactionReferenceCode}. OR No {$or_number}";
        
        if($employee->merchant->id == 190 || $employee->merchant->id == 232) {
            $message = "You have earned {$points} points for your transaction totaling PHP {$amount} in {$branch} on {$datetime->format('j-M-Y h:i A')}. Your new balance is {$current_points} {$points_name} points. Use your points and get exciting rewards by downloading our app from {$employee->merchant->settings->vanity_url}";
        }
        $this->setCredential($merchant_id);
        $response = $this->smsHelper->send($transaction->mobileNumber, $message);
        SmsLogHelper::log($transaction->mobileNumber, $message, $response, $merchant_id, 'non-member-earn-points');

        return $response;

    }

    public function issueReward( $transaction ){

        $mobile_no = $transaction->customer->mobileNumber;
        $merchant_id = $transaction->customer->merchant->merchantid;

        if( $merchant_id == 223 ){
            $message = "Enjoy your Globe Wanderland prize, Wanderer!";
        } else {
            $message = "You have claimed the redeemed {$transaction->reward->name} from {$transaction->employee->branch->branchName} on {$transaction->updated_at->format('g:i:s A')}. Keep collecting {$transaction->customer->merchant->settings->points_name} stamps every time you make a purchase to redeem exciting rewards! Ref. No {$transaction->transaction_ref}";
        }

        $this->setCredential($merchant_id);
        $response = $this->smsHelper->send($mobile_no, $message);
        SmsLogHelper::log($mobile_no, $message, $response, $merchant_id, 'customer-claim-reward');

        return $response;

    }

    public function createCustomer( $customer ){

        $customer_name = $customer->fullName;
        $program_name = $customer->merchant->settings->program_name;
        $points_name = $customer->merchant->settings->points_name;
        $message = "Hi, {$customer_name}! Thank you for signing up for {$program_name}! Start earning {$points_name} points every time you make a purchase to redeem exciting rewards!";

        $this->setCredential($customer->merchantId);
        $response = $this->smsHelper->send($customer->mobileNumber, $message);
        SmsLogHelper::log($customer->mobileNumber, $message, $response, $customer->merchantId, 'customer-register');

        return $response;

    }

    public function voidPackagePunchcard($transaction){
        $customer = $transaction->customer;
        $class_package = $transaction->class_package()->withTrashed()->first();
        $transaction_reference = $transaction->transaction_ref ? $transaction->transaction_ref : $transaction->or_no;

        $message = "Your Package {$class_package->package->name} has been voided. Ref. No {$transaction_reference}.";

        $this->setCredential($customer->merchantId);
        $response = $this->smsHelper->send($customer->mobileNumber, $message);
        SmsLogHelper::log($customer->mobileNumber, $message, $response, $customer->merchantId, 'customer-void-purchase-package');

        return $response;
    }

    public function sendPnpRegistrationFailedNotif($mobile_no)
    {
        $message = "Sorry, your request cannot be processed as of the moment or you may have entered an invalid keyword. Please make sure you've register with the following keyword: REG [Merchant Code], Name, Email, 4-Digit PIN, [Customer ID#], [Birthdate mm/dd/yyyy], [City]";

        $this->setCredential($this->genericMerchantId);
        $response = $this->smsHelper->send($mobile_no, $message);
        SmsLogHelper::log($mobile_no, $message, $response, $this->genericMerchantId, 'sms-service-pnp-registration-failed');

        return $response;
    }

    public function sendInvalidCardNumberNotif( $mobile_no )
    {
        $message = "Sorry, your request cannot be processed as of the moment due to invalid card number. Please try again";

        $this->setCredential( $this->genericMerchantId );
        $response = $this->smsHelper->send($mobile_no, $message);
        SmsLogHelper::log($mobile_no, $message, $response, $this->genericMerchantId, 'sms-service-invalid-card-number');
        return $response;
    }

    public function sendCustomerUpdateSuccessNotif($customer)
    {
        $sender_name = $customer->fullName;

        $message = "Hi, {$sender_name}! you have successfully updated your details!";

        $this->setCredential($customer->merchantId);
        $response = $this->smsHelper->send($customer->mobileNumber, $message);
        SmsLogHelper::log($customer->mobileNumber, $message, $response, $customer->merchantId, 'sms-service-customer-update');

        return $response;
    }

    public function sendUnregisteredPnpNumberNotif($mobile_no, $partials)
    {
        $message = "Sorry, your mobile number is not yet registered with the program. To register please text REG {$partials['merchant_code']}, Name, Email, 4-Digit PIN, Card#, Birthdate: mm/dd/yyyy/ City";

        $this->setCredential($this->genericMerchantId);
        $response = $this->smsHelper->send($mobile_no, $message);
        SmsLogHelper::log($mobile_no, $message, $response, $this->genericMerchantId, 'sms-service-pnp-sender-not-member');

        return $response;
    }

}
