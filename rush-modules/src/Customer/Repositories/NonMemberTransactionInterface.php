<?php

namespace Rush\Modules\Customer\Repositories;

use Rush\Modules\Repository\RepositoryInterface;

/**
 * The NonMemberTransactionInterface contains unique method signatures related to NonMemberTransaction object
 */
interface NonMemberTransactionInterface extends RepositoryInterface
{
    public function getByMerchant($merchantId);
}
