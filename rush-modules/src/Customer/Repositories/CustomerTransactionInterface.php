<?php

namespace Rush\Modules\Customer\Repositories;

use Rush\Modules\Repository\RepositoryInterface;

interface CustomerTransactionInterface extends RepositoryInterface
{
    public function getByTransactionReferenceCode($transactionReferenceCode);
}