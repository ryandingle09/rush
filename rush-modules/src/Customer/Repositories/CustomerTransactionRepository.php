<?php

namespace Rush\Modules\Customer\Repositories;

use Rush\Modules\Customer\Models\CustomerTransactionsModel;
use Rush\Modules\Customer\Repositories\CustomerTransactionInterface;
use Rush\Modules\Repository\AbstractRepository;

class CustomerTransactionRepository extends AbstractRepository implements CustomerTransactionInterface
{
    public function __construct(CustomerTransactionsModel $customerTransactionsModel)
    {
        parent::__construct($customerTransactionsModel);
    }

    public function getByTransactionReferenceCode($transactionReferenceCode)
    {
        return $this->model->where(['transactionReferenceCode' => $transactionReferenceCode])->first();
    }

    public function getByBranchId($branch_id)
    {
        return $this->model->where('branchId', $branch_id)->get();
    }
}
