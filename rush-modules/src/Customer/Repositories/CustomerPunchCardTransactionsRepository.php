<?php

namespace Rush\Modules\Customer\Repositories;

use Rush\Modules\Customer\Models\CustomerPunchCardTransactionsModel;
use Rush\Modules\Customer\Repositories\CustomerPunchCardTransactionsInterface;
use Rush\Modules\Repository\AbstractRepository;

/**
 * A repository class would encapsulate each Eloquent Model class
 * and be responsible for CRUD operations on the database.
 */
class CustomerPunchCardTransactionsRepository extends AbstractRepository implements CustomerPunchCardTransactionsInterface
{
    public function __construct(CustomerPunchCardTransactionsModel $customerPunchCardTransactionsModel)
    {
        parent::__construct($customerPunchCardTransactionsModel);
    }

    public function getByOrNoAndCustomerId($orNo,
        $customerId)
    {
        return $this->model->where([
            'customer_id' => $customerId,
            'or_no' => $orNo
        ])->first();
    }

    public function getPurchasePunchcardByOrNo($or_no, $merchant_id){
        return $this->model->where([
            'or_no'=> $or_no,
            'merchant_id' => $merchant_id,
            'type' => "purchase-package"
        ])->first();
    }

    public function getByReferenceNo($reference_no){
        return $this->model->where('transaction_ref', $reference_no)->first();
    }

    public function getByBranchId($branch_id){
        return $this->model->where('branch_id', $branch_id)->get();
    }

    public function addTransaction(
        $employee,
        $card,
        $amount = null,
        $reward_id = 0,
        $status = false,
        $type = 'earn',
        $channel = '',
        $stamps = 0,
        $card_id = null,
        $or_no = null,
        $branch_id = null,
        $remarks = null
    ) {
        $channel = ( ! is_null($channel) ? strtolower($channel) : 'merchantapp' );
        $reference = time();
        $transaction = new $this->model;
        $transaction->customer_id = $card->customer->id;
        $transaction->employee_id = $employee ? $employee->userId : null;
        $transaction->merchant_id = $card->customer->merchant->id;
        $transaction->date_punched = date('Y-m-d G:i:s', $reference);
        $transaction->promo_id = $card->punchcard->id;
        $transaction->amount = $amount;
        $transaction->reward_id = $reward_id;
        $transaction->transaction_ref = $reference;
        $transaction->stamp_card_id = $card->id;
        $transaction->void = 0;
        $transaction->status = $status;
        $transaction->type = $type;
        $transaction->channel = $channel;
        $transaction->stamps = $stamps;
        $transaction->card_id = $card_id;
        $transaction->or_no = $or_no;
        $transaction->branch_id = $branch_id ? $branch_id : ($employee ? $employee->branch->id : null);
        $transaction->remarks = $remarks;
        $transaction->save();

        return $transaction;
    }

    public function getLastTransaction( $customer_id )
    {
        return $this->model
            ->where('customer_id', $customer_id)
            ->orderBy('created_at', 'desc')
            ->first();
    }
}
