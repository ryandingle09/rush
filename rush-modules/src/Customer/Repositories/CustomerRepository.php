<?php

namespace Rush\Modules\Customer\Repositories;

use Carbon\Carbon;
use Rush\Modules\Customer\Models\CustomerMembershipCardModel;
use Rush\Modules\Helpers\Facades\StorageHelper;
use Rush\Modules\Customer\Models\CustomerModel;
use Rush\Modules\Customer\Models\CustomerTransactionsModel;
use Rush\Modules\Customer\Models\CustomerPointsModel;
use Rush\Modules\Customer\Models\RedeemModel;
use Rush\Modules\Customer\Models\RedeemedItemsModel;
use Rush\Modules\Customer\Models\CustomerBadgeModel;
use Rush\Modules\Customer\Models\CustomerPunchCardTransactionsModel;
use Rush\Modules\Merchant\Models\MerchantEmployeesModel;
use Rush\Modules\Merchant\Models\MerchantModel;
use Rush\Modules\Merchant\Models\MerchantRewardsModel;
use Rush\Modules\Merchant\Models\MerchantGlobeRewardsModel;
use Rush\Modules\Merchant\Models\MerchantPunchCardRewardsModel;
use Rush\Modules\Merchant\Models\MerchantCodeModel;
use Rush\Modules\Merchant\Models\OrderModel;
use Rush\Modules\Merchant\Models\OrderItemModel;
use Rush\Modules\Merchant\Models\OrderStatusHistoryModel;
use Rush\Modules\Customer\Models\CustomerReferralModel;
use Rush\Modules\Customer\Models\NatConMessageModel;
use Rush\Modules\Customer\Models\CustomerActivationCodesModel;
use Rush\Modules\Customer\Models\CustomerScannedQrCodesModel;
use Rush\Modules\Customer\Models\CustomerDeliveryDetailModel;
use Rush\Modules\Customer\Models\CustomerPaymentDetailModel;
use Rush\Modules\Repository\AbstractRepository;
use Rush\Modules\Customer\Models\NonMemberTransactionModel;
use Rush\Modules\Merchant\Repositories\AmaxApiRepository;
use Ramsey\Uuid\Uuid;
use App\Library\SMS;
use Mail;
use Rush\Modules\Customer\Models\CustomerClassPackageModel;
use Rush\Modules\Customer\Models\CustomerClassPackagePunchcardModel;
use Rush\Modules\Customer\Models\CustomerClassPackagePunchcardStampsModel;
use Rush\Modules\Attendance\Models\ClientModel;
use Intervention\Image\ImageManager;
use Rush\Modules\Merchant\Models\MerchantVotesModel;
use Rush\Modules\Customer\Models\CustomerQuizAnswerModel;
use Rush\Modules\Merchant\Models\MerchantAvatarModel;
use Rush\Modules\Customer\Models\PnpExistingCustomerModel;
use Rush\Modules\Customer\Models\PnpNewCardNumberModel;

/**
 * Class CustomerRepository
 * @package Rush\Modules\Customer\Repositories
 */
class CustomerRepository  extends AbstractRepository implements  CustomerInterface
{

    public $basicColumns = [
        'uuid as id',
        'profile_id',
        'fullName as name',
        'email',
        'mobileNumber as mobile_no',
        'birthDate as birthdate',
        'gender',
        'profileImage as photo',
        'fbId as fb_id',
        'email_verified',
        'mobile_verified',
        'created_at as registration_date',
    ];

    public $punchCardTransactionsColumns = [
        'uuid',
        'type',
        'amount',
        'void',
        'updated_at',
        'created_at',
    ];

    public $loyaltyTransactionsColumns = [
        'id',
        'transactionReferenceCode as reference_code',
        'receiptReferenceNumber as receipt_no',
        'branchId as branch_id',
        'amountPaidWithpoints as amount_paid_with_points',
        'amountPaidWithCash as amount_paid_with_cash',
        'pointsEarned as points_earned',
        'stampsEarned as stamps_earned',
        'transactionType as transaction_type',
        'employee_id',
        'timestamp as date',
    ];

    /**
     * CustomerRepository constructor.
     * @param CustomerModel $customerModel
     */
    public function __construct(CustomerModel $customerModel)
    {
        parent::__construct($customerModel);
    }

    public function auth($merchant_id, $mobile_no, $pin, $fb_id = null)
    {
       if( $fb_id ) {
           return $this->model->where('fbId', $fb_id)->where('merchantId', $merchant_id)->first();
       } else {
           return $this->model->where('mobileNumber', $mobile_no)->where('PIN', $pin)->where('merchantId', $merchant_id)->first();
       }
    }

    protected function mapFields($fields)
    {
        $fields_map = [
            'customerId' => 'id',
            'merchantId' => 'merchant_id',
            'fbId' => 'fb_id',
            'mobileNumber' => 'mobile_no',
            'PIN' => 'pin',
            'birthDate' => 'birthdate',
            'email' => 'email',
        ];
        $formatted_fields = [];
        foreach ($fields as $key => $value) {
            if (in_array($key, $fields_map)) {
                $index = array_search($key, $fields_map);
                $formatted_fields[$index] = $value;
            } else {
                $formatted_fields[$key] = $value;
            }
        }

        return $formatted_fields;
    }

    public function getCustomers($args = null)
    {
        if ($args) {
            $args = $this->mapFields($args);
            return $this->model->where($args)->get();
        }
        return $merchant->customers()->get();
    }

    public function returnValidationErrors( $errors ){

        $response['error_code'] = '0x1';
        $response['errors'] = $errors;

        return $response;

    }

    public function createCustomer( $merchant_id, $request, $channel = null ){

        $this->model->merchantId = $merchant_id;
        $this->model->fullName = $request['name'];
        $this->model->email = $request['email'];
        $this->model->mobileNumber = $request['mobile_no'];
        $this->model->PIN = $request['mpin'];
        $this->model->birthDate = $request['birthdate'];
        $this->model->gender = ucfirst($request['gender']);
        $this->model->profileImage = $request['photo'];
        $this->model->fbId = $request['fb_id'];
        $this->model->email_verified = (int) $request['email_verified'];
        $this->model->mobile_verified = (int) $request['mobile_verified'];
        $this->model->uuid = Uuid::uuid1()->toString();
        $this->model->timestamp = date("Y-m-d H:i:s");
        $this->model->registrationChannel = ( ! is_null($channel) ? strtolower($channel) : 'Merchant App' );
        $this->model->save();

        // update profile id upon customer creation.
        $this->model->profile_id = $this->generateCustomerProfileId( $merchant_id, $this->model->customerId );
        $this->model->save();

        $response['error_code'] = '0x0';
        $response['data'] = $request;
        $response['data']['photo'] = StorageHelper::repositoryUrlForView($request['photo']);
        $response['data']['uuid'] = $this->model->uuid;
        $response['data']['email_verified'] = (int) $request['email_verified'];
        $response['data']['mobile_verified'] = (int) $request['mobile_verified'];
        $response['data']['registration_date'] = $this->model->timestamp;

        return $response;

    }

    public function generateCustomerProfileId( $merchant_id, $customer_id ){

        $year_substr = date('y');
        $merchant_id_substr = substr('0000' . $merchant_id, -4);
        $customer_id_substr = substr('000000' . $customer_id, -6) ;

        return $year_substr . '-' . $merchant_id_substr . '-' . $customer_id_substr;

    }

    public function getSingleCustomer( $uuid ){

        $response['error_code'] = '0x0';
        $response['data'] = $this->model->select( $this->basicColumns )->where('uuid', $uuid)->first();

        return $response;

    }

    public function updateCustomer( $uuid, $request ){

        $customer = $this->model->where('uuid', $uuid)->first();
        $customer->fullName = $request['name'];
        $customer->email = $request['email'];
        $customer->mobileNumber = $request['mobile_no'];
        $customer->birthDate = $request['birthdate'];
        $customer->gender = $request['gender'];
        $customer->profileImage = $request['photo'];
        $customer->fbId = $request['fb_id'];
        $customer->email_verified = $request['email_verified'];
        $customer->mobile_verified = $request['mobile_verified'];
        $customer->save();

        $response['error_code'] = '0x0';
        $response['data'] = $request;
        $response['data']['photo'] = StorageHelper::repositoryUrlForView($request['photo']);

        return $response;

    }

    public function checkIfMobileNumberNotUnique($mobile_number, $customer_id, $merchant_id){
        return ($this->model->where('customerId', '!=', $customer_id)->where('mobileNumber', $mobile_number)->where('merchantId', $merchant_id)->count() > 0);
    }

    public function checkIfEmailAddressNotUnique($email_address, $customer_id, $merchant_id){
        return ($this->model->where('customerId', '!=', $customer_id)->where('email', $email_address)->where('merchantId', $merchant_id)->count() > 0);
    }

    public function checkIfClientCodeNotUnique($client_code, $customer_id, $merchant_id){
        return ($this->model->where('customerId', '!=', $customer_id)->where('client_code', $client_code)->where('merchantId', $merchant_id)->count() > 0);
    }

    public function checkIfClientCodeNotValid($client_code, $merchant_id){
        return (ClientModel::where('code', $client_code)->where('merchant_id', $merchant_id)->count() <= 0);
    }

    public function checkIfCustomerHasPriorClientCode($customer_id, $merchant_id){
        $customer = $this->model->where('customerId', $customer_id)->where('merchantId', $merchant_id)->first();

        return (!empty($customer->client_code));
    }

    public function createCustomerFromCms($input)
    {
        $customer = new CustomerModel();
        $customer->uuid = Uuid::uuid1()->toString();
        $customer->timestamp = date("Y-m-d H:i:s");

        $customer->merchantId = $input['merchantId'];
        $customer->registrationChannel = 'Web';
        $customer->fullName = $input['fullName'];
        $customer->mobileNumber = $input['mobileNumber'];
        $customer->email = $input['email'];
        $customer->updated_at = date("Y-m-d H:i:s");
        if (isset($input['client_code']))
            $customer->client_code = $input['client_code'];
        if (isset($input['birthDate']))
            $customer->birthDate = $input['birthDate'];
        if (isset($input['gender']))
            $customer->gender = strtolower($input['gender']);

        // apply data manipulations
        if (empty($input['PIN']))
            $customer->PIN = substr($input['mobileNumber'], -4);
        else
            $customer->PIN = $input['PIN'];

        $customer->save();

        return $customer;
    }

    public function editCustomerFromCms($request){
        $customer = $this->model->where('customerId', $request->customer_id)->first();
        $membership_card = $customer->membership_cards()->where('status', 1)->first();

        $customer->fullName = $request->client_name;
        $customer->email = $request->email_address;
        $customer->mobileNumber = $request->mobile_number;
        $customer->client_code = $request->client_code;
        $customer->grade_level = $request->grade_level;
        $customer->PIN = $request->PIN;

        if(isset($request->membership_level_id)){
            if($membership_card){
                $membership_card->membership_id = $request->membership_level_id;
                $membership_card->update();
            } else {
                $membership_card = new CustomerMembershipCardModel();

                $membership_card->customer_id = $customer->customerId;
                $membership_card->uuid = $customer->uuid;
                $membership_card->merchant_id = $customer->merchantId;
                $membership_card->membership_id = $request->membership_level_id;
                $membership_card->status = 1;
                $membership_card->card_no = time();
                $membership_card->save();
            }
        }

        $customer->update();

        return $customer;
    }

    public function deleteCustomer( $uuid ){

        $customer = $this->model->where('uuid', $uuid)->first();
        $customer->forceDelete();

        $response['error_code'] = '0x0';

        return $response;

    }

    public function getCustomerPoints( $uuid ){

        $customer = $this->model->where('uuid', $uuid)->first();

        if( $customer ) $response = ['error_code' => '0x0', 'data' => number_format($customer->points->currentpoints, 2, '.', '')];
        else $response = ['error_code' => '0x1', 'message' => 'Customer not found'];

        return $response;

    }

    public function earnPoints( $customer, $employee, $request, $channel = null ){

        // add customer earn transaction.
        $transaction = $this->addCustomerEarn( $customer, $employee, $request, $channel );

        // update customer points.
        $this->addCustomerPoints( $customer, $transaction->pointsEarned );

        return $transaction;

    }

    public function addCustomerEarn( $customer, $employee, $request, $channel = null ){

        $points_earned = ( isset($request['points']) ? $request['points'] : $customer->merchant->getEarningPointsOfAmount($request['amount']) );
        $conversion_settings_id = ( isset($request['amount']) ? $customer->merchant->conversion_settings->id : null );
        $amount_paid_with_cash = ( isset($request['amount']) ? $request['amount'] : 0 );
        $channel = ( ! is_null($channel) ? strtolower($channel) : 'merchantapp' );

        $transaction = new CustomerTransactionsModel;

        if( $employee ){
            $transaction->branchId = $employee->branchId ? $employee->branchId : $request['seed_branch'];
            $transaction->employee_id = $employee->userId;
            $transaction->userId = $employee->userId;
        }

        $transaction->transactionReferenceCode = str_random(12);
        $transaction->transactionType = 'earn';
        $transaction->conversionSettingsId = $conversion_settings_id;
        $transaction->customerId = $customer->customerId;
        $transaction->pointsEarned = $points_earned;
        $transaction->amountPaidWithpoints = 0;
        $transaction->amountPaidWithCash = $amount_paid_with_cash;
        $transaction->transactionStatus = 'done';
        $transaction->receiptReferenceNumber = $request['or_no'];
        $transaction->timestamp = date("Y-m-d H:i:s", time());
        $transaction->merchant_id = $customer->merchant->merchantid;
        $transaction->channel = $channel;
        $transaction->save();

        return $transaction;

    }

    // function used by gmovies to earn points.
    public function addCustomerEarnFromBadge( $customer, $request, $channel = null ){

        $transaction = new CustomerTransactionsModel;
        $transaction->transactionReferenceCode = str_random(12);
        $transaction->transactionType = 'earn';
        $transaction->customerId = $customer->customerId;
        $transaction->pointsEarned = $request['points'];
        $transaction->amountPaidWithpoints = 0;
        $transaction->amountPaidWithCash = 0;
        $transaction->transactionStatus = 'done';
        $transaction->timestamp = date("Y-m-d H:i:s", time());
        $transaction->merchant_id = $customer->merchant->merchantid;
        $transaction->channel = ( ! is_null($channel) ? strtolower($channel) : 'merchantapp' );
        $transaction->save();

        return $transaction;

    }

    public function earnBadge( $customer, $request, $channel ){

        // add customer earn transaction.
        $transaction = $this->addCustomerEarnFromBadge( $customer, $request, $channel );

        // add customer points.
        $this->addCustomerPoints( $customer, $request['points'] );

        // earn customer badge.
        $badge = new CustomerBadgeModel;
        $badge->transactionReferenceCode = $transaction->transactionReferenceCode;
        $badge->customer_id = $customer->customerId;
        $badge->badge_group = $request['badge_group'];
        $badge->badge_id = $request['badge_id'];
        $badge->badge_name = $request['badge_name'];
        $badge->validity_date = $request['validity_date'];
        $badge->save();

        return $badge->transactionReferenceCode;

    }

    public function addCustomerPoints( $customer, $points ){

        $customer_points = $customer->points;
        $customer_points->currentpoints = $customer_points->currentpoints + $points;
        $customer_points->totalPoints = $customer_points->totalPoints + $points;
        $customer_points->save();

    }

    public function getBadges( $customer_uuid, $request ){

        $page = (int) $request['page'];
        $limit = (int) $request['limit'];
        $offset = ($page - 1) * $limit;

        $customer = $this->getCustomerByUuid( $customer_uuid );
        $formatted_badges = [];
        $raw_badges = [];

        $badges_query = CustomerBadgeModel
            ::where('customer_id', $customer->customerId)
            ->select('badge_id', 'badge_name', 'badge_group', 'validity_date');

        if( isset($request['badge_group']) ) $badges_query = $badges_query->where('badge_group', $request['badge_group']);

        if( $page >= 1 && $limit >= 1 ){
            $badges_query = $badges_query->offset( $offset );
            $badges_query = $badges_query->limit( $limit );
        }

        $formatted_badges['count'] = $badges_query->count();
        $raw_badges = $badges_query->get();

        foreach( $raw_badges as $badge ){
            $formatted_badges['badges'][] = [
                'badge_id' => (int) $badge->badge_id,
                'badge_name' => $badge->badge_name,
                'badge_group' => $badge->badge_group,
                'validity_date' => $badge->validity_date,
            ];
        }

        return $formatted_badges;

    }

    public function payPoints( $customer, $employee, $request, $channel = null ){

        // add customer pay points transaction.
        $transaction = $this->addCustomerPay( $customer, $employee, $request, $channel );

        // update customer points.
        $this->subtractCustomerPoints( $customer, $transaction->pointsBurn );

        return $transaction;
    }

    public function addCustomerPay( $customer, $employee, $request, $channel = null ){

        $conversion_settings_id = ( isset($request['amount']) ? $customer->merchant->conversion_settings->id : null );
        $amount_paid_with_cash = ( isset($request['amount']) ? $request['amount'] : 0 );
        $channel = ( ! is_null($channel) ? strtolower($channel) : 'merchantapp' );

        $transaction = new CustomerTransactionsModel;

        if( $employee ){
            $transaction->branchId = $employee->branchId;
            $transaction->employee_id = $employee->userId;
        }

        $transaction->transactionReferenceCode = str_random(12);
        $transaction->transactionType = 'paypoints';
        $transaction->conversionSettingsId = $conversion_settings_id;
        $transaction->customerId = $customer->customerId;
        $transaction->userId = $customer->customerId;
        $transaction->pointsEarned = 0;
        $transaction->pointsBurn = $request['points'];
        $transaction->amountPaidWithpoints = $request['points'];
        $transaction->amountPaidWithCash = $amount_paid_with_cash;
        $transaction->transactionStatus = 'done';
        $transaction->receiptReferenceNumber = $request['or_no'];
        $transaction->timestamp = date("Y-m-d H:i:s", time());
        $transaction->merchant_id = $customer->merchant->merchantid;
        $transaction->channel = $channel;
        $transaction->save();

        return $transaction;

    }

    public function subtractCustomerPoints( $customer, $points ){

        $customer_points = $customer->points;
        $customer_points->currentpoints = $customer_points->currentpoints - $points;
        $customer_points->usedPoints = $customer_points->usedPoints + $points;
        $customer_points->save();

    }

    public function redeemReward( $customer, $reward, $employee, $request, $channel = null ){

        // for globe reward, ignore given $quantity and set it to 1.
        $quantity = ( ! isset($request['quantity']) ? 1 : (int) $request['quantity'] );
        $quantity = ( $reward->is_globe_reward ? 1 : $quantity );

        // globe reward's redeem status should be automatically 1.
        // redeem status should be automatically set to 1 for globe reward
        // or if there's employee in the redemption.
        $redeem_status = ( $reward->is_globe_reward ? 1 : 0 );
        $redeem_status = ( $employee ? 1 : $redeem_status );

        $amax_transaction_id = 0;


        // if reward is a globe reward, call amax api.
        if( $reward->is_globe_reward ){

            $amaxApiRepository = new AmaxApiRepository;
            $address = substr($customer->mobileNumber, 1);
            $amax_result = $amaxApiRepository->redeemReward( $address, $reward->name );

            if( isset($amax_result['outboundRewardRequest']['transaction_id']) ){
                $amax_transaction_id = (int) $amax_result['outboundRewardRequest']['transaction_id'];
            }else{
                $message = "Globe reward redemption API failed.";
                $message .= ( isset($amax_result['error']) ? " {$amax_result['error']}" : '' );
                return ['error_code' => '0x1', 'message' => $message];
            }

        }


        // add customer transaction.
        $transaction = $this->addCustomerRedeem( $employee, $customer, $reward, $channel, $amax_transaction_id, $quantity );


        // update customer points.
        $customer_points = $customer->points;
        $customer_points->currentpoints = $customer_points->currentpoints - $transaction->pointsRedeem;
        $customer_points->usedPoints = $customer_points->usedPoints + $transaction->pointsRedeem;
        $customer_points->save();


        // add itemm in Redeem table.
        $redeem = new RedeemModel;
        $redeem->redeemReferenceCode = $transaction->transactionReferenceCode;
        $redeem->customerId = $customer->customerId;
        $redeem->status = $redeem_status;
        $redeem->timestamp = $transaction->timestamp;
        $redeem->quantity = $quantity;
        $redeem->save();
        $redeem_id = $redeem->redeemId;

        // add itemm in redeemedItems.
        $redeemedItem = new RedeemedItemsModel;
        $redeemedItem->redeemId = $redeem_id;
        $redeemedItem->redeemItemId = $reward->redeemItemId;
        $redeemedItem->include = 1;
        $redeemedItem->save();

        return $transaction;

    }

    public function addCustomerRedeem( $employee, $customer, $reward, $channel = null, $amax_transaction_id = 0, $quantity ){

        $channel = ( ! is_null($channel) ? strtolower($channel) : 'merchantapp' );
        $transaction = new CustomerTransactionsModel;

        if( $employee ){
            $transaction->branchId = $employee->branchId;
            $transaction->employee_id = $employee->userId;
        }

        $transaction->transactionReferenceCode = str_random(12);
        $transaction->transactionType = 'redeem';
        $transaction->conversionSettingsId = $customer->merchant->conversion_settings->id;
        $transaction->customerId = $customer->customerId;
        $transaction->userId = $customer->customerId;
        $transaction->pointsEarned = 0;
        $transaction->pointsRedeem = ((float) $reward->pointsRequired * $quantity);
        $transaction->amountPaidWithpoints = 0;
        $transaction->amountPaidWithCash = 0;
        $transaction->transactionStatus = 'done';
        $transaction->receiptReferenceNumber = null;
        $transaction->timestamp = date("Y-m-d H:i:s", time());
        $transaction->merchant_id = $customer->merchant->merchantid;
        $transaction->channel = $channel;
        $transaction->amax_transaction_id = $amax_transaction_id;
        $transaction->save();

        return $transaction;

    }

    public function unRedeemItem( $transactionReferenceCode ){

        $redeem = RedeemModel::where('redeemReferenceCode', $transactionReferenceCode)->first();
        $redeem->status = 0;
        $redeem->save();

    }

    public function getTransactions( $uuid ){

        $customer = $this->model->where('uuid', $uuid)->first();

        if( ! $customer ) return ['error_code' => '0x1', 'message' => 'Customer not found.'];

        $transactions_columns = ( $customer->is_basic ? $this->punchCardTransactionsColumns : $this->loyaltyTransactionsColumns );
        $transactions = $customer->transactions()->select( $transactions_columns )->get();
        $transactions = ( $customer->is_basic ? $this->formatPunchCardTransactions($transactions) : $transactions );

        return ['error_code' => '0x0', 'data' => $transactions];

    }

    public function getTransactionsCap( $uuid ){

        $customer = $this->model->where('uuid', $uuid)->first();

        if( ! $customer ) return ['error_code' => '0x1', 'message' => 'Customer not found.'];

        $cap['earn_transaction_count'] = (int) $customer->day_earn_transactions_count;
        $cap['earn_points_total'] = (float) $customer->day_points_earned_total;
        $cap['redemption_transaction_count'] = (int) $customer->day_redemption_transactions_count;
        $cap['paypoint_transaction_count'] = (int) $customer->day_pay_points_transactions_count;
        $cap['paypoint_points_total'] = (float) $customer->day_points_paid_total;

        return ['error_code' => '0x0', 'data' => $cap];

    }

    public function formatPunchCardTransactions( $raw_transactions ){

        $formatted_transactions = [];

        foreach( $raw_transactions as $transaction ){

            $stamps = 0;
            $reward = "";
            $amount = 0;
            $void = $transaction->void ? true : false;
            $type = 1;

            if( $transaction->type == 'earn' ){

                $type = 1;
                $amount = $transaction->amount;
                $stamps = $transaction->stamps;

            }else{

                $type = 2;
                $reward = $transaction->reward->reward;

            }

            $formatted_transactions[] = [
                'id'  => $transaction->uuid,
                'type' => (int) $type,
                'reward' => (string) $reward,
                'amount' => (float) $amount,
                'stamps' => (int) $stamps,
                'void'  => (bool) $void,
                'date' => $transaction->updated_at ? $transaction->updated_at->format('Y-m-d H:i:s') : ($transaction->created_at ? $transaction->created_at->format('Y-m-d H:i:s') : null)
            ];

        }

        return $formatted_transactions;

    }

    public function getCard( $customer_uuid ){

        $customer = $this->model->where('uuid', $customer_uuid)->first();

        if( ! $customer ) return ['error_code' => '0x1', 'message' => 'Customer not found.'];
        if( ! $customer->card ) return ['error_code' => '0x2', 'message' => 'No ongoing promo.'];

        return ['error_code' => '0x0', 'data' => $customer->card->data];

    }

    public function earnStamps( $customer_uuid, $request, $channel = null ){

        $customer = $this->model->where('uuid', $customer_uuid)->first();
        $employee = MerchantEmployeesModel::where('uuid', $request['employee_uuid'])->first();
        $amount = (float) $request['amount'];

        if( ! $customer ) return ['error_code' => '0x1', 'message' => 'Customer not found.'];
        if( ! $customer->card ) return ['error_code' => '0x2', 'message' => 'No ongoing promo.'];
        if( ! $employee ) return ['error_code' => '0x3', 'message' => 'Employee not found.'];
        if( $amount == 0 ) return ['error_code' => '0x4', 'message' => 'Amount is invalid.'];
        if( $customer->merchant->merchantid != $employee->merchantId ) return ['error_code' => '0x5', 'message' => 'Unauthorized employee.'];

        $transaction = $customer->card->earnStamps($employee, $amount, $channel);
        return ['error_code' => '0x0', 'data' => $transaction->transaction_ref];

    }

    public function redeemStamps( $customer_uuid, $request, $channel = null ){

        $customer = $this->model->where('uuid', $customer_uuid)->first();
        $employee = MerchantEmployeesModel::where('uuid', $request['employee_uuid'])->first();
        $pin = $request['pin'];
        $reward_id = $request['reward_id'];
        $claimed = ( isset($request['claimed']) ? (int) $request['claimed'] : 1 );
        $reward = MerchantPunchCardRewardsModel::where('uuid', $reward_id)->first();

        if( ! $customer ) return ['error_code' => '0x1', 'message' => 'Customer not found.'];
        if( ! $customer->card ) return ['error_code' => '0x2', 'message' => 'No ongoing promo.'];
        if( ! $employee ) return ['error_code' => '0x3', 'message' => 'Employee not found.'];
        if( $pin != $customer->PIN ) return ['error_code' => '0x4', 'message' => 'Customer 4-Digit pin is invalid.'];
        if( $customer->merchant->merchantid != $employee->merchantId ) return ['error_code' => '0x5', 'message' => 'Unauthorized employee.'];
        if( $reward_id == '' || ! $reward ) return ['error_code' => '0x6', 'message' => 'Invalid reward id.'];
        if( $customer->card->isRewardClaimed($reward->id) ) return ['error_code' => '0x7', 'message' => 'Customer already redeemed this reward.'];
        if( $reward->no_stamps > count($customer->card->stamps) ) return ['error_code' => '0x8', 'message' => "Customer doesn't have enough stamps."];
        if( $claimed != 0 && $claimed != 1 ) return ['error_code' => '0x9', 'message' => 'Invalid claimed value.'];

        $transaction = $customer->card->redeemStamps($employee, $reward, $claimed, $channel);
        return ['error_code' => '0x0', 'data' => $transaction->transaction_ref];

    }

    public function voidStamp( $customer_uuid, $request ){

        $customer = $this->model->where('uuid', $customer_uuid)->first();
        $employee = MerchantEmployeesModel::where('uuid', $request['employee_uuid'])->first();
        $transaction_reference = $request['transaction_reference'];
        $transaction = CustomerPunchCardTransactionsModel::whereTransactionRef($transaction_reference)->first();

        if( ! $customer ) return ['error_code' => '0x1', 'message' => 'Customer not found.'];
        if( ! $customer->card ) return ['error_code' => '0x2', 'message' => 'No ongoing promo.'];
        if( ! $employee ) return ['error_code' => '0x3', 'message' => 'Employee not found.'];
        if( ! $transaction ) return ['error_code' => '0x4', 'message' => 'Transaction not found.'];
        if( $transaction->customer_id != $customer->customerId ) return ['error_code' => '0x5', 'message' => 'Invalid transaction.'];
        if( $transaction->stamp_card_id != $customer->card->id ) return ['error_code' => '0x6', 'message' => 'Sorry new stamp card has been issued to customer. Voiding of transactions no longer allowed.'];

        if( $transaction->voidStamp( $customer->card ) ) return ['error_code' => '0x0'];

    }

    public function updatePin( $customer_uuid, $request ){

        $customer = $this->model->where('uuid', $customer_uuid)->first();

        if( ! $customer ) return ['error_code' => '0x1', 'message' => 'Customer not found.'];
        if( $customer->PIN != $request['old_pin'] ) return ['error_code' => '0x2', 'message' => 'Invalid old pin.'];
        if( strlen($request['new_pin']) != 4 || (!is_numeric($request['new_pin'])) ) return ['error_code' => '0x3', 'message' => 'M-Pin is invalid.'];

        $customer->PIN = $request['new_pin'];
        $customer->save();

        return ['error_code' => '0x0'];
    }

    public function validateSmsRegistrationContact( $sender, $partials ){

        $merchant_code = MerchantCodeModel::where('code', $partials['merchant_code'])->first();
        $merchant_id = $merchant_code->merchant_id;

        $count = $this->model->where('mobileNumber', $sender)->where('merchantId', $merchant_id)->count();

        return $count;

    }

    public function validateSmsRegistrationEmail( $partials ){

        $merchant_code = MerchantCodeModel::where('code', $partials['merchant_code'])->first();
        $merchant_id = $merchant_code->merchant_id;

        $count = $this->model->where('email', $partials['email'])->where('merchantId', $merchant_id)->count();

        return ( $count == 0 ? true : false );

    }

    public function validateSmsCustomerUpdateEmail( $sender, $partials )
    {
        $merchant_code = MerchantCodeModel::where('code', $partials['merchant_code'])->first();
        $merchant_id = $merchant_code->merchant_id;

        $count = $this->model->where('email', $partials['email'])
            ->where('merchantId', $merchant_id)
            ->where('mobileNumber', '!=', $sender)
            ->count();

        return ( $count == 0 ? true : false );
    }

    public function smsRegister( $sender, $partials ){

        $merchant_code = MerchantCodeModel::where('code', $partials['merchant_code'])->first();

        $this->model->merchantId = $merchant_code->merchant_id;
        $this->model->registrationChannel = 'Sms';
        $this->model->uuid = Uuid::uuid1()->toString();
        $this->model->fullName = $partials['name'];
        $this->model->mobileNumber = $sender;
        $this->model->email = $partials['email'];
        $this->model->PIN = $partials['pin'];
        $this->model->timestamp = date("Y-m-d H:i:s");
        $this->model->save();

        return $this->model;

    }

    public function getMemberReferralParams( $customer, $referral_code ){

        $referrer = $this->model->where('referral_code', $referral_code)->first();

        $params = [
            'customer_referrer_id' => $referrer->customerId,
            'customer_referred_id' => $customer->customerId,
            'merchant_id' => $customer->merchantId
        ];

        return $params;

    }

    public function validateTransferPointsSufficiency( $sender, $partials ){

        $merchant_code = MerchantCodeModel::where('code', $partials['merchant_code'])->first();
        $merchant_id = $merchant_code->merchant_id;

        $customer = $this->model->where('mobileNumber', $sender)->where('merchantId', $merchant_id)->first();
        $current_points = (float) $customer->points->currentpoints;
        $transfer_points = (float) $partials['points'];

        return ( $current_points >= $transfer_points ? true : false );

    }

    public function validateTransferPointsCustomerPin( $sender, $partials ){

        $merchant_code = MerchantCodeModel::where('code', $partials['merchant_code'])->first();
        $merchant_id = $merchant_code->merchant_id;
        $customer = $this->model->where('mobileNumber', $sender)->where('merchantId', $merchant_id)->first();

        return ( $customer->PIN == $partials['pin'] ? true : false );

    }

    public function transferCustomerPoints( $sender, $partials ){

        $merchant_code = MerchantCodeModel::where('code', $partials['merchant_code'])->first();
        $merchant_id = $merchant_code->merchant_id;

        $transferrer = $this->model->where('mobileNumber', $sender)->where('merchantId', $merchant_id)->first();
        $receiver = $this->model->where('mobileNumber', $partials['receiver'])->where('merchantId', $merchant_id)->first();
        $points_transferred = (float) $partials['points'];

        // add customer transaction for the points transferrer.
        $transaction = $this->addPointsTransferrerTransaction( $transferrer, $points_transferred, $transferrer->customerId, $receiver->customerId );

        // add customer transaction for the points receiver.
        $this->addPointsReceiverTransaction( $transferrer, $points_transferred, $transferrer->customerId, $receiver->customerId );

        // subtract the points transferred to the transferrer's points.
        $transferrer_points = $transferrer->points;
        $transferrer_points->currentpoints = $transferrer_points->currentpoints - $points_transferred;
        $transferrer_points->usedPoints = $transferrer_points->usedPoints + $points_transferred;
        $transferrer_points->save();

        // add the points transferred to the receiver's points.
        $receiver_points = $receiver->points;
        $receiver_points->currentpoints = $receiver_points->currentpoints + $points_transferred;
        $receiver_points->totalPoints = $receiver_points->totalPoints + $points_transferred;
        $receiver_points->save();

        return $transaction;

    }

    public function addPointsTransferrerTransaction( $transferrer, $points_transferred, $transferrer_id, $receiver_id ){

        $transaction = new CustomerTransactionsModel;
        $transaction->transactionReferenceCode = str_random(12);
        $transaction->transactionType = 'transfer';
        $transaction->conversionSettingsId = $transferrer->merchant->conversion_settings->id;
        $transaction->customerId = $transferrer->customerId;
        $transaction->transactionStatus = 'done';
        $transaction->timestamp = date("Y-m-d H:i:s", time());
        $transaction->merchant_id = $transferrer->merchant->merchantid;
        $transaction->pointsTransferred = $points_transferred;
        $transaction->transferredFrom = $transferrer_id;
        $transaction->transferredTo = $receiver_id;
        $transaction->channel = 'sms';
        $transaction->save();

        return $transaction;

    }

    public function addPointsReceiverTransaction( $transferrer, $points_transferred, $transferrer_id, $receiver_id ){

        $transaction = new CustomerTransactionsModel;
        $transaction->transactionReferenceCode = str_random(12);
        $transaction->transactionType = 'transfer';
        $transaction->conversionSettingsId = $transferrer->merchant->conversion_settings->id;
        $transaction->customerId = $receiver_id;
        $transaction->transactionStatus = 'done';
        $transaction->timestamp = date("Y-m-d H:i:s", time());
        $transaction->pointsTransferred = $points_transferred;
        $transaction->transferredFrom = $transferrer_id;
        $transaction->transferredTo = $receiver_id;
        $transaction->channel = 'sms';
        $transaction->save();

    }

    public function getAllByNumbers($numbers, $merchant_id)
    {
        return $this->model->where('merchantId', $merchant_id)->whereIn('mobileNumber', $numbers)->get();
    }

    public function forgotPin( $customer ){

        if( ! $customer ) return ['error_code' => '0x1', 'message' => 'Customer not found.'];

        Mail::send('emails.forgot-mpin', ['customer' => $customer, 'program_name' => $customer->merchant->settings->program_name], function ($m) use ($customer){
            $temp_array = explode('@',$customer->merchant->contactPersonEmail);
            $m->from('no-reply@'. $temp_array[1], $customer->merchant->settings->app_name);
            $m->to($customer->email, $customer->fullName)->subject($customer->merchant->settings->program_name . ": Forgot M-Pin");
        });

        return ['error_code' => '0x0', 'message' => 'Your 4-Digit PIN has been sent to your mobile number and to your email ' . $customer->email . "."];

    }

    public function getCustomerByMobile( $mobile_no, $merchant_id = null ){

        $customer = $this->model->where('mobileNumber', $mobile_no);

        if( ! is_null($merchant_id) ) $customer->where('merchantId', $merchant_id);

        $customer = $customer->first();

        return $customer;

    }

    public function saveMemberReferral($data)
    {
        $referral = new CustomerReferralModel;
        $referral->customer_referred_id = $data['customer_referred_id'];
        $referral->customer_referrer_id = $data['customer_referrer_id'];
        return $referral->save();
    }

    public function getCustomerByUuid( $uuid ){

        $customer = $this->model->where('uuid', $uuid)->first();

        return $customer;

    }

    public function getSingleTransaction( $transactionReferenceCode ){

        $transaction = CustomerTransactionsModel::where('transactionReferenceCode', $transactionReferenceCode)->first();

        return $transaction;

    }

    public function getTransactionByOrNo( $or_no, $merchant_id = null ){

        $transaction = CustomerTransactionsModel::where('receiptReferenceNumber', $or_no);
        if( ! is_null($merchant_id) ) $transaction->where('merchant_id', $merchant_id);
        $transaction = $transaction->first();

        return $transaction;

    }

    public function getNonMemberTransactionByOrNo( $or_no, $merchant_id = null ){

        $transaction = NonMemberTransactionModel::where('receiptReferenceNumber', $or_no);
        if( ! is_null($merchant_id) ) $transaction->where('merchant_id', $merchant_id);
        $transaction = $transaction->first();

        return $transaction;

    }

    public function validatePointsTransferSettings( $mobile_no, $partials ){

        $merchant_code = MerchantCodeModel::where('code', $partials['merchant_code'])->first();
        $merchant_id = $merchant_code->merchant_id;

        $customer = $this->getCustomerByMobile( $mobile_no, $merchant_id );

        return $customer->merchant->settings->points_transfer;

    }

    public function getCustomersByMerchant( $merchant_id )
    {
      return $this->model->where('merchantId', $merchant_id)->get();
    }

    public function validateSmsRegistrationReferralCode( $partials ){

        if( trim($partials['referral_code']) == '' ) return true;

        $customer = $this->model->where('referral_code', $partials['referral_code'])->first();

        if( $customer ) return true;

        else false;

    }

    public function updateTransactionAmaxTransactionId( $id, $amax_transaction_id ){

        $transaction = CustomerTransactionsModel::where('id', $id)->first();
        $transaction->amax_transaction_id = $amax_transaction_id;
        $transaction->save();

    }

    public function getAmaxTransaction( $amax_transaction_id ){

        $transaction = CustomerTransactionsModel::where('amax_transaction_id', $amax_transaction_id)->first();
        return $transaction;

    }

    public function getNonMemberTransactions( $merchant, $mobile_no, $email = null ){

        $merchant_id = $merchant->id;
        $customer_login_param = $merchant->settings->customer_login_param;

        $query = NonMemberTransactionModel
            ::where('merchant_id', $merchant_id)
            ->where('status', 0);

        if( $customer_login_param == 'email' ) $query->where('email', $email);
        else $query->where('mobileNumber', $mobile_no);

        $transactions = $query->get();

        return $transactions;

    }

    public function activateNonMemberTransactions( $customer_uuid, $non_member_transactions ){

        $customer = $this->model->where('uuid', $customer_uuid)->first();
        $customer_login_param = $customer->merchant->settings->customer_login_param;
        $transactions = [];
        $total_points = 0;

        foreach( $non_member_transactions as $row ){
            $transactions[] = [
                'customerId' => $customer->customerId,
                'transactionReferenceCode' => $row->transactionReferenceCode,
                'branchId' => $row->branchId,
                'transactionType' => $row->transactionType,
                'conversionSettingsId' => $row->conversionSettingsId,
                'employee_id' => $row->employee_id,
                'userId' => $row->userId,
                'pointsEarned' => $row->pointsEarned,
                'amountPaidWithCash' => $row->amountPaidWithCash,
                'transactionStatus' => $row->transactionStatus,
                'receiptReferenceNumber' => $row->receiptReferenceNumber,
                'timestamp' => $row->timestamp,
                'merchant_id' => $row->merchant_id,
                'channel' => $row->channel
            ];

            $total_points += (float) $row->pointsEarned;
        }

        // insert non member transactions to customer transactions.
        CustomerTransactionsModel::insert( $transactions );

        // insert customer points record.
        $customer_points = $customer->points;
        $customer_points->totalPoints = $customer_points->totalPoints + $total_points;
        $customer_points->currentpoints = $customer_points->currentpoints + $total_points;
        $customer_points->save();

        // activate the non member transactions.
        $query = NonMemberTransactionModel
            ::where('merchant_id', $customer->merchantId)
            ->where('status', 0);

        if( $customer_login_param == 'email' ) $query->where('email', $customer->email);
        else $query->where('mobileNumber', $customer->mobileNumber);

        $query->update(['status' => 1]);

    }

    public function earnNonMemberPoints( $employee, $request, $channel = null ){

        // earn non member points.
        $transaction = NonMemberTransactionModel::earnPoints( $employee, $request, $channel );

        return $transaction;

    }

    public function getSingleNonMemberTransaction( $transactionReferenceCode ){

        $transaction = NonMemberTransactionModel::where('transactionReferenceCode', $transactionReferenceCode)->first();

        return $transaction;

    }

    public function issueReward( $customer_uuid, $request ){

        $pin = $request['pin'];
        $reward_id = $request['reward_id'];
        $customer = $this->model->where('uuid', $customer_uuid)->first();
        $employee = MerchantEmployeesModel::where('uuid', $request['employee_uuid'])->first();
        $reward = MerchantPunchCardRewardsModel::where('uuid', $reward_id)->first();

        if( ! $customer ) return ['error_code' => '0x1', 'message' => 'Customer not found.'];
        if( ! $customer->card ) return ['error_code' => '0x2', 'message' => 'No ongoing promo.'];
        if( ! $employee ) return ['error_code' => '0x3', 'message' => 'Employee not found.'];
        if( $pin != $customer->PIN ) return ['error_code' => '0x4', 'message' => 'Customer 4-Digit PIN is invalid.'];
        if( $customer->merchant->merchantid != $employee->merchantId ) return ['error_code' => '0x5', 'message' => 'Unauthorized employee.'];
        if( trim($reward_id) == '' || ! $reward ) return ['error_code' => '0x6', 'message' => 'Invalid reward id.'];

        // issue reward.
        $transaction_ref = $customer->card->issueReward($reward_id, $employee);

        if( $transaction_ref === false ) return ['error_code' => '0x7', 'message' => 'Reward redeemed not found.'];

        return ['error_code' => '0x0', 'data' => $transaction_ref];

    }

    public function getPunchcardTransaction( $transaction_ref ){

        $punchcard_transaction = CustomerPunchCardTransactionsModel::where('transaction_ref', $transaction_ref)->first();

        return $punchcard_transaction;

    }

    /**
     *  Get customer by ID or UUID
     * @param  integer|string $id
     * @param  array $args
     * @return mixed|null|\Rush\Modules\Customer\Models\CustomerModel
     */
    public function find($id, $args)
    {
        if (is_int($id)) {
            $args = array_merge($args, ['customerId' => $id]);
        } else {
            $args = array_merge($args, ['uuid' => $id]);
        }

        return $this->model->where($args)->first();
    }

    public function countAllCustomer($request){
        return $this->model->where('merchantId', $request->merchant_id)->count();
    }

    public function countFilterData($query){
        $query = clone $query;
        return $query->count();
    }

    public function getCustomerData($query, $request)
    {
        $limit = $request->length;
        $query = clone $query;

        if($limit == -1){
            $filteredCount = $this->countFilterData($query);
            $limit = ($filteredCount > 10) ? $filteredCount : $this->countAllCustomer($request);
        }

        return $query->offset($request->start)->limit($limit)->get();
    }

    public function getCustomerWithReserationsDetails($customer_id)
    {
        return $this->model->where('customerId', $customer_id)->first();
    }

    public function buildCustomerQuery($request)
    {
        $customer_table_columns = array(
            'Customer.timestamp',
            'Customer.fullName',
            'Customer.client_code',
            'Customer.mobileNumber',
            'Customer.email',
            'Customer.birthDate',
            'Customer.gender',
            'Customer.registrationChannel',
            'Branches.branchName',
            'Customer.uuid',
            'Customer.attendee_status',
            'Customer.firstName',
            'Customer.lastName',
            'Customer.middleName',
            'Customer.grade_level',
            'Customer.PIN'
        );

        return $this->model
            ->leftjoin('Branches', 'Customer.branch_id', '=', 'Branches.id')
            ->leftjoin('customer_cards', 'Customer.customerId', "=", "customer_cards.customer_id")
            ->leftjoin('customer_memberships', 'customer_cards.membership_id', "=", "customer_memberships.id")
            ->where('Customer.merchantId', $request->merchant_id)
            ->where(function($query) use ($request) {
                $query->where('Customer.fullName', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('Customer.client_code', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('Customer.mobileNumber', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('Customer.email', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('Customer.birthDate', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('Customer.gender', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('Customer.registrationChannel', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('Branches.branchName', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('Customer.firstName', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('Customer.lastName', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('Customer.middleName', 'like', '%' . $request->search['value'] . '%');
            })
            ->orderBy($customer_table_columns[$request->order[0]['column']], $request->order[0]['dir'])
            ->select(
                'Customer.customerId',
                'Customer.timestamp',
                'Customer.fullName',
                'Customer.client_code',
                'Customer.mobileNumber',
                'Customer.email',
                'Customer.birthDate',
                'Customer.gender',
                'Customer.registrationChannel',
                'Branches.branchName as branchName',
                'Customer.uuid',
                'Customer.attendee_status',
                'Customer.firstName',
                'Customer.lastName',
                'Customer.middleName',
                'Customer.grade_level',
                'Customer.PIN',
                'customer_memberships.name as membership_level',
                'customer_memberships.id as membership_id'
            );
    }

    public function buildCustomerPointsQuery($request)
    {
        $points_table_columns = array(
            'Customer.timestamp',
            'Customer.fullName',
            'Customer.mobileNumber',
            'customerPoints.totalPoints',
            'customerPoints.usedPoints',
            'customerPoints.currentPoints',
        );

        return $this->model
            ->leftJoin('customerPoints', 'Customer.customerId', '=', 'customerPoints.customerId')
            ->where('Customer.merchantId', '=', $request->merchant_id)
            ->where(function($query) use ($request) {
                $query->where('Customer.fullName', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('Customer.mobileNumber', 'like', '%' . $request->search['value'] . '%');
            })
            ->orderBy($points_table_columns[$request->order[0]['column']], $request->order[0]['dir'])
            ->select('Customer.timestamp', 'Customer.fullName', 'Customer.mobileNumber', 'customerPoints.totalPoints as totalPoints', 'customerPoints.usedPoints as usedPoints', 'customerPoints.currentPoints as currentPoints');
    }

    public function earnClientStamps($client_id, $employee_uuid, $reference)
    {
        $customer = $this->getCustomers(['client_code' => $client_id])->first();
        if ($customer) {
            $employee = MerchantEmployeesModel::where('uuid', $employee_uuid)->first();

            if (! $employee) return false;

            $transaction = $customer->card->earnClientStamps($employee, $reference);

            return $transaction;
        }

        return false;
    }

    public function getUnclaimedRewards( $customer_uuid, $employee ){

        $customer = $this->getCustomerByUuid( $customer_uuid );
        $redemptions = $customer->unclaimed_redemptions;
        $branch_id = ( $employee ? (int) $employee->branchId : 0 );
        $rewards = [];

        foreach( $redemptions as $redemption ){

            $branch_ids = $redemption->reward['branch_ids'];

            // return only the rewards that are claimable in employee's branch.
            if( in_array( $branch_id, $branch_ids) || in_array(0, $branch_ids) ){
                $rewards[] = [
                    'uuid' => $redemption->uuid,
                    'date' => $redemption->timestamp,
                    'quantity' => (int) $redemption->quantity,
                    'claimed' => (int) $redemption->claimed,
                    'reward' => $redemption->reward
                ];
            }

        }

        return $rewards;

    }

    public function getRedemptionByUuid( $redemption_uuid ){

        $redemption = RedeemModel::where('uuid', $redemption_uuid)->first();
        return $redemption;

    }

    public function claimReward( $customer, $employee, $redemption, $request ){

        $claim_quantity = ( isset($request['quantity']) ? (int) $request['quantity'] : $redemption->quantity );

        $redemption->transaction->employee_id = $employee->userId;
        $redemption->transaction->customerId = $customer->customerId;
        $redemption->transaction->userId = $employee->userId;
        $redemption->transaction->branchId = $employee->branchId;
        $redemption->transaction->conversionSettingsId = $customer->merchant->conversion_settings->id;
        $redemption->transaction->save();

        // set the claimed quantity.
        $redemption->claimed = (int) $redemption->claimed + $claim_quantity;

        // if claimed quantity is equal to redeemed quantity, set the redeem status to 1.
        if( $redemption->claimed >= $redemption->quantity ) $redemption->status = 1;

        $claim_result = $redemption->save();

        return $claim_result;

    }

    /**
     * store Attendee as a partial Customer object
     * @param  array $input
     */
    public function storeAttendee($input)
    {
        $customer = null;
        if (array_key_exists('uuid', $input)) {
            $customer = $this->getCustomerByUuid($input['uuid']);
        }
        if (!$customer)  {
            $customer = new CustomerModel();
            $customer->uuid = Uuid::uuid1()->toString();
            $customer->timestamp = array_key_exists('timestamp', $input) ? $input['timestamp'] : date("Y-m-d H:i:s");
            $customer->created_at = date("Y-m-d H:i:s");
        }

        $customer->merchantId = $input['merchantId'];
        $customer->registrationChannel = 'Customer App';
        $customer->fullName = $input['fullName'];
        $customer->mobileNumber = $input['mobileNumber'];
        $customer->email = $input['email'];
        $customer->PIN = $input['PIN'];
        $customer->attendee_status = $input['attendee_status'];
        $customer->updated_at = date("Y-m-d H:i:s");
        $customer->save();

        return $customer;
    }

    public function confirmAttendee($uuid)
    {
        $customer = $this->getCustomerByUuid($uuid);
        $customer->attendee_status = 1;
        $customer->updated_at = date("Y-m-d H:i:s");
        $customer->save();

        return $customer;
    }

    /**
     * @param \Rush\Modules\Customer\Models\CustomerModel
     * @param int $customer_class_package_id
     * @return bool|\Carbon\Carbon
     */
    public function getClassPackageExpiration(CustomerModel $customer, $customer_class_package_id)
    {
        $customer_class_package = $customer->class_packages()->where('id', $customer_class_package_id)->first();
        if (!$customer_class_package)
            return false;

        $package = $customer_class_package->package;
        switch ($package->validity) {
            case '1 day':
                $expiration_date = $customer_class_package->start_date;
                break;
            case '1 week':
                $expiration_date = $customer_class_package->start_date->addWeek()->subDay();
                break;
            case '2 weeks':
                $expiration_date = $customer_class_package->start_date->addWeeks(2)->subDay();
                break;
            case '1 month':
                $expiration_date = $customer_class_package->start_date->addMonth()->subDay();
                break;
            case '2 months':
                $expiration_date = $customer_class_package->start_date->addMonths(2)->subDay();
                break;
            case '3 months':
                $expiration_date = $customer_class_package->start_date->addMonths(3)->subDay();
                break;
            case '6 months':
                $expiration_date = $customer_class_package->start_date->addMonths(6)->subDay();
                break;
            case '9 months':
                $expiration_date = $customer_class_package->start_date->addMonths(9)->subDay();
                break;
            case '24 months':
                $expiration_date = $customer_class_package->start_date->addMonths(24)->subDay();
                break;
            case '36 months':
                $expiration_date = $customer_class_package->start_date->addMonths(36)->subDay();
                break;
            default:
                $expiration_date = $customer_class_package->start_date->addMonths(12)->subDay();
        }
        /** @var \Carbon\Carbon $expiration_date */
        $expiration_date->hour = 0;
        $expiration_date->minute = 0;
        $expiration_date->second = 0;

        return $expiration_date;
    }

    public function getClassPackageRedemptionExpiration(CustomerModel $customer, $customer_class_package_id)
    {
        $customer_class_package = $customer->class_packages()->where('id', $customer_class_package_id)->first();

        if (!$customer_class_package)
            return false;

        $expiration = $customer_class_package->end_date;

        if ($customer->merchant->settings->enable_extended_package_expiry) {
            $expiration = $expiration->addDays(7);
        }

        return $expiration->addWeeks(2);
    }

    public function getValidClassPackages(CustomerModel $customer)
    {
        $class_packages = $customer->class_packages;
        $now = Carbon::now();
        $now->hour = 0;
        $now->minute = 0;
        $now->second = 0;
        $packages = [];
        /** @var \Rush\Modules\Customer\Models\CustomerClassPackageModel $class_package */
        foreach ($class_packages as $class_package) {
            $card = $class_package->card;
            $redemption_expiration_date = $this->getClassPackageRedemptionExpiration($customer, $class_package->id);
            $end_date = $class_package->end_date;
            if ($customer->merchant->settings->enable_extended_package_expiry) {
                $end_date = $end_date->addDays(7);
            }
            if ($now->gte($class_package->start_date) && $now->lte($end_date)) {
                    $packages[] = $class_package;
            } else {
                if ($now->lte($redemption_expiration_date)
                    && count($card->stamps) > 0
                    && $card->class_package->package->stamps->count()) {
                    $packages[] = $class_package;
                }
            }
        }

        return $packages;
    }

    public function getClassPackagePunchcardById($class_package_punchcard_id)
    {
        return CustomerClassPackagePunchcardModel::find($class_package_punchcard_id);
    }

    /**
     * Get Class Package by Package Id
     *
     * @param CustomerModel                                   $customer
     * @param int                                             $package_id
     * @return mixed|CustomerClassPackageModel
     */
    public function getClassPackageByPackageId($customer, $package_id)
    {
        $class_packages = $customer->class_packages()->where('class_package_id', $package_id)->get();
        $customer_class_package = [];
        /** @var \Rush\Modules\Customer\Models\CustomerClassPackageModel $class_package */
        foreach ($class_packages as $class_package) {
            $now = Carbon::now();
            $now->hour = 0;
            $now->minute = 0;
            $now->second = 0;
            $package_expiration = $this->getClassPackageExpiration($customer, $class_package->id);
            $package_expiration->hour = 0;
            $package_expiration->minute = 0;
            $package_expiration->second = 0;
            if ($now->diffInDays($class_package->start_date, false) <= 0 && $now->diffInDays($package_expiration, false) >= 0) {
                $customer_class_package = $class_package;
            }
        }

        return $customer_class_package;
    }

    /**
     * Get Class Packages by Start Date
     *
     * @param CustomerModel                                   $customer
     * @param \Carbon\Carbon                                  $start_date
     * @return null|CustomerClassPackageModel[]\Illuminate\Database\Eloquent\Collection
     */
    public function getClassPackagesByStartDate($customer, $start_date)
    {
        return  $customer->class_packages()
                        ->where('start_date', '<=', $start_date->format('Y-m-d').' 00:00:00')
                        ->where('end_date', '>=', $start_date->format('Y-m-d').' 00:00:00')
                        ->get();
    }

    /**
     * Get Class Packages by End Date
     *
     * @param CustomerModel                                   $customer
     * @param \Carbon\Carbon                                  $end_date
     * @return null|CustomerClassPackageModel[]\Illuminate\Database\Eloquent\Collection
     */
    public function getClassPackagesByEndDate($customer, $end_date)
    {
        return  $customer->class_packages()
            ->where('start_date', '<=', $end_date->format('Y-m-d').' 00:00:00')
            ->where('end_date', '>=', $end_date->format('Y-m-d').' 00:00:00')
            ->get();
    }

    /**
     * Get Customer Class Package Punchcard by Package ID
     *
     * @param CustomerModel $customer
     * @param int $class_package_id
     * @return CustomerClassPackageModel|null
     */
    public function getClassPackageById(CustomerModel $customer, $class_package_id)
    {
        return $customer->class_packages()->where('id', $class_package_id)->first();
    }

    public function addClassPackagePunchcardStamp(CustomerModel $customer, CustomerClassPackagePunchcardModel $class_package_punchcard, $employee_id, $remarks = null)
    {
        /** @var \Rush\Modules\Customer\Models\CustomerClassPackagePunchcardModel $class_package_punchcard */
        // add transaction
        $now = Carbon::now();
        $employee = MerchantEmployeesModel::find($employee_id);
        $transaction = new CustomerPunchCardTransactionsModel;
        $transaction->employee_id = $employee ? $employee->id : null;
        $transaction->customer_id = $customer->id;
        $transaction->amount = 0;
        $transaction->date_punched = $now->format('Y-m-d G:i:s');
        $transaction->promo_id = null;
        $transaction->transaction_ref = $now->timestamp;
        $transaction->merchant_id = $customer->merchantId;
        $transaction->void = 0;
        $transaction->stamp_card_id = null;
        $transaction->stamps = 1;
        $transaction->achievement_transaction_id = null;
        $transaction->type = 'earn';
        $transaction->reward_id = 0;
        $transaction->status = 0;
        $transaction->channel = 'merchantapp';
        $transaction->card_id = null;
        $transaction->or_no = null;
        $transaction->branch_id = $employee ? $employee->branchId : null;
        $transaction->class_package_stamp_card_id = $class_package_punchcard->id;
        $transaction->class_package_id = $class_package_punchcard->class_package->id;
        $transaction->remarks = $remarks;
        $transaction->save();

        // save stamp
        $stamp = new CustomerClassPackagePunchcardStampsModel;
        $stamp->class_package_punchcard_id = $class_package_punchcard->id;
        $stamp->status = true;
        $stamp->transaction_id = $transaction->id;
        $stamp->branch_id = $employee ? $employee->branchId : null;
        $stamp->save();

        return $transaction;
    }

    /**
     * Redeem Class Package Punchcard Stamps
     *
     * @param CustomerModel $customer
     * @param \Rush\Modules\ClassManagement\Models\PackageStampModel $reward
     * @param null|int $employee_id
     * @param string $channel
     * @return CustomerPunchCardTransactionsModel
     */
    public function redeemClassPackagePunchcardStamps(CustomerModel $customer, $reward, $employee_id, $channel = 'merchantapp')
    {
        $class_package = $this->getClassPackageByPackageId($customer, $reward->package->id);
        // add transaction
        $now = Carbon::now();
        $employee = MerchantEmployeesModel::find($employee_id);
        $transaction = new CustomerPunchCardTransactionsModel;
        $transaction->employee_id = $employee ? $employee->id : null;
        $transaction->customer_id = $customer->id;
        $transaction->amount = 0;
        $transaction->date_punched = $now->format('Y-m-d G:i:s');
        $transaction->promo_id = null;
        $transaction->transaction_ref = $now->timestamp;
        $transaction->merchant_id = $customer->merchantId;
        $transaction->void = 0;
        $transaction->stamp_card_id = null;
        $transaction->stamps = $reward->stamp_no;
        $transaction->achievement_transaction_id = null;
        $transaction->type = 'redeem';
        $transaction->reward_id = $reward->id;
        $transaction->status = $employee ? 1 : 0;
        $transaction->channel = $channel;
        $transaction->card_id = null;
        $transaction->or_no = null;
        $transaction->branch_id = $employee ? $employee->branchId : null;
        $transaction->class_package_stamp_card_id = $class_package->card->id;
        $transaction->class_package_id = $class_package->id;
        $transaction->save();

        // get un-redeemed stamps
        $stamps = $class_package->card->un_redeemed_stamps->take($reward->stamp_no);
        $stamps->each(function ($stamp) {
            $stamp->redeem = 1;
            $stamp->save();
        });

        return $transaction;
    }

    /**
     * @param CustomerModel  $customer
     * @param string $transaction_reference
     * @return bool|null|\Rush\Modules\Customer\Models\CustomerPunchcardTransactionsModel
     */
    public function voidClassPackagePunchcardStamps(CustomerModel $customer, $transaction_reference)
    {
        $transaction = $customer->transactions()->where('transaction_ref', $transaction_reference)->first();

        $transaction->class_package_stamps->each(function ($stamp) {
            $stamp->void = 1;
            $stamp->save();
        });

        $transaction->void = 1;
        $transaction->save();

        return $transaction;
    }

    /**
     * @param \Rush\Modules\Customer\Models\CustomerModel $customer
     * @param array $data
     * @param int|null $employee_id
     * @param string $channel
     * @return bool|null|\Rush\Modules\Customer\Models\CustomerPunchcardTransactionsModel
     */
    public function addClassPackageToCustomer(CustomerModel $customer, $data, $employee_id = null, $channel = 'merchantapp')
    {
        // add transaction
        $now = Carbon::now();
        $employee = MerchantEmployeesModel::find($employee_id);
        $transaction = new CustomerPunchCardTransactionsModel;
        $transaction->employee_id = $employee ? $employee->id : null;
        $transaction->customer_id = $customer->id;
        $transaction->amount = $data['amount'];
        $transaction->date_punched = $now->format('Y-m-d G:i:s');
        $transaction->promo_id = null;
        $transaction->transaction_ref = $now->timestamp;
        $transaction->merchant_id = $customer->merchantId;
        $transaction->void = 0;
        $transaction->stamp_card_id = null;
        $transaction->stamps = 0;
        $transaction->achievement_transaction_id = null;
        $transaction->type = 'purchase-package';
        $transaction->reward_id = 0;
        $transaction->status = 0;
        $transaction->channel = $channel;
        $transaction->card_id = null;
        $transaction->or_no = $data['or_no'];
        $transaction->branch_id = $employee ? $employee->branchId : null;
        $transaction->class_package_stamp_card_id = 0;

        $class_package = $this->saveCustomerClassPackage( $customer, $data );

        $transaction->class_package_id = $class_package->id;
        $transaction->save();

        $customer->membership_status = true;
        $customer->save();

        return $transaction;
    }

    public function saveCustomerClassPackage( $customer, $data ){

        $start_date = $data['start_date'] ? Carbon::createFromFormat('Y-m-d', $data['start_date']) : Carbon::now();
        $start_date->hour = 0;
        $start_date->minute = 0;
        $start_date->second = 0;
        $class_package = new CustomerClassPackageModel;
        $class_package->class_package_id = $data['package_id'];
        $class_package->customer_id = $customer->id;
        $class_package->start_date = $start_date->toDateTimeString();
        $class_package->branch_ids = isset($data['branch_ids']) ? $data['branch_ids'] : null;
        $class_package->save();

        $expiration_date = $this->getClassPackageExpiration($customer, $class_package->id);
        $expiration_date->hour = 0;
        $expiration_date->minute = 0;
        $expiration_date->second = 0;
        $class_package->end_date = $expiration_date->toDateTimeString();
        $class_package->save();

        return $class_package;

    }

    public function addLoyaltyClassPackageToCustomer(CustomerModel $customer, $data, $employee_id = null, $channel = 'merchantapp'){

        $now = Carbon::now();
        $employee = MerchantEmployeesModel::find($employee_id);
        $transaction = new CustomerTransactionsModel;

        $transaction->transactionReferenceCode = str_random(12);
        $transaction->branchId = $employee ? $employee->branchId : null;
        $transaction->customerId = $customer->id;
        $transaction->userId = $employee ? $employee->id : null;
        $transaction->amountPaidWithCash = $data['amount'];
        $transaction->transactionStatus = 'done';
        $transaction->receiptReferenceNumber = $data['or_no'];
        $transaction->transactionType = 'purchase-package';
        $transaction->timestamp = $now->format('Y-m-d G:i:s');
        $transaction->merchant_id = $customer->merchantId;
        $transaction->employee_id = $employee ? $employee->id : null;
        $transaction->channel = $channel;

        $class_package = $this->saveCustomerClassPackage( $customer, $data );

        $transaction->class_package_id = $class_package->id;
        $transaction->save();

        return $transaction;

    }

    public function renewLoyaltyClassPackage(CustomerModel $customer, $data, $employee_id = null, $channel = 'merchantapp'){

        $now = Carbon::now();
        $employee = MerchantEmployeesModel::find($employee_id);
        $transaction = new CustomerTransactionsModel;

        $transaction->transactionReferenceCode = str_random(12);
        $transaction->branchId = $employee ? $employee->branchId : null;
        $transaction->customerId = $customer->id;
        $transaction->userId = $employee ? $employee->id : null;
        $transaction->amountPaidWithCash = $data['amount'];
        $transaction->transactionStatus = 'done';
        $transaction->receiptReferenceNumber = $data['or_no'];
        $transaction->transactionType = 'renew-package';
        $transaction->timestamp = $now->format('Y-m-d G:i:s');
        $transaction->merchant_id = $customer->merchantId;
        $transaction->employee_id = $employee ? $employee->id : null;
        $transaction->channel = $channel;

        $class_package = $this->renewCustomerClassPackage( $customer, $data );

        $transaction->class_package_id = $class_package->id;
        $transaction->save();

        return $transaction;

    }

    public function renewCustomerClassPackage( $customer, $data ){

        $customer_class_package = CustomerClassPackageModel
            ::where('customer_id', $customer->id)
            ->where('class_package_id', $data['package_id'])
            ->first();

        $package = $customer_class_package->package;


        switch ($package->validity) {
            case '1 day':
                $expiration_date = $customer_class_package->start_date;
                break;
            case '1 week':
                $expiration_date = $customer_class_package->end_date->addWeek()->subDay();
                break;
            case '2 weeks':
                $expiration_date = $customer_class_package->end_date->addWeeks(2)->subDay();
                break;
            case '1 month':
                $expiration_date = $customer_class_package->end_date->addMonth()->subDay();
                break;
            case '2 months':
                $expiration_date = $customer_class_package->end_date->addMonths(2)->subDay();
                break;
            case '3 months':
                $expiration_date = $customer_class_package->end_date->addMonths(3)->subDay();
                break;
            case '6 months':
                $expiration_date = $customer_class_package->end_date->addMonths(6)->subDay();
                break;
            case '9 months':
                $expiration_date = $customer_class_package->end_date->addMonths(9)->subDay();
                break;
            case '24 months':
                $expiration_date = $customer_class_package->end_date->addMonths(24)->subDay();
                break;
            case '36 months':
                $expiration_date = $customer_class_package->end_date->addMonths(36)->subDay();
                break;
            default:
                $expiration_date = $customer_class_package->end_date->addMonths(12)->subDay();
        }


        $customer_class_package->end_date = $expiration_date->toDateTimeString();
        $customer_class_package->save();

        return $customer_class_package;

    }

    public function save_activation_code( $type, $customer_id, $code ){

        CustomerActivationCodesModel
            ::where('type', $type)
            ->where('customer_id', $customer_id)
            ->update(['status' => 0]);

        $activation_code = new CustomerActivationCodesModel;
        $activation_code->type = $type;
        $activation_code->customer_id = $customer_id;
        $activation_code->code = $code;
        $activation_code->status = 1;
        $activation_code->save();

    }

    public function get_activation_code( $type, $customer_id ){

        $code = CustomerActivationCodesModel
            ::where('type', $type)
            ->where('customer_id', $customer_id)
            ->where('status', 1)
            ->value('code');

        return $code;

    }

    public function disable_activation_code( $customer_id, $type = null, $code = null ){

        $model = CustomerActivationCodesModel::where('customer_id', $customer_id);

        if( ! is_null($type) ) $model->where('type', $type);
        if( ! is_null($code) ) $model->where('code', $code);

        $model->where('status', 1);
        $model->update(['status' => 0]);

    }

    public function enable_membership_status( $customer ){

        $customer->membership_status = 1;
        $customer->save();

    }

    public function update_membership_status( $customer, $status ){

        $customer->membership_status = (int) $status;
        $customer->save();

    }

    /**
     * @param \Rush\Modules\Customer\Models\CustomerPunchcardTransactionsModel $earn_stamp_transaction
     * @return null|\Rush\Modules\Customer\Models\CustomerPunchcardTransactionsModel
     */
    public function voidClassPackageRedeemedTransactions($earn_stamp_transaction)
    {
        $punchcard = $earn_stamp_transaction->class_package_punchcard;
        $affected_transactions = [];

        foreach ($punchcard->redeem_transactions as $redeem_transaction) {
            /** @var \Rush\Modules\Customer\Models\CustomerPunchcardTransactionsModel $redeem_transaction) */
            $redeem_stamps = $redeem_transaction->class_package_reward->stamp_no;
            if ($redeem_stamps <= ($earn_stamp_transaction->class_package_stamps->count() + $punchcard->earned_stamps->count())) {
                $redeem_transaction->void = 1;
                $redeem_transaction->save();
                $affected_transactions[] = $redeem_transaction;
            }
        }

        return count($affected_transactions) ? $affected_transactions : null;
    }

    /**
     * @param CustomerModel $customer
     * @param array $filter
     * @return mixed|CustomerClassPackageModel
     */
    public function getClassPackages(CustomerModel $customer, $filter)
    {
        return $customer->class_packages()->where($filter)->get();
    }

    public function redeemStampsToCredit(array $data)
    {
        if ($data['customer']->merchant->settings->enable_stamps_summary_punchcard_redemption) {

            if ($data['punchcard']->rewards->count()) {
                $reward = $data['punchcard']->rewards->first();
            } else {
                $promo_no_stamps = (int) $data['punchcard']->num_stamps;
                $reward = $data['punchcard']->addRewardToStamps($promo_no_stamps);
            }

            $isTransactionExist = CustomerPunchCardTransactionsModel::where('reward_id', $reward->id)
                ->where('stamp_card_id', $data['punchcard']->id)
                ->where('customer_id', $data['customer']->id)
                ->first();
            if ($isTransactionExist) {
                return null;
            }

            $now = Carbon::now();
            $transaction = new CustomerPunchCardTransactionsModel;
            $transaction->employee_id = null;
            $transaction->customer_id = $data['customer']->id;
            $transaction->amount = 0;
            $transaction->date_punched = $now->format('Y-m-d G:i:s');
            $transaction->promo_id = $data['punchcard']->id;
            $transaction->transaction_ref = $now->timestamp;
            $transaction->merchant_id = $data['customer']->merchant->id;
            $transaction->void = 0;
            $transaction->stamp_card_id = $data['cards']->last()->id;
            $transaction->stamps = $data['stamps'];
            $transaction->achievement_transaction_id = null;
            $transaction->type = 'redeem';
            $transaction->reward_id = $reward->id;
            $transaction->status = 0;
            $transaction->channel = 'cms';
            $transaction->card_id = null;
            $transaction->or_no = null;
            $transaction->branch_id = null;
            $transaction->class_package_stamp_card_id = 0;
            $transaction->class_package_id = 0;
            $transaction->save();

            if ($transaction) {
                $data['cards']->each(function($card) {
                    $card->stamps->each(function($stamp) {
                        $stamp->redeem = 1;
                        $stamp->save();
                    });
                });
            }
            return $transaction;
        } else {
            return null;
        }
    }

    public function deactivate_class_package( $customer ){

        $class_packages = $customer->class_packages()->get();
        $now = Carbon::now();
        $now->hour = 0;
        $now->minute = 0;
        $now->second = 0;
        $now->subDay();
        $expired_date = $now->format('Y-m-d G:i:s');

        foreach( $class_packages as $class_package ){

            $class_package->end_date = $expired_date;
            $class_package->save();

        }

    }

    public function getLoyaltyTransactionByClassPackageId($class_package_id){
        return CustomerTransactionsModel::where('class_package_id', $class_package_id)->first();
    }

    public function getPunchcardTransactionByClassPackageId($class_package_id){
        return CustomerPunchCardTransactionsModel::where('class_package_id', $class_package_id)->first();
    }

    public function get_customer_scanned_qr_code_count( $customer_id, $qr_code_id ){

        return CustomerScannedQrCodesModel
            ::where('customer_id', $customer_id)
            ->where('qr_code_id', $qr_code_id)
            ->count();

    }

    public function scan_qr_code( $customer_id, $qr_code, $scan_type = null ){

        $scanned_qr_code = new CustomerScannedQrCodesModel;
        $scanned_qr_code->customer_id = $customer_id;
        $scanned_qr_code->qr_code_id = $qr_code->id;
        $scanned_qr_code->save();

        if( ! is_null($scan_type) && $scan_type == 'earn' ){
            if( $qr_code->load != -1 && $qr_code->load >= $qr_code->points ){

                $qr_code->load = $qr_code->load - $qr_code->points;
                $qr_code->save();

            }
        }
    }

    public function issueVoucherCode(CustomerModel $customer, $receipt_base64, $employee)
    {
        $voucher = $employee->branch->vouchers->first();
        $codes = $voucher->codes->filter(function($code) {
            return $code->send == 0;
        });
        $voucher_code = $codes->first();
        $voucher_code->send = true;
        $voucher_code->save();

        $now = Carbon::now();
        $earn_transaction = new CustomerPunchCardTransactionsModel;
        $earn_transaction->employee_id = $employee ? $employee->id : null;
        $earn_transaction->customer_id = $customer->id;
        $earn_transaction->amount = 0;
        $earn_transaction->date_punched = $now->format('Y-m-d G:i:s');
        $earn_transaction->promo_id = $customer->card->punchcard->id;
        $earn_transaction->transaction_ref = $now->timestamp;
        $earn_transaction->merchant_id = $customer->merchantId;
        $earn_transaction->void = 0;
        $earn_transaction->stamp_card_id = $customer->card->id;
        $earn_transaction->stamps = 1;
        $earn_transaction->achievement_transaction_id = null;
        $earn_transaction->type = 'earn';
        $earn_transaction->reward_id = 0;
        $earn_transaction->status = 0;
        $earn_transaction->channel = 'merchantapp';
        $earn_transaction->card_id = null;
        $earn_transaction->or_no = null;
        $earn_transaction->branch_id = $employee ? $employee->branchId : null;
        $earn_transaction->class_package_stamp_card_id = null;
        $earn_transaction->class_package_id = 0;

        // add reward
        $reward = new MerchantPunchCardRewardsModel;
        $reward->promo_id = $customer->card->punchcard->id;
        $reward->reward = 'Voucher Code: '. $voucher_code->code;
        $reward->include = false;
        $reward->merchant_id = $customer->merchant->id;
        $reward->save();

        $redeem_transaction = new CustomerPunchCardTransactionsModel;
        $redeem_transaction->employee_id = $employee ? $employee->id : null;
        $redeem_transaction->customer_id = $customer->id;
        $redeem_transaction->amount = 0;
        $redeem_transaction->date_punched = $now->format('Y-m-d G:i:s');
        $redeem_transaction->promo_id = $customer->card->punchcard->id;
        $redeem_transaction->transaction_ref = $now->timestamp;
        $redeem_transaction->merchant_id = $customer->merchantId;
        $redeem_transaction->void = 0;
        $redeem_transaction->stamp_card_id = $customer->card->id;
        $redeem_transaction->stamps = 1;
        $redeem_transaction->achievement_transaction_id = null;
        $redeem_transaction->type = 'redeem';
        $redeem_transaction->reward_id = $reward->id;
        $redeem_transaction->status = 0;
        $redeem_transaction->channel = 'merchantapp';
        $redeem_transaction->card_id = null;
        $redeem_transaction->or_no = null;
        $redeem_transaction->branch_id = $employee ? $employee->branchId : null;
        $redeem_transaction->class_package_stamp_card_id = null;
        $redeem_transaction->class_package_id = 0;
        $redeem_transaction->remarks = null;
        $redeem_transaction->voucher_code_id = $voucher_code->id;
        $redeem_transaction->save();

        $disk = StorageHelper::getRepositoryDisk();
        $manager = new ImageManager();
        $image = $manager->make($receipt_base64);
        $voucher_code_filename = urlencode($redeem_transaction->voucher_code->code) . '.png';
        $file_path = 'customers/photos/' . $customer->uuid . '/vouchers/'. $voucher_code_filename;
        $disk->put($file_path, $image->stream()->__toString(), 'public');

        return $redeem_transaction;
    }

    public function get_last_order_number( $merchant_id ){

        $last_order_number = OrderModel
            ::where('merchant_id', $merchant_id)
            ->orderBy('order_number', 'desc')
            ->value('order_number');

        return $last_order_number;

    }

    public function save_order_details( $customer, $order_number, $order_status, $total_amount, $request, $promo_code ){

        $order = new OrderModel;
        $order->merchant_id = $customer->merchantId;
        $order->customer_id = $customer->customerId;
        $order->order_number = $order_number;
        $order->status = $order_status;
        $order->total_amount = $total_amount;

        $order->voucher_id = ( ! empty($promo_code) ? $promo_code->voucher_item_id : 0 );
        $order->promo_code = ( ! empty($promo_code) ? $promo_code->code : null );
        $order->promo_code_discount_amount = ( ! empty($promo_code) ? $promo_code->voucher->get_discount_amount($total_amount) : 0 );
        $order->net_amount = ($total_amount - $order->promo_code_discount_amount);

        $order->delivery_name = $request->delivery_name;
        $order->delivery_mobile_number = $request->delivery_mobile_number;
        $order->delivery_email = $request->delivery_email;
        $order->delivery_building = $request->delivery_building;
        $order->delivery_floor = $request->delivery_floor;
        $order->delivery_street = $request->delivery_street;
        $order->delivery_landmark = $request->delivery_landmark;

        $order->payment_name = $request->payment_name;
        $order->payment_mobile_number = $request->payment_mobile_number;
        $order->payment_change_for = $request->payment_change_for;
        $order->payment_notes = $request->payment_notes;

        $order->save();

        return $order;

    }

    public function save_order_item_details( $customer, $order, $items, $quantities ){

        foreach( $items as $item ){

            $order_item = new OrderItemModel;
            $order_item->merchant_id = $customer->merchantId;
            $order_item->customer_id = $customer->customerId;
            $order_item->order_id = $order->id;
            $order_item->item_id = $item->id;
            $order_item->amount = $item->amount;
            $order_item->quantity = $quantities[ $item->uuid ];
            $order_item->save();

        }

    }

    public function save_order_status_history( $order_id, $status, $channel = null, $user_id = null ){

        $order_status = new OrderStatusHistoryModel;
        $order_status->order_id = $order_id;
        $order_status->status = $status;
        $order_status->channel = $channel;
        $order_status->user_id = $user_id;
        $order_status->save();

    }

    public function save_customer_delivery_details( $customer, $request ){

        $delivery_detail = $customer->delivery_detail;
        $delivery_detail->name = $request->delivery_name;
        $delivery_detail->mobile_number = $request->delivery_mobile_number;
        $delivery_detail->email = $request->delivery_email;
        $delivery_detail->building = $request->delivery_building;
        $delivery_detail->floor = $request->delivery_floor;
        $delivery_detail->street = $request->delivery_street;
        $delivery_detail->landmark = $request->delivery_landmark;
        $delivery_detail->save();

    }

    public function save_customer_payment_details( $customer, $request ){

        $payment_detail = $customer->payment_detail;
        $payment_detail->name = $request->payment_name;
        $payment_detail->mobile_number = $request->payment_mobile_number;
        $payment_detail->save();

    }

    public function add_customer_delivery_transaction( $customer, $order_number, $total_amount ){

        $transaction = new CustomerTransactionsModel;
        $transaction->transactionReferenceCode = str_random(12);
        $transaction->customerId = $customer->customerId;
        $transaction->userId = $customer->customerId;
        $transaction->amountPaidWithCash = $total_amount;
        $transaction->transactionStatus = 'done';
        $transaction->receiptReferenceNumber = $order_number;
        $transaction->transactionType = 'delivery';
        $transaction->timestamp = date("Y-m-d H:i:s", time());
        $transaction->merchant_id = $customer->merchantId;
        $transaction->channel = 'customerapp';
        $transaction->save();

        return $transaction;

    }

    public function get_customer_orders( $customer_id ){

        $orders = OrderModel
            ::where('customer_id', $customer_id)
            ->orderBy('created_at', 'desc')
            ->get();

        return $orders;
    }

    public function updateProfilePhoto(CustomerModel $customer, $file)
    {
        $disk = StorageHelper::getRepositoryDisk();
        try {
            $manager = new ImageManager();
            $image = $manager->make($file)
                        ->fit(200)
                        ->encode('png');
            $filename = md5(time()) . ".png";
            $customer_directory  = 'customers/photos/' . $customer->uuid;
            $file_path = $customer_directory . '/'. $filename;
            if ($customer->profileImage) {
                $photo_path = StorageHelper::rebasePathToRepository($customer->profileImage);
                $existing_photo = $customer_directory . '/'. basename($photo_path);
                $disk->delete($existing_photo);
            }
            $disk->put($file_path, $image->stream()->__toString(), 'public');
            $customer->profileImage = '/repository/' . $file_path;
            $customer->save();
            return true;
        } catch (\Intervention\Image\Exception\NotReadableException $e) {
            return false;
        }
    }

    public function getCustomerByEmail( $email, $merchant_id = null ){

        $customer = $this->model->where('email', $email);

        if( ! is_null($merchant_id) ) $customer->where('merchantId', $merchant_id);

        $customer = $customer->first();

        return $customer;

    }

    public function earnPointsSeeding( $employee, $customer, $data, $channel = 'points-seeding' )
    {
        $conversion_setting = $customer->merchant->conversion_settings;
        $dividend = floatval($data['amount']) / floatval($conversion_setting->earning_peso);
        $points = intval($conversion_setting->earning_points) * $dividend;
        if ($customer->merchant->settings->rounding_off_points) {
            $points = floor($points);
        }
        $transaction = new CustomerTransactionsModel();
        $transaction->transactionReferenceCode = str_random(12);
        $transaction->branchId = $employee->branchId;
        $transaction->transactionType = 'earn';
        $transaction->conversionSettingsId = $conversion_setting->id;
        $transaction->employee_id = $employee->id;
        $transaction->customerId = $customer->id;
        $transaction->userId = $customer->id;
        $transaction->pointsEarned = (float) $points;
        $transaction->amountPaidWithpoints = 0;
        $transaction->amountPaidWithCash = $data['amount'];
        $transaction->transactionStatus = 'done';
        $transaction->receiptReferenceNumber = $data['or_no'];
        $transaction->timestamp = date("Y-m-d H:i:s", time());
        $transaction->merchant_id = $customer->merchant->id;
        $transaction->channel = $channel;
        $transaction->card_id = isset($data['card_id']) ? $data['card_id'] : null;
        $transaction->save();

        return $transaction;
    }

    public function addVote(array $data)
    {
        $vote = new MerchantVotesModel;
        $vote->merchant_id = $data['merchant_id'];
        $vote->customer_id = $data['customer_id'];
        $vote->post_id = $data['post_id'];
        $vote->vote = $data['vote'];
        $vote->save();

        return $vote;
    }

    public function get_customer_reward_redemption_count( $customer_id, $reward_id, $reward_type = 1 ){

        // get how many times a customer has redeemed a reward.
        $query = RedeemModel
            ::join('redeemedItems', 'Redeem.redeemId', '=', 'redeemedItems.redeemId')
            ->join('CustomerTransaction', 'Redeem.redeemReferenceCode', '=', 'CustomerTransaction.transactionReferenceCode')
            ->where('Redeem.customerId', $customer_id)
            ->where('redeemedItems.redeemItemId', $reward_id);

        // this part determines whethere we're querying a regular reward or globe reward.
        if( $reward_type == 0 ) $query->where('CustomerTransaction.amax_transaction_id', 0);
        elseif( $reward_type == 1 ) $query->where('CustomerTransaction.amax_transaction_id', '!=', 0);

        $redemption_count = $query->count();

        return $redemption_count;

    }

    public function saveQuizAnswerData( $quiz_answer_data )
    {
        CustomerQuizAnswerModel::insert( $quiz_answer_data );
    }

    public function getCustomerPersonality( $merchant_id, $customer_id )
    {
        $personality_name = CustomerQuizAnswerModel
            ::where('customer_id', $customer_id)
            ->select(
                'answer_text',
                \DB::raw('COUNT(answer_text) AS answer_count'),
                \DB::raw('
                    CASE
                        WHEN answer_text = "Travel Junkie" THEN 5
                        WHEN answer_text = "Health Buff" THEN 4
                        WHEN answer_text = "Foodie" THEN 3
                        WHEN answer_text = "Style Star" THEN 2
                        WHEN answer_text = "Techie" THEN 1
                    END AS answer_weight
                ')
            )
            ->groupBy('customer_id', 'answer_text')
            ->orderBy('answer_count', 'desc')
            ->orderBy('answer_weight', 'desc')
            ->value('answer_text');

        $personality = MerchantAvatarModel
            ::where('merchant_id', $merchant_id)
            ->where('name', $personality_name)
            ->first();

        return $personality;
    }

    public function validateExistingPnpCardNumber( $card_number, $birthdate )
    {
        $count = PnpExistingCustomerModel
            ::where('card_number', $card_number)
            ->where('birthdate', $birthdate)
            ->where('migrated', 0)
            ->count();

        return $count;
    }

    public function validateNewPnpCardNumber( $card_number )
    {
        $count = PnpNewCardNumberModel
            ::where('code', $card_number)
            ->where('used', 0)
            ->count();

        return $count;
    }

    public function flagRegisterdPnpCardNumber( $card_number )
    {
        // flag the card number and return the model card number.

        $card_number_model = PnpExistingCustomerModel::where('card_number', $card_number)->first();
        if( $card_number_model ){
            $card_number_model->migrated = 1;
            $card_number_model->save();
            return $card_number_model;
        }

        $card_number_model = PnpNewCardNumberModel::where('code', $card_number)->first();
        if( $card_number_model ){
            $card_number_model->used = 1;
            $card_number_model->save();
            return $card_number_model;
        }
    }

    public function updateCustomerLevelFromPnpData( $rush_customer, $points_level_id = null, $pnp_enrollment_date = null )
    {
        $rush_customer->points_level_id = $points_level_id;
        $rush_customer->pnp_enrollment_date = $pnp_enrollment_date;
        $rush_customer->save();
    }

    public function updateCustomerPointsFromPnpData( $rush_customer, $pnp_customer_data )
    {
        $points = $rush_customer->points;
        $points->totalPoints = (float) $pnp_customer_data->points;
        $points->currentpoints = (float) $pnp_customer_data->points;
        $points->save();
    }

    public function addMigratedCustomerTransactionsFromPnpData( $rush_customer, $pnp_customer_data )
    {
        $transaction = new CustomerTransactionsModel;
        $transaction->transactionReferenceCode = str_random(12);
        $transaction->transactionType = 'migrated-earn';
        $transaction->customerId = $rush_customer->customerId;
        $transaction->pointsEarned = $pnp_customer_data->points;
        $transaction->amountPaidWithpoints = 0;
        $transaction->transactionStatus = 'done';
        $transaction->timestamp = "$pnp_customer_data->last_sale_date 00:00:00";
        $transaction->merchant_id = $rush_customer->merchantId;
        $transaction->channel = 'migration';
        $transaction->save();
    }

    public function getCustomerPnpPoints( $card_number )
    {
        return 0;
    }

    public function updatePnpDataFromRegistration( $card_number, $request )
    {
        return true;
    }

    public function getCustomerPnpTransactionsByDateRange( $card_number, $start_date, $end_date, $last_pnp_sync_datetime )
    {
        return [];
    }

    public function insertCustomerPnpPromoTransactions( $card_number, $birthday_transactions )
    {
        return true;
    }

    public function updateCustomerPnpPoints( $card_number, $new_points )
    {
        return true;
    }

    public function getCustomerPnpUnsyncedTransactions( $card_number, $last_pnp_sync_date )
    {
        return [];
    }

    public function addCustomerTransactionsFromPnpUnsyncedTransactions( $pnp_unsynced_transactions )
    {
        return true;
    }

    public function updateCustomerLastPnpSyncDatetime( $customer )
    {
        $customer->last_pnp_sync_datetime = date('Y-m-d H:i:s');
        $customer->save();
    }

    public function updateCustomerPointsFromPnpPoints( $customer, $pnp_points )
    {
        $customer_points = $customer->points;
        $customer_points->currentpoints = $pnp_points;
        $customer_points->totalPoints = $customer_points->currentpoints + $customer_points->usedPoints;
        $customer_points->save();
    }

    public function smsCustomerUpdate( $sender, $partials )
    {
        $merchant_code = MerchantCodeModel::where('code', $partials['merchant_code'])->first();
        $customer = CustomerModel
            ::where('merchantId', $merchant_code->merchant_id)
            ->where('mobileNumber', $sender)
            ->first();

        if( $customer ){

            $customer->fullName = $partials['name'];
            $customer->email = $partials['email'];
            $customer->PIN = $partials['pin'];
            $customer->firstName = $partials['card_number'];
            $customer->birthDate = $partials['birthdate'];
            $customer->address = $partials['city'];
            $customer->registrationChannel = 'Sms';
            $customer->save();

            return $customer;

        }
    }
}
