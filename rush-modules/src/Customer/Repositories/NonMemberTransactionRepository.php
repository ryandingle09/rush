<?php

namespace Rush\Modules\Customer\Repositories;

use Rush\Modules\Customer\Models\NonMemberTransactionModel;
use Rush\Modules\Customer\Repositories\NonMemberTransactionInterface;
use Rush\Modules\Repository\AbstractRepository;

/**
 * A repository class would encapsulate each Eloquent Model class
 * and be responsible for CRUD operations on the database.
 */
class NonMemberTransactionRepository extends AbstractRepository implements NonMemberTransactionInterface
{
    public function __construct(NonMemberTransactionModel $nonMemberTransactionMode)
    {
        parent::__construct($nonMemberTransactionMode);
    }

    public function getByMerchant($merchantId)
    {
        return $this->model
            ->with(['branch' => function ($query) {
                $query->where('is_deleted', 0)
                    ->withTrashed();
            }])
            ->where(['merchant_id' => $merchantId])
            ->where(['status' => 0])
            ->get();
    }

    public function getDayEarnTransactionsCount( $mobile_no, $merchant_id ){

        return $this->model->getDayEarnTransactionsCount( $mobile_no, $merchant_id );

    }

    public function getDayPointsEarnedTotal( $mobile_no, $merchant_id ){

        return $this->model->getDayPointsEarnedTotal( $mobile_no, $merchant_id );

    }

    public static function getCurrentPointsByEmail( $email ){

       return NonMemberTransactionModel::where('email', $email)->sum('pointsEarned');

    }
}
