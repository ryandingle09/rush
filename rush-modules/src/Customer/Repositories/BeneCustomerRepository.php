<?php

namespace Rush\Modules\Customer\Repositories;

use Rush\Modules\Merchant\Models\MerchantCodeModel;
use Rush\Modules\Customer\Models\BeneCustomerRsvpModel;
use Rush\Modules\Customer\Models\CustomerModel;
use Ramsey\Uuid\Uuid;
use Validator;
use Response;

class BeneCustomerRepository
{
    
    private $beneCustomerSmsRepository;
    private $beneMerchantCode = 'BENE';
    private $defaultPin = '0000';
    
    public function __construct(){
        
        $this->beneCustomerSmsRepository = new BeneCustomerSmsRepository();
        
    }
    
    public function isBeneSmsService( $message ){
        
        // extract the message partials.
        $partials = $this->getRegistrationMessagePartials( $message );
        
        // if merchant code is BENE, and MerchantCode.enabled = 1, return true. return false otherwise.
        $validator = Validator::make(
            [ 'merchant_code' => $partials['merchant_code'] ],
            [ 'merchant_code' => "required|in:{$this->beneMerchantCode}|exists:MerchantCode,code,enabled,1" ]
        );
        
        return $validator->passes();
        
    }
    
    public function processBeneSmsService( $mobile_no, $keyword, $message ){
        
        // extract the message partials.
        $partials = $this->getRegistrationMessagePartials( $message );
        
        if( strtoupper($message) == 'RSVP BENE CONFIRM' ) $this->confirmPreRegistration( $mobile_no );
        elseif( $keyword == 'RSVP' ) $this->preRegister( $mobile_no, $partials );
        elseif( $keyword == 'REG' ) $this->register( $mobile_no, $partials );
        else $this->beneCustomerSmsRepository->sendInvalidKeywordNotif( $mobile_no );
        
    }
    
    private function getRegistrationMessagePartials( $message ){
        
        $message_arr = explode(',', $message);
        
        $first_partial = ( isset($message_arr[0]) ? trim($message_arr[0]) : '' );
        $first_partial_arr = explode(' ', $first_partial);
        
        $partials['merchant_code'] = ( isset($first_partial_arr[1]) ? strtoupper(trim($first_partial_arr[1])) : '' );
        
        $partials['name'] = ( isset($message_arr[1]) ? trim($message_arr[1]) : '' );
        $partials['email'] = ( isset($message_arr[2]) ? trim($message_arr[2]) : '' );
        
        return $partials;
        
    }
    
    private function confirmPreRegistration( $mobile_no ){
        
        // if user tries to confirm rsvp after event date, send a registration-concluded message
        if( date('Y-m-d') > $this->getEventDate() ) return $this->beneCustomerSmsRepository->sendInvalidRegistrationConcludedNotif( $mobile_no );
        
        // if mobile is not pre-registered and tries to confirm rsvp before event date, suggest an rsvp bene message.
        if( ! $this->isMobilePreRegistered( $mobile_no ) && date('Y-m-d') < $this->getEventDate() ) return $this->beneCustomerSmsRepository->sendInvalidPreRegistrationFormatNotif( $mobile_no );
        
        // if mobile is not pre-registered and tries to confirm rsvp on event date, suggest a reg bene message.
        if( ! $this->isMobilePreRegistered( $mobile_no ) && date('Y-m-d') == $this->getEventDate() ) return $this->beneCustomerSmsRepository->sendMobileNotPreRegisteredNotif( $mobile_no );
        
        // if mobile is pre-registered and tries to confirm before event date, suggest to confirm on event date.
        if( date('Y-m-d') < $this->getEventDate() ) return $this->beneCustomerSmsRepository->sendInvalidPreRegistrationConfirmationDateNotif( $mobile_no );
        
        // check if mobile number is already registered.
        if( $this->isMobileRegistered( $mobile_no ) ) return $this->beneCustomerSmsRepository->sendMobileAlreadyRegisteredNotif( $mobile_no );
        
        // confirm customer pre-registration.
        $customer = $this->confirmCustomerPreRegistration( $mobile_no );
        
        // send rsvp confirmation success message.
        $this->beneCustomerSmsRepository->sendPreRegistrationConfirmationSuccessNotif( $customer );
        
    }
    
    private function preRegister( $mobile_no, $partials ){
        
        // validate message format.
        if( ! $this->validateRegistrationFields( $partials ) ) return $this->beneCustomerSmsRepository->sendInvalidPreRegistrationFormatNotif( $mobile_no );
        
        // if user tries to pre-register after event date, send a registration-concluded message
        if( date('Y-m-d') > $this->getEventDate() ) return $this->beneCustomerSmsRepository->sendInvalidRegistrationConcludedNotif( $mobile_no );
        
        // if user tries to pre-register on event date, suggest a reg been message.
        if( date('Y-m-d') == $this->getEventDate() ) return $this->beneCustomerSmsRepository->sendInvalidPreRegistrationDateNotif( $mobile_no );
        
        // check if mobile number is already pre-registered.
        if( $this->isMobilePreRegistered( $mobile_no ) ) return $this->beneCustomerSmsRepository->sendMobileAlreadyRsvpPreRegisteredNotif( $mobile_no );
        
        // rsvp pre-register the customer.
        $customer = $this->preRegisterCustomer( $mobile_no, $partials );
        
        // send rsvp success message.
        $this->beneCustomerSmsRepository->sendPreRegistrationSuccessfulNotif( $customer );
        
    }
    
    private function validateRegistrationFields( $partials ){
        
        $validator = Validator::make(
            [
                'merchant_code' => $partials['merchant_code'],
                'name' => $partials['name'],
                'email' => $partials['email']
            ],
            [
                'merchant_code' => 'required|exists:MerchantCode,code,enabled,1',
                'name' => 'required',
                'email' => 'required|email'
            ]
        );
        
        return $validator->passes();
        
    }
    
    private function register( $mobile_no, $partials ){
        
        // validate message format.
        if( ! $this->validateRegistrationFields( $partials ) ) return $this->beneCustomerSmsRepository->sendInvalidRegistrationFormatNotif( $mobile_no );
        
        // registration is only allowed on registration date.
        if( date('Y-m-d') < $this->getEventDate() ) return $this->beneCustomerSmsRepository->sendInvalidRegistrationDateNotif( $mobile_no );
        
        // check if mobile number is already registered.
        if( $this->isMobileRegistered( $mobile_no ) ) return $this->beneCustomerSmsRepository->sendMobileAlreadyRegisteredNotif( $mobile_no );
        
        // check if mobile number is already pre-registered.
        if( $this->isMobilePreRegistered( $mobile_no ) ) return $this->beneCustomerSmsRepository->sendMobileAlreadyPreRegisteredNotif( $mobile_no );
        
        // registration is only allowed on registration date.
        if( date('Y-m-d') > $this->getEventDate() ) return $this->beneCustomerSmsRepository->sendInvalidRegistrationConcludedNotif( $mobile_no );
        
        // register the customer.
        $customer = $this->registerCustomer( $mobile_no, $partials );
        
        // send registration success message.
        $this->beneCustomerSmsRepository->sendRegistrationSuccessfulNotif( $customer );
        
    }
    
    private function isMobilePreRegistered( $mobile_no ){
        
        $count = BeneCustomerRsvpModel::where('mobileNumber', $mobile_no)->count();
        return $count;
        
    }
    
    private function preRegisterCustomer( $sender, $partials ){
        
        $merchant_code = MerchantCodeModel::where('code', $partials['merchant_code'])->first();
        
        $customer = new BeneCustomerRsvpModel;
        $customer->merchantId = $merchant_code->merchant_id;
        $customer->registrationChannel = 'Sms';
        $customer->uuid = Uuid::uuid1()->toString();
        $customer->fullName = $partials['name'];
        $customer->mobileNumber = $sender;
        $customer->email = $partials['email'];
        $customer->PIN = $this->defaultPin;
        $customer->timestamp = date("Y-m-d H:i:s");
        $customer->save();
        
        return $customer;
        
    }
    
    private function isMobileRegistered( $mobile_no ){
        
        $merchant_code = MerchantCodeModel::where('code', $this->beneMerchantCode)->first();
        $count = CustomerModel::where('mobileNumber', $mobile_no)->where('merchantId', $merchant_code->merchant_id)->count();
        return $count;
        
    }
    
    private function confirmCustomerPreRegistration( $mobile_no ){
        
        $rsvp_customer = BeneCustomerRsvpModel::where('mobileNumber', $mobile_no)->first();
        $rsvp_customer->confirmed = 1;
        $rsvp_customer->save();
        
        $customer = new CustomerModel();
        $customer->merchantId = $rsvp_customer->merchantId;
        $customer->mobileNumber = $rsvp_customer->mobileNumber;
        $customer->PIN = $rsvp_customer->PIN;
        $customer->email = $rsvp_customer->email;
        $customer->fullName = $rsvp_customer->fullName;
        $customer->registrationChannel = $rsvp_customer->registrationChannel;
        $customer->uuid = $rsvp_customer->uuid;
        $customer->timestamp = $rsvp_customer->timestamp;
        $customer->save();
        
        return $customer;
        
    }
    
    private function getEventDate(){
        
        $event_date = MerchantCodeModel::where('code', $this->beneMerchantCode)->value('event_date');
        
        if( ! $event_date ) throw new \Exception('Event date not set');
        
        return $event_date;
        
    }
    
    public function registerCustomer( $mobile_no, $partials ){
        
        $merchant_code = MerchantCodeModel::where('code', $partials['merchant_code'])->first();

        $customer = new CustomerModel();
        $customer->merchantId = $merchant_code->merchant_id;
        $customer->registrationChannel = 'Sms';
        $customer->uuid = Uuid::uuid1()->toString();
        $customer->fullName = $partials['name'];
        $customer->mobileNumber = $mobile_no;
        $customer->email = $partials['email'];
        $customer->PIN = $this->defaultPin;
        $customer->timestamp = date("Y-m-d H:i:s");
        $customer->save();
        
        return $customer;
        
    }
    
    public function beneDownloadRsvp(){
        
        $customers = BeneCustomerRsvpModel::all();
        
        $column_headers[] = 'Name';
        $column_headers[] = 'Mobile';
        $column_headers[] = 'Email';
        $column_headers[] = 'Confirm';
        $column_headers[] = 'Date';
        $column_headers[] = "\n";
        $output = implode(",", $column_headers);
        
        foreach( $customers as $row ){
            $row_data = [];
            $row_data[] = $row->fullName;
            $row_data[] = "=\"$row->mobileNumber\"";
            $row_data[] = $row->email;
            $row_data[] = ( $row->confirmed == 1 ? 'Confirmed' : 'Rsvp' );
            $row_data[] = $row->created_at;
            $row_data[] = "\n";
            $output .=  implode(",", $row_data);
        }
        
        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="customers.csv"',
        ];
        
        return Response::make($output, 200, $headers);
        
    }
    
}