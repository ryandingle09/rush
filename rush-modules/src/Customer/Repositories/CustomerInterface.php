<?php
namespace Rush\Modules\Customer\Repositories;

use Rush\Modules\Repository\RepositoryInterface;

/**
 * Interface CustomerInterface
 * @package Rush\Modules\Customer\Repositories
 */
interface CustomerInterface extends RepositoryInterface
{
}
