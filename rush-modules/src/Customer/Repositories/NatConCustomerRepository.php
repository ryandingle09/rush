<?php

namespace Rush\Modules\Customer\Repositories;

use Rush\Modules\Merchant\Models\MerchantCodeModel;
use Rush\Modules\Customer\Models\CustomerModel;
use Ramsey\Uuid\Uuid;
use Validator;

class NatConCustomerRepository
{
    
    private $natConCustomerSmsRepository;
    private $natConMerchantCode = 'MIDCON';
    
    public function __construct(){
        
        $this->natConCustomerSmsRepository = new NatConCustomerSmsRepository();
        
    }
    
    public function isNatConSmsService( $message ){
        
        // extract the message partials.
        $partials = $this->getRegistrationMessagePartials( $message );

        // if merchant code is NATCON, and MerchantCode.enabled = 1, return true. return false otherwise.
        $validator = Validator::make(
            [ 'merchant_code' => $partials['merchant_code'] ],
            [ 'merchant_code' => "required|in:{$this->natConMerchantCode}|exists:MerchantCode,code,enabled,1" ]
        );
        
        return $validator->passes();

    }

    public function processNatConSmsService( $mobile_no, $keyword, $message ){

        // extract the message partials.
        $partials = $this->getRegistrationMessagePartials( $message );
        
        if( $keyword == 'REG' ) $this->register( $mobile_no, $partials );
        else $this->natConCustomerSmsRepository->sendInvalidKeywordNotif( $mobile_no );

    }

    private function getRegistrationMessagePartials( $message ){
        
        $message_arr = explode(',', $message);
        
        $first_partial = ( isset($message_arr[0]) ? trim($message_arr[0]) : '' );
        $first_partial_arr = explode(' ', $first_partial);
        
        $partials['merchant_code'] = ( isset($first_partial_arr[1]) ? strtoupper(trim($first_partial_arr[1])) : '' );
        
        $partials['name'] = ( isset($message_arr[1]) ? trim($message_arr[1]) : '' );
        $partials['pin'] = ( isset($message_arr[2]) ? trim($message_arr[2]) : '' );
        
        return $partials;
        
    }

    private function register( $mobile_no, $partials ){

        $merchant_code = MerchantCodeModel::where('code', $this->natConMerchantCode)->first();
        
        // validate message format.
        if( ! $this->validateRegistrationFields( $partials ) ) return $this->natConCustomerSmsRepository->sendInvalidRegistrationFormatNotif( $mobile_no, $merchant_code );
        
        // check if mobile number is already registered.
        if( $this->isMobileRegistered( $mobile_no ) ) return $this->natConCustomerSmsRepository->sendMobileAlreadyRegisteredNotif( $mobile_no, $merchant_code );
        
        // if natcon enabled mobile white listing, check if mobile is whitelisted.
        if( ! $this->isMobileWhitelisted( $mobile_no ) ) return $this->natConCustomerSmsRepository->sendMobileNotWhitelistedNotif( $mobile_no, $merchant_code );
        
        // register the customer.
        $customer = $this->registerCustomer( $mobile_no, $partials );
        
        // send registration success message.
        $this->natConCustomerSmsRepository->sendRegistrationSuccessfulNotif( $customer, $merchant_code );

    }

    private function validateRegistrationFields( $partials ){
        
        $validator = Validator::make(
            [
                'merchant_code' => $partials['merchant_code'],
                'name' => $partials['name'],
                'pin' => $partials['pin']
            ],
            [
                'merchant_code' => 'required|exists:MerchantCode,code,enabled,1',
                'name' => 'required',
                'pin' => 'required|regex:/^\d{4}$/',
            ]
        );

        return $validator->passes();
        
    }

    private function isMobileRegistered( $mobile_no ){
        
        $merchant_code = MerchantCodeModel::where('code', $this->natConMerchantCode)->first();
        $count = CustomerModel::where('mobileNumber', $mobile_no)->where('merchantId', $merchant_code->merchant_id)->count();
        return $count;

    }

    private function isMobileWhitelisted( $mobile_no ){

        $merchant_code = MerchantCodeModel::where('code', $this->natConMerchantCode)->first();

        if( $merchant_code->merchant->settings->whitelisting_mobile_numbers == 1 ){

            $merchant_whitelisted_mobile_numbers = $merchant_code->merchant->whitelisted_mobile_numbers;
            $whitelisted_mobile_numbers = [];
            
            foreach( $merchant_whitelisted_mobile_numbers as $whitelist ){
                $whitelisted_mobile_numbers[] = $whitelist->mobile_number;
            }

            if( ! in_array($mobile_no, $whitelisted_mobile_numbers) ) return false;

        }
        
        return true;

    }

    public function registerCustomer( $mobile_no, $partials ){
        
        $merchant_code = MerchantCodeModel::where('code', $partials['merchant_code'])->first();

        $customer = new CustomerModel();
        $customer->merchantId = $merchant_code->merchant_id;
        $customer->registrationChannel = 'Sms';
        $customer->uuid = Uuid::uuid1()->toString();
        $customer->fullName = $partials['name'];
        $customer->mobileNumber = $mobile_no;
        $customer->email = $mobile_no . '@rush.ph';
        $customer->PIN = $partials['pin'];
        $customer->timestamp = date("Y-m-d H:i:s");
        $customer->save();
        
        return $customer;
        
    }
    
}