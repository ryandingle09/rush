<?php
namespace Rush\Modules\Customer\Repositories;

class BeneCustomerSmsRepository extends CustomerSmsRepository{
    
    public function sendInvalidKeywordNotif( $mobile_no ){
        
        $message = "Sorry, the keyword you sent is invalid";

        $this->setCredential( $this->genericMerchantId );
        $response = $this->send( $mobile_no, $message );
        $this->log( $mobile_no, $message, $response, $this->genericMerchantId, 'bene-sms-service-invalid-keyword' );
        
        return $response;
        
    }
    
    public function sendInvalidPreRegistrationFormatNotif( $mobile_no ){
        
        $message = "Sorry, the keyword you sent is invalid. Please resend the SMS in this format: RSVP BENE, <First Name Last Name>, <Email>";

        $this->setCredential( $this->genericMerchantId );
        $response = $this->send( $mobile_no, $message );
        $this->log( $mobile_no, $message, $response, $this->genericMerchantId, 'bene-sms-service-invalid-pre-registration-format' );

        return $response;
        
    }
    
    public function sendInvalidPreRegistrationDateNotif( $mobile_no ){
        
        $message = "Sorry, the keyword you sent is invalid. Please resend the SMS in this format: REG BENE, <First Name Last Name>, <Email>";

        $this->setCredential( $this->genericMerchantId );
        $response = $this->send( $mobile_no, $message );
        $this->log( $mobile_no, $message, $response, $this->genericMerchantId, 'bene-sms-service-invalid-pre-registration-date' );

        return $response;
        
    }
    
    public function sendMobileAlreadyRegisteredNotif( $mobile_no ){
        
        $message = "Sorry, mobile number is already registered.";

        $this->setCredential( $this->genericMerchantId );
        $response = $this->send( $mobile_no, $message );
        $this->log( $mobile_no, $message, $response, $this->genericMerchantId, 'bene-sms-service-mobile-already-registered' );

        return $response;
        
    }
    
    public function sendMobileAlreadyRsvpPreRegisteredNotif( $mobile_no ){
        
        $message = "Sorry, mobile number is already pre-registered.";

        $this->setCredential( $this->genericMerchantId );
        $response = $this->send( $mobile_no, $message );
        $this->log( $mobile_no, $message, $response, $this->genericMerchantId, 'bene-sms-service-mobile-already-rsvp-pre-registered' );

        return $response;
        
    }
    
    public function sendPreRegistrationSuccessfulNotif( $customer ){
        
        $message = "Thanks for registering! We look forward to seeing you at the Grand Alumni Homecoming on December 10, 2016.";
        
        $this->setCredential( $customer->merchantId );
        $response = $this->send( $customer->mobileNumber, $message );
        $this->log( $customer->mobileNumber, $message, $response, $customer->merchantId, 'bene-sms-service-customer-pre-registration' );

        return $response;
        
    }
    
    public function sendMobileNotPreRegisteredNotif( $mobile_no ){
        
        $message = "Sorry, the keyword you sent is invalid. Please resend the SMS in this format: REG BENE, <First Name Last Name>, <Email>";

        $this->setCredential( $this->genericMerchantId );
        $response = $this->send( $mobile_no, $message );
        $this->log( $mobile_no, $message, $response, $this->genericMerchantId, 'bene-sms-service-mobile-not-pre-registered' );

        return $response;
        
    }
    
    public function sendPreRegistrationConfirmationSuccessNotif( $customer ){
        
        $message = "Thanks for coming to the Grand Alumni Homecoming!";
        
        $this->setCredential( $customer->merchantId );
        $response = $this->send( $customer->mobileNumber, $message );
        $this->log( $customer->mobileNumber, $message, $response, $customer->merchantId, 'bene-sms-service-customer-pre-registration-confirmation' );

        return $response;
        
    }
    
    public function sendInvalidPreRegistrationConfirmationDateNotif( $mobile_no ){
        
        $message = "Sorry, you will need to confirm your attendance for Grand Alumni Homecoming on December 10, 2016.";

        $this->setCredential( $this->genericMerchantId );
        $response = $this->send( $mobile_no, $message );
        $this->log( $mobile_no, $message, $response, $this->genericMerchantId, 'bene-sms-service-invalid-pre-registration-confirmation-date' );

        return $response;
        
    }
    
    public function sendInvalidRegistrationFormatNotif( $mobile_no ){
        
        $message = "Sorry, the keyword you sent is invalid. Please resend the SMS in this format: REG BENE, <First Name Last Name>, <Email>";

        $this->setCredential( $this->genericMerchantId );
        $response = $this->send( $mobile_no, $message );
        $this->log( $mobile_no, $message, $response, $this->genericMerchantId, 'bene-sms-service-invalid-registration-format' );

        return $response;
        
    }
    
    public function sendInvalidRegistrationDateNotif( $mobile_no ){
        
        $message = "Sorry, registration is not yet open for Grand Alumni Homecoming on December 10, 2016.";

        $this->setCredential( $this->genericMerchantId );
        $response = $this->send( $mobile_no, $message );
        $this->log( $mobile_no, $message, $response, $this->genericMerchantId, 'bene-sms-service-invalid-registration-date' );

        return $response;
        
    }
    
    public function sendMobileAlreadyPreRegisteredNotif( $mobile_no ){
        
        $message = "Sorry, mobile number is pre-registered. To confirm attendance, please resend the SMS in this format: RSVP BENE CONFIRM";

        $this->setCredential( $this->genericMerchantId );
        $response = $this->send( $mobile_no, $message );
        $this->log( $mobile_no, $message, $response, $this->genericMerchantId, 'bene-sms-service-mobile-already-pre-registered' );

        return $response;
        
    }
    
    public function sendInvalidRegistrationConcludedNotif( $mobile_no ){
        
        $message = "Sorry, registration is already concluded for San Beda Alabang Grand Alumni Homecoming";

        $this->setCredential( $this->genericMerchantId );
        $response = $this->send( $mobile_no, $message );
        $this->log( $mobile_no, $message, $response, $this->genericMerchantId, 'bene-sms-service-registration-concluded' );

        return $response;
        
    }
    
    public function sendRegistrationSuccessfulNotif( $customer ){
        
        $message = "Thanks for coming to the Grand Alumni Homecoming!";
        
        $this->setCredential( $customer->merchantId );
        $response = $this->send( $customer->mobileNumber, $message );
        $this->log( $customer->mobileNumber, $message, $response, $customer->merchantId, 'bene-sms-service-customer-registration' );

        return $response;
        
    }
    
}