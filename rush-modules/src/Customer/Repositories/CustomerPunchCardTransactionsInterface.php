<?php

namespace Rush\Modules\Customer\Repositories;

use Rush\Modules\Repository\RepositoryInterface;

/**
 * The CustomerPunchCardTransactionsInterface contains unique method signatures related to CustomerPunchCardTransactions object
 */
interface CustomerPunchCardTransactionsInterface extends RepositoryInterface
{
    public function getByOrNoAndCustomerId($orNo, $customerId);

    public function addTransaction(
        $employee,
        $card,
        $amount = null,
        $reward_id = 0,
        $status = false,
        $type = 'earn',
        $channel = '',
        $stamps = 0,
        $card_id = null,
        $or_no = null,
        $branch_id = null,
        $remarks = null
    );
}
