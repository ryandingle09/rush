<?php
namespace Rush\Modules\Customer\Repositories;

use Rush\Modules\Merchant\Models\NotificationSmsMessagesModel;

class NatConCustomerSmsRepository extends CustomerSmsRepository{
    
    public function sendInvalidKeywordNotif( $mobile_no ){
        
        $message = "Sorry, the keyword you sent is invalid";

        $this->setCredential( $this->genericMerchantId );
        $response = $this->send( $mobile_no, $message );
        $this->log( $mobile_no, $message, $response, $this->genericMerchantId, 'natcon-sms-service-invalid-keyword' );
        
        return $response;
        
    }

    public function sendInvalidRegistrationFormatNotif( $mobile_no, $merchant_code )
    {

        $sms_message = NotificationSmsMessagesModel
            ::where('merchant_id', $merchant_code->merchant_id)
            ->where('type', 'midcon-sms-service-registration-failed')
            ->first();

        if( $sms_message ){

            $this->setCredential( $this->genericMerchantId );
            $response = $this->send( $mobile_no, $sms_message->message );
            $this->log( $mobile_no, $sms_message->message, $response, $this->genericMerchantId, $sms_message->type );

            return $response;

        }
        
    }

    public function sendMobileAlreadyRegisteredNotif( $mobile_no, $merchant_code ){

        $sms_message = NotificationSmsMessagesModel
            ::where('merchant_id', $merchant_code->merchant_id)
            ->where('type', 'midcon-sms-service-mobile-already-registered')
            ->first();

        if( $sms_message ){

            $this->setCredential( $this->genericMerchantId );
            $response = $this->send( $mobile_no, $sms_message->message );
            $this->log( $mobile_no, $sms_message->message, $response, $this->genericMerchantId, $sms_message->type );

            return $response;

        }

    }

    public function sendMobileNotWhitelistedNotif( $mobile_no, $merchant_code ){

        $sms_message = NotificationSmsMessagesModel
            ::where('merchant_id', $merchant_code->merchant_id)
            ->where('type', 'midcon-sms-service-mobile-not-whitelisted')
            ->first();

        if( $sms_message ){

            $this->setCredential( $this->genericMerchantId );
            $response = $this->send( $mobile_no, $sms_message->message );
            $this->log( $mobile_no, $sms_message->message, $response, $this->genericMerchantId, $sms_message->type );

            return $response;

        }

    }

    public function sendRegistrationSuccessfulNotif( $customer, $merchant_code ){

        $sms_message = NotificationSmsMessagesModel
            ::where('merchant_id', $merchant_code->merchant_id)
            ->where('type', 'midcon-sms-service-customer-registration')
            ->first();

        if( $sms_message ){

            $message = str_replace('%name%', $customer->fullName, $sms_message->message);

            $this->setCredential( $customer->merchantId );
            $response = $this->send( $customer->mobileNumber, $message );
            $this->log( $customer->mobileNumber, $message, $response, $customer->merchantId, $sms_message->type );

            return $response;

        }

    }
    
}