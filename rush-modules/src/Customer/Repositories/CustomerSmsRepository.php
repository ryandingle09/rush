<?php
namespace Rush\Modules\Customer\Repositories;

use Rush\Modules\Helpers\SmsHelper;
use Rush\Modules\Customer\Helpers\SmsLogHelper;
use Rush\Modules\Sms\Models\SmsCredentialModel;

class CustomerSmsRepository{
    
    protected $smsHelper;

    // 0 is the merchant id of the generic shortcode.
    protected $genericMerchantId = 0;

    protected function setCredential($merchant_id = 0){
        
        $merchant_credentials = SmsCredentialModel::where('merchant_id', $merchant_id)->first();

        $this->smsHelper = new SmsHelper(
            $merchant_credentials->shortcode,
            $merchant_credentials->app_id,
            $merchant_credentials->app_secret,
            $merchant_credentials->passphrase
        );
        
    }
    
    protected function send( $mobile_no, $message ){
        
        $response = $this->smsHelper->send( $mobile_no, $message );
        return $response;
        
    }
    
    protected function log( $mobile_no, $message, $response, $merchant_id, $type ){
        
        SmsLogHelper::log( $mobile_no, $message, $response, $merchant_id, $type );
        
    }
    
}