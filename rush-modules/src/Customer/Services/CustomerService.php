<?php

namespace Rush\Modules\Customer\Services;

use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory;
use Carbon\Carbon;
use Event;
use Illuminate\Support\Facades\Input;
use Rush\Modules\Achievements\Services\AchievementsService;
use Rush\Modules\Attendance\Repositories\ClientInterface;
use Rush\Modules\Customer\Events\ConfirmAttendanceEvent;
use Rush\Modules\Customer\Helpers\SmsLogHelper;
use Rush\Modules\Customer\Repositories\BeneCustomerRepository;
use Rush\Modules\Customer\Repositories\CustomerInterface;
use Rush\Modules\Customer\Repositories\CustomerLoyaltySmsRepository as CustomerLoyaltySmsService;
use Rush\Modules\Customer\Repositories\CustomerLoyaltySmsRepository;
use Rush\Modules\Customer\Repositories\CustomerPunchCardTransactionsRepository;
use Rush\Modules\Customer\Repositories\CustomerTransactionRepository;
use Rush\Modules\Customer\Repositories\NatConCustomerRepository;
use Rush\Modules\Customer\Repositories\NonMemberTransactionInterface;
use Rush\Modules\Helpers\SmsHelper;
use Rush\Modules\Merchant\Repositories\AmaxApiRepository;
use Rush\Modules\Merchant\Repositories\EmployeeInterface;
use Rush\Modules\Merchant\Repositories\MerchantInterface;
use Rush\Modules\Merchant\Repositories\SmsNotificationInterface;
use Rush\Modules\Merchant\Repositories\SmsNotificationRepository;
use Rush\Modules\Sms\Library\Sms;
use Rush\Modules\Sms\Repositories\SmsLogInterface;
use Rush\Modules\Sms\Services\SmsCredentialService;
use Validator;
use Rush\Modules\Merchant\Services\NotificationService;

class CustomerService
{
    /**
     * @var \Rush\Modules\Customer\Repositories\CustomerRepository
     */
    protected $customerRepository;
    protected $customerLoyaltySmsRepository;
    /**
     * @var \Rush\Modules\Merchant\Repositories\MerchantRepository
     */
    protected $merchantRepository;
    protected $amaxApiRepository;
    protected $beneCustomerRepository;
    protected $nonMemberTransactionRepository;
    protected $employeeRepository;
    protected $smsNotificationRepository;
    protected $smsCredentialService;
    protected $smsLogRepository;
    protected $customerPunchcardTransactionRepository;
    protected $notification_service;
    protected $clientRepository;
    protected $customerTransactionRepository;

    public function __construct(
        CustomerInterface $customerRepository,
        CustomerLoyaltySmsRepository $customerLoyaltySmsRepository,
        MerchantInterface $merchantRepository,
        AmaxApiRepository $amaxApiRepository,
        BeneCustomerRepository $beneCustomerRepository,
        NatConCustomerRepository $natConCustomerRepository,
        NonMemberTransactionInterface $nonMemberTransactionRepository,
        EmployeeInterface $employeeRepository,
        SmsNotificationInterface $smsNotificationRepository,
        SmsCredentialService $smsCredentialService,
        SmsLogInterface $smsLogRepository,
        CustomerPunchCardTransactionsRepository $customerPunchCardTransactionsRepository,
        CustomerLoyaltySmsService $customerLoyaltySmsService,
        NotificationService $notification_service,
        ClientInterface $clientRepository,
        CustomerTransactionRepository $customerTransactionRepository
    ) {
        $this->customerRepository = $customerRepository;
        $this->customerLoyaltySmsRepository = $customerLoyaltySmsRepository;
        $this->merchantRepository = $merchantRepository;
        $this->amaxApiRepository = $amaxApiRepository;
        $this->beneCustomerRepository = $beneCustomerRepository;
        $this->natConCustomerRepository = $natConCustomerRepository;
        $this->nonMemberTransactionRepository = $nonMemberTransactionRepository;
        $this->employeeRepository = $employeeRepository;
        $this->smsNotificationRepository = $smsNotificationRepository;
        $this->smsCredentialService = $smsCredentialService;
        $this->smsLogRepository = $smsLogRepository;
        $this->customerPunchcardTransactionRepository = $customerPunchCardTransactionsRepository;
        $this->customerLoyaltySmsService = $customerLoyaltySmsService;
        $this->notification_service = $notification_service;
        $this->clientRepository = $clientRepository;
        $this->customerTransactionRepository = $customerTransactionRepository;
    }

    public function auth($merchant_id, $mobile_no, $pin, $fb_id = null)
    {
        return $this->customerRepository->auth($merchant_id, $mobile_no, $pin, $fb_id);
    }

    /**
     *  Get customer by ID or UUID
     * @param  integer|string $id
     * @param  array $args
     * @return mixed|null|\Rush\Modules\Customer\Models\CustomerModel
     */
    public function find($id, $args = [])
    {
        return $this->customerRepository->find($id, $args);
    }

    public function getById($id)
    {
        if ($id instanceof \Rush\Modules\Customer\Models\CustomerModel) {
            $customer = $id;
        } else {
            $customer = $this->customerRepository->getById($id);
        }

        return $customer;
    }

    public function getAllByNumbers($numbers, $merchant_id)
    {
       return $this->customerRepository->getAllByNumbers($numbers, $merchant_id);
    }

    public function getCustomers($args = null)
    {
        return $this->customerRepository->getCustomers($args);
    }

    public function createCustomer( $merchant_id, $request, $channel ){

        $feature_bypass = $this->merchantRepository->getGenericFeatureBypass( $merchant_id );
        $validation_bypass = $this->merchantRepository->getGenericValidationBypass( $merchant_id );
        $errors = $this->validateCustomerDetails( $validation_bypass, $merchant_id, $request, null );

        if( ! empty($errors) ) return $errors;

        $create_customer_result = $this->customerRepository->createCustomer( $merchant_id, $request, $channel );
        $customer = $this->customerRepository->getCustomerByUuid( $create_customer_result['data']['uuid'] );

        if( ! isset($feature_bypass['customer_register']['sms']) ){
            $this->customerLoyaltySmsRepository->createCustomer( $customer );
        }

        // get non member transactions.
        $non_member_transactions = $this->customerRepository->getNonMemberTransactions( $customer->merchant, $request['mobile_no'], $request['email'] );

        // activate the non member transactions.
        if( $non_member_transactions ) $this->customerRepository->activateNonMemberTransactions( $create_customer_result['data']['uuid'], $non_member_transactions );

        return $create_customer_result;

    }

    public function validateCustomerDetails( $validation_bypass, $merchant_id, $request, $uuid = null ){

        $merchant = $this->merchantRepository->getById($merchant_id);
        if ($merchant && $this->merchantRepository->validateCustomerTierCount($merchant) == false) {
            return ['error_code' => '0x5', 'message' => 'Sorry! New member\'s registration is not allowed at this time. Please visit any of our branches for assistance. Thank You!'];
        }

        if( ! is_null($uuid) ){
            $customer = $this->customerRepository->getCustomerByUuid( $uuid );
            if( ! $customer ) return ['error_code' => '0x1', 'message' => 'Customer not found.'];
        }

        if( trim($request['name']) == '' ) return ['error_code' => '0x2', 'message' => 'Name is required.'];
        if( trim($request['email']) == '' ) return ['error_code' => '0x3', 'message' => 'Email is required.'];
        if( filter_var($request['email'], FILTER_VALIDATE_EMAIL) === false ) return ['error_code' => '0x4', 'message' => 'Invalid email.'];

        if( ! isset($validation_bypass['customer_register']['email']['unique']) ){
            $args = ['merchantId' => $merchant_id, 'email' => $request['email']];
            $customers = $this->customerRepository->getCustomers( $args );
            if( count($customers) ){
                if( is_null($uuid) ) return ['error_code' => '0x5', 'message' => 'Email already exists.'];
                elseif( $customers[0]->uuid != $uuid ) return ['error_code' => '0x6', 'message' => 'Email already exists.'];
            }
        }

        if( trim($request['mobile_no']) == '' ) return ['error_code' => '0x7', 'message' => 'Mobile number is required.'];
        if( ! preg_match('/^0\d{10}$/', $request['mobile_no']) ) return ['error_code' => '0x8', 'message' => 'Invalid mobile number.'];

        $customer = $this->customerRepository->getCustomerByMobile( $request['mobile_no'], $merchant_id );
        if( count($customer) ){
            if( is_null($uuid) ) return ['error_code' => '0x9', 'message' => 'Mobile number is already registered to this merchant.'];
            elseif( $customer->uuid != $uuid ) return ['error_code' => '0xa', 'message' => 'Mobile number is already registered to this merchant.'];
        }

        if( is_null($uuid) ){
            if( trim($request['mpin']) == '' ) return ['error_code' => '0xb', 'message' => 'Pin is required.'];
            if( ! is_numeric($request['mpin']) ) return ['error_code' => '0xc', 'message' => 'Invalid pin.'];
            if( strlen($request['mpin']) < 4 ) return ['error_code' => '0xd', 'message' => 'Invalid pin.'];
        }

        if( ! isset($validation_bypass['customer_register']['birthdate']['required']) ){
            if( trim($request['birthdate']) != '' ){
                if( ! preg_match('/^\d{2}\/\d{2}\/\d{4}$/', $request['birthdate']) ) return ['error_code' => '0xe', 'message' => 'Invalid birthdate format.'];
            }
        }

        if( ! isset($validation_bypass['customer_register']['gender']['required']) ){
            if( trim($request['gender']) == '' ) return ['error_code' => '0xf', 'message' => 'Gender is required.'];
            if( ! in_array(strtolower($request['gender']), array('male', 'female')) ) return ['error_code' => '1x0', 'message' => 'Invalid gender.'];
        }

        if( ! isset($validation_bypass['customer_register']['fb_id']['required']) ){
            if( trim($request['fb_id']) != '' ){
                if( ! is_numeric($request['fb_id']) ) return ['error_code' => '1x1', 'message' => 'Invalid FB ID.'];
            }
        }

        if( ! isset($validation_bypass['customer_register']['email_verified']['required']) ){
            if( trim($request['email_verified']) == '' ) return ['error_code' => '1x2', 'message' => 'Email verified field is required.'];
            if( ! in_array($request['email_verified'], array('0', '1')) ) return ['error_code' => '1x3', 'message' => 'Invalid email verified value.'];
        }

        if( ! isset($validation_bypass['customer_register']['mobile_verified']['required']) ){
            if( trim($request['mobile_verified']) == '' ) return ['error_code' => '1x4', 'message' => 'Mobile verified field is required.'];
            if( ! in_array($request['mobile_verified'], array('0', '1')) ) return ['error_code' => '1x5', 'message' => 'Invalid email verified value.'];
        }

    }

    public function getSingleCustomer( $uuid ){

        return $this->customerRepository->getSingleCustomer( $uuid );

    }

    public function updateCustomer( $merchant_id, $uuid, $request ){

        $validation_bypass = $this->merchantRepository->getGenericValidationBypass( $merchant_id );
        $errors = $this->validateCustomerDetails( $validation_bypass, $merchant_id, $request, $uuid );

        if( ! empty($errors) ) return $errors;

        else return $this->customerRepository->updateCustomer( $uuid, $request );

    }

    public function deleteCustomer( $uuid ){

        return $this->customerRepository->deleteCustomer( $uuid );


    }

    public function getCustomerPoints( $uuid ){

        return $this->customerRepository->getCustomerPoints( $uuid );

    }

    public function earnCustomerPoints( $merchant_id, $customer_uuid, $request, $channel = null ){

        $feature_bypass = $this->merchantRepository->getGenericFeatureBypass( $merchant_id );
        $validation_bypass = $this->merchantRepository->getGenericValidationBypass( $merchant_id );
        $errors = $this->validateCustomerPointsEarnData( $validation_bypass, $merchant_id, $customer_uuid, $request );

        if( ! empty($errors) ) return $errors;

        $customer = $this->customerRepository->getCustomerByUuid( $customer_uuid );
        $employee = $this->employeeRepository->findByUuid( $request['employee_uuid'] );
        $transaction = $this->customerRepository->earnPoints( $customer, $employee, $request, $channel );

        if( ! isset($feature_bypass['customer_points_earn']['sms']) ){
            $this->customerLoyaltySmsRepository->earnPoints( $transaction );
        }

        return ['error_code' => '0x0', 'data' => $transaction->transactionReferenceCode, 'earned_points' => $transaction->pointsEarned];

    }

    public function validateCustomerPointsEarnData( $validation_bypass, $merchant_id, $customer_uuid, $request ){

        $customer = $this->customerRepository->getCustomerByUuid( $customer_uuid );
        if( ! $customer ) return ['error_code' => '0x1', 'message' => 'Customer not found.'];
        if( $merchant_id != $customer->merchant->merchantid ) return ['error_code' => '0x2', 'message' => 'Invalid customer.'];


        if( isset($request['employee_uuid']) ){
            if( trim($request['employee_uuid']) == '' ) return ['error_code' => '0x3', 'message' => 'Employee uuid is required.'];
            $employee = $this->employeeRepository->findByUuid( $request['employee_uuid'] );
            if( ! $employee ) return ['error_code' => '0x4', 'message' => 'Employee not found.'];
            if( $merchant_id != $employee->merchantId ) return ['error_code' => '0x5', 'message' => 'Invalid employee.'];
        }


        if( isset($request['amount']) ){
            if( trim($request['amount']) == '' && ! isset($request['points']) ) return ['error_code' => '0x6', 'message' => 'Amount is required if points is not given.'];
            if( ! is_numeric($request['amount']) || floatval($request['amount']) <= 0 ) return ['error_code' => '0x7', 'message' => 'Invalid amount.'];
        }


        if( isset($request['points']) ){
            if( trim($request['points']) == '' && ! isset($request['amount']) ) return ['error_code' => '0x8', 'message' => 'Points is required if amount is not given.'];
            if( ! is_numeric($request['points']) || floatval($request['points']) <= 0 ) return ['error_code' => '0x9', 'message' => 'Invalid points.'];
        }


        if( ! isset($request['amount']) && ! isset($request['points']) ){
            return ['error_code' => '0xe', 'message' => 'Amount or points is required.'];
        }


        if( trim($request['or_no']) == '' ) return ['error_code' => '0xa', 'message' => 'OR number is required.'];
        $transaction = $this->customerRepository->getTransactionByOrNo( $request['or_no'], $merchant_id );
        if( $transaction ) return ['error_code' => '0xb', 'message' => 'OR number already exists.'];


        // check if customer's earn transactions count has met the merchant's earn transaction limit.
        $merchant_earn_transaction_limit = (int) $customer->merchant->settings->earn_transaction_limit;
        $customer_earn_transactions_count = (int) $customer->day_earn_transactions_count;
        if( $customer_earn_transactions_count >= $merchant_earn_transaction_limit ) return ['error_code' => '0xc', 'message' => 'Customer has reached the merchant\'s daily earn transaction limit.'];

        // check if customer's total earned points + his points to be earned in the current transaction
        // has met the merchant's earn points limit.
        $merchant_earn_points_limit = (float) $customer->merchant->settings->earn_points_limit;
        $customer_points_earned_total = (float) $customer->day_points_earned_total;
        $points_to_be_earned = (float) $customer->merchant->getEarningPointsOfAmount( $request['amount'] );
        if( ($customer_points_earned_total + $points_to_be_earned) > $merchant_earn_points_limit ) return ['error_code' => '0xd', 'message' => 'Customer has reached the merchant\'s daily earn points limit.'];

    }

    public function validateNonMemberPointsEarnData( $merchant_id, $request, $customer_uuid = null ){

        if( trim($request['employee_uuid']) == '' ) return ['error_code' => '0x1', 'message' => 'Employee uuid is required.'];
        $employee = $this->employeeRepository->findByUuid( $request['employee_uuid'] );
        if( ! $employee ) return ['error_code' => '0x2', 'message' => 'Employee not found.'];
        if( $merchant_id != $employee->merchantId ) return ['error_code' => '0x3', 'message' => 'Invalid employee.'];


        if( trim($request['mobile_no']) == '' ) return ['error_code' => '0x4', 'message' => 'Mobile number is required.'];
        if( ! preg_match('/^0\d{10}$/', $request['mobile_no']) ) return ['error_code' => '0x5', 'message' => 'Invalid mobile number.'];


        $customer = $this->customerRepository->getCustomerByMobile( $request['mobile_no'], $merchant_id );
        if( $customer ) return ['error_code' => '0x6', 'message' => 'Mobile number is already registered to this merchant.'];


        if( trim($request['amount']) == '' ) return ['error_code' => '0x7', 'message' => 'Amount is required.'];
        if( ! is_numeric($request['amount']) || floatval($request['amount']) <= 0 ) return ['error_code' => '0x8', 'message' => 'Invalid amount.'];


        if( trim($request['or_no']) == '' ) return ['error_code' => '0x9', 'message' => 'OR number is required.'];
        $non_member_transaction = $this->customerRepository->getNonMemberTransactionByOrNo( $request['or_no'], $merchant_id );
        if( $non_member_transaction ) return ['error_code' => '0xa', 'message' => 'OR number already exists.'];


        $merchant_earn_transaction_limit = (int) $employee->merchant->settings->earn_transaction_limit;
        $non_member_earn_transactions_count = (int) $this->nonMemberTransactionRepository->getDayEarnTransactionsCount( $request['mobile_no'], $merchant_id );

        if( $non_member_earn_transactions_count >= $merchant_earn_transaction_limit ) return ['error_code' => '0xb', 'message' => 'Non member has reached the merchant\'s earn transaction limit.'];

        // check if non member's total earned points + his points to be earned in the current transaction
        // has reached the merchant's earn points limit.
        $merchant_earn_points_limit = (float) $employee->merchant->settings->earn_points_limit;
        $non_member_points_earned_total = (float) $this->nonMemberTransactionRepository->getDayPointsEarnedTotal( $request['mobile_no'], $merchant_id );
        $points_to_be_earned = (float) $employee->merchant->getEarningPointsOfAmount( $request['amount'] );

        if( ($non_member_points_earned_total + $points_to_be_earned) > $merchant_earn_points_limit ) return ['error_code' => '0xc', 'message' => 'Non member has reached the merchant\'s earn points limit.'];

    }

    public function earnBadge( $merchant_id, $customer_uuid, $request, $channel = null ){

        $errors = $this->validateEarnBadgeData( $merchant_id, $customer_uuid, $request );

        if( ! empty($errors) ) return $errors;

        $customer = $this->customerRepository->getCustomerByUuid( $customer_uuid );
        $reference = $this->customerRepository->earnBadge( $customer, $request, $channel );

        return ['error_code' => '0x0', 'data' => $reference];

    }

    public function validateEarnBadgeData( $merchant_id, $customer_uuid, $request ){

        $customer = $this->customerRepository->getCustomerByUuid( $customer_uuid );

        if( ! $customer ) return ['error_code' => '0x1', 'message' => 'Customer not found.'];
        if( $merchant_id != $customer->merchant->merchantid ) return ['error_code' => '0x2', 'message' => 'Invalid customer.'];

        if( trim($request['badge_group']) == '' ) return ['error_code' => '0x3', 'message' => 'Badge group is required.'];
        if( trim($request['badge_id']) == '' ) return ['error_code' => '0x4', 'message' => 'Badge id is required.'];
        if( ! is_numeric($request['badge_id']) ) return ['error_code' => '0x5', 'message' => 'Badge id should be numeric.'];
        if( trim($request['badge_name']) == '' ) return ['error_code' => '0x6', 'message' => 'Badge name is required.'];
        if( trim($request['validity_date']) == '' ) return ['error_code' => '0x7', 'message' => 'Validity date is required.'];
        if( trim($request['points']) == '' ) return ['error_code' => '0x8', 'message' => 'Points is required.'];
        if( floatval($request['points']) <= 0 ) return ['error_code' => '0x9', 'message' => 'Invalid points.'];
        if( in_array($request['badge_id'], $customer->earned_badge_ids) ) return ['error_code' => '0xa', 'message' => 'Badge already earned.'];

    }

    public function getBadges( $merchant_id, $customer_uuid, $request ){

        $errors = $this->validateGetBadgesData( $merchant_id, $customer_uuid, $request );

        if( ! empty($errors) ) return $errors;

        $badges = $this->customerRepository->getBadges( $customer_uuid, $request );

        return ['error_code' => '0x0', 'data' => $badges];

    }

    public function validateGetBadgesData( $merchant_id, $customer_uuid, $request ){

        $customer = $this->customerRepository->getCustomerByUuid( $customer_uuid );

        if( ! $customer ) return ['error_code' => '0x1', 'message' => 'Customer not found.'];
        if( $merchant_id != $customer->merchant->merchantid ) return ['error_code' => '0x2', 'message' => 'Invalid customer.'];

        // if( ! isset($request['badge_group']) ) return ['error_code' => '0x3', 'message' => 'Badge group is required.'];
        // if( trim($request['badge_group']) == '' ) return ['error_code' => '0x4', 'message' => 'Badge group is required.'];

    }

    public function payCustomerPoints( $merchant_id, $customer_uuid, $request, $channel = null ){

        $feature_bypass = $this->merchantRepository->getGenericFeatureBypass( $merchant_id );
        $validation_bypass = $this->merchantRepository->getGenericValidationBypass( $merchant_id );
        $errors = $this->validateCustomerPointsPayData( $validation_bypass, $merchant_id, $customer_uuid, $request );

        if( ! empty($errors) ) return $errors;

        $customer = $this->customerRepository->getCustomerByUuid( $customer_uuid );
        $employee = $this->employeeRepository->findByUuid( $request['employee_uuid'] );
        $transaction = $this->customerRepository->payPoints( $customer, $employee, $request, $channel );

        if( ! isset($feature_bypass['customer_points_pay']['sms']) ){
            $this->customerLoyaltySmsRepository->payPoints( $transaction );
        }

        return ['error_code' => '0x0', 'data' => $transaction->transactionReferenceCode];

    }

    public function validateCustomerPointsPayData( $validation_bypass, $merchant_id, $customer_uuid, $request ){

        $customer = $this->customerRepository->getCustomerByUuid( $customer_uuid );
        if( ! $customer ) return ['error_code' => '0x1', 'message' => 'Customer not found.'];
        if( $merchant_id != $customer->merchant->merchantid ) return ['error_code' => '0x2', 'message' => 'Invalid customer.'];


        if( isset($request['employee_uuid']) ){
            if( trim($request['employee_uuid']) == '' ) return ['error_code' => '0x3', 'message' => 'Employee uuid is required.'];
            $employee = $this->employeeRepository->findByUuid( $request['employee_uuid'] );
            if( ! $employee ) return ['error_code' => '0x4', 'message' => 'Employee not found.'];
            if( $merchant_id != $employee->merchantId ) return ['error_code' => '0x5', 'message' => 'Invalid employee.'];
        }


        if( isset($request['amount']) ){
            if( trim($request['amount']) == '' ) return ['error_code' => '0x6', 'message' => 'Amount is required.'];
            if( ! is_numeric($request['amount']) || floatval($request['amount']) <= 0 ) return ['error_code' => '0x7', 'message' => 'Invalid amount.'];
        }


        if( trim($request['points']) == '' ) return ['error_code' => '0x8', 'message' => 'Points is required.'];
        if( ! is_numeric($request['points']) || floatval($request['points']) <= 0 ) return ['error_code' => '0x9', 'message' => 'Invalid points.'];
        if( (float) $request['points'] > $customer->points->currentpoints ) return ['error_code' => '1x1', 'message' => 'Customer doesn\'t have enough points.'];


        if( trim($request['or_no']) == '' ) return ['error_code' => '0xa', 'message' => 'OR number is required.'];
        $transaction = $this->customerRepository->getTransactionByOrNo( $request['or_no'], $merchant_id );
        if( $transaction ) return ['error_code' => '0xb', 'message' => 'OR number already exists.'];


        if( ! isset($validation_bypass['customer_points_pay']['pin']['required']) ){
            if( trim($request['pin']) == '' ) return ['error_code' => '0xc', 'message' => 'Pin is required.'];
            if( ! is_numeric($request['pin']) ) return ['error_code' => '0xd', 'message' => 'Invalid pin.'];
            if( $customer->PIN != $request['pin'] ) return ['error_code' => '0xe', 'message' => 'Invalid pin.'];
        }


        // check if customer's pay points transactions count has met the merchant's pay points transaction limit.
        $merchant_pay_points_transaction_limit = (int) $customer->merchant->settings->paypoint_transaction_limit;
        $customer_pay_points_transactions_count = (int) $customer->day_pay_points_transactions_count;

        if( $customer_pay_points_transactions_count >= $merchant_pay_points_transaction_limit ) return ['error_code' => '0xf', 'message' => 'Customer has reached merchant\'s daily pay points transction limit.'];

        // check if customer's total points paid + his points paid for the current transaction
        // has met the merchant's pay points limit.
        $merchant_pay_points_limit = (float) $customer->merchant->settings->paypoint_points_limit;
        $customer_points_paid_total = (float) $customer->day_points_paid_total;
        $points_paid = (float) $request['points'];

        if( ($customer_points_paid_total + $points_paid) > $merchant_pay_points_limit ) return ['error_code' => '1x0', 'message' => 'Customer has reached merchant\'s daily pay points limit.'];

    }

    public function redeemReward( $merchant_id, $customer_uuid, $reward_uuid, $request, $channel = null ){

        $feature_bypass = $this->merchantRepository->getGenericFeatureBypass( $merchant_id );
        $customer = $this->customerRepository->getCustomerByUuid( $customer_uuid );
        $reward = $this->merchantRepository->getMerchantRewardByUuid( $merchant_id, $reward_uuid );
        $employee = $this->employeeRepository->findByUuid( $request['employee_uuid'] );

        $errors = $this->validateRedeemRewardData( $merchant_id, $customer, $reward, $employee, $request );

        if( ! empty($errors) ) return $errors;

        $result = $this->customerRepository->redeemReward( $customer, $reward, $employee, $request, $channel );

        // if redemption's result is an array, it's an error from amax api.
        if( is_array($result) ){

            return $result;

        // if redemption's result is an object, it a transaction object of successful redemption.
        }elseif( is_object($result) ){

            if( ! isset($feature_bypass['customer_rewards_redeem']['sms']) ){
                $this->customerLoyaltySmsRepository->redeemReward( $result );
            }

            return ['error_code' => '0x0', 'data' => $result->transactionReferenceCode];

        }

    }

    public function redeemAmaxReward( $request ){

        $feature_bypass = $this->merchantRepository->getGenericFeatureBypass( $merchant_id );
        $status = $request['outboundRewardRequest']['status'];
        $transaction_id = $request['outboundRewardRequest']['transaction_id'];
        $promo = $request['outboundRewardRequest']['promo'];

        $transaction = $this->customerRepository->getAmaxTransaction( $transaction_id );
        $merchant = $transaction->customer->merchant;
        $customer = $transaction->customer;

        // if amax redemption succeeds, send amax redemption success sms.
        if( $status == 'SUCCESS' ){

            if( ! isset($feature_bypass['customer_rewards_redeem']['amax_sms']) ){
                $this->notification_service->redeemAmaxReward( $merchant, $transaction, $promo );
            }

        // if amax redemption failed, void the redemption transaction,
        // send amax redemption failed sms.
        }elseif( $status == 'FAILED' ){

            $transaction->void();
            $this->customerRepository->unRedeemItem( $transaction->transactionReferenceCode );

            if( ! isset($feature_bypass['customer_rewards_redeem']['amax_sms']) ){
                $this->notification_service->redeemAmaxRewardFailed( $merchant, $customer );
            }
        }

    }

    public function validateRedeemRewardData( $merchant_id, $customer, $reward, $employee, $request ){

        $quantity = ( ! isset($request['quantity']) ? 1 : (int) $request['quantity'] );

        // customer should be an existing merchant customer.
        if( ! $customer ) return ['error_code' => '0x1', 'message' => 'Customer not found.'];
        if( $merchant_id != $customer->merchant->merchantid ) return ['error_code' => '0x2', 'message' => 'Invalid customer.'];

        // reward should be an existing merchant reward.
        if( ! $reward ) return ['error_code' => '0x3', 'message' => 'Reward not found.'];

        // if quantity is given, it should be positive non-zero number.
        // quantity is not applicable to globe rewards.
        if( isset($request['quantity']) && ! $reward->is_globe_reward ){
            if( ! is_numeric($request['quantity']) || (int) $request['quantity'] <= 0 ) return ['error_code' => '0x4', 'message' => 'Invalid quantity.'];
        }

        // if employee_uuid is given, it should be an existing merchant employee.
        if( isset($request['employee_uuid']) ){
            if( ! $employee ) return ['error_code' => '0x5', 'message' => 'Employee not found.'];
            if( $merchant_id != $employee->merchantId ) return ['error_code' => '0x6', 'message' => 'Invalid employee.'];
        }

        // if pin is given, it should match the customer's pin.
        if( isset($request['pin']) ){
            if( $customer->PIN != $request['pin'] ) return ['error_code' => '0x7', 'message' => 'Invalid pin.'];
        }

        // check if customer have enough points.
        if( $customer->points->currentpoints < ((float) $reward->pointsRequired * $quantity) ) return ['error_code' => '0x8', 'message' => 'Insufficient points.'];

        // globe reward is only for prepaid globe subscriber.
        if( $reward->is_globe_reward && ! $customer->is_globe_subscriber ) return ['error_code' => '0x9', 'message' => 'Globe reward is only for prepaid globe subscriber.'];
        if( $reward->is_globe_reward && $customer->is_globe_subscriber && ! $customer->is_prepaid_globe_subscriber ) return ['error_code' => '0xa', 'message' => 'Globe reward is only for prepaid globe subscriber.'];

        // check if customer's redemption transactions count has reached merchant's redemption transaction limit.
        $merchant_redemption_transaction_limit = (int) $customer->merchant->settings->redemption_transaction_limit;
        $customer_redemption_transactions_count = (int) $customer->day_redemption_transactions_count;

        if( $customer_redemption_transactions_count >= $merchant_redemption_transaction_limit ) return ['error_code' => '0xb', 'message' => 'Customer has reached merchant\'s redemption transaction limit.'];

    }

    public function getUnclaimedRewards( $merchant_id, $customer_uuid, $request ){

        $employee = $this->employeeRepository->findByUuid( $request['employee_uuid'] );
        $errors = $this->validateGetUnclaimedRewardsData( $merchant_id, $customer_uuid, $employee, $request );

        if( ! empty($errors) ) return $errors;

        $rewards = $this->customerRepository->getUnclaimedRewards( $customer_uuid, $employee );

        return ['error_code' => '0x0', 'data' => $rewards];

    }

    public function validateGetUnclaimedRewardsData( $merchant_id, $customer_uuid, $employee, $request ){

        $customer = $this->customerRepository->getCustomerByUuid( $customer_uuid );

        if( ! $customer ) return ['error_code' => '0x1', 'message' => 'Customer not found.'];
        if( $merchant_id != $customer->merchant->merchantid ) return ['error_code' => '0x2', 'message' => 'Invalid customer.'];

        if( isset($request['employee_uuid']) ){
            if( ! $employee ) return ['error_code' => '0x3', 'message' => 'Employee not found.'];
            if( $merchant_id != $employee->merchantId ) return ['error_code' => '0x4', 'message' => 'Invalid employee.'];
        }

    }

    public function claimReward( $merchant_id, $customer_uuid, $request ){

        $customer = $this->customerRepository->getCustomerByUuid( $customer_uuid );
        $employee = $this->employeeRepository->findByUuid( $request['employee_uuid'] );
        $redemption = $this->customerRepository->getRedemptionByUuid( $request['redemption_uuid'] );

        $errors = $this->validateClaimRewardData( $merchant_id, $customer, $employee, $redemption, $request );

        if( ! empty($errors) ) return $errors;

        $claim_result = $this->customerRepository->claimReward( $customer, $employee, $redemption, $request );

        if( $claim_result ){
            if( $customer->merchant->business_system_link != 'gmovies' ){
                $this->customerLoyaltySmsRepository->claimReward( $redemption->transaction );
            }
        }

        return ['error_code' => '0x0'];

    }

    public function validateClaimRewardData( $merchant_id, $customer, $employee, $redemption, $request ){

        if( ! $customer ) return ['error_code' => '0x1', 'message' => 'Customer not found.'];
        if( $merchant_id != $customer->merchant->merchantid ) return ['error_code' => '0x2', 'message' => 'Invalid customer.'];
        if( $customer->PIN != $request['pin'] ) return ['error_code' => '0x3', 'message' => 'Invalid pin.'];

        if( ! $employee ) return ['error_code' => '0x4', 'message' => 'Employee not found.'];
        if( $merchant_id != $employee->merchantId ) return ['error_code' => '0x5', 'message' => 'Invalid employee.'];

        if( ! $redemption ) return ['error_code' => '0x6', 'message' => 'Redemption not found.'];
        if( $redemption->status == 1 ) return ['error_code' => '0x7', 'message' => 'Invalid redemption.'];
        if( $redemption->customerId != $customer->customerId ) return ['error_code' => '0x8', 'message' => 'Invalid redemption.'];

        if( isset($request['quantity']) ){

            if( ! is_numeric($request['quantity']) || (int) $request['quantity'] <= 0 ) return ['error_code' => '0x9', 'message' => 'Invalid quantity.'];
            if( ($redemption->claimed + (int) $request['quantity']) > $redemption->quantity ) return ['error_code' => '0xa', 'message' => 'Invalid quantity.'];

        }

    }

    public function getTransactions( $uuid ){

        return $this->customerRepository->getTransactions( $uuid );

    }

    public function getTransactionsCap( $uuid ){

        return $this->customerRepository->getTransactionsCap( $uuid );

    }

    public function getCard( $customer_uuid ){

        return $this->customerRepository->getCard( $customer_uuid );

    }

    public function postStamps( $customer_uuid, $request, $channel = null ){

        return $this->customerRepository->earnStamps( $customer_uuid, $request, $channel );

    }

    public function postRedeem( $customer_uuid, $request, $channel = null ){

        return $this->customerRepository->redeemStamps( $customer_uuid, $request, $channel );

    }

    public function postVoidStamp( $customer_uuid, $request ){

        return $this->customerRepository->voidStamp( $customer_uuid, $request );

    }

    public function updatePin( $customer_uuid, $request ){

        return $this->customerRepository->updatePin( $customer_uuid, $request );

    }

    public function smsService( $request ){

        if( strtolower(env('APP_ENV')) == 'local' ){
            $sender = $request->sender;
            $message = $request->message;
        }else{
            $sender = $request->inboundSMSMessageList['inboundSMSMessage'][0]['senderAddress'];
            $message = $request->inboundSMSMessageList['inboundSMSMessage'][0]['message'];
        }

        // get 11-digit sender.
        $sender = $this->getElevenDigitSender( $sender );

        // get sms keyword.
        $keyword = $this->getSmsKeyword( $message );

        // BENE sms service.
        if( $this->beneCustomerRepository->isBeneSmsService( $message ) ) return $this->beneCustomerRepository->processBeneSmsService( $sender, $keyword, $message );

        // NATCON sms service.
        if( $this->natConCustomerRepository->isNatConSmsService( $message ) ) return $this->natConCustomerRepository->processNatConSmsService( $sender, $keyword, $message );

        // execute the corresponding action based on the sms keyword.
        if( $keyword == 'REG' ) return $this->smsRegister( $sender, $message );
        elseif( $keyword == 'POINTS' ) return $this->sendCustomerPointsSms( $sender, $message );
        elseif( $keyword == 'TRANSFER' ) return $this->transferCustomerPoints( $sender, $message );
        elseif( $keyword == 'KEYWORD' ) return $this->sendKeywordsList( $sender, $message );
        elseif( $keyword == 'UPDATE' ) return $this->smsCustomerUpdate( $sender, $message );
        else return $this->customerLoyaltySmsRepository->sendInvalidKeywordNotif( $sender );

    }

    public function getElevenDigitSender( $sender ){

        return str_replace('tel:+63', '0', $sender);

    }

    public function getSmsKeyword( $message ){

        $partials = explode(' ', $message);
        $keyword = ( isset($partials[0]) ? strtoupper(trim($partials[0])) : '' );

        return $keyword;

    }

    public function smsRegister( $sender, $message ){

        // extract the registration message.
        $partials = $this->getRegistrationMessagePartials( $message );

        // try to get merchant even merchant code is still unverified.
        $merchant = $this->merchantRepository->getMerchantByMerchantCode( $partials['merchant_code'] );


        // validate existence and format of fields in the message.
        if( ! $this->validateSmsRegistrationFields( $partials, $merchant ) ){
            if( ! is_null($merchant) && $merchant->is_pnp ){
                return $this->customerLoyaltySmsRepository->sendPnpRegistrationFailedNotif( $sender );
            }else{
                return $this->customerLoyaltySmsRepository->sendRegistrationFailedNotif( $sender );
            }
        }


        // validate duplicate contact.
        if( $this->customerRepository->validateSmsRegistrationContact( $sender, $partials ) > 0 ) return $this->customerLoyaltySmsRepository->sendDuplicateContactNotif( $sender, $partials );

        // validate duplicate email.
        if( ! $this->customerRepository->validateSmsRegistrationEmail( $partials ) ) return $this->customerLoyaltySmsRepository->sendDuplicateEmailNotif( $sender, $partials );


        // validate referral code as card number for plains and prints.
        if( ! is_null($merchant) && $merchant->is_pnp ){

            $existing_card_number = $this->validateExistingPnpCardNumber( $partials['referral_code'], $partials['birthdate'] );
            $new_card_number = $this->validateNewPnpCardNumber( $partials['referral_code'] );

            if( ! $existing_card_number && ! $new_card_number ) return $this->customerLoyaltySmsRepository->sendInvalidCardNumberNotif( $sender );

        // validate referral code if given.
        }else{
            if( ! $this->customerRepository->validateSmsRegistrationReferralCode( $partials ) ) return $this->customerLoyaltySmsRepository->sendInvalidReferralCodeNotif( $sender );
        }


        // register customer.
        $customer = $this->customerRepository->smsRegister( $sender, $partials );

        // extra process if merchant is plains and prints.
        if( ! is_null($merchant) && $merchant->is_pnp ){

            // flag the registered card number as migrated.
            $this->flagRegisterdPnpCardNumber( $partials['referral_code'] );

            // update customer card number and birthdate.
            $customer->firstName = $partials['referral_code'];
            $customer->birthDate = $partials['birthdate'];
            $customer->address = $partials['city'];
            $customer->save();

            // sync customer pnp data to rush.
            $this->syncCustomerDataFromPnp( $customer );

            // update plains and prints data based on registered data.
            $request = new \stdClass;
            $request->name = $partials['name'];
            $request->email = $partials['email'];
            $request->mobile = $sender;
            $this->updatePnpDataFromRegistration( $partials['referral_code'], $request );

        }


        // notify customer of the successful registration.
        $this->customerLoyaltySmsRepository->sendRegistrationSuccessNotif( $customer );

        // at this point, if referral code is given, it is automatically valid.
        // return the member referral params to be used in achievement service in customer cotnroller.
        // also, merchant automatically exists, but don't return this for plains and prints.
        if( ! $merchant->is_pnp ){
            if( $partials['referral_code'] != '' ) return $this->customerRepository->getMemberReferralParams( $customer, $partials['referral_code'] );
        }

    }

    public function sendCustomerPointsSms( $sender, $message ){

        // get points inquiry message partials.
        $partials = $this->getPointsInquiryMessagePartials( $message );


        // validate merchant code.
        if( ! $this->validateSmsPointsInquiryMerchantCode( $partials ) ) return $this->customerLoyaltySmsRepository->sendPointsInquiryFailedNotif( $sender );

        // validate sender existence.
        if( $this->customerRepository->validateSmsRegistrationContact( $sender, $partials ) == 0 ) return $this->customerLoyaltySmsRepository->sendUnregisteredNumberNotif( $sender, $partials );


        // send customer points sms.
        $this->customerLoyaltySmsRepository->sendCustomerPointsSms( $sender, $partials );

    }

    public function transferCustomerPoints( $sender, $message ){

        // get transfer points message partials.
        $partials = $this->getTransferPointsMessagePartials( $message );


        // validate message format.
        if( ! $this->validateTransferPointsFields( $partials ) ) return $this->customerLoyaltySmsRepository->sendTransferPointsFailedNotif( $sender );

        // validate if points is zero.
        if( (float) $partials['points'] == 0 ) return $this->customerLoyaltySmsRepository->sendZeroPointsTransferNotif( $sender );

        // validate if points is negative.
        if( (float) $partials['points'] < 0 ) return $this->customerLoyaltySmsRepository->sendNegativePointsTransferNotif( $sender );

        // validate sender membership.
        if( $this->customerRepository->validateSmsRegistrationContact( $sender, $partials ) == 0 ) return $this->customerLoyaltySmsRepository->sendUnregisteredNumberNotif( $sender, $partials );

        // validate receipient membership.
        if( $this->customerRepository->validateSmsRegistrationContact( $partials['receiver'], $partials ) == 0 ) return $this->customerLoyaltySmsRepository->sendTransferPointsInvalidRecipientNotif( $sender, $partials );

        // validate if sender and receiver are the saeme.
        if( $sender == $partials['receiver'] ) return $this->customerLoyaltySmsRepository->sendTransferPointsToSelfNotif( $sender );

        // validate points transfer settings of merchant.
        if( $this->customerRepository->validatePointsTransferSettings( $sender, $partials ) == 0 ) return $this->customerLoyaltySmsRepository->sendDisabledPointsTransferNotif( $sender, $partials );

        // validate points sufficiency.
        if( ! $this->customerRepository->validateTransferPointsSufficiency( $sender, $partials ) ) return $this->customerLoyaltySmsRepository->sendInsufficientPointsNotif( $sender, $partials );

        // validate customer pin.
        if( ! $this->customerRepository->validateTransferPointsCustomerPin( $sender, $partials ) ) return $this->customerLoyaltySmsRepository->sendTransferPointsInvalidPinNotif( $sender, $partials );


        // transfer points.
        $transaction = $this->customerRepository->transferCustomerPoints( $sender, $partials );

        // send success message of points transfer.
        $this->customerLoyaltySmsRepository->sendTransferPointsSuccessNotif( $sender, $partials, $transaction->transactionReferenceCode );
        $this->customerLoyaltySmsRepository->sendReceivePointsSuccessNotif( $sender, $partials, $transaction->transactionReferenceCode );

    }

    public function smsCustomerUpdate( $sender, $message )
    {
        // extract the update message.
        $partials = $this->getCustomerUpdateMessagePartials( $message );

        // try to get merchant even merchant code is still unverified.
        $merchant = $this->merchantRepository->getMerchantByMerchantCode( $partials['merchant_code'] );


        // this function should only be ussable to plains and prints.
        if( ! $merchant ) return $this->customerLoyaltySmsRepository->sendCustomerUpdateFailedNotif( $sender );
        if( ! $merchant->is_pnp ) return $this->customerLoyaltySmsRepository->sendCustomerUpdateFailedNotif( $sender );


        // validate if contact exists.
        if( $this->customerRepository->validateSmsRegistrationContact( $sender, $partials ) == 0 ) return $this->customerLoyaltySmsRepository->sendUnregisteredPnpNumberNotif( $sender, $partials );

        // validate existence and format of fields in the message.
        if( ! $this->validateSmsCustomerUpdateFields( $partials ) ) return $this->customerLoyaltySmsRepository->sendCustomerUpdateFailedNotif( $sender );

        // validate duplicate email.
        if( ! $this->customerRepository->validateSmsCustomerUpdateEmail( $sender, $partials ) ) return $this->customerLoyaltySmsRepository->sendDuplicateEmailNotif( $sender, $partials );

        // card number should not be existing and should be on the whitelist.
        $new_card_number = $this->validateNewPnpCardNumber( $partials['card_number'] );

        if( ! $new_card_number ) return $this->customerLoyaltySmsRepository->sendInvalidCardNumberNotif( $sender );

        // sms update customer.
        $customer = $this->customerRepository->smsCustomerUpdate( $sender, $partials );

        // flag the registered card number as migrated.
        $this->flagRegisterdPnpCardNumber( $partials['card_number'] );

        // update plains and prints data based on registered data.
        $request = new \stdClass;
        $request->name = $partials['name'];
        $request->email = $partials['email'];
        $request->mobile = $sender;
        $this->updatePnpDataFromRegistration( $partials['card_number'], $request );

        // notify customer of the successful update.
        return $this->customerLoyaltySmsRepository->sendCustomerUpdateSuccessNotif( $customer );
    }

    public function getRegistrationMessagePartials( $message ){

        $message_arr = explode(',', $message);

        $first_partial = ( isset($message_arr[0]) ? trim($message_arr[0]) : '' );
        $first_partial_arr = explode(' ', $first_partial);

        $partials['merchant_code'] = ( isset($first_partial_arr[1]) ? strtoupper(trim($first_partial_arr[1])) : '' );

        $partials['name'] = ( isset($message_arr[1]) ? trim($message_arr[1]) : '' );
        $partials['email'] = ( isset($message_arr[2]) ? trim($message_arr[2]) : '' );
        $partials['pin'] = ( isset($message_arr[3]) ? trim($message_arr[3]) : '' );
        $partials['referral_code'] = ( isset($message_arr[4]) ? trim($message_arr[4]) : '' );

        // additional partials for plains and prints.
        $partials['birthdate'] = ( isset($message_arr[5]) ? trim($message_arr[5]) : '' );
        $partials['city'] = ( isset($message_arr[6]) ? trim($message_arr[6]) : '' );

        return $partials;

    }

    public function getCustomerUpdateMessagePartials( $message )
    {
        $message_arr = explode(',', $message);

        $first_partial = ( isset($message_arr[0]) ? trim($message_arr[0]) : '' );
        $first_partial_arr = explode(' ', $first_partial);

        $partials['merchant_code'] = ( isset($first_partial_arr[1]) ? strtoupper(trim($first_partial_arr[1])) : '' );

        $partials['name'] = ( isset($message_arr[1]) ? trim($message_arr[1]) : '' );
        $partials['email'] = ( isset($message_arr[2]) ? trim($message_arr[2]) : '' );
        $partials['pin'] = ( isset($message_arr[3]) ? trim($message_arr[3]) : '' );
        $partials['card_number'] = ( isset($message_arr[4]) ? trim($message_arr[4]) : '' );
        $partials['birthdate'] = ( isset($message_arr[5]) ? trim($message_arr[5]) : '' );
        $partials['city'] = ( isset($message_arr[6]) ? trim($message_arr[6]) : '' );

        return $partials;
    }

    public function validateSmsCustomerUpdateFields( $partials )
    {
        $fields['merchant_code'] = $partials['merchant_code'];
        $fields['name'] = $partials['name'];
        $fields['email'] = $partials['email'];
        $fields['pin'] = $partials['pin'];
        $fields['card_number'] = $partials['card_number'];
        $fields['birthdate'] = $partials['birthdate'];
        $fields['city'] = $partials['city'];

        $rules['merchant_code'] = 'required|exists:MerchantCode,code,enabled,1';
        $rules['name'] = 'required';
        $rules['email'] = 'required|email';
        $rules['pin'] = 'required|regex:/^\d{4}$/';
        $rules['card_number'] = 'required';
        $rules['birthdate'] = 'required|regex:/^\d{2}\/\d{2}\/\d{4}$/';
        $rules['city'] = 'required';

        $validator = Validator::make($fields, $rules);

        return $validator->passes();
    }

    public function validateSmsRegistrationFields( $partials, $merchant = null ){

        $fields['merchant_code'] = $partials['merchant_code'];
        $fields['name'] = $partials['name'];
        $fields['email'] = $partials['email'];
        $fields['pin'] = $partials['pin'];

        $rules['merchant_code'] = 'required|exists:MerchantCode,code,enabled,1';
        $rules['name'] = 'required';
        $rules['email'] = 'required|email';
        $rules['pin'] = 'required|regex:/^\d{4}$/';

        // additional validation rules for plains and prints.
        if( ! is_null($merchant) && $merchant->is_pnp ){
            $fields['birthdate'] = $partials['birthdate'];
            $fields['city'] = $partials['city'];

            $rules['birthdate'] = 'required|regex:/^\d{2}\/\d{2}\/\d{4}$/';
            $rules['city'] = 'required';
        }

        $validator = Validator::make($fields, $rules);

        return $validator->passes();

    }

    public function getPointsInquiryMessagePartials( $message ){

        $message_arr = explode(' ', $message);

        $partials['merchant_code'] = ( isset($message_arr[1]) ? strtoupper(trim($message_arr[1])) : '' );

        return $partials;

    }

    public function validateSmsPointsInquiryMerchantCode( $partials ){

        $validator = Validator::make(
            [ 'merchant_code' => $partials['merchant_code'] ],
            [ 'merchant_code' => 'required|exists:MerchantCode,code,enabled,1' ]
        );

        return $validator->passes();

    }

    public function getTransferPointsMessagePartials( $message ){

        $message_arr = explode(',', $message);

        $first_partial = ( isset($message_arr[0]) ? trim($message_arr[0]) : '' );
        $first_partial_arr = explode(' ', $first_partial);

        $partials['merchant_code'] = ( isset($first_partial_arr[1]) ? strtoupper(trim($first_partial_arr[1])) : '' );

        $partials['receiver'] = ( isset($message_arr[1]) ? trim($message_arr[1]) : '' );
        $partials['points'] = ( isset($message_arr[2]) ? trim($message_arr[2]) : '' );
        $partials['pin'] = ( isset($message_arr[3]) ? trim($message_arr[3]) : '' );

        return $partials;

    }

    public function validateTransferPointsFields( $partials ){

        $validator = Validator::make(
            [
                'merchant_code' => $partials['merchant_code'],
                'receiver' => $partials['receiver'],
                'points' => $partials['points'],
                'pin' => $partials['pin']
            ],
            [
                'merchant_code' => 'required|exists:MerchantCode,code,enabled,1',
                'receiver' => 'required|regex:/^0\d{10}$/',
                'points' => 'required|numeric',
                'pin' => 'required|regex:/^\d{4}$/',
            ]
        );

        return $validator->passes();

    }

    public function sendKeywordsList( $sender, $message ){

        // keywords list message partials anda validations are same with points inquiry.

        // get keywords list message partials.
        $partials = $this->getPointsInquiryMessagePartials( $message );


        // validate merchant code.
        // missing or incorrect merchant code should not get a response.
        // if( ! $this->validateSmsPointsInquiryMerchantCode( $partials ) ) return $this->customerLoyaltySmsRepository->sendInvalidKeywordNotif( $sender );
        if( ! $this->validateSmsPointsInquiryMerchantCode( $partials ) ) return;

        // validate sender membership.
        if( $this->customerRepository->validateSmsRegistrationContact( $sender, $partials ) == 0 ) return $this->customerLoyaltySmsRepository->sendUnregisteredNumberNotif( $sender, $partials );


        // send customer keywords list sms.
        $this->customerLoyaltySmsRepository->sendKeywordsList( $sender, $partials );

    }

    public function forgotPin( $merchant_id, $request ){

        $customer = $this->customerRepository->getCustomerByMobile( $request['mobile_no'], $merchant_id );

        $forgot_pin_result = $this->customerRepository->forgotPin( $customer );

        if( $forgot_pin_result['error_code'] == '0x0' ) $this->customerLoyaltySmsRepository->forgotPin( $customer );

        return $forgot_pin_result;

    }

    public function saveMemberReferral($data)
    {
       return $this->customerRepository->saveMemberReferral($data);
    }

    public function earnNonMemberPoints( $merchant_id, $request, $channel = null ){

        $errors = $this->validateNonMemberPointsEarnData( $merchant_id, $request );

        if( ! empty($errors) ) return $errors;

        $employee = $this->employeeRepository->findByUuid( $request['employee_uuid'] );
        $transaction = $this->customerRepository->earnNonMemberPoints( $employee, $request, $channel );
        $this->customerLoyaltySmsRepository->earnNonMemberPoints( $transaction );

        return ['error_code' => '0x0', 'data' => $transaction->transactionReferenceCode];

    }

    public function getNonMemberTransactions($merchantId)
    {
        return $this->nonMemberTransactionRepository->getByMerchant($merchantId);
    }

    public function issueReward( $customer_uuid, $request ){

        $issue_reward_result = $this->customerRepository->issueReward( $customer_uuid, $request );

        if( $issue_reward_result['error_code'] == '0x0' ){
            $punchcard_transaction = $this->customerRepository->getPunchcardTransaction( $issue_reward_result['data'] );
            $this->customerLoyaltySmsRepository->issueReward( $punchcard_transaction );
        }

        return $issue_reward_result;

    }

    public function gatherCustomerDataTableData($request, $branch = null){
        $customerQuery = $this->customerRepository->buildCustomerQuery($request);

        if(!empty($branch)){
            $customer_ids = $this->customerPunchcardTransactionRepository->getByBranchId($branch->id)->pluck('customer_id')->toArray();

            if(empty($customer_ids)){
                $customer_ids = [0];
            }

            $customerQuery->whereIn('customerId', $customer_ids);
        }

        $response_data['draw'] = $request->draw;
        $response_data['data'] = $this->customerRepository->getCustomerData($customerQuery, $request);
        $response_data['recordsFiltered'] = $this->customerRepository->countFilterData($customerQuery);
        $response_data['recordsTotal'] = $customerQuery->count();
        return $response_data;
    }

    public function gatherCustomerPointsDataTableData($request, $branch = null){
        $customerPointsQuery = $this->customerRepository->buildCustomerPointsQuery($request);

        if(!empty($branch)){
            $customer_ids = $this->customerTransactionRepository->getByBranchId($branch->id)->pluck('customerId')->toArray();

            if(empty($customer_ids)){
                $customer_ids = [0];
            }

            $customerPointsQuery->whereIn('customerId', $customer_ids);
        }

        $response_data['draw'] = $request->draw;
        $response_data['data'] = $this->customerRepository->getCustomerData($customerPointsQuery, $request);
        $response_data['recordsFiltered'] = $this->customerRepository->countFilterData($customerPointsQuery);
        $response_data['recordsTotal'] = $this->customerRepository->countAllCustomer($request);
        return $response_data;
    }

    public function getCustomerWithReserationsDetails($customer_id){
        $customer_info = $this->customerRepository->getCustomerWithReserationsDetails($customer_id);

        $today = Carbon::today();
        $available_package = [];
        $expired_package = [];
        $current_reservation = [];
        $previous_reservation = [];

        foreach($customer_info->class_packages as $customer_package){
            $package['branch'] = ($customer_package->branch_ids == null) ? 'All Branches' : '';
            $package['package_name'] = $customer_package->package->name;
            $package['start_date'] = $customer_package->start_date->toDateString();
            $package['end_date'] = $customer_package->end_date->toDateString();
            $package['amount'] = $customer_package->transaction->amount;
            $package['or_no'] = $customer_package->transaction->or_no;
            $package['remaining'] = $customer_info->reservations()->where('customer_package_id', $customer_package->id)->count() . '/' . $customer_package->package->no_of_visits;

            if($customer_package->end_date->lt($today)){
                $package['status'] = "In Active";

                $expired_package[] = $package;
            } else {
                $package['status'] = "Available for Use";

                $available_package[] = $package;
            }
        }

        foreach ($customer_info->reservations as $reservation) {
            if($reservation->class_list != null){
                $temp_reservation['date'] = $reservation->class_list->start_date_time->toDateString();
                $temp_reservation['time'] = $reservation->class_list->start_time_label . " - " . $reservation->class_list->end_time_label;
                $temp_reservation['package_name'] = $reservation->customer_package->package->name;
                $temp_reservation['class_name'] = $reservation->class_list->schedule->classes->name;
                $temp_reservation['branch'] = $reservation->class_list->schedule->branch->name;

                if($reservation->class_list->instructor_status != 'original'){
                    $temp_reservation['instructor'] = $reservation->class_list->instructor->name . "<span style='color:red;'>(substitute)</span>";
                } else {
                    $temp_reservation['instructor'] = $reservation->class_list->instructor->name;
                }

                if($reservation->class_list->start_date_time->gte($today)){
                    if($reservation->class_list->start_date_time->isPast()){
                        $temp_reservation['status'] = ($reservation->class_list->showed_up == 1) ? "Showed Up" : "No Show";
                    } else {
                        $temp_reservation['status'] = ($reservation->class_list->showed_up == 1) ? "Showed Up" : "Booked";
                    }

                    $current_reservation[] = $temp_reservation;
                } else {
                    $temp_reservation['status'] = ($reservation->class_list->showed_up == 1) ? "Showed Up" : "No Show";
                    $previous_reservation[] = $temp_reservation;
                }
            }
        }

        $customer_info->packages = array_merge($available_package, $expired_package); //tab1
        $customer_info->current_reservation = $current_reservation; //tab2
        $customer_info->previous_reservation = $previous_reservation; //tab3

        return $customer_info;
    }

    public function validateCustomerData(&$customerInput)
    {
        $merchant = $this->merchantRepository->getById($customerInput['merchantId']);

        $validationRules = [
            'fullName' => 'required',
            'mobileNumber' => ($merchant->settings->customer_login_param == 'email') ? 'sometimes' : 'required'.'|mobilenumber|unique:'.$this->customerRepository->getTable().',mobileNumber,NULL,id,merchantId,'.$customerInput['merchantId'],
            'email' => 'sometimes|required|email',
            'client_code' => 'sometimes|exists:'.$this->clientRepository->getTable().',code,merchant_id,'.$customerInput['merchantId'].'|unique:'.$this->customerRepository->getTable().',client_code,NULL,id,merchantId,'.$customerInput['merchantId'],
            'birthDate' => 'sometimes|before:'.date("Y-m-d").'|date_format:"Y-m-d"',
            'gender' => 'sometimes|in:m,f,M,F',
            'PIN' => ($merchant->settings->customer_login_param == 'email') ? 'required' : 'sometimes'.'|regex:/^\d{4}$/'
        ];

        $validationMessages = [
            'required' => 'The :attribute is required.',
            'unique' => 'The :attribute has already been registered.',
            'mobileNumber.unique' => 'Mobile number already used.',
            'client_code.exists' => 'Client Code is not valid.',
            'client_code.unique' => 'Client Code already used.',
            'gender.in' => 'Gender field uses "m" or "f"'
        ];

        if($merchant->settings->allow_multiple_email != 1) {
            $validationRules['email'] = 'sometimes|required|email|unique:'.$this->customerRepository->getTable().',email,NULL,id,merchantId,'.$customerInput['merchantId'];
            $validationMessages['email.unique'] = 'Email address already used.';
        }

        $validator = $this->validate(
            $customerInput,
            $validationRules,
            $validationMessages
        );

        if ($validator->fails()) {
            return $validator;
        }

        return true;
    }

    public function addCustomerData(array $input)
    {
        $customerInput = $input['customer'];

        $validateResult = $this->validateCustomerData($customerInput);

        if (!is_bool($validateResult) && $validateResult->failed()) {
            return $validateResult;
        }

        $this->customerRepository->createCustomerFromCms($customerInput);

        return true;
    }

    public function readCustomerData($customersDataFilePath)
    {
        $rowData = false;
        $customers = [];
        $columns = [];
        $columnsCount = 0;
        $reader = ReaderFactory::create(Type::XLSX);
        $reader->setShouldFormatDates(true);
        $reader->open($customersDataFilePath);

        foreach ($reader->getSheetIterator() as $sheet) {
            // only read data from "Members" sheet
            if ($sheet->getName() === 'Members') {
                foreach ($sheet->getRowIterator() as $row) {
                    if ($rowData) {
                        if ($row[0] === "") {
                            break;
                        }
                        $customers[] = array_combine($columns, array_map('trim', array_slice($row, 0, $columnsCount)));
                    } else {
                        if ($row[0] == 'fullName') {
                            $columns = array_filter($row);
                            $columnsCount = count($columns);
                            $rowData = true;
                        }
                    }
                }
                break;
            }
        }

        $reader->close();

        return $customers;
    }

    public function uploadCustomerData(array $input)
    {
        $members = [];
        $memberCount = 0;
        $customerInput = $input['customer'];
        $validator = $this->validate(
            $customerInput,
            [   'customerData' => 'required|mimetypes:application/octet-stream,application/zip,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
            [   'required' => 'The :attribute is required.']
        );
        if ($validator->fails()) {
            return $validator;
        }

        // read file
        $originalFilename = '';
        if (Input::hasFile('customerData')) {
            $members = $this->readCustomerData(Input::file('customerData'));
            $membersCount = count($members);
            $originalFilename = Input::file('customerData')->getClientOriginalName();
            $originalFilename = preg_replace("/[^a-z0-9\.]/", "", strtolower($originalFilename));
        }

        // avoid duplicates in mobileNumber
        if ($membersCount != count(array_unique(array_column($members, 'mobileNumber')))) {
            $validator->errors()->add('mobileNumber', 'The mobileNumber column contains duplicates!');
        }

        // avoid duplicates in email (check if multiple_email is allowed)
        $merchant = $this->merchantRepository->getById($customerInput['merchantId']);
        if($merchant->settings->allow_multiple_email != 1) {
            if ($membersCount != count(array_unique(array_column($members, 'email')))) {
                $validator->errors()->add('email', 'The email column contains duplicates!');
            }
        }

        // avoid duplicates in client_code
        $filtered = array_filter(array_column($members, 'client_code'), 'strlen');
        if (count($filtered) > count(array_unique($filtered))) {
            $validator->errors()->add('client_code', 'The client_code column contains duplicates!');
        }

        if ($validator->errors()->count()) {
            return $validator;
        }

        // model insert pre-validation
        $i = 0;
        foreach($members as $member) {
            $i++;
            $memberStr = 'member_'.$i;
            $member['merchantId'] = $customerInput['merchantId'];
            $result = $this->validateCustomerData($member);
            if (!is_bool($result) && $result->errors()->count()) {
                foreach ($result->errors()->messages() as $key => $value) {
                    $validator->errors()->add($memberStr, $memberStr.' : '.$value[0]);
                }
            }
        }

        if ($validator->errors()->count()) {
            return $validator;
        }

        foreach($members as $member) {
            $member['merchantId'] = $customerInput['merchantId'];
            $this->customerRepository->createCustomerFromCms($member);
        }

        return true;
    }

    public function editCustomerData($request){
        $merchant_id = $request->merchant_id;
        $customer_id = $request->customer_id;
        $mobile_number = $request->mobile_number;
        $email_address = $request->email_address;
        $client_code = $request->client_code;
        $pin = $request->PIN;
        $change_pin = $request->change_pin;

        $merchant = $this->merchantRepository->getById($merchant_id);

        if(empty($email_address)){
            return ['message' => 'E-mail is required'];
        }

        if($merchant->settings->customer_login_param == "mobile"){
            if(empty($mobile_number)){
                return ['message' => 'Mobile number is required'];
            }
        }

        if(!empty($mobile_number)){
            if($this->customerRepository->checkIfMobileNumberNotUnique($mobile_number, $customer_id, $merchant_id)){
                return ['message' => 'Mobile number already use'];
            }
        }

        if($merchant->settings->allow_multiple_email != 1){
            if($this->customerRepository->checkIfEmailAddressNotUnique($email_address, $customer_id, $merchant_id)){
                return ['message' => 'Email address already use'];
            }
        }

        if($this->customerRepository->checkIfCustomerHasPriorClientCode($customer_id, $merchant_id)){
            if(empty($client_code)){
                return ['message' => 'Client Code is required'];
            }
        }

        if(!empty($client_code)){
            if($this->customerRepository->checkIfClientCodeNotValid($client_code, $merchant_id)){
                return ['message' => 'Client Code is not valid'];
            }

            if($this->customerRepository->checkIfClientCodeNotUnique($client_code, $customer_id, $merchant_id)){
                return ['message' => 'Client Code already use'];
            }
        }

        if($change_pin){
            if(empty($pin)){
                return ['message' => 'PIN Code is required'];
            }
        }

        return $this->customerRepository->editCustomerFromCms($request);
    }

    public function earnClientStamps($client_id, $employee_uuid, $reference = null)
    {
        $transaction = $this->customerRepository->earnClientStamps($client_id, $employee_uuid, $reference);
        if ($transaction) {
            $sms_message = $this->merchantRepository->getSmsNotificationMessages($transaction->employee->merchant->id, 'earn-client-stamp');
            if (!$sms_message) {
                $sms_message = $this->smsNotificationRepository->getDefaultMessage('earn-client-stamp');
            }
            $sms = new SmsHelper(
                $transaction->employee->merchant->sms_credentials->shortcode,
                $transaction->employee->merchant->sms_credentials->app_id,
                $transaction->employee->merchant->sms_credentials->app_secret,
                $transaction->employee->merchant->sms_credentials->passphrase
            );
            if ($sms_message) {
                $datetime = Carbon::now();
                $data = [
                    'transaction_stamps' => $transaction->stamps,
                    'stamp_name' => $transaction->customer->merchant->settings->points_name,
                    'business' => $transaction->customer->merchant->businessName,
                    'datetime' => $datetime->format('m-d-Y') .','. $datetime->format('g:i:s A'),
                    'current_stamps' => $transaction->customer->card->stamps,
                    'reference' => $transaction->transaction_ref,
                    'branch' => $transaction->employee->branch->name
                ];
                $message = $sms_message->message;
                foreach ($data as $key => $value) {
                    $message = str_replace("%". $key ."%", $value, $message);
                }
                $response = $sms->send($transaction->customer->mobile_no, $message);
                SmsLogHelper::log($transaction->customer->mobile_no, $message, $response, $transaction->employee->merchant->id, 'earn-client-stamp');
            }
        }

        return $transaction;
    }

    public function storeAttendee(array $input)
    {
        $attendeeInput = $input['attendee'];
        $attendeeInput['merchantId'] = $attendeeInput['merchant_id'];
        $attendeeInput['PIN'] = '0000';
        $attendeeInput['mobileNumber'] = (strlen($attendeeInput['mobileNumber']) == 10) ? '0'.$attendeeInput['mobileNumber'] : $attendeeInput['mobileNumber'];
        $attendeeInput['attendee_status'] = (array_key_exists('attendee_status', $attendeeInput) && $attendeeInput['attendee_status'] == 'on') ? 1 : 0;

        $rules = [
            'fullName' => 'required',
            'email' => 'required|email',
            'PIN' => 'required',
            'merchantId' => 'required'
        ];
        $customerTable = $this->customerRepository->getTable();
        $oldRecord = false;
        if (array_key_exists('uuid', $attendeeInput)) {
            $oldRecord = $this->customerRepository->getCustomerByUuid($attendeeInput['uuid']);
        }
        if ($oldRecord && $oldRecord->mobileNumber == $attendeeInput['mobileNumber']) {
            // no validation since old record already passed
        } else {
            $rules['mobileNumber'] = 'required|mobilenumber|unique:'.$customerTable.',mobileNumber,NULL,id,merchantId,'.$attendeeInput['merchantId'];
        }

        $validator = $this->validate(
            $attendeeInput,
            $rules,
            [   'mobileNumber.mobilenumber' => 'Invalid mobile number.']
        );

        if ($validator->fails()) {
            return $validator;
        }
        $customer = $this->customerRepository->storeAttendee($attendeeInput);

        if ($attendeeInput['attendee_status']) {
            Event::fire(new ConfirmAttendanceEvent($attendeeInput['merchantId'], $customer));
        }

        return true;
    }

    public function deleteAttendee(array $input)
    {
        $attendeeInput = $input['attendee'];
        $attendeeInput['merchantId'] = $attendeeInput['merchant_id'];

        $validator = $this->validate(
            $attendeeInput,
            [   'uuid' => 'required|exists:'.$this->customerRepository->getTable().',uuid,merchantId,'.$attendeeInput['merchantId']],
            [   'uuid.required' => 'Attendee not found!',
                'uuid.exists' => 'Attendee not found!']
        );

        if ($validator->fails()) {
            return $validator;
        }
        $this->customerRepository->deleteCustomer($attendeeInput['uuid']);

        return true;
    }

    public function confirmAttendee(array $input)
    {
        $attendeeInput = $input['attendee'];
        $attendeeInput['merchantId'] = $attendeeInput['merchant_id'];

        $validator = $this->validate(
            $attendeeInput,
            [   'uuid' => 'required|exists:'.$this->customerRepository->getTable().',uuid,merchantId,'.$attendeeInput['merchantId']],
            [   'uuid.required' => 'Attendee not found!',
                'uuid.exists' => 'Attendee not found!']
        );

        if ($validator->fails()) {
            return $validator;
        }
        $customer = $this->customerRepository->confirmAttendee($attendeeInput['uuid']);

        Event::fire(new ConfirmAttendanceEvent($attendeeInput['merchantId'], $customer));

        return true;
    }

    public function uploadAttendee(array $input)
    {
        $attendees = [];
        $attendeeInput = $input['attendee'];
        $validator = $this->validate(
            $attendeeInput,
            [   'attendeesData' => 'required|mimetypes:application/octet-stream,application/zip,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
            [   'required' => 'The :attribute is required.']
        );
        if ($validator->fails()) {
            return $validator;
        }

        $originalFilename = '';
        if (Input::hasFile('attendeesData')) {
            $attendees = $this->readAttendeesData(Input::file('attendeesData'));
        }

        try {
            foreach($attendees AS $attendee) {
                $attendeeRow = [];
                $attendeeRecord = $this->customerRepository->getCustomerByMobile($attendee[2], $attendeeInput['merchant_id']);

                if ($attendeeRecord) {
                    $attendeeRow['uuid'] = $attendeeRecord->uuid;
                }
                $attendeeRow['merchant_id'] = $attendeeInput['merchant_id'];
                $attendeeRow['timestamp'] = $attendee[0];
                $attendeeRow['fullName'] = $attendee[1];
                $attendeeRow['mobileNumber'] = $attendee[2];
                $attendeeRow['email'] = $attendee[3];

                $this->storeAttendee(['attendee' => $attendeeRow]);
            }
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    private function validate($input, $rules, $messages = [])
    {
        return \Validator::make($input, $rules, $messages);
    }

    public function readAttendeesData($attendeesDataFilePath)
    {
        $rowData = false;
        $attendees = [];
        $reader = ReaderFactory::create(Type::XLSX);
        $reader->open($attendeesDataFilePath);

        foreach ($reader->getSheetIterator() as $sheet) {
            // only read data from "Attendees" sheet
            if ($sheet->getName() === 'Attendees') {
                foreach ($sheet->getRowIterator() as $row) {
                    if ($rowData) {
                        if ($row[0] === "") {
                            break;
                        }
                        // hndle exceptions
                        // invalid content
                        // not consistent xsls values etc
                        $attendees[] = $row;
                    }
                    $rowData = ($row[0] == 'Date Registered' || $rowData);
                }
                break;
            }
        }
        $reader->close();

        return $attendees;
    }

    /**
     * @param \Rush\Modules\Customer\Models\CustomerModel|int $customer_id
     * @param int $customer_class_package_id
     * @return bool|\Carbon\Carbon
     */
    public function getClassPackageExpiration($customer_id, $customer_class_package_id)
    {
        $customer = $this->getById($customer_id);

        if (!$customer)
            return false;

        return $this->customerRepository->getClassPackageExpiration($customer, $customer_class_package_id);
    }

    /**
     * Get Class Package By ID
     *
     * @param \Rush\Modules\Customer\Models\CustomerModel|int $customer_id
     * @param int $class_package_id
     * @return \Rush\Modules\Customer\Models\CustomerClassPackageModel|null
     */
    public function getClassPackageById($customer_id, $class_package_id)
    {
        $customer = $this->getById($customer_id);
        if (!$customer) {
            return false;
        }

        return $this->customerRepository->getClassPackageById($customer, $class_package_id);
    }

    /**
     * Get Class Package By Package ID
     *
     * @param \Rush\Modules\Customer\Models\CustomerModel|int $customer_id
     * @param int                                             $package_id
     * @return \Rush\Modules\Customer\Models\CustomerClassPackageModel|null
     */
    public function getClassPackageByPackageId($customer_id, $package_id)
    {
        $customer = $this->getById($customer_id);
        if (!$customer) {
            return false;
        }

        return $this->customerRepository->getClassPackageByPackageId($customer, $package_id);
    }

    /**
     * @param \Rush\Modules\Customer\Models\CustomerModel|int                               $customer_id
     * @param \Carbon\Carbon                                                                $start_date
     * @return null|\Rush\Modules\Customer\Models\CustomerClassPackageModel[]\Illuminate\Database\Eloquent\Collection
     */
    public function getClassPackagesByStartDate($customer_id, $start_date)
    {
        $customer = $this->getById($customer_id);
        if (!$customer) {
            return false;
        }

        return $this->customerRepository->getClassPackagesByStartDate($customer, $start_date);
    }

    /**
     * @param \Rush\Modules\Customer\Models\CustomerModel|int                               $customer_id
     * @param \Carbon\Carbon                                                                $end_date
     * @return null|\Rush\Modules\Customer\Models\CustomerClassPackageModel[]\Illuminate\Database\Eloquent\Collection
     */
    public function getClassPackagesByEndDate($customer_id, $end_date)
    {
        $customer = $this->getById($customer_id);
        if (!$customer) {
            return false;
        }

        return $this->customerRepository->getClassPackagesByEndDate($customer, $end_date);
    }

    /**
     * @param \Rush\Modules\Customer\Models\CustomerModel|int $customer_id
     * @param int $customer_class_package_id
     * @return bool|\Carbon\Carbon
     */
    public function getClassPackageRedemptionExpiration($customer_id, $customer_class_package_id)
    {
        $customer = $this->find($customer_id);

        if (!$customer)
            return false;

        return $this->customerRepository->getClassPackageRedemptionExpiration($customer, $customer_class_package_id);
    }

    public function getValidClassPackages($customer_id)
    {
        $customer = $this->find($customer_id);

        if (!$customer)
            return false;

        return $this->customerRepository->getValidClassPackages($customer);
    }

    public function addClassPackagePunchcardStamp($customer_id, $class_package_punchcard_id, $employee_id = null, $remarks = null)
    {
        if ($customer_id instanceof \Rush\Modules\Customer\Models\CustomerModel) {
            $customer = $customer_id;
        } else {
            $customer = $this->getById($customer_id);
        }

        if ($class_package_punchcard_id instanceof \Rush\Modules\Customer\Models\CustomerClassPackagePunchcardModel) {
            $class_package_punchcard = $class_package_punchcard_id;
        } else {
            $class_package_punchcard = $this->getClassPackagePunchcard($class_package_punchcard_id);
        }

        if (!$customer || !$class_package_punchcard) {
            return false;
        }

        return $this->customerRepository->addClassPackagePunchcardStamp($customer, $class_package_punchcard, $employee_id, $remarks);
    }

    /**
     * Redeem Class Package Punchcard Stamps
     *
     * @param \Rush\Modules\Customer\Models\CustomerModel|int $customer_id
     * @param \Rush\Modules\ClassManagement\Models\PackageStampModel $reward
     * @param null|int $employee_id
     * @param string $channel
     * @return \Rush\Modules\Customer\Models\CustomerPunchCardTransactionsModel
     */
    public function redeemClassPackagePunchcardStamps($customer_id, $reward, $employee_id = null, $channel = 'merchantapp')
    {
        $customer = $this->getById($customer_id);

        return $this->customerRepository->redeemClassPackagePunchcardStamps($customer, $reward, $employee_id, $channel);
    }

    /**
     * @param \Rush\Modules\Customer\Models\CustomerModel|int $customer_id
     * @param string $transaction_reference
     * @return bool|null|\Rush\Modules\Customer\Models\CustomerPunchcardTransactionsModel
     */
    public function voidClassPackagePunchcardStamps($customer_id, $transaction_reference)
    {
        $customer = $this->getById($customer_id);
        if (!$customer) {
            return false;
        }

        return $this->customerRepository->voidClassPackagePunchcardStamps($customer, $transaction_reference);
    }

    /**
     * @param \Rush\Modules\Customer\Models\CustomerModel|int $customer_id
     * @param array $data
     * @param int|null $employee_id
     * @return bool|null|\Rush\Modules\Customer\Models\CustomerPunchcardTransactionsModel
     */
    public function addClassPackageToCustomer($customer_id, array $data, $employee_id = null)
    {
        $customer = $this->getById($customer_id);
        if (!$customer) {
            return false;
        }

        return $this->customerRepository->addClassPackageToCustomer($customer, $data, $employee_id);
    }

    public function voidPunchcardTransaction($reference_no, $merchant_id){
        $transaction = $this->customerPunchcardTransactionRepository->getByReferenceNo($reference_no);
        if($transaction == null){
            $transaction = $this->customerPunchcardTransactionRepository->getPurchasePunchcardByOrNo($reference_no, $merchant_id);
        }

        if($transaction){
            $transaction->void();
            $this->customerLoyaltySmsService->voidPackagePunchcard($transaction);
        }

        return $transaction;
    }

    public function addLoyaltyClassPackageToCustomer($customer_id, array $data, $employee_id = null)
    {
        $customer = $this->getById($customer_id);
        if (!$customer) {
            return false;
        }

        return $this->customerRepository->addLoyaltyClassPackageToCustomer($customer, $data, $employee_id);
    }

    public function renewLoyaltyClassPackage($customer_id, array $data, $employee_id = null)
    {
        $customer = $this->getById($customer_id);
        if (!$customer) {
            return false;
        }

        return $this->customerRepository->renewLoyaltyClassPackage($customer, $data, $employee_id);
    }

    public function generate_activation_code( $type, $customer_id ){

        $code = str_random(6);
        $this->customerRepository->save_activation_code( $type, $customer_id, $code );

        return $code;

    }

    public function deactivate_class_package( $customer ){

        $this->customerRepository->deactivate_class_package( $customer );

    }

    public function get_activation_code( $type, $customer_id ){

        return $this->customerRepository->get_activation_code( $type, $customer_id );

    }

    public function validate_activation_code( $customer_id, $type, $code ){

        $stored_code = $this->customerRepository->get_activation_code( $type, $customer_id );

        if( $stored_code == $code ) return true;

        return false;

    }

    public function disable_activation_code( $customer_id, $type = null, $code = null ){

        $this->customerRepository->disable_activation_code( $customer_id, $type, $code );

    }

    public function send_activation_code($customer, $code){

        $merchant = $customer->merchant;
        $flag = 'generic-activation-code';
        $data['subject_app_name'] = $merchant->settings->app_name;
        $data['activation_code'] = $code;
        $data['name'] = $customer->fullName;

        $this->notification_service->send_notification( $merchant, $customer, $flag, $data );

    }

    public function enable_membership_status( $customer ){

        $this->customerRepository->enable_membership_status( $customer );

    }

    public function update_membership_status($customer, $status){

        $this->customerRepository->update_membership_status( $customer, $status );

    }

    /**
     * @param \Rush\Modules\Customer\Models\CustomerPunchcardTransactionsModel $earn_stamp_transaction
     * @return null|\Rush\Modules\Customer\Models\CustomerPunchcardTransactionsModel
     */
    public function voidClassPackageRedeemedTransactions($earn_stamp_transaction)
    {
        return $this->customerRepository->voidClassPackageRedeemedTransactions($earn_stamp_transaction);
    }

    /**
     * Get Customer Class Packages
     *
     * @param int $customer_id
     * @param array $filter
     * @return mixed|\Rush\Modules\Customer\Models\CustomerClassPackageModel
     */
    public function getClassPackages($customer_id, array $filter = [])
    {
        $customer = $this->getById($customer_id);
        if (!$customer) {
            return false;
        }

        return $this->customerRepository->getClassPackages($customer, $filter);
    }

    public function redeemStampsToCredit(array $data)
    {
        $transaction = $this->customerRepository->redeemStampsToCredit($data);
        if ($transaction) {
            $sms_type = 'redeem-stamp-cms';
            $message = $this->smsNotificationRepository->getMessage($sms_type, $transaction->merchant_id);
            $datetime = Carbon::createFromTimestamp($transaction->transaction_ref);
            $data = [
                'name' => $transaction->customer->name,
                'business_name' => $transaction->customer->merchant->businessName,
                'reward_name' => $transaction->reward->name,
                'redemption_booth' => null,
                'branch' => $transaction->employee ? $transaction->employee->branch->name : null,
                'datetime' => $datetime->format('m-d-Y') .', '. $datetime->format('g:i:s A'),
                'stamps' => $transaction->stamps,
                'stamp_name' => $transaction->customer->merchant->settings->points_name,
                'promo_name' => $transaction->promo_punchcard->promo_title,
                'reference' => $transaction->transaction_ref
            ];
            $sms = new Sms($transaction->customer->merchantId, $this->smsCredentialService);
            $message = $sms->buildMessage($message, $data);
            $result = $sms->send($message, $transaction->customer->mobileNumber, $sms_type);
            $this->smsLogRepository->create([
                'merchant_id' => $transaction->customer->merchantId,
                'message' => $message,
                'mobile_number' => $transaction->customer->mobileNumber,
                'response' => $result,
                'transaction_type' => $sms_type
            ]);
            return $transaction;
        } else {
            return false;
        }
    }

    public function getLoyaltyTransactionByClassPackageId($class_package_id){
        return $this->customerRepository->getLoyaltyTransactionByClassPackageId($class_package_id);
    }

    public function getPunchcardTransactionByClassPackageId($class_package_id){
        return $this->customerRepository->getPunchcardTransactionByClassPackageId($class_package_id);
    }

    public function get_customer_scanned_qr_code_count( $customer_id, $qr_code_id ){
        return $this->customerRepository->get_customer_scanned_qr_code_count( $customer_id, $qr_code_id );
    }

    public function scan_qr_code( $customer_id, $qr_code, $scan_type = null ){
        return $this->customerRepository->scan_qr_code( $customer_id, $qr_code, $scan_type );
     }

    public function issueVoucherCode($customer_id, $receipt_base64, $employee)
    {
        $customer = $this->getById($customer_id);
        if (!$customer) {
            return false;
        }
        $transaction = $this->customerRepository->issueVoucherCode($customer, $receipt_base64, $employee);

        $sms_message = $this->merchantRepository->getSmsNotificationMessages($transaction->employee->merchant->id, 'issue-voucher-code');
        if (!$sms_message) {
            $sms_message = $this->smsNotificationRepository->getDefaultMessage('issue-voucher-code');
        }
        $sms = new SmsHelper(
            $transaction->employee->merchant->sms_credentials->shortcode,
            $transaction->employee->merchant->sms_credentials->app_id,
            $transaction->employee->merchant->sms_credentials->app_secret,
            $transaction->employee->merchant->sms_credentials->passphrase
        );
        if ($sms_message) {
            $datetime = Carbon::now();
            $data = [
                'stamp_name' => $transaction->customer->merchant->settings->points_name,
                'business' => $transaction->customer->merchant->businessName,
                'datetime' => $datetime->format('m-d-Y') .','. $datetime->format('g:i:s A'),
                'reference' => $transaction->transaction_ref,
                'branch' => $transaction->employee->branch->name,
                'voucher_code' => $transaction->voucher_code->code,
                'expiration' => $transaction->voucher_code->expiration->format('Y-m-d')
            ];
            $message = $sms_message->message;
            foreach ($data as $key => $value) {
                $message = str_replace("%". $key ."%", $value, $message);
            }
            $response = $sms->send($transaction->customer->mobile_no, $message);
            SmsLogHelper::log($transaction->customer->mobile_no, $message, $response, $transaction->employee->merchant->id, 'issue-voucher-code');
        }

        return $transaction;
    }

    public function resendVoucherCode($transaction)
    {
        $sms_message = $this->merchantRepository->getSmsNotificationMessages($transaction->employee->merchant->id, 'issue-voucher-code');
        if (!$sms_message) {
            $sms_message = $this->smsNotificationRepository->getDefaultMessage('issue-voucher-code');
        }
        $sms = new SmsHelper(
            $transaction->employee->merchant->sms_credentials->shortcode,
            $transaction->employee->merchant->sms_credentials->app_id,
            $transaction->employee->merchant->sms_credentials->app_secret,
            $transaction->employee->merchant->sms_credentials->passphrase
        );
        if ($sms_message) {
            $datetime = Carbon::now();
            $data = [
                'stamp_name' => $transaction->customer->merchant->settings->points_name,
                'business' => $transaction->customer->merchant->businessName,
                'datetime' => $datetime->format('m-d-Y') .','. $datetime->format('g:i:s A'),
                'reference' => $transaction->transaction_ref,
                'branch' => $transaction->employee->branch->name,
                'voucher_code' => $transaction->voucher_code->code,
                'expiration' => $transaction->voucher_code->expiration->format('Y-m-d')
            ];
            $message = $sms_message->message;
            foreach ($data as $key => $value) {
                $message = str_replace("%". $key ."%", $value, $message);
            }
            $response = $sms->send($transaction->customer->mobile_no, $message);
            SmsLogHelper::log($transaction->customer->mobile_no, $message, $response, $transaction->employee->merchant->id, 'issue-voucher-code');
        }
    }

    public function getCustomerIdsWithBranchPunchcardTransaction($branch_id = 0){
        $customer_ids = $this->customerPunchcardTransactionRepository->getByBranchId($branch_id)->pluck('customer_id')->toArray();

        if(empty($customer_ids)){
            $customer_ids = [0];
        }

        return $customer_ids;
    }

    public function getCustomerIdsWithBranchPointsTransaction($branch_id = 0){
        $customer_ids = $this->customerTransactionRepository->getByBranchId($branch_id)->pluck('customerId')->toArray();

        if(empty($customer_ids)){
            $customer_ids = [0];
        }

        return $customer_ids;
    }

    public function getCustomerByMobile( $mobile_no, $merchant_id = null ) {
        return $this->customerRepository->getCustomerByMobile( $mobile_no, $merchant_id );
    }

    public function generate_order_number( $merchant_id ){

        // get last order number of the merchant.
        $last_order_number = (int) $this->customerRepository->get_last_order_number( $merchant_id );

        // increment the last order number or start with 100 for the first order.
        $order_number = ( $last_order_number == 0 ? 100 : ++$last_order_number );

        // generate 6-digit order number.
        $order_number = substr('000000' . $order_number, -6);

        return $order_number;

    }

    public function place_order($customer, $items, $quantities, $request, $promo_code){

        $order_number = $this->generate_order_number( $customer->merchantId );
        $order_status = 'Placed';
        $total_amount = 0;

        foreach( $items as $item ){
            $total_amount += ((float) $item->amount * (float) $quantities[ $item->uuid ]);
        }

        try{

            $order = \DB::transaction(function() use( $customer, $items, $quantities, $request, $order_number, $order_status, $total_amount, $promo_code ){

                // save order details.
                $order = $this->customerRepository->save_order_details( $customer, $order_number, $order_status, $total_amount, $request, $promo_code );

                // mark promo code as used.
                $this->merchantRepository->set_promo_code_as_used( $promo_code );

                // save order items details.
                $this->customerRepository->save_order_item_details( $customer, $order, $items, $quantities );

                // save order status history.
                $this->customerRepository->save_order_status_history( $order->id, $order_status, 'customer_app', $customer->customerId );

                // save customer delivery details.
                $this->customerRepository->save_customer_delivery_details( $customer, $request );

                // save customer payment details.
                $this->customerRepository->save_customer_payment_details( $customer, $request );
                
                // add customer delivery transaction.
                $this->customerRepository->add_customer_delivery_transaction( $customer, $order_number, $total_amount );

                return $order;

            });

        }catch( \Exception $e ){

            return false;

        }

        return $order;

    }

    public function send_order_placed_notification( $order ){

        $customer = $order->customer;
        $merchant = $customer->merchant;
        $flag = 'order-placed';

        $data['subject_business_name'] = $merchant->businessName;
        $data['subject_order_number'] = $order->order_number;
        $data['logo_url'] = \Config::get('app.url') . $merchant->settings->app_logo;
        $data['customer_name'] = $customer->fullName;
        $data['business_name'] = $merchant->businessName;
        $data['order_number'] = $order->order_number;
        $data['delivery_name'] = $order->delivery_name;
        $data['delivery_mobile_number'] = $order->delivery_mobile_number;
        $data['delivery_building'] = $order->delivery_building;
        $data['delivery_floor'] = $order->delivery_floor;
        $data['delivery_street'] = $order->delivery_street;
        $data['delivery_landmark'] = $order->delivery_landmark;
        $data['payment_change_for'] = $order->payment_change_for;
        $data['payment_notes'] = $order->payment_notes;
        $data['order_items'] = $order->order_items;
        $data['total_amount'] = $order->total_amount;
        $data['promo_code'] = ( ! empty($order->promo_code) ? $order->promo_code : 'None' );
        $data['promo_code_discount_amount'] = $order->promo_code_discount_amount;
        $data['net_amount'] = $order->net_amount;
        $data['business_contact'] = $merchant->contactPersonNumber;

        $this->notification_service->send_notification( $merchant, $customer, $flag, $data );

    }

    public function get_customer_orders( $customer_id ){

        return $this->customerRepository->get_customer_orders( $customer_id );

    }

    public function get_formatted_customer_orders( $raw_orders ){

        $orders = [];

        if( count($raw_orders) ){
            foreach( $raw_orders as $order ){

                $items = [];
                $status_history = [];

                foreach( $order->order_items as $order_item ){

                    $items[] = [
                        'name' => (string) $order_item->item->name,
                        'amount' => (float) $order_item->amount,
                        'quantity' => (int) $order_item->quantity,
                    ];

                }

                foreach( $order->status_history as $status ){

                    $status_history[] = [
                        'status' => (string) $status->status,
                        'date' => (string) $status->created_at->format('Y-m-d H:i:s'),
                    ];

                }

                $orders[] = [
                    'order_number' => (string) $order->order_number,
                    'status' => (string) $order->status,
                    'total_amount' => (float) $order->total_amount,
                    'promo_code' => (string) $order->promo_code,
                    'promo_code_discount_amount' => (float) $order->promo_code_discount_amount,
                    'net_amount' => (float) $order->net_amount,
                    'date' => $order->created_at->format('Y-m-d H:i:s'),
                    'items' => $items,
                    'status_history' => $status_history,
                ];

            }
        }

        return $orders;
    }

    public function updateProfilePhoto($customer_id, $file)
    {
        $customer = $this->getById($customer_id);
        if (!$customer) {
            return false;
        } 
        
        return $this->customerRepository->updateProfilePhoto($customer, $file);
    }

    public function getCustomerByEmail( $email, $merchant_id = null ) {
        return $this->customerRepository->getCustomerByEmail( $email, $merchant_id );
    }

    public function earnPointsSeeding( $employee, $customer, $data, $channel )
    {
        return $this->customerRepository->earnPointsSeeding( $employee, $customer, $data, $channel );
    }

    public function addVote(array $data)
    {
        return $this->customerRepository->addVote($data);
    }

    public function get_customer_reward_redemption_count( $customer_id, $reward_id, $reward_type = 1 ){

        return $this->customerRepository->get_customer_reward_redemption_count( $customer_id, $reward_id, $reward_type );

    }

    public function submitQuiz( $merchant_id, $customer_id, $quiz_answers )
    {
        $quiz_answer_data = $this->composeQuizAnswerData( $merchant_id, $customer_id, $quiz_answers );
        $this->customerRepository->saveQuizAnswerData( $quiz_answer_data );
        $personality = $this->customerRepository->getCustomerPersonality( $merchant_id, $customer_id );
        $personality = $this->format_personality( $personality );

        return $personality;
    }

    public function composeQuizAnswerData( $merchant_id, $customer_id, $quiz_answers )
    {
        $quiz_answer_data = [];

        foreach( $quiz_answers as $answer ){
            $quiz_answer_data[] = [
                'merchant_id' => $merchant_id,
                'customer_id' => $customer_id,
                'question_id' => $answer->question_id,
                'answer_text' => $answer->answer_text,
            ];
        }

        return $quiz_answer_data;
    }

    public function format_personality( $personality )
    {
        $formatted_personality['personality'] = '';
        $formatted_personality['description'] = '';
        $formatted_personality['base_64_image'] = '';

        if( $personality ){

            $formatted_personality['personality'] = (string) $personality->name;
            $formatted_personality['description'] = (string) $personality->description;
            $formatted_personality['base_64_image'] = (string) $personality->base_64_image;

        }

        return $formatted_personality;
    }

    public function validateExistingPnpCardNumber( $card_number, $birthdate )
    {
        // reformat birthdate to yyyy-mm-dd.
        $formatted_birthdate = new Carbon($birthdate);
        $formatted_birthdate = $formatted_birthdate->toDateString();

        return $this->customerRepository->validateExistingPnpCardNumber( $card_number, $formatted_birthdate );
    }

    public function validateNewPnpCardNumber( $card_number )
    {
        return $this->customerRepository->validateNewPnpCardNumber( $card_number );
    }

    public function flagRegisterdPnpCardNumber( $card_number )
    {
        return $this->customerRepository->flagRegisterdPnpCardNumber( $card_number );
    }

    public function updateCustomerDataFromPnpData( $rush_customer, $pnp_customer_data )
    {
        // update customer level (points_level_id, enrollment_date).
        $this->updateCustomerLevelFromPnpData( $rush_customer, $pnp_customer_data );

        // update customer points.
        $this->updateCustomerPointsFromPnpData( $rush_customer, $pnp_customer_data );

        // add customer migrated transaction.
        $this->addMigratedCustomerTransactionsFromPnpData( $rush_customer, $pnp_customer_data );
    }

    private function updateCustomerLevelFromPnpData( $rush_customer, $pnp_customer_data )
    {
        $points_level_id = $pnp_customer_data->points_level_id;
        $pnp_enrollment_date = $pnp_customer_data->enrollment_date;

        $this->customerRepository->updateCustomerLevelFromPnpData( $rush_customer, $points_level_id, $pnp_enrollment_date );
    }

    private function updateCustomerPointsFromPnpData( $rush_customer, $pnp_customer_data )
    {
        $this->customerRepository->updateCustomerPointsFromPnpData( $rush_customer, $pnp_customer_data );
    }

    private function addMigratedCustomerTransactionsFromPnpData( $rush_customer, $pnp_customer_data )
    {
        $this->customerRepository->addMigratedCustomerTransactionsFromPnpData( $rush_customer, $pnp_customer_data );
    }

    public function syncCustomerDataFromPnp( $customer, $promos = [] )
    {
        \DB::transaction(function() use($customer, $promos){

            $card_number = $customer->firstName;
            $last_pnp_sync_datetime = $customer->last_pnp_sync_datetime;


            // reprocess customer's promo-eligible transactions from plains db that are not yet synced.
            // that means new transactions will be inserted to pnp db instead of modifying the existing ones.
            if( count($promos) ){
                foreach( $promos as $promo ){
                    $this->reprocessCustomerPnpTransactions( $customer, $promo );
                }
            }


            // update customer rush points from pnp points.
            $pnp_points = (float) $this->customerRepository->getCustomerPnpPoints( $card_number );
            $this->customerRepository->updateCustomerPointsFromPnpPoints( $customer, $pnp_points );

            // get customer pnp transactions that are not yet synced.
            $pnp_unsynced_transactions = $this->customerRepository->getCustomerPnpUnsyncedTransactions( $card_number, $last_pnp_sync_datetime );
            
            // update customer rush transactions from unsynced pnp transactions.
            if( count($pnp_unsynced_transactions) ){
                $this->customerRepository->addCustomerTransactionsFromPnpUnsyncedTransactions( $pnp_unsynced_transactions );
            }

            // update customer's last sync datetime.
            $this->customerRepository->updateCustomerLastPnpSyncDatetime( $customer );

        });
    }

    public function updatePnpDataFromRegistration( $card_number, $request )
    {
        return $this->customerRepository->updatePnpDataFromRegistration( $card_number, $request );
    }

    private function reprocessCustomerPnpTransactions( $customer, $promo )
    {
        if( $promo->code == 'birthday_double_points' ) $this->reprocessCustomerBirthdayDoublePointsPnpTransactions( $customer );
    }

    private function reprocessCustomerBirthdayDoublePointsPnpTransactions( $customer )
    {
        $card_number = $customer->firstName;
        $last_pnp_sync_datetime = $customer->last_pnp_sync_datetime;
        $start_date = $customer->birthDate;
        $end_date = $customer->birthDate;


        // get customer pnp transactions dated to his birthday that are not yet synced.
        $birthday_transactions = $this->customerRepository->getCustomerPnpTransactionsByDateRange( $card_number, $start_date, $end_date, $last_pnp_sync_datetime );

        if( $birthday_transactions ){

            // insert promo transactions based on customer's promo-eligible transactions.
            $this->customerRepository->insertCustomerPnpPromoTransactions( $card_number, $birthday_transactions );

            // get the points of the customer's promo-eligible transactions.
            $promo_points = (float) $this->getCustomerPnpPromoPointsFromTransactions( $birthday_transactions );

            // get customer current pnp points.
            $pnp_points = (float) $this->customerRepository->getCustomerPnpPoints( $card_number );

            // update customer points in plains and prints db with new points.
            $new_points = $pnp_points + $promo_points;
            $this->customerRepository->updateCustomerPnpPoints( $card_number, $new_points );

        }

    }

    private function getCustomerPnpPromoPointsFromTransactions( $birthday_transactions )
    {
        return 0;
    }

    public function updateCustomerPnpPoints( $card_number, $new_points )
    {
        $this->customerRepository->updateCustomerPnpPoints( $card_number, $new_points );
    }

    public function getLastPunchcardTransaction( $customer_id )
    {
        return $this->customerPunchcardTransactionRepository->getLastTransaction( $customer_id );
    }
}
