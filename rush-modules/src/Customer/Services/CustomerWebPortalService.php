<?php

namespace Rush\Modules\Customer\Services;

use Rush\Modules\Merchant\Models\MerchantModel;
use Ixudra\Curl\CurlService;

class CustomerWebPortalService
{
    public $merchantModel;
    public $curlService;
    public $token;
    public $apiRoutes;

    public function __construct(MerchantModel $merchantModel, $customer = null)
    {
        $this->merchantModel = $merchantModel;
        $this->curlService = new CurlService();
        $this->setApiRoutes($customer);
        $this->setToken();
    }

    public function setApiRoutes($customer = null)
    {
        $this->apiRoutes = [
            'token' => getenv('BASE_URL') . '/api/dev/token',
            'login' => getenv('BASE_URL') . '/api/dev/loyalty/customerapp/customer/login',
            'register' => getenv('BASE_URL') . '/api/dev/loyalty/customerapp/customer/register',
            'forgot_pin' => getenv('BASE_URL') . '/api/dev/loyalty/customerapp/customer/forgot_pin',
            'catalogue' => getenv('BASE_URL') . '/api/dev/loyalty/customerapp/merchant/catalogue',
            'branches' => getenv('BASE_URL') . '/api/dev/loyalty/customerapp/merchant/branches'
        ];
        if ($customer) {
            $this->apiRoutes['details'] = getenv('BASE_URL') . '/api/dev/loyalty/customerapp/customer/' . $customer->id;
            $this->apiRoutes['request_otp'] = getenv('BASE_URL') . '/api/dev/loyalty/customerapp/customer/' . $customer->id . '/verify/mobile';
            $this->apiRoutes['confirm_otp'] = getenv('BASE_URL') . '/api/dev/loyalty/customerapp/customer/' . $customer->id . '/confirm/mobile';
            $this->apiRoutes['points'] = getenv('BASE_URL') . '/api/dev/loyalty/customerapp/customer/' . $customer->id . '/points';
            $this->apiRoutes['rewards'] = getenv('BASE_URL') . '/api/dev/loyalty/customerapp/customer/' . $customer->id . '/rewards?filter=0';
            $this->apiRoutes['redeem'] = getenv('BASE_URL') . '/api/dev/loyalty/customerapp/customer/' . $customer->id . '/redeem';
            $this->apiRoutes['transactions'] = getenv('BASE_URL') . '/api/dev/loyalty/customerapp/customer/' . $customer->id . '/transactions';
            $this->apiRoutes['profile.update'] = getenv('BASE_URL') . '/api/dev/loyalty/customerapp/customer/' . $customer->id . '/profile/update/general';
            $this->apiRoutes['profile.photo'] = getenv('BASE_URL') . '/api/dev/loyalty/customerapp/customer/' . $customer->id . '/profile/update/photo';
            $this->apiRoutes['profile.pin'] = getenv('BASE_URL') . '/api/dev/loyalty/customerapp/customer/' . $customer->id . '/profile/update/pin';
            $this->apiRoutes['profile.email'] = getenv('BASE_URL') . '/api/dev/loyalty/customerapp/customer/' . $customer->id . '/profile/update/email';
            $this->apiRoutes['profile.mobile'] = getenv('BASE_URL') . '/api/dev/loyalty/customerapp/customer/' . $customer->id . '/profile/update/mobile';
            $this->apiRoutes['report'] = getenv('BASE_URL') . '/api/dev/loyalty/customerapp/customer/' . $customer->id . '/report';
            $this->apiRoutes['points_transfer_settings'] = getenv('BASE_URL') . '/api/dev/loyalty/customerapp/merchant/points_transfer';
            $this->apiRoutes['transfer_points'] = getenv('BASE_URL') . '/api/dev/loyalty/customerapp/customer/' . $customer->id . '/points/transfer';
            $this->apiRoutes['achievements.facebook_page_url'] = getenv('BASE_URL') . '/api/dev/loyalty/customerapp/customer/' . $customer->id . '/achievement/fb_page_url';
            $this->apiRoutes['achievements.referral_code'] = getenv('BASE_URL') . '/api/dev/loyalty/customerapp/customer/' . $customer->id . '/achievement/referral_code';
            $this->apiRoutes['achievements.invite'] = getenv('BASE_URL') . '/api/dev/loyalty/customerapp/customer/' . $customer->id . '/invite';
            $this->apiRoutes['membership_card'] = getenv('BASE_URL') . '/api/dev/loyalty/customerapp/customer/' . $customer->id . '/membership_card';
        }
    }

    public function generateToken()
    {
        // Get Merchant App API credential
        $credential = $this->merchantModel->user->apps()->where('api_route_id', 2)->first();
        if ($credential) {
            $api_request = $this->curlService->to($this->apiRoutes['token'])
                ->withData(['app_key' => $credential->key, 'app_secret' => $credential->secret])
                ->returnResponseObject()
                ->post();

            if ($api_request->status == 200) {
                return json_decode($api_request->content)->token;
            }

            return false;
        }

        return false;
    }

    public function setToken()
    {
        $this->token = $this->generateToken();
    }


    public function login($params)
    {
        $params['mobile_no'] = '0' . $params['mobile_no'];
        $api_request = $this->curlService->to($this->apiRoutes['login'])
            ->withHeader('Authorization: Bearer ' . $this->token)
            ->returnResponseObject()
            ->withData($params)
            ->post();
        if ($api_request->status === 400) {
            $this->setToken();
            $this->login($params);
        }

        $api_result = json_decode($api_request->content);

        if ($api_result->error_code == '0x0') {
            $this->setApiRoutes($api_result->data);
        }

        return $api_result;
    }

    public function requestOtp()
    {
        $api_request = $this->curlService->to($this->apiRoutes['request_otp'])
            ->withHeader('Authorization: Bearer ' . $this->token)
            ->returnResponseObject()
            ->get();

        if ($api_request->status === 400) {
            $this->setToken();
            $this->requestOtp();
        }

        $api_result = json_decode($api_request->content);

        return $api_result;
    }


    public function confirmOtp($params)
    {
        $api_request = $this->curlService->to($this->apiRoutes['confirm_otp'])
            ->withHeader('Authorization: Bearer ' . $this->token)
            ->returnResponseObject()
            ->withData($params)
            ->post();

        if ($api_request->status === 400) {
            $this->setToken();
            $this->confirmOtp($params);
        }

        $api_result = json_decode($api_request->content);

        return $api_result;
    }

    public function getPoints()
    {
        $api_request = $this->curlService->to($this->apiRoutes['points'])
            ->withHeader('Authorization: Bearer ' . $this->token)
            ->returnResponseObject()
            ->get();

        if ($api_request->status === 400) {
            $this->setToken();
            $this->getPoints();
        }

        $api_result = json_decode($api_request->content);

        return $api_result;
    }

    public function getRewards()
    {
        $api_request = $this->curlService->to($this->apiRoutes['rewards'])
            ->withHeader('Authorization: Bearer ' . $this->token)
            ->returnResponseObject()
            ->get();

        if ($api_request->status === 400) {
            $this->setToken();
            $this->getRewards();
        }

        $api_result = json_decode($api_request->content);

        return $api_result;
    }

    public function getCatalogue()
    {
        $api_request = $this->curlService->to($this->apiRoutes['catalogue'])
            ->withHeader('Authorization: Bearer ' . $this->token)
            ->returnResponseObject()
            ->get();

        if ($api_request->status === 400) {
            $this->setToken();
            $this->getCatalogue();
        }

        $api_result = json_decode($api_request->content);

        return $api_result;
    }

    public function getBranches()
    {
        $api_request = $this->curlService->to($this->apiRoutes['branches'])
            ->withHeader('Authorization: Bearer ' . $this->token)
            ->returnResponseObject()
            ->get();

        if ($api_request->status === 400) {
            $this->setToken();
            $this->getBranches();
        }

        $api_result = json_decode($api_request->content);

        return $api_result;
    }

    public function getTransactions()
    {
        $api_request = $this->curlService->to($this->apiRoutes['transactions'])
            ->withHeader('Authorization: Bearer ' . $this->token)
            ->returnResponseObject()
            ->get();

        if ($api_request->status === 400) {
            $this->setToken();
            $this->getTransactions();
        }

        $api_result = json_decode($api_request->content);

        return $api_result;
    }

    public function updateProfile($params)
    {
        $api_request = $this->curlService->to($this->apiRoutes['profile.update'])
            ->withHeader('Authorization: Bearer ' . $this->token)
            ->returnResponseObject()
            ->withData($params)
            ->post();

        if ($api_request->status === 400) {
            $this->setToken();
            $this->updateProfile($params);
        }

        $api_result = json_decode($api_request->content);

        return $api_result;
    }

    public function uploadPhoto($params)
    {
        $api_request = $this->curlService->to($this->apiRoutes['profile.photo'])
            ->withHeader('Authorization: Bearer ' . $this->token)
            ->returnResponseObject()
            ->withData($params)
            ->post();

        if ($api_request->status === 400) {
            $this->setToken();
            $this->uploadPhoto($params);
        }

        $api_result = json_decode($api_request->content);

        return $api_result;
    }

    public function updatePin($params)
    {
        $api_request = $this->curlService->to($this->apiRoutes['profile.pin'])
            ->withHeader('Authorization: Bearer ' . $this->token)
            ->returnResponseObject()
            ->withData($params)
            ->post();

        if ($api_request->status === 400) {
            $this->setToken();
            $this->updatePin($params);
        }

        $api_result = json_decode($api_request->content);

        return $api_result;
    }

    public function updateEmail($params)
    {
        $api_request = $this->curlService->to($this->apiRoutes['profile.email'])
            ->withHeader('Authorization: Bearer ' . $this->token)
            ->returnResponseObject()
            ->withData($params)
            ->post();

        if ($api_request->status === 400) {
            $this->setToken();
            $this->updateEmail($params);
        }

        $api_result = json_decode($api_request->content);

        return $api_result;
    }

    public function updateMobile($params)
    {
        $params['mobile_no'] = '0' . $params['mobile_no'];
        $api_request = $this->curlService->to($this->apiRoutes['profile.mobile'])
            ->withHeader('Authorization: Bearer ' . $this->token)
            ->returnResponseObject()
            ->withData($params)
            ->post();

        if ($api_request->status === 400) {
            $this->setToken();
            $this->updateMobile($params);
        }

        $api_result = json_decode($api_request->content);

        return $api_result;
    }

    public function sendPin($params)
    {
        $params['mobile_no'] = '0' . $params['mobile_no'];
        $api_request = $this->curlService->to($this->apiRoutes['forgot_pin'])
            ->withHeader('Authorization: Bearer ' . $this->token)
            ->returnResponseObject()
            ->withData($params)
            ->post();

        if ($api_request->status === 400) {
            $this->setToken();
            $this->updateMobile($params);
        }

        $api_result = json_decode($api_request->content);

        return $api_result;
    }

    public function redeem($params)
    {
        $api_request = $this->curlService->to($this->apiRoutes['redeem'])
            ->withHeader('Authorization: Bearer ' . $this->token)
            ->withHeader('X-App: Customer Web')
            ->returnResponseObject()
            ->withData($params)
            ->post();

        if ($api_request->status === 400) {
            $this->setToken();
            $this->redeem($params);
        }

        $api_result = json_decode($api_request->content);

        return $api_result;
    }

    public function register($params)
    {
        $params['mobile_no'] = '0' . $params['mobile_no'];
        $api_request = $this->curlService->to($this->apiRoutes['register'])
            ->withHeader('Authorization: Bearer ' . $this->token)
            ->returnResponseObject()
            ->withHeader('X-App: Customer Web')
            ->withData($params)
            ->post();

        if ($api_request->status === 400) {
            $this->setToken();
            $this->register($params);
        }

        $api_result = json_decode($api_request->content);

        return $api_result;
    }

    public function sendReport($params)
    {
        $api_request = $this->curlService->to($this->apiRoutes['report'])
            ->withHeader('Authorization: Bearer ' . $this->token)
            ->withHeader('X-App: Customer Web')
            ->returnResponseObject()
            ->withData($params)
            ->post();

        if ($api_request->status === 400) {
            $this->setToken();
            $this->sendReport($params);
        }

        $api_result = json_decode($api_request->content);

        return $api_result;
    }

    public function getPointsTransferSettings()
    {
        $api_request = $this->curlService->to($this->apiRoutes['points_transfer_settings'])
            ->withHeader('Authorization: Bearer ' . $this->token)
            ->returnResponseObject()
            ->get();

        if ($api_request->status === 400) {
            $this->setToken();
            $this->getPointsTransferSettings();
        }

        $api_result = json_decode($api_request->content);

        return $api_result;
    }

    public function transferPoints($params)
    {
        $params['mobile_number'] = '0' . $params['mobile_number'];

        $api_request = $this->curlService->to($this->apiRoutes['transfer_points'])
            ->withHeader('Authorization: Bearer ' . $this->token)
            ->returnResponseObject()
            ->withData($params)
            ->post();

        if ($api_request->status === 400) {
            $this->setToken();
            $this->transferPoints($params);
        }

        $api_result = json_decode($api_request->content);

        return $api_result;
    }

    public function getMerchantFacebookPageUrl()
    {
        $api_request = $this->curlService->to($this->apiRoutes['achievements.facebook_page_url'])
            ->withHeader('Authorization: Bearer ' . $this->token)
            ->returnResponseObject()
            ->get();

        if ($api_request->status === 400) {
            $this->setToken();
            $this->getMerchantFacebookPageUrl();
        }

        $api_result = json_decode($api_request->content);

        return $api_result;
    }

    public function getReferralCode()
    {
        $api_request = $this->curlService->to($this->apiRoutes['achievements.referral_code'])
            ->withHeader('Authorization: Bearer ' . $this->token)
            ->returnResponseObject()
            ->get();

        if ($api_request->status === 400) {
            $this->setToken();
            $this->getMerchantFacebookPageUrl();
        }

        $api_result = json_decode($api_request->content);

        return $api_result;
    }

    public function sendInvite($params)
    {
        $api_request = $this->curlService->to($this->apiRoutes['achievements.invite'])
            ->withHeader('Authorization: Bearer ' . $this->token)
            ->returnResponseObject()
            ->withData($params)
            ->post();
            
        if ($api_request->status === 400) {
            $this->setToken();
            $this->sendInvite($params);
        }

        $api_result = json_decode($api_request->content);

        return $api_result;
    }

    public function getDetails()
    {
        $api_request = $this->curlService->to($this->apiRoutes['details'])
            ->withHeader('Authorization: Bearer ' . $this->token)
            ->returnResponseObject()
            ->get();

        if ($api_request->status === 400) {
            $this->setToken();
            $this->getDetails();
        }

        $api_result = json_decode($api_request->content);

        return $api_result;
    }

    public function getMembershipCard()
    {
        $api_request = $this->curlService->to($this->apiRoutes['membership_card'])
            ->withHeader('Authorization: Bearer ' . $this->token)
            ->returnResponseObject()
            ->get();

        if ($api_request->status === 400) {
            $this->setToken();
            $this->getMembershipCard();
        }

        $api_result = json_decode($api_request->content);

        return $api_result;
    }

    public function linkMembershipCard($params)
    {
        $api_request = $this->curlService->to($this->apiRoutes['membership_card'])
            ->withHeader('Authorization: Bearer ' . $this->token)
            ->withData($params)
            ->returnResponseObject()
            ->post();

        if ($api_request->status === 400) {
            $this->setToken();
            $this->linkMembershipCard($params);
        }

        $api_result = json_decode($api_request->content);

        return $api_result;
    }
}
