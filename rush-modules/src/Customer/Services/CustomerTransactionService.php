<?php

namespace Rush\Modules\Customer\Services;

use Rush\Modules\Customer\Repositories\CustomerLoyaltySmsRepository as CustomerLoyaltySmsService;
use Rush\Modules\Customer\Repositories\CustomerTransactionInterface;
use Rush\Modules\Merchant\Services\NotificationService;

/**
* Our CustomerTransactionService, containing all useful methods for business logic around CustomerTransaction
* Service just hands off data it was given by the controller to the appropriate repositories
*/
class CustomerTransactionService
{
    /**
     * @var Rush\Modules\Customer\Repositories\CustomerTransactionRepository
     */
    protected $customerTransactionRepository;
    protected $notification_service;

    /**
     * TODO: CustomerLoyaltySmsRepository is not a real Repository but a Service
     * @var Rush\Modules\Customer\Repositories\CustomerLoyaltySmsRepository
     */
    protected $customerLoyaltySmsService;

    public function __construct(
        CustomerTransactionInterface $customerTransactionRepository,
        CustomerLoyaltySmsService $customerLoyaltySmsService,
        NotificationService $notification_service
    ) {
        $this->customerTransactionRepository = $customerTransactionRepository;
        $this->customerLoyaltySmsService = $customerLoyaltySmsService;
        $this->notification_service = $notification_service;
    }

    public function getByTransactionReferenceCode($transactionReferenceCode)
    {
        return $this->customerTransactionRepository->getByTransactionReferenceCode($transactionReferenceCode);
    }

    public function voidEarnByTransactionReferenceCode($transactionReferenceCode)
    {
        $transaction = $this->getByTransactionReferenceCode($transactionReferenceCode);
        if ($transaction) {
            $transaction->void();
            $this->notification_service->voidEarn( $transaction->customer->merchant, $transaction );
        }

        return $transaction;
    }

    public function voidPointsPackage($receiptReferenceNo){
        $transaction = $this->getByTransactionReferenceCode($receiptReferenceNo);

        if ($transaction) {
            $customer = $transaction->customer;

            $transaction->void();

            if($customer->class_packages->count() <= 0){
                $customer->membership_status = 0;
                $customer->activations_codes()->where('type', 'customer_account')->update(['status' => 0]);

                $customer->update();
            }

            $this->notification_service->voidPackage( $transaction->customer->merchant, $transaction );
        }

        return $transaction;
    }
}
