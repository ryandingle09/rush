<?php

namespace Rush\Modules\Customer\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Rush\Modules\Customer\Events\ConfirmAttendanceEvent;
use Rush\Modules\Merchant\Services\SmsNotificationService;
use Rush\Modules\Sms\Library\Sms;
use Rush\Modules\Sms\Repositories\SmsLogRepository;
use Rush\Modules\Sms\Services\SmsCredentialService;

class ConfirmAttendanceListener implements ShouldQueue
{

    /**
     * @var Rush\Modules\Sms\Repositories\SmsLogRepository
     */
    protected $smsLogRepository;

    protected $smsCredentialService;

    protected $smsNotificationService;

    public function __construct(
        SmsLogRepository $smsLogRepository,
        SmsCredentialService $smsCredentialService,
        SmsNotificationService $smsNotificationService
    ) {
        $this->smsLogRepository = $smsLogRepository;
        $this->smsCredentialService = $smsCredentialService;
        $this->smsNotificationService = $smsNotificationService;
    }

    /**
     * Handle the event.
     *
     * @param  Rush\Modules\Customer\Events\ConfirmAttendanceEvent  $event
     * @return void
     */
    public function handle(ConfirmAttendanceEvent $event)
    {
        try {
            $customer = $event->customer;
            $merchantId = $event->merchantId;
            $transactionType = 'attendance-confirmation';
            $sms = new Sms($merchantId, $this->smsCredentialService);
            $message = $this->smsNotificationService->getMessage($transactionType, $merchantId);
            $message = $sms->buildMessage($message, ['name' => $customer->fullName, 'program' => $customer->merchant->businessName]);
            $result = $sms->send($message, $customer->mobileNumber, $transactionType);

            $this->smsLogRepository->create([
                'merchant_id' => $merchantId,
                'message' => $message,
                'mobile_number' => $customer->mobileNumber,
                'response' => $result,
                'transaction_type' => $transactionType
            ]);
        } catch (Exception $e) {
            throw $e;
        }
    }
}
