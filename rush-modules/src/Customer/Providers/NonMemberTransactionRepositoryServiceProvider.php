<?php

namespace Rush\Modules\Customer\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Customer\Models\NonMemberTransactionModel;
use Rush\Modules\Customer\Repositories\NonMemberTransactionRepository;

/**
* Register our Repository with Laravel
*/
class NonMemberTransactionRepositoryServiceProvider extends ServiceProvider
{
    /**
    * Registers the NonMemberTransactionInterface with Laravels IoC Container
    */
    public function register()
    {
        $this->app->bind(
            'Rush\Modules\Customer\Repositories\NonMemberTransactionInterface',
            function($app) {
                return new NonMemberTransactionRepository(new NonMemberTransactionModel());
            }
        );
    }
}
