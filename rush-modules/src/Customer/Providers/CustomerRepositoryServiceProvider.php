<?php
namespace Rush\Modules\Customer\Providers;

use Event;
use Illuminate\Support\ServiceProvider;
use Rush\Modules\Customer\Repositories\CustomerInterface;
use Rush\Modules\Customer\Repositories\CustomerRepository;

class CustomerRepositoryServiceProvider extends  ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CustomerInterface::class, CustomerRepository::class);

        Event::listen('Rush\Modules\Customer\Events\ConfirmAttendanceEvent', 'Rush\Modules\Customer\Listeners\ConfirmAttendanceListener');
    }
}
