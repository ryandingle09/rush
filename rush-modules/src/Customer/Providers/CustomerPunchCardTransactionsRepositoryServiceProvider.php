<?php

namespace Rush\Modules\Customer\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Customer\Models\CustomerPunchCardTransactionsModel;
use Rush\Modules\Customer\Repositories\CustomerPunchCardTransactionsRepository;
use Rush\Modules\Customer\Repositories\CustomerPunchCardTransactionsInterface;

/**
* Register our Repository with Laravel
*/
class CustomerPunchCardTransactionsRepositoryServiceProvider extends ServiceProvider
{
    /**
    * Registers the CustomerPunchCardTransactionsInterface with Laravels IoC Container
    */
    public function register()
    {
        $this->app->bind(
            CustomerPunchCardTransactionsInterface::class,
            function($app) {
                return new CustomerPunchCardTransactionsRepository(new CustomerPunchCardTransactionsModel());
            }
        );
    }
}
