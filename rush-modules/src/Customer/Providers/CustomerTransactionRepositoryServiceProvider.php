<?php

namespace Rush\Modules\Customer\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Customer\Models\CustomerTransactionsModel;
use Rush\Modules\Customer\Repositories\CustomerTransactionRepository;
use Rush\Modules\Customer\Repositories\CustomerTransactionInterface;

/**
* Register our Repository with Laravel
*/
class CustomerTransactionRepositoryServiceProvider extends ServiceProvider
{
    /**
    * Registers the CustomerTransactionInterface with Laravels IoC Container
    */
    public function register()
    {
        $this->app->bind(
            CustomerTransactionInterface::class,
            function($app) {
                return new CustomerTransactionRepository(new CustomerTransactionsModel());
            }
        );
    }
}
