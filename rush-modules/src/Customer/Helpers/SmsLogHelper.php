<?php
namespace Rush\Modules\Customer\Helpers;

use Rush\Modules\Customer\Models\CustomerSmsLogModel;

class SmsLogHelper {
    
   public function __construct() {}
   
   public static function log($mobile_number, $message, $response, $merchant_id, $transaction_type)
   {
      $log = new CustomerSmsLogModel;
      $log->merchant_id = $merchant_id;
      $log->mobile_number = $mobile_number;
      $log->response = $response;
      $log->message = $message;
      $log->transaction_type = $transaction_type;
      
      $log->save();
   }
    
}