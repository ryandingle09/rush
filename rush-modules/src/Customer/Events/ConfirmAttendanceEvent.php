<?php

namespace Rush\Modules\Customer\Events;

use App\Events\Event;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;
use Rush\Modules\Customer\Models\CustomerModel;

class ConfirmAttendanceEvent extends Event
{
    use SerializesModels;

    public $merchantId = null;

    /**
     * @var Rush\Modules\Customer\Models\CustomerModel
     */
    public $customer;

    /**
     * Create a new event instance.
     * @param Rush\Modules\Customer\Models\CustomerModel $customer
     * @return void
     */
    public function __construct($merchantId, CustomerModel $customer)
    {
        $this->merchantId = $merchantId;
        $this->customer = $customer;
    }
}
