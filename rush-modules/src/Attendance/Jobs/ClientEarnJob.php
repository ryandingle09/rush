<?php

namespace Rush\Modules\Attendance\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Rush\Modules\Attendance\Models\AttendanceModel;
use Rush\Modules\Attendance\Services\ClientStampsEarnService;
use Rush\Modules\Merchant\Models\MerchantEmployeesModel;

class ClientEarnJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * @var Rush\Modules\Attendance\Models\AttendanceModel
     */
    protected $attendance;

    /**
     * @var Rush\Modules\Merchant\Models\MerchantEmployeesModel
     */
    protected $employee;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(AttendanceModel $attendance, MerchantEmployeesModel $employee)
    {
        $this->attendance = $attendance;
        $this->employee = $employee;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ClientStampsEarnService $clientStampsEarnService)
    {
        $clientStampsEarnService->process($this->attendance, $this->employee);
    }
}
