<?php

namespace Rush\Modules\Attendance\Repositories;

use Rush\Modules\Repository\RepositoryInterface;

/**
 * The AttendanceInterface contains unique method signatures related to Attendance object
 */
interface AttendanceInterface extends RepositoryInterface
{
}
