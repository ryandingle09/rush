<?php

namespace Rush\Modules\Attendance\Repositories;

use Rush\Modules\Attendance\Models\AttendanceModel;
use Rush\Modules\Attendance\Repositories\AttendanceInterface;
use Rush\Modules\Repository\AbstractRepository;

/**
 * A repository class would encapsulate each Eloquent Model class
 * and be responsible for CRUD operations on the database.
 */
class AttendanceRepository extends AbstractRepository implements AttendanceInterface
{
    public function __construct(AttendanceModel $attendanceModel)
    {
        parent::__construct($attendanceModel);
    }
}
