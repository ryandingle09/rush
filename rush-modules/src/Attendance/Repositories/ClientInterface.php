<?php

namespace Rush\Modules\Attendance\Repositories;

use Rush\Modules\Repository\RepositoryInterface;

/**
 * The ClientInterface contains unique method signatures related to Client object
 */
interface ClientInterface extends RepositoryInterface
{
    public function getByName($name);
}
