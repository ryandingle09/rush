<?php

namespace Rush\Modules\Attendance\Repositories;

use Rush\Modules\Repository\RepositoryInterface;

/**
 * The ClientAttendanceUploadInterface contains unique method signatures related to ClientAttendanceUpload object
 */
interface ClientAttendanceUploadInterface extends RepositoryInterface
{
    public function storeByBatch($data);

    public function getListByAttendance($attendanceId);
}
