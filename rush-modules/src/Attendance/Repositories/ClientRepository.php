<?php

namespace Rush\Modules\Attendance\Repositories;

use Rush\Modules\Attendance\Models\ClientModel;
use Rush\Modules\Attendance\Repositories\ClientInterface;
use Rush\Modules\Repository\AbstractRepository;

/**
 * A repository class would encapsulate each Eloquent Model class
 * and be responsible for CRUD operations on the database.
 */
class ClientRepository extends AbstractRepository implements ClientInterface
{
    public function __construct(ClientModel $clientModel)
    {
        parent::__construct($clientModel);
    }

    public function getByName($name)
    {
        return $this->model->where('name', $name)->first();
    }
}
