<?php

namespace Rush\Modules\Attendance\Repositories;

use Illuminate\Support\Facades\DB;
use Rush\Modules\Attendance\Helpers\AttendanceHelper;
use Rush\Modules\Attendance\Models\ClientAttendanceUploadModel;
use Rush\Modules\Attendance\Repositories\ClientAttendanceUploadInterface;
use Rush\Modules\Repository\AbstractRepository;

/**
 * A repository class would encapsulate each Eloquent Model class
 * and be responsible for CRUD operations on the database.
 */
class ClientAttendanceUploadRepository extends AbstractRepository implements ClientAttendanceUploadInterface
{
    public function __construct(ClientAttendanceUploadModel $clientAttendanceUploadModel)
    {
        parent::__construct($clientAttendanceUploadModel);
    }

    public function storeByBatch($data)
    {
        // insert per 300 rows
        $batches = array_chunk($data, 300);
        foreach ($batches as $batch) {
            $this->model->insert($batch);
        }
    }

    public function getListByAttendance($attendanceId)
    {
        return $this->model->where(['attendance_id' => $attendanceId])->get();
    }

    /**
     * look for a similar record in processing or processed status
     * @param  array $attributes the attributes that makes a record unique
     * @return mixed
     */
    public function findAlpha($attributes)
    {
        $alpha = $this->model
            ->whereIn('status', [AttendanceHelper::PROCESSING, AttendanceHelper::PROCESSED])
            ->where(array_intersect_key($attributes, AttendanceHelper::getUniqueIndexes()))
            ->first();

        return $alpha;
    }
}
