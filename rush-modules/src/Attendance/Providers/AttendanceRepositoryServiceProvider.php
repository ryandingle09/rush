<?php

namespace Rush\Modules\Attendance\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Attendance\Models\AttendanceModel;
use Rush\Modules\Attendance\Repositories\AttendanceRepository;

/**
* Register our Repository with Laravel
*/
class AttendanceRepositoryServiceProvider extends ServiceProvider
{
    /**
    * Registers the AttendanceInterface with Laravels IoC Container
    */
    public function register()
    {
        $this->app->bind(
            'Rush\Modules\Attendance\Repositories\AttendanceInterface',
            function($app) {
                return new AttendanceRepository(new AttendanceModel());
            }
        );
    }
}
