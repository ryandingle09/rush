<?php

namespace Rush\Modules\Attendance\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Attendance\Models\ClientAttendanceUploadModel;
use Rush\Modules\Attendance\Repositories\ClientAttendanceUploadRepository;

/**
* Register our Repository with Laravel
*/
class ClientAttendanceUploadRepositoryServiceProvider extends ServiceProvider
{
    /**
    * Registers the AttendanceInterface with Laravels IoC Container
    */
    public function register()
    {
        $this->app->bind(
            'Rush\Modules\Attendance\Repositories\ClientAttendanceUploadInterface',
            function($app) {
                return new ClientAttendanceUploadRepository(new ClientAttendanceUploadModel());
            }
        );
    }
}
