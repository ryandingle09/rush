<?php

namespace Rush\Modules\Attendance\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Attendance\Models\ClientModel;
use Rush\Modules\Attendance\Repositories\ClientRepository;

/**
* Register our Repository with Laravel
*/
class ClientRepositoryServiceProvider extends ServiceProvider
{
    /**
    * Registers the ClientInterface with Laravels IoC Container
    */
    public function register()
    {
        $this->app->bind(
            'Rush\Modules\Attendance\Repositories\ClientInterface',
            function($app) {
                return new ClientRepository(new ClientModel());
            }
        );
    }
}
