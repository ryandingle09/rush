<?php

namespace Rush\Modules\Attendance\Services;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Rush\Modules\Attendance\Helpers\AttendanceHelper;
use Rush\Modules\Attendance\Repositories\AttendanceInterface;
use Rush\Modules\Attendance\Services\ClientAttendanceUploadService;

class AttendanceService
{
    protected $attendanceRepository;

    protected $clientAttendanceUploadService;

    public function __construct(
        AttendanceInterface $attendanceRepository,
        ClientAttendanceUploadService $clientAttendanceUploadService
    ) {
        $this->attendanceRepository = $attendanceRepository;
        $this->clientAttendanceUploadService = $clientAttendanceUploadService;
    }

    public function getById($campaignId)
    {
        return $this->attendanceRepository->getById($campaignId);
    }

    public function all()
    {
        return $this->attendanceRepository->all();
    }

    public function store(array $input)
    {
        $attendees = [];
        $attendanceInput = $input['attendance'];
        $validator = $this->validate(
            $attendanceInput,
            [   'attendanceData' => 'required|mimetypes:application/octet-stream,application/zip,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
            [   'required' => 'The :attribute is required.']
        );
        if ($validator->fails()) {
            return $validator;
        }

        $originalFilename = '';
        if (Input::hasFile('attendanceData')) {
            $attendees = $this->clientAttendanceUploadService->readAttendeesData(Input::file('attendanceData'));
            $originalFilename = Input::file('attendanceData')->getClientOriginalName();
            $originalFilename = preg_replace("/[^a-z0-9\.]/", "", strtolower($originalFilename));
        }

        $attendanceInput['name'] = substr($originalFilename, 0, 15);
        $attendanceInput['status'] = AttendanceHelper::INACTIVE;
        $validator = $this->validate(
            $attendanceInput,
            [   'name' => 'required|unique:'.$this->attendanceRepository->getTable().',name,NULL'],
            [   'name.unique' => 'The :attribute has already been registered.']
        );
        if ($validator->fails()) {
            return $validator;
        }

        try {
            $attendance = $this->attendanceRepository->create($attendanceInput);
            $this->clientAttendanceUploadService->storeAttendeesData($attendance->id, $attendees);
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    private function validate($input, $rules, $messages = [])
    {
        return \Validator::make($input, $rules, $messages);
    }
}
