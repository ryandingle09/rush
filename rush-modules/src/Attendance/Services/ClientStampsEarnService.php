<?php

namespace Rush\Modules\Attendance\Services;

use Exception;
use Illuminate\Support\Facades\DB;
use Rush\Citolaravel\Helpers\MigrationHelper;
use Rush\Modules\Attendance\Helpers\AttendanceHelper;
use Rush\Modules\Attendance\Models\AttendanceModel;
use Rush\Modules\Attendance\Services\ClientAttendanceUploadService;
use Rush\Modules\Attendance\Services\ClientService;
use Rush\Modules\Customer\Services\CustomerService;
use Rush\Modules\Helpers\JobHelper;
use Rush\Modules\Merchant\Models\MerchantEmployeesModel;
use Rush\Modules\Merchant\Services\BranchService;
use Rush\Modules\Merchant\Services\EmployeeService;

class ClientStampsEarnService
{
    protected $clientAttendanceUploadService;

    protected $customerService;

    protected $clientService;

    protected $branchService;

    public function __construct(
        ClientAttendanceUploadService $clientAttendanceUploadService,
        CustomerService $customerService,
        ClientService $clientService,
        BranchService $branchService,
        EmployeeService $employeeService
    ) {
        $this->clientAttendanceUploadService = $clientAttendanceUploadService;
        $this->customerService = $customerService;
        $this->clientService = $clientService;
        $this->branchService = $branchService;
        $this->employeeService = $employeeService;
    }

    /**
     * seed the stamps
     * @param  AttendanceModel        $attendance
     * @param  MerchantEmployeesModel $employee
     * @return mixed
     */
    public function process(AttendanceModel $attendance, MerchantEmployeesModel $employee)
    {
        try {
            JobHelper::start($attendance);

            $attendees = $this->clientAttendanceUploadService->getListByAttendance($attendance->id);

            foreach ($attendees as $attendee) {
                if ($attendee->status != AttendanceHelper::INACTIVE) {
                    continue;
                }

                // find a similar attendee with status processing or processed
                if ($this->clientAttendanceUploadService->isDuplicate($attendee->toArray())) {
                    $attendee->status = AttendanceHelper::IGNORED;
                    $attendee->save();
                    continue;
                }

                // find client or create one
                $client = $this->clientService->firstOrNew([
                    'code' => $attendee->client_id,
                    'merchant_id' => $attendance->merchant_id
                ]);
                if (!$client->exists) {
                    $client->save();
                }

                // find branch or create new
                $branch = $this->branchService->firstOrNew([
                    'branchName' => $attendee->visit_location,
                    'merchantId' => $attendance->merchant_id
                ]);
                $branch->enable_earn_stamp = 1;
                $branch->save();

                // find or create a System Branch Employee
                $branchEmployeeUser = 'system_employee_'.$attendance->merchant_id.'_branch_'.$branch->id;
                $branchEmployee = $this->employeeService->firstOrNew([
                    'merchantId' => $attendance->merchant_id,
                    'username' => $branchEmployeeUser,
                    'employeeName' => $branchEmployeeUser,
                    'branchId' => $branch->id
                ]);
                if (!$branchEmployee->exists) {
                    $branchEmployee->save();
                }

                $request = $this->customerService->earnClientStamps($attendee->client_id, $branchEmployee->uuid, $attendance->name);

                if ($request) {
                    $attendee->status = AttendanceHelper::PROCESSED;
                    $attendee->save();
                } else {
                    // fail silently and ignore the attendee since he/she is not registered
                    $attendee->status = AttendanceHelper::IGNORED;
                    $attendee->save();
                }
            }
            $attendance->status = AttendanceHelper::PROCESSED;
            $attendance->save();
        } catch (\Exception $e) {
            JobHelper::reactivate($attendance, $e);
            throw $e;
        }
    }
}
