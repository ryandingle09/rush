<?php

namespace Rush\Modules\Attendance\Services;

use Rush\Modules\Attendance\Repositories\ClientInterface;


class ClientService
{
    /**
     * @var Rush\Modules\Attendance\Repositories\ClientRepository
     */
    protected $clientRepository;

    public function __construct(ClientInterface $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    public function firstOrCreate($attributes)
    {
        return $this->clientRepository->firstOrCreate($attributes);
    }

    public function firstOrNew($attributes)
    {
        return $this->clientRepository->firstOrNew($attributes);
    }
}
