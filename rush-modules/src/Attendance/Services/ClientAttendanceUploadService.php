<?php

namespace Rush\Modules\Attendance\Services;

use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory;
use Rush\Modules\Attendance\Helpers\AttendanceHelper;
use Rush\Modules\Attendance\Repositories\ClientAttendanceUploadInterface;

class ClientAttendanceUploadService
{
    protected $clientAttendanceUploadRepository;

    public function __construct(ClientAttendanceUploadInterface $clientAttendanceUploadRepository)
    {
        $this->clientAttendanceUploadRepository = $clientAttendanceUploadRepository;
    }

    public function getListByAttendance($attendanceId)
    {
        return $this->clientAttendanceUploadRepository->getListByAttendance($attendanceId);
    }

    public function readAttendeesData($attendeesDataFilePath)
    {
        $rowData = false;
        $attendees = [];
        $reader = ReaderFactory::create(Type::XLSX);
        $reader->open($attendeesDataFilePath);

        foreach ($reader->getSheetIterator() as $sheet) {
            // only read data from "summary" sheet
            if ($sheet->getName() === 'Attendance') {
                foreach ($sheet->getRowIterator() as $row) {
                    if ($rowData) {
                        if ($row[0] === "" || !(is_object($row[0]) && get_class($row[0]) == 'DateTime')) {
                            break;
                        }
                        // hndle exceptions
                        // invalid content
                        // not consistent xsls values etc
                        $attendees[] = $row;
                    }
                    $rowData = ($row[0] == 'Date' || $rowData);
                }
                break;
            }
        }

        $reader->close();

        return $attendees;
    }

    public function storeAttendeesData($attendanceId, $data)
    {
        foreach ($data as $key => $value) {
            $data[$key] = [
                'date_time' => date('Y-m-d H:i:s', strtotime($value[0]->format('Y-m-d')." $value[2]")),
                'client_id' => $value[3],
                'client' => $value[4],
                'visit_service_category' => $value[5],
                'visit_type' => $value[6],
                'type' => $value[7],
                'pricing_option' => $value[8],
                'expiration_date' => $value[9],
                'visits_remaining' => $value[10],
                'staff' => $value[11],
                'visit_location' => $value[12],
                'sale_location' => $value[13],
                'staff_paid' => $value[14],
                'late_cancel' => $value[15],
                'no_show' => $value[16],
                'booking_method' => $value[17],
                'payment_service_category' => $value[18],
                'attendance_id' => $attendanceId,
                'status' => AttendanceHelper::INACTIVE,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
        }

        $this->clientAttendanceUploadRepository->storeByBatch($data);
    }

    public function isDuplicate($attributes)
    {
        return (bool) $this->clientAttendanceUploadRepository->findAlpha($attributes);
    }
}