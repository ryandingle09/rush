<?php

namespace Rush\Modules\Attendance\Models;

use Illuminate\Database\Eloquent\Model;

class ClientModel extends Model
{
    protected $table = "client";

    protected $fillable = [
        'code',
        'merchant_id'
    ];
}