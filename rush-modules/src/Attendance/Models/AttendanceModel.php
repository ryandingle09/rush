<?php

namespace Rush\Modules\Attendance\Models;

use Illuminate\Database\Eloquent\Model;

class AttendanceModel extends Model
{
    protected $table = "attendance";

    protected $fillable = [
        'name',
        'status',
        'merchant_id'
    ];
}