<?php

namespace Rush\Modules\Attendance\Models;

use Illuminate\Database\Eloquent\Model;

class ClientAttendanceUploadModel extends Model
{
    protected $table = "client_attendance_upload";

    protected $fillable = [
        'date_time',
        'client_id',
        'client',
        'visit_service_category',
        'visit_type',
        'type',
        'pricing_option',
        'expiration_date',
        'visits_remaining',
        'staff',
        'visit_location',
        'sale_location',
        'staff_paid',
        'late_cancel',
        'no_show',
        'booking_method',
        'payment_service_category',
        'attendance_id',
        'status',
        'version',
    ];
}