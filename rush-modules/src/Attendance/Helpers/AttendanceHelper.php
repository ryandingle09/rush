<?php

namespace Rush\Modules\Attendance\Helpers;

use Rush\Modules\Helpers\AbstractProcessingHelper;

final class AttendanceHelper extends AbstractProcessingHelper
{
    private function __construct(){
        throw new Exception("AttendanceHelper not instantiable");
    }

    const UNIQUE_INDEXES = ['date_time', 'client_id', 'type', 'visit_location'];

    public static function getUniqueIndexes()
    {
        return array_fill_keys(self::UNIQUE_INDEXES, null);
    }
}