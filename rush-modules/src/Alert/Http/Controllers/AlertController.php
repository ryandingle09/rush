<?php

namespace Rush\Modules\Alert\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Event;
use Illuminate\Http\Request;
use Rush\Modules\Alert\Events\ChangeAppstoreLogoEvent;
use Rush\Modules\Alert\Events\ChangePasswordEvent;
use Rush\Modules\Alert\Events\MerchantSignupEvent;
use Rush\Modules\Alert\Events\MerchantWelcomeEvent;
use Rush\Modules\Alert\Events\QuickstartCompletionEvent;
use Rush\Modules\Merchant\Services\MerchantService;

/**
 * This is a temporary route that would act as a bridge allowing
 * Codeigniter to post requests matched by a specific alert
 * TODO: remove this controller once migration to Laravel is finished
 */
class AlertController extends Controller
{
    /**
     * @var Rush\Modules\Merchant\Services\MerchantService
     */
    protected $merchantService = null;

    public function __construct(MerchantService $merchantService)
    {
        $this->merchantService = $merchantService;
    }

    public function changePassword(Request $request)
    {
        $merchantId = $request->input('merchantId');
        $newPassword = $request->input('newPassword');
        $merchant = $this->merchantService->getById($merchantId);
        Event::fire(new ChangePasswordEvent($merchant, $newPassword));
    }

    public function merchantSignup(Request $request)
    {
        $merchantId = $request->input('merchantId');
        $merchant = $this->merchantService->getByUsername($merchantId);
        Event::fire(new MerchantSignupEvent($merchant));
    }

    public function quickstartCompletion(Request $request)
    {
        $merchantId = $request->input('merchantId');
        $merchant = $this->merchantService->getById($merchantId);
        Event::fire(new QuickstartCompletionEvent($merchant));
    }

    public function merchantWelcome(Request $request)
    {
        $registration = $request->all();
        Event::fire(new MerchantWelcomeEvent((object) $registration));
    }

    public function changeAppstoreLogo(Request $request)
    {
        $merchantId = $request->input('merchantId');
        $merchant = $this->merchantService->getById($merchantId);
        Event::fire(new ChangeAppstoreLogoEvent($merchant));
    }
}
