<?php

/**
 * create a hidden route which will receive POST from CodeIgniter
 */

Route::group(
    [   'prefix' => 'alert',
    ],
    function() {
        Route::post('changePassword', 'Rush\Modules\Alert\Http\Controllers\AlertController@changePassword');
        Route::post('merchantSignup', 'Rush\Modules\Alert\Http\Controllers\AlertController@merchantSignup');
        Route::post('quickstartCompletion', 'Rush\Modules\Alert\Http\Controllers\AlertController@quickstartCompletion');
        Route::post('merchantWelcome', 'Rush\Modules\Alert\Http\Controllers\AlertController@merchantWelcome');
        Route::post('changeAppstoreLogo', 'Rush\Modules\Alert\Http\Controllers\AlertController@changeAppstoreLogo');
    }
);