<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width" />
</head>
<body>
	<table style="width: 100%;background-color: #f7f7f7;padding:0;">
		<tr>
			<td style="padding:30px 0;">
				<table id="th-ticket-template" style="margin: 0 auto;
				border-collapse: collapse;
				font-family: 'roboto', 'helvetica neue','helvetica','arial','sans-serif';
				color: #333;
				width: 640px;
				table-layout: fixed;">
				<tr style="background-color: #fff;">
					<td colspan='4' style="padding: 0;">	
						<img src="{{ url('app/assets/email/rush-email-header.jpg') }}" style="width:100%;"/>
					</td>
				</tr>
				<tr style="background-color: #fff;">
					<td colspan='4' style="padding: 24px 24px;font-weight: bold;font-size: 22px;">
						<span style="color:#ff8c00;">A new merchant has signed-up with RUSH Loyalty On Demand!</span>
					</td>
				</tr>
				<tr style="background-color: #fff;">
					<td colspan='4' style="padding: 0px 24px;font-size: 13px;line-height: 24px;">
						<span style="padding-left:40px;"><b>RUSH Package</b><span>: {{$plan}}</span></span><br>
						<span style="padding-left:40px;"><b>Company Name</b><span>: {{$merchant->businessName}}</span></span><br>
						<span style="padding-left:40px;"><b>Industry</b><span>: {{$industry}}</span></span><br>
						<span style="padding-left:40px;"><b>Authorized Company Representative</b><span>: {{$merchant->personName}}</span></span><br>
						<span style="padding-left:40px;"><b>Designation/Position</b><span>: {{$merchant->personPosition}}</span></span><br>
						<span style="padding-left:40px;"><b>Telephone No.</b><span>: {{$merchant->contactPersonNumber}}</span></span><br>
						<span style="padding-left:40px;"><b>Email Address</b><span>: {{$merchant->contactPersonEmail}}</span></span><br>
						<span style="padding-left:40px;"><b>Location</b><span>: {{$merchant->address}}</span></span><br>
						<span style="padding-left:40px;"><b>Customers served per month</b><span>: {{$merchant->averageCustomer}}</span></span><br>
						<span style="padding-left:40px;"><b>Number of Branches</b><span>: {{$merchant->business_branches}}</span></span><br>
						<span style="padding-left:40px;"><b>Loyalty Program Objective</b><span>: {{$merchant->loyaltyProgramReason}}</span></span><br>
					</td>
				</tr>
				<tr style="background-color: #fff;">
					<td colspan='4' style="padding: 12px 24px 32px;">
						<p style="color:gray;font-style: italic;font-size:12px;">This is a system-generated email.</p>
					</td>
				</tr>
				<tr style=""><td colspan="4">&nbsp;</td></tr>
				<tr>
					<td colspan='4' style="text-align: center;">
						<a href="https://www.facebook.com/rushloyalty"><img src="{{ url('app/assets/email/fb-icon.png') }}" style="width:24px"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="{{ MigrationHelper::getServerUrl() }}"><img src="{{ url('app/assets/email/link-icon.png') }}" style="width:24px;"></a>
					</td>
				</tr>
				<tr style=""><td colspan="4">&nbsp;</td></tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>