Hi,

Quick Start has been completed by {{$merchant->businessName}} for {{$loyaltyProgramName}}

Program Details
Program Name: {{$merchantSetting->program_name}}
{{$packageType}} Name {{$merchantSetting->points_name}}

App Details
App Name: {{$merchantSetting->app_name}}
Description: {{$merchantSetting->app_description}}

Cheers,

RUSH Loyalty On Demand

This is a system-generated e-mail. Please do not reply.