<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width" />
</head>
<body>
	<table style="width: 100%;background-color: #f7f7f7;padding:0;">
		<tr>
			<td style="padding:30px 0;">
				<table id="th-ticket-template" style="margin: 0 auto;
				border-collapse: collapse;
				font-family: 'roboto', 'helvetica neue','helvetica','arial','sans-serif';
				color: #333;
				width: 640px;
				table-layout: fixed;">
				<tr style="background-color: #fff;">
					<td colspan='4' style="padding: 0;">	
						<img src="{{ env('APP_URL') . '/app/assets/email/rush-email-header.jpg' }}" style="width:100%;"/>
					</td>
				</tr>
				<tr style="background-color: #fff;">
					<td colspan='4' style="padding: 24px 24px;font-weight: bold;font-size: 22px;">
						<span style="color:#ff8c00;">
							@if ( isset($from_sg) && $from_sg )
								Appointment Request [Globe myBusiness x RUSH]
							@else
								Request A Demo
							@endif
						</span>
					</td>
				</tr>
				<tr style="background-color: #fff;">
					<td colspan='4' style="padding: 0px 24px;font-size: 13px;line-height: 24px;">
						<span>
							@if( isset($from_sg) && $from_sg )
							{{ $email }} has sent us appointment request. Please see the details below:
							@else
							{{ $email }} has sent us a demo request. Please see the details below:
							@endif
						</span><br>
					</td>
				</tr>
				<tr style="background-color: #fff;">
					<td colspan=4 style="padding: 12px 24px;font-size: 13px;line-height: 24px;">
						{{-- @if( !empty($fname) && !empty($lname) ) --}}
						{{-- 	<span style="padding-left:40px;"><b>First Name</b><span>: {{ $fname }}</span></span><br> --}}
						{{-- 	<span style="padding-left:40px;"><b>Last Name</b><span>: {{ $lname }}</span></span><br> --}}
						{{-- @else --}}
						{{-- 	<span style="padding-left:40px;"><b>Name</b><span>: {{ $name }}</span></span><br> --}}
						{{-- @endif --}}
						<span style="padding-left:40px;"><b>Company Name</b><span>: {{ $companyname }}</span></span><br>
						<span style="padding-left:40px;"><b>Mobile Number</b><span>: {{ $mobile }}</span></span><br>
						<span style="padding-left:40px;"><b>Email Address</b><span>: {{ $email }}</span></span><br>
						
						{{-- <span style="padding-left:40px;"><b>Industry:</b></span> --}}
						{{-- <span style="padding-left:40px;"> --}}
						{{-- 	@if ($industry) --}}
						{{-- 		<ul style="padding-left:40px"> --}}
						{{-- 		@foreach( $industry as $item) --}}
						{{-- 			<li>{{ $item }}</li> --}}
						{{-- 		@endforeach --}}
						{{-- 		</ul> --}}
						{{-- 	@endif --}}
						{{-- </span> --}}
						{{-- <span style="padding-left:40px;"><b>Objective:</b></span> --}}
						{{-- <span style="padding-left:40px;"> --}}
						{{-- 	@if ($objective) --}}
						{{-- 		<ul style="padding-left:40px"> --}}
						{{-- 		@foreach( $objective as $item) --}}
						{{-- 			<li>{{ $item }}</li> --}}
						{{-- 		@endforeach --}}
						{{-- 		</ul> --}}
						{{-- 	@endif --}}
						{{-- </span> --}}
						{{-- <span style="padding-left:40px;"><b>Package</b><span>: {{ $package }}</span></span><br> --}}
						{{-- @if( isset($landing_page) && !empty($landing_page) ) --}}
						{{-- 	<span style="padding-left:40px;"><b>URL</b><span>: {{ $landing_page }}</span></span><br> --}}
						{{-- @endif --}}
						{{-- <span style="padding-left:40px;"><b>Message</b><span>: {{ $request_message }}</span></span><br> --}}
					</td>
				</tr>
				<tr style="background-color: #fff;">
					<td colspan=4 style="padding: 12px 24px;font-size: 13px;line-height: 24px;">
						<span style="padding-left:40px;"></span><br>
						<span style="padding-left:40px;"></span><br>
					</td>
				</tr>
				<tr style="background-color: #fff;">
					<td colspan='4' style="padding: 12px 24px 32px;">
						<p style="color:gray;font-style: italic;font-size:12px;">This is a system-generated email.</p>
					</td>
				</tr>
				<tr style=""><td colspan="4">&nbsp;</td></tr>
				<tr>
					<td colspan='4' style="text-align: center;">
						<a href="https://www.facebook.com/rushloyalty"><img src="{{ env('APP_URL') . '/app/assets/email/fb-icon.png' }}" style="width:24px"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="{{ env('APP_URL') }}"><img src="{{ env('APP_URL') . '/app/assets/email/link-icon.png' }}" style="width:24px;"></a>
					</td>
				</tr>
				<tr style=""><td colspan="4">&nbsp;</td></tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>