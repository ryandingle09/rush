<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width" />
</head>
<body style="width: 100% !important; min-width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; color: #222222; font-family: \'Helvetica\', \'Arial\', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 12px; margin: 0; padding: 0;">
    <p>Dear {{ $name }} of {{ $company }}</p>
	<p>Attached is the Statement of Account (SOA) for billing period {{ $billing_period }}.</p>
	<p>To view your billing history and to know more about the other payment options, you may login to your <a href="{{ MigrationHelper::getServerUrl() }}">RUSH account</a></p>
	<p>Should you require further assistance, you may email us at billing@rush.ph</p>
	<p>Thank you.</p>
	<p>RUSH Loyalty made easy</p>
	<p>---</p>
	<p><em>This is a system-generated e-mail. Please do not reply.</em></p>
</body>
</html>