<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width" />
</head>
<body>
	
	<table style="width: 100%;background-color: #f7f7f7;padding:0;">
		<tr>
			<td style="padding:30px 0;">
				<table id="th-ticket-template" style="margin: 0 auto;
				border-collapse: collapse;
				font-family: 'roboto', 'helvetica neue','helvetica','arial','sans-serif';
				color: #333;
				width: 640px;
				table-layout: fixed;">
			<!-- tr>
				<th colspan='4' style="background: #7e4370; /* For browsers that do not support gradients */    
		    background: -webkit-linear-gradient(left, #5D307A ,#7e4370, #b9655d); /* For Safari 5.1 to 6.0 */
		    background: -o-linear-gradient(right, #5D307A ,#7e4370, #b9655d); /* For Opera 11.1 to 12.0 */
		    background: -moz-linear-gradient(right, #5D307A ,#7e4370, #b9655d); /* For Firefox 3.6 to 15 */
		    background: linear-gradient(to right, #5D307A ,#7e4370, #b9655d); /* Standard syntax (must be last) */
		    border-radius: 8px 8px 0 0;">
		    <img src="tickethub-logo-white.png" style="width: 120px;margin: 20px 0;"></th>
		</tr> -->
		<tr style="background-color: #fff;">
			<td colspan='4' style="padding: 0;">	
				<img src="{{ env('APP_URL') . '/app/assets/email/rush-email-header.jpg' }}" style="width:100%;"/>
			</td>
		</tr>
		<tr style="background-color: #fff;">
			<td colspan='4' style="padding: 24px 24px;font-weight: bold;font-size: 22px;">
				<p style="color:#ff8c00;">{{ $user_email_content }}</p>
			</td>
		</tr>
		<tr style="background-color: #fff;">
			<td colspan='4' style="padding: 0px 24px;font-size: 13px;line-height: 24px;">
				@if(isset($from_sg) && $from_sg)
					<span>A member of our team will get in touch with you within 3-5 business day.</span><br>
				@else
					<span>A member of our team will get in touch with you within 1 business day.</span><br>
				@endif
				<span>Get a glimpse of how it works through this link: <a href="http://rush.ph/how-it-works">http://rush.ph/how-it-works</a>.</span><br>
				<span>Learn about the different features of our products:  <a href="http://rush.ph/products">http://rush.ph/products</a>.</span>
			</td>
		</tr>
		<tr style="background-color: #fff;">
			<td colspan='4' style="padding: 12px 24px;font-size: 13px;line-height: 24px;">
				<p>We look forward to helping you build and foster long-lasting customer relationships with a RUSH-powered loyalty program. </p>
			</td>
		</tr>
		<tr style="background-color: #fff;">
			<td colspan='4' style="padding: 12px 24px 32px;">
				<p style="color:gray;font-style: italic;font-size:12px;">This is a system-generated email.</p>
			</td>
		</tr>
		<tr style=""><td colspan="4">&nbsp;</td></tr>
		<tr>
			<td colspan='4' style="text-align: center;">
				<a href="https://www.facebook.com/rushloyalty"><img src="{{ env('APP_URL') . '/app/assets/email/fb-icon.png' }}" style="width:24px"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="{{ env('APP_URL') }}"><img src="{{ env('APP_URL') . '/app/assets/email/link-icon.png' }}" style="width:24px;"></a>
			</td>
		</tr>
		<tr style=""><td colspan="4">&nbsp;</td></tr>
	</table>
</td>
</tr>
</table>
</body>
</html>