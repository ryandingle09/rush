<?php

namespace Rush\Modules\Alert\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use Rush\Modules\Alert\AlertHelper;
use Rush\Modules\Alert\Events\JobFailedEvent;
use Rush\Modules\Email\Repositories\EmailLogRepository;
use Rush\Modules\Helpers\CmsHelper;

class JobFailedListener implements ShouldQueue
{
    /**
     * @var Rush\Modules\Email\Repositories\EmailLogRepository
     */
    protected $emailLogRepository;

    public function __construct(EmailLogRepository $emailLogRepository)
    {
        $this->emailLogRepository = $emailLogRepository;
    }

    /**
     * Handle the event.
     *
     * @param  Rush\Modules\Alert\Events\JobFailedEvent  $event
     * @return void
     */
    public function handle(JobFailedEvent $event)
    {
        $recipient = AlertHelper::getReceivers('module.alert.job_failed');
        $appEnv = env('APP_ENV', 'local');
        $appUrl = env('APP_URL', 'http://localhost');
        $basePath = base_path();

        // get the related merchant object
        $command = unserialize($event->job['data']['command']);
        $merchantId = method_exists($command, 'getMerchantId') ? $command->getMerchantId() : CmsHelper::SYSTEM_MERCHANT_ID;

        Mail::send(
            ['text' => 'alert::email.jobFailed'],
            [   'appEnv' => $appEnv,
                'appUrl' => $appUrl,
                'basePath' => $basePath,
                'job' => $event->job
            ],
            function($email) use ($recipient, $appUrl) {
                $email->to($recipient['to'])
                    ->cc($recipient['cc'])
                    ->bcc($recipient['bcc'])
                    ->from(AlertHelper::getFrom(), 'Rush')
                    ->subject('Queue Job Failed in: '.$appUrl);
            }
        );

        $this->emailLogRepository->create([
            'merchant_id' => $merchantId,
            'email_id' => null,
            'email_address' => json_encode($recipient),
            'transaction_type' => 'Alert: JobFailed'
        ]);
    }
}
