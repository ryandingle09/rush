<?php

namespace Rush\Modules\Alert\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use Rush\Modules\Alert\AlertHelper;
use Rush\Modules\Alert\Events\BillingGenerationEvent;
use Rush\Modules\Alert\Services\AlertService;
use Rush\Modules\Email\Repositories\EmailLogRepository;
use Rush\Modules\Billing\Services\BillingService;
use Rush\Modules\Merchant\Services\MerchantService;
use Carbon;


class BillingGenerationListener implements ShouldQueue
{
    protected $emailLogRepository;
    protected $billingService;
    protected $merchantService;

    public function __construct(
        EmailLogRepository $emailLogRepository,
        BillingService $billingService,
        MerchantService $merchantService
    ) {
        $this->emailLogRepository = $emailLogRepository;
        $this->billingService = $billingService;
        $this->merchantService = $merchantService;
    }

    public function handle(BillingGenerationEvent $event)
    {
        $billing = $this->billingService->getById($event->billing);
        $merchant = $this->merchantService->getById($event->merchant);
        $billing_period = Carbon\Carbon::parse($billing->billing_period_start)->format('M. j') . ' - ' . Carbon\Carbon::parse($billing->billing_period_end)->format('M. j, Y');
        $recipient['to'] = explode("|", getenv('BG_EMAIL_DEV'));
        $recipient['bcc'] = explode("|", getenv('BG_EMAIL_BCC'));
        if ( app()->env == "production" ) {
            $recipient['to'] = $merchant->username;
        }
      
        Mail::send(
            ['html' => 'alert::email.billingGeneration'],
            [   'name' => $merchant->personName,
                'company' => $merchant->businessName,
                'billing_period' => $billing_period
            ],
            function($email) use ($recipient, $merchant, $billing, $billing_period) {
                $email->to( $recipient['to'] )
                    ->bcc($recipient['bcc'])
                    ->from( AlertHelper::getFrom(), 'Rush')
                    ->subject('RUSH SOA ['. $billing_period .']');

                // attach the SOA
                $pdf = $this->billingService->getBillingPdf( $billing, $merchant );
                $email->attachData($pdf->output(), $billing->merchant_id . '-soa-'. time() . '.pdf');

            }
        );

        $this->emailLogRepository->create([
            'merchant_id' => $merchant->merchantid,
            'email_id' => null,
            'email_address' => json_encode($recipient),
            'transaction_type' => 'Alert: BillingGeneration'
        ]);
    }
}
