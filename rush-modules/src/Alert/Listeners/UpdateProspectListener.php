<?php

namespace Rush\Modules\Alert\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use Rush\Modules\Alert\AlertHelper;
use Rush\Modules\Alert\Events\UpdateProspectEvent;
use Rush\Modules\Email\Repositories\EmailLogRepository;
use Rush\Modules\Helpers\CmsHelper;

class UpdateProspectListener implements ShouldQueue
{
    /**
     * @var EmailLogRepository
     */
    protected $emailLogRepository;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(EmailLogRepository $emailLogRepository)
    {
        $this->emailLogRepository = $emailLogRepository;
    }

    /**
     * Handle the event.
     *
     * @param  Rush\Modules\Alert\Events\LoyaltyCardOrderEvent  $event
     * @return void
     */
    public function handle(UpdateProspectEvent $event)
    {
        $recipient = [
            'bcc' => (array) json_decode( getenv('REQUEST_DEMO_BCC') ),
            'sg_bcc' => (array) json_decode( env('SG_EMAIL_BCC') ),
        ];

        $data = $event->data;

        Mail::send(
            [   'html' => 'alert::email.updateProspect' ],
            [   'name' => $data['name'],
                'fname' => isset($data['fname']) ? $data['fname'] : '',
                'lname' => isset($data['lname']) ? $data['lname'] : '',
                'companyname' => $data['companyname'],
                'industry' => $data['industry'],
                'mobile' => $data['mobile'],
                'email' => $data['email'],
                'objective' => $data['objective'],
                'package' => $data['package'],
                'request_message' => $data['request_message'],
                'landing_page' => isset($data['landing_page']) ? $data['landing_page'] : false,
                'ip_address' => isset($data['ip_address']) ? $data['ip_address'] : false,
                'bd_name' => isset($data['bd_name']) ? $data['bd_name'] : false,
            ],
            function($email) use ($recipient, $data) {
                $email->to( $data['bd_email'] )
                ->bcc($recipient['bcc'], 'bcc')
                ->from(AlertHelper::getFrom(), 'RUSH')
                ->subject( '[RUSH] Customer Prospect' );
            }
        );

        $this->emailLogRepository->create([
            'merchant_id' => CmsHelper::SYSTEM_MERCHANT_ID,
            'email_id' => null,
            'email_address' => $data['email'],
            'transaction_type' => 'Alert: BD Assignment Email'
        ]);

    }
}
