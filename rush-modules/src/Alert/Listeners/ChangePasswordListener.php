<?php

namespace Rush\Modules\Alert\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use Rush\Modules\Alert\AlertHelper;
use Rush\Modules\Alert\Events\ChangePasswordEvent;
use Rush\Modules\Email\Repositories\EmailLogRepository;

class ChangePasswordListener implements ShouldQueue
{
    /**
     * @var EmailLogRepository
     */
    protected $emailLogRepository;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(EmailLogRepository $emailLogRepository)
    {
        $this->emailLogRepository = $emailLogRepository;
    }

    /**
     * Handle the event.
     *
     * @param  Rush\Modules\Alert\Events\ChangePasswordEvent  $event
     * @return void
     */
    public function handle(ChangePasswordEvent $event)
    {
        $recipient = AlertHelper::getReceivers('module.alert.change_password');
        $merchant = $event->merchant;
        $merchantSetting = $merchant->settings;
        Mail::send(
            ['text' => 'alert::email.changePassword'],
            [   'programName' => ($merchantSetting) ? $merchantSetting->program_name : 'NOT SET: Initial Change Password',
                'username' => $merchant->username,
                'newPassword' => $event->newPassword
            ],
            function($email) use ($recipient) {
                $email->to($recipient['to'])
                    ->cc($recipient['cc'])
                    ->bcc($recipient['bcc'])
                    ->from(AlertHelper::getFrom(), 'Rush')
                    ->subject('Merchant Password Changed');
            }
        );

        $this->emailLogRepository->create([
            'merchant_id' => $merchant->merchantid,
            'email_id' => null,
            'email_address' => json_encode($recipient),
            'transaction_type' => 'Alert: ChangePassword'
        ]);
    }
}
