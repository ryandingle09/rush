<?php

namespace Rush\Modules\Alert\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use Rush\Modules\Alert\AlertHelper;
use Rush\Modules\Alert\Events\RequestDemoEvent;
use Rush\Modules\Email\Repositories\EmailLogRepository;
use Rush\Modules\Admin\Repositories\ProspectsRepository;
use Rush\Modules\Helpers\CmsHelper;

class RequestDemoListener implements ShouldQueue
{
    /**
     * @var EmailLogRepository
     */
    protected $emailLogRepository;
    protected $prospectsRepository;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(EmailLogRepository $emailLogRepository, ProspectsRepository $prospectsRepository)
    {
        $this->emailLogRepository = $emailLogRepository;
        $this->prospectsRepository = $prospectsRepository;
    }

    /**
     * Handle the event.
     *
     * @param  Rush\Modules\Alert\Events\LoyaltyCardOrderEvent  $event
     * @return void
     */
    public function handle(RequestDemoEvent $event)
    {
        $recipient = [
            'to' => explode("|", getenv('REQUEST_DEMO_TO')),
            'bcc' => explode("|", getenv('REQUEST_DEMO_BCC')),
            'sg_to' => explode("|", getenv('SG_EMAIL_TO')),
            'sg_bcc' => explode("|", getenv('SG_EMAIL_BCC')),
        ];

        $data = $event->data;
        
        Mail::send(
            [   'html' => 'alert::email.requestDemoAdmin' ],
            [   //'name' => $data['name'],
                //'fname' => isset($data['fname']) ? $data['fname'] : '',
                //'lname' => isset($data['lname']) ? $data['lname'] : '',
                'companyname' => $data['companyname'],
                //'industry' => $data['industry'],
                'mobile' => $data['mobile'],
                'email' => $data['email'],
                //'objective' => $data['objective'],
                //'package' => $data['package'],
                //'request_message' => $data['request_message'],
                'from_sg' => isset($data['from_sg']) ? $data['from_sg'] : false,
            ],
            function($email) use ($recipient, $data) {

                if(isset($data['from_sg']) && $data['from_sg']){
                    $email->to( $recipient['sg_to'] )
                    ->bcc( $recipient['sg_bcc'], 'bcc')
                    ->from(AlertHelper::getFrom(), 'RUSH')
                    ->subject('[RUSH x SG] Appointment Request');
                } else {
                    $email->to($recipient['to'])
                    ->bcc($recipient['bcc'], 'bcc')
                    ->from(AlertHelper::getFrom(), 'RUSH')
                    ->subject( isset($data['admin_email_subject']) ? $data['admin_email_subject'] : '[RUSH] Request A Demo' );
                }
            }
        );

        $admin_email_log['to'] = ( isset($data['from_sg']) ) ? json_encode($recipient['sg_to']) : json_encode($recipient['to']);
        $admin_email_log['bcc'] = ( isset($data['from_sg']) ) ? json_encode($recipient['sg_bcc']) : json_encode($recipient['bcc']);
        $admin_email_log = json_encode( $admin_email_log );
        $url = ( isset($data['url'])) ? $data['url'] : '';

        $this->emailLogRepository->create([
            'merchant_id' => CmsHelper::SYSTEM_MERCHANT_ID,
            'email_id' => null,
            'email_address' => $admin_email_log,
            'transaction_type' => 'Alert: Request-A-Demo Admin ' . $url
        ]);

        Mail::send(
            [   'html' => 'alert::email.requestDemoUser' ],
            [   'name' => $data['email'],
                'from_sg' => isset($data['from_sg']) ? $data['from_sg'] : false,
                'user_email_content' => isset($data['user_email_content']) ? $data['user_email_content'] : 'Thank you for your interest!'
            ],
            function($email) use ($recipient, $data) {

                $email->to( $data['email'] )
                    ->bcc($recipient['bcc'], 'bcc')
                    ->from(AlertHelper::getFrom(), 'RUSH')
                    ->subject( isset($data['user_email_subject']) ? $data['user_email_subject'] : 'Thank you for your interest!' );
            }
        );

        $this->prospectsRepository->save( $data );

        $email_log['to'] = $data['email'];
        $email_log['bcc'] = ( isset($data['from_sg']) ) ? json_encode($recipient['sg_bcc']) : json_encode($recipient['bcc']);
        $email_log = json_encode( $email_log );

        $this->emailLogRepository->create([
            'merchant_id' => CmsHelper::SYSTEM_MERCHANT_ID,
            'email_id' => null,
            'email_address' => $email_log,
            'transaction_type' => 'Alert: Request-A-Demo ' . $url
        ]);

    }
}
