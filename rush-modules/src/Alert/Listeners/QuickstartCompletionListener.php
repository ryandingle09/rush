<?php

namespace Rush\Modules\Alert\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use Rush\Modules\Alert\AlertHelper;
use Rush\Modules\Alert\Events\QuickstartCompletionEvent;
use Rush\Modules\Email\Repositories\EmailLogRepository;
use Rush\Modules\Helpers\Facades\StorageHelper;

class QuickstartCompletionListener implements ShouldQueue
{
    /**
     * @var EmailLogRepository
     */
    protected $emailLogRepository;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(EmailLogRepository $emailLogRepository)
    {
        $this->emailLogRepository = $emailLogRepository;
    }

    /**
     * Handle the event.
     *
     * @param  Rush\Modules\Alert\Events\QuickstartCompletionEvent  $event
     * @return void
     * @var Rush\Modules\Merchant\Models\MerchantModel $merchant
     * @var Rush\Modules\Merchant\Models\MerchantSettingModel $merchantSetting
     * @var Rush\Modules\Merchant\Models\MerchantPackageModel $merchantPackage
     */
    public function handle(QuickstartCompletionEvent $event)
    {
        $recipient = AlertHelper::getReceivers('module.alert.quickstart_completion');
        $merchant = $event->merchant;
        $merchantSetting = $merchant->settings;
        $merchantPackage = $merchant->package;
        $disk = StorageHelper::getRepositoryDisk();

        if ($merchantSetting) {
            $recipient['appstoreLogo'] = $merchantSetting->app_logo;
            $recipient['merchantAppLogo'] = $merchant->merchant_design->logoURL;
            $recipient['customerAppLogo'] = $merchant->customer_design->logoURL;
            Mail::send(
                ['text' => 'alert::email.quickstartCompletion'],
                [   'merchant' => $merchant,
                    'merchantSetting' => $merchantSetting,
                    'loyaltyProgramName' => ($merchant->packageId == 1) ? 'Rush Pro' : 'Rush Basic',
                    'packageType' => ($merchant->packageId == 1) ? 'Points' : 'Stamp'
                ],
                function($email) use ($recipient, $merchant, $disk) {
                    $email->to($recipient['to'])
                        ->cc($recipient['cc'])
                        ->bcc($recipient['bcc'])
                        ->from(AlertHelper::getFrom(), 'Rush')
                        ->subject($merchant->businessName.': Quick Start Completion');

                    if ($disk->exists(StorageHelper::rebasePathToRepository($recipient['appstoreLogo']))) {
                        $email->attachData(
                            stream_get_contents(StorageHelper::repositoryUrlToStream($recipient['appstoreLogo'])),
                            'appstoreLogo.'.StorageHelper::metadata($recipient['appstoreLogo'], 'extension')
                        );
                    }
                    if ($disk->exists(StorageHelper::rebasePathToRepository($recipient['merchantAppLogo']))) {
                        $email->attachData(
                            stream_get_contents(StorageHelper::repositoryUrlToStream($recipient['merchantAppLogo'])),
                            'merchantAppLogo.'.StorageHelper::metadata($recipient['merchantAppLogo'], 'extension')
                        );
                    }
                    if ($disk->exists(StorageHelper::rebasePathToRepository($recipient['customerAppLogo']))) {
                        $email->attachData(
                            stream_get_contents(StorageHelper::repositoryUrlToStream($recipient['customerAppLogo'])),
                            'customerAppLogo.'.StorageHelper::metadata($recipient['customerAppLogo'], 'extension')
                        );
                    }
                }
            );

            $this->emailLogRepository->create([
                'merchant_id' => $merchant->merchantid,
                'email_id' => null,
                'email_address' => json_encode($recipient),
                'transaction_type' => 'Alert: QuickstartCompletion'
            ]);
        }
    }
}
