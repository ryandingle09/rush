<?php

namespace Rush\Modules\Alert\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use Rush\Modules\Alert\AlertHelper;
use Rush\Modules\Alert\Events\MerchantSignupEvent;
use Rush\Modules\Email\Repositories\EmailLogRepository;

class MerchantSignupListener implements ShouldQueue
{
    /**
     * @var EmailLogRepository
     */
    protected $emailLogRepository;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(EmailLogRepository $emailLogRepository)
    {
        $this->emailLogRepository = $emailLogRepository;
    }

    /**
     * Handle the event.
     *
     * @param  Rush\Modules\Alert\Events\MerchantSignupEvent  $event
     * @return void
     */
    public function handle(MerchantSignupEvent $event)
    {
        $recipient = AlertHelper::getReceivers('module.alert.merchant_signup');
        $merchant = $event->merchant;
        $industry = array();

        foreach($merchant->business_type_name AS $businessType) {
            $industry[] = $businessType->name;
        }
        $industryName = implode(',', $industry);

        Mail::send(
            [   'html' => 'alert::email.merchantSignup' ],
            [   'merchant' => $merchant,
                'plan' => ($merchant->packageId == 1) ? 'Points' : 'Punch Card',
                'industry' => $industryName
            ],
            function($email) use ($recipient, $merchant) {
                $email->to($recipient['to'])
                    ->cc($recipient['cc'])
                    ->bcc($recipient['bcc'])
                    ->from(AlertHelper::getFrom(), 'Rush')
                    ->subject($merchant->businessName.': New Merchant Sign-Up');
            }
        );

        $this->emailLogRepository->create([
            'merchant_id' => $merchant->merchantid,
            'email_id' => null,
            'email_address' => json_encode($recipient),
            'transaction_type' => 'Alert: MerchantSignup'
        ]);
    }
}
