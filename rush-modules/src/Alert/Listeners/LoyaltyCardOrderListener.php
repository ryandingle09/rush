<?php

namespace Rush\Modules\Alert\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use Rush\Modules\Alert\AlertHelper;
use Rush\Modules\Alert\Events\LoyaltyCardOrderEvent;
use Rush\Modules\Email\Repositories\EmailLogRepository;

class LoyaltyCardOrderListener implements ShouldQueue
{
    /**
     * @var EmailLogRepository
     */
    protected $emailLogRepository;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(EmailLogRepository $emailLogRepository)
    {
        $this->emailLogRepository = $emailLogRepository;
    }

    /**
     * Handle the event.
     *
     * @param  Rush\Modules\Alert\Events\LoyaltyCardOrderEvent  $event
     * @return void
     */
    public function handle(LoyaltyCardOrderEvent $event)
    {
        $recipient = [
            'to' => env( 'ADMIN_EMAIL', 'dreal+test@yondu.com'),
            'bcc' => env( 'BCC_EMAIL', 'dreal+bcc@yondu.com')
        ];

        $merchant = $event->merchant;
        $quantity = $event->quantity;
        $amount = $quantity * 50;

        Mail::send(
            ['html' => 'alert::email.loyaltyCardOrder'],
            [   'merchant_name' => $merchant->businessName,
                'quantity'  => $quantity,
                'amount' => $amount,
            ],
            function($email) use ($recipient) {
                $email->to($recipient['to'])
                    ->bcc($recipient['bcc'], 'bcc')
                    ->from(AlertHelper::getFrom(), 'Rush')
                    ->subject('White-Labeled Loyalty Card Order');
            }
        );

        $this->emailLogRepository->create([
            'merchant_id' => $merchant->merchantid,
            'email_id' => null,
            'email_address' => $recipient['to'],
            'transaction_type' => 'Alert: LoyaltyCardOrder'
        ]);
    }
}
