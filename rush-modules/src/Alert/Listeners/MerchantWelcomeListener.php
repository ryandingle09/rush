<?php

namespace Rush\Modules\Alert\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use Rush\Modules\Alert\AlertHelper;
use Rush\Modules\Alert\Events\MerchantWelcomeEvent;
use Rush\Modules\Alert\Services\AlertService;
use Rush\Modules\Email\Repositories\EmailLogRepository;
use Rush\Modules\Merchant\Services\MerchantService;

class MerchantWelcomeListener implements ShouldQueue
{
    /**
     * @var Rush\Modules\Email\Repositories\EmailLogRepository
     */
    protected $emailLogRepository;

    /**
     * @var Rush\Modules\Alert\Services\AlertService
     */
    protected $alertService;

    /**
     * @var Rush\Modules\Merchant\Services\MerchantService
     */
    protected $merchantService;

    public function __construct(
        EmailLogRepository $emailLogRepository,
        AlertService $alertService,
        MerchantService $merchantService
    ) {
        $this->emailLogRepository = $emailLogRepository;
        $this->alertService = $alertService;
        $this->merchantService = $merchantService;
    }

    /**
     * Handle the event.
     *
     * @param  Rush\Modules\Alert\Events\MerchantWelcomeEvent  $event
     * @return void
     */
    public function handle(MerchantWelcomeEvent $event)
    {
        $alertService =& $this->alertService;
        $registration = $event->registration;
        $merchant = $this->merchantService->getByUsername($registration->person_email);
        $recipient = AlertHelper::getReceivers('module.alert.merchant_welcome');
        $recipient['to'] = $registration->person_email;

        Mail::send(
            ['html' => 'alert::email.merchantWelcome'],
            [   'name' => $registration->person_name,
                'company' => $registration->business_name,
                'package' => $registration->business_package,
                'user' => $registration->person_email,
                'password' => $registration->person_password
            ],
            function($email) use ($recipient, $merchant, &$alertService) {
                $email->to($recipient['to'])
                    ->bcc($recipient['bcc'])
                    ->from(AlertHelper::getFrom(), 'Rush')
                    ->subject('Experience loyalty made easy with RUSH.');

                // attach the Service Agreement
                $pdf = $alertService->getServiceAgreementPdf($merchant->merchantid);
                $email->attachData($pdf->output(), 'service-agreement.pdf');

                // attach Loyalty User Manual
                if ($merchant->isPro() && $loyaltyUserManualPdf = $alertService->getLoyaltyUserManualPdf()) {
                    $email->attach($loyaltyUserManualPdf);
                }
            }
        );

        $this->emailLogRepository->create([
            'merchant_id' => $merchant->merchantid,
            'email_id' => null,
            'email_address' => json_encode($recipient),
            'transaction_type' => 'Alert: MerchantWelcome'
        ]);
    }
}
