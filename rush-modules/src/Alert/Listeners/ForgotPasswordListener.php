<?php

namespace Rush\Modules\Alert\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use Rush\Modules\Alert\AlertHelper;
use Rush\Modules\Alert\Events\ForgotPasswordEvent;
use Rush\Modules\Email\Repositories\EmailLogRepository;
use Rush\Modules\Helpers\CmsHelper;

class ForgotPasswordListener implements ShouldQueue
{
    /**
     * @var EmailLogRepository
     */
    protected $emailLogRepository;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(EmailLogRepository $emailLogRepository)
    {
        $this->emailLogRepository = $emailLogRepository;
    }

    /**
     * Handle the event.
     *
     * @param  Rush\Modules\Alert\Events\LoyaltyCardOrderEvent  $event
     * @return void
     */
    public function handle(ForgotPasswordEvent $event)
    {
        $recipient = [
            'to' => explode("|", getenv('REQUEST_DEMO_TO')),
            'bcc' => explode("|", getenv('REQUEST_DEMO_BCC'))
        ];

        $data = $event->data;

        Mail::send(
            [   'html' => 'alert::email.forgotPassword' ],
            [   'name' => $data['name'],
                'email' => $data['email'],
                'password' => $data['password']
            ],
            function($email) use ($recipient, $data) {

                $email->to( $data['email'] )
                    ->bcc( $recipient['bcc'], 'bcc' )
                    ->from(AlertHelper::getFrom(), 'RUSH')
                    ->subject('Password Reset');
            }
        );

        $this->emailLogRepository->create([
            'merchant_id' => CmsHelper::SYSTEM_MERCHANT_ID,
            'email_id' => null,
            'email_address' => $data['email'],
            'transaction_type' => 'Alert: Forgot Password'
        ]);

    }
}
