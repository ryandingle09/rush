<?php

namespace Rush\Modules\Alert\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use Rush\Modules\Alert\AlertHelper;
use Rush\Modules\Alert\Events\ChangeAppstoreLogoEvent;
use Rush\Modules\Email\Repositories\EmailLogRepository;
use Rush\Modules\Helpers\Facades\StorageHelper;

class ChangeAppstoreLogoListener implements ShouldQueue
{
    /**
     * @var Rush\Modules\Email\Repositories\EmailLogRepository
     */
    protected $emailLogRepository;

    public function __construct(EmailLogRepository $emailLogRepository)
    {
        $this->emailLogRepository = $emailLogRepository;
    }

    /**
     * Handle the event.
     *
     * @param  Rush\Modules\Alert\Events\ChangeAppstoreLogoEvent  $event
     * @return void
     */
    public function handle(ChangeAppstoreLogoEvent $event)
    {
        $recipient = AlertHelper::getReceivers('module.alert.change_appstore_logo');
        $merchant = $event->merchant;
        $merchantSetting = $merchant->settings;
        $recipient['appstoreLogo'] = $merchantSetting->app_logo;

        Mail::send(
            ['text' => 'alert::email.changeAppstoreLogo'],
            [   'programName' => $merchantSetting->program_name,
                'merchantId' => $merchantSetting->merchant_id,
                'appName' => $merchantSetting->app_name,
                'appDescription' => $merchantSetting->app_description,
            ],
            function($email) use ($recipient, $merchantSetting) {
                $email->to($recipient['to'])
                    ->cc($recipient['cc'])
                    ->bcc($recipient['bcc'])
                    ->from(AlertHelper::getFrom(), 'Rush')
                    ->subject($merchantSetting->program_name.': Change Appstore Logo');

                if ($recipient['appstoreLogo']) {
                    $email->attachData(
                        stream_get_contents(StorageHelper::repositoryUrlToStream($recipient['appstoreLogo'])),
                        'appstoreLogo.'.StorageHelper::metadata($recipient['appstoreLogo'], 'extension')
                    );
                }
            }
        );

        $this->emailLogRepository->create([
            'merchant_id' => $merchant->merchantid,
            'email_id' => null,
            'email_address' => json_encode($recipient),
            'transaction_type' => 'Alert: ChangeAppstoreLogo'
        ]);
    }
}
