<?php

namespace Rush\Modules\Alert\Events;

use App\Events\Event;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;
use Rush\Modules\Merchant\Models\MerchantModel;

class ChangePasswordEvent extends Event
{
    use SerializesModels;

    /**
     * @var Rush\Modules\Merchant\Models\MerchantModel
     */
    public $merchant;

    public $newPassword;

    /**
     * Create a new event instance.
     * @param Rush\Modules\Merchant\Models\MerchantModel $merchant
     * @return void
     */
    public function __construct(MerchantModel $merchant, $newPassword)
    {
        $this->merchant = $merchant;
        $this->newPassword = $newPassword;
    }
}
