<?php

namespace Rush\Modules\Alert\Events;

use App\Events\Event;
use Illuminate\Queue\Events\JobFailed;
use Illuminate\Queue\SerializesModels;

class JobFailedEvent extends Event
{
    use SerializesModels;

    /**
     * @var Illuminate\Queue\Events\JobFailed
     */
    public $job;

    /**
     * Create a new event instance.
     * @param Illuminate\Queue\Events\JobFailed $job
     * @return void
     */
    public function __construct($job)
    {
        $this->job = $job;
    }
}
