<?php

namespace Rush\Modules\Alert\Events;

use App\Events\Event;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;

class MerchantWelcomeEvent extends Event
{
    use SerializesModels;

    /**
     * @var stdClass
     */
    public $registration;

    /**
     * Create a new event instance.
     * @param \stdClass
     * @return void
     */
    public function __construct($registration)
    {
        $this->registration = $registration;
    }
}
