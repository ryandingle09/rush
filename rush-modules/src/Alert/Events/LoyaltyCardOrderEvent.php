<?php

namespace Rush\Modules\Alert\Events;

use App\Events\Event;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;
use Rush\Modules\Merchant\Models\MerchantModel;

class LoyaltyCardOrderEvent extends Event
{
    use SerializesModels;

    public $merchant, $quantity;

    public function __construct(MerchantModel $merchant, $quantity)
    {
        $this->merchant = $merchant;
        $this->quantity = $quantity;
    }
}
