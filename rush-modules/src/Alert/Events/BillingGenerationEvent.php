<?php

namespace Rush\Modules\Alert\Events;

use App\Events\Event;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;

class BillingGenerationEvent extends Event
{
    use SerializesModels;

    public $billing;
    public $merchant;

    /**
     * Create a new event instance.
     * @param \stdClass
     * @return void
     */
    public function __construct($billing, $merchant)
    {
        $this->billing = $billing;
        $this->merchant = $merchant;
    }
}
