<?php

namespace Rush\Modules\Alert\Events;

use App\Events\Event;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;

class ForgotPasswordEvent extends Event
{
    use SerializesModels;

    public $data;
    
    public function __construct( $data )
    {
        $this->data = $data;
    }
}
