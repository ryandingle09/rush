<?php

namespace Rush\Modules\Alert\Events;

use App\Events\Event;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;
use Rush\Modules\Merchant\Models\MerchantModel;

class MerchantSignupEvent extends Event
{
    use SerializesModels;

    /**
     * @var Rush\Modules\Merchant\Models\MerchantModel
     */
    public $merchant;

    /**
     * Create a new event instance.
     * @param Rush\Modules\Merchant\Models\MerchantModel $merchant
     * @return void
     */
    public function __construct(MerchantModel $merchant)
    {
        $this->merchant = $merchant;
    }
}
