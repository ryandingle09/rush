<?php

namespace Rush\Modules\Alert;

final class AlertHelper
{
    public static function getReceivers($key)
    {
        return [
            'to' => config("$key.to"),
            'cc' => config("$key.cc"),
            'bcc' => config("$key.bcc"),
        ];
    }

    public static function getFrom()
    {
        return env('ALERT_DEFAULT_FROM', 'no-reply@rush.ph');
    }

    private function __construct(){
        throw new Exception("AlertHelper not instantiable");
    }
}