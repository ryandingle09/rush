<?php

namespace Rush\Modules\Alert\Services;

use Illuminate\Support\Facades\File;
use PDF;
use Rush\Modules\Helpers\Facades\StorageHelper;
use Rush\Modules\Merchant\Repositories\MerchantRepository;
use View;
use \Carbon\Carbon;

/**
* Our AlertService, containing all useful methods for business logic around Alert
* Service just hands off data it was given by the controller to the appropriate repositories
*/
class AlertService
{
    /**
     * @var MerchantRepository
     */
    protected $merchantRepository;

    public function __construct(MerchantRepository $merchantRepository)
    {
        $this->merchantRepository = $merchantRepository;
    }

    public function getServiceAgreementPdf($merchantId)
    {
        $merchant = $this->merchantRepository->getById($merchantId);

        $footerView = View::make('page.service-agreement-footer', [
            'name' => $merchant->contactPerson,
            'email' => $merchant->contactPersonEmail,
            'date' => Carbon::today()->toFormattedDateString(),
        ]);
        $footer = $footerView->render();

        $pdf = PDF::loadView('page.service-agreement', [])
            ->setOption('zoom', .75)
            ->setOption('footer-right', $footer)
            ->setOption('footer-line', true)
            ->setOption('footer-font-size', 7);

        return $pdf;
    }

    /**
     * return the path to Loyalty User Manual pdf file
     * @return string
     */
    public function getLoyaltyUserManualPdf()
    {
        $loyaltyUserManualPath = StorageHelper::rush_path('cpanel/assets/pdf/RUSH_PRO_20161108.pdf');

        return File::exists($loyaltyUserManualPath) ? $loyaltyUserManualPath : null;
    }
}
