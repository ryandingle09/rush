<?php

return [
    'alert' => [
        'change_password' => [
            'to' => explode('|', env('ALERT_CHANGE_PASSWORD_TO', env('ALERT_DEFAULT_TO', ''))),
            'cc' => explode('|', env('ALERT_CHANGE_PASSWORD_CC', env('ALERT_DEFAULT_CC', ''))),
            'bcc' => explode('|', env('ALERT_CHANGE_PASSWORD_BCC', env('ALERT_DEFAULT_BCC', '')))
        ],
        'merchant_signup' => [
            'to' => explode('|', env('ALERT_MERCHANT_SIGNUP_TO', env('ALERT_DEFAULT_TO', ''))),
            'cc' => explode('|', env('ALERT_MERCHANT_SIGNUP_CC', env('ALERT_DEFAULT_CC', ''))),
            'bcc' => explode('|', env('ALERT_MERCHANT_SIGNUP_BCC', env('ALERT_DEFAULT_BCC', '')))
        ],
        'quickstart_completion' => [
            'to' => explode('|', env('ALERT_QUICKSTART_COMPLETION_TO', env('ALERT_DEFAULT_TO', ''))),
            'cc' => explode('|', env('ALERT_QUICKSTART_COMPLETION_CC', env('ALERT_DEFAULT_CC', ''))),
            'bcc' => explode('|', env('ALERT_QUICKSTART_COMPLETION_BCC', env('ALERT_DEFAULT_BCC', '')))
        ],
        'merchant_welcome' => [
            'bcc' => explode('|', env('ALERT_MERCHANT_WELCOME_BCC', env('ALERT_DEFAULT_BCC', '')))
        ],
        'change_appstore_logo' => [
            'to' => explode('|', env('ALERT_CHANGE_APPSTORE_LOGO_TO', env('ALERT_DEFAULT_TO', ''))),
            'cc' => explode('|', env('ALERT_CHANGE_APPSTORE_LOGO_CC', env('ALERT_DEFAULT_CC', ''))),
            'bcc' => explode('|', env('ALERT_CHANGE_APPSTORE_LOGO_BCC', env('ALERT_DEFAULT_BCC', '')))
        ],
        'job_failed' => [
            'to' => explode('|', env('ALERT_JOB_FAILED_TO', env('ALERT_DEFAULT_TO', ''))),
            'cc' => explode('|', env('ALERT_JOB_FAILED_CC', env('ALERT_DEFAULT_CC', ''))),
            'bcc' => explode('|', env('ALERT_JOB_FAILED_BCC', env('ALERT_DEFAULT_BCC', '')))
        ],
    ]
];