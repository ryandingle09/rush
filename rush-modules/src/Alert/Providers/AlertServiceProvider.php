<?php

namespace Rush\Modules\Alert\Providers;

use Event;
use Illuminate\Container\Container;
use Illuminate\Queue\Events\JobFailed;
use Illuminate\Support\ServiceProvider;
use Queue;
use Rush\Modules\Alert\Events\JobFailedEvent;

/**
* Register our Module with Laravel
*/
class AlertServiceProvider extends ServiceProvider
{
    /**
    * Perform post-registration booting of services.
    *
    * @return void
    */
    public function boot()
    {
        if (! $this->app->routesAreCached()) {
            require __DIR__.'/../Http/routes.php';
        }

        // register package views
        $this->loadViewsFrom(__DIR__.'/../Views', 'alert');

        // register helper
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('AlertHelper', 'Rush\Modules\Alert\AlertHelper');
    }

    public function register()
    {
        Event::listen('Rush\Modules\Alert\Events\ChangePasswordEvent', 'Rush\Modules\Alert\Listeners\ChangePasswordListener');
        Event::listen('Rush\Modules\Alert\Events\MerchantSignupEvent', 'Rush\Modules\Alert\Listeners\MerchantSignupListener');
        Event::listen('Rush\Modules\Alert\Events\QuickstartCompletionEvent', 'Rush\Modules\Alert\Listeners\QuickstartCompletionListener');
        Event::listen('Rush\Modules\Alert\Events\LoyaltyCardOrderEvent', 'Rush\Modules\Alert\Listeners\LoyaltyCardOrderListener');
        Event::listen('Rush\Modules\Alert\Events\MerchantWelcomeEvent', 'Rush\Modules\Alert\Listeners\MerchantWelcomeListener');
        Event::listen('Rush\Modules\Alert\Events\ChangeAppstoreLogoEvent', 'Rush\Modules\Alert\Listeners\ChangeAppstoreLogoListener');
        Event::listen('Rush\Modules\Alert\Events\RequestDemoEvent', 'Rush\Modules\Alert\Listeners\RequestDemoListener');
        Event::listen('Rush\Modules\Alert\Events\JobFailedEvent', 'Rush\Modules\Alert\Listeners\JobFailedListener');
        Event::listen('Rush\Modules\Alert\Events\ForgotPasswordEvent', 'Rush\Modules\Alert\Listeners\ForgotPasswordListener');
        Event::listen('Rush\Modules\Alert\Events\UpdateProspectEvent', 'Rush\Modules\Alert\Listeners\UpdateProspectListener');
        Event::listen('Rush\Modules\Alert\Events\BillingGenerationEvent', 'Rush\Modules\Alert\Listeners\BillingGenerationListener');

        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'module'
        );
    }
}
