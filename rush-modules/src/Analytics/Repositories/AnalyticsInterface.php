<?php

namespace Rush\Modules\Analytics\Repositories;

use Rush\Modules\Repository\RepositoryInterface;

/**
 * The AnalyticsInterface contains unique method signatures related to Analytics object
 */
interface AnalyticsInterface extends RepositoryInterface
{
    public function getTransactionTotal($merchantId);
    public function getPointsSeededTotal($merchantId);
    public function getPointsUsedTotal($merchantId);
    public function getMonthTransactionAmountTotal($merchantId, $dateOfFilter);
    public function getAverageRevenuePerCustomer($merchantId, $dateOfFilter);
    public function getAveragePointsEarnedPerCustomer($merchantId, $dateOfFilter);
    public function getAveragePointsBurnedPerCustomer($merchantId, $dateOfFilter);
    public function getMembersCount($merchantId);
    public function getNewMembersThisMonthCount($merchantId, $dateOfFilter);
    public function getAverageVisitsPerCustomer($merchantId, $dateOfFilter);
    public function getTopRedeemedRewards($merchantId);
    public function getTopCustomers($merchantId);
    public function getTopBranches($merchantId);
    public function getChannelsData($merchantId);
    public function getTransactionChannelData($merchantId, $filterMonth, $previousFilterMonth);
    public function getTransactionValuePeriodData($merchantId, $givenMonth);
    public function getTransactionFrequencyPeriodData($merchantId, $givenMonth);
    public function getAppDownloadData($merchantId, $platforms);
    public function getAppDownloadDataLifetime($merchantId, $platforms);
    public function getCustomerMonthlyTransactionData($merchantId);
    public function getVisitsData($merchantId);
    public function getProfileData($merchantId);
}
