<?php

namespace Rush\Modules\Analytics\Repositories;

use \stdClass;
use DB;
use Illuminate\Database\Eloquent\Model;
use Rush\Modules\Analytics\Models\PunchcardAnalyticsModel;
use Rush\Modules\Analytics\Repositories\PunchcardAnalyticsInterface;
use Rush\Modules\Repository\AbstractRepository;

/**
 * A repository class would encapsulate each Eloquent Model class
 * and be responsible for CRUD operations on the database.
 */
class PunchcardAnalyticsRepository extends AbstractRepository implements PunchcardAnalyticsInterface
{
    protected $connection = 'mysql';

    public function __construct(PunchcardAnalyticsModel $punchcardAnalyticsModel)
    {
        parent::__construct($punchcardAnalyticsModel);
    }

    /**
     * get members count
     * @param  int $merchantId
     * @return int
     */
    public function getMembersCount($merchantId)
    {
        $count = 0;
        $sql =
        "   SELECT
            COUNT(*) AS count
            FROM Customer c
            WHERE
            c.merchantid = ?
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId)
        );
        $count = $query[0]->count;

        return $count;
    }

    /**
     * get members count for the current month
     * @param  int $merchantId
     * @return int
     */
    public function getNewMembersThisMonthCount($merchantId, $dateOfFilter)
    {
        $count = 0;
        $sql =
        "   SELECT
            COUNT(*) AS count
            FROM Customer c
            WHERE
            c.merchantid = ?
            AND  DATE_FORMAT(c.timestamp, '%Y-%m') = ?
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql), [
               $merchantId,
                date('Y-m', $dateOfFilter)
            ]
        );
        $count = $query[0]->count;

        return $count;
    }

    public function getTransactionTotal($merchantId)
    {
        $total = 0;
        $sql =
        "   SELECT
            SUM(cpt.amount) AS total
            FROM
            CustomerPunchCardTransactions cpt
            LEFT JOIN Customer c ON cpt.customer_id = c.customerId
            WHERE
            cpt.merchant_id = ?
            AND cpt.void = 0
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId)
        );
        $total = $query[0]->total;

        return $total;
    }

    public function getPointsSeededTotal($merchantId)
    {
        $total = 0;
        $sql =
        "   SELECT
            SUM(cpt.amount) AS total
            FROM
            CustomerPunchCardTransactions cpt
            LEFT JOIN Customer c ON cpt.customer_id = c.customerId
            WHERE
            cpt.type = 'earn'
            AND cpt.merchant_id = ?
            AND cpt.void = 0
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId)
        );
        $total = $query[0]->total;

        return $total;
    }

    public function getPointsUsedTotal($merchantId)
    {
        $total = 0;
        $sql =
        "   SELECT
            SUM(cpt.amount) AS total
            FROM
            CustomerPunchCardTransactions cpt
            LEFT JOIN Customer c ON cpt.customer_id = c.customerId
            WHERE
            cpt.type = 'redeem'
            AND cpt.merchant_id = ?
            AND cpt.void = 0
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId)
        );
        $total = $query[0]->total;

        return $total;
    }

    public function getTopCustomers($merchantId)
    {
        $count = 0;
        $sql =
        "   SELECT
            c.fullName
            , COUNT(t.customer_id) AS totalPunch
            , SUM(t.pointsEarned) AS totalPointsEarned
            FROM
            (   SELECT
                cpt.customer_id
                , IF(cpt.type = 'earn', cpt.amount, 0) AS pointsEarned
                FROM
                CustomerPunchCardTransactions cpt
                WHERE
                cpt.merchant_id = ?
                AND cpt.void = 0
            ) AS t
            LEFT JOIN Customer c
                ON t.customer_id = c.customerId
            GROUP BY t.customer_id
            ORDER BY totalPointsEarned DESC
            LIMIT 20
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId)
        );

        return $query;
    }

    public function getTopBranches($merchantId)
    {
        $count = 0;
        $sql =
        "   SELECT
            b.branchName
            , SUM(t.earning) AS totalEarning
            , SUM(t.redemption) AS totalRedemption
            FROM
            (   SELECT
                cpt.branch_id
                , IF(cpt.type = 'earn', 1, 0) AS earning
                , IF(cpt.type = 'redeem', 1, 0) AS redemption
                FROM
                CustomerPunchCardTransactions cpt
                WHERE
                cpt.branch_id > 0
                AND cpt.merchant_id = ?
            ) AS t
            LEFT JOIN Branches b
                ON t.branch_id = b.id
            GROUP BY t.branch_id
            ORDER BY totalEarning DESC
            LIMIT 20
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId)
        );

        return $query;
    }

    /**
     * AVE. VISITS PER CUSTOMER
     * daily unique member trans, sum within the month
     * @param  int $merchantId
     * @return int
     */
    public function getAverageVisitsPerCustomer($merchantId, $dateOfFilter)
    {
        $count = 0;
        $sql =
        "   SELECT
            SUM(monthly.dailyUnique) AS count
            FROM
            (   SELECT
                DATE_FORMAT(ct.created_at, '%Y-%m-%d') AS day
                , COUNT(DISTINCT ct.customer_id) AS dailyUnique
                FROM
                CustomerPunchCardTransactions ct
                WHERE
                ct.merchant_id = ?
                AND NOT (ct.branch_id IS NULL OR ct.branch_id = 0)
                AND ct.type IN ('earn', 'redeem', 'paypoints')
                AND DATE_FORMAT(ct.created_at, '%Y-%m') = ?
                GROUP BY day
            ) monthly

        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql), [
                $merchantId,
                date('Y-m', $dateOfFilter)
            ]
        );
        $count = $query[0]->count;

        return $count;
    }

    public function getTopRedeemedRewards($merchantId)
    {
        $sql =
        "   SELECT
            mpr.reward as name
            , count(mpr.id) AS totalRedemption
            FROM MerchantPunchCardRewards as mpr
            LEFT JOIN CustomerPunchCardTransactions cpt
                ON mpr.id = cpt.reward_id
            WHERE cpt.merchant_id = ?
            GROUP BY mpr.id
            ORDER BY totalRedemption DESC
            LIMIT 20
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId)
        );

        return $query;
    }

    /**
     * TOTAL TXNS THIS MONTH (PHP)
     * is equal to transaction amount in Earned transaction (month)
     * @param  int $merchantId
     * @return int
     */
    public function getMonthTransactionAmountTotal($merchantId, $dateOfFilter)
    {
        $total = 0;
        $sql =
        "   SELECT
            SUM(cpt.amount) total
            FROM
            CustomerPunchCardTransactions cpt
            JOIN Customer c ON cpt.customer_id = c.customerId
            WHERE
            cpt.void = 0
            AND c.merchantId = ?
            AND DATE_FORMAT(cpt.date_punched, '%Y-%m') = ?
            GROUP BY
            c.merchantId
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId, date('Y-m', $dateOfFilter))
        );
        $total = $query ? $query[0]->total : 0;

        return $total;
    }

    /**
     * AVG REVENUE PER CUSTOMER (PHP)
     * TOTAL TXNS AMOUNT (MONTH)/TOTAL NUMBER OF UNIQUE CUSTOMER (MONTH) with earned transactions /4 weeks
     * @param  int $merchantId
     * @return int
     */
    public function getAverageRevenuePerCustomer($merchantId, $dateOfFilter)
    {
        $total = 0;
        $sql =
        "   SELECT
            SUM(cpt.amount) totalAmount
            , COUNT(distinct cpt.customer_id) uniqueCustomer
            FROM
            CustomerPunchCardTransactions cpt
            WHERE
            cpt.void = 0
            AND cpt.type = 'earn'
            AND cpt.merchant_id = ?
            AND DATE_FORMAT(cpt.date_punched, '%Y-%m') = ?
            GROUP BY
            cpt.merchant_id;
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId, date('Y-m', $dateOfFilter))
        );

        if ($query) {
            $total = ($query[0]->totalAmount / $query[0]->uniqueCustomer) / 4;
        }

        return $total;
    }

    /**
     * AVERAGE PTS EARNED PER CUSTOMER
     * TOTAL POINTS EARNED (MONTH)/TOTAL NUMBER OF UNIQUE CUSTOMER (MONTH) with earned transactions
     * @param  int $merchantId
     * @return int
     */
    public function getAveragePointsEarnedPerCustomer($merchantId, $dateOfFilter)
    {
        $total = 0;
        $sql =
        "   SELECT
            SUM(ct.amount) totalAmount
            , COUNT(distinct ct.customer_id) uniqueCustomer
            FROM
            CustomerPunchCardTransactions ct
            WHERE
            ct.type = 'earn'
            AND ct.merchant_id = ?
            AND DATE_FORMAT(ct.created_at, '%Y-%m') = ?
            GROUP BY
            ct.merchant_id;
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId, date('Y-m', $dateOfFilter))
        );

        if ($query) {
            $total = ($query[0]->totalAmount / $query[0]->uniqueCustomer);
        }

        return $total;
    }

    /**
     * AVERAGE PTS BURNED PER CUSTOMER
     * TOTAL POINTS BURNED (MONTH)/TOTAL NUMBER OF UNIQUE CUSTOMER (MONTH) with redeemed/burned transactions
     * @param  int $merchantId
     * @return int
     */
    public function getAveragePointsBurnedPerCustomer($merchantId, $dateOfFilter)
    {
        $total = 0;
        $sql =
        "   SELECT
            SUM(amount) totalAmount
            , COUNT(distinct ct.customer_id) uniqueCustomer
            FROM
            CustomerPunchCardTransactions ct
            WHERE
            ct.type IN ('paypoints', 'redeem')
            AND ct.merchant_id = ?
            AND DATE_FORMAT(ct.created_at, '%Y-%m') = ?
            GROUP BY
            ct.merchant_id;
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId, date('Y-m', $dateOfFilter))
        );

        if ($query) {
            $total = ($query[0]->totalAmount / $query[0]->uniqueCustomer);
        }

        return $total;
    }

    public function getChannelsData($merchantId)
    {
        $sql =
        "   SELECT
            SUM(if(c.registrationChannel IN ('sms'), 1, 0)) AS sms
            , SUM(if(c.registrationChannel IN ('Merchant App'), 1, 0)) AS merchantApp
            , SUM(if(c.registrationChannel IN ('Customer App'), 1, 0)) AS customerApp
            , SUM(if(c.registrationChannel IN ('Customer Web'), 1, 0)) AS customerWeb
            , SUM(if(c.registrationChannel IN ('Card'), 1, 0)) AS card
            FROM Customer c
            WHERE
            c.merchantId = ?
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId)
        );

        return $query[0];
    }

    public function getTransactionChannelData($merchantId, $filterMonth, $previousFilterMonth)
    {
        $sql =
        "   SELECT
            cpt.channel
            , SUM(if(cpt.type IN ('earn'), 1, 0)) AS earn
            , SUM(if(cpt.type IN ('redeem'), 1, 0)) AS redeem
            , SUM(if(DATE_FORMAT(cpt.date_punched, '%Y-%m') = ?, 1, 0)) AS previousMonth
            , SUM(if(DATE_FORMAT(cpt.date_punched, '%Y-%m') = ?, 1, 0)) AS givenMonth
            FROM
            CustomerPunchCardTransactions cpt
            WHERE
            cpt.merchant_id = ?
            GROUP BY cpt.channel
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            [   $previousFilterMonth,
                $filterMonth,
                $merchantId]
        );

        return $query;
    }

    public function getTransactionValuePeriodData($merchantId, $givenMonth)
    {
        $sql =
        "   SELECT
            cpt.type as type
            , SUM(if(WEEKDAY(cpt.date_punched) = 0, cpt.amount, 0)) AS monday
            , SUM(if(WEEKDAY(cpt.date_punched) = 1, cpt.amount, 0)) AS tuesday
            , SUM(if(WEEKDAY(cpt.date_punched) = 2, cpt.amount, 0)) AS wednesday
            , SUM(if(WEEKDAY(cpt.date_punched) = 3, cpt.amount, 0)) AS thursday
            , SUM(if(WEEKDAY(cpt.date_punched) = 4, cpt.amount, 0)) AS friday
            , SUM(if(WEEKDAY(cpt.date_punched) = 5, cpt.amount, 0)) AS saturday
            , SUM(if(WEEKDAY(cpt.date_punched) = 6, cpt.amount, 0)) AS sunday
            FROM
            CustomerPunchCardTransactions cpt
            WHERE
            cpt.merchant_id = ?
            AND DATE_FORMAT(cpt.date_punched, '%Y-%m') = ?
            GROUP BY cpt.type
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            [   $merchantId,
                $givenMonth]
        );

        return $query;
    }

    public function getTransactionFrequencyPeriodData($merchantId, $givenMonth)
    {
        $sql =
        "   SELECT
            cpt.type as type
            , SUM(if(WEEKDAY(cpt.date_punched) = 0, 1, 0)) AS monday
            , SUM(if(WEEKDAY(cpt.date_punched) = 1, 1, 0)) AS tuesday
            , SUM(if(WEEKDAY(cpt.date_punched) = 2, 1, 0)) AS wednesday
            , SUM(if(WEEKDAY(cpt.date_punched) = 3, 1, 0)) AS thursday
            , SUM(if(WEEKDAY(cpt.date_punched) = 4, 1, 0)) AS friday
            , SUM(if(WEEKDAY(cpt.date_punched) = 5, 1, 0)) AS saturday
            , SUM(if(WEEKDAY(cpt.date_punched) = 6, 1, 0)) AS sunday
            FROM
            CustomerPunchCardTransactions cpt
            WHERE
            cpt.merchant_id = ?
            AND DATE_FORMAT(cpt.date_punched, '%Y-%m') = ?
            GROUP BY cpt.type
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            [   $merchantId,
                $givenMonth]
        );

        return $query;
    }

    public function getAppDownloadData($merchantId, $platforms)
    {
        $firstDayOfMonth = date('Y-m-d', strtotime('first day of this month'));
        $lastDayOfMonth = date('Y-m-d', strtotime('last day of this month'));
        $currentDay = $firstDayOfMonth;
        $weekPeriods = [
            'week1' => [
                'start' => $currentDay,
                'end' => $currentDay = date('Y-m-d',  strtotime($currentDay. ' + 6 days'))
            ],
            'week2' => [
                'start' => $currentDay = date('Y-m-d', strtotime($currentDay. ' + 1 days')),
                'end' => $currentDay = date('Y-m-d', strtotime($currentDay. ' + 6 days'))
            ],
            'week3' => [
                'start' => $currentDay = date('Y-m-d', strtotime($currentDay. ' + 1 days')),
                'end' => $currentDay = date('Y-m-d', strtotime($currentDay. ' + 6 days'))
            ],
            'week4' => [
                'start' => $currentDay = date('Y-m-d', strtotime($currentDay. ' + 1 days')),
                'end' => $lastDayOfMonth
            ]
        ];

        $platformSql = '';
        foreach($platforms AS $platform => $platformTags) {
            foreach ($platformTags as $platformTag) {
                $platformSql .= "WHEN '$platformTag' THEN '$platform'".PHP_EOL;
            }
        }
        $platformSql .= 'END as platform'.PHP_EOL;

        $sql =
        "   SELECT
            CASE d.platform
                $platformSql
            , SUM(if(DATE_FORMAT(d.created_at, '%Y-%m-%d') BETWEEN ? AND ?, 1, 0)) AS week1
            , SUM(if(DATE_FORMAT(d.created_at, '%Y-%m-%d') BETWEEN ? AND ?, 1, 0)) AS week2
            , SUM(if(DATE_FORMAT(d.created_at, '%Y-%m-%d') BETWEEN ? AND ?, 1, 0)) AS week3
            , SUM(if(DATE_FORMAT(d.created_at, '%Y-%m-%d') BETWEEN ? AND ?, 1, 0)) AS week4
            FROM
            lara_customer_devices d
            WHERE
            id IN (
                SELECT MAX(id) FROM lara_customer_devices
                WHERE
                customer_id IN (
                    SELECT customerId FROM Customer WHERE merchantId = ?
                )
                GROUP BY token
            )
            AND DATE_FORMAT(d.created_at, '%Y-%m') = ?
            GROUP BY platform
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            [   $weekPeriods['week1']['start'], $weekPeriods['week1']['end'],
                $weekPeriods['week2']['start'], $weekPeriods['week2']['end'],
                $weekPeriods['week3']['start'], $weekPeriods['week3']['end'],
                $weekPeriods['week4']['start'], $weekPeriods['week4']['end'],
                $merchantId,
                date('Y-m', time())]
        );

        return $query;
    }

    public function getAppDownloadDataLifetime($merchantId, $platforms)
    {
        $platformSql = '';
        foreach($platforms AS $platform => $platformTags) {
            foreach ($platformTags as $platformTag) {
                $platformSql .= "WHEN '$platformTag' THEN '$platform'".PHP_EOL;
            }
        }
        $platformSql .= 'END as platform'.PHP_EOL;

        $sql =
        "   SELECT
            platform
            , SUM(tally) AS total
            FROM (
                SELECT
                CASE d.platform
                    $platformSql
                , 1 AS tally
                FROM
                lara_customer_devices d
                WHERE
                id IN (
                    SELECT MAX(id) FROM lara_customer_devices
                    WHERE
                    customer_id IN (
                        SELECT customerId FROM Customer WHERE merchantId = ?
                    )
                    GROUP BY token
                )
            ) s
            GROUP BY s.platform
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            [$merchantId]
        );

        return $query;
    }

    /**
     * Definitions:
     * Return in the past month - with transcation from past month and the current month
     * New this month - new members with transactions
     * Has not returned - no transactions in past month and current month
     * @param  int $merchantId
     */
    public function getCustomerMonthlyTransactionData($merchantId)
    {
        $customerMonthLyTransactionData = [
            'returnInThePastMonth' => 0,
            'newThisMonth' => 0,
            'hasNotReturned' => 0
        ];

        $prevMonth = date("Y-m", strtotime("previous month"));
        $currMonth = date("Y-m", strtotime("this month"));

        $sql =
        "   SELECT
            SUM(if(pct.newReg = 0 AND pct.previousMonthTrans > 0 AND pct.currentMonthTrans > 0, 1, 0)) AS returnInThePastMonth
            , SUM(if(pct.newReg > 0 AND pct.currentMonthTrans > 0, 1, 0)) AS newThisMonth
            , SUM(if(pct.newReg = 0 AND pct.previousMonthTrans = 0 AND pct.currentMonthTrans = 0, 1, 0)) AS hasNotReturned
            FROM (
                SELECT
                c.customerId
                , SUM(if(DATE_FORMAT(ct.created_at, '%Y-%m') = ?, 1, 0)) AS previousMonthTrans
                , SUM(if(DATE_FORMAT(ct.created_at, '%Y-%m') = ?, 1, 0)) AS currentMonthTrans
                , SUM(if(DATE_FORMAT(c.timestamp, '%Y-%m') = ?, 1, 0)) AS newReg
                FROM
                Customer c
                LEFT JOIN CustomerPunchCardTransactions ct
                    ON (c.customerId = ct.customer_id && DATE_FORMAT(ct.created_at, '%Y-%m') BETWEEN ? AND ?)
                WHERE
                c.merchantId = ?
                GROUP BY c.customerId
            ) as pct
        ";
        $query = DB::select(
            DB::raw($sql),
            [   $prevMonth,
                $currMonth,
                $currMonth,
                $prevMonth,
                $currMonth,
                $merchantId]
        );

        if ($query) {
            $customerMonthLyTransactionData = array_merge($customerMonthLyTransactionData, json_decode(json_encode($query[0]), true));
        }

        return $customerMonthLyTransactionData;
    }

    public function getVisitsData($merchantId)
    {
        $sql =
        "   SELECT
            IF(DATE_FORMAT(visits.regDate, '%Y-%m') = ?, 'new', 'recurring') as visitType
            , SUM(if(WEEKDAY(visits.transDate) = 0, 1, 0)) AS monday
            , SUM(if(WEEKDAY(visits.transDate) = 1, 1, 0)) AS tuesday
            , SUM(if(WEEKDAY(visits.transDate) = 2, 1, 0)) AS wednesday
            , SUM(if(WEEKDAY(visits.transDate) = 3, 1, 0)) AS thursday
            , SUM(if(WEEKDAY(visits.transDate) = 4, 1, 0)) AS friday
            , SUM(if(WEEKDAY(visits.transDate) = 5, 1, 0)) AS saturday
            , SUM(if(WEEKDAY(visits.transDate) = 6, 1, 0)) AS sunday
            FROM
            (   SELECT
                DATE_FORMAT(c.timestamp, '%Y-%m-%d') regDate
                , DATE_FORMAT(ct.created_at, '%Y-%m-%d') transDate
                , c.customerId customerId
                FROM
                CustomerPunchCardTransactions ct
                LEFT JOIN Customer c
                    ON ct.customer_id = c.customerId
                WHERE
                ct.merchant_id = ?
                AND DATE_FORMAT(ct.created_at, '%Y-%m') = ?
                GROUP BY c.customerId, DATE_FORMAT(ct.created_at, '%Y-%m-%d')
                ORDER BY ct.created_at DESC
            ) visits
            GROUP BY visitType, visits.customerId, DATE_FORMAT(visits.transDate, '%Y-%m-%d')
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            [   date('Y-m', time()),
                $merchantId,
                date('Y-m', time())]
        );

        return $query;
    }

    /**
     * TODO: create migration to change gender datatype to char(1) and limit values to (null, f, m)
     * @param  int $merchantId
     */
    public function getProfileData($merchantId)
    {
        $sql =
        "   SELECT
            IF(c.gender = 'm', 'male', 'unknown') as profileType
            , SUM(if(TIMESTAMPDIFF(YEAR, c.birthDate, CURDATE()) IS NULL OR TIMESTAMPDIFF(YEAR, c.birthDate, CURDATE()) BETWEEN 0 AND 17, 1, 0)) AS notSet
            , SUM(if(TIMESTAMPDIFF(YEAR, c.birthDate, CURDATE()) BETWEEN 18 AND 24, 1, 0)) AS age18_24
            , SUM(if(TIMESTAMPDIFF(YEAR, c.birthDate, CURDATE()) BETWEEN 25 AND 34, 1, 0)) AS age25_34
            , SUM(if(TIMESTAMPDIFF(YEAR, c.birthDate, CURDATE()) BETWEEN 35 AND 44, 1, 0)) AS age35_44
            , SUM(if(TIMESTAMPDIFF(YEAR, c.birthDate, CURDATE()) BETWEEN 45 AND 54, 1, 0)) AS age45_n
            FROM
            Customer c
            WHERE
            c.merchantId = ?
            GROUP BY profileType
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql), [$merchantId]
        );

        return $query;
    }
}
