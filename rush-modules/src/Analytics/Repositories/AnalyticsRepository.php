<?php

namespace Rush\Modules\Analytics\Repositories;

use \stdClass;
use DB;
use Illuminate\Database\Eloquent\Model;
use Rush\Modules\Analytics\Models\AnalyticsModel;
use Rush\Modules\Analytics\Repositories\AnalyticsInterface;
use Rush\Modules\Repository\AbstractRepository;

/**
 * A repository class would encapsulate each Eloquent Model class
 * and be responsible for CRUD operations on the database.
 */
class AnalyticsRepository extends AbstractRepository implements AnalyticsInterface
{
    protected $connection = 'mysql';

    public function __construct(AnalyticsModel $analyticsModel)
    {
        parent::__construct($analyticsModel);
    }

    /**
     * get members count
     * @param  int $merchantId
     * @return int
     */
    public function getMembersCount($merchantId)
    {
        $count = 0;
        $sql =
        "   SELECT
            COUNT(*) AS count
            FROM Customer c
            WHERE
            c.merchantid = ?
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId)
        );
        $count = $query[0]->count;

        return $count;
    }

    /**
     * get members count for the current month
     * @param  int $merchantId
     * @return int
     */
    public function getNewMembersThisMonthCount($merchantId, $dateOfFilter)
    {
        $count = 0;
        $sql =
        "   SELECT
            COUNT(*) AS count
            FROM Customer c
            WHERE
            c.merchantid = ?
            AND  DATE_FORMAT(c.timestamp, '%Y-%m') = ?
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql), [
               $merchantId,
                date('Y-m', $dateOfFilter)
            ]
        );
        $count = $query[0]->count;

        return $count;
    }

    public function getTransactionTotal($merchantId)
    {
        $total = 0;
        $sql =
        "   SELECT
            SUM(ct.amountPaidWithCash) AS total
            FROM
            CustomerTransaction ct
            LEFT JOIN Customer c ON ct.customerId = c.customerId
            WHERE
            ct.merchant_id = ?
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId)
        );
        $total = $query[0]->total;

        return $total;
    }

    public function getPointsSeededTotal($merchantId)
    {
        $total = 0;
        $sql =
        "   SELECT
            SUM(ct.pointsEarned) AS total
            FROM
            CustomerTransaction ct
            LEFT JOIN Customer c ON ct.customerId = c.customerId
            WHERE
            ct.transactionType = 'earn'
            AND ct.merchant_id = ?
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId)
        );
        $total = $query[0]->total;

        return $total;
    }

    public function getPointsUsedTotal($merchantId)
    {
        $total = 0;
        $sql =
        "   SELECT
            SUM(ct.amountPaidWithPoints) AS total
            FROM
            CustomerTransaction ct
            LEFT JOIN Customer c ON ct.customerId = c.customerId
            WHERE
            ct.transactionType IN ('paypoints', 'redeem')
            AND ct.merchant_id = ?
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId)
        );
        $total = $query[0]->total;

        return $total;
    }

    public function getTopCustomers($merchantId)
    {
        $count = 0;
        $sql =
        "   SELECT
            c.fullName
            , COUNT(DISTINCT t.visit) AS totalVisit
            , SUM(t.pointsEarned) AS totalPointsEarned
            FROM
            (   SELECT
                ct.customerId
                , DATE_FORMAT(ct.timestamp, '%Y-%m-%d') AS visit
                , IF(ct.transactionType = 'earn', ct.pointsEarned, 0) AS pointsEarned
                FROM
                CustomerTransaction ct
                WHERE
                ct.merchant_id = ?
            ) AS t
            LEFT JOIN Customer c
                ON t.customerId = c.customerId
            GROUP BY t.customerId
            ORDER BY totalPointsEarned DESC
            LIMIT 20
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId)
        );

        return $query;
    }

    public function getTopBranches($merchantId)
    {
        $count = 0;
        $sql =
        "   SELECT
            b.branchName
            , SUM(t.earning) AS totalEarning
            , SUM(t.redemption) AS totalRedemption
            FROM
            (   SELECT
                ct.branchId
                , IF(ct.transactionType = 'earn', 1, 0) AS earning
                , IF(ct.transactionType IN ('paypoints', 'redeem'), 1, 0) AS redemption
                FROM
                CustomerTransaction ct
                WHERE
                ct.branchId > 0
                AND ct.merchant_id = ?
            ) AS t
            LEFT JOIN Branches b
                ON t.branchId = b.id
            GROUP BY t.branchId
            ORDER BY totalEarning DESC
            LIMIT 20
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId)
        );

        return $query;
    }

    /**
     * AVE. VISITS PER CUSTOMER
     * daily unique member trans, sum within the month
     * @param  int $merchantId
     * @return int
     */
    public function getAverageVisitsPerCustomer($merchantId, $dateOfFilter)
    {
        $count = 0;
        $sql =
        "   SELECT
            SUM(monthly.dailyUnique) AS count
            FROM
            (   SELECT
                DATE_FORMAT(ct.timestamp, '%Y-%m-%d') AS day
                , COUNT(DISTINCT ct.customerId) AS dailyUnique
                FROM
                CustomerTransaction ct
                WHERE
                ct.merchant_id = ?
                AND NOT (ct.branchId IS NULL OR ct.branchId = 0)
                AND ct.transactionType IN ('earn', 'redeem', 'paypoints')
                AND DATE_FORMAT(ct.timestamp, '%Y-%m') = ?
                GROUP BY day
            ) monthly

        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql), [
                $merchantId,
                date('Y-m', $dateOfFilter)
            ]
        );
        $count = $query[0]->count;

        return $count;
    }

    public function getTopRedeemedRewards($merchantId)
    {
        $count = 0;
        $sql =
        "   SELECT
            ifr.name
            , SUM(t.redemption) AS totalRedemption
            FROM
            (   SELECT
                ri.redeemItemId
                , IF(ct.transactionType = 'redeem', 1, 0) AS redemption
                FROM
                CustomerTransaction ct
                LEFT JOIN Redeem r
                    ON ( ct.transactionReferenceCode = r.redeemReferenceCode and r.status = 1 )
                LEFT JOIN redeemedItems ri
                    ON r.redeemId = ri.redeemId
                WHERE
                ct.branchId > 0
                AND ct.merchant_id = ?
            ) AS t
            LEFT JOIN ItemsForRedeem ifr
                ON t.redeemItemId = ifr.redeemItemId
            GROUP BY t.redeemItemId
            ORDER BY totalRedemption DESC
            LIMIT 20
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId)
        );

        return $query;
    }

    /**
     * TOTAL TXNS THIS MONTH (PHP)
     * is equal to transaction amount in Earned transaction (month)
     * @param  int $merchantId
     * @return int
     */
    public function getMonthTransactionAmountTotal($merchantId, $dateOfFilter)
    {
        $total = 0;
        $sql =
        "   SELECT
            SUM(ct.amountPaidWithCash) total
            FROM
            CustomerTransaction ct
            WHERE
            ct.transactionType = 'earn'
            AND ct.merchant_id = ?
            AND DATE_FORMAT(ct.timestamp, '%Y-%m') = ?
            GROUP BY
            ct.merchant_id
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId, date('Y-m', $dateOfFilter))
        );
        $total = $query ? $query[0]->total : 0;

        return $total;
    }

    /**
     * AVG REVENUE PER CUSTOMER (PHP)
     * TOTAL TXNS AMOUNT (MONTH)/TOTAL NUMBER OF UNIQUE CUSTOMER (MONTH) with earned transactions /4 weeks
     * @param  int $merchantId
     * @return int
     */
    public function getAverageRevenuePerCustomer($merchantId, $dateOfFilter)
    {
        $total = 0;
        $sql =
        "   SELECT
            SUM(ct.amountPaidWithCash) totalAmount
            , COUNT(distinct ct.customerId) uniqueCustomer
            FROM
            CustomerTransaction ct
            WHERE
            ct.transactionType = 'earn'
            AND ct.merchant_id = ?
            AND DATE_FORMAT(ct.timestamp, '%Y-%m') = ?
            GROUP BY
            ct.merchant_id;
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId, date('Y-m', $dateOfFilter))
        );

        if ($query) {
            $total = ($query[0]->totalAmount / $query[0]->uniqueCustomer) / 4;
        }

        return $total;
    }

    /**
     * AVERAGE PTS EARNED PER CUSTOMER
     * TOTAL POINTS EARNED (MONTH)/TOTAL NUMBER OF UNIQUE CUSTOMER (MONTH) with earned transactions
     * @param  int $merchantId
     * @return int
     */
    public function getAveragePointsEarnedPerCustomer($merchantId, $dateOfFilter)
    {
        $total = 0;
        $sql =
        "   SELECT
            SUM(ct.pointsEarned) totalAmount
            , COUNT(distinct ct.customerId) uniqueCustomer
            FROM
            CustomerTransaction ct
            WHERE
            ct.transactionType = 'earn'
            AND ct.merchant_id = ?
            AND DATE_FORMAT(ct.timestamp, '%Y-%m') = ?
            GROUP BY
            ct.merchant_id;
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId, date('Y-m', $dateOfFilter))
        );

        if ($query) {
            $total = ($query[0]->totalAmount / $query[0]->uniqueCustomer);
        }

        return $total;
    }

    /**
     * AVERAGE PTS BURNED PER CUSTOMER
     * TOTAL POINTS BURNED (MONTH)/TOTAL NUMBER OF UNIQUE CUSTOMER (MONTH) with redeemed/burned transactions
     * @param  int $merchantId
     * @return int
     */
    public function getAveragePointsBurnedPerCustomer($merchantId, $dateOfFilter)
    {
        $total = 0;
        $sql =
        "   SELECT
            SUM(ct.pointsRedeem + ct.pointsBurn) totalAmount
            , COUNT(distinct ct.customerId) uniqueCustomer
            FROM
            CustomerTransaction ct
            WHERE
            ct.transactionType IN ('paypoints', 'redeem')
            AND ct.merchant_id = ?
            AND DATE_FORMAT(ct.timestamp, '%Y-%m') = ?
            GROUP BY
            ct.merchant_id;
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId, date('Y-m', $dateOfFilter))
        );

        if ($query) {
            $total = ($query[0]->totalAmount / $query[0]->uniqueCustomer);
        }

        return $total;
    }

    public function getChannelsData($merchantId)
    {
        $sql =
        "   SELECT
            SUM(if(c.registrationChannel IN ('sms'), 1, 0)) AS sms
            , SUM(if(c.registrationChannel IN ('Merchant App'), 1, 0)) AS merchantApp
            , SUM(if(c.registrationChannel IN ('Customer App'), 1, 0)) AS customerApp
            , SUM(if(c.registrationChannel IN ('Customer Web'), 1, 0)) AS customerWeb
            , SUM(if(c.registrationChannel IN ('Card'), 1, 0)) AS card
            FROM Customer c
            WHERE
            c.merchantId = ?
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            array($merchantId)
        );

        return $query[0];
    }

    public function getTransactionChannelData($merchantId, $filterMonth, $previousFilterMonth)
    {
        $sql =
        "   SELECT
            ct.channel
            , SUM(if(ct.transactionType IN ('earn'), 1, 0)) AS earn
            , SUM(if(ct.transactionType IN ('paypoints'), 1, 0)) AS paypoints
            , SUM(if(ct.transactionType IN ('redeem'), 1, 0)) AS redeem
            , SUM(if(DATE_FORMAT(ct.timestamp, '%Y-%m') = ?, 1, 0)) AS previousFilterMonth
            , SUM(if(DATE_FORMAT(ct.timestamp, '%Y-%m') = ?, 1, 0)) AS filterMonth
            FROM
            CustomerTransaction ct
            WHERE
            ct.merchant_id = ?
            GROUP BY ct.channel
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            [   $previousFilterMonth,
                $filterMonth,
                $merchantId]
        );

        return $query;
    }

    public function getTransactionValuePeriodData($merchantId, $givenMonth)
    {
        $sql =
        "   SELECT
            ct.transactionType as transactionType
            , SUM(if(WEEKDAY(ct.timestamp) = 0, ct.amountPaidWithCash + ct.amountPaidWithPoints + ct.pointsRedeem, 0)) AS monday
            , SUM(if(WEEKDAY(ct.timestamp) = 1, ct.amountPaidWithCash + ct.amountPaidWithPoints + ct.pointsRedeem, 0)) AS tuesday
            , SUM(if(WEEKDAY(ct.timestamp) = 2, ct.amountPaidWithCash + ct.amountPaidWithPoints + ct.pointsRedeem, 0)) AS wednesday
            , SUM(if(WEEKDAY(ct.timestamp) = 3, ct.amountPaidWithCash + ct.amountPaidWithPoints + ct.pointsRedeem, 0)) AS thursday
            , SUM(if(WEEKDAY(ct.timestamp) = 4, ct.amountPaidWithCash + ct.amountPaidWithPoints + ct.pointsRedeem, 0)) AS friday
            , SUM(if(WEEKDAY(ct.timestamp) = 5, ct.amountPaidWithCash + ct.amountPaidWithPoints + ct.pointsRedeem, 0)) AS saturday
            , SUM(if(WEEKDAY(ct.timestamp) = 6, ct.amountPaidWithCash + ct.amountPaidWithPoints + ct.pointsRedeem, 0)) AS sunday
            FROM
            CustomerTransaction ct
            WHERE
            ct.merchant_id = ?
            AND DATE_FORMAT(ct.timestamp, '%Y-%m') = ?
            GROUP BY ct.transactionType
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            [   $merchantId,
                $givenMonth]
        );

        return $query;
    }

    public function getTransactionFrequencyPeriodData($merchantId, $givenMonth)
    {
        $sql =
        "   SELECT
            ct.transactionType as transactionType
            , SUM(if(WEEKDAY(ct.timestamp) = 0, 1, 0)) AS monday
            , SUM(if(WEEKDAY(ct.timestamp) = 1, 1, 0)) AS tuesday
            , SUM(if(WEEKDAY(ct.timestamp) = 2, 1, 0)) AS wednesday
            , SUM(if(WEEKDAY(ct.timestamp) = 3, 1, 0)) AS thursday
            , SUM(if(WEEKDAY(ct.timestamp) = 4, 1, 0)) AS friday
            , SUM(if(WEEKDAY(ct.timestamp) = 5, 1, 0)) AS saturday
            , SUM(if(WEEKDAY(ct.timestamp) = 6, 1, 0)) AS sunday
            FROM
            CustomerTransaction ct
            WHERE
            ct.merchant_id = ?
            AND DATE_FORMAT(ct.timestamp, '%Y-%m') = ?
            GROUP BY ct.transactionType
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            [   $merchantId,
                $givenMonth]
        );

        return $query;
    }

    public function getAppDownloadData($merchantId, $platforms)
    {
        $firstDayOfMonth = date('Y-m-d', strtotime('first day of this month'));
        $lastDayOfMonth = date('Y-m-d', strtotime('last day of this month'));
        $currentDay = $firstDayOfMonth;
        $weekPeriods = [
            'week1' => [
                'start' => $currentDay,
                'end' => $currentDay = date('Y-m-d',  strtotime($currentDay. ' + 6 days'))
            ],
            'week2' => [
                'start' => $currentDay = date('Y-m-d', strtotime($currentDay. ' + 1 days')),
                'end' => $currentDay = date('Y-m-d', strtotime($currentDay. ' + 6 days'))
            ],
            'week3' => [
                'start' => $currentDay = date('Y-m-d', strtotime($currentDay. ' + 1 days')),
                'end' => $currentDay = date('Y-m-d', strtotime($currentDay. ' + 6 days'))
            ],
            'week4' => [
                'start' => $currentDay = date('Y-m-d', strtotime($currentDay. ' + 1 days')),
                'end' => $lastDayOfMonth
            ]
        ];

        $platformSql = '';
        foreach($platforms AS $platform => $platformTags) {
            foreach ($platformTags as $platformTag) {
                $platformSql .= "WHEN '$platformTag' THEN '$platform'".PHP_EOL;
            }
        }
        $platformSql .= 'END as platform'.PHP_EOL;

        $sql =
        "   SELECT
            CASE d.platform
                $platformSql
            , SUM(if(DATE_FORMAT(d.created_at, '%Y-%m-%d') BETWEEN ? AND ?, 1, 0)) AS week1
            , SUM(if(DATE_FORMAT(d.created_at, '%Y-%m-%d') BETWEEN ? AND ?, 1, 0)) AS week2
            , SUM(if(DATE_FORMAT(d.created_at, '%Y-%m-%d') BETWEEN ? AND ?, 1, 0)) AS week3
            , SUM(if(DATE_FORMAT(d.created_at, '%Y-%m-%d') BETWEEN ? AND ?, 1, 0)) AS week4
            FROM
            lara_customer_devices d
            WHERE
            id IN (
                SELECT MAX(id) FROM lara_customer_devices
                WHERE
                customer_id IN (
                    SELECT customerId FROM Customer WHERE merchantId = ?
                )
                GROUP BY token
            )
            AND DATE_FORMAT(d.created_at, '%Y-%m') = ?
            GROUP BY platform
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            [   $weekPeriods['week1']['start'], $weekPeriods['week1']['end'],
                $weekPeriods['week2']['start'], $weekPeriods['week2']['end'],
                $weekPeriods['week3']['start'], $weekPeriods['week3']['end'],
                $weekPeriods['week4']['start'], $weekPeriods['week4']['end'],
                $merchantId,
                date('Y-m', time())]
        );

        return $query;
    }

    public function getAppDownloadDataLifetime($merchantId, $platforms)
    {
        $platformSql = '';
        foreach($platforms AS $platform => $platformTags) {
            foreach ($platformTags as $platformTag) {
                $platformSql .= "WHEN '$platformTag' THEN '$platform'".PHP_EOL;
            }
        }
        $platformSql .= 'END as platform'.PHP_EOL;

        $sql =
        "   SELECT
            platform
            , SUM(tally) AS total
            FROM (
                SELECT
                CASE d.platform
                    $platformSql
                , 1 AS tally
                FROM
                lara_customer_devices d
                WHERE
                id IN (
                    SELECT MAX(id) FROM lara_customer_devices
                    WHERE
                    customer_id IN (
                        SELECT customerId FROM Customer WHERE merchantId = ?
                    )
                    GROUP BY token
                )
            ) s
            GROUP BY s.platform
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            [$merchantId]
        );

        return $query;
    }

    /**
     * Definitions:
     * Return in the past month - with transcation from past month and the current month
     * New this month - new members with transactions
     * Has not returned - no transactions in past month and current month
     * @param  int $merchantId
     */
    public function getCustomerMonthlyTransactionData($merchantId)
    {
        $customerMonthLyTransactionData = [
            'returnInThePastMonth' => 0,
            'newThisMonth' => 0,
            'hasNotReturned' => 0
        ];

        $prevMonth = date("Y-m", strtotime("previous month"));
        $currMonth = date("Y-m", strtotime("this month"));

        $sql =
        "   SELECT
            SUM(if(pct.newReg = 0 AND pct.previousMonthTrans > 0 AND pct.currentMonthTrans > 0, 1, 0)) AS returnInThePastMonth
            , SUM(if(pct.newReg > 0 AND pct.currentMonthTrans > 0, 1, 0)) AS newThisMonth
            , SUM(if(pct.newReg = 0 AND pct.previousMonthTrans = 0 AND pct.currentMonthTrans = 0, 1, 0)) AS hasNotReturned
            FROM (
                SELECT
                c.customerId
                , SUM(if(DATE_FORMAT(ct.timestamp, '%Y-%m') = ?, 1, 0)) AS previousMonthTrans
                , SUM(if(DATE_FORMAT(ct.timestamp, '%Y-%m') = ?, 1, 0)) AS currentMonthTrans
                , SUM(if(DATE_FORMAT(c.timestamp, '%Y-%m') = ?, 1, 0)) AS newReg
                FROM
                Customer c
                LEFT JOIN CustomerTransaction ct
                    ON (c.customerId = ct.customerId && DATE_FORMAT(ct.timestamp, '%Y-%m') BETWEEN ? AND ?)
                WHERE
                c.merchantId = ?
                GROUP BY c.customerId
            ) as pct
        ";
        $query = DB::select(
            DB::raw($sql),
            [   $prevMonth,
                $currMonth,
                $currMonth,
                $prevMonth,
                $currMonth,
                $merchantId]
        );

        if ($query) {
            $customerMonthLyTransactionData = array_merge($customerMonthLyTransactionData, json_decode(json_encode($query[0]), true));
        }

        return $customerMonthLyTransactionData;
    }

    public function getVisitsData($merchantId)
    {
        $sql =
        "   SELECT
            IF(DATE_FORMAT(visits.regDate, '%Y-%m') = ?, 'new', 'recurring') as visitType
            , SUM(if(WEEKDAY(visits.transDate) = 0, 1, 0)) AS monday
            , SUM(if(WEEKDAY(visits.transDate) = 1, 1, 0)) AS tuesday
            , SUM(if(WEEKDAY(visits.transDate) = 2, 1, 0)) AS wednesday
            , SUM(if(WEEKDAY(visits.transDate) = 3, 1, 0)) AS thursday
            , SUM(if(WEEKDAY(visits.transDate) = 4, 1, 0)) AS friday
            , SUM(if(WEEKDAY(visits.transDate) = 5, 1, 0)) AS saturday
            , SUM(if(WEEKDAY(visits.transDate) = 6, 1, 0)) AS sunday
            FROM
            (   SELECT
                DATE_FORMAT(c.timestamp, '%Y-%m-%d') regDate
                , DATE_FORMAT(ct.timestamp, '%Y-%m-%d') transDate
                , c.customerId customerId
                FROM
                CustomerTransaction ct
                LEFT JOIN Customer c
                    ON ct.customerId = c.customerId
                WHERE
                ct.merchant_id = ?
                AND DATE_FORMAT(ct.timestamp, '%Y-%m') = ?
                GROUP BY c.customerId, DATE_FORMAT(ct.timestamp, '%Y-%m-%d')
                ORDER BY ct.timestamp DESC
            ) visits
            GROUP BY visitType, visits.customerId, DATE_FORMAT(visits.transDate, '%Y-%m-%d')
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql),
            [   date('Y-m', time()),
                $merchantId,
                date('Y-m', time())]
        );

        return $query;
    }

    /**
     * TODO: create migration to change gender datatype to char(1) and limit values to (null, f, m)
     * @param  int $merchantId
     */
    public function getProfileData($merchantId)
    {
        $sql =
        "   SELECT
            IF(c.gender = 'm', 'male', 'unknown') as profileType
            , SUM(if(TIMESTAMPDIFF(YEAR, c.birthDate, CURDATE()) IS NULL OR TIMESTAMPDIFF(YEAR, c.birthDate, CURDATE()) BETWEEN 0 AND 17, 1, 0)) AS notSet
            , SUM(if(TIMESTAMPDIFF(YEAR, c.birthDate, CURDATE()) BETWEEN 18 AND 24, 1, 0)) AS age18_24
            , SUM(if(TIMESTAMPDIFF(YEAR, c.birthDate, CURDATE()) BETWEEN 25 AND 34, 1, 0)) AS age25_34
            , SUM(if(TIMESTAMPDIFF(YEAR, c.birthDate, CURDATE()) BETWEEN 35 AND 44, 1, 0)) AS age35_44
            , SUM(if(TIMESTAMPDIFF(YEAR, c.birthDate, CURDATE()) BETWEEN 45 AND 54, 1, 0)) AS age45_n
            FROM
            Customer c
            WHERE
            c.merchantId = ?
            GROUP BY profileType
        ";
        $query = DB::connection($this->connection)->select(
            DB::raw($sql), [$merchantId]
        );

        return $query;
    }
}
