<?php

namespace Rush\Modules\Analytics\Providers;

use Illuminate\Container\Container;
use Illuminate\Support\ServiceProvider;
use Rush\Modules\Analytics\Models\PunchcardAnalyticsModel;
use Rush\Modules\Analytics\Repositories\PunchcardAnalyticsRepository;

/**
* Register our Repository with Laravel
*/
class PunchcardAnalyticsRepositoryServiceProvider extends ServiceProvider
{
    /**
    * Registers the AnalyticsInterface with Laravels IoC Container
    */
    public function register()
    {
        // Bind the returned class to the namespace 'Repositories\PunchcardAnalyticsInterface
        $this->app->bind(
            '\Rush\Modules\Analytics\Repositories\PunchcardAnalyticsInterface',
            function($app) {
                return new PunchcardAnalyticsRepository(new PunchcardAnalyticsModel());
            }
        );
    }
}
