<?php

namespace Rush\Modules\Analytics\Providers;

use Illuminate\Container\Container;
use Illuminate\Support\ServiceProvider;
use Rush\Modules\Analytics\Models\AnalyticsModel;
use Rush\Modules\Analytics\Repositories\AnalyticsRepository;

/**
* Register our Repository with Laravel
*/
class AnalyticsRepositoryServiceProvider extends ServiceProvider
{
    /**
    * Registers the AnalyticsInterface with Laravels IoC Container
    */
    public function register()
    {
        // Bind the returned class to the namespace 'Repositories\AnalyticsInterface
        $this->app->bind(
            '\Rush\Modules\Analytics\Repositories\AnalyticsInterface',
            function($app) {
                return new AnalyticsRepository(new AnalyticsModel());
            }
        );
    }
}
