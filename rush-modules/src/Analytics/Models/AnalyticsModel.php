<?php

namespace Rush\Modules\Analytics\Models;

use Eloquent;

class AnalyticsModel extends Eloquent
{
    // TODO: build analytics table for caching analytics
    // so we dont need to perform expensive query everytime Analytics page is accessed
}