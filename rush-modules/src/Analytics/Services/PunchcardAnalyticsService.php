<?php

namespace Rush\Modules\Analytics\Services;

use Rush\Modules\Analytics\Repositories\PunchcardAnalyticsInterface;
use Rush\Modules\Push\PushHelper;

/**
* Our AnalyticsService, containing all useful methods for business logic around Analytics
* Service just hands off data it was given by the controller to the appropriate repositories
*/
class PunchcardAnalyticsService
{
    /**
     * @var Rush\Modules\Analytics\Repositories\PunchcardAnalyticsRepository
     */
    protected $punchcardAnalyticsRepository;

    protected $type = ['earn', 'redeem'];
    protected $adjacentMonth = ['givenMonth', 'previousMonth'];
    protected $periods = ['previousMonth', 'givenMonth'];
    protected $platforms = ['ios', 'android', 'active'];
    protected $visitTypes = ['new', 'recurring'];
    protected $profileTypes = ['male', 'unknown'];
    public $profileAgeBrackets = ['notSet', 'age18_24', 'age25_34', 'age35_44', 'age45_n'];
    protected $days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
    protected $weeks = ['week1', 'week2', 'week3', 'week4'];
    public $transactionChannels = ['sms', 'merchantapp', 'customerapp', 'customerweb', 'card'];

    public function __construct(PunchcardAnalyticsInterface $punchcardAnalyticsRepository)
    {
        $this->punchcardAnalyticsRepository = $punchcardAnalyticsRepository;
    }

    public function getMembersCount($merchantId)
    {
        return $this->punchcardAnalyticsRepository->getMembersCount($merchantId);
    }

    public function getNewMembersThisMonthCount($merchantId, $dateOfFilter)
    {
        return $this->punchcardAnalyticsRepository->getNewMembersThisMonthCount($merchantId, $dateOfFilter);
    }

    public function getTransactionTotal($merchantId)
    {
        return $this->punchcardAnalyticsRepository->getTransactionTotal($merchantId);
    }

    public function getPointsSeededTotal($merchantId)
    {
        return $this->punchcardAnalyticsRepository->getPointsSeededTotal($merchantId);
    }

    public function getPointsUsedTotal($merchantId)
    {
        return $this->punchcardAnalyticsRepository->getPointsUsedTotal($merchantId);
    }

    public function getTopCustomers($merchantId)
    {
        return $this->punchcardAnalyticsRepository->getTopCustomers($merchantId);
    }

    public function getTopBranches($merchantId)
    {
        return $this->punchcardAnalyticsRepository->getTopBranches($merchantId);
    }

    public function getTopRedeemedRewards($merchantId)
    {
        return $this->punchcardAnalyticsRepository->getTopRedeemedRewards($merchantId);
    }

    public function getMonthTransactionAmountTotal($merchantId, $dateOfFilter)
    {
        return $this->punchcardAnalyticsRepository->getMonthTransactionAmountTotal($merchantId, $dateOfFilter);
    }

    public function getAverageRevenuePerCustomer($merchantId, $dateOfFilter)
    {
        return $this->punchcardAnalyticsRepository->getAverageRevenuePerCustomer($merchantId, $dateOfFilter);
    }

    public function getAveragePointsEarnedPerCustomer($merchantId, $dateOfFilter)
    {
        return $this->punchcardAnalyticsRepository->getAveragePointsEarnedPerCustomer($merchantId, $dateOfFilter);
    }

    public function getAveragePointsBurnedPerCustomer($merchantId, $dateOfFilter)
    {
        return $this->punchcardAnalyticsRepository->getAveragePointsBurnedPerCustomer($merchantId, $dateOfFilter);
    }

    public function getAverageVisitsPerCustomer($merchantId, $dateOfFilter)
    {
        return $this->punchcardAnalyticsRepository->getAverageVisitsPerCustomer($merchantId, $dateOfFilter);
    }

    public function getChannelsData($merchantId)
    {
        return $this->punchcardAnalyticsRepository->getChannelsData($merchantId);
    }

    public function getTransactionChannelData($merchantId, $dateOfFilter)
    {
        $filterMonth = date('Y-m', $dateOfFilter);
        $previousFilterMonth = $this->getPreviousMonth($dateOfFilter);
        $transactionTypes = array_merge($this->type, $this->adjacentMonth);

        $transactionChannelData = [];
        foreach($this->transactionChannels AS $transactionChannel) {
            $transactionChannelData[$transactionChannel] = array_fill_keys($transactionTypes, 0);
        }
        $transactionChannelDataResult = $this->punchcardAnalyticsRepository->getTransactionChannelData($merchantId, $filterMonth, $previousFilterMonth);
        foreach($transactionChannelDataResult AS $value) {
            foreach($transactionTypes AS $transactionType) {
                $transactionChannelData[$value->channel][$transactionType] = $value->$transactionType;
            }
        }
        // invert the array
        $finalTransactionChannelData = [];
        foreach ($transactionTypes AS $transactionType) {
            foreach($this->transactionChannels AS $transactionChannel) {
                $finalTransactionChannelData[$transactionType][$transactionChannel] = $transactionChannelData[$transactionChannel][$transactionType];
            }
        }
        foreach ($finalTransactionChannelData AS $key => $value) {
            $finalTransactionChannelData[$key] = implode(', ', $value);
        }

        return $finalTransactionChannelData;
    }

    public function getPreviousWeekStart()
    {
        return date('Y-m-d', strtotime('last Monday', strtotime('monday this week')));
    }

    public function getPreviousWeekEnd()
    {
        return date('Y-m-d', strtotime('last Monday +6 day', strtotime('monday this week')));
    }

    public function getThisWeekStart()
    {
        return date('Y-m-d', strtotime('monday this week'));
    }

    public function getThisWeekEnd()
    {
        return date('Y-m-d', strtotime('monday this week +6 day'));
    }

    public function getTransactionBaseArray()
    {
        $baseArray = [];
        foreach($this->type AS $transactionType) {
            foreach ($this->periods as $period) {
                $baseArray[$transactionType][$period] = '0, 0, 0, 0, 0, 0, 0';
            }
        }

        return $baseArray;
    }

    public function getPreviousMonth($givenMonth){
        return date('Y-m', strtotime('-1 months', $givenMonth));
    }

    public function getTransactionValueData($merchantId, $dateOfFilter)
    {
        $transactionValueData = $this->getTransactionBaseArray();
        $filterMonth = date('Y-m', $dateOfFilter);
        $previousFilterMonth = $this->getPreviousMonth($dateOfFilter);

        $previousFilterMonthValueData = $this->punchcardAnalyticsRepository->getTransactionValuePeriodData($merchantId, $previousFilterMonth);
        $filterMonthValueData = $this->punchcardAnalyticsRepository->getTransactionValuePeriodData($merchantId, $filterMonth);

        foreach ($previousFilterMonthValueData as $value) {
            $transactionValueData[$value->type]['previousMonth'] = "$value->monday, $value->tuesday, $value->wednesday, $value->thursday, $value->friday, $value->saturday, $value->sunday";
        }
        foreach ($filterMonthValueData as $value) {
            $transactionValueData[$value->type]['givenMonth'] = "$value->monday, $value->tuesday, $value->wednesday, $value->thursday, $value->friday, $value->saturday, $value->sunday";
        }

        return $transactionValueData;
    }

    public function getTransactionFrequencyData($merchantId, $dateOfFilter)
    {
        $transactionFrequencyData = $this->getTransactionBaseArray();
        $filterMonth = date('Y-m', $dateOfFilter);
        $previousFilterMonth = $this->getPreviousMonth($dateOfFilter);

        $previousFilterMonthFrequencyData = $this->punchcardAnalyticsRepository->getTransactionFrequencyPeriodData($merchantId, $previousFilterMonth);
        $filterMonthFrequencyData = $this->punchcardAnalyticsRepository->getTransactionFrequencyPeriodData($merchantId, $filterMonth);

        foreach ($previousFilterMonthFrequencyData as $value) {
            $transactionFrequencyData[$value->type]['previousMonth'] = "$value->monday, $value->tuesday, $value->wednesday, $value->thursday, $value->friday, $value->saturday, $value->sunday";
        }
        foreach ($filterMonthFrequencyData as $value) {
            $transactionFrequencyData[$value->type]['givenMonth'] = "$value->monday, $value->tuesday, $value->wednesday, $value->thursday, $value->friday, $value->saturday, $value->sunday";
        }

        return $transactionFrequencyData;
    }

    public function getAppDownloadData($merchantId)
    {
        $appDownloadData = [];
        foreach($this->platforms AS $platform) {
            $appDownloadData[$platform] = array_fill(0, 4, 0);
        }
        $totalActive = array_fill(0, 4, 0);

        $platforms = PushHelper::getPlatformTags();

        $appDownloadDataResult = $this->punchcardAnalyticsRepository->getAppDownloadData($merchantId, $platforms);
        foreach ($appDownloadDataResult as $value) {
            foreach($this->weeks AS $weekInt => $weekName) {
                $appDownloadData[$value->platform][$weekInt] = $value->$weekName;
                $totalActive[$weekInt] += $value->$weekName;
            }
        }
        $appDownloadData['active'] = $totalActive;
        foreach($this->platforms AS $platform) {
            $appDownloadData[$platform] = implode(', ', $appDownloadData[$platform]);
        }

        return $appDownloadData;
    }

    public function getAppDownloadDataLifetime($merchantId)
    {
        $platforms = PushHelper::getPlatformTags();
        $appDownloadDataLifetimeResult = $this->punchcardAnalyticsRepository->getAppDownloadDataLifetime($merchantId, $platforms);
        $appDownloadDataLifetime = array_fill_keys(PushHelper::getPlatforms(), 0);
        $both = 0;
        foreach($appDownloadDataLifetimeResult AS $value) {
            $appDownloadDataLifetime[$value->platform] = $value->total;
            $both += $value->total;
        }
        $appDownloadDataLifetime[PushHelper::PLATFORM_BOTH] = $both;

        return $appDownloadDataLifetime;
    }

    public function getCustomerMonthlyTransactionData($merchantId)
    {
        return $this->punchcardAnalyticsRepository->getCustomerMonthlyTransactionData($merchantId);
    }

    public function getVisitsData($merchantId)
    {
        $visitsData = [];
        foreach ($this->visitTypes AS $visitType) {
            $visitsData[$visitType] = array_fill(0, 7, 0);
        }
        $totalVisits = array_fill(0, 7, 0);

        $visitsDataResult = $this->punchcardAnalyticsRepository->getVisitsData($merchantId);
        foreach ($visitsDataResult as $value) {
            foreach($this->days AS $dayInt => $dayName) {
                $visitsData[$value->visitType][$dayInt] += $value->$dayName;
                $totalVisits[$dayInt] += $value->$dayName;
            }
        }
        foreach ($this->visitTypes AS $visitType) {
            $visitsData[$visitType]= implode(', ', $visitsData[$visitType]);
        }
        $visitsData['total'] = implode(', ', $totalVisits);

        return $visitsData;
    }

    public function getProfileData($merchantId)
    {
        $profileData = [];
        foreach ($this->profileTypes AS $profileType) {
            $profileData[$profileType] = array_fill_keys($this->profileAgeBrackets, 0);
        }
        $totalProfile = array_fill_keys($this->profileAgeBrackets, 0);

        $profileDataResult = $this->punchcardAnalyticsRepository->getProfileData($merchantId);

        foreach ($profileDataResult as $value) {
            foreach($this->profileAgeBrackets AS $profileAgeBracket) {
                $profileData[$value->profileType][$profileAgeBracket] = $value->$profileAgeBracket;
                $totalProfile[$profileAgeBracket] += $value->$profileAgeBracket;
            }
        }
        $profileData['total'] = $totalProfile;

        return $profileData;
    }

    public function getControllerViewData($merchantId, $request)
    {
        $dateOfFilter = ($request->analyticsDate) ? strtotime($request->analyticsDate) : time();
        
        return [
            'analytics.punchcard',
            [   'login_name' => $request->session()->get('login_name'),
                'quickstart' => 'punchcard',
                'module' => 'analytics',
                'header_text' => 'Analytics',
                'current_date' => date('F j, Y', time()),
                'current_month_year' => date('F Y', time()),
                'menu' => 'menu.punchcard',
                'transactionTotal' => $this->getTransactionTotal($merchantId),
                'pointsSeededTotal' => $this->getPointsSeededTotal($merchantId),
                'pointsUsedTotal' => $this->getPointsUsedTotal($merchantId),
                'monthTransactionAmountTotal' => $this->getMonthTransactionAmountTotal($merchantId, $dateOfFilter),
                'averageRevenuePerCustomer' => $this->getAverageRevenuePerCustomer($merchantId, $dateOfFilter),
                'averagePointsEarnedPerCustomer' => $this->getAveragePointsEarnedPerCustomer($merchantId, $dateOfFilter),
                'averagePointsBurnedPerCustomer' => $this->getAveragePointsBurnedPerCustomer($merchantId, $dateOfFilter),
                'membersCount' => $this->getMembersCount($merchantId),
                'newMembersThisMonthCount' => $this->getNewMembersThisMonthCount($merchantId, $dateOfFilter),
                'topRedeemedRewards' => $this->getTopRedeemedRewards($merchantId),
                'topCustomers' => $this->getTopCustomers($merchantId),
                'topBranches' => $this->getTopBranches($merchantId),
                'averageVisitsPerCustomer' => $this->getAverageVisitsPerCustomer($merchantId, $dateOfFilter),
                'channelsData' => $this->getChannelsData($merchantId),
                'transactionChannelData' => $this->getTransactionChannelData($merchantId, $dateOfFilter),
                'transactionValueData' => $this->getTransactionValueData($merchantId, $dateOfFilter),
                'transactionFrequencyData' => $this->getTransactionFrequencyData($merchantId, $dateOfFilter),
                'appDownloadData' => $this->getAppDownloadData($merchantId),
                'appDownloadDataLifetime' => $this->getAppDownloadDataLifetime($merchantId),
                'customerMonthlyTransactionData' => $this->getCustomerMonthlyTransactionData($merchantId),
                'visitsData' => $this->getVisitsData($merchantId),
                'profileData' => $this->getProfileData($merchantId),
                'profileAgeBrackets' => $this->profileAgeBrackets,
                'dateOfFilter' => date('Y-m', $dateOfFilter)
            ]
        ];
    }
}
