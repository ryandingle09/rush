<?php
namespace Rush\Modules\OnboardingCms\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\OnboardingCms\Repositories\OnboardingCmsInterface;
use Rush\Modules\OnboardingCms\Repositories\OnboardingCmsRepository;

class OnboardingCmsRepositoryServiceProvider extends ServiceProvider
{
	public function register()
	{
		$this->app->singleton(OnboardingCmsInterface::class, OnboardingCmsRepository::class);
	}
}
