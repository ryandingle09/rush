<?php
namespace Rush\Modules\OnboardingCms\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Rush\Modules\Helpers\Facades\StorageHelper;
use Rush\Modules\OnboardingCms\Repositories\OnboardingCmsInterface;

class OnboardingCmsService
{
    protected $onboardingCmsRepository;

    public function __construct(OnboardingCmsInterface $onboardingCmsRepository)
    {
        $this->onboardingCmsRepository = $onboardingCmsRepository;
    }

    public function checkExistingLogin($username, $password)
    {
        return $this->onboardingCmsRepository->checkExistingLogin( $username, $password );
    }

    public function getModules($id)
    {
        return $this->onboardingCmsRepository->getModules( $id );
    }

    public function getAllNews()
    {
        return $this->onboardingCmsRepository->getAllNews();
    }

    public function getNews($id)
    {
        return $this->onboardingCmsRepository->getNews($id);
    }

    public function createNews($data)
    {
        return $this->onboardingCmsRepository->createNews( $data );
    }

    public function updateNews($data)
    {
        return $this->onboardingCmsRepository->updateNews( $data );
    }

    public function newsVisibility($data)
    {
        return $this->onboardingCmsRepository->newsVisibility( $data );
    }

    public function deleteNews($data)
    {
        return $this->onboardingCmsRepository->deleteNews( $data );
    }

    public function getAllClientLogos()
    {
        return $this->onboardingCmsRepository->getAllClientLogos();
    }

    public function getClientLogo($id)
    {
        return $this->onboardingCmsRepository->getClientLogo($id);
    }

    public function clientLogosVisibility($data)
    {
        return $this->onboardingCmsRepository->clientLogosVisibility($data);
    }

    public function createClientLogo($data)
    {
        return $this->onboardingCmsRepository->createClientLogo($data);
    }

    public function editClientLogo($data)
    {
        return $this->onboardingCmsRepository->editClientLogo($data);
    }

    public function deleteClientLogo($data)
    {
        return $this->onboardingCmsRepository->deleteClientLogo($data);
    }

    public function getAllOnboardingNews()
    {
        return $this->onboardingCmsRepository->getAllOnboardingNews();
    }

    public function getNewsbySlug($slug)
    {
        return $this->onboardingCmsRepository->getNewsbySlug($slug);
    }

    public function getAllOnboardingNewsExceptSlug($slug)
    {
        return $this->onboardingCmsRepository->getAllOnboardingNewsExceptSlug($slug);
    }

    public function getAllOnboardingClientLogos()
    {
        return $this->onboardingCmsRepository->getAllOnboardingClientLogos();
    }

}