<?php
namespace Rush\Modules\OnboardingCms\Repositories;

use Rush\Modules\OnboardingCms\Models\OnboardingCmsUserModel;
use Rush\Modules\OnboardingCms\Models\OnboardingCmsPermissionsModel;
use Rush\Modules\OnboardingCms\Models\OnboardingCmsNewsModel;
use Rush\Modules\OnboardingCms\Models\OnboardingCmsClientLogosModel;
use Rush\Modules\Repository\AbstractRepository;

class OnboardingCmsRepository extends AbstractRepository implements OnboardingCmsInterface
{
    protected $onboardingCmsUser;
    protected $onboardingCmsPermissions;
    protected $onboardingCmsNews;
    protected $onboardingCmsClientLogos;

    public function __construct(OnboardingCmsUserModel $onboardingCmsUser, OnboardingCmsPermissionsModel $onboardingCmsPermissions, OnboardingCmsNewsModel $onboardingCmsNews, OnboardingCmsClientLogosModel $onboardingCmsClientLogos)
    {
        $this->onboardingCmsUser = $onboardingCmsUser;
        $this->onboardingCmsPermissions = $onboardingCmsPermissions;
        $this->onboardingCmsNews = $onboardingCmsNews;
        $this->onboardingCmsClientLogos = $onboardingCmsClientLogos;
    }

    public function checkExistingLogin($username, $password)
    {
        $user = $this->onboardingCmsUser->where([ 'email' => $username, 'password' => hash('sha512', $password), 'enabled' => true ]);
        return ( $user->count() > 0 ?  $user->first() : false );
    }

    public function getModules($id)
    {
        $modules = $this->onboardingCmsPermissions->whereIn('id', $id)->where([ 'type' => 'module']);
        return ( $modules->count() > 0 ?  $modules->get() : false );   
    }

    public function getAllNews()
    {
        return $this->onboardingCmsNews->all();
    }

    public function getNews($id)
    {
        return $this->onboardingCmsNews->find($id);
    }

    public function createNews($data)
    {
        $news = new $this->onboardingCmsNews();
        $news->title = $data['title'];
        $news->url = str_slug( $data['title'] );
        $news->merchant_name = $data['merchant_name'];
        $news->description = $data['description'];
        $news->background = $data['background'];
        $news->background_image = $data['background_image'];
        $news->logo = $data['logo'];
        $news->app_image_top = $data['app_image_top'];
        $news->android_link = $data['android_link'];
        $news->ios_link = $data['ios_link'];
        $news->content = $data['content'];
        $news->app_name = $data['app_name'];
        $news->banner_image = $data['banner_image'];
        $news->mobile_app_content = $data['mobile_app_content'];
        $news->app_image1 = $data['app_image1'];
        $news->app_image2 = $data['app_image2'];
        $news->app_image3 = $data['app_image3'];
        $news->app_image1_label = $data['app_image1_label'];
        $news->app_image2_label = $data['app_image2_label'];
        $news->app_image3_label = $data['app_image3_label'];
        $news->publish = $data['publish'];
        $news->publish_date = $data['publish_date'];
        $news->visibility = $data['visibility'];
        $news->created_by = $data['user_id'];
        return $news->save();
    }

    public function newsVisibility($data)
    {
        $news = $this->onboardingCmsNews->find($data['id']);
        $news->visibility = $data['visibility'];
        $news->save();
        return $news->visibility;
    }

    public function updateNews($data)
    {
        $news = $this->onboardingCmsNews->find($data['id']);
        $news->title = $data['title'];
        $news->url = str_slug( $data['title'] );
        $news->merchant_name = $data['merchant_name'];
        $news->description = $data['description'];
        $news->background = $data['background'];
        $news->background_image = $data['background_image'];
        $news->logo = $data['logo'];
        $news->app_image_top = $data['app_image_top'];
        $news->android_link = $data['android_link'];
        $news->ios_link = $data['ios_link'];
        $news->content = $data['content'];
        $news->app_name = $data['app_name'];
        $news->banner_image = $data['banner_image'];
        $news->mobile_app_content = $data['mobile_app_content'];
        $news->app_image1 = $data['app_image1'];
        $news->app_image2 = $data['app_image2'];
        $news->app_image3 = $data['app_image3'];
        $news->app_image1_label = $data['app_image1_label'];
        $news->app_image2_label = $data['app_image2_label'];
        $news->app_image3_label = $data['app_image3_label'];
        $news->publish = $data['publish'];
        $news->publish_date = $data['publish_date'];
        $news->visibility = $data['visibility'];
        $news->created_by = $data['user_id'];
        return $news->save();
    }

    public function deleteNews( $data )
    {
        $news = $this->onboardingCmsNews->find($data['id']); 
        return $news->delete();
    }

    public function getAllClientLogos()
    {
        return $this->onboardingCmsClientLogos->all();
    }

    public function getClientLogo($id)
    {
        return $this->onboardingCmsClientLogos->find($id);
    }

    public function clientLogosVisibility($data)
    {
        $logo = $this->onboardingCmsClientLogos->find($data['id']);
        $logo->visibility = $data['visibility'];
        $logo->save();
        return $logo->visibility;
    }

    public function createClientLogo($data)
    {
        $news = new $this->onboardingCmsClientLogos();
        $news->client_name = $data['client_name'];
        $news->logo = $data['logo'];
        $news->visibility = true;
        $news->created_by = $data['user_id'];
        return $news->save();
    }

    public function editClientLogo($data)
    {
        $news = $this->onboardingCmsClientLogos->find($data['id']);
        $news->client_name = $data['client_name'];
        $news->logo = $data['logo'];
        $news->visibility = $data['visibility'];
        $news->created_by = $data['user_id'];
        return $news->save();
    }

    public function deleteClientLogo( $data )
    {
        $news = $this->onboardingCmsClientLogos->find($data['id']); 
        return $news->delete();
    }

    public function getAllOnboardingNews()
    {
        
        return $this->onboardingCmsNews->where(['visibility' => true, 'publish' => true])->orderBy('publish_date','DESC')->get();
    
    }

    public function getNewsbySlug($slug)
    {
        return $this->onboardingCmsNews->where(['url' => $slug, 'visibility' => true, 'publish' => true])->first();   
    }

    public function getAllOnboardingNewsExceptSlug($slug)
    {
        return $this->onboardingCmsNews->where('url','<>',$slug)->where(['visibility' => true, 'publish' => true])->orderBy('publish_date','DESC')->get();   
    }

    public function getAllOnboardingClientLogos()
    {
        return $this->onboardingCmsClientLogos->where(['visibility' => true])->get();
    }
}
