<?php
namespace Rush\Modules\OnboardingCms\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OnboardingCmsUserTypeModel extends Model
{
	use SoftDeletes;

	protected $table = "onboarding_cms_user_type";

	protected $primaryKey = "id";
}
