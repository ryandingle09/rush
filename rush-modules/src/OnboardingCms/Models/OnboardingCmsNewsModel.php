<?php
namespace Rush\Modules\OnboardingCms\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class OnboardingCmsNewsModel extends Model
{
	use SoftDeletes;

	protected $table = "onboarding_cms_news";

	protected $primaryKey = "id";

	public function getPublishLabelAttribute()
	{
		return ($this->publish) ? 'Published' : 'Draft';
	}

	public function getPublishDateLabelAttribute()
	{
		return ($this->publish) ? Carbon::parse($this->publish_date)->format('M d, Y') : '-';
	}
}
