<?php
namespace Rush\Modules\OnboardingCms\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OnboardingCmsUserModel extends Model
{
	use SoftDeletes;

	protected $table = "onboarding_cms_user";

	protected $primaryKey = "id";

	public function type()
	{
        return $this->hasOne('Rush\Modules\OnboardingCms\Models\OnboardingCmsUserTypeModel', 'id', 'user_type');
    }
}
