<?php
namespace Rush\Modules\OnboardingCms\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OnboardingCmsPermissionsModel extends Model
{
	use SoftDeletes;

	protected $table = "onboarding_cms_permissions";

	protected $primaryKey = "id";
}
