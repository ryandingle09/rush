<?php
namespace Rush\Modules\OnboardingCms\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class OnboardingCmsClientLogosModel extends Model
{
	use SoftDeletes;

	protected $table = "onboarding_cms_clientlogos";

	protected $primaryKey = "id";

	public function getCreatedAtLabelAttribute()
	{
		return ($this->created_at) ? Carbon::parse($this->created_at)->format('M d, Y') : '-';
	}
}
