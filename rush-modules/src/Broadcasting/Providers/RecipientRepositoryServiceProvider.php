<?php

namespace Rush\Modules\Broadcasting\Providers;

use Illuminate\Container\Container;
use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;
use Rush\Modules\Broadcasting\Models\RecipientModel;
use Rush\Modules\Broadcasting\Repositories\RecipientRepository;

/**
* Register our Repository with Laravel
*/
class RecipientRepositoryServiceProvider extends ServiceProvider
{
    /**
    * Registers the RecipientInterface with Laravels IoC Container
    */
    public function register()
    {
        // Bind the returned class to the namespace 'Repositories\RecipientInterface
        $this->app->bind(
            '\Rush\Modules\Broadcasting\Repositories\RecipientInterface',
            function($app) {
                return new RecipientRepository(new RecipientModel(), new Collection());
            }
        );
    }
}
