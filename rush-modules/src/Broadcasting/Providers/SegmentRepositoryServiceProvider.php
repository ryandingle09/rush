<?php

namespace Rush\Modules\Broadcasting\Providers;

use Illuminate\Container\Container;
use Illuminate\Support\ServiceProvider;
use Rush\Modules\Broadcasting\Models\SegmentModel;
use Rush\Modules\Broadcasting\Repositories\SegmentRepository;

/**
* Register our Repository with Laravel
*/
class SegmentRepositoryServiceProvider extends ServiceProvider
{
    /**
    * Registers the SegmentInterface with Laravels IoC Container
    */
    public function register()
    {
        // Bind the returned class to the namespace 'Repositories\SegmentInterface
        $this->app->bind(
            '\Rush\Modules\Broadcasting\Repositories\SegmentInterface',
            function($app) {
                return new SegmentRepository(new SegmentModel());
            }
        );
    }
}
