<?php

namespace Rush\Modules\Broadcasting\Providers;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\ServiceProvider;

class BroadcastingServiceProvider extends ServiceProvider
{
    protected $commands = [
        'Rush\Modules\Broadcasting\Console\Commands\ScheduleCheckCommand',
    ];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('BroadcastHelper', 'Rush\Modules\Broadcasting\BroadcastHelper');

        // schedule commands internally when the app has booted
        $this->app->booted(function () {
            $schedule = $this->app->make(Schedule::class);
            $schedule->command('broadcasting:schedule-check')->everyMinute();
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands($this->commands);
    }
}
