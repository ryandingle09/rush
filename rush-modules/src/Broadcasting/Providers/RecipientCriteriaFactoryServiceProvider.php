<?php

namespace Rush\Modules\Broadcasting\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Broadcasting\Criteria\Recipient\RecipientCriteriaFactory;

class RecipientCriteriaFactoryServiceProvider extends ServiceProvider
{

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(RecipientCriteriaFactory::class);
    }
}
