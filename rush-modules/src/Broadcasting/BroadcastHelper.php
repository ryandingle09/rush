<?php

namespace Rush\Modules\Broadcasting;

use Rush\Modules\Helpers\AbstractProcessingHelper;

final class BroadcastHelper extends AbstractProcessingHelper
{
    private function __construct(){
        throw new Exception("BroadcastHelper not instantiable");
    }
}