<?php

namespace Rush\Modules\Broadcasting\Contracts;

use Rush\Modules\Broadcasting\Models\MessageModel;

/**
 * The BroadcasterInterface
 */
interface BroadcasterInterface
{
    /**
     * MessageModel shall serve as the parent type for messages like Sms, Email, Push
     * @param  MessageModel $messageModel
     */
    public function send(MessageModel $messageModel);
}
