<?php

namespace Rush\Modules\Broadcasting\Contracts;

/**
 * The BroadcastingInterface
 */
interface BroadcastingInterface
{
    public function getScheduledItems();
}
