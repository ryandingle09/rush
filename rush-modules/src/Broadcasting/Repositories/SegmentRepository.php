<?php

namespace Rush\Modules\Broadcasting\Repositories;

use Illuminate\Database\Eloquent\Model;
use Rush\Modules\Broadcasting\Models\SegmentModel;
use Rush\Modules\Broadcasting\Repositories\SegmentInterface;
use Rush\Modules\Repository\AbstractRepository;
use \stdClass;

/**
 * A repository class would encapsulate each Eloquent Model class
 * and be responsible for CRUD operations on the database.
 */
class SegmentRepository extends AbstractRepository implements SegmentInterface
{
    public function __construct(SegmentModel $segmentModel)
    {
        parent::__construct($segmentModel);
    }

    public function getSegmentByScopeString($merchant_id, $string){
        return $this->model->where('merchant_id', $merchant_id)
            ->where('scopes', 'like', '%' . $string . '%')->get();
    }
}
