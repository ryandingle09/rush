<?php

namespace Rush\Modules\Broadcasting\Repositories;

use Rush\Modules\Repository\RepositoryInterface;

/**
 * The SegmentInterface contains unique method signatures related to Segment object
 */
interface SegmentInterface extends RepositoryInterface
{
}
