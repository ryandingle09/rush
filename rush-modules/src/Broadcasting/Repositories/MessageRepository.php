<?php

namespace Rush\Modules\Broadcasting\Repositories;

use Illuminate\Support\Facades\DB;
use Rush\Modules\Broadcasting\BroadcastHelper;
use Rush\Modules\Broadcasting\Models\MessageModel;
use Rush\Modules\Broadcasting\Repositories\MessageInterface;
use Rush\Modules\Repository\AbstractRepository;

/**
 * A repository class would encapsulate each Eloquent Model class
 * and be responsible for CRUD operations on the database.
 */
class MessageRepository extends AbstractRepository implements MessageInterface
{
    public function __construct(MessageModel $messageModel)
    {
        parent::__construct($messageModel);
    }

    public function getScheduledItems()
    {
        $schedules = $this->model
            ->where('status', BroadcastHelper::INACTIVE)
            ->where('publish_date', '<=', date('Y-m-d H:i:s'))
            ->get();

        return $schedules;
    }

    public function allWithSegment($merchantId)
    {
        // cant seem to do it via eloquent
        /*return $this->model
            ->where('merchant_id', $merchantId)
            ->with(['segment' => function($query){
                $query->select('name');
            }])
            ->get();*/
        $messageTable = $this->model->getTable();
        $segmentTable =  $this->model->segment()->getRelated()->getTable();

        return (object) DB::table($messageTable)
            ->select("$messageTable.*", "$segmentTable.name AS segment_name")
            ->leftJoin($segmentTable, "$segmentTable.id", '=', "$messageTable.segment_id")
            ->where("$messageTable.merchant_id", $merchantId)
            ->get();
    }

    public function getBySegmentId($segment_id){
        $messageTable = $this->model->getTable();
        $segmentTable =  $this->model->segment()->getRelated()->getTable();

        return (object) DB::table($messageTable)
            ->select("$messageTable.*", "$segmentTable.name AS segment_name")
            ->leftJoin($segmentTable, "$segmentTable.id", '=', "$messageTable.segment_id")
            ->where("$messageTable.segment_id", $segment_id)
            ->get();
    }
}
