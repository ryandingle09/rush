<?php

namespace Rush\Modules\Broadcasting\Repositories;

use Rush\Modules\Repository\RepositoryInterface;

/**
 * The MessageInterface contains unique method signatures related to Message object
 */
interface MessageInterface extends RepositoryInterface
{
    public function getScheduledItems();

    public function allWithSegment($merchantId);
}
