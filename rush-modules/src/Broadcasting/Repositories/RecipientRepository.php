<?php

namespace Rush\Modules\Broadcasting\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Rush\Modules\Broadcasting\Models\RecipientModel;
use Rush\Modules\Broadcasting\Repositories\RecipientInterface;
use Rush\Modules\Merchant\Models\MerchantModel;
use Rush\Modules\Repository\AbstractRepository;
use Rush\Modules\Repository\Criteria;
use Rush\Modules\Repository\CriteriaInterface;
use \stdClass;

/**
 * A repository class would encapsulate each Eloquent Model class
 * and be responsible for CRUD operations on the database.
 */
class RecipientRepository extends AbstractRepository implements RecipientInterface, CriteriaInterface
{
    /**
     * @var Collection
     */
    protected $criteria;

    /**
     * @var Rush\Modules\Merchant\Models\MerchantModel
     */
    protected $merchant;

    public function __construct(RecipientModel $recipientModel, Collection $collection)
    {
        parent::__construct($recipientModel);
        $this->criteria = $collection;
    }

    public function setMerchant(MerchantModel $merchant)
    {
        $this->merchant = $merchant;
    }

    public function getMerchant()
    {
        return $this->merchant;
    }

    /**
     * @return mixed
     */
    public function getCriteria()
    {
        return $this->criteria;
    }

    /**
     * @param Criteria $criteria
     * @return $this
     */
    public function pushCriteria(Criteria $criteria)
    {
        // Find existing criteria
        $key = $this->criteria->search(function ($item) use ($criteria) {
            return (is_object($item) && (get_class($item) == get_class($criteria)));
        });

        // Remove old criteria
        if (is_int($key)) {
            $this->criteria->offsetUnset($key);
        }

        $this->criteria->push($criteria);
        return $this;
    }

    /**
     * @return $this
     */
    public function applyCriteria()
    {
        foreach ($this->getCriteria() as $criteria) {
            if ($criteria instanceof Criteria)
                $this->model = $criteria->apply($this->model, $this);
        }

        return $this;
    }

    /**
     * override parents all method
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all()
    {
        $this->applyCriteria();
        return $this->model->get();
    }

    /**
     * Use inner join and aggregate the result to get unique customer device tokens
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getCustomerDevices()
    {
        $recipientTable = $this->model->getTable();
        $recipientDeviceTable = $this->model->devices()->getRelated()->getTable();

        $this->applyCriteria();
        $latestDevices =  $this->model
            ->join($recipientDeviceTable, function ($join) use ($recipientTable, $recipientDeviceTable) {
                $join->on("$recipientTable.customerId", '=', "$recipientDeviceTable.customer_id");
            })
            ->groupBy("$recipientDeviceTable.device_id")
            ->select(DB::raw("MAX($recipientDeviceTable.id) AS id"))
            ->pluck("$recipientDeviceTable.id");

        //MTODO consider incresing max_allowed_packet when the query length becomes too long
        return DB::table($recipientDeviceTable)
            ->select('id', 'platform', 'token', 'device_id')
            ->whereIn('id', $latestDevices)
            ->get();
    }
}
