<?php

namespace Rush\Modules\Broadcasting\Repositories;

use Rush\Modules\Merchant\Models\MerchantModel;
use Rush\Modules\Repository\RepositoryInterface;

/**
 * The RecipientInterface contains unique method signatures related to Recipient object
 */
interface RecipientInterface extends RepositoryInterface
{
    public function setMerchant(MerchantModel $merchant);

    public function getMerchant();
}
