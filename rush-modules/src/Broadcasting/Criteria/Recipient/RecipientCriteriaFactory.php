<?php

namespace Rush\Modules\Broadcasting\Criteria\Recipient;

class RecipientCriteriaFactory
{
    /**
     * @var array
     */
    protected $typeList;

    public function __construct()
    {
        $this->typeList = [
            'age' => __NAMESPACE__.'\Age',
            'birthMonth' => __NAMESPACE__.'\BirthMonth',
            'gender' => __NAMESPACE__.'\Gender',
            'merchant' => __NAMESPACE__.'\Merchant',
            'firstCheckIn' => __NAMESPACE__.'\BreakOutLimit',
            'noClientCode' => __NAMESPACE__.'\NoClientCode',
            'prospect' => __NAMESPACE__.'\Prospect',
            'topCustomer' => __NAMESPACE__.'\TopCustomer',
            'newMember' => __NAMESPACE__.'\NewMember',
            'noPreviousMonthTransaction' => __NAMESPACE__.'\NoPreviousMonthTransaction',
            'branchTransaction' => __NAMESPACE__.'\BranchTransaction',
        ];
    }

    public function createCriteria($type, $params)
    {
        if (!array_key_exists($type, $this->typeList)) {
            throw new \InvalidArgumentException("$type is not a valid Recipient Criteria");
        }
        $className = $this->typeList[$type];

        return new $className($params);
    }
}