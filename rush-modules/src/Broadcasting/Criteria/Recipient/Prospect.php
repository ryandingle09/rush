<?php

namespace Rush\Modules\Broadcasting\Criteria\Recipient;

use Rush\Modules\Repository\RepositoryInterface as Repository;
use Rush\Modules\Repository\Criteria;

class Prospect extends Criteria
{
    /**
     * @param $model
     * @param RecipientInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $model = $model->where('membership_status', '=', '0');
        return $model;
    }
}