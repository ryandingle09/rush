<?php

namespace Rush\Modules\Broadcasting\Criteria\Recipient;

use Illuminate\Support\Facades\DB;
use Rush\Modules\Repository\Criteria;
use Rush\Modules\Repository\RepositoryInterface as Repository;

class TopCustomer extends Criteria
{
    /**
     * @param $model
     * @param RecipientInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        // reward currency type is points
        if ($repository->getMerchant()->isPro()) {
            $model = $model
                ->from('Customer as c')
                ->join('CustomerTransaction as ct', function ($join) {
                    $join->on('c.customerId', '=', 'ct.customerId')
                    ->where('ct.transactionType', '=', 'earn');
                })
                ->groupBy('c.customerId')
                ->orderByRaw('SUM(ct.pointsEarned) desc')
                ->limit($this->params);
        } else {
            // reward currency type is stamps
            $model = $model
                ->from('Customer as c')
                ->join('CustomerPunchCardTransactions as cpt', function ($join) {
                    $join->on('c.customerId', '=', 'cpt.customer_id')
                    ->where('cpt.type', '=', 'earn')
                    ->where('cpt.void', '=', '0');
                })
                ->groupBy('c.customerId')
                ->orderByRaw('SUM(cpt.amount) desc')
                ->limit($this->params);
        }

        return $model;
    }
}