<?php

namespace Rush\Modules\Broadcasting\Criteria\Recipient;

use Rush\Modules\Repository\RepositoryInterface as Repository;
use Rush\Modules\Repository\Criteria;

class NoClientCode extends Criteria
{
    /**
     * @param $model
     * @param RecipientInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $model = $model->where(function ($query) {
            $query->whereNull('client_code')
            ->orWhere('client_code', '=', '');
        });

        return $model;
    }
}