<?php

namespace Rush\Modules\Broadcasting\Criteria\Recipient;

use Rush\Modules\Repository\RepositoryInterface as Repository;
use Rush\Modules\Repository\Criteria;
use Carbon\Carbon;

class Age extends Criteria
{
    /**
     * @param $model
     * @param RecipientInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $brackets = [];
        foreach($this->params AS $bracket) {
            $ages = explode('-', $bracket);
            $start = $ages[0];
            $end = isset($ages[1]) ? $ages[1] : 120;
            $now = Carbon::now();
            $brackets[] = [
                'start' => Carbon::now()->subYears($end)->subYear()->addDay(),
                'end' => Carbon::now()->subYears($start),
            ];
        }
        if ($brackets) {
            $model->where(function($qb) use($model, $brackets) {
                foreach($brackets AS $bracket) {
                    $qb->orWhereBetween('birthDate', [$bracket['start']->format('Y-m-d'), $bracket['end']->format('Y-m-d')]);
                }
            });
        }

        return $model;
    }
}