<?php

namespace Rush\Modules\Broadcasting\Criteria\Recipient;

use Illuminate\Support\Facades\DB;
use Rush\Modules\Repository\Criteria;
use Rush\Modules\Repository\RepositoryInterface as Repository;

class NewMember extends Criteria
{
    /**
     * @param $model
     * @param RecipientInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        // return newly registered Customers of the current month
        $model = $model
            ->from('Customer as c')
            ->where(DB::raw("DATE_FORMAT(c.timestamp, '%Y-%m')"), '=', date('Y-m', time()));

        return $model;
    }
}