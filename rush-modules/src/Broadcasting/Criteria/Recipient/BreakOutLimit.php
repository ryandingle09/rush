<?php

namespace Rush\Modules\Broadcasting\Criteria\Recipient;

use Rush\Modules\Repository\RepositoryInterface as Repository;
use Rush\Modules\Repository\Criteria;
use Carbon\Carbon;

class BreakOutLimit extends Criteria
{
    /**
     * @param $model
     * @param RecipientInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $model = $model
            ->join('CustomerPunchCardTransactions', 'Customer.customerId', '=','CustomerPunchCardTransactions.customer_id')
            ->whereIn('CustomerPunchCardTransactions.employee_id', function($query){
                $query->select('Users.userId')->from('Users')
                ->join('Branches', 'Users.branchId', '=', 'Branches.id')
                ->where('Branches.branchName', 'like', 'BREAKOUT SESSIONS');
            })
            ->groupBy('Customer.customerId')
            ->orderBy('CustomerPunchCardTransactions.date_punched', 'asc')
            ->limit($this->params);
        return $model;
    }
}