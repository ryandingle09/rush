<?php

namespace Rush\Modules\Broadcasting\Criteria\Recipient;

use Illuminate\Support\Facades\DB;
use Rush\Modules\Repository\Criteria;
use Rush\Modules\Repository\RepositoryInterface as Repository;

class NoPreviousMonthTransaction extends Criteria
{
    /**
     * @param $model
     * @param RecipientInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $merchant = $repository->getMerchant();
        $merchantId = $merchant->id;

        // reward currency type is points
        if ($merchant->isPro()) {
            $model = $model
                ->from('Customer as c')
                ->whereNotIn('c.customerId', function($queryB) use ($merchantId){
                    $queryB->select('ct.customerId')
                        ->from('CustomerTransaction as ct')
                        ->where('ct.transactionType', 'earn')
                        ->where('ct.merchant_id', $merchantId)
                        ->where(DB::raw("DATE_FORMAT(ct.timestamp, '%Y-%m')"), '=', date('Y-m', strtotime('first day of last month')));
                });
        } else {
            // reward currency type is stamps
            $model = $model
                ->from('Customer as c')
                ->whereNotIn('c.customerId', function($queryB) use ($merchantId){
                    $queryB->select('cpt.customer_id')
                        ->from('CustomerPunchCardTransactions as cpt')
                        ->where('cpt.type', 'earn')
                        ->where('cpt.void', '=', 0)
                        ->where('cpt.merchant_id', $merchantId)
                        ->where(DB::raw("DATE_FORMAT(cpt.date_punched, '%Y-%m')"), '=', date('Y-m', strtotime('first day of last month')));
                });
        }

        return $model;
    }
}