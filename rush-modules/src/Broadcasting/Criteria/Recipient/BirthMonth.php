<?php

namespace Rush\Modules\Broadcasting\Criteria\Recipient;

use Rush\Modules\Repository\RepositoryInterface as Repository;
use Rush\Modules\Repository\Criteria;
use Carbon\Carbon;

class BirthMonth extends Criteria
{
    /**
     * @param $model
     * @param RecipientInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $model = $model
            //->whereNotNull('birthDate')
            ->whereMonth('birthDate', '=', $this->params);
        return $model;
    }
}