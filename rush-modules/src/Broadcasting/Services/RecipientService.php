<?php

namespace Rush\Modules\Broadcasting\Services;

use Rush\Modules\Broadcasting\Criteria\Recipient\Merchant;
use Rush\Modules\Broadcasting\Criteria\Recipient\RecipientCriteriaFactory;
use Rush\Modules\Broadcasting\Repositories\RecipientInterface;
use Rush\Modules\Broadcasting\Repositories\SegmentInterface;
use Rush\Modules\Merchant\Services\MerchantService;
use Rush\Modules\Push\PushHelper;

/**
* Our RecipientService, containing all useful methods for business logic around Recipient
* Service just hands off data it was given by the controller to the appropriate repositories
*/
class RecipientService
{
    /**
     * @var Rush\Modules\Broadcasting\Repositories\RecipientRepository
     */
    protected $recipientRepository;

    /**
     * @var Rush\Modules\Broadcasting\Criteria\Recipient\RecipientCriteriaFactory
     */
    protected $recipientCriteriaFactory;

    /**
     * @var Rush\Modules\Broadcasting\Repositories\SegmentRepository
     */
    protected $segmentRepository;

    /**
     * @var Rush\Modules\Merchant\Services\MerchantService
     */
    protected $merchantService;

    public function __construct(
        RecipientInterface $recipientRepository,
        RecipientCriteriaFactory $recipientCriteriaFactory,
        SegmentInterface $segmentRepository,
        MerchantService $merchantService
    ) {
        $this->recipientRepository = $recipientRepository;
        $this->recipientCriteriaFactory = $recipientCriteriaFactory;
        $this->segmentRepository = $segmentRepository;
        $this->merchantService = $merchantService;
    }

    public function all()
    {
        return $this->recipientRepository->all();
    }

    public function buildCustomerQuery($merchantId, $segmentId)
    {
        $merchant = $this->merchantService->getById($merchantId);
        $this->recipientRepository->setMerchant($merchant);
        $this->recipientRepository->pushCriteria(new Merchant($merchantId));
        if ($segmentId) {
            $segment = $this->segmentRepository->getById($segmentId);
            $scopes = json_decode($segment->scopes);

            foreach ($scopes as $criteria => $params) {
                $this->recipientRepository->pushCriteria($this->recipientCriteriaFactory->createCriteria($criteria, $params));
            }
        }
    }

    public function getCustomers($merchantId, $segmentId = null)
    {
        $this->buildCustomerQuery($merchantId, $segmentId);
        return $this->recipientRepository->all();
    }

    public function getCustomerDevices($merchantId, $segmentId = null)
    {
        $this->buildCustomerQuery($merchantId, $segmentId);
        $devices = $this->recipientRepository->getCustomerDevices();

        $platformDevices = [];
        foreach ($devices AS $device) {
            $platformDevices[PushHelper::getPlatform($device->platform)][] = [
                'id' => $device->id,
                'token' => $device->token,
                'device_id' => $device->device_id,
            ];
        }

        return $platformDevices;
    }
}
