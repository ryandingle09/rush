<?php

namespace Rush\Modules\Broadcasting\Services;

use Rush\Modules\Broadcasting\Repositories\SegmentInterface;
use Rush\Modules\Merchant\Services\BranchService;

/**
* Our SegmentService, containing all useful methods for business logic around Segment
* Service just hands off data it was given by the controller to the appropriate repositories
*/
class SegmentService
{
    /**
     * SegmentRepository
     * @var Rush\Modules\Broadcasting\Repositories\SegmentRepository
     */
    protected $segmentRepository;

    /**
     * BranchSerice
     * @var Rush\Modules\Merchant\Services\BranchService
     */
    protected $branchService;

    public function __construct(
        SegmentInterface $segmentRepository,
        BranchService $branchService
    ) {
        $this->segmentRepository = $segmentRepository;
        $this->branchService = $branchService;
    }

    public function all()
    {
        return $this->segmentRepository->all();
    }

    public function store(array $input)
    {
        $segmentInput = $input['segment'];
        $segmentInput['status'] = isset($segmentInput['segActive']) ? ($segmentInput['segActive'] == 'on' ? 1 : 0): 0;
        $segmentInput['name'] = isset($segmentInput['segName']) ? $segmentInput['segName']: null;
        $scopes = [];
        foreach($segmentInput AS $key => $value) {
            if (strpos($key, 'targFilter') !== false) {
                switch($key) {
                    case 'targFilterGenderO':
                        if ($value != 'both')
                            $scopes['gender'] = $value[0];
                        break;
                    case 'targFilterAgeO':
                        $scopes['age'] = $value;
                        break;
                    case 'targFilterBirthMonthO':
                        $scopes['birthMonth'] = $value;
                        break;
                    case 'targFilterFirstCheckIn':
                        $scopes['firstCheckIn'] = $value;
                        break;
                    case 'targFilterNoClientCode':
                        $scopes['noClientCode'] = $value;
                        break;
                    case 'targFilterProspect':
                        $scopes['prospect'] = $value;
                        break;
                    case 'targFilterTopCustomer0':
                        $scopes['topCustomer'] = $value;
                        break;
                    case 'targFilterNewMember':
                        $scopes['newMember'] = $value;
                        break;
                    case 'targFilterNoPreviousMonthTransaction':
                        $scopes['noPreviousMonthTransaction'] = $value;
                        break;
                    case 'targFilterBranchTransaction0':
                        $branch = $this->branchService->find((int) $value);
                        $scopes['branchTransaction'] = [
                            'id' => $branch->id,
                            'name' => $branch->branchName
                        ];
                        break;
                    default:
                        break;
                }
            }
        }
        $segmentInput['scopes'] = json_encode($scopes);

        $validator = $this->validate($segmentInput,
            [   'status' => 'required',
                'name' => 'required|unique:segments,name,NULL,id,merchant_id,'.$segmentInput['merchant_id'],
                'scopes' => 'required',
                'merchant_id' => 'required'],
            [   'name.unique' => 'The :attribute has already been registered.']
        );
        if ($validator->fails()) {
            return $validator;
        }
        $this->segmentRepository->create($segmentInput);

        return true;
    }

    public function delete(array $input)
    {
        $validator = $this->validate($input, ['id' => 'required'], []);
        if ($validator->fails()) {
            return false;
        }
        $this->segmentRepository->delete($input['id']);

        return true;
    }

    private function validate($input, $rules, $messages = [])
    {
        return \Validator::make($input, $rules, $messages);
    }
}
