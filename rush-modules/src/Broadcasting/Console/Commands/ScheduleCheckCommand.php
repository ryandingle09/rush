<?php

namespace Rush\Modules\Broadcasting\Console\Commands;

use Illuminate\Console\Command;
use Rush\Modules\Email\Services\EmailService;
use Rush\Modules\Push\Services\PushService;
use Rush\Modules\Sms\Services\SmsService;

class ScheduleCheckCommand extends Command
{
    protected $isOn = false;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'broadcasting:schedule-check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks for scheduled tasks.';

    /**
     * SmsService
     * @var \Rush\Modules\Sms\Services\SmsService
     */
    protected $smsService = null;

    /**
     * EmailService
     * @var \Rush\Modules\Email\Services\EmailService
     */
    protected $emailService = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        SmsService $smsService,
        PushService $pushService,
        EmailService $emailService
    ) {
        parent::__construct();

        $this->smsService = $smsService;
        $this->pushService = $pushService;
        $this->emailService = $emailService;
        $this->init();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->isOn) {
            $this->smsService->check();
            $this->pushService->check();
            $this->emailService->check();
        }
    }

    public function init()
    {
        $broadcastingScheduleStatus = env('BROADCASTING_SCHEDULE', 'OFF');
        if ($broadcastingScheduleStatus == 'ON') {
            $this->isOn = true;
        }
    }
}
