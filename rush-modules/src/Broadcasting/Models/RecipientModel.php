<?php

namespace Rush\Modules\Broadcasting\Models;

use Rush\Modules\Customer\Models\CustomerModel;

class RecipientModel extends CustomerModel
{
    public function devices()
    {
        return $this->hasMany('Rush\Modules\Broadcasting\Models\RecipientDeviceModel', 'customer_id', 'customerId');
    }
}