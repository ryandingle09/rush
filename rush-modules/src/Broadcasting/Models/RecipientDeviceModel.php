<?php

namespace Rush\Modules\Broadcasting\Models;

use Illuminate\Database\Eloquent\Model;

class RecipientDeviceModel extends Model
{
    protected $table = "lara_customer_devices";
}
