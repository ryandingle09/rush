<?php

namespace Rush\Modules\Broadcasting\Models;

use Eloquent;

class MessageModel extends Eloquent
{
    protected $table = "message";
    protected $primaryKey = "id";
    public $timestamps = true;
    protected $fillable = [
        'status',
        'publish_date',
        'message',
        'merchant_id',
        'segment_id'
    ];

    /**
     * Get the segment
     */
    public function segment()
    {
        return $this->belongsTo('Rush\Modules\Broadcasting\Models\SegmentModel', 'segment_id', 'id');
    }
}