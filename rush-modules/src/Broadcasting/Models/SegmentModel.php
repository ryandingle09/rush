<?php

namespace Rush\Modules\Broadcasting\Models;

use Illuminate\Database\Eloquent\Model;

class SegmentModel extends Model
{
    protected $table = "segments";
    protected $primaryKey = "id";
    public $timestamps = true;
    protected $fillable = array(
        'status',
        'name',
        'scopes',
        'merchant_id'
    );
}
