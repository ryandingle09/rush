<?php

namespace Rush\Modules\Broadcasting\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Rush\Modules\Push\Models\PushModel AS Push;
use Rush\Modules\Push\Services\PushBroadcasterService;
use Rush\Modules\Queue\Contracts\MerchantAwareJobInterface;

class PushBroadcastJob extends Job implements ShouldQueue, MerchantAwareJobInterface
{
    use InteractsWithQueue, SerializesModels;

    /**
     * PushModel
     * @var Rush\Modules\Push\Models\PushModel
     */
    protected $push;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Push $push)
    {
        $this->push = $push;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(PushBroadcasterService $pushBroadcasterService)
    {
        $pushBroadcasterService->send($this->push);
    }

    /**
     * Provide a mechanism within the Job object to return the merchant_id
     * @return [int] The merchant_id
     */
    public function getMerchantId()
    {
        return $this->push->merchant_id;
    }
}
