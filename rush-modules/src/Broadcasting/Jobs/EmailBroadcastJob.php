<?php

namespace Rush\Modules\Broadcasting\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Rush\Modules\Email\Models\EmailModel AS Email;
use Rush\Modules\Email\Services\EmailBroadcasterService;
use Rush\Modules\Queue\Contracts\MerchantAwareJobInterface;

class EmailBroadcastJob extends Job implements ShouldQueue, MerchantAwareJobInterface
{
    use InteractsWithQueue, SerializesModels;

    /**
     * EmailModel
     * @var Rush\Modules\Email\Models\EmailModel
     */
    protected $email;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Email $email)
    {
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(EmailBroadcasterService $emailBroadcasterService)
    {
        $emailBroadcasterService->send($this->email);
    }

    /**
     * Provide a mechanism within the Job object to return the merchant_id
     * @return [int] The merchant_id
     */
    public function getMerchantId()
    {
        return $this->email->merchant_id;
    }
}
