<?php

namespace Rush\Modules\Broadcasting\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Rush\Modules\Queue\Contracts\MerchantAwareJobInterface;
use Rush\Modules\Sms\Models\SmsModel AS Sms;
use Rush\Modules\Sms\Services\SmsBroadcasterService;

class SmsBroadcastJob extends Job implements ShouldQueue, MerchantAwareJobInterface
{
    use InteractsWithQueue, SerializesModels;

    /**
     * SmsModel
     * @var Rush\Modules\Sms\Models\SmsModel
     */
    protected $sms;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Sms $sms)
    {
        $this->sms = $sms;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(SmsBroadcasterService $smsBroadcasterService)
    {
        $smsBroadcasterService->send($this->sms);
    }

    /**
     * Provide a mechanism within the Job object to return the merchant_id
     * @return [int] The merchant_id
     */
    public function getMerchantId()
    {
        return $this->sms->merchant_id;
    }
}
