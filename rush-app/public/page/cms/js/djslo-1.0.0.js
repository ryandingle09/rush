/* 
 * Dynamic scripts loader 
 * Version 1.0.0
 * 
 * Copyright (c) 2016
 */

 var AUTOLOAD_SCRIPTS = [
   // '//code.jquery.com/jquery-1.11.3.min.js',
   // '//cdn.datatables.net/1.10.10/js/jquery.dataTables.js',
   // 'assets/javascripts/lib/jquery.validate.js',
   // 'assets/bower_components/moment/min/moment.min.js',
   // 'assets/bower_components/bootstrap-sass/assets/javascripts/bootstrap.min.js',
   // '//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.9.1/bootstrap-table.min.js',
   // 'assets/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
   // '//d3js.org/d3.v3.min.js',
   // 'assets/javascripts/lib/jquery.fittext.js',
   // 'assets/javascripts/lib/jquery.activeNavigation.js',
   // 'assets/javascripts/lib/bootstrap-table-filter-control.js',
   // 'assets/javascripts/lib/spectrum.js',
   // 'assets/javascripts/lib/my.class.min.js',
   // 'assets/javascripts/lib/blur.min.js',
   'page/cms/js/common.js',
   'page/cms/js/global.js',
   'page/cms/js/lib/validator.js',
   'page/cms/js/reports.js',
   'page/cms/js/design.js',
   'page/cms/js/quicksetup.js',
   'page/cms/js/merchant.js',
   // 'assets/javascripts/main.js',
   'page/cms/js/admin.js'
];

var Loader,Config;

(function() {
	Loader = {
      charset: 'utf-8',
      init: function() {
          Config.init();    
          Loader.autoloadCompoments();
      },
        
      autoloadCompoments: function () {
        	var num_scripts = AUTOLOAD_SCRIPTS.length;
          Loader.loadComponent(AUTOLOAD_SCRIPTS,0,num_scripts);
      },
        
      loadComponent: function(scripts,index,num_scripts) {
        	if (index < num_scripts) {
	            Loader.addComponent(scripts[index],Loader.charset,function() {
	                Loader.loadComponent(scripts,index+1,num_scripts);     
	            });
          }
      },
        
      addComponent: function(url,charset,onload) {
        	var filePath = url;
        	var script = document.createElement('script');

          if (!filePath.startsWith("//") && !filePath.startsWith("http://") && !filePath.startsWith("https://")) {
              filePath = Config.base_url + filePath;
          }

        	script.type = 'text/javascript';
        	script.src = filePath;
        	script.charset = charset;
        	script.onreadystatechange = onload;
        	script.onload = onload;

        	if (typeof script != 'undefined') {
        		  document.getElementsByTagName("body")[0].appendChild(script);
        	}
      }
	};

  Config = {
      quicksetup: false,
      base_url: "",
      module: "",

      init: function() {
          var script = document.currentScript || (function() {
              document.getElementsByTagName('script');
              script = script[script.length - 1];
              return script;
          })();

          if (script.hasAttribute('data-base_url')) {
              Config.base_url = script.getAttribute('data-base_url');
          }

          if (script.hasAttribute('data-quicksetup')) {
              Config.quicksetup = script.getAttribute('data-quicksetup');
          }

          if (script.hasAttribute('data-module')) {
              Config.module = script.getAttribute('data-module');    
          }
      }
  };

	Loader.init(); 
}());