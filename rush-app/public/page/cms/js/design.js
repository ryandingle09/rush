/* design.js */

var CardDesign,PunchcardDesign,MerchantAppDesign;

(function() {
	var bgColors = [ '#00A8A4', '#00E6C5', '#8BFF6F' ];

	var rgb2hex = function (rgb) {
	    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
	    function hex(x) {
	    	var hexDigits = ["0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"];
	        return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
	    }
	    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
	};

	var hex2rgba = function(color) {
		var rgba = 'rgba(' + parseInt(color.slice(-6,-4),16) + ',' + parseInt(color.slice(-4,-2),16) + ',' + parseInt(color.slice(-2),16) +',0.4)';
		return rgba;
	}

	var initDesign = function()
	{
		var $hash = window.location.hash;
		if( $hash == "#tablet" ) {
			designTablet($("#btnTablet"));
		} else if ( $hash == "#stamp-icon") {
			designStampIcon($("#btnStampIcon"));
		}else{
			designTablet($("#btnTablet"));
		}

		$("#btnStampIcon").on('click',function(){
		    designStampIcon($(this));
		    window.location.hash = "stamp-icon";
		});
		$("#btnTablet").on('click',function(){
		    designTablet($(this));
		    window.location.hash = "tablet";
		});
	}

	CardDesign = {
        create: function() {
            // preloaded images
            var default_image = '/cpanel/assets/images/addImage.jpg';
            if ($('.cardUploadLogo .imgPrev').attr('src') != '' && $('.cardUploadLogo .imgPrev').attr('src') != default_image) {
            	$('.cardLogo').attr('src', $('.cardUploadLogo .imgPrev').attr('src'));
            } else {
            	$('.cardUploadLogo .imgPrev').attr('src',default_image);
            }

            if ($('.cardAddBackImage .imgPrev').attr('src') != '' && $('.cardAddBackImage .imgPrev').attr('src') != default_image) {
            	$('.cardWrap').css('background-image','url(' + $('.cardAddBackImage .imgPrev').attr('src') + ')');
            } else {
            	$('.cardAddBackImage .imgPrev').attr('src',default_image);
            }

			var uploadCardLogo = $(".cardUploadLogo .btn-file :file");
			if (typeof uploadCardLogo != 'undefined') {
			    uploadCardLogo.on('showPreview', function(event) {
			    	var getImagePath = URL.createObjectURL(event.target.files[0]);
			    	if (uploadCardLogo.attr("validationStatus") != "invalid") {
				        $('.cardUploadLogo .imgPrev').attr('src', getImagePath);
				        $('.cardLogo').attr('src', getImagePath);
			    	} else {
				        $('.cardUploadLogo .imgPrev').attr('src', default_image);
			    	}
			    });
			}

			var backgroundUpload = $(".cardAddBackImage .btn-file :file");
			if (typeof backgroundUpload != 'undefined') {
				backgroundUpload.on('showPreview', function(event) {
				    var getImagePath = URL.createObjectURL(event.target.files[0]);
				    if (backgroundUpload.attr("validationStatus") != "invalid") {
					    $('.cardAddBackImage .imgPrev').attr('src', getImagePath);
					    $('.cardWrap').css('background-image','url(' + getImagePath + ')');
				    } else {
					    $('.cardAddBackImage .imgPrev').attr('src', default_image);
				    }
				});
			}

			var saveDesignButton = $('.cardTab .saveDesign');
		    if (typeof saveDesignButton != 'undefined') {
		    	saveDesignButton.on('click', function() {
		    		if (uploadCardLogo.attr("validationStatus") == 'invalid') {
		    			return false;
		    		}
		    		if (backgroundUpload.attr("validationStatus") == 'invalid') {
		    			return false;
		    		}
		    		return true;
		    	});
		    }

			$("#cardBgColor").on('move.spectrum', function() {
			    var hexcolor = $("#cardBgColor").spectrum('get').toHexString();
			    $(this).siblings('.hex').html(hexcolor);

			    var rgbaCol = 'rgba(' + parseInt(hexcolor.slice(-6,-4),16) + ',' + parseInt(hexcolor.slice(-4,-2),16) + ',' + parseInt(hexcolor.slice(-2),16) +',0.4)';
			    $('.overlay').css('background-color',rgbaCol);
		    });

		    var bgColor = bgColors[0];

		    if ($("#cardBgColor").val() == "") {
		    	$("#cardBgColor").val(bgColor);
		    } else {
		    	bgColor = $("#cardBgColor").val();
		    }

		    $("#cardBackgroundColor").text(bgColor);
		    $('.overlay').css('background-color',hex2rgba(bgColor));

		    $("#cardBgColor").spectrum({
			    color: bgColor,
			    preferredFormat: "hex",
			    showInput: true,
			    showButtons: true,
			    chooseText: "Select",
			    cancelText: "Cancel"
		    });
        }
	};

	MobileAppDesign = {
		create: function() {
			console.log('MobileAppDesign:create');
			var $mobileAppDesign = $('#mobileAppDesign');
			var default_image = 'assets/images/addImage.jpg';
            if ($('.uploadLogo .imgPrev').attr('src') != '' && $('.uploadLogo .imgPrev').attr('src') != default_image) {
            	$('.customLogo').attr('src', $('.uploadLogo .imgPrev').attr('src'));
            } else {
            	$('.uploadLogo .imgPrev').attr('src',default_image);
            }

            if ($('.addBackImage .imgPrev').attr('src') != '' && $('.addBackImage .imgPrev').attr('src') != default_image) {
            	$('.customMobileBgImg').css('background-image','url(' + $('.addBackImage .imgPrev').attr('src') + ')');
            } else {
            	$('.addBackImage .imgPrev').attr('src',default_image);
            }

        	var mobileAppLogo = $(".uploadLogo .btn-file :file");
        	if (typeof mobileAppLogo != 'undefined') {
	        	mobileAppLogo.on('showPreview', function(event) {
	                var getImagePath = URL.createObjectURL(event.target.files[0]);
	                if (mobileAppLogo.attr("validationStatus") != "invalid") {
					    $('.uploadLogo .imgPrev').attr('src', getImagePath);
					    $('.customLogo').attr('src', getImagePath);
	                } else {
	                	$('.uploadLogo .imgPrev').attr('src', default_image);
	                }
	        	});
        	}

			var backgroundUpload = $(".addBackImage .btn-file :file");
			if (typeof backgroundUpload != 'undefined') {
				backgroundUpload.on('showPreview', function(event) {
				    var getImagePath = URL.createObjectURL(event.target.files[0]);
				    if (backgroundUpload.attr("validationStatus") != "invalid") {
					    $('.addBackImage .imgPrev').attr('src', getImagePath);
					    $('.customMobileBgImg').css('background-image', 'url(' + getImagePath + ')');
				    } else {
						$('.addBackImage .imgPrev').attr('src', default_image);
				    }
				});
			}

			var saveDesignButton = $('.mobileTab .saveDesign');
		    if (typeof saveDesignButton != 'undefined') {
		    	saveDesignButton.on('click', function() {
		    		if (mobileAppLogo.attr("validationStatus") == 'invalid') {
		    			return false;
		    		}
		    		if (backgroundUpload.attr("validationStatus") == 'invalid') {
		    			return false;
		    		}
		    		return true;
		    	});
		    }

            var bgColor1 = bgColors[0];
		    var bgColor2 = bgColors[1];
		    var bgColor3 = bgColors[2];

		    if ($("#pOverlay").val() == "" || $("#pOverlay").val() == 'undefined') {
		    	$("#pOverlay").val(bgColor1);
		    } else {
		    	bgColor1 = $("#pOverlay").val();
		    }

		    if ($("#pText").val() == "" || $("#pText").val() == 'undefined') {
		    	$("#pText").val(bgColor2);
		    } else {
		    	bgColor2 = $("#pText").val();
		    }

		    if ($("#pButtons").val() == "" || $("#pButtons").val() == 'undefined') {
		    	$("#pButtons").val(bgColor3);
		    } else {
		    	bgColor3 = $("#pButtons").val();
		    }

			$('span.p1').css('background-color', bgColor1);
			$('span.p2').css('background-color', bgColor2);
			$('span.p3').css('background-color', bgColor3);

			// update mobileAppDesign preview
			$('.topPane').css('background-color', bgColor1);
			$('#topPaneLabel').css('color', bgColor2);
			$('.redeem').css('background-color', bgColor3);
			$('.redeem').css('color', bgColor2);

			$mobileAppDesign.find('.prePaletteMnu li').on('click', function(e) {
		    	var overlayColor = rgb2hex($(this).find('i:first-child').css('background-color'));
		    	var textColor = rgb2hex($(this).find('i:nth-child(2)').css('background-color'));
		    	var buttonsColor = rgb2hex($(this).find('i:last-child').css('background-color'));

		    	var rgbaCol = 'rgba(' + parseInt(overlayColor.slice(-6,-4),16) + ',' + parseInt(overlayColor.slice(-4,-2),16) + ',' + parseInt(overlayColor.slice(-2),16) +',0.4)';

		    	$('span.p1').css('background-color', overlayColor);
		    	$('span.p2').css('background-color', textColor);
		    	$('span.p3').css('background-color', buttonsColor);

			    var palette = $mobileAppDesign.find('.customPaletteBox').children('div');
			    palette.eq(0).find('.sp-preview-inner').css('background-color', overlayColor);
			    palette.eq(1).find('.sp-preview-inner').css('background-color', textColor);
			    palette.eq(2).find('.sp-preview-inner').css('background-color', buttonsColor);

			    $('#pOverlay').val(overlayColor);
			    $('#pText').val(textColor);
			    $('#pButtons').val(buttonsColor);

			    $('#cThemeVal').text(overlayColor);
			    $('#cTextVal').text(textColor);
			    $('#cButtonsVal').text(buttonsColor);

                $('#topPaneLabel').css('color', textColor);
                $('.redeem').css('color', textColor);
                $('.redeem').css('background-color', buttonsColor);

			    var rgbaCol = 'rgba(' + parseInt(overlayColor.slice(-6,-4),16) + ',' + parseInt(overlayColor.slice(-4,-2),16) + ',' + parseInt(overlayColor.slice(-2),16) +',0.4)';
			    $('.topPane').css('background-color', rgbaCol);
		    });

		    $("#pOverlay").on('move.spectrum', function() {
			    var hexcolor = $("#pOverlay").spectrum('get').toHexString();
			    $(this).siblings('.hex').html(hexcolor);
			    $('span.p1').css('background-color',hexcolor);

			    var rgbaCol = 'rgba(' + parseInt(hexcolor.slice(-6,-4),16) + ',' + parseInt(hexcolor.slice(-4,-2),16) + ',' + parseInt(hexcolor.slice(-2),16) +',0.4)';
			    // $('.customMobileBgColor').css('background-color',rgbaCol);
			    $('.topPane').css('background-color',hexcolor);
			});
			$("#pText").on('move.spectrum', function() {
			    var hexcolor = $("#pText").spectrum('get').toHexString();
			    $(this).siblings('.hex').html(hexcolor);
			    $('span.p2').css('background-color',hexcolor);

			    $('#topPaneLabel').css('color',hexcolor);
                $('.redeem').css('color',hexcolor);
			});
			$("#pButtons").on('move.spectrum', function() {
			    var hexcolor = $("#pButtons").spectrum('get').toHexString();
			    $(this).siblings('.hex').html(hexcolor);
			    $('span.p3').css('background-color',hexcolor);

			    $('.redeem').css('background-color',hexcolor);
			});

			$("#pOverlay").spectrum({
			    color: bgColor1,
			    preferredFormat: "hex",
			    showInput: true,
			    showButtons: true,
			    chooseText: "Select",
			    cancelText: "Cancel"
			});
			$("#pText").spectrum({
			    color: bgColor2,
			    preferredFormat: "hex",
			    showInput: true,
			    showButtons: true,
			    chooseText: "Select",
			    cancelText: "Cancel"
			});
			$("#pButtons").spectrum({
			    color: bgColor3,
			    preferredFormat: "hex",
			    showInput: true,
			    showButtons: true,
			    chooseText: "Select",
			    cancelText: "Cancel"
			});

			var merchantAppPaletteCount = 1;

			$('#saveCustomerAppPalette').on('click', function() {
			    var cloned = $('#customerAppPalette').find('li:last').clone(true);
			    cloned.find('span').text('Custom Palette ' + (merchantAppPaletteCount++));

			    cloned.find('i:first-child').css('background-color', $('#cThemeVal').text());
			    cloned.find('i:nth-child(2)').css('background-color', $('#cTextVal').text());
			    cloned.find('i:last-child').css('background-color', $('#cButtonsVal').text());

			    cloned.appendTo('#customerAppPalette');
			});
		}

		//imageFileValidator.init();
	};

	PunchcardDesign = {
        create: function() {
        	console.log('PunchcardDesign:create');
        	var $punchcardDesign = $('#punchcardDesign');
            var default_image = 'assets/images/addImage.jpg';

		    var bgColor1 = bgColors[0];
		    var bgColor2 = bgColors[1];
		    var bgColor3 = bgColors[2];

		    if ($("#pOverlay").val() == "" || $("#pOverlay").val() == undefined) {
		    	$("#pOverlay").val(bgColor1);
		    } else {
		    	bgColor1 = $("#pOverlay").val();
		    }

		    if ($("#pText").val() == "" || $("#pText").val() == undefined) {
		    	$("#pText").val(bgColor2);
		    } else {
		    	bgColor2 = $("#pText").val();
		    }

		    if ($("#pButtons").val() == "" || $("#pButtons").val() == undefined) {
		    	$("#pButtons").val(bgColor3);
		    } else {
		    	bgColor3 = $("#pButtons").val();
		    }

			$('span.p1').css('background-color',bgColor1);
            $('span.p2').css('background-color',bgColor2);
            $('span.p3').css('background-color',bgColor3);

 			$("#cThemeVal").text(bgColor1);
			$("#cTextVal").text(bgColor2);
			$("#cButtonsVal").text(bgColor3);

			// update mobileAppDesign preview
			$('.topPane').css('background-color', bgColor1);
			$('#topPaneLabel').css('color', bgColor2);
			$('.redeem').css('background-color', bgColor3);
			$('.redeem').css('color', bgColor2);

		    $punchcardDesign.find('.prePaletteMnu li').on('click', function(e) {
		    	var overlayColor = rgb2hex($(this).find('i:first-child').css('background-color'));
		    	var textColor = rgb2hex($(this).find('i:nth-child(2)').css('background-color'));
		    	var buttonsColor = rgb2hex($(this).find('i:last-child').css('background-color'));

		    	$('span.p1').css('background-color', overlayColor);
		    	$('span.p2').css('background-color', textColor);
		    	$('span.p3').css('background-color', buttonsColor);

			    var palette = $punchcardDesign.find('.customPaletteBox').children('div');
			    palette.eq(0).find('.sp-preview-inner').css('background-color', overlayColor);
			    palette.eq(1).find('.sp-preview-inner').css('background-color', textColor);
			    palette.eq(2).find('.sp-preview-inner').css('background-color', buttonsColor);

			    $('#pOverlay').val(overlayColor);
			    $('#pText').val(textColor);
			    $('#pButtons').val(buttonsColor);

			    $('#cThemeVal').text(overlayColor);
			    $('#cTextVal').text(textColor);
			    $('#cButtonsVal').text(buttonsColor);

			    $('#topPaneLabel').css('color', textColor);
                $('.redeem').css('color', textColor);
                $('.redeem').css('background-color', buttonsColor);

				var rgbaCol = 'rgba(' + parseInt(overlayColor.slice(-6,-4),16) + ',' + parseInt(overlayColor.slice(-4,-2),16) + ',' + parseInt(overlayColor.slice(-2),16) +',0.4)';
			    $('.topPane').css('background-color', rgbaCol);
		    });

		    $("#pOverlay").on('move.spectrum', function() {
			    var hexcolor = $("#pOverlay").spectrum('get').toHexString();
			    $(this).siblings('.hex').html(hexcolor);
			    $('span.p1').css('background-color',hexcolor);

			    var rgbaCol = 'rgba(' + parseInt(hexcolor.slice(-6,-4),16) + ',' + parseInt(hexcolor.slice(-4,-2),16) + ',' + parseInt(hexcolor.slice(-2),16) +',0.4)';
			    // $('.customMobileBgColor').css('background-color',rgbaCol);
			    $('.topPane').css('background-color',hexcolor);
			});
			$("#pText").on('move.spectrum', function() {
			    var hexcolor = $("#pText").spectrum('get').toHexString();
			    $(this).siblings('.hex').html(hexcolor);
			    $('span.p2').css('background-color',hexcolor);

			    $('#topPaneLabel').css('color',hexcolor);
                $('.redeem').css('color',hexcolor);
			});
			$("#pButtons").on('move.spectrum', function() {
			    var hexcolor = $("#pButtons").spectrum('get').toHexString();
			    $(this).siblings('.hex').html(hexcolor);
			    $('span.p3').css('background-color',hexcolor);

			    $('.redeem').css('background-color',hexcolor);
			});

			$("#pOverlay").spectrum({
			    color: bgColor1,
			    preferredFormat: "hex",
			    showInput: true,
			    showButtons: true,
			    chooseText: "Select",
			    cancelText: "Cancel"
			});
			$("#pText").spectrum({
			    color: bgColor2,
			    preferredFormat: "hex",
			    showInput: true,
			    showButtons: true,
			    chooseText: "Select",
			    cancelText: "Cancel"
			});
			$("#pButtons").spectrum({
			    color: bgColor3,
			    preferredFormat: "hex",
			    showInput: true,
			    showButtons: true,
			    chooseText: "Select",
			    cancelText: "Cancel"
			});

			var merchantAppPaletteCount = 1;

			$('#saveCustomerAppPalette').on('click',function() {
		        var cloned = $('#customerAppPalette').find("li:last").clone(true);
		        cloned.find("span").text("Custom Palette " + (merchantAppPaletteCount++));

		        cloned.find('i:first-child').css('background-color',$('#cThemeVal').text());
		        cloned.find('i:nth-child(2)').css('background-color',$('#cTextVal').text());
		        cloned.find('i:last-child').css('background-color',$('#cButtonsVal').text());

		        cloned.appendTo('#customerAppPalette');
		    });
        }
	};

    MerchantAppDesign = {
        create: function() {
        	console.log('MerchantAppDesign:create');
            // preloaded images
            var $merchantAppDesign = $('#merchantAppDesign');
            var default_image = 'assets/images/addImage.jpg';
            if ($('.tabletUploadLogo .imgPrev').attr('src') != '' && $('.tabletUploadLogo .imgPrev').attr('src') != default_image) {
            	$('.tabletCustomLogo').attr('src', $('.tabletUploadLogo .imgPrev').attr('src'));
            } else {
            	$('.tabletUploadLogo .imgPrev').attr('src','assets/images/addImage.jpg');
            }

            if ($('.tabletAddBackImage .imgPrev').attr('src') != '' && $('.tabletAddBackImage .imgPrev').attr('src') != default_image) {
            	$('.tabletLayout').css('background-image','url(' + $('.tabletAddBackImage .imgPrev').attr('src') + ')');
            } else {
            	$('.tabletAddBackImage .imgPrev').attr('src','assets/images/addImage.jpg');
            }
            if ($('.upStampC .imgPrev').attr('src') != '' && $('.upStampC .imgPrev').attr('src') != default_image) {
            	var image = $('.upStampC .imgPrev').attr('src');
            	$('.stampWrap .c').css('background-image','url(' + image + ')');
            	$('.stampWrap .g').css('background-image','url(' + image + ')');
            } else {
            	$('.upStampC .imgPrev').attr('src','assets/images/addImage.jpg');
            	$('.upStampG .imgPrev').attr('src','assets/images/addImage.jpg');
            }

        	var merchantAppLogo = $(".tabletUploadLogo .btn-file :file");
        	if (typeof merchantAppLogo != 'undefined') {
	        	merchantAppLogo.on('showPreview', function(event) {
	                var getImagePath = URL.createObjectURL(event.target.files[0]);
	                if (merchantAppLogo.attr("validationStatus") != "invalid") {
		                $('.tabletUploadLogo .imgPrev').attr('src', getImagePath);
		                $('.tabletCustomLogo').attr('src', getImagePath);
	                } else {
		                $('.tabletUploadLogo .imgPrev').attr('src', default_image);
	                }
	        	});
        	}

			var backgroundUpload = $(".tabletAddBackImage .btn-file :file");
			if (typeof backgroundUpload != 'undefined') {
				backgroundUpload.on('showPreview', function(event) {
				    var getImagePath = URL.createObjectURL(event.target.files[0]);
				    if (backgroundUpload.attr("validationStatus") != "invalid") {
					    $('.tabletAddBackImage .imgPrev').attr('src', getImagePath);
					    $('.tabletLayout').css('background-image','url(' + getImagePath + ')');
				    } else {
					    $('.tabletAddBackImage .imgPrev').attr('src', default_image);
				    }
				});
			}

			var stampUpload = $(".upStampC .btn-file :file");
            if (typeof stampUpload != 'undefined') {
			    stampUpload.on('showPreview',function(event) {
			        var getImagePath = URL.createObjectURL(event.target.files[0]);
			        if (stampUpload.attr("validationStatus") != "invalid") {
					    $('.upStampC .imgPrev').attr('src', getImagePath);
					    $('.upStampG .imgPrev').attr('src', getImagePath);
			        } else {
					    $('.upStampC .imgPrev').attr('src', default_image);
					    $('.upStampG .imgPrev').attr('src', default_image);
			        }
			    });
		    }

		    var saveDesignButton = $('.tabletTab .saveDesign');
		    if (typeof saveDesignButton != 'undefined') {
		    	saveDesignButton.on('click', function() {
		    		if (merchantAppLogo.attr("validationStatus") == 'invalid') {
		    			return false;
		    		}
		    		if (backgroundUpload.attr("validationStatus") == 'invalid') {
		    			return false;
		    		}
		    		return true;
		    	});
		    }

		    var stampSaveDesignButton = $('.stampIconTab .saveDesign');
		    if (typeof stampSaveDesignButton != 'undefined') {
		    	stampSaveDesignButton.on('click', function() {
		    		var parentDiv = $(this).parent().parent().parent();
		    		var msgPleaseSupplyImage = 'Please upload the required image.';
		    		var msgInvalidDimension = 'Image does not meet the image size requirement. Please check again.';
		    		if (stampUpload.attr("validationStatus") == 'invalid') {
		    			parentDiv.find('.error-msg').text(msgInvalidDimension).removeClass('hide');
		    			return false;
		    		}
		    		if (stampUpload.attr("validationStatus") == undefined
		    			&& parentDiv.find('.imgPrev').attr('src') == default_image
		    		) {
			    		parentDiv.find('.error-msg').text(msgPleaseSupplyImage).removeClass('hide');
		    			return false;
		    		}
		    		parentDiv.find('.error-msg').text(msgPleaseSupplyImage).addClass('hide');
		    		return true;
		    	});
		    }

		    var bgColor1 = bgColors[0];
		    var bgColor2 = bgColors[1];
		    var bgColor3 = bgColors[2];

		    // if ($("#tpOverlay").val() == "") {
		    // 	$("#tpOverlay").val(bgColor1);
		    // } else {
		    // 	bgColor1 = $("#tpOverlay").val();
		    // }

		    if ($("#tpText").val() == "") {
		    	$("#tpText").val(bgColor2);
		    } else {
		    	bgColor2 = $("#tpText").val();
		    }

		    if ($("#tpButtons").val() == "") {
		    	$("#tpButtons").val(bgColor3);
		    } else {
		    	bgColor3 = $("#tpButtons").val();
		    }

			// $('span.tp1').css('background-color',bgColor1);
            $('span.tp2').css('background-color',bgColor2);
            $('span.tp3').css('background-color',bgColor3);

			// $("#mThemeVal").text(bgColor1);
			$("#mTextVal").text(bgColor2);
			$("#mButtonsVal").text(bgColor3);

			$('.customButton').css('color',bgColor2);
			$('.customButton').css('background-color',bgColor3);

		    $merchantAppDesign.find('.prePaletteMnu li').on('click', function(e) {
		        var textColor = rgb2hex($(this).find('i:first-child').css('background-color'));
		        var buttonsColor = rgb2hex($(this).find('i:last-child').css('background-color'));

		        $('span.tp2').css('background-color', textColor);
		        $('span.tp3').css('background-color', buttonsColor);

		        var palette = $merchantAppDesign.find('.customPaletteBox').children('div');
		        palette.eq(0).find('.sp-preview-inner').css('background-color', textColor);
		        palette.eq(1).find('.sp-preview-inner').css('background-color', buttonsColor);

		        $('#tpText').val(textColor);
		        $('#tpButtons').val(buttonsColor);

		        $('#mTextVal').text(textColor);
		        $('#mButtonsVal').text(buttonsColor);

		        $('.customButton').css('color', textColor);
		        $('.customButton').css('background-color', buttonsColor);
		    });

			$("#tpText").on('move.spectrum', function() {
			    var hexcolor = $("#tpText").spectrum('get').toHexString();
			    $(this).siblings('.hex').html(hexcolor);
			    $('span.tp2').css('background-color',hexcolor);
			    $('.customButton').css('color',hexcolor);
			});
			$("#tpButtons").on('move.spectrum', function() {
			    var hexcolor = $("#tpButtons").spectrum('get').toHexString();
			    $(this).siblings('.hex').html(hexcolor);
			    $('span.tp3').css('background-color',hexcolor);

			    $('.customButton').css('background-color',hexcolor);
			});

			$("#tpText").spectrum({
			    color: bgColor2,
			    preferredFormat: "hex",
			    showInput: true,
			    showButtons: true,
			    chooseText: "Select",
			    cancelText: "Cancel"
			});
			$("#tpButtons").spectrum({
			    color: bgColor3,
			    preferredFormat: "hex",
			    showInput: true,
			    showButtons: true,
			    chooseText: "Select",
			    cancelText: "Cancel"
			});

			var merchantAppPaletteCount = 1;

			$('#saveMerchantAppPalette').on('click',function() {
		        var cloned = $('#merchantAppPalette').find("li:last").clone(true);
		        cloned.find("span").text("Custom Palette " + (merchantAppPaletteCount++));

		        cloned.find('i:first-child').css('background-color',$('#mTextVal').text());
		        cloned.find('i:last-child').css('background-color',$('#mButtonsVal').text());

		        cloned.appendTo('#merchantAppPalette');
		    });
        }
	}

	initDesign();
})();
