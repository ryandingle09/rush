var Rush = Rush || {};

(function() {
	Rush = {
		Admin : {
	        init: function() {
	        	//PromoAddView
			    $(".promos .savePromo").click(function(){
			        $('input[name=status]').val(2);
			        if($("#updatePromoMechanics").size() > 0 ) {
			        	$("#updatePromoMechanics").submit();
			        }
			       	return true; 
			    });

			    $(".promos .publishPromo").click(function(){
			        $('input[name=status]').val(1);
			        if($("#updatePromoMechanics").size() > 0 ) {
			        	$("#updatePromoMechanics").submit();
			        }
			       	return true; 
			    });

			    $('.datetimepicker').datetimepicker({
				    format: 'YYYY-MM-DD'
				});

				$("#promoDurationStart").on("dp.change", function(e) {
					$("#durationEnd").removeAttr('disabled');
					$("#redemptionDurationStart").removeAttr('disabled');
					var durationStart = $(this).data("DateTimePicker").date();

					$("#promoDurationEnd").data("DateTimePicker").minDate( durationStart.format('YYYY-MM-DD') );
					$("#promoDurationEnd").data("DateTimePicker").clear();

					$("#promoRedemptionDurationStart").data("DateTimePicker").minDate( durationStart.format('YYYY-MM-DD') );
					$("#promoRedemptionDurationStart").data("DateTimePicker").clear();

				});

				$("#promoDurationStart").on("click", function(e) {
					if ($('#updatePromoMechanics input[name="status"]').val() != "1")
						$("#promoDurationStart").data("DateTimePicker").minDate(new Date());
				});

				// prevent updating the promoDurationStart when punchcard is published
				var inputStatus = $('#updatePromoMechanics input[name="status"]');
				var promoMechanicsStatus = (inputStatus !== null) ? inputStatus.val() : 0;
				if (promoMechanicsStatus == '1') {
					$('#durationStart').attr('readonly', 'readonly');
				}

				$("#promoDurationEnd").on("click", function(e) {
					var durationStart = $("#promoDurationStart").data("DateTimePicker").date();
					$("#promoDurationEnd").data("DateTimePicker").minDate( durationStart.format('YYYY-MM-DD') );
					$("#promoDurationEnd").data("DateTimePicker").clear();
				});

				$("#promoRedemptionDurationStart").on("dp.change", function(e) {
					if( $(this).data("DateTimePicker").date() ) {
						$("#redemptionDurationEnd").removeAttr('disabled');
						var redemptionDurationStart = $(this).data("DateTimePicker").date();
						$("#promoRedemptionDurationEnd").data("DateTimePicker").minDate( redemptionDurationStart.format('YYYY-MM-DD') );
						$("#promoRedemptionDurationEnd").data("DateTimePicker").clear();
					}
				});

				$("#promoRedemptionDurationStart").on("click", function(e) {
					var durationStart = $("#promoDurationStart").data("DateTimePicker").date();
					$("#promoRedemptionDurationStart").data("DateTimePicker").minDate( durationStart.format('YYYY-MM-DD') );
					$("#promoRedemptionDurationStart").data("DateTimePicker").clear();
				});

				$("#promoRedemptionDurationEnd").on("click", function(e) {
					var redemptionStart = $("#promoRedemptionDurationStart").data("DateTimePicker").date();
					if (redemptionStart === null) {
						redemptionStart = $("#promoDurationStart").data("DateTimePicker").date();
					}
					$("#promoRedemptionDurationEnd").data("DateTimePicker").minDate( redemptionStart.format('YYYY-MM-DD') );
					$("#promoRedemptionDurationEnd").data("DateTimePicker").clear();
				});

			    $('#stamps').on("change", "textarea", function() {
			    	$(this).parent().parent().parent().find('input[type=hidden]').val(1);
			    });

			    $('#stamps').on("click", ":checkbox", function() {
			    	$(this).parent().parent().parent().parent().parent().find('input[type=hidden]').val(1);
			    });

			    $("#stamps").on("click", ".clearStamp", function() {
			    	var $modal_body = $(this).parent().parent().parent().parent().find(".modal-body");
			    	if ($modal_body.find('canvas').size() > 0 && $modal_body.find('textarea').size() > 0) {
			    		if (!confirm('Are you sure you want to remove this reward from our database?')) {
			    			$modal_body.parent().find('.close').click();
			    			return false;
			    		}
			    	} else {
			    		return false;
			    	}
			    	var $modal_container = $modal_body.parent().parent().parent();
			    	var $stamp_link = $modal_body.parent().parent().parent().parent().find("a");
			    	$stamp_link.removeClass("with-stamp");
			    	$modal_body.find("textarea").val('');
			    	$modal_body.find("input[type=file]").val('');
			    	$modal_body.find("canvas").remove();
			    	$modal_body.find(".alert").remove();
			    	$modal_body.find(".with-img").removeClass("with-img");
			    	var $promptSuccess = "<div class='alert alert-success'>";
			    	$message = "Reward successfully cleared!";
		    		$promptSuccess += "<p>" + $message + "</p>"+ "</div>";
		    		$modal_body.prepend($promptSuccess);
			    	if( $modal_body.find('input[type=hidden]').size() > 0 ) {
		    			$modal_body.find('input[type=hidden]').val(1);
		    		}
			    });

			    $("#stamps").on('click', ".saveStamp", function(){
			    	var $modal_body = $(this).parent().parent().parent().parent().find(".modal-body");
			    	var $modal_container = $modal_body.parent().parent().parent();
			    	var $message;
			    	var $promptError = "<div class='alert alert-danger'>";
			    	var $promptSuccess = "<div class='alert alert-success'>";
			    	$modal_body.find(".alert").remove();
			    	if( $modal_body.find("textarea").val() && $modal_body.find(":checkbox.options:checked").length > 0 && $modal_body.find('canvas').size() > 0){
			    		var $stamp_link = $modal_body.parent().parent().parent().parent().find("a");
			    		$stamp_link.addClass("with-stamp");
			    		$message = "Reward successfully saved!";
			    		$promptSuccess += "<p>" + $message + "</p>"+ "</div>";
			    		$modal_body.prepend($promptSuccess);
			    		return;
			    	}

		    		$message = "Reward image, description and at least one branch assignment are required! ";
		    		$promptError += "<p>" + $message + "</p>"+ "</div>";
		    		$modal_body.prepend($promptError);
			    });

			    $("#stamps .modal").on('hidden.bs.modal', function () {
			    	$(this).find(".modal-body").find(".alert").remove();
				});

			    $("#historyTable th:first .th-inner").trigger('click');

			    $("#stamps").on('stampsDomUpdated', function () {
			    	Rush.Admin.bindFileUpload();
			    });
	        },

	        bindFileUpload: function() {
	        	$("#stamps input[type=file]").fileupload({
			        dataType: 'json',
			        autoUpload: false,
			        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
			        maxFileSize: 999000,
			        disableImageResize: /Android(?!.*Chrome)|Opera/
			            .test(window.navigator.userAgent),
			        previewMaxWidth: 350,
			        previewMaxHeight: 200,
			        previewCrop: false,
			        replaceFileInput:false
			    }).on('fileuploadprocessalways', function (e, data) {
			    	var that = $(this);
			    	var modal_body = that.parent().parent().parent().parent().find(".modal-body");
			    	var file = data.files[0];
			    	var dimensionIsValid = true;

			    	var showPreview = function() {
				    	that.parent().find("canvas").remove();
			    		if (dimensionIsValid) {
			    			that.parent().append(file.preview);
			    			that.parent().addClass("with-img");
				    		if( that.parent().find('input[type=hidden]').size() > 0 ) {
				    			that.parent().find('input[type=hidden]').val(1);
				    		}
				    		modal_body.find(".alert").addClass('hide');
			    		} else {
			    			that.parent().removeClass("with-img");
			    			modal_body.find(".alert").removeClass('hide');
			    		}
			    	}

			    	var fr = new FileReader;
					fr.onload = function() {
						var img = new Image;
						img.onload = function() {
					        if (img.width != 350 || img.height != 200) {
								dimensionIsValid = false;
					        }
					        showPreview();
					    };
					    img.src = fr.result;
					};
					fr.readAsDataURL(file);
			    });
	        },

	        multipleStamps: (function() {
	        	var multiple = 3;
	        	var minimumStamps = 3;
	        	var maximumStamps = 15;
	        	if ($('#stampsMax').length) {
					maximumStamps = $('#stampsMax').val();
	        	}
	        	if ($('#stampsMultiplier').length) {
					multiple = $('#stampsMultiplier').val();
	        	}

                var removeStampsSelector = '#removeStampsBtn';
                var addStampsSelector = '#addStampsBtn';

                var showAddRemoveButtons = function() {
                	var stampsCount = $('#stamps ul li').length;
	        		if (stampsCount > minimumStamps) {
	        			$(removeStampsSelector).removeClass('hide');
	        		} else {
	        			$(removeStampsSelector).addClass('hide');
	        		}
	        		if (stampsCount < maximumStamps) {
	        			$(addStampsSelector).removeClass('hide');
	        		} else {
	        			$(addStampsSelector).addClass('hide');
	        		}
                }

	        	var init = function() {
                    $('#stamps').on('addStamps', function() {
                        var stampsCount = $('#stamps ul li').length;
                        if (stampsCount < maximumStamps) {
	                        var markup = $('#stamps ul li:last-child').html();
	                    	var regex = new RegExp(stampsCount, 'g');
	                        for (var i = 1; i <= multiple; i++) {
	                            var newMarkup = markup.replace(regex, stampsCount + 1);
	                            $("#stamps ul").append("<li>" + newMarkup + "</li>");
	                        	stampsCount++;
	                        }
	                        Rush.Admin.bindFileUpload();
	                        showAddRemoveButtons();
                        }
                    });

                    $('#stamps').on('removeStamps', function() {
                    	var stampsCount = $('#stamps ul li').length;
                    	if (stampsCount > minimumStamps) {
                    		for (var i = 0; i < multiple; i++) {
                    			$('#stamps ul li:last-child').remove();
                    		}
                    		showAddRemoveButtons();
                    	}
                    });

                    $(addStampsSelector).click(function() {
                    	$('#stamps').trigger('addStamps');
                    });

                    $(removeStampsSelector).click(function() {
                    	$('#stamps').trigger('removeStamps');
                    });

                    showAddRemoveButtons();
                };

	        	return {
	        		init: init
	        	}
	        })()
		}
	};

	$(document).ready(function() {
		Rush.Admin.init();
		Rush.Admin.bindFileUpload();
		Rush.Admin.multipleStamps.init();
	});
})();