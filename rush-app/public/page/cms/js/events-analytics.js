$(document).ready( function() {
	$('body').on("click", "#menu ul li.ana", function () {
		ga('send', {
		  hitType: 'event',
		  eventCategory: 'Menu-Analytics',
		  eventAction: 'click',
		  eventLabel: _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});

		gtag('event', 'click', {
		  'event_category': 'Menu-Analytics',
		  'event_label': _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});
	});

	$('body').on("click", "#menu ul li.tra", function () {
		ga('send', {
		  hitType: 'event',
		  eventCategory: 'Menu-Transaction-History',
		  eventAction: 'click',
		  eventLabel: _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});

		gtag('event', 'click', {
		  'event_category': 'Menu-Transaction-History',
		  'event_label': _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});
	});

	$('body').on("click", "#menu ul li.sms", function () {
		ga('send', {
		  hitType: 'event',
		  eventCategory: 'Menu-Customer-Engagements',
		  eventAction: 'click',
		  eventLabel: _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});

		gtag('event', 'click', {
		  'event_category': 'Menu-Customer-Engagements',
		  'event_label': _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});
	});

	$('body').on("click", "#menu ul li.hel", function () {
		ga('send', {
		  hitType: 'event',
		  eventCategory: 'Menu-Help',
		  eventAction: 'click',
		  eventLabel: _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});

		gtag('event', 'click', {
		  'event_category': 'Menu-Help',
		  'event_label': _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});
	});

	$('body').on("click", "#menu ul li.bil", function () {
		ga('send', {
		  hitType: 'event',
		  eventCategory: 'Menu-Billing',
		  eventAction: 'click',
		  eventLabel: _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});

		gtag('event', 'click', {
		  'event_category': 'Menu-Billing',
		  'event_label': _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});
	});

	$('body').on("click", ".analytics-export-pdf", function () {
		ga('send', {
		  hitType: 'event',
		  eventCategory: 'Analytics-Export-Pdf',
		  eventAction: 'click',
		  eventLabel: _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});

		gtag('event', 'click', {
		  'event_category': 'Analytics-Export-Pdf',
		  'event_label': _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});
	});

	$('body').on("click", "a.sms-broadcast-tab", function () {
		ga('send', {
		  hitType: 'event',
		  eventCategory: 'Customer-Enagement-Sms-Broadcast-Tab',
		  eventAction: 'click',
		  eventLabel: _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});

		gtag('event', 'click', {
		  'event_category': 'Customer-Enagement-Sms-Broadcast-Tab',
		  'event_label': _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});
	});

	$('body').on("click", "a.push-notif-tab", function () {
		ga('send', {
		  hitType: 'event',
		  eventCategory: 'Customer-Engagement-Push-Notif-Tab',
		  eventAction: 'click',
		  eventLabel: _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});

		gtag('event', 'click', {
		  'event_category': 'Customer-Engagement-Push-Notif-Tab',
		  'event_label': _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});
	});

	$('body').on("click", "a.email-broadcast-tab", function () {
		ga('send', {
		  hitType: 'event',
		  eventCategory: 'Customer-Engagement-Email-Broadcast-Tab',
		  eventAction: 'click',
		  eventLabel: _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});

		gtag('event', 'click', {
		  'event_category': 'Customer-Engagement-Email-Broadcast-Tab',
		  'event_label': _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});
	});

	$('body').on("click", "a.target-segment-tab", function () {
		ga('send', {
		  hitType: 'event',
		  eventCategory: 'Customer-Engagement-Target-Segment-Tab',
		  eventAction: 'click',
		  eventLabel: _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});

		gtag('event', 'click', {
		  'event_category': 'Customer-Engagement-Target-Segment-Tab',
		  'event_label': _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});
	});

	$('body').on("click", "a.broadcast-events-tab", function () {
		ga('send', {
		  hitType: 'event',
		  eventCategory: 'Customer-Engagement-Broadcast-Events-Tab',
		  eventAction: 'click',
		  eventLabel: _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});

		gtag('event', 'click', {
		  'event_category': 'Customer-Engagement-Broadcast-Events-Tab',
		  'event_label': _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});
	});

	$('body').on("click", "button.target-segment-button", function () {
		ga('send', {
		  hitType: 'event',
		  eventCategory: 'Customer-Engagement-Target-Segment-Button',
		  eventAction: 'click',
		  eventLabel: _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});

		gtag('event', 'click', {
		  'event_category': 'Customer-Engagement-Target-Segment-Button',
		  'event_label': _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});
	});

	$('body').on("click", "button.add-sms-button", function () {
		ga('send', {
		  hitType: 'event',
		  eventCategory: 'Customer-Engagement-Add-Sms-Button',
		  eventAction: 'click',
		  eventLabel: _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});

		gtag('event', 'click', {
		  'event_category': 'Customer-Engagement-Add-Sms-Button',
		  'event_label': _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});
	});

	$('body').on("click", "button.add-push-notif-button", function () {
		ga('send', {
		  hitType: 'event',
		  eventCategory: 'Customer-Engagement-Add-Push-Notif-Button',
		  eventAction: 'click',
		  eventLabel: _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});

		gtag('event', 'click', {
		  'event_category': 'Customer-Engagement-Add-Push-Notif-Button',
		  'event_label': _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});
	});

	$('body').on("click", "a.submit-payment-button", function () {
		ga('send', {
		  hitType: 'event',
		  eventCategory: 'Billing-Submit-Payment',
		  eventAction: 'click',
		  eventLabel: _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});

		gtag('event', 'click', {
		  'event_category': 'Billing-Submit-Payment',
		  'event_label': _merchantId,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId
		});
	});

	$('body').on("click", "a.view-bill-button", function () {
		var _dimension3 = $(this).data('id');
		ga('send', {
		  hitType: 'event',
		  eventCategory: 'Billing-View-Bill',
		  eventAction: 'click',
		  eventLabel: _merchantId,
		  eventValue: _dimension3,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId,
		  'dimension3' : _dimension3
		});

		gtag('event', 'click', {
		  'event_category': 'Billing-View-Bill',
		  'event_label': _merchantId,
		  'value': _dimension3,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId,
		  'dimension3' : _dimension3
		});
	});

	$('body').on("click", "a.download-pdf-button", function () {
		var _dimension3 = $(this).data('id');
		ga('send', {
		  hitType: 'event',
		  eventCategory: 'Billing-Download-Pdf',
		  eventAction: 'click',
		  eventLabel: _merchantId,
		  eventValue: _dimension3,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId,
		  'dimension3' : _dimension3
		});

		gtag('event', 'click', {
		  'event_category': 'Billing-Download-Pdf',
		  'event_label': _merchantId,
		  'value': _dimension3,
		  'dimension1' : _merchantName,
		  'dimension2' : _merchantId,
		  'dimension3' : _dimension3
		});
	});

});

