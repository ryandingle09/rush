/* modules */

var Modules;
(function() {
    var AchievementUnlock = {
    	achivement_number: 1,
        init: function() {
        	var raw_html_data = '<div class="achievement" id="achievement-row"><div class="col-xs-4"><div class="input-group"><span class="input-group-addon"><i class="number">1</i></span><input type="text" class="form-control description" name="description[]" placeholder="Share 200 points to a friend"></div></div><div class="col-xs-1 noPadLeft noPadRight"><div class="input-group"><input type="text" class="form-control points" name="points[]" value="20" placeholder=""><span class="input-group-addon mul-color">x</span></div></div><div class="col-xs-1 noPadLeft noPadRight"><div class="input-group"><select name="multiplier[] multiplier" class="form-control"><option value="1">1</option><option value="2">2</option></select></div></div><div class="col-xs-2"><div class="input-group date fordatepkr"><input type="text" class="form-control fromDate" name="fromDate[]"><span class="input-group-addon"><i class="fa fa-calendar"></i></span></div></div><div class="col-xs-2"><div class="input-group date todatepkr"><input type="text" class="form-control toDate" name="toDate[]"><span class="input-group-addon"><i class="fa fa-calendar"></i></span></div></div><div class="col-xs-1"><a class="remove-achivement"><i class="fa del fa-times"></i></a></div></div><!--achiveMents-->';
        	if ($(".achievements").length == 1) {
		        $('#achievements-row').append(raw_html_data);
		        var $row = $('.achievements:last-child');
		        $row.find('.number').text(""+($('.achievements').length-1));
		        $row.find('.remove-achievement').on('click',function() {
		            $row.remove();
		            $('.achievements').each(function(index) {
		                $(this).find('.number').text(index);
		            });
		        });
		        $row.find(".description").val("");
		        $row.find(".points").val("0");
		        $row.find(".fromDate").val("");
		        $row.find(".toDate").val("");
		        $row.find('.fordatepkr').datetimepicker({
                    format: 'MM/DD/YYYY' 
		        });
		        $row.find('.todatepkr').datetimepicker({
                    format: 'MM/DD/YYYY'
		        });
        	}

		    $(".addAchievement").on('click',function(e) {
		        $('#achievements-row').append(raw_html_data);
		        var $row = $('.achievements:last-child');
		        $row.find('.number').text(""+($('.achievements').length-1));
		        $row.find('.remove-achievement').on('click',function() {
		            $row.remove();
		            $('.achievements').each(function(index) {
		                $(this).find('.number').text(index);
		            });
		        });
		        $row.find(".description").val("");
		        $row.find(".points").val("0");
		        $row.find(".fromDate").val("");
		        $row.find(".toDate").val("");
		        $row.find('.fordatepkr').datetimepicker({
                    format: 'MM/DD/YYYY' 
		        });
		        $row.find('.todatepkr').datetimepicker({
                    format: 'MM/DD/YYYY'
		        });
		    });
        } 
    };

    var Management = {
        init: function() {
            var emptyTableMessage = 'No matching records found';
            var excelExportButton = '<div class="text-center" id="export-btn-container"><button type="button" class="btn btn-default exportButton">Export to Excel</button></div>';

            $.extend($.fn.dataTableExt.oStdClasses, {
                'sWrapper': 'bootstrap-table dataTables_wrapper',
                'sFilter': 'pull-right search'
            });

            var datatableSpecs = {
                'order': [[ 0, 'desc' ]],
                'sDom': '<"fixed-table-toolbar"f><"excelButton hide"B><"customButton">rt<"bottom"ilp><"clear">',
                'buttons': [
                    'excel'
                ],
                'language': {
                    'emptyTable': emptyTableMessage
                },
                'oLanguage': {
                   'sSearch': ''
                },
                'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
                'scrollX': true,
                'sScrollX' : '100%',
                'scrollY': '300px',
                'sScrollY' : '100%',
                'autoWidth': false
            };

            var dataTableAjax = {
                processing: true,
                serverSide: true
            };

            var customerTableDefs = {
                "ajax": {
                    "url": "management/getCustomer"
                },

                "fnDrawCallback" : function(data){
                    var client_code_td = $('.client-code-td:not(.sorting)');
                    var with_val = false;
                    var client_code_column = $('#customerTable').DataTable().columns([2]);

                    client_code_td.each(function(index, elem){
                        if($(elem).html() !== ''){
                            with_val = true;
                        }
                    });

                    client_code_column.visible(with_val);
                }
            };

            var customerPunchcardTable = $('#customerTable.punchcard').DataTable( $.extend({}, datatableSpecs, dataTableAjax, customerTableDefs, {
                columns: [
                    { data: 'timestamp', name: 'Date Registered' },
                    { data: 'fullName', name: 'Customer Name',
                        render : function(data, type, row, meta){
                            return '<a href="#" class="editCustomerBtn editable client-name" data-customer-id="' + row.customerId  + '" data-customer-pin="' + row.PIN  + '">' + data + '</a>';
                        }
                    },
                    { data: 'client_code', name: 'Client Code' },
                    { data: 'mobileNumber', name: 'Mobile No.' },
                    { data: 'email', name: 'Email Address' },
                    { data: 'birthDate', name: 'Birthdate' },
                    { data: 'gender', name: 'Gender' },
                    { data: 'registrationChannel', name: 'Registration Channel' },
                    { data: 'branchName', name: 'Branch' }
                ],

                "columnDefs": [
                    { className: "editable client-code client-code-td", "targets": [ 2 ] },
                    { className: "editable mobile-number", "targets": [ 3 ] },
                    { className: "editable email-address", "targets": [ 4 ] }
                ]
            }));

            var customerLoyaltyTable = $('#customerTable.loyalty').DataTable( $.extend({}, datatableSpecs, dataTableAjax, customerTableDefs, {
                columns: [
                    { data: 'timestamp', name: 'Date Registered' },
                    { data: 'fullName', name: 'Customer Name',
                        render : function(data, type, row, meta){
                            var customer_id = row.customerId;
                            var customer_pin = row.PIN;
                            var customer_membership_id = (row.membership_id != null) ? row.membership_id : 0;

                            return '<a href="#" class="editCustomerBtn editable client-name" data-customer-id="' + customer_id  + '" data-membership-id="' + customer_membership_id + '" data-customer-pin="' + customer_pin  + '">' + data + '</a>';
                        }
                    },
                    { data: 'client_code', name: 'Client Code' },
                    { data: 'membership_level', name: 'Membership Level', defaultContent : "Regular Membership" },
                    { data: 'mobileNumber', name: 'Mobile No.' },
                    { data: 'email', name: 'Email Address' },
                    { data: 'birthDate', name: 'Birthdate' },
                    { data: 'gender', name: 'Gender' },
                    { data: 'registrationChannel', name: 'Registration Channel' },
                    { data: 'branchName', name: 'Branch' }
                ],

                "columnDefs": [
                    { className: "editable client-code client-code-td", "targets": [ 2 ] },
                    { className: "editable membership-level", "targets": [ 3 ] },
                    { className: "editable mobile-number", "targets": [ 4 ] },
                    { className: "editable email-address", "targets": [ 5 ] }
                ]
            }));

            $('#customerTableCustom').appendTo('#customerTable_wrapper .customButton');

            var customerGifTable = $('#customerGifTable').DataTable( $.extend({}, datatableSpecs, dataTableAjax, {
                columns: [
                    { data: 'timestamp', name: 'Date Registered' },
                    { data: 'fullName', name: 'Customer Name' },
                    { data: 'mobileNumber', name: 'Mobile No.' },
                    { data: 'email', name: 'Email Address' },
                    { data: 'grade_level', name: 'Emp ID' },
                    { data: 'firstName', name: 'Company' },
                    { data: 'lastName', name: 'Group' },
                    { data: 'middleName', name: 'Division' },
                ],

                "ajax": {
                    "url": "management/getCustomer"
                }
            }));

            if ($('#customerTable tbody tr.odd td, #customerGifTable tbody tr.odd td').text() != emptyTableMessage) {
                $('#customer').append(excelExportButton);
                $('#customer-gif').append(excelExportButton);
            }

            var pointsTable = $('#pointsTable.punchcard').DataTable(datatableSpecs);

            // initialize points datatable
            var pointsTable = $('#pointsTable.loyalty').DataTable( $.extend({}, datatableSpecs, dataTableAjax, {
                columns: [
                    { data: 'timestamp', name: 'Date Registered' },
                    { data: 'fullName', name: 'Customer Name' },
                    { data: 'mobileNumber', name: 'Mobile No.' },
                    { data: 'totalPoints', name: 'Earned' },
                    { data: 'usedPoints', name: 'Burned' },
                    { data: 'currentPoints', name: 'Available' },
                ],

                "ajax": {
                    "url": "management/getCustomerPoints",
                }
            }));

            if ($('#pointsTable tbody tr.odd td').text() != emptyTableMessage) {
                $('#points').append(excelExportButton);
            }

            if ( $("#reportsTable").data("fbcat") ) {
                var reportsDatatableSpecs = {
                    'order': [[ 0, 'desc' ]],
                    'sDom': '<"fixed-table-toolbar"f><"excelButton hide"B><"customButton">rt<"bottom"ilp><"clear">',
                    'buttons': [ {
                        extend: 'excel',
                        exportOptions: {
                          columns: [0,1,2,3,5,6]
                        }   
                    }],
                    'language': {
                        'emptyTable': emptyTableMessage
                    },
                    'oLanguage': {
                       'sSearch': ''
                    },
                    "columnDefs": [ {
                            "targets": [ 5,6 ],
                            "visible": false
                        }],
                    'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
                    'scrollX': true,
                    'sScrollX' : '100%',
                    'scrollY': '300px',
                    'sScrollY' : '100%',
                    'autoWidth': false
                };
            } else { 
                var reportsDatatableSpecs = {
                    'order': [[ 0, 'desc' ]],
                    'sDom': '<"fixed-table-toolbar"f><"excelButton hide"B><"customButton">rt<"bottom"ilp><"clear">',
                    'buttons': [ {
                        extend: 'excel',
                        exportOptions: {
                          columns: [0,1,2,4,5]
                        }   
                    }],
                    'language': {
                        'emptyTable': emptyTableMessage
                    },
                    'oLanguage': {
                       'sSearch': ''
                    },
                    "columnDefs": [ {
                            "targets": [ 4,5 ],
                            "visible": false
                        }],
                    'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
                    'scrollX': true,
                    'sScrollX' : '100%',
                    'scrollY': '300px',
                    'sScrollY' : '100%',
                    'autoWidth': false
                };
            }

            var codeMgmtTableDefs = {
                "ajax": {
                    "url": "management/getClientCode"
                }
            };

            var codeMgmtTable = $('#codeMgmtTable').DataTable( $.extend({}, datatableSpecs, dataTableAjax, codeMgmtTableDefs, {
                columns: [
                    { data: 'code', name: 'Client Code' },
                    { data: 'status', name: 'Status' },
                    { data: 'customer', name: 'Customer' },
                    { data: 'created_at', name: 'Date Created' },
                    { data: 'updated_at', name: 'Date Updated' },
                    { data: 'id', sortable: false, name: 'Action',
                        render : function(data, type, row, meta){
                            return '<a href="#" data-toggle="modal" data-id="' + row['id'] + '" data-code="' + row['code'] + '" data-status="' + row['status'] + '" data-rcustomer="' + row['customer'] + '" data-acustomer="'+ row['customer'] +'" data-target="#editClientCode" class="edit pull-left editClientCodeButton" onclick="return false"><i class="fa fa-pencil"></i> Edit</a>';
                        }
                    },
                ], 
                'buttons': [
                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4 ]
                        }
                    }
                ]
            }));

            if ($('#codeMgmtTable tbody tr.odd td').text() != emptyTableMessage) {
                $('#code-management').append(excelExportButton);
            }
            
            var branchesTableDefs = {
                "ajax": {
                    "url": "management/getBranches"
                }
            };

            var branchesTable = $('#branchTable').DataTable( $.extend({}, datatableSpecs, dataTableAjax, branchesTableDefs, {
                columns: [
                    { data: 'branchName', name: 'Branch Name' },
                    { data: 'address', name: 'Branch Address' },
                    { data: 'branch_logo', sortable: false, name: 'Branch Logo',
                        render : function(data, type, row, meta) {
                            return '<img src="' + row['branch_logo'] + '" class="imgPrev" alt="" height="64" width="64">';
                        }
                    },
                    { data: 'action', sortable: false, name: 'Action' }
                ],
                createdRow: function (row, data, index) {
                    $(row).attr('data-id', data['id']);
                    $(row).attr('data-name', data['branchName']);
                    $(row).attr('data-street-name', data['street_name']);
                    $(row).attr('data-district', data['district']);
                    $(row).attr('data-city', data['city']);
                    $(row).attr('data-zipcode', data['zipcode']);
                    $(row).attr('data-branchlogo', data['branch_logo']);
                    $(row).attr('data-show', data['show']);
                },
                'buttons': [
                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: [ 0, 1, 2]
                        }
                    }
                ]
            }));

            if ($('#branchTable tbody tr.odd td').text() != emptyTableMessage) {
                $('#branch').append(excelExportButton);
            }

            var employeesTableDefs = {
                "ajax": {
                    "url": "management/getEmployees"
                }
            };

            var employeesTable = $('#employeeTable').DataTable( $.extend({}, datatableSpecs, dataTableAjax, employeesTableDefs, {
                columns: [
                    { data: 'userId', name: 'Employee ID No.' },
                    { data: 'employeeName', name: 'Employee Name' },
                    { data: 'branchName', name: 'Branch Assigned' },
                    { data: 'employee_status', name: 'Status', sortable: false },
                    { data: 'action', sortable: false, name: 'Action' }
                ],
                createdRow: function (row, data, index) {
                    $(row).attr('data-id', data['userId']);
                    $(row).attr('data-employee', data['employeeName']);
                    $(row).attr('data-requirepass', data['requirePassword']);
                    $(row).attr('data-fourdigit', data['fourDigit']);
                    $(row).attr('data-branch', data['branchId']);
                },
                'buttons': [
                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3 ]
                        }
                    }
                ]
            }));

            if ($('#employeeTable tbody tr.odd td').text() != emptyTableMessage) {
                $('#employee').append(excelExportButton);
            }


            // initialize reports datatable
            var reportsTable = $('#reportsTable').DataTable(reportsDatatableSpecs);
            if ($('#reportsTable tbody tr.odd td').text() != emptyTableMessage) {
                $('#feedback').append(excelExportButton);
            }

            var $bootstrapSearchInput = $('div.bootstrap-table .search input');
            $bootstrapSearchInput
                .attr('type', 'text')
                .attr('placeholder', 'Search')
                .addClass('form-control');

            $('.dataTables_wrapper')
                .css('clear', 'none')
                .css('position', 'static');

            $('select[name=customerTable_length], select[name=customerGifTable_length], select[name=pointsTable_length], select[name=reportsTable_length]').unwrap();
            $('#customerTable_length, #customerGifTable_length, #pointsTable_length, #reportsTable_length').addClass('dataTables_info');

            // bind excel export button
            $('.tab-pane').on('click', '.exportButton', function(event) {
                $(this).parent().parent().find('.dataTables_wrapper .excelButton a').click();
            });

        	$('#tabs li').each(function() {
                if ($(this).hasClass('active')) {
                	Management.setActive($(this).find('a').attr('href'));
                }
        	});
			// change add button on tab click
			$(document).on("click", ".management .nav-tabs li a", function() {
			    Management.setActive($(this).attr("href"));
			});
			//-- end

            $('.bootstrap-table').on('click', '.view-message', function() {
                var $row = $(this).closest('tr');
                $('#view-report-message').html('');
                $('#view-report-date').html( $row.data('date') );
                $('#view-report-customer').html( $row.data('customer') );
                $('#view-report-mobile').html( $row.data('mobile') );
                if ( $row.data('type') == "text") {
                    $('#view-report-message').html( $row.data('message') );
                } else {
                    var _audio_file = '/repository/merchant/voice_mails/' + $row.data('merchantid') + '/' + $row.data('message');
                    var _html_audio = '<audio controls><source src="'+ _audio_file +'" type="audio/wav">Your browser does not support the audio element.</audio>';
                    $('#view-report-message').html( _html_audio );
                }
            });

            $('.bootstrap-table').on('click', '.branchEdit', function() {
                var $row = $(this).closest('tr');
                var $cell = $row.find('td');
                $('#editBranch form :text, #editBranch form textarea').css('border', 'transparent');
                $('#ebranchId').val($row.data('id'));
                $('#ebranchName').val($cell.eq(0).text());
                $('#eStreetName').val($row.data('street-name'));
                $('#eDistrict').val($row.data('district'));
                $('#eCity').val($row.data('city'));
                $('#eZipcode').val($row.data('zipcode'));
                $('#eBranchLogo').attr('src', $row.data('branchlogo'));
                $('#eShow').val($row.data('show'));
            });

		    $('.bootstrap-table').on('click', '.branchDelete', function() {
		        var $row = $(this).closest('tr');
		        var $cell = $row.find('td');
		        $('#branchToDelete').text($cell.eq(1).text());
		        $('#deleteBranchForm').attr('action',$(this).data('action'));
		    });

            $('.bootstrap-table').on('click', '.employeeEdit', function() {
                var $row = $(this).closest('tr');
                $('#eEmployeeId').val($row.data('id'));
                $('#eEmployeeName').val($row.data('employee'));
                $('#eRequirePassword').prop('checked', !!+$row.data('requirepass'))
                $('#eFourDigit').val($row.data('fourdigit'));
                $('#eSelectBranch').val($row.data('branch'));
            });

            $("#postaddEmployee").on('submit', function() {
                if ($("#employeeName").val().length == 0) {
                    alert('Please provide the Employee Name!');
                    $("#employeeName").css('border', 'solid 1px #FF0000');
                    return false;
                }
                if ($("#requirePassword").is(':checked')
                    &&($("#fourDigit").val().length != 4
                        || (/^\d\d\d\d$/.test($("#fourDigit").val()) === false)
                    )
                ) {
                    alert('Please provide the 4 Digit Code!');
                    $("#fourDigit").css('border', 'solid 1px #FF0000');
                    return false;
                }
            });

            $("#eEmployeeSubmit").on('submit', function(){
                if ($("#eRequirePassword").is(':checked') && $("#eFourDigit").val().length != 4) {
                    alert('Please provide the 4 Digit Code!');
                    $("#eFourDigit").css('border', 'solid 1px #FF0000');
                    return false;
                }
            });

		    $('.bootstrap-table').on('click', '.employeeDelete', function() {
                var $row = $(this).closest('tr');
                var $cell = $row.find('td');
                $('#employeeToDelete').text($cell.eq(0).text());
                $('#deleteEmployeeForm').attr('action',$(this).data('action'));
            });

            $('#addBranch').on('submit', function() {
                $('#addBranch form :text, #addBranch form textarea').css('border', 'transparent');
                var validation = true;
                var message = '';
                if ($('#branchName').val().length == 0) {
                    message += "Please provide the Branch Name!\n";
                    $('#branchName').css('border', 'solid 1px #FF0000');
                    validation = false;
                }
                if ($('#city').val().length == 0) {
                    message += "Please provide the City!\n";
                    $('#city').css('border', 'solid 1px #FF0000');
                    validation = false;
                }
                if ($('#streetName').val().length == 0) {
                    message += "Please provide the Street Name/Unit!\n";
                    $('#streetName').css('border', 'solid 1px #FF0000');
                    validation = false;
                }
                if ($('#zipcode').val().length == 0 || (/^\d\d\d\d$/.test($("#zipcode").val()) === false)) {
                    message += "Please provide a numeric 4-digit Zipcode!\n";
                    $('#zipcode').css('border', 'solid 1px #FF0000');
                    validation = false;
                }
                if ($('#district').val().length == 0) {
                    message += "Please provide the District!\n";
                    $('#district').css('border', 'solid 1px #FF0000');
                    validation = false;
                }
                if ($('#branchLogoFile').attr('validationStatus') == 'invalid') {
                    message += "Please provide Branch Logo (correct size)!\n";
                    validation = false;
                }
                if (!message.length == 0) {
                    alert(message);
                }

                return validation;
            });

            $('#editBranch').on('submit', function() {
                $('#editBranch form :text, #editBranch form textarea').css('border', 'transparent');
                var validation = true;
                var message = '';
                if ($('#ebranchName').val().length == 0) {
                    message += "Please provide the Branch Name!\n";
                    $('#ebranchName').css('border', 'solid 1px #FF0000');
                    validation = false;
                }
                if ($('#eCity').val().length == 0) {
                    message += "Please provide the City!\n";
                    $('#eCity').css('border', 'solid 1px #FF0000');
                    validation = false;
                }
                if ($('#eStreetName').val().length == 0) {
                    message += "Please provide the Street Name/Unit!\n";
                    $('#eStreetName').css('border', 'solid 1px #FF0000');
                    validation = false;
                }
                if ($('#eZipcode').val().length == 0 || (/^\d\d\d\d$/.test($("#eZipcode").val()) === false)) {
                    message += "Please provide a numeric 4-digit Zipcode!\n";
                    $('#eZipcode').css('border', 'solid 1px #FF0000');
                    validation = false;
                }
                if ($('#eDistrict').val().length == 0) {
                    message += "Please provide the District!\n";
                    $('#eDistrict').css('border', 'solid 1px #FF0000');
                    validation = false;
                }
                if ($('#eBranchLogoFile').attr('validationStatus') == 'invalid') {
                    message += "Please provide Branch Logo (correct size)!\n";
                    validation = false;
                }
                if (!message.length == 0) {
                    alert(message);
                }

                return validation;
            });

            var default_image = 'assets/images/addImage.jpg';

            var addBranchLogo = $(".addBranchLogo :file");
            if (typeof addBranchLogo != 'undefined') {
                addBranchLogo.on('showPreview', function(event) {
                    var getImagePath = URL.createObjectURL(event.target.files[0]);
                    if (addBranchLogo.attr("validationStatus") != "invalid") {
                        $('.addBranchLogo .imgPrev').attr('src', getImagePath);
                    } else {
                        $('.addBranchLogo .imgPrev').attr('src', default_image);
                    }
                });
            }

            var editBranchLogo = $(".editBranchLogo :file");
            if (typeof editBranchLogo != 'undefined') {
                console.log(editBranchLogo);
                editBranchLogo.on('showPreview', function(event) {
                    var getImagePath = URL.createObjectURL(event.target.files[0]);
                    if (editBranchLogo.attr("validationStatus") != "invalid") {
                        $('.editBranchLogo .imgPrev').attr('src', getImagePath);
                    } else {
                        $('.editBranchLogo .imgPrev').attr('src', default_image);
                    }
                });
            }

            
        },
        setActive: function(tag) {
    		if (tag == "#branch"){
		        $('.navTabsWrap .addNew').show();
                $('.navTabsWrap .addNew').attr("href","#addBranch");
                $('.navTabsWrap .addNew').html("<i class='fa fa-plus'></i> Add Branch");
		    }
		    else if (tag == "#employee") {
		        $('.navTabsWrap .addNew').show();
                $('.navTabsWrap .addNew').attr("href","#addEmployee");
                $('.navTabsWrap .addNew').html("<i class='fa fa-plus'></i> Add Employee");
		    } else if (tag == "#customer") {
                $('.navTabsWrap .addNew').hide();
		    } else if (tag == "#points") {
                $('.navTabsWrap .addNew').hide();
            } else if (tag == "#feedback") {
                $('.navTabsWrap .addNew').hide();
            } else if (tag == "#checkins") {
                $('.navTabsWrap .addNew').hide();
            } else if (tag == "#code-management") {
                $('.navTabsWrap .addNew').hide();
            } else if (tag == "#stamps-summary") {
                $('.navTabsWrap .addNew').hide();
            } else if (tag == "#class-summary") {
                $('.navTabsWrap .addNew').hide();
            }
        }
    };

    var Redemption = {
        init: function() {
            var raw_html_data = $('#redeemRowTemplate').html();
		    var redeem_number = 1;
		    if ($('.itemsRedeemed').length == 1) {
		        $('#redeem-rows').append(raw_html_data);
		        var $row = $('.itemsRedeemed:last-child');
		        $row.find('.number').text(""+($('.itemsRedeemed').length-1));
		        $row.find('.remove-redeem').on('click',function() {
		            $row.remove();
		            $('.itemsRedeemed').each(function(index) {
		                $(this).find('.number').text(index);
		            });
		        });
		        $row.find('.edit-redeem').css('display','none');
		        $row.find(".rProdName").val("");
		        $row.find(".rDescription").val("");
		        $row.find(".rPoints").val("0");
		        $row.find(".rRedeemId").val("0");
		        $row.find('.rProdImg .btn-file :file').change(function() {
		            var file = $(this)[0].files[0];
		            var getImagePath = URL.createObjectURL(file);
		            var prodImg = $row.find('.imgPrev');
		            prodImg.attr('src', getImagePath);
		            prodImg.css("display","block");
		            prodImg.siblings(".btn-file").css("display","none");
		        });
		    }

		    $('.itemsRedeemed').each(function(index) {
		        var $row = $(this);
		        $row.find('.number').text(index);
		        if (index > 0) {
		            $row.find('.remove-redeem').on('click',function() {
		                var id = $row.find('.rRedeemId').val();
		                $.post(window.location.href+"/delete",{redeemId: id},function (data,status) {
		                    $row.remove();
		                    $('.itemsRedeemed').each(function(index) {
		                        $(this).find('.number').text(index);
		                    });
		                });
		            });
		        }

		        $(this).find('.edit-redeem').on('click',function() {
		            var productName = $row.find('input[name="productName\[\]"]').val();
		            var productDescription = $row.find('input[name="productDescription\[\]"]').val();
		            var points = $row.find('input[name="points\[\]"]').val();
		            var redeemId = $row.find('input[name="redeemId\[\]"]').val();
                    var number = $row.find('.number').text();
		            var branch = $row.find('input[name="productBranch\[\]"]').val();
                    var productImagePreview = $row.find('.imgPrev').attr('src');

                    var redeemRowEditHtml = $('#redeemRowEditTemplate').html();
		            $row.html(redeemRowEditHtml);

		            $row.find('input[name="productName\[\]"]').val(productName);
		            $row.find('textarea[name="productDescription\[\]"]').text(productDescription);
		            $row.find('input[name="points\[\]"]').val(points);
		            $row.find('input[name="redeemId\[\]"]').val(redeemId);

                    $row.find('.number').text(number);
                    $row.find('.imgPrev').attr('src', productImagePreview);

                    if ( branch == 0 ) {
                        var _input = $row.find('.rBranch');
                        var _inputText = $row.find('.rBranchInput');
                        var _val = [];
                        _input.each( function() {
                            $(this).attr('checked', true);
                            _val.push( $(this).val() );
                        });
                        _inputText.val( _val );
                    } else {
                        var _branch = JSON.parse("[" + branch + "]");
                        var _input = $row.find('.rBranch');
                        _input.each( function() {
                            _branch = _branch.map(String);
                            if( jQuery.inArray( $(this).val() , _branch) !== -1 ) {
                                $(this).attr('checked', true);
                            }
                        });
                    }

		            $row.find('.edit-redeem').css('display','none');

		            $row.find('.remove-redeem').on('click',function() {
		                $row.remove();
		                var id = $row.find('.rRedeemId').val();
		                $.post(window.location.href+"/delete",{redeemId: id},function (data,status) {});
		                $('.itemsRedeemed').each(function(index) {
		                    $(this).find('.number').text(index);
		                });
		            });

                    var productImage = $row.find('.rProdImg .btn-file :file');
                    productImage.on('showPreview', function(event) {
                        var default_image = 'assets/images/addImage.jpg';
                        var getImagePath = URL.createObjectURL(event.target.files[0]);
                        var productImagePreview = productImage.parent().parent().parent().find('.imgPrev');
                        if (productImage.attr("validationStatus") != 'invalid') {
                            productImagePreview.attr('src', getImagePath);
                        } else {
                            productImagePreview.attr('src', default_image);
                        }
                    });

                    imageFileValidator.init();

                    $('.rBranch').unbind("change");
                    $('.rBranch').bind("change", function() {
                        var _input = $row.find('.rBranch');
                        var _inputText = $row.find('.rBranchInput');
                        var _val = [];
                        var _chcked = 0;
                        _input.each(function( index ) {
                            if ( $(this).is(':checked') ) {
                                _val.push( $(this).val() );
                                _chcked++;
                            }
                        });
                        if ( _chcked == 0 ) _val = 0;
                        _inputText.val( _val );
                    });
		        });

		        var $imgPrev = $('.rProdImg .imgPrev');
		        if ($imgPrev.attr('src') == 'assets/images/addImage.jpg') {
		            $imgPrev.css("display","none");
		            $imgPrev.siblings(".btn-file").css("display","block");
		        } else {
		            $imgPrev.css("display","block");
		            $imgPrev.siblings(".btn-file").css("display","none");
		        }
		    });

		    $(".addRedeemItem").on('click',function(e) {
		        $('#redeem-rows').append(raw_html_data);
		        var $row = $('.itemsRedeemed:last-child');
		        $row.find('.number').text(""+($('.itemsRedeemed').length-1));
		        $row.find('.remove-redeem').on('click',function() {
		            $row.remove();
		            $('.itemsRedeemed').each(function(index) {
		                $(this).find('.number').text(index);
		            });
		        });
		        $row.find('.edit-redeem').css('display','none');
		        $row.find(".rProdName").val("");
		        $row.find(".rDescription").val("");
		        $row.find(".rPoints").val("0");
		        $row.find(".rRedeemId").val("0");

                var productImage = $row.find('.rProdImg .btn-file :file');
                productImage.on('showPreview', function(event) {
                    var default_image = 'assets/images/addImage.jpg';
                    var getImagePath = URL.createObjectURL(event.target.files[0]);
                    var productImagePreview = productImage.parent().parent().parent().find('.imgPrev');
                    if (productImage.attr('validationStatus') != 'invalid') {
                        productImagePreview.attr('src', getImagePath);
                    } else {
                        productImagePreview.attr('src', default_image);
                    }
                });

                imageFileValidator.init();

                $('.rBranch').unbind("change");
                $('.rBranch').bind("change", function() {
                    var _input = $row.find('.rBranch');
                    var _inputText = $row.find('.rBranchInput');
                    var _val = [];                    
                    _input.each(function( index ) {
                        if ( $(this).is(':checked') ) {
                            _val.push( $(this).val() );
                        }
                    });
                    _inputText.val( _val );
                });
		    });

            $('.redemption .saveButton').on('click', function() {
                var do_submit = true;
                var default_image = 'assets/images/addImage.jpg';
                var msgPleaseSupplyImage = 'Please upload the required image.';
                var itemsCount = 0;
                $.each($('#redeem-rows .itemsRedeemed'), function(key, item) {
                    var itemFile = $(item).find(':file');
                    if (itemFile != undefined) {
                        var itemFileValidationStatus = itemFile.attr('validationStatus');
                        if (itemFileValidationStatus == undefined && $(item).find('.imgPrev').attr('src') == default_image) {
                            $(item).find('.error-msg').text(msgPleaseSupplyImage);
                            $(item).find('.error-msg').removeClass('hide');
                            do_submit = false;
                        }
                        if (itemFileValidationStatus == 'invalid') {
                            do_submit = false;
                        }
                    }
                    itemsCount++;
                });

                if (itemsCount == 0) {
                    do_submit = false;
                }

                return do_submit;
            });
        }
    };

    var Design = {
    	init: function() {
    		var hash = window.location.hash;

    	    if (hash == '#tablet') {
                Design.designTablet($('#btnTablet'));
            } else if (hash == '#mobile') {
                Design.designMobile('#btnMobile');
            } else if (hash == '#card') {
                Design.designCard($('#btnCard'));
            } else if (hash == '#stamp-icon') {
            }

            if ($('#mobileAppDesign').length) {
                MobileAppDesign.create();
            }
            if ($('#merchantAppDesign').length) {
                MerchantAppDesign.create();
            }

            $("#btnTablet").on('click',function(){
			    Design.designTablet($(this));
			    window.location.hash = "tablet";
			});

			$("#btnCard").on('click',function(){
			    Design.designCard($(this));
			    window.location.hash = "card";
			});

			$("#btnMobile").on('click',function(){
			    Design.designMobile($(this));
			    window.location.hash = "mobile";
			});

			$('#merchantName').on('keyup',function() {
		        var value = $(this).val();
		        if (value == "") {
		            value = "Merchant Name";
		        }
		        $('.topPane').text(value);
		    });
    	},
    	designTablet: function(button) {
		    $(button).removeClass('btn-default').addClass('btn-primary active');
		    $('#btnMobile').removeClass('btn-primary active').addClass('btn-default');
		    $('.mobileTab,.cardTab').hide();
		    $('.tabletTab').show();
    	},
    	designMobile: function(button) {
    		$(button).removeClass('btn-default').addClass('btn-primary active');
    		$('#btnTablet,#btnStampIcon').removeClass('btn-primary active').addClass('btn-default');
		    $('.tabletTab,.stampIconTab').hide();
		    $('.mobileTab').show();
    	},
    	designCard: function(button) {
    		$(button).removeClass('btn-default').addClass('btn-primary active');
		    $('#btnMobile,#btnTablet').removeClass('btn-primary active').addClass('btn-default');
		    $('.mobileTab,.tabletTab').hide();
		    $('.cardTab').show();

		    CardDesign.create();
    	}
    }

    var Promos = {
        init: function() {
            var defaultDate = $('#durationStart').val();
            if (defaultDate == '') {
                defaultDate = new Date();
            }
        	$('.durationStart').datetimepicker({
                format: 'YYYY-MM-DD',
                defaultDate: defaultDate
		    });

        	$('.durationEnd').datetimepicker({
                format: 'YYYY-MM-DD'
		    });

		    //- stay addProduct in promos module
		    if (window.location.hash == "#addProduct"){
		        Promos.apProd($(".apProd"));
		    } else if (window.location.hash == "#addAmount"){
		        Promos.apAmnt($(".apAmnt"));
		    }
		    //-- end

			// Promos amount or product
			$(".apProd").on('click',function(){
			    Promos.apProd($(this));
			});
			$(".apAmnt").on('click',function(){
			    Promos.apAmnt($(this));
			});		    
        },
        apProd: function(prop) {
        	window.location.hash = "addProduct";
		    $(prop).addClass('active');
		    $(".apAmnt").removeClass('active');
		    $(".earningReward1").slideUp('normal');
		    $(".earningReward2").slideDown('normal');
        },
        apAmnt: function(prop) {
        	window.location.hash = "addAmount";
		    $(prop).addClass('active');
		    $(".apProd").removeClass('active');
		    $(".earningReward2").slideUp('normal');
		    $(".earningReward1").slideDown('normal');
        }
    };

    var TransactionHistory = {
        init: function() {
            var emptyTableMessage = 'No matching records found';
            var excelExportButton = '<div class="text-center" id="export-btn-container"><button type="button" class="btn btn-default exportButton">Export to Excel</button></div>';

            $.extend( $.fn.dataTable.defaults, {
                language: {
                    "processing": "Loading. Please wait..."
                }
            });
            $.extend($.fn.dataTableExt.oStdClasses, {
                'sWrapper': 'bootstrap-table dataTables_wrapper',
                'sFilter': 'pull-right search'
            });

            $.fn.dataTableExt.afnFiltering.push(
                function(oSettings, aData, iDataIndex) {
                    var iFini = $('#dateStart input').val();
                    var iFfin = $('#dateEnd input').val();
                    var iStartDateCol = 0;
                    var iEndDateCol = 0;

                    var datofini = aData[iStartDateCol].substring(0, 10);
                    var datoffin = aData[iEndDateCol].substring(0, 10);

                    if (iFini === "" && iFfin === "") {
                        return true;
                    } else if (iFini <= datofini && iFfin === "") {
                        return true;
                    } else if (iFfin >= datoffin && iFini === "") {
                        return true;
                    } else if (iFini <= datofini && iFfin >= datoffin) {
                        return true;
                    }
                    return false;
                }
            );

            var datatableSpecs = {
                'order': [[ 0, 'desc' ]],
                'sDom': '<"fixed-table-toolbar"f><"excelButton hide"B><"customButton">rt<"bottom"ilp><"clear">',
                'buttons': [
                    'excel'
                ],
                'language': {
                    'emptyTable': emptyTableMessage
                },
                'oLanguage': {
                   'sSearch': ''
                },
                'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
                'processing': true,
                'scrollX': true,
                'sScrollX' : '100%',
                'scrollY': '300px',
                'sScrollY' : '100%',
                'autoWidth': false
            };

            var dataTableAjax = {
                'processing': true,
                'serverSide': false,
                'deferLoading': 0,
                'deferRender': true,
                'paging': true,
            };

            var pageDatatables = ['historyTable', 'productTransactionTable','guestTransactionTable', 'attendanceTable', 'attendeesTable' ];

            // initialize datatable
            var table = $('#productTransactionTable, #guestTransactionTable').DataTable(datatableSpecs);
            var attendanceTable = $('#attendanceTable').DataTable( $.extend({}, datatableSpecs, dataTableAjax, {
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'status', name: 'status' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'updated_at', name: 'updated_at' },
                    { data: 'action', name: 'action' }
                ],
                "columnDefs": [
                    {   "targets": -1,
                        "data": null,
                        "defaultContent": '<a href="#"><i class="fa fa-money"></i>View Attendees</a>'
                    },
                    {   "targets": 2,
                        "render": function ( data, type, row ) {
                            var statusString = {
                                '1': 'Inactive',
                                '2': 'Active',
                                '3': 'Processed',
                                '4': 'Processing'
                            };
                            var statusDisplay = {
                                '1': 'New',
                                '2': 'Active',
                                '3': 'Finished',
                                '4': 'Processing'
                            };
                            var status = statusString[row['status']];
                            var statusText = statusDisplay[row['status']];
                            return '<span class="stat s'+status+'">'+statusText+'</span>';
                        }
                    }
                ]
            }));
            var attendeesTable = $('#attendeesTable').DataTable( $.extend({}, datatableSpecs, {
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'date_time', name: 'date_time' },
                    { data: 'client_id', name: 'client_id' },
                    { data: 'client', name: 'client' },
                    { data: 'type', name: 'type' },
                    { data: 'visit_location', name: 'visit_location' },
                    { data: 'attendance_id', name: 'attendance_id' },
                    { data: 'updated_at', name: 'updated_at' }
                ]
            }));

            var historyTableSpecs = {
                "paging": true,
                'order': [[ 0, 'desc' ]],
                'sDom': '<"fixed-table-toolbar"f><"excelButton hide"B><"customButton">rt<"bottom"ilp><"clear">',
                'buttons': [
                    'excel'
                ],
                'language': {
                    'emptyTable': emptyTableMessage
                },
                'oLanguage': {
                   'sSearch': ''
                },
                'lengthMenu': [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
                'processing': true,
                'scrollX': true,
                'sScrollX' : '100%',
                'scrollY': '300px',
                'sScrollY' : '100%',
                'autoWidth': false
            };

            var historyTableAjax = {
                "processing": true,
                "serverSide": true,
            };
            
            if ( $('.punchcard-history-table').length > 0 ) {
                var historyTable = $('#historyTable').DataTable( $.extend({}, historyTableSpecs, historyTableAjax, {
                    "ajax": {
                        "url": "transactionhistory/gethistory",
                        "dataType": "json",
                        data: function (d) { 
                            d.minDate = $('#dateStart input').val(); 
                            d.maxDate = $('#dateEnd input').val();
                            },
                    },
                    columns: [
                        { data: 'timestamp', name: 'timestamp' },
                        { data: 'promoTitle', name: 'promoTitle' },
                        { data: 'transactionType', name: 'transactionType' },
                        { data: 'customerName', name: 'customerName' },
                        { data: 'amountPaidWithCash', name: 'amountPaidWithCash' },
                        { data: 'stamps', name: 'stamps' },
                        { data: 'transaction_ref', name: 'transaction_ref' },
                        { data: 'branchName', name: 'branchName' },
                        { data: 'employeeNumber', name: 'employeeNumber' }
                    ],
                    "columnDefs": [
                        {   "targets": 9,
                            "data": null,
                            "render": function ( data, type, row ) {
                                var elem = '';

                                elem += '<a class="transaction_receipts_view" data-id="' + row['id'] + '"><i class="fa fa-search" aria-hidden="true"></i></a>';

                                if(typeof row['voucher_img'] !== "undefined"){
                                    elem += '<a download target="_blank" href="' + row['voucher_img'] + '" class="transaction_receipts_download"><i class="fa fa-download" aria-hidden="true"></i></i></a>';
                                }

                                return elem;
                            }
                        },
                        {   "targets": 10,
                            "data": null,
                            "render": function ( data, type, row ) {
                                if ( row['transactionType'] == 'purchase-package' && row['stamps'] <= 0 ) {
                                    return '<a href="#" data-toggle="modal" data-target="#voidPackageTransaction" data-action="transactionhistory/void-punchcard/' + row['transaction_ref'] + '" class="voidPackageTransaction" onclick="return false"><i class="fa fa-close"></i> void</a>';
                                }
                                return '';
                            }
                        }
                    ],

                    "fnDrawCallback" : function(data){
                        var receipt_header = $('th#receipt-header');
                        var client_code_column = $('#historyTable').DataTable().columns([9]);

                        if(!receipt_header.hasClass('with-receipt')){
                            client_code_column.visible(false);
                        }
                    }
                }));
            }
            
            if ( $('.points-history-table').length > 0 ) {
                var historyTable = $('#historyTable').DataTable( $.extend({}, historyTableSpecs, historyTableAjax, {
                    "ajax": {
                        "url": "transactionhistory/gethistory",
                        "dataType": "json",
                        data: function (d) { 
                            d.minDate = $('#dateStart input').val(); 
                            d.maxDate = $('#dateEnd input').val();
                            },
                    },
                    columns: [
                        { data: 'timestamp', name: 'timestamp' },
                        { data: 'transactionType', name: 'transactionType' },
                        { data: 'transaction_method ', name: 'Transaction Method',
                            render : function(data, type, row){
                                var transaction_method = row.transaction_method;

                                if(transaction_method){
                                    return transaction_method;
                                }

                                return " ";
                            }
                        },
                        { data: 'customerName', name: 'customerName' },
                        { data: 'amountPaidWithCash', name: 'amountPaidWithCash' },
                        { data: 'points', name: 'points' },
                        { data: 'quantity', name: 'quantity' },
                        { data: 'claimed', name: 'claimed' },
                        { data: 'reward', name: 'reward' },
                        { data: 'receiptReferenceNumber', name: 'receiptReferenceNumber' },
                        { data: 'branchName', name: 'branchName' },
                        { data: 'employeeNumber', name: 'employeeNumber' },
                    ],
                    "columnDefs": [
                        {   "targets": 12,
                            "data": null,
                            "render": function ( data, type, row ) {
                                var today = new Date();
                                var timestamp = new Date( Date.parse( row['timestamp'] ) );
                                var limit_date = new Date();
                                limit_date.setDate( limit_date.getDate()-30 );
                                if ( ( timestamp <= today && timestamp > limit_date ) && row['transactionType'] == 'earn' && row['related_transactions_count'] == 0 ) {
                                    return '<a href="#" data-toggle="modal" data-target="#voidEarn" data-action="transactionhistory/earn/void/'+ row['transactionReferenceCode'] +'" onclick="return false" class="voidEarn"><i class="fa fa-close"></i> void</a>';
                                }

                                if ( row['transactionType'] == 'purchase-package' ) {
                                    return '<a href="#" data-toggle="modal" data-target="#voidPackageTransaction" data-action="transactionhistory/void-package/' + row['transactionReferenceCode'] + '" class="voidPointsPackageTransaction" onclick="return false"><i class="fa fa-close"></i> void</a>';
                                }
                                return '';
                            }
                        }
                    ],
                    initComplete: function () {
                        var target_table = $('#historyTable');
                        var api = this.api();

                        if (target_table.attr('data-enable-transaction-method') === "0") {
                            api.column(2).visible(false);
                        }
                    }
                }));
            }

            // append export button
            $.each(pageDatatables, function( index, value ) {
                if ($('#' + value + ' tbody tr.odd td').text() != emptyTableMessage) {
                    $('#' + value + '_wrapper').parent().append(excelExportButton);
                }

                if(value === 'historyTable'){
                    console.log(historyTable);
                    $('#export-btn-container').append('<button type="button" class="btn btn-default" style="margin: 0 10px;" id="export-all-btn">Export All to Excel</button>');
                }
            });

            $('#export-all-btn').on('click', function(){
                var loading_gif = $('#loading');

                loading_gif.removeClass('hidden');

                $.ajax({
                    "url" : "transactionhistory/get-all-history",
                    "dataType" : "json",
                    "method" : "GET",
                    "success" : function(response){
                        var wb = XLSX.utils.book_new();
                        if ( $('.punchcard-history-table').length > 0 ){
                            var keys = [
                                'timestamp',
                                'promoTitle',
                                'transactionType',
                                'customerName',
                                'mobileNumber',
                                'amountPaidWithCash',
                                'stamps',
                                'transaction_ref',
                                'branchName',
                                'employeeNumber'
                            ];

                            var keys_label = [
                                'Date & Time',
                                'Promo Title',
                                'Transaction Type',
                                'Customer Name',
                                'Mobile Number',
                                'Transaction Amount',
                                'Stamps',
                                'Ref./OR No.',
                                'Branch',
                                'Emp#'
                            ];
                        }

                        if ( $('.points-history-table').length > 0 ){
                            var keys = [
                                'timestamp',
                                'transactionType',
                                'customerName',
                                'mobileNumber',
                                'amountPaidWithCash',
                                'points',
                                'quantity',
                                'claimed',
                                'reward',
                                'receiptReferenceNumber',
                                'branchName',
                                'employeeNumber'
                            ];

                            var keys_label = [
                                'Date & Time',
                                'Transaction Type',
                                'Customer Name',
                                'Mobile Number',
                                'Transaction Amount',
                                'Points',
                                'Qty(R)',
                                'Qty(C)',
                                'Reward',
                                'Ref./OR No.',
                                'Branch',
                                'Emp#'
                            ];
                        }

                        wb.SheetNames.push("Sheet 1");

                        var ws_data = [keys_label];

                        response.forEach(function(item) {
                            var result = [];

                            keys.forEach(function(key) {
                                tmp_item = (item[key] === null) ? " " : item[key];

                                result.push(tmp_item);
                            });

                            ws_data.push(result);
                        });

                        var ws = XLSX.utils.aoa_to_sheet(ws_data);
                        wb.Sheets["Sheet 1"] = ws;
                        var wbout = XLSX.write(wb, {bookType:'xlsx',  type: 'binary'});

                        function s2ab(s) {
                            var buf = new ArrayBuffer(s.length);
                            var view = new Uint8Array(buf);
                            for (var i=0; i<s.length; i++) view[i] = s.charCodeAt(i) & 0xFF;

                            return buf;
                        }

                        saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), 'CMS - RUSH.xlsx');

                        loading_gif.addClass('hidden');
                    }
                })
            });

            $('#dateStart, #dateEnd').datetimepicker({
                format: 'YYYY-MM-DD'
            });

            $('#dateStart, #dateEnd').on('dp.change', function(e) {
                table.draw();
                historyTable.draw();
                if ($(this).attr('id') == 'dateStart') {
                    $("#dateEnd").data("DateTimePicker").minDate($("#dateStart").data("DateTimePicker").date().format('YYYY-MM-DD'));
                } else {
                    $("#dateStart").data("DateTimePicker").maxDate($("#dateEnd").data("DateTimePicker").date().format('YYYY-MM-DD'));
                }
            });

            var $bootstrapSearchInput = $('div.bootstrap-table .search input');
            $bootstrapSearchInput
                .attr('type', 'text')
                .attr('placeholder', 'Search')
                .addClass('form-control');

            $('.dataTables_wrapper')
                .css('clear', 'none')
                .css('position', 'static');

            $('#content').removeClass('hide');

            $.each(pageDatatables, function( index, value ) {
                $('select[name=' + value + '_length]').unwrap();
                $('#' + value + '_length').addClass('dataTables_info');
            });

            // bind excel export button
            $('.transHistory').on('click', '.exportButton', function(event) {
                $(this).parent().parent().find('.dataTables_wrapper .excelButton a').click();
            });

            $('.productTransactions').on('click', '.exportButton', function(event) {
                $(this).parent().parent().find('.dataTables_wrapper .excelButton a').click();
            });

            $('.guestTransHistory').on('click', '.exportButton', function(event) {
                $(this).parent().parent().find('.dataTables_wrapper .excelButton a').click();
            });

            $('.pointsSeeding').on('click', '.exportButton', function(event) {
                $(this).parent().parent().find('.dataTables_wrapper .excelButton a').click();
            });

            // add custom buttons
            $('#attendanceTable_wrapper .customButton').append('<button href="#addAttendance" class="btn btn-primary" data-toggle="modal" onclick="return false" style="margin-top:20px;">Add Attendance</button>');

            $('#attendeesTable_wrapper .customButton').before('<button href="#clientEarn" class="btn btn-primary hide" data-toggle="modal" onclick="return false" style="margin-top:20px;">Process Stamps</button>');

            // append spinner icon
            $('.dataTables_processing').prepend( '<i class="fa fa-refresh fa-spin"></i>' );
        }
    };

    var Help = {

    };

    var Settings = {
        init: function() {
            // preloaded images
            var default_image = 'assets/images/addImage.jpg';
            if ($('.appstoreLogo .imgPrev').attr('src') == '') {
                $('.appstoreLogo .imgPrev').attr('src', default_image);
            }
            var appstoreLogo = $(".appstoreLogo .btn-file :file");
            if (typeof appstoreLogo != 'undefined') {
                appstoreLogo.on('showPreview', function(event) {
                    var getImagePath = URL.createObjectURL(event.target.files[0]);
                    if (appstoreLogo.attr("validationStatus") != "invalid") {
                        $('.appstoreLogo .imgPrev').attr('src', getImagePath);
                        $('.appstoreLogo').attr('src', getImagePath);
                    } else {
                        $('.appstoreLogo .imgPrev').attr('src', default_image);
                    }
                });
            }

            $(".appstoreLogo input[type='submit']").on('click', function(event) {
                if (appstoreLogo.attr("validationStatus") == 'invalid') {
                    return false;
                }
                return true;
            });
        }
    };

    var Addons = {
    	init: function() {
            Design.designCard($('#btnCard'));

			$("#btnCard").on('click',function(){
			    Design.designCard($(this));
			    window.location.hash = "card";
			});

			$('#merchantName').on('keyup',function() {
		        var value = $(this).val();
		        if (value == "") {
		            value = "Merchant Name";
		        }
		        $('.topPane').text(value);
		    });
    	},
    	designCard: function(button) {
    		$(button).removeClass('btn-default').addClass('btn-primary active');
		    $('#btnMobile,#btnTablet').removeClass('btn-primary active').addClass('btn-default');
		    $('.mobileTab,.tabletTab').hide();
		    $('.cardTab').show();

		    CardDesign.create();
    	}
    } 

    var ChangePass = {
        init: function() {
        	$("#changePassword").submit(function(e) {
        		// e.preventDefault();
        	}).validate({
                rules: {
                	old_password: {
                		required: true,
                		minlength: 6,
                		maxlength: 32
                	},
                	new_password: {
                		required: true,
                		minlength: 6,
                		maxlength: 32
                	},
                	confirm_password: {
                		equalTo:"#new_password",
                		required: true,
                		minlength: 6,
                		maxlength: 32
                	}
                },
                messages: {
                	old_password: "Please provide your old password",
                	new_password: "Please provide your new password",
                	confirm_password: "Your new password and confirmation password does not match"
                },
                submitHanlder: function(form) {
                	form.submit();
                }
            })        	
        }
    }

    var AccountView = {
        init: function() {
        	$("#changePassword").submit(function(e) {
        		// e.preventDefault();
        	}).validate({
                rules: {
                	old_password: {
                		required: true,
                		minlength: 6,
                		maxlength: 32
                	},
                	new_password: {
                		required: true,
                		minlength: 6,
                		maxlength: 32
                	},
                	confirm_password: {
                		equalTo:"#new_password",
                		required: true,
                		minlength: 6,
                		maxlength: 32
                	}
                },
                messages: {
                	old_password: "Please provide your old password",
                	new_password: "Please provide your new password",
                	confirm_password: "Your new password and confirmation password does not match"
                },
                submitHanlder: function(form) {
                	if (form.valid()) {
                	    form.submit();
                    } else {
                    	return false;
                    }
                }
            })
        }
    }

    var quickstartNotification = function() {
    	$("#notes").on('click',function(e) {
    		window.location="/cpanel/quickstart";
    	});
    }

    var imageFileValidator = {
        init : function() {
            $(".fuImage").each(function () {
                $(this).on('validateImageSize', function(e) {
                    var ew = $(this).attr('ew');
                    var eh = $(this).attr('eh')
                    var aw = $(this).attr('aw');
                    var ah = $(this).attr('ah');
                    var errorMsg = $(this).parent().parent().parent().find('.error-msg');

                    if (ew != aw || eh != ah) {
                        $(this).attr('validationStatus', 'invalid');
                        errorMsg.text("Image does not meet the image size requirement. Please check again.");
                        errorMsg.removeClass('hide');
                    } else {
                        $(this).attr('validationStatus', 'valid');
                        errorMsg.addClass('hide');
                    }
                    $(this).trigger('showPreview');
                });

                $(this).fileupload({
                    dataType: 'json',
                    autoUpload: false,
                    acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                    maxFileSize: 999000,
                    disableImageResize: true,
                    previewMaxWidth: 200,
                    previewMaxHeight: 200,
                    previewCrop: false,
                    replaceFileInput:false
                }).on('fileuploadprocessalways', function (e, data) {
                    var that = $(this);
                    var file = data.files[0];
                    var dimensionIsValid = true;

                    var fr = new FileReader;
                    fr.onload = function() {
                        var img = new Image;
                        img.onload = function() {
                            that.attr("aw", img.width);
                            that.attr("ah", img.height);
                            that.trigger('validateImageSize');
                        };
                        img.src = fr.result;
                    };
                    fr.readAsDataURL(file);
                });
            });
        }
    }

    var Attendees = {
        init: function() {
            var emptyTableMessage = 'No matching records found';
            var excelExportButton = '<div class="text-center"><button type="button" class="btn btn-default exportButton">Export to Excel</button></div>';

            $.extend($.fn.dataTableExt.oStdClasses, {
                'sWrapper': 'bootstrap-table dataTables_wrapper',
                'sFilter': 'pull-right search'
            });

            var datatableSpecs = {
                'order': [[ 0, 'desc' ]],
                'sDom': '<"fixed-table-toolbar"f><"excelButton hide"B><"customButton">rt<"bottom"ilp><"clear">',
                'buttons': [
                    'excel'
                ],
                'language': {
                    'emptyTable': emptyTableMessage
                },
                'oLanguage': {
                   'sSearch': ''
                },
                'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']]
            };

            var dataTableAjax = {
                processing: true,
                serverSide: true,
            };

            var attendeesTable = $('#attendeesTable').DataTable( $.extend({}, datatableSpecs, dataTableAjax, {
                columns: [
                    { data: 'timestamp', name: 'Date Registered' },
                    { data: 'fullName', name: 'Customer Name' },
                    { data: 'mobileNumber', name: 'Mobile No.' },
                    { data: 'email', name: 'Email Address' },
                    { data: 'uuid', name: 'uuid' },
                    { data: 'attendee_status', name: 'confirm' },
                    { data: null, name: 'action' }
                ],

                "ajax": {
                    "url": "management/getCustomer"
                },

                "columnDefs": [
                    {   "targets": -3,
                        "visible": false
                    },
                    {   "targets": -1,
                        "data": null,
                        "render": function ( data, type, row ) {
                            return '<a href="#" data-toggle="modal" data-target="#editAttendee" data-uuid="' + row['uuid'] + '" onclick="return false" class="attendeeEdit"><i class="fa fa-pencil"></i>Edit</a>&nbsp;<a href="#" data-toggle="modal" data-target="#deleteAttendee" data-uuid="' + row['uuid'] + '" onclick="return false" class="attendeeDelete"><i class="fa fa-trash"></i>Delete</a>';
                        }
                    },
                    {   "targets": -2,
                        "data": null,
                        "render": function ( data, type, row ) {
                            var statusDisplay = {
                                '0': 'no',
                                '1': 'yes'
                            };
                            var status = statusDisplay[row['attendee_status']];
                            var statusText = statusDisplay[row['attendee_status']];
                            return ((row['attendee_status'] == 0) ? '<a href="#confirmAttendance" data-toggle="modal" onclick="return false" class="attendeeConfirm" data-uuid="' + row['uuid'] + '">' : '') + '<span class="attendance ' + status + '">' + statusText + '</span>' + ((row['attendee_status'] == 0) ? '</a>' : '');
                        }
                    }
                ],
                'buttons': [
                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 5 ]
                        }
                    }
                ]
            }));

            if ($('#attendeesTable tbody tr.odd td').text() != emptyTableMessage) {
                $('#attendees').append(excelExportButton);
            }

            var $bootstrapSearchInput = $('div.bootstrap-table .search input');
            $bootstrapSearchInput
                .attr('type', 'text')
                .attr('placeholder', 'Search')
                .addClass('form-control');

            $('.dataTables_wrapper')
                .css('clear', 'none')
                .css('position', 'static');

            $('select[name=attendeesTable_length]').unwrap();
            $('#attendeesTable_length').addClass('dataTables_info');

            // bind excel export button
            $('.tab-pane').on('click', '.exportButton', function(event) {
                $(this).parent().parent().find('.dataTables_wrapper .excelButton a').click();
            });

            $('#tabs li').each(function() {
                if ($(this).hasClass('active')) {
                    Management.setActive($(this).find('a').attr('href'));
                }
            });
            // change add button on tab click
            $(document).on("click", ".management .nav-tabs li a", function() {
                Management.setActive($(this).attr("href"));
            });
            //-- end

            $('.bootstrap-table').on('click', '.attendeeEdit', function() {
                var $row = $(this).closest('tr');
                var $cell = $row.find('td');
                $('#e_attendeeName').val($cell.eq(1).text());
                $('#e_attendeeMobile').val($cell.eq(2).text().substr(1));
                $('#e_attendeeEmail').val($cell.eq(3).text());
                $('#e_attendeeAttendance').prop('checked', ($cell.eq(4).text() == 'yes') ? true: false);
                $('#e_attendee').val($(this).data('uuid'));
            });

            $('.bootstrap-table').on('click', '.attendeeDelete', function() {
                var $row = $(this).closest('tr');
                var $cell = $row.find('td');
                $('#attendeeToDelete').text($cell.eq(1).text());
                $('#attendee').val($(this).data('uuid'));
            });

            $('.bootstrap-table').on('click', '.attendeeConfirm', function() {
                var $row = $(this).closest('tr');
                var $cell = $row.find('td');
                $('#attendeeToConfirm').text($cell.eq(1).text());
                $('#c_attendee').val($(this).data('uuid'));
            });
        }
    };

    $(document).ready(function() {
    	if (Config.quicksetup != "none") {
    		if (Config.quicksetup != "loyalty") {
    			MerchantAppDesign.create(0);
                PunchcardDesign.create();
                MobileAppDesign.create();
            } else {
            	MobileAppDesign.create();
            	MerchantAppDesign.create(0);
            }
    	} else {
    		quickstartNotification();
    	}

    	if (Config.module == "achievementunlock") {
            AchievementUnlock.init();
        } else if (Config.module == "management") {
            Management.init();
        } else if (Config.module == "redemption") {
            Redemption.init();
        } else if (Config.module == "design") {
        	if (Config.quicksetup == "none") {
        	    Design.init();
            }
        } else if (Config.module == "addons") {
        	CardDesign.create();
            Addons.init();
        } else if (Config.module == "promos") {
        	Promos.init();
        } else if (Config.module == "settings") {
        	Settings.init();
        } else if (Config.module == "welcome") {
        	ChangePass.init();
        } else if (Config.module == "account") {
            AccountView.init();
        } else if (Config.module == "dashboard") {
        	Settings.init();
        } else if (Config.module == "transactionhistory") {
            TransactionHistory.init();
        } else if (Config.module == "attendees") {
            Attendees.init();
        }

        imageFileValidator.init();
    });
})();