$(document).ready(function(){
    var emptyTableMessage = 'No matching records found';
    var table = $('#clientlogos-table').DataTable({
        'initComplete': function() {
            $('.dataTables_length').insertBefore('.dataTables_info');
            $('.dataTables_length select').unwrap('label');
        },
        'sDom': '<"fixed-table-toolbar"f><"excelButton hide"B>t<"bottom"ilp><"clear">',
        'buttons': [
            'excel'
        ],
        'language': {
            'emptyTable': emptyTableMessage
        },
        'oLanguage': {
           'sSearch': ''
        },
        'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
        columnDefs: [
            { orderable: false, searchable: false, targets: [0, 1, 4, 5] }
        ],
        "scrollX": true,
        "searching": false,
        rowReorder: true
    });

    // initialize code management datatable
    $.extend($.fn.dataTableExt.oStdClasses, {
        'sWrapper': 'bootstrap-table dataTables_wrapper',
        'sFilter': 'pull-right search',
        'sInfo': 'pagination-detail',
        'sLength': 'page-list'
    });
    
    // Re-order Rows
    table.on( 'row-reorder', function ( e, diff, edit ) {
        var result = 'Reorder started on row: '+edit.triggerRow.data()[2]+'<br>';
 
        for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
            var rowData = table.row( diff[i].node ).data();
 
            result += rowData[2]+' updated to be in position '+
                diff[i].newData+' (was '+diff[i].oldData+')<br>';
        }
 
        // $('#result').html( 'Event result:<br>'+result );
    });

    $('select[id^="news-visibility"]').each(function(){
        var news_id = $(this).attr('id');
        var news_visibility = $(this).val();

        if(news_visibility == 1) $(this).removeClass('vis-hidden').addClass('vis-visible');
        else $(this).removeClass('vis-visible').addClass('vis-hidden');
    });

    $('select[id^="news-visibility"]').on('change', function(){
        var news_id = $(this).attr('id');
        var news_visibility = $(this).val();
        console.log(news_id, news_visibility);

        if(news_visibility == 1) $(this).removeClass('vis-hidden').addClass('vis-visible');
        else $(this).removeClass('vis-visible').addClass('vis-hidden');
    });

    /* Image Upload Get Filename */
    $('input.fuImage').on('change', function(e){
        var input_name = this.name
        var filename_display = $('input[name="' + input_name +'-filename"]');
        var fileName = e.target.files[0].name;

        filename_display.val(fileName);
    });
});
