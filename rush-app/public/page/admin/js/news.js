$(function () {
    'use strict';
    $.noConflict(); 
    $(document).ready(function(){
        var emptyTableMessage = 'No matching records found';
        var newsTableSpecs = {
            'initComplete': function(){
                // Apply RUSH standard search style
                $('.search input').unwrap('label')
                    .attr({
                        'type': 'text',
                        'placeholder': 'Search'
                    })
                    .addClass('form-control');
                $('.page-list').insertBefore('.pagination-detail');
                $('.page-list select').unwrap('label');
                $('#news-table').wrap('<div class="scrollX-table"></div>');
            },
            'sDom': '<"fixed-table-toolbar"f><"excelButton hide"B>t<"bottom"ilp><"clear">',
            'buttons': [
                'excel'
            ],
            'language': {
                'emptyTable': emptyTableMessage
            },
            'oLanguage': {
            'sSearch': ''
            },
            'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
            columnDefs: [
                { targets: [3, 4], orderable: false, searchable: false }
            ],
            'order': [[ 2, "desc" ]]
        };

        // initialize code management datatable
        $.extend($.fn.dataTableExt.oStdClasses, {
            'sWrapper': 'bootstrap-table dataTables_wrapper',
            'sFilter': 'pull-right search',
            'sInfo': 'pagination-detail',
            'sLength': 'page-list'
        });
        $('#news-table').DataTable(newsTableSpecs);

        // Character Count
        var max = 600;
        $('.charcount').keypress(function(e) {
            if (e.which < 0x20) {
                // e.which < 0x20, then it's not a printable character
                // e.which === 0 - Not a character
                return; // Do nothing
            }
            if (this.value.length == max) {
                e.preventDefault();
            } else if (this.value.length > max) {
                // Maximum exceeded
                this.value = this.value.substring(0, max);
                console.log("count:".this.value.substring(0, max));
            }
        });

        $('select[id^="news-visibility"]').each(function(){
            var news_id = $(this).attr('id');
            var news_visibility = $(this).val();

            if(news_visibility == 1) $(this).removeClass('vis-hidden').addClass('vis-visible');
            else $(this).removeClass('vis-visible').addClass('vis-hidden');
        });

        $('select[id^="news-visibility"]').on('change', function(){
            var news_id = $(this).attr('id');
            var news_visibility = $(this).val();
            console.log(news_id, news_visibility);

            if(news_visibility == 1) $(this).removeClass('vis-hidden').addClass('vis-visible');
            else $(this).removeClass('vis-visible').addClass('vis-hidden');
        });

        /* Input File */
        function getImgSize(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var browse = input.name;
        
                reader.onload = function (e) {
                    $('input[name="' + browse + '"]').closest('.row').find('.imgprev').attr('src', e.target.result).wrap('<span class="imgwrap"></span>');
                }
        
                reader.readAsDataURL(input.files[0]);
            }
        }
        /* Image Upload Get Filename */
        $('input.fuImage').on('change', function(e){
            var input_name = this.name
            var filename_display = $('input[name="' + input_name +'-filename"]');
            var fileName = e.target.files[0].name;

            filename_display.val(fileName);
        });
        // Spectrum color picker
        if( $('#newsheadBG').length ) {
            $('#newsheadBG').spectrum({
                showInitial: true,
                preferredFormat: "hex",
                showInput: true
            });           
        }
        // Rich Text Editor
        // if( $('.textarea-editor').length ) {
        //     $('.textarea-editor').richText({
        //         fontColor: false,
        //         fileUpload: false,
        //         imageUpload: true,
        //         imageHTML: ''
        //     });
        //     $('.richText-btn.fa.fa-image').on('click', function(){
        //         setTimeout(function(){
        //             $('#imageURL').val('http://rush.ph/assets/uploads/').focus();
        //         }, 20);
        //     });
        // }
        var editor_config = {
            selector: "textarea.textarea-editor",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern",
                "charactercount"
            ],
            // plugins: 'image code',
            toolbar: "undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            content_css: [
                '../../css/vendor/bootstrap.css',
                '../../page/onboarding/style.css'
            ],
            content_style: ".col-md-9.wow.fadeIn {width: 100%;} #tinymce {padding: 10px;}",
            theme: 'modern',
            // enable title field in the Image dialog
            image_title: true, 
            // enable automatic uploads of images represented by blob or data URIs
            automatic_uploads: true,
            // URL of our upload handler (for more details check: https://www.tinymce.com/docs/configure/file-image-upload/#images_upload_url)
            // images_upload_url: 'postAcceptor.php',
            // here we add custom filepicker only to Image dialog
            file_picker_types: 'image', 
            // and here's our custom image picker
            file_picker_callback: function(cb, value, meta) {
                var input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');
                
                // Note: In modern browsers input[type="file"] is functional without 
                // even adding it to the DOM, but that might not be the case in some older
                // or quirky browsers like IE, so you might want to add it to the DOM
                // just in case, and visually hide it. And do not forget do remove it
                // once you do not need it anymore.

                input.onchange = function() {
                var file = this.files[0];
                
                var reader = new FileReader();
                reader.onload = function () {
                    // Note: Now we need to register the blob in TinyMCEs image blob
                    // registry. In the next release this part hopefully won't be
                    // necessary, as we are looking to handle it internally.
                    var id = 'blobid' + (new Date()).getTime();
                    var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                    var base64 = reader.result.split(',')[1];
                    var blobInfo = blobCache.create(id, file, base64);
                    blobCache.add(blobInfo);

                    // call the callback and populate the Title field with the file name
                    cb(blobInfo.blobUri(), { title: file.name });
                };
                reader.readAsDataURL(file);
                };
                
                input.click();
            }
        }
        tinymce.init(editor_config);      
    

        $('#app_img_txt1').html("Characters: " + decodeTxt($('#app_image1_label').val()) + "/100");
        $('#app_img_txt2').html("Characters: " + decodeTxt($('#app_image2_label').val()) + "/100");
        $('#app_img_txt3').html("Characters: " + decodeTxt($('#app_image3_label').val()) + "/100");

        $('#titleChar').html("Characters: " + parseInt( $('#newsTitle').val().length) + "/150");
        $('#merchantNameChar').html("Characters: " + parseInt( $('#newsMerchantName').val().length) + "/150");
        $('#descChar').html("Characters: " + parseInt( $('#newsDescription').val().length) + "/500");
        $('#mobileAppContentChar').html("Characters: " + parseInt( $('#mobile_app_content').val().length) + "/500");

        $('#app_image1_label').keyup(function() {
            $('#app_img_txt1').html("Characters: " + parseInt(decodeTxt($('#app_image1_label').val())) + "/100");
        });

        $('#app_image2_label').keyup(function() {
            $('#app_img_txt2').html("Characters: " + parseInt(decodeTxt($('#app_image2_label').val())) + "/100");
        });

        $('#app_image3_label').keyup(function() {
            var text_length = $('#app_image3_label').val().length;
            $('#app_img_txt3').html("Characters: " + parseInt(decodeTxt($('#app_image3_label').val())) + "/100");
        });

        $('#newsTitle').keyup(function() {
            var text_length = $('#newsTitle').val().length;
            $('#titleChar').html("Characters: " + parseInt(text_length) + "/150");
        });

        $('#newsMerchantName').keyup(function() {
            var text_length = $('#newsMerchantName').val().length;
            $('#merchantNameChar').html("Characters: " + parseInt(text_length) + "/150");
        });

        $('#newsDescription').keyup(function() {
            var text_length = $('#newsDescription').val().length;
           
            $('#descChar').html("Characters: " + parseInt(text_length) + "/500");
        });

         $('#mobile_app_content').keyup(function() {
            var text_length = $('#mobile_app_content').val().length;
            $('#mobileAppContentChar').html("Characters: " + parseInt(text_length) + "/500");
        });


         $("#btnRemoveBgImg").click(function(){
            $("#news_bgimg").val(null);
            $("#news_bgimg-filename").val(null);
        });

    imageFileValidator.init();
    });

    var imageFileValidator = {
        init : function() {
            $(".fuImage").each(function () {
                $(this).on('validateImageSize', function(e) {
                    var ew = $(this).attr('ew');
                    var eh = $(this).attr('eh')
                    var aw = $(this).attr('aw');
                    var ah = $(this).attr('ah');
                    var name = $(this).attr('name');
                    var errorMsg = $(this).parent().parent().parent().find('.error-msg');
                    if (parseInt(aw) > parseInt(ew)|| parseInt(ah) > parseInt(eh)) {
                        $(this).attr('validationStatus', 'invalid');
                        errorMsg.text("Image does not meet the image size requirement. Please check again.");
                        errorMsg.removeClass('hide');
                    }else {
                        if(name == "banner_image" && aw != ew && ah != eh){
                            $(this).attr('validationStatus', 'invalid');
                            errorMsg.text("Image does not meet the image size requirement. Please check again.");
                            errorMsg.removeClass('hide')
                        }else{
                            $(this).attr('validationStatus', 'valid');
                            errorMsg.addClass('hide');
                        } 
                        
                    }
                    $(this).trigger('showPreview');
                });

                $(this).fileupload({
                    dataType: 'json',
                    autoUpload: false,
                    acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                    maxFileSize: 999000,
                    disableImageResize: true,
                    // previewMaxWidth: 200,
                    // previewMaxHeight: 200,
                    previewCrop: false,
                    replaceFileInput:false
                }).on('fileuploadprocessalways', function (e, data) {
                    var that = $(this);
                    var file = data.files[0];
                    var dimensionIsValid = true;

                    var fr = new FileReader;
                    fr.onload = function() {
                        var img = new Image;
                        img.onload = function() {
                            that.attr("aw", img.width);
                            that.attr("ah", img.height);
                            that.trigger('validateImageSize');
                        };
                        img.src = fr.result;
                    };
                    fr.readAsDataURL(file);
                });
            });
        }
    }
});

function decodeTxt(tx){
     var decoded = decodeHtml(tx);
     var decodedStripped = decoded.replace(/(<([^>]+)>)/ig, "").trim();
     var tc = decodedStripped.length;
     return tc;


}

function decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
  }

