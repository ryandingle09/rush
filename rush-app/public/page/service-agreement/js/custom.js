$(document).ready(function() {

    /*---------------------print service agreement-----------------------*/
    $(".print-service-agreement").click(function(){
        var url = $(this).attr('data-print-url');

        var printWindow = window.open(url);
        printWindow.print();

        return false;
    });
/*---------------------package-----------------------*/
				$(".start-btn2").hover(function(){
					$(this).css('width','320');
       			 }, function(){
        		   $(this).css('width','300');

				});

				$(".package").hover(function(){
						$(this).addClass( "top" );
				}, function(){
					$(this).removeClass( "top" );

				});

				$('#modal').modal('toggle');

/*---------------------payment-----------------------*/
 $("#payment-type").change(function(){
		        if($(this).val() == "1")
		            {
		            $(".trigger").remove();
		            var item = $('<div class="trigger"><div class="row"><div class="col-md-12"><label>Credit Card No.</label><input type="text" class="form-control" name="creadit-card"></div></div><div class="row"><div class="col-md-12"><br/><label>Cardholder\'s Name</label><input type="text" class="form-control" name="cardholder"></div></div><br/><div class="row"><div class="col-md-6"><label>Securtiy Code</label><input type="text" class="form-control" name="code"></div><div class="col-md-3"><label>Expiration</label><div class="styled-select"><select name="date" class="form-control"><option value="01">01</option><option value="02">02</option></select></div></div><div class="col-md-3"><label>&nbsp;</label><div class="styled-select"><select name="year" class="form-control"><option value="2001">2001</option><option value="2002">2002</option></select></div></div></div></div>').hide().fadeIn(500);
		         	$('#credit-card').append(item);
		            }
		        else if($(this).val() == "2")
		         	{
		         		$(".trigger").remove();
		         		 var item = $('<div class="trigger"><div class="row"><div class="col-md-12"><label>Quickperk\'s account details will be provided via email.</label></div></div></div>').hide().fadeIn(500);
						$('#credit-card').append(item);
		         	}
		       else if($(this).val() == "3")
		         	{
		         		$(".trigger").remove();
				            var item = $('<div class="trigger"><div class="row"><div class="col-md-12"><label>Enter Globe Mobile No</label><input type="text" class="form-control" name="creadit-card"></div></div></div>').hide().fadeIn(500);
						$('#credit-card').append(item);
		         	}
		        });


$(".chosen-select").chosen();

$("#loyalty").chosen().change(function(){

     var text  = $('#loyalty').find(":selected").text();
     var term = "Others";
      if( text.indexOf( term ) != -1 )
          {
            $('#loyalty option:selected').removeAttr('selected');
            $('#loyalty').trigger('chosen:updated');
          		$("#others2").show(1,
				    function() {
				        $('#others_ID2').focus();
					$("#others_ID2").prop("required", true);
				        //alert('test');
				    }
				);
          }else{
          	$('#others2').hide();
		$("#others_ID2").prop("required", false);
          }

});

/* -- Feature Carousel with Tabs | added March 30 2016 --*/
    $('#featCarousel').carousel({
        interval: 4000
    });

    var clickEvent = false;
    $('#featCarousel').on('click', '.nav a', function() {
        clickEvent = true;
        $('.nav li').removeClass('active');
        $(this).parent().addClass('active');
    }).on('slid.bs.carousel', function(e) {
        if(!clickEvent) {
            var count = $('#featCarousel .nav').children().length -1;
            var current = $('#featCarousel .nav li.active');
            current.removeClass('active').next().addClass('active');
            var id = parseInt(current.data('slide-to'));
            if(count == id) {
                $('#featCarousel .nav li').first().addClass('active');
            }
        }
        clickEvent = false;
    });

    $('#package, #industry, #objective').on('click',function(){
      $(this).css({'color':'#555555'})
    })
    $('#package, #industry, #objective').on('change blur',function(){
      if($(this).val() != null){
        $(this).css({'color':'#555555'})
      }else {
        $(this).css({'color':'#999'})
      }
    })
    $('#form').on('submit', function (){
        if($("#industry").val() == null) {
          $("#industry").focus();
          return false;
        }
        // if($("#objective").val() == null) {
        //   $("#objective_chosen").trigger('mousedown');
        //   return false;
        // }
        // if($("#package").val() == null) {
        //   $('#package').focus();
        //   return false;
        // }
        var googleResponse = jQuery('#g-recaptcha-response').val();
        if (!googleResponse) {
            $('.hidden-text').removeClass("hidden");
            return false;
        } else {
            return true;
        }
        return true;
    })

    var allRadios = document.getElementsByName('package');
    var booRadio;
    var x = 0;
    for(x = 0; x < allRadios.length; x++){

        allRadios[x].onclick = function() {
            if(booRadio == this){
                this.checked = false;
                booRadio = null;
            }else{
                booRadio = this;
            }
        };
    }

    $(document.body).on('click', function(){
      $('.contents').removeClass('open')
    })
    $('.contents').click(function(e) {
      e.stopPropagation();
  });
    $('.drp-down .field').on('click', function(e){
      $(this).next('.contents').toggleClass('open')
      e.stopPropagation();
    })
    $('[name="objective[]"').on('change', function(){
      var $checkboxes = $('#objective input[type="checkbox"]');
      var countCheckedCheckboxes = $checkboxes.filter(':checked').length;
      if(countCheckedCheckboxes !=0 ) {
        $('.drp-down .field').addClass('active')
        $('#objective button').text(countCheckedCheckboxes + ' Selected');
      }
      else{
        $('.drp-down .field').removeClass('active')
        $('#objective button').text('Select Objective');
      } 
    })
});
