// Pie Chart
var data = {
  series: [
      {
          value: 35,
          name: 'Mobile # Only',
          className: "pie-chart-v1"
      },
      {
          value: 35,
          name: 'App',
          className: "pie-chart-v2"
      },
      {
          value: 30,
          name: 'Card',
          className: "pie-chart-v3"
      }
  ]
};

var sum = function(a, b) { return a + b };
var options = {
  width: '216px',
  height: '215px',
  showLabel: false
};
new Chartist.Pie('.ct-chart', data, options);




/* App Chart */
var barChartData = {
   labels: ["Week 1", "Week 2", "Week 3", "Week 4"],
   datasets: [{
     data: [30, 45, 43, 20],
     type: 'line',
     label: 'This Year',
     fill: false,
     backgroundColor: "#5c5c5c",
     borderColor: "#d6d6d6",
     borderCapStyle: 'butt',
     borderDash: [],
     borderDashOffset: 0.0,
     borderJoinStyle: 'miter',
     lineTension: 0,
     pointBackgroundColor: "#5c5c5c",
     pointBorderColor: "#d6d6d6",
     pointBorderWidth: 5,
     pointHoverRadius: 10,
     pointHoverBackgroundColor: "#d6d6d6",
     pointHoverBorderColor: "#d6d6d6",
     pointHoverBorderWidth: 2,
     pointRadius: 6,
     pointHitRadius: 10,
     yAxisID: "bar-y-axis",
 }, {
     label: 'Promoters',
     backgroundColor: [
                    'rgba(245, 127, 32, 1)',
                    'rgba(212, 31, 57, 1)',
                    'rgba(245, 127, 32, 1)',
                    'rgba(212, 31, 57, 1)',
                ],
                borderColor: [
                    'rgba(245, 127, 32, 1)',
                    'rgba(212, 31, 57, 1)',
                    'rgba(245, 127, 32, 1)',
                    'rgba(212, 31, 57, 1)',
                ],
     yAxisID: "bar-y-axis",
      data: [13, 20, 43, 15],
   }, {
     label: 'Passives',
     backgroundColor: [
                    'rgba(213, 213, 213, 1)',
                    'rgba(213, 213, 213, 1)',
                    'rgba(213, 213, 213, 1)',
                    'rgba(213, 213, 213, 1)',
                ],
                borderColor: [
                    'rgba(213, 213, 213, 1)',
                    'rgba(213, 213, 213, 1)',
                    'rgba(213, 213, 213, 1)',
                    'rgba(213, 213,213, 1)',
                ],
     yAxisID: "bar-y-axis",
     data: [12, 19, 3, 5],
   }]
 };

 window.onload = function() {
     var ctx = document.getElementById("myChart").getContext('2d');
     var myChart = new Chart(ctx, {
     type: 'bar',
     data: barChartData,
     options: {
         legend: {
           display: false
        },
       title: {
         display: true,
         text: "# of downloads",
         position:'left'
       },
       tooltips: {
         mode: 'label',
       },
       responsive: true,
       scales: {
         xAxes: [{
           stacked: true,
           gridLines : {
            display : false
           },
         }],
         yAxes: [{
           stacked: false,
           ticks: {
             beginAtZero: true,
         },
           display: false,

         }, {
           id: "bar-y-axis",
           stacked: true,
           gridLines : {
            display : false
           },
           ticks: {
             beginAtZero: true,
           },
           display: false,
           type: 'linear'
         }]
       }
     }
   });
 };


/* Card Chart */
var CarBarChartData = {
   labels: ["Week 1", "Week 2", "Week 3", "Week 4"],
   datasets: [{
     data: [10, 15, 28, 20],
     type: 'line',
     label: 'This Year',
     fill: true,
     backgroundColor: "rgba(223, 223,223, 0.6)",
     borderColor: "#d6d6d6",
     borderCapStyle: 'butt',
     borderDashOffset: 0.0,
     lineTension: 0,
     pointBackgroundColor: "#5c5c5c",
     pointBorderWidth: 5,
     pointHoverRadius: 10,
     pointHoverBackgroundColor: "#d6d6d6",
     pointHoverBorderColor: "#d6d6d6",
     pointHoverBorderWidth: 2,
     pointRadius: 6,
     pointHitRadius: 10,
     yAxisID: "bar-y-axis",
 }, {
     label: 'Promoters',
     backgroundColor: [
                    'rgba(245, 127, 32, 1)',
                    'rgba(212, 31, 57, 1)',
                    'rgba(245, 127, 32, 1)',
                    'rgba(212, 31, 57, 1)',
                ],
                borderColor: [
                    'rgba(245, 127, 32, 1)',
                    'rgba(212, 31, 57, 1)',
                    'rgba(245, 127, 32, 1)',
                    'rgba(212, 31, 57, 1)',
                ],
     yAxisID: "bar-y-axis",
      data: [13, 20, 30, 15],
   }]
 };

 var cardChart = document.getElementById("card-chart").getContext('2d');
 var myCardChart = new Chart(cardChart, {
 type: 'bar',
 data: CarBarChartData,
 options: {
     legend: {
       display: false
    },
   title: {
     display: true,
     text: "# of Card Issued",
     position:'left'
   },
   tooltips: {
     mode: 'label',
   },
   responsive: true,
   scales: {
     xAxes: [{
       stacked: true,
       gridLines : {
        display : false
       },
     }],
     yAxes: [{
       stacked: false,
       ticks: {
         beginAtZero: true,
     },
       display: false,

     }, {
       id: "bar-y-axis",
       stacked: true,
       gridLines : {
        display : false
       },
       ticks: {
         beginAtZero: true,
       },
       display: false,
       type: 'linear'
     }]
   }
 }
});


/* Visit Chart */
var visitChartData = {
   labels: ["M", "T", "W", "TH", "F", "SAT", "SUN"],
   datasets: [{
     data: [30, 15, 25, 15, 25, 35, 45],
     type: 'line',
     label: 'This Year',
     fill: false,
     backgroundColor: "#5c5c5c",
     borderColor: "#d6d6d6",
     borderCapStyle: 'butt',
     borderDash: [],
     borderDashOffset: 0.0,
     borderJoinStyle: 'miter',
     lineTension: 0,
     pointBackgroundColor: "#5c5c5c",
     pointBorderColor: "#d6d6d6",
     pointBorderWidth: 5,
     pointHoverRadius: 10,
     pointHoverBackgroundColor: "#d6d6d6",
     pointHoverBorderColor: "#d6d6d6",
     pointHoverBorderWidth: 2,
     pointRadius: 6,
     pointHitRadius: 10,
     yAxisID: "bar-y-axis",
 }, {
     label: 'Promoters',
     backgroundColor: [
            'rgba(245, 127, 32, 1)',
            'rgba(212, 31, 57, 1)',
            'rgba(245, 127, 32, 1)',
            'rgba(212, 31, 57, 1)',
            'rgba(245, 127, 32, 1)',
            'rgba(212, 31, 57, 1)',
            'rgba(245, 127, 32, 1)',
        ],
        borderColor: [
            'rgba(245, 127, 32, 1)',
            'rgba(212, 31, 57, 1)',
            'rgba(245, 127, 32, 1)',
            'rgba(212, 31, 57, 1)',
            'rgba(245, 127, 32, 1)',
            'rgba(212, 31, 57, 1)',
            'rgba(245, 127, 32, 1)',
        ],
     yAxisID: "bar-y-axis",
      data: [15, 5, 10, 5, 10, 15, 10],
   }, {
     label: 'Passives',
     backgroundColor: [
         'rgba(212, 31, 57, 1)',
         'rgba(245, 127, 32, 1)',
         'rgba(212, 31, 57, 1)',
         'rgba(245, 127, 32, 1)',
         'rgba(212, 31, 57, 1)',
         'rgba(245, 127, 32, 1)',
         'rgba(212, 31, 57, 1)',
     ],
     borderColor: [
         'rgba(212, 31, 57, 1)',
         'rgba(245, 127, 32, 1)',
         'rgba(212, 31, 57, 1)',
         'rgba(245, 127, 32, 1)',
         'rgba(212, 31, 57, 1)',
         'rgba(245, 127, 32, 1)',
         'rgba(212, 31, 57, 1)',
     ],
     yAxisID: "bar-y-axis",
     data: [10, 5, 10, 5, 10, 15, 30],
   }]
 };
 var visitCtx = document.getElementById("visit-chart").getContext('2d');
 var visitChart = new Chart(visitCtx, {
 type: 'bar',
 data: visitChartData,
 options: {
     legend: {
       display: false
    },
   title: {
     display: false,
     text: "",
     position:'left'
   },
   tooltips: {
     mode: 'label',
   },
   responsive: true,
   scales: {
     xAxes: [{
       stacked: true,
       gridLines : {
        display : false
       },
       categoryPercentage: 0.4,
       barPercentage: 0.5,
     }],
     yAxes: [{
       stacked: false,
       ticks: {
         beginAtZero: true,
     },
       display: false,
     }, {
       id: "bar-y-axis",
       stacked: true,
       gridLines : {
        display : false
       },
       ticks: {
         beginAtZero: true,
       },
       display: false,
       type: 'linear'
     }]
   }
 }
});


/* Txn Channel Chart */
var txnChartData = {
   labels: ["Mobile #", "App", "Card", "Web"],
   datasets: [{
        label: 'Earning',
        backgroundColor: 'rgba(212, 31, 57, 1)',
        data: [25, 25, 25, 25],
        }, {
        label: 'Pay with points',
        backgroundColor:'rgba(245, 127, 32, 1)',
        data: [25, 25, 25, 25],
        }, {
        label: 'Redemptions',
        backgroundColor:'rgba(92, 92, 92, 1)',
        data: [25, 25, 25, 25],
        }, {
        label: 'Current Week #',
        backgroundColor:'rgba(172, 173, 204, 1)',
        data: [25, 25, 25, 25],
        }, {
        label: 'Previous Week #',
        backgroundColor:'rgba(138, 139, 164, 1)',
        data: [25, 25, 25, 25],
   }]
 };

var ctx = document.getElementById("txn-channel").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'horizontalBar',
    data: txnChartData,
    options: {
        legend: {
            display: false
        },
        title: {
            display: false,
            text: "# of downloads",
            position:'left'
        },
        tooltips: {
            mode: 'label',
        },
        responsive: true,
        scales: {
            xAxes: [{
                stacked: true,
                gridLines : {
                    display : false,
                },
                ticks: {
                    beginAtZero: true,
                },
                display: false,
            }],
            yAxes: [{
                categoryPercentage: 0.6,
                barPercentage: 0.5,
                stacked: true,
                ticks: {
                    beginAtZero: true,
                },
                gridLines : {
                    display : false
                },
            }]
        }
    }
});
/* Txn Value */
function initTxnValue(obj,val){
    var txnValueChartData = {
            labels: ["M", "T", "W", "TH", "F", "SAT", "SUN"],
            datasets: [
                {
                    label: "Previous Week",
                    fill: true,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(213, 213, 213, 0.5)',
                    borderColor: "#5c5c5c",
                    borderCapStyle: 'butt',
                    borderDash: [6],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    lineTension: 0,
                    pointBackgroundColor: "#ffffff",
                    pointBorderColor: "#5c5c5c",
                    pointBorderWidth: 3,
                    pointHoverRadius: 6,
                    pointHoverBackgroundColor: "#5c5c5c",
                    pointHoverBorderColor: "#5c5c5c",
                    pointHoverBorderWidth: 2,
                    pointRadius: 5,
                    pointHitRadius: 5,
                    data: obj.data[val].previous,
                },
                {
                    label: "Current Week",
                    fill: true,
                    lineTension: 0.1,
                    backgroundColor: obj.data[val].backgroundColor,
                    borderColor: "#d6d6d6",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    lineTension: 0,
                    pointBackgroundColor: "#5c5c5c",
                    pointBorderColor: "#d6d6d6",
                    pointBorderWidth: 3,
                    pointHoverRadius: 6,
                    pointHoverBackgroundColor: "#d6d6d6",
                    pointHoverBorderColor: "#d6d6d6",
                    pointHoverBorderWidth: 2,
                    pointRadius: 5,
                    pointHitRadius: 5,
                    data: obj.data[val].thisWeek,
                }

            ]
    }

    txnValue = document.getElementById("txn-value").getContext('2d');
    myLineChart = new Chart(txnValue, {
        type: 'line',
        data: txnValueChartData,
        options: {
            legend: {
                display: false
            },
            title: {
                display: false,
                text: "Transaction Value (PHP)",
                position:'left'
            },
            tooltips: {
                mode: 'label',
            },
            responsive: true,
            scales: {
                xAxes: [{
                    // stacked: true,
                    gridLines : {
                        display : false,
                    },
                    ticks: {
                        beginAtZero: true,
                        stepSize: 50,

                    },
                    // display: false,
                }],
                yAxes: [{
                    // stacked: true,
                    ticks: {
                        beginAtZero: true,
                        stepSize: 50,
                    },
                    gridLines : {
                        display : false
                    },
                }]
            }
        }
    });
}

var datasets = {
    data: [
        {
            backgroundColor: 'rgba(212, 31, 57, 1)',
            thisWeek:[50, 60, 15, 33, 48, 18, 48],
            previous:[22, 34, 23, 56, 51, 74, 28],
        },
        {
            backgroundColor: 'rgba(245, 127, 32, 1)',
            thisWeek:[10, 50, 25, 43, 78, 88, 48],
            previous:[40, 60, 25, 43, 88, 18, 78],
        },
        {
            backgroundColor: 'rgba(92, 92, 92, 1)',
            thisWeek:[10, 60, 35, 53, 58, 18, 78],
            previous:[50, 90, 25, 13, 78, 28, 38],
        },
        {
            backgroundColor: 'rgba(138, 139, 164, 1)',
            thisWeek:[50, 30, 55, 33, 75, 58, 48],
            previous:[60, 30, 65, 33, 68, 48, 28],
        }
    ]
}
initTxnValue(datasets,0);

/* Txn Frq Value */
function initTxnFreqValue(obj,val){
    var txnFrqValueChartData = {
            labels: ["M", "T", "W", "TH", "F", "SAT", "SUN"],
            datasets: [
                {
                    label: "Previous Week",
                    fill: true,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(213, 213, 213, 0.5)',
                    borderColor: "#5c5c5c",
                    borderCapStyle: 'butt',
                    borderDash: [6],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    lineTension: 0,
                    pointBackgroundColor: "#ffffff",
                    pointBorderColor: "#5c5c5c",
                    pointBorderWidth: 3,
                    pointHoverRadius: 6,
                    pointHoverBackgroundColor: "#5c5c5c",
                    pointHoverBorderColor: "#5c5c5c",
                    pointHoverBorderWidth: 2,
                    pointRadius: 5,
                    pointHitRadius: 5,
                    data: obj.data[val].previous,
                },
                {
                    label: "Current Week",
                    fill: true,
                    lineTension: 0.1,
                    backgroundColor: obj.data[val].backgroundColor,
                    borderColor: "#d6d6d6",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    lineTension: 0,
                    pointBackgroundColor: "#5c5c5c",
                    pointBorderColor: "#d6d6d6",
                    pointBorderWidth: 3,
                    pointHoverRadius: 6,
                    pointHoverBackgroundColor: "#d6d6d6",
                    pointHoverBorderColor: "#d6d6d6",
                    pointHoverBorderWidth: 2,
                    pointRadius: 5,
                    pointHitRadius: 5,
                    data: obj.data[val].thisWeek,
                }

            ]
    }

    txnFrqValue = document.getElementById("txn-freq-value").getContext('2d');
    myLineChart2 = new Chart(txnFrqValue, {
        type: 'line',
        data: txnFrqValueChartData,
        options: {
            legend: {
                display: false
            },
            title: {
                display: false,
                text: "Transaction Value (PHP)",
                position:'left'
            },
            tooltips: {
                mode: 'label',
            },
            responsive: true,
            scales: {
                xAxes: [{
                    // stacked: true,
                    gridLines : {
                        display : false,
                    },
                    ticks: {
                        beginAtZero: true,
                        stepSize: 50,

                    },
                    // display: false,
                }],
                yAxes: [{
                    // stacked: true,
                    ticks: {
                        beginAtZero: true,
                        stepSize: 50,
                    },
                    gridLines : {
                        display : false
                    },
                }]
            }
        }
    });
}

var txnfreqdatasets = {
    data: [
        {
            backgroundColor: 'rgba(212, 31, 57, 1)',
            thisWeek:[50, 60, 15, 33, 48, 18, 48],
            previous:[22, 34, 23, 56, 51, 74, 28],
        },
        {
            backgroundColor: 'rgba(245, 127, 32, 1)',
            thisWeek:[10, 50, 25, 43, 78, 88, 48],
            previous:[40, 60, 25, 43, 88, 18, 78],
        },
        {
            backgroundColor: 'rgba(92, 92, 92, 1)',
            thisWeek:[10, 60, 35, 53, 58, 18, 78],
            previous:[50, 90, 25, 13, 78, 28, 38],
        }
    ]
}
initTxnFreqValue(txnfreqdatasets,0);

$('.c-select').on('click',function(){
    $(this).find('.pie-legends').toggle();
})
$('.txn-update-set').on('click',function(e){
    myLineChart.destroy();
    $(this).closest('.c-select').find('label').attr('class','label-'+(parseInt($(this).data('dataset'))+1)).text($(this).text())
    initTxnValue(datasets,$(this).data('dataset'));
})
$('.txn-freq-update-set').on('click',function(e){
    myLineChart2.destroy();
    $(this).closest('.c-select').find('label').attr('class','label-'+(parseInt($(this).data('dataset'))+1)).text($(this).text())
    initTxnFreqValue(txnfreqdatasets,$(this).data('dataset'));
})
