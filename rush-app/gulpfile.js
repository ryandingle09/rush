/* This gulpfile takes care of the day-to-day elixir tasks */
/* important: run gulpfile-packages.js before running these tasks */

var elixir = require('laravel-elixir');

var config = {
    'bower': './resources/assets/bower_components',
    'bootstrap': './resources/assets/bower_components/bootstrap-sass',
    'bootstrap_dist': './resources/assets/bower_components/bootstrap/dist',
    'sassPath': './resources/assets/sass',
    'jsPath': './resources/assets/js',
    'onboarding_cms': './resources/assets/onboarding-cms'
};

elixir(function (mix) {
    mix.sass(config.sassPath + '/app.scss')
        .copy(config.bootstrap_dist + '/css/bootstrap.css', 'public/css/vendor/bootstrap.css')
        .copy(config.bootstrap + '/assets/fonts/bootstrap', 'public/fonts')
        .copy(config.bootstrap + '/assets/javascripts/bootstrap.min.js', 'public/js/vendor/bootstrap.min.js')
        .copy(config.bower + '/jquery/dist/jquery.min.js', 'public/js/vendor/jquery.min.js')
        .copy(config.bower + '/font-awesome/css/font-awesome.min.css', 'public/css/vendor/font-awesome.css')
        .copy(config.bower + '/font-awesome/fonts', 'public/css/fonts')
        .copy(config.bower + '/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css', 'public/css/vendor/bootstrap-datetimepicker.min.css')
        .copy(config.bower + '/datatables.net-dt/css/jquery.dataTables.min.css', 'public/css/vendor/jquery.dataTables.min.css')
        .copy(config.bower + '/datatables.net-buttons-dt/css/buttons.dataTables.min.css', 'public/css/vendor/buttons.dataTables.min.css')
        .copy(config.bower + '/spectrum/spectrum.css', 'public/css/vendor/spectrum.css')
        .copy(config.bower + '/datatables.net-buttons/js/buttons.html5.min.js', 'public/js/vendor/buttons.html5.min.js')
        .copy(config.bower + '/jquery-validation/dist/jquery.validate.js', 'public/js/vendor/jquery.validate.js')
        .copy(config.bower + '/moment/min/moment.min.js', 'public/js/vendor/moment.min.js')
        .copy(config.bower + '/bootstrap/dist/js/bootstrap.min.js', 'public/js/vendor/bootstrap.min.js')
        .copy(config.bower + '/bootstrap-table/dist/bootstrap-table.min.js', 'public/js/vendor/bootstrap-table.min.js')
        .copy(config.bower + '/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js', 'public/js/vendor/bootstrap-datetimepicker.min.js')
        .copy(config.bower + '/FitText.js/jquery.fittext.js', 'public/js/vendor/jquery.fittext.js')
        .copy(config.bower + '/ActiveNavigation/jquery.activeNavigation.js', 'public/js/vendor/jquery.activeNavigation.js')
        .copy(config.bower + '/bootstrap-table/dist/extensions/filter-control/bootstrap-table-filter-control.js', 'public/js/vendor/bootstrap-table-filter-control.js')
        .copy(config.bower + '/spectrum/spectrum.js', 'public/js/vendor/spectrum.js')
        .copy(config.bower + '/background-blur/dist/background-blur.min.js', 'public/js/vendor/background-blur.min.js')
        .copy(config.bower + '/jquery-ui/jquery-ui.min.js', 'public/js/vendor/jquery-ui.min.js')
        .copy(config.bower + '/blueimp-load-image/js/load-image.all.min.js', 'public/js/vendor/load-image.all.min.js')
        .copy(config.bower + '/blueimp-file-upload/js/jquery.fileupload.js', 'public/js/vendor/jquery.fileupload.js')
        .copy(config.bower + '/blueimp-file-upload/js/jquery.fileupload-process.js', 'public/js/vendor/jquery.fileupload-process.js')
        .copy(config.bower + '/blueimp-file-upload/js/jquery.fileupload-image.js', 'public/js/vendor/jquery.fileupload-image.js')
        .copy(config.bower + '/datatables.net/js/jquery.dataTables.js', 'public/js/vendor/jquery.dataTables.js');
});



elixir(function (mix) {
    mix.sass(config.sassPath + '/override.scss', 'public/css')
});

elixir(function (mix) {
    mix.sass(config.sassPath + '/analytics/style.scss', 'public/page/analytics/css')
        .copy(config.bower + '/chartist/dist/chartist.min.css', 'public/page/analytics/css/chartist.min.css')
        .copy(config.bower + '/chartist/dist/chartist.min.js', 'public/page/analytics/js/chartist.min.js');
});

elixir(function (mix) {
    mix.sass(config.sassPath + '/admin/style.scss', 'public/page/admin/css')
    .copy(config.bower + '/sweetalert/dist/sweetalert.css', 'public/page/admin/css/sweetalert.css')
    .copy(config.bower + '/sweetalert/dist/sweetalert.min.js', 'public/page/admin/js/sweetalert.min.js')
    .copy(config.bower + '/chosen/chosen.jquery.js', 'public/page/admin/js/chosen.jquery.js')
    .copy(config.bower + '/chosen/chosen.css', 'public/page/admin/css/chosen.css');
});

elixir(function (mix) {
    mix.sass([
            config.bower + '/bootstrap-sweetalert/dist/sweetalert.css',
            config.sassPath + '/achievement-unlock/style.scss'
       ], 'public/page/achievement-unlock/css/style.css')
       .scripts([
            config.bower + '/jquery.numeric/jquery.numeric.js',
            config.bower + '/jquery-form/jquery.form.js',
            config.bower + '/bootstrap-sweetalert/dist/sweetalert.js',
            config.jsPath + '/achievement-unlock/index.js'
        ], 'public/page/achievement-unlock/js/scripts.js');
});

elixir(function(mix) {
    mix.scripts(['app.js'], 'public/js/app.js');
});

elixir(function (mix) {
    mix.sass('onboarding/style.scss', 'public/page/onboarding')
});

elixir(function (mix) {
    mix.sass('onboarding/style.scss', 'public/page/onboarding')
});

elixir(function (mix) {
    mix.sass(config.sassPath + '/class/style.scss', 'public/page/class/css')
});

elixir(function (mix) {
    mix.sass(config.sassPath + '/voucher/style.scss', 'public/page/voucher/css')
        .copy(config.bower + '/sweetalert/dist/sweetalert.css', 'public/page/voucher/css/sweetalert.css')
        .scripts([
            config.bower + '/bootstrap-sweetalert/dist/sweetalert.js',
        ], 'public/page/voucher/js/scripts.js');
});

/**
 * Store and Customer Management Assets
 */
elixir(function (mix) {
    mix.sass([
            config.bower + '/bootstrap-sweetalert/dist/sweetalert.css',
            config.sassPath + '/management/customer-stamps-summary.scss'
       ], 'public/page/management/css/style.css')
       .scripts([
            config.bower + '/bootstrap-sweetalert/dist/sweetalert.js',
            config.jsPath + '/management/customer-stamps-summary.js'
        ], 'public/page/management/js/scripts.js');
});

/* Onboarding CMS Assets */
elixir(function (mix) {
    mix.copy(config.onboarding_cms + '/css/bootstrap.min.css', 'public/page/onboarding-cms/css/bootstrap.min.css');
    mix.copy(config.onboarding_cms + '/css/font-awesome.min.css', 'public/page/onboarding-cms/css/font-awesome.min.css');
    mix.copy(config.onboarding_cms + '/js/bootstrap.min.js', 'public/page/onboarding-cms/js/bootstrap.min.js');
    mix.copy(config.onboarding_cms + '/js/jquery.min.js', 'public/page/onboarding-cms/js/jquery.min.js');
    mix.copy(config.onboarding_cms + '/fonts/fontawesome-webfont.woff2', 'public/page/onboarding-cms/fonts/fontawesome-webfont.woff2');
    mix.copy(config.onboarding_cms + '/fonts/FontAwesome.otf', 'public/page/onboarding-cms/fonts/FontAwesome.otf');
    mix.copy(config.onboarding_cms + '/fonts/fontawesome-webfont.eot', 'public/page/onboarding-cms/fonts/fontawesome-webfont.eot');
    mix.copy(config.onboarding_cms + '/fonts/fontawesome-webfont.svg', 'public/page/onboarding-cms/fonts/fontawesome-webfont.svg');
    mix.copy(config.onboarding_cms + '/fonts/fontawesome-webfont.ttf', 'public/page/onboarding-cms/fonts/fontawesome-webfont.ttf');
    mix.copy(config.onboarding_cms + '/fonts/fontawesome-webfont.woff', 'public/page/onboarding-cms/fonts/fontawesome-webfont.woff');
    mix.copy(config.onboarding_cms + '/sweetalert/dist/sweetalert.css', 'public/page/onboarding-cms/css/sweetalert.css');
    mix.copy(config.onboarding_cms + '/chosen/chosen.css', 'public/page/onboarding-cms/css/chosen.css');
});

elixir(function (mix) {
    mix.sass(config.sassPath + '/punchcard/style.scss', 'public/page/punchcard/css')
});