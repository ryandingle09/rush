/* This gulpfile pulls in required packages */
/* in the deployment script use: gulp --gulpfile gulpfile-packages.js */

var gulp = require('gulp');
var gutil = require('gulp-util');
var install = require("gulp-install");

gulp.task('pull-npm', function() {
    gulp.src(['./package.json'])
        .pipe(install());
});

gulp.task('pull-bower', ['pull-npm'], function(){
    gulp.src(['./bower.json'])
        .pipe(install());
});

gulp.task('default', ['pull-bower'], function(){
    return gutil.log('Dependencies Updated!');
});