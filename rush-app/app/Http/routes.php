<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'OnboardingController@index')->name('onboarding');
Route::get('about', 'OnboardingController@about')->name('onboarding_about');
Route::get('auntieannes', 'OnboardingController@auntieannes')->name('onboarding_auntieannes');
Route::get('theathletesfoot', 'OnboardingController@theathletesfoot')->name('onboarding_theathletesfoot');
Route::get('unilever', 'OnboardingController@unilever')->name('onboarding_unilever');
Route::get('goldsgym', 'OnboardingController@goldsgym')->name('onboarding_goldsgym');
Route::get('mathemagis', 'OnboardingController@mathemagis')->name('onboarding_mathemagis');
Route::get('soulscape', 'OnboardingController@soulscape')->name('onboarding_soulscape');
Route::get('yogaplus', 'OnboardingController@yogaplus')->name('onboarding_yogaplus');
Route::get('how-it-works', 'OnboardingController@how_it_works')->name('onboarding_how_it_works');
Route::get('products', 'OnboardingController@products')->name('onboarding_products');
Route::get('terms-of-use', 'OnboardingController@terms')->name('onboarding_terms');
Route::get('privacy-policy', 'OnboardingController@privacy')->name('onboarding_privacy');
Route::get('faqs', 'OnboardingController@faqs')->name('onboarding_faqs');
Route::get('newsroom', 'OnboardingController@newsroom')->name('onboarding_newsroom');
Route::get('newsroom/{url}', 'OnboardingController@newsroom_article')->name('onboarding_newsroom');
Route::get('signup-success', 'OnboardingController@signup_success')->name('onboarding_signup_success');
Route::get('demo-success', 'OnboardingController@demo_success')->name('onboarding_demo_success');
Route::get('merchant-signup', 'OnboardingController@merchant_signup')->name('onboarding_merchant_signup');
Route::group(
    [   'prefix' => 'request-demo',
        'as' => 'request-demo'
    ],
    function() {
        Route::get('/', 'OnboardingController@request_demo');
        Route::post('/', 'OnboardingController@request_demo');
        Route::get('basic', 'OnboardingController@requestBasic');
        Route::post('basic', 'OnboardingController@requestBasic');
        Route::get('pro', 'OnboardingController@requestPro');
        Route::post('pro', 'OnboardingController@requestPro');
    }
);

Route::group(
    [   'prefix' => 'Globemybusiness',
        'as' => 'my-business'
    ],
    function() {
        Route::get('/', 'MyBusinessDemoController@index');
        Route::post('/', 'MyBusinessDemoController@index');
    }
);


Route::post('prelogin', 'OnboardingController@validate_login');
Route::post('login', 'OnboardingController@login');
Route::post('presignup', 'OnboardingController@validate_signup');
Route::post('select-package', 'OnboardingController@select_package')->name('onboarding_select_package');
Route::post('register', 'OnboardingController@register')->name('onboarding_register');
Route::get('logout', 'OnboardingController@logout');
Route::post('preforgot', 'OnboardingController@validate_forgotpassword');
Route::post('forgot-password', 'OnboardingController@forgotpassword');

Route::group(
    [   'prefix' => 'dashboard',
        'middleware' => ['auth.cms','auth.modules'],
        'as' => 'home'
    ],
    function() {
        Route::get('/', 'HomeController@index');
        Route::post('member-tier', 'HomeController@memberTier');
    }
);

Route::group(
    [   'prefix' => 'quicksetup',
        'middleware' => ['auth.cms'],
        'as' => 'quicksetup'
    ],
    function() {
        Route::post('setupProgram', 'HomeController@setupProgram');
        Route::post('saveMerchantAppDesign', 'HomeController@saveMerchantAppDesign');
        Route::post('saveCustomerAppDesign', 'HomeController@saveCustomerAppDesign');
        Route::post('saveApplicationDetails', 'HomeController@saveApplicationDetails');
    }
);

Route::group(
    [   'prefix' => 'rush-demo',
        'as' => 'rush-demo'
    ],
    function() {
        Route::get('/', 'RequestDemoController@index');
        Route::post('/', 'RequestDemoController@index');
    }
);

Route::group(
    ['prefix' => 'secret'],
    function() {
        Route::get('cisessview', 'SecretController@cisessview');
    }
);

Route::group(
    [   'prefix' => 'punchcard',
        'middleware' => ['auth.cms','auth.modules'],
        'as' => 'promos'
    ],
    function() {
        Route::get('/', 'PunchcardController@index');
        Route::get('add', 'PunchcardController@add');
        Route::get('delete/{id}', 'PunchcardController@delete');
        Route::get('update/{id}', 'PunchcardController@update');
        Route::post('update/{id}', 'PunchcardController@updatePromoMechanics');
        Route::post('add', 'PunchcardController@add');
        Route::post('voucher/add', 'PunchcardController@voucher_add');
        Route::post('voucher/edit', 'PunchcardController@voucher_edit');
        Route::post('voucher/delete', 'PunchcardController@voucher_delete');
        Route::post('voucher/transfer', 'PunchcardController@voucher_transfer');
        Route::post('voucher/uploadcodes', 'PunchcardController@voucher_uploadcodes');
        Route::get('voucher-codes/{id}', 'PunchcardController@voucher_codes');
        Route::get('voucher-branches', 'PunchcardController@get_voucher_branches');
        Route::post('voucher/uploaditems', 'PunchcardController@voucher_uploaditems');
        Route::post('voucher-codes/{id}', 'PunchcardController@voucher_add_code');
        Route::delete('voucher-codes/{id}', 'PunchcardController@voucher_delete_code');
        Route::post('/product', 'PunchcardController@storeProduct');
        Route::patch('/product', 'PunchcardController@updateProduct');
        Route::delete('/product/{product_id}', 'PunchcardController@deleteProduct');
        Route::get('/product/getProductStamps', 'PunchcardController@getProductStamps');
    }
);

Route::group(
    [   'prefix' => 'billing',
        'middleware' => ['auth.cms','auth.modules'],
        'as' => 'billing'
    ],
    function() {
        Route::get('/', 'AccountController@index');
        Route::get('index', 'AccountController@index');
        Route::get('{id}/payment', 'AccountController@payment');
        Route::get('view/{id}/billing', 'AccountController@billing');
        Route::post('payment', 'AccountController@process_payment');
    }
);

Route::get('billing/statement/{id}', 'AccountController@statement');

Route::group(
    [   'prefix' => 'paynamics'],
    function() {
        Route::post('payment-listener', 'AccountController@payment_listener');
    }
);

Route::group(
    [   'prefix' => 'analytics',
        'middleware' => ['auth.cms','auth.modules'],
        'as' => 'analytics'
    ],
    function() {
        Route::get('/', 'AnalyticsController@index');
        Route::get('index', 'AnalyticsController@index');
    }
);

Route::group(
    [   'prefix' => 'broadcast',
        'middleware' => ['auth.cms','auth.modules'],
        'as' => 'broadcast'
    ],
    function() {
        Route::group(['prefix' => 'sms'], function() {
            Route::get('/', 'BroadcastingController@index');
            Route::post('/', 'BroadcastingController@smsStore');
            Route::delete('/{id}', 'BroadcastingController@smsDelete');
        });
        Route::group(['prefix' => 'push'], function() {
            Route::get('/', 'BroadcastingController@index');
            Route::post('/', 'BroadcastingController@pushStore');
            Route::delete('/{id}', 'BroadcastingController@pushDelete');
        });
        Route::group(['prefix' => 'email'], function() {
            Route::get('/', 'BroadcastingController@index');
            Route::post('/', 'BroadcastingController@emailStore');
            Route::delete('/{id}', 'BroadcastingController@emailDelete');
        });
        Route::group(['prefix' => 'segment'], function() {
            Route::get('/', 'BroadcastingController@index');
            Route::post('/', 'BroadcastingController@segmentStore');
            Route::delete('/{id}', 'BroadcastingController@segmentDelete');
        });
        //MTODO: this is a hidden route, transfer this to superAdmin page
        Route::group(['prefix' => 'pushCredential'], function() {
            Route::get('/', 'BroadcastingController@pushCredentialIndex');
            Route::post('/', 'BroadcastingController@pushCredentialStore');
            Route::post('/pushAndroidTry', 'BroadcastingController@pushAndroidTry');
            Route::post('/pushIosTry', 'BroadcastingController@pushIosTry');
        });
        Route::group(['prefix' => 'smsCredential'], function() {
            Route::get('/', 'BroadcastingController@smsCredentialIndex');
            Route::post('/', 'BroadcastingController@smsCredentialStore');
            Route::post('/smsTry', 'BroadcastingController@smsTry');
        });
        Route::get('/preview/email', 'BroadcastingController@emailPreview');

        Route::group(['prefix' => 'event'], function() {
            Route::post('/edit/{id}', 'BroadcastingController@editEvent');
            Route::post('/publish/{id}', 'BroadcastingController@publishEvent');
            Route::post('/delete/{id}', 'BroadcastingController@unpublishEvent');
            Route::post('/add', 'BroadcastingController@addEvent');
        });
    }
);

Route::group(
    [   'prefix' => 'e'
    ],
    function() {
        Route::get('/{emailId}/{slug}', 'PageController@emailPreview');
    }
);

 Route::group(
     [   'prefix' => 'addons',
         'middleware' => ['auth.cms', 'auth.modules'],
         'as' => 'addons'
     ],
     function() {
        Route::get('/', 'AddonsController@index');
        Route::get('index', 'AddonsController@index');
        Route::post('saveCardDesign', 'AddonsController@saveCardDesign');
        Route::post('orderCard', 'AddonsController@orderCard');
     }
 );

 Route::group(
     [ 'prefix' => 'admin',
       'middleware' => ['auth.admin']
     ],
     function() {
        Route::group( [ 'as' => 'admin-home'], function() {
            Route::get('/', 'AdminController@index');
            Route::get('index', 'AdminController@index');
            Route::get('logout', 'AdminController@logout');
        });

        Route::group( [ 'as' => 'admin-settings'], function() {
            Route::get('settings', 'AdminController@settings');
            Route::post('settings', 'AdminController@settings');
        });

        Route::group( [ 'as' => 'admin-merchant'], function() {
            Route::post('enable', 'AdminController@enable');
            Route::get('merchant', 'AdminController@merchant_index');
            Route::get('merchant/{id}', 'AdminController@merchant');
            Route::get('merchant/{id}/branches', 'AdminController@merchant_branches');
            Route::get('merchant/{id}/employees', 'AdminController@merchant_employees');
            Route::get('merchant/{id}/customers', 'AdminController@merchant_customers');
            Route::get('merchant/{id}/points', 'AdminController@merchant_points');
            Route::get('merchant/{id}/transactions', 'AdminController@merchant_transactions');
            Route::post('merchant/{id}', 'AdminController@merchant');
            Route::get('merchant/{id}/billing', 'AdminController@merchant_billing');
            Route::get('merchant/{merchant_id}/billing-view/{billing_id}', 'AdminController@billing');
            Route::get('merchant/{merchant_id}/statement/{billing_id}', 'AdminController@statement');
        });

        Route::group( [ 'as' => 'admin-management'], function() {
            Route::get('management', 'AdminController@management_branches');
            Route::get('management/branches', 'AdminController@management_branches');
            Route::get('management/employees', 'AdminController@management_employees');
            Route::get('management/customers', 'AdminController@management_customers');
            Route::get('management/points', 'AdminController@management_points');
        });

        Route::group( [ 'as' => 'admin-rewards'], function() {
            Route::get('rewards', 'AdminController@rewards');
            Route::get('rewards/redemption', 'AdminController@rewards_redemption');
            Route::get('rewards/add', 'AdminController@rewards_add');
            Route::post('rewards/add', 'AdminController@rewards_add');
            Route::get('rewards/{id}', 'AdminController@reward');
            Route::post('rewards/{id}', 'AdminController@reward');
            Route::post('reward/status', 'AdminController@reward_status');
        });

        Route::group( [ 'as' => 'admin-billing'], function() {
            Route::get('billing', 'AdminBillingController@index');
            Route::get('billing/{id}', 'AdminBillingController@billing');
            Route::post('billing/{id}', 'AdminBillingController@billing');
        });

        Route::group( [ 'as' => 'admin-prospects'], function() {
            Route::get('prospects', 'AdminProspectsController@index');
            Route::get('prospects/{id}/info', 'AdminProspectsController@info');
            Route::post('prospects/{id}/info', 'AdminProspectsController@info');
            Route::get('bdaccounts/{id}/info', 'AdminProspectsController@bdaccounts');
            Route::post('bdaccounts/{id}/info', 'AdminProspectsController@bdaccounts_update');
            Route::get('bdaccounts/add', 'AdminProspectsController@add_bdaccount');
            Route::post('bdaccounts/add', 'AdminProspectsController@bdaccounts_add');
            Route::get('bdaccounts/{id}/delete', 'AdminProspectsController@bdaccounts_delete');
        });
     }
 );

Route::group(
    [   'prefix' => 'achievement-unlock',
        'middleware' => ['auth.cms', 'auth.modules'],
        'as' => 'achievement-unlock'
    ],
    function() {
        Route::get('/', 'AchievementUnlockController@index');
        Route::post('/', 'AchievementUnlockController@updateAchievementList');
        Route::post('give-points', 'AchievementUnlockController@givePoints');
        Route::post('give-stamps', 'AchievementUnlockController@giveStamps');
    }
);

Route::group(
    [   'prefix' => 'branch-qrcode',
        'middleware' => ['auth.cms']
    ],
    function() {
        Route::get('/', 'BranchQrcodeController@index');
        Route::get('generate', 'BranchQrcodeController@generate');
    }
);

Route::group(
    [   'prefix' => 'cards-qrcode',
        'middleware' => ['auth.cms']
    ],
    function() {
        Route::get('/membership/{membership}/count/{count}', 'CardsQrcodeController@index');
    }
);

Route::group(
    [   'prefix' => 'redemption',
        'middleware' => ['auth.cms','auth.modules'],
        'as' => 'redemption'
    ],
    function() {
        Route::get('/', 'RedemptionController@index');
        Route::post('save', 'RedemptionController@save');
        Route::post('delete', 'RedemptionController@delete');
        Route::post('enable', 'RedemptionController@enable');
        Route::post('voucher/add', 'RedemptionController@voucher_add');
        Route::post('voucher/edit', 'RedemptionController@voucher_edit');
        Route::post('voucher/delete', 'RedemptionController@voucher_delete');
        Route::post('voucher/uploadcodes', 'RedemptionController@voucher_uploadcodes');
    }
);

Route::group(
    [   'prefix' => 'transactionhistory',
        'middleware' => ['auth.cms','auth.modules'],
        'as' => 'transactionhistory'
    ],
    function() {
        Route::get('/', 'TransactionController@index');
        Route::get('/attendance', 'TransactionController@attendanceAjaxlist');
        Route::get('/attendance/{attendanceId}/attendees', 'TransactionController@attendeesAjaxList');
        Route::post('/attendance', 'TransactionController@attendanceStore');
        Route::post('/attendance/earn', 'TransactionController@attendanceClientEarn');
        Route::post('/earn/void/{transactionReferenceCode}', 'TransactionController@voidEarn');
        Route::post('/void-package/{transactionReferenceCode}', 'TransactionController@voidPointsPackage');
        Route::post('/void-punchcard/{transactionReferenceCode}', 'TransactionController@voidTransactionPunchcard');
        Route::post('points-seeding-upload','TransactionController@pointsSeedingUpload');
        Route::post('getreceiptdata','TransactionController@get_receipt_data');
        Route::get('gethistory', 'TransactionController@getHistory');
        Route::get('votes-dt-ajax', 'TransactionController@votesDtAjax');
        Route::get('get-all-history', 'TransactionController@getAllHistory');
    }
);

Route::group(
    [   'prefix' => 'settings',
        'middleware' => ['auth.cms','auth.modules'],
        'as' => 'settings'
    ],
    function() {
        Route::get('/', 'SettingsController@index');
        Route::post('/', 'SettingsController@store');
        Route::post('/capping', 'SettingsController@storeCapping');
    }
);

Route::group(
    [   'prefix' => 'promos',
        'middleware' => ['auth.cms','auth.modules'],
        'as' => 'promos'
    ],
    function() {
        Route::get('/', 'ProgramMechanicsController@index');
        Route::post('/', 'ProgramMechanicsController@store');
        Route::post('/membership', 'ProgramMechanicsController@storeMembership');
        Route::post('/product', 'ProgramMechanicsController@storeProduct');
        Route::patch('/product', 'ProgramMechanicsController@updateProduct');
        Route::delete('/product/{product_id}', 'ProgramMechanicsController@deleteProduct');
        Route::post('/product_qr', 'ProgramMechanicsController@storeProductQR');
        Route::patch('/product_qr', 'ProgramMechanicsController@updateProductQR');
        Route::delete('/product_qr/{product_qr_id}', 'ProgramMechanicsController@deleteProductQR');
    }
);

Route::group(
    [   'prefix' => 'design',
        'middleware' => ['auth.cms','auth.modules'],
        'as' => 'design'
    ],
    function() {
        Route::get('/', 'DesignController@index');
        Route::post('saveMerchantAppDesign', 'DesignController@saveMerchantAppDesign');
        Route::post('saveCustomerAppDesign', 'DesignController@saveCustomerAppDesign');
        Route::post('saveStampIcon', 'DesignController@saveStampIcon');
    }
);

Route::group(
    [   'prefix' => 'management',
        'middleware' => ['auth.cms','auth.modules'],
        'as' => 'management'
    ],
    function() {
        Route::get('/', 'ManagementController@index');
        Route::post('addbranch', 'ManagementController@addbranch');
        Route::post('editbranch', 'ManagementController@editbranch');
        Route::post('deletebranch/{id}', 'ManagementController@deletebranch');
        Route::post('addEmployee', 'ManagementController@addEmployee');
        Route::post('editEmployee', 'ManagementController@editEmployee');
        Route::post('deleteEmployee/{id}', 'ManagementController@deleteEmployee');
        Route::get('getCustomerPoints', 'ManagementController@getCustomerPoints');
        Route::get('getCustomer', 'ManagementController@getCustomer');
        Route::post('addCustomerData', 'ManagementController@addCustomerData');
        Route::post('uploadCustomerData', 'ManagementController@uploadCustomerData');
        Route::post('editCustomerData', 'ManagementController@editCustomerData');
        Route::get('getCustomerWithReserationsDetails', 'ManagementController@getCustomerWithReserationsDetails');
        route::get('customer-stamps-summary', 'ManagementController@getCustomerStampsSummary');
        route::get('class-package-summary', 'ManagementController@getClassSummary');
        route::post('redeem-stamps', 'ManagementController@postRedeemStamps');
        route::post('expireCustomerPackage', 'ManagementController@expireCustomerPackage');
        route::post('expirePointsCustomerPackage', 'ManagementController@expirePointsCustomerPackage');
        route::post('addClientCode', 'ManagementController@addClientCode');
        route::post('editClientCode', 'ManagementController@editClientCode');
        route::post('importClientCode', 'ManagementController@importClientCode');
        route::get('getClientCode', 'ManagementController@getClientCode');
        route::get('getBranches', 'ManagementController@getBranches');
        route::get('getEmployees', 'ManagementController@getEmployees');
    }
);

Route::group(
    [   'prefix' => 'help',
        'middleware' => ['auth.cms','auth.modules'],
        'as' => 'help'
    ],
    function() {
        Route::get('/', 'AccountController@help');
    }
);

Route::group(
    [   'prefix' => 'changepass',
        'middleware' => ['auth.cms']
    ],
    function() {
        Route::get('/', 'AccountController@changepass');
        Route::post('/', 'AccountController@process_changepass');
    }
);

Route::group(
    [   'prefix' => 'event-attendees',
        'middleware' => ['auth.cms','auth.modules'],
        'as' => 'event-attendees'
    ],
    function() {
        Route::get('/', 'EventAttendeesController@index');
        Route::post('/attendee', 'EventAttendeesController@storeAttendee');
        Route::post('/attendee/update', 'EventAttendeesController@updateAttendee');
        Route::post('/attendee/delete', 'EventAttendeesController@deleteAttendee');
        Route::post('/attendee/confirm', 'EventAttendeesController@confirmAttendee');
        Route::post('/attendee/upload', 'EventAttendeesController@uploadAttendee');
    }
);

Route::group(
    [   'prefix' => 'users',
        'middleware' => ['auth.cms','auth.modules'],
        'as' => 'users'
    ],
    function() {
        Route::get('/', 'UserController@index');
        Route::post('/', 'UserController@index');
        Route::get('adduser', 'UserController@add');
        Route::get('{id}/edituser', 'UserController@edit');
        Route::post('{id}/edituser', 'UserController@edit');
        Route::post('deleteuser/{id}', 'UserController@delete');
        Route::post('enableuser/{id}', 'UserController@enable');
        Route::post('adduser', 'UserController@add');
    }
);

Route::group(
    [   'prefix' => 'class',
        'middleware' => ['auth.ci','auth.modules'],
        'as' => 'class'
    ],
    function() {
        Route::group(['prefix' => 'package'], function() {
            Route::get('/', 'ClassManagementController@index');
            Route::post('/', 'ClassManagementController@storePackage');
            Route::delete('/', 'ClassManagementController@deletePackage');
            Route::post('publishStamp', 'ClassManagementController@publishStamp');
            Route::get('getPackageStamp', 'ClassManagementController@getPackageStamp');
        });

        Route::group(['prefix' => 'instructor'], function() {
            Route::get('/', 'ClassManagementController@index');
            Route::post('/', 'ClassManagementController@storeInstructor');
            Route::delete('/', 'ClassManagementController@deleteInstructor');
        });

        Route::group(['prefix' => 'class'], function() {
            Route::get('/', 'ClassManagementController@index');
            Route::post('/', 'ClassManagementController@storeClass');
            Route::delete('/', 'ClassManagementController@deleteClass');
        });

        Route::group(['prefix' => 'classsched'], function() {
            Route::get('/', 'ClassManagementController@index');
            Route::post('/', 'ClassManagementController@storeClassSchedule');
            Route::delete('/', 'ClassManagementController@deleteClassSchedule');
            Route::get('getClassInstructorAjax', 'ClassManagementController@getClassInstructorAjax');
            Route::get('validateClassScheduleList', 'ClassManagementController@validateClassScheduleList');
        });

        Route::group(['prefix' => 'classlist'], function() {
            Route::get('/', 'ClassManagementController@index');
            Route::post('updateClassListSchedule', 'ClassManagementController@updateClassListSchedule');
            Route::post('cancelClassListSchedule', 'ClassManagementController@cancelClassListSchedule');
            Route::post('registerCustomerToClass', 'ClassManagementController@registerCustomerToClass');
            Route::post('deleteReservation', 'ClassManagementController@deleteReservation');
            Route::post('confirmAttendanceReservation', 'ClassManagementController@confirmAttendanceReservation');
            Route::get('getAllCustomerFromClass', 'ClassManagementController@getAllCustomerFromClass');
            Route::get('getClassListPageData', 'ClassManagementController@getClassListPageData');
            Route::get('getInstructorOnGoingClassScheduleList', 'ClassManagementController@getInstructorOnGoingClassScheduleList');
            Route::get('getClassOnGoingClassScheduleList', 'ClassManagementController@getClassOnGoingClassScheduleList');
            Route::get('getMultibookFormDates', 'ClassManagementController@getMultibookFormDates');
            Route::post('multipleBookCustomerToClass', 'ClassManagementController@multipleBookCustomerToClass');
        });
    }
);

Route::group(
    [   'prefix' => 'customers',
        'middleware' => ['auth.cms']
    ],
    function() {
        Route::get('generatePdf', 'CustomerController@generatePdf');
        Route::get('generatePdf/{mobile_no}', 'CustomerController@generateCustomerPdf');
        Route::get('register', 'CustomerController@registrationForm');
        Route::post('register', 'CustomerController@submitRegistration');
        Route::get('importFromCSV', 'CustomerController@importFromCSVForm');
        Route::post('importFromCSV', 'CustomerController@submitImportCSV');
        Route::get('generatePdf/branch/{branch}', 'CustomerController@generateCustomerPdfByBranch');
    }
);

Route::group(['prefix' => 'qr_code'], function(){
    Route::get('download/{branch_name}/{name}/{points}/{scan_limit?}', 'CustomerController@qr_code_download');
});

Route::group(
    [   'prefix' => 'vouchers',
        'middleware' => ['auth.cms']
    ],
    function() {
        Route::get('addVoucher', 'VoucherController@addVoucherForm');
        Route::post('addVoucher', 'VoucherController@submitAddVoucherForm');
        Route::get('importVoucherCodes', 'VoucherController@importVoucherCodesForm');
        Route::post('importVoucherCodes', 'VoucherController@submitImportVoucherCodesForm');
    }
);

Route::group(
    [   'prefix' => 'points-seeding',
        'middleware' => ['auth.cms','auth.modules'],
        'as' => 'transactionhistory'
    ],
    function() {
        Route::get('{id}', 'TransactionController@points_seeding');
        Route::get('{id}/mass-seed', 'TransactionController@pointsSeedingMassSeed');
        Route::post('data/{id}', 'TransactionController@points_seeding_data');
        Route::post('batch', 'TransactionController@points_seeding_batch');
        Route::get('seed/{id}', 'TransactionController@points_seeding_seed');
    }
);

Route::group(
    [   'prefix' => 'verifyaccount',
        'middleware' => ['auth.cms','auth.modules'],
        'as' => 'promos'
    ],
    function() {
        Route::post('/', 'OnboardingController@verifyAccount');
    }
);

Route::get('onboarding-cms/login', 'OnboardingCmsController@login');
Route::post('onboarding-cms/login', 'OnboardingCmsController@processLogin');
Route::get('onboarding-cms/logout', 'OnboardingCmsController@logout');

Route::group(
    [   'prefix' => 'onboarding-cms',
        'middleware' => ['auth.onboardingcms'],
        'as' => 'onboarding-cms'
    ],
    function() {
        Route::get('/', 'OnboardingCmsController@index');

        Route::group([ 'prefix' => 'news'], function() {
            Route::get('/', 'OnboardingCmsController@news');
            Route::get('create', 'OnboardingCmsController@createNews');
            Route::post('create', 'OnboardingCmsController@processCreateNews');
            Route::post('visibility', 'OnboardingCmsController@newsVisibility');
            Route::get('edit/{id}', 'OnboardingCmsController@editNews');
            Route::post('edit/{id}', 'OnboardingCmsController@processEditNews');
            Route::post('delete', 'OnboardingCmsController@deleteNews');
        });
        Route::group( [ 'prefix' => 'clientlogos'], function() {
            Route::get('/', 'OnboardingCmsController@clientLogos');
            Route::post('visibility', 'OnboardingCmsController@clientLogosVisibility');
            Route::post('add', 'OnboardingCmsController@addClientLogo');
            Route::post('edit', 'OnboardingCmsController@editClientLogo');
            Route::post('delete', 'OnboardingCmsController@deleteClientLogo');
        });
    }
);