<?php

namespace App\Http\Controllers;

use Event;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Rush\Modules\Alert\Events\RequestDemoEvent;

class RequestDemoController extends Controller
{
    protected $request;

    public function __construct( Request $request ) {
        $this->request = $request;
    }

    public function index()
    {
        $data['form'] = 'show';
        $data['industry_options'] = [
                                    '' => 'Select Industry *',
                                    'Automotive Dealers and Manufacturers'=>'Automotive Dealers and Manufacturers',
                                    'Automotive Services'=>'Automotive Services',
                                    'Entertainment'=>'Entertainment',
                                    'Fitness'=>'Fitness',
                                    'Food and Beverage Establishment'=>'Food and Beverage Establishment',
                                    'Fuel and Petroleum'=>'Fuel and Petroleum',
                                    'Grocery and Supermarket'=>'Grocery and Supermarket',
                                    'Grooming and Wellness'=>'Grooming and Wellness',
                                    'Hospitals and Healthcare Services'=>'Hospitals and Healthcare Services',
                                    'Insurance'=>'Insurance',
                                    'Petroleum'=>'Petroleum',
                                    'Retail - Apparel' => 'Retail - Apparel',
                                    'Retail - Electronics' => 'Retail - Electronics',
                                    'Retail - Household and Hardware' => 'Retail - Household and Hardware',
                                    'Retail - Sporting Goods' => 'Retail - Sporting Goods',
                                    'Telecommunication and Utilities' => 'Telecommunication and Utilities',
                                    'Transportation' => 'Transportation',
                                    'Travel and Leisure' => 'Travel and Leisure',
                                    'Others'=>'Others'
                                    ];
        $data['objective_options'] = [
                                    'Enhance customer engagement',
                                    'Improve customer retention',
                                    'Increase frequency of customer visits',
                                    'Drive revenue per transaction',
                                    'Gather insights and understand customer behavior',
                                    'Promote and push new products and services',
                                    'Others'
                                    ];
        if ( $this->request->isMethod('post') ) {

            $rules = array(
              'fname' => 'required',
              'lname' => 'required',
              'companyname' => 'required',
              'industry' => 'required',
              'mobile' => 'required',
              'email' => 'required|email',
              'g-recaptcha-response' => 'recaptcha_check'
            );

            $error_messages = [ 'recaptcha_check' => 'The Captcha field is invalid.' ];

            $input['fname'] = $this->request->fname;
            $input['lname'] = $this->request->lname;
            $input['name'] = $this->request->fname . ' ' . $this->request->lname;
            $input['companyname'] = $this->request->companyname;
            $input['industry'] = $this->request->industry;
            $input['mobile'] = isset($this->request->mobile) ? '+63' . $this->request->mobile : '';
            $input['email'] = $this->request->email;
            $input['objective'] = $this->request->objective;
            $input['request_message'] = $this->request->message;
            $input['package'] = isset($this->request->package) ? implode(', ', $this->request->package) : "";
            $input['g-recaptcha-response'] = $this->request['g-recaptcha-response'];
            
            $input['url'] = 'DEMO REQUEST';
            $input['ip_address'] = $request->ip();

            Validator::extend('recaptcha_check', function ($attribute, $value, $parameters, $validator) {
	            return $this->recaptcha_check($value);
	        });

            $validator = Validator::make( $input, $rules, $error_messages );
            if ( $validator->fails() )
            {
                $data['errors'] = $validator->messages();
                $this->request->flash();
            } else {
                Event::fire(new RequestDemoEvent( $input ) );
                $data['form'] = 'hide';
                $data['message'] = '<p>Thank you for your interest in RUSH!</p><p>An account manager will get in touch with you within 1 business day to arrange the demo of the loyalty program solution for your needs.</p>';
            }
        }

        return view('demo.index', $data);
    }

    public function recaptcha_check($captcha)
    {
        $verify_captcha = json_decode(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='. getenv('G_RECAPTCHA_SECRET') .'&response='. $captcha));
        if($verify_captcha) {
            if (isset($verify_captcha->success) && $verify_captcha->success) {
                return true;
            }
        }
        return false;
    }

}
