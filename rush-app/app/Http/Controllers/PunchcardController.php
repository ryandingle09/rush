<?php

namespace App\Http\Controllers;

use Rush\Modules\Merchant\Services\MerchantService;
use Rush\Modules\Merchant\Models\VoucherCodeModel;
use Illuminate\Http\Request;
use Intervention\Image\ImageManager;
use File;
use Exception;
use Rush\Modules\Merchant\Services\ProductService;
use Rush\Modules\Merchant\Services\BranchService;

use Carbon\Carbon;
use Validator;
use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory;
use Rush\Modules\Helpers\Facades\StorageHelper;

class PunchcardController extends AbstractCmsController
{
    protected $merchantService;
    protected $productService;
    protected $branchService;
    
    public function __construct(BranchService $branchService, ProductService $productService, MerchantService $merchantService)
    {
        $this->merchantService = $merchantService;
        $this->productService = $productService;
        $this->branchService = $branchService;
    }

    public function index(Request $request)
    {
        $merchant_id = $request->session()->get( 'user_id' );
        $package_id = $request->session()->get( 'package_id' );
        $data['merchant'] = $merchant = $this->merchantService->getById( $merchant_id );
        $data['branches'] = $merchant->branches()->where('is_deleted',0)->get()->toArray();
        $data['vouchers'] = $this->merchantService->getVouchers( $data['branches'] );
        $data['products'] = $this->productService->getAll($merchant_id);
        $data['is_enable_product_module'] = $merchant->settings->enable_product_module;
        
        $data['promos'] = $this->merchantService->getSavedPromoMechanics( $merchant_id , $package_id );
        $data['menu'] = ( $package_id == 1 ) ? 'menu.loyalty' : 'menu.punchcard' ;
        $data['login_name'] = $request->session()->get('login_name');
        $data['module'] = 'promos';
        $data['header_text'] = 'Punchcard Management';
        $data['errors'] = $request->session()->get('errors') ? $request->session()->get('errors') : null;
        $data['message'] = $request->session()->get('message') ? $request->session()->get('message') : null;
        return view( 'punchcard.index', $data );
    }

    public function add(Request $request)
    {
        $merchant_id = $request->session()->get( 'user_id' );
        $package_id = $request->session()->get( 'package_id' );
        
        $merchant = $this->merchantService->getById( $merchant_id );
        $data['branches'] = $merchant->branches()->where('is_deleted',0)->get()->toArray();

        if ( $request->isMethod('post') ) {
            
            $stampsCount = $this->getStampCount($request);
            $validator = $this->save_validate( $request, $stampsCount );
            if( $validator->fails() ) {
                $errors = $validator->errors();
                $data['errors'] = $errors->all();
                $request->flash();
            } else {
                $process = $this->merchantService->isPromoDurationValid( $merchant_id, $request->durationStart, $request->durationEnd );
                if ( $process == false ) {
                    $data['errors'] = ['Duration date conflict!'];
                    $request->flash();
                } else {
                    $files = $this->upload_rewards_image($request, $stampsCount);
                    $rewards = array();
                    if( $files ) {
                        foreach($files as $key => $file) {
                            $reward_no_stamps = $file['stamp_no'];
                            $reward_description = $request['stamp_reward_description_'. $reward_no_stamps];
                            $reward_details = $request['stamp_reward_details_'. $reward_no_stamps];
                            $reward_image_url = $file['image'];
                            $reward_branch = "[0]";
                            if ( isset($request['stamp_reward_branch_'. $reward_no_stamps]) && count($data['branches']) != count( $request['stamp_reward_branch_'. $reward_no_stamps] ) ) {
                              $reward_branch = str_replace('"','',json_encode($request['stamp_reward_branch_'. $reward_no_stamps] ) );
                            }
                            $rewards[] = array(
                                    'no_stamps' => $reward_no_stamps,
                                    'reward'    => $reward_description,
                                    'details'    => $reward_details,
                                    'image_url' => "/repository/merchant/rewards/" . $reward_image_url,
                                    'branch_ids' => $reward_branch
                                );
                        }
                    }

                    $save_data['merchant_id'] = $merchant_id;
                    $save_data['package_id'] = $package_id;
                    $save_data['stampsCount'] = $stampsCount;
                    $save_data['title'] = $title = $request->promoTitle;
                    $save_data['duration_promo_start'] = $request->durationStart;
                    $save_data['duration_promo_end'] = $request->durationEnd;
                    $save_data['redemption_promo_start'] = $request->redemptionDurationStart;
                    $save_data['redemption_promo_end'] = $request->redemptionDurationEnd;
                    $save_data['amount'] = $request->amount;
                    $save_data['status'] = $request->status;
                    
                    $punchcard_id = $this->merchantService->savePromoMechanics( $save_data, $rewards );
                    $request->session()->flash('message', $title .' punchcard has been added!');
                    return redirect( url('punchcard') );
                }
            }
        }

        $data['menu'] = ( $package_id == 1 ) ? 'menu.loyalty' : 'menu.punchcard' ;
        $data['login_name'] = $request->session()->get('login_name');
        $data['module'] = 'promos';
        $data['header_text'] = 'Punchcard Management';
        $data['addRemoveButton'] = $this->getAddRemoveButton();

        return view( 'punchcard.add', $data );
    }

    public function delete(Request $request)
    {
        $merchant_id = $request->session()->get( 'user_id' );
        $id = $request->id;
        $promo = $this->merchantService->getPromoMechanics( $id );
        if( $promo ) {
            if ( $promo['status'] == 1 ) {
                if( $promo['duration_to_date'] <= date('Y-m-d') ) {
                    $process_expired = $this->merchantService->deleteExpiredPromoMechanics( $merchant_id, $id);
                    if ( $process_expired )
                    {
                        $request->session()->flash('message', $promo['promo_title'] . ' punchcard successfuly deleted!');
                    }
                } else {
                    $request->session()->flash('errors', "Deleting a published punchcard is not allowed!");
                }
            } else {
                $process = $this->merchantService->deletePromoMechanics( $merchant_id, $id);
                if ( $process )
                {
                    $request->session()->flash('message', $promo['promo_title'] . ' punchcard successfuly deleted!');
                }
            }
        }
        return redirect( url('punchcard') );
    }

    public function update(Request $request)
    {
        $merchant_id = $request->session()->get( 'user_id' );
        $package_id = $request->session()->get( 'package_id' );
        $id = $request->id;
        $promo = $this->merchantService->getPromoMechanics( $id );
        if($promo) {

            $data['menu'] = ( $package_id == 1 ) ? 'menu.loyalty' : 'menu.punchcard' ;
            $data['login_name'] = $request->session()->get('login_name');
            $data['module'] = 'promos';
            $data['header_text'] = 'Punchcard Management';
            $data['errors'] = $request->session()->get('errors') ? $request->session()->get('errors') : null;
            $data['message'] = $request->session()->get('message') ? $request->session()->get('message') : null;
            
            $data['promoId'] = $promo['promo_id'];
            $data['promo_title'] = $promo['promo_title'];
            $data['duration_from_date'] = $promo['duration_from_date'];
            $data['duration_to_date'] = $promo['duration_to_date'];
            $data['redemption_duration_from_date'] = $promo['redemption_duration_from_date'];
            $data['redemption_duration_to_date'] = $promo['redemption_duration_to_date'];
            $data['amount'] = $promo['amount'];
            $data['status'] = $promo['status'];
            $data['n'] = $promo['num_stamps'];

            //initial reward data
            for($i = 1; $i <= $promo['num_stamps']; $i++ ) {
                $data['reward_image_canvas_'. $i] = null;
                $data['reward_img_class_'. $i] = null;
                $data['reward_stamp_class_'. $i] = null;
                $data['reward_description_' . $i] = null; 
                $data['reward_details_' . $i] = null; 
                $data['reward_branches_' . $i] = []; 
            }

            //set existing reward data
            foreach($promo['rewards'] as $reward) {
                $data['reward_image_canvas_' . $reward['no_stamps']] = '<canvas width="500" height="100" style="background-image:url('. StorageHelper::repositoryUrlForView($reward['image_url']) .');background-size:cover;"></canvas>';
                $data['reward_img_class_'. $reward['no_stamps']] = "with-img";
                $data['reward_stamp_class_'. $reward['no_stamps']] = "with-stamp";
                $data['reward_description_' . $reward['no_stamps']] = $reward['reward'];
                $data['reward_details_' . $reward['no_stamps']] = $reward['details'];
                $data['reward_branches_' . $reward['no_stamps']] = json_decode($reward['branch_ids']);
            }

            $data['addRemoveButton'] = ( $promo['status'] == 1) ? '' : $this->getAddRemoveButton($promo['num_stamps'], $promo['multiplier']);

            $merchant = $this->merchantService->getById( $merchant_id );
            $data['branches'] = $merchant->branches()->where('is_deleted',0)->get()->toArray();

            return view( 'punchcard.update', $data );
        } else {
            return redirect( url('punchcard') );
        }
    }

    public function updatePromoMechanics(Request $request)
    {
        $merchant_id = $request->session()->get( 'user_id' );
        $package_id = $request->session()->get( 'package_id' );
        $id = $request->id;

        $title = $request->promoTitle;
        $duration_promo_start = $request->durationStart;
        $duration_promo_end = $request->durationEnd;
        $redemption_promo_start = $request->redemptionDurationStart;
        $redemption_promo_end = $request->redemptionDurationEnd;
        $amount = $request->amount;
        $status = $request->status;

        $merchant = $this->merchantService->getById( $merchant_id );
        $branches_count = $merchant->branches()->where('is_deleted',0)->count();

        $promo = $this->merchantService->getPromoMechanics( $id );
        try {
            if ($promo['status'] == '1' && ($promo['status'] != $status)) {
                throw new Exception("A published punchcard cannot be set to draft!");
            }
            if (!$promo) {
                throw new Exception("Punchcard requested to update does not exist!");
            }
            if ($promo['duration_from_date'] != $duration_promo_start && $promo['status'] == 1) {
                throw new Exception("Promo Duration start cannot be edited when punchcard is already published!");
            }
            if ($duration_promo_end < $duration_promo_start) {
                throw new Exception("Invalid promo Duration range!");
            }
            if ($redemption_promo_start < $duration_promo_start) {
                throw new Exception("Redemption may only start on or after the promo Duration start!");
            }
            if ($redemption_promo_end < $redemption_promo_start) {
                throw new Exception("Invalid promo Redemption range!");
            }
            if ($redemption_promo_end < $duration_promo_end) {
                throw new Exception("Redemption end should be on or after the Duration end!");
            }

            if ( $this->merchantService->isPromoDurationValid( $merchant_id, $duration_promo_start, $duration_promo_end, $id ) == false ) {
                throw new Exception("Duration date conflict!");
            }

            $stampsCount = $this->getStampCount($request);
            $files = $this->upload_rewards_image($request, $stampsCount);
            $rewards_image_changed = array();

            // update promo info
            $this->merchantService->updatePromoMechanics( $merchant_id, $id, $status, $title, $duration_promo_start, $duration_promo_end, $redemption_promo_start, $redemption_promo_end, $amount, $stampsCount);

            // updating rewards
            for($i = 1; $i <= $stampsCount; $i++) {
                $isRewardChanged = $request['stamp_reward_change'. $i];
                
                //if reward has been changed
                if( $isRewardChanged ) {
                    //check if it has value
                    $reward_description = $request['stamp_reward_description_'. $i];
                    $reward_details = $request['stamp_reward_details_'. $i];
                    $reward_image_url   = isset($files['stamp_reward_image_'. $i]) ? "/repository/merchant/rewards/" . $files['stamp_reward_image_'. $i]['image'] : false;
                    $reward_branch = "[0]";
                    if ( isset($request['stamp_reward_branch_'. $i]) && count($request['stamp_reward_branch_'. $i]) != $branches_count ) {
                        $reward_branch = str_replace('"','',json_encode($request['stamp_reward_branch_'. $i] ) );
                    }

                    $reward = $this->merchantService->getPromoReward($id, $i);
                    if( $reward_description || $reward_image_url || $reward_details || $reward_branch ) {
                        // update or add stamp
                        if( $reward ) {
                            // reward exist so update this reward
                            $this->merchantService->updatePromoReward($id, $i, $reward_description, $reward_image_url, $reward_details, $reward_branch);
                        }else{
                            // reward doesn't exist so add this reward
                            $this->merchantService->addPromoReward($id, $i, $reward_description, $reward_image_url, $merchant_id, $reward_details, $reward_branch);
                        }
                    } else {
                        //remove stamp
                        $this->merchantService->removePromoReward($id, $i);
                    }
                }
            }
            
            } catch (Exception $e) {
                $request->session()->flash('errors', $e->getMessage() );
            } finally {
                if ( $request->session()->get('errors') ){
                    return redirect( url('punchcard/update/'. $id) );
                }
                return redirect( url('punchcard') );
            }
    }

    public function save_validate($request, $stampsCount)
    {
        $rules = array(
            'promoTitle' => 'required',
            'durationStart' => 'required',
            'durationEnd' => 'required',
            'redemptionDurationStart' => 'required',
            'redemptionDurationEnd' => 'required',
            'amount' => 'required|numeric|min:1',
          );
        
        for($i = 1; $i <= $stampsCount; $i++) {

            if ( $request['stamp_reward_branch_'. $i ] || $request['stamp_reward_description_'. $i ] || $request->file('stamp_reward_image_'. $i ) )
            {
                if( !$request['stamp_reward_description_'. $i] == false )
                {
                    $rules['stamp_reward_description_'. $i] = 'required';
                }

                if( !$request['stamp_reward_branch_'. $i] == false )
                {
                    $rules['stamp_reward_branch_'. $i] = 'required';
                }

                if( !$request->file('stamp_reward_image_'. $i ) == false )
                {
                    $rules['stamp_reward_image_'. $i] = 'required';
                }

                $data['stamp_reward_branch_'. $i] = $request['stamp_reward_branch_'. $i];
                $data['stamp_reward_description_'. $i] = $request['stamp_reward_description_'. $i];
                $data['stamp_reward_image_'. $i] = $request['stamp_reward_image_'. $i];
            }
        }

        $data['promoTitle'] = $request->promoTitle;
        $data['durationStart'] = $request->durationStart;
        $data['durationEnd'] = $request->durationEnd;
        $data['redemptionDurationStart'] = $request->redemptionDurationStart;
        $data['redemptionDurationEnd'] = $request->redemptionDurationEnd;
        $data['amount'] = $request->amount;
        
        return Validator::make( $data, $rules );
    }

    public function upload_rewards_image($request, $stampsCount = 9)
    {
        $merchant_id = $request->session()->get( 'user_id' );
        $stamps = array();
        for ($i=1; $i <= $stampsCount; $i++) {
            $stamps[] = 'stamp_reward_image_'.$i;
        }
        $uploaded_files = [];
        $i = 1;
        foreach($stamps as $stamp) {
            $file = $request->file($stamp);
            if ( $file ) {
                if ( $this->check_image_supported( $file->getMimeType() ) ) {
                    $disk = StorageHelper::getRepositoryDisk();
                    $filename = $merchant_id."-".sprintf('%02d', rand(1,99))."-".time().'.'.$file->getClientOriginalExtension();
                    $upload_path = 'merchant/rewards/';
                    $save_path = $upload_path . $filename;
                    $manager = new ImageManager();
                    $image = $manager->make($file)->stream();
                    $disk->put($save_path, $image->__toString(), 'public');
                    $uploaded_files[$stamp]['image'] = $filename;
                    $uploaded_files[$stamp]['stamp_no'] = $i;
                }
            }
            $i++;
        }
        return $uploaded_files;
    }

    public function getStampCount($request)
    {
        $stampsCount = 0;
        foreach ($_POST as $key => $value) {
            if (strpos($key, 'stamp_reward_description_') !== false) {
                $stampsCount++;
            }
        }
        return $stampsCount;
    }

    public function getAddRemoveButton($max = 15, $multiplier = 3)
    {
        //MTODO: move this to view once we are using Laravel views
        $addRemoveButton =
        '   <button type="button" class="btn btn-default btn-sm hide" id="removeStampsBtn">
            <span class="glyphicon glyphicon-minus-sign"></span> Remove %d Stamps
            </button>
            <button type="button" class="btn btn-default btn-sm hide" id="addStampsBtn">
            <span class="glyphicon glyphicon-plus-sign"></span> Add %d Stamps
            </button>
            <input type="hidden" name="stampsMax" id="stampsMax" value="%d"/>
            <input type="hidden" name="stampsMultiplier" id="stampsMultiplier" value="%d"/>
        ';
        return sprintf($addRemoveButton, $multiplier, $multiplier, $max, $multiplier);
    }

    public function check_image_supported( $mime_type )
    {
      if ( in_array( $mime_type, array('image/jpg', 'image/gif', 'image/jpeg','image/png') ) )
      {
          return true;
      }
      return false;
    }

    public function voucher_add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'voucher-name' => 'required',
            'branch_ids' => 'required',
            'voucher_image' => 'required|image',
        ]);

        if ($validator->fails()) {
            return redirect('punchcard#voucher')
                ->withErrors($validator)
                ->withInput();
        } else {

            $voucher_image = $request->file('voucher_image', FALSE);

            if ( $voucher_image )
            {
                $merchant_id = $request->session()->get( 'user_id' );
                if ( $this->check_image_supported( $voucher_image->getMimeType() ) ) {
                    $ext = $voucher_image->getClientOriginalExtension();
                    $voucher_image_filename =  $merchant_id . '_' . Carbon::now()->timestamp . '_' . sprintf('%02d', rand(1,99)) . '.' . $ext;
                    $voucher_image_path = '/repository/merchant/voucher/';
                    $save_path = base_path() . '/..' . $voucher_image_path . $voucher_image_filename;

                    $manager = new ImageManager();
                    $image = $manager->make($voucher_image)->resize(350, 200)->save($save_path);
                    $request->voucher_image = $voucher_image_filename;
                } else {
                    $error = true;
                }
            }

            $this->merchantService->addVoucher( $request );

            return redirect('punchcard#voucher')->with('message', 'New voucher has been added!');
        }
    }

    public function voucher_edit(Request $request)
    {
        $rules = [
            'voucher_id' => 'required',
            'evoucher-name' => 'required',
        ];

        if ( $request->evoucher_image_changed == 1 ) {
            $rules['evoucher_image'] = 'required|image';
        }

        $validator = Validator::make($request->all(), $rules );

        if ($validator->fails()) {
            return redirect('punchcard#voucher')
                ->withErrors($validator)
                ->withInput();
        } else {

          if ( $request->evoucher_image_changed == 1 ) {
            $voucher_image = $request->file('evoucher_image', FALSE);

            if ( $voucher_image )
            {
                $merchant_id = $request->session()->get( 'user_id' );
                if ( $this->check_image_supported( $voucher_image->getMimeType() ) ) {
                    $ext = $voucher_image->getClientOriginalExtension();
                    $voucher_image_filename =  $merchant_id . '_' . Carbon::now()->timestamp . '_' . sprintf('%02d', rand(1,99)) . '.' . $ext;
                    $voucher_image_path = '/repository/merchant/voucher/';
                    $save_path = base_path() . '/..' . $voucher_image_path . $voucher_image_filename;

                    $manager = new ImageManager();
                    $image = $manager->make($voucher_image)->resize(350, 200)->save($save_path);
                    $request->evoucher_image = $voucher_image_filename;
                } else {
                    $error = true;
                }
            }
          }

          $this->merchantService->editVoucher( $request );

          return redirect('punchcard#voucher')
            ->with('message', 'Voucher has been modified!');
        }
    }

    public function voucher_delete(Request $request)
    {
      $validator = Validator::make($request->all(), [
            'voucher_id' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('punchcard#voucher')
                ->withErrors($validator)
                ->withInput();
        } else {
          $this->merchantService->deleteVoucher( $request->voucher_id );
          return redirect('punchcard#voucher')
            ->with('message', 'Voucher has been deleted!');
        }
    }

    public function voucher_uploadcodes(Request $request)
    {
      $validator = Validator::make($request->all(), [
            'voucher_id' => 'required',
            'voucher-import' => 'required|file|mimes:txt,csv',
        ]);

        if ($validator->fails()) {
            return redirect('punchcard/voucher-codes/' . $request->voucher_id)
                ->withErrors($validator)
                ->withInput();
        } else {
          $response = $this->submitImportVoucherCodesForm( $request );
          return redirect('punchcard/voucher-codes/' . $request->voucher_id)
            ->with('data', ['message' => 'Voucher codes have been uploaded!', 'insert_response' => $response]);
        }
    }

    private function submitImportVoucherCodesForm( $request )
    {
        $file = $request->file('voucher-import');
        $extension = $file->extension();
        if ($file->isValid() &&
            ($extension == 'txt' || $extension == 'csv')) {
            $path = $file->path();
            $reader = ReaderFactory::create(Type::CSV);
            $reader->open($path);
            $response = [];

            foreach ($reader->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $index => $row) {
                    if ($index != 1) {
                        if ( $row[0] != '' && $row[1] != '' ) {
                            if(VoucherCodeModel::where('code', $row[0])->count() > 0){
                                $response['existed'][] = $row[0];
                            } else {
                                $expiration = Carbon::parse($row[1]);
                                $voucher_code = new VoucherCodeModel;
                                $voucher_code->voucher_item_id = $request->voucher_id;
                                $voucher_code->code = $row[0];
                                $voucher_code->expiration = $expiration->toDateTimeString();
                                $voucher_code->save();

                                $response['inserted'][] = $row[0];
                            }
                        }
                    }
                }
            }
            $reader->close();
            return $response;
        }
        return false;
    }

    public function voucher_add_code(Request $request, $voucher_id){
        $validator = Validator::make($request->all(), [
            'code' => 'required',
            'expiration' => 'date|required',
        ]);

        if ($validator->fails()) {
            return redirect('punchcard/voucher-codes/' . $voucher_id)
                ->withErrors($validator)
                ->withInput();
        } else {
            $expiration = Carbon::parse($request->expiration);

            if($request->has('voucher_code_id')){
                $voucher_code = \Rush\Modules\Merchant\Models\VoucherCodeModel::find($request->voucher_code_id);
            } else {
                $voucher_code = new \Rush\Modules\Merchant\Models\VoucherCodeModel;
            }

            if(VoucherCodeModel::where('code', $request->code)->count() > 0){
                return redirect('punchcard/voucher-codes/' . $voucher_id)
                    ->with('data', ['insert_response' => ['existed' => [$request->code]]]);
            }

            $voucher_code->voucher_item_id = $voucher_id;
            $voucher_code->code = $request->code;
            $voucher_code->expiration = $expiration->toDateTimeString();
            $voucher_code->save();

            return redirect('punchcard/voucher-codes/' . $voucher_id)
                ->with('message', 'Voucher code has been added!');
        }
    }

    public function voucher_delete_code(Request $request, $voucher_id){
        $voucher_code = \Rush\Modules\Merchant\Models\VoucherCodeModel::find($request->voucher_code_id);
        $voucher_code->delete();

        return redirect('punchcard/voucher-codes/' . $voucher_id)
            ->with('message', 'Voucher code has been deleted!');
    }

    public function voucher_codes( Request $request )
    {
        $merchant_id = $request->session()->get( 'user_id' );
        $package_id = $request->session()->get( 'package_id' );
        $item_id = $request->id;
        $data['voucher_data'] = $this->merchantService->getVoucherItemData( $item_id );
        $data['voucher_codes'] = $this->merchantService->getVoucherCodes( $item_id );
        $data['merchant'] = $merchant = $this->merchantService->getById( $merchant_id );
        $data['branches'] = $merchant->branches()->where('is_deleted',0)->get()->toArray();
        $data['vouchers'] = $this->merchantService->getVouchers( $data['branches'] );
        $data['menu'] = ( $package_id == 1 ) ? 'menu.loyalty' : 'menu.punchcard' ;
        $data['login_name'] = $request->session()->get('login_name');
        $data['module'] = 'promos';
        $data['header_text'] = 'Voucher Codes Management';
        return view( 'punchcard.voucher_codes', $data );
    }

    public function voucher_transfer( Request $request ) 
    {
        $validator = Validator::make($request->all(), [
            'codes' => 'required',
            'branch' => 'required',
            'voucher' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $codes = '';
        $codes_str = parse_str($request->input('codes'), $codes);

        $merchant_id = $request->session()->get('user_id');
        $voucher_id = $request->input('voucher');

        foreach($codes['v_c_id'] as $id)
        {
            $this->merchantService->transferVoucherCode($id, $merchant_id, $voucher_id);
        }

        return redirect()->back()->with('message', 'Voucher codes has been transfered!');

    }

    public function get_voucher_branches(Request $request)
    {
        $merchant_id = $request->session()->get('user_id');
        $branches = $this->merchantService->getVoucherBranches($merchant_id);
        return response()->json($branches, 200);
    }

    public function voucher_uploaditems(Request $request)
    {
        $this->submitImportVoucherItemsForm( $request );
        return redirect('punchcard#voucher')->with('message', 'Voucher items have been uploaded!');
    }

    private function submitImportVoucherItemsForm( $request )
    {
        $merchant_id = $request->session()->get( 'user_id' );
        $file = $request->file('voucheritem-import');
        $extension = $file->extension();
        if ($file->isValid() &&
            ($extension == 'txt' || $extension == 'csv')) {
            $path = $file->path();
            $reader = ReaderFactory::create(Type::CSV);
            $reader->open($path);
            foreach ($reader->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $index => $row) {
                    if ($index != 1) {
                        if ( $row[0] != '' && $row[1] != '' && $row[2] != '' )
                        {
                            $voucher = new \Rush\Modules\Merchant\Models\VoucherModel;
                            $voucher->name = $row[0];
                            $voucher->description = $row[1];
                            $voucher->branch_id = $this->merchantService->getBranchIdByName( $row[2], $merchant_id );
                            $voucher->threshold = $row[3];
                            $voucher->save();
                        }
                    }
                }
            }
            $reader->close();
            return true;
        }
        return false;
    }

    public function storeProduct(Request $request)
    {
        $input = $this->getInput($request);
        $input['merchant_id'] = $request->session()->get('user_id');
        $input['points'] = 0;
        $branch_count = count($this->branchService->getListByMerchant($input['merchant_id']));

        if(count($input['branch_ids']) >= $branch_count){
            $input['branch_ids'] = "[0]";
        } else {
            $input['branch_ids'] = '[' . implode(',', $input['branch_ids']) . ']';
        }

        $this->productService->store($input);

        return $this->redirect('punchcard#mechanics-per-product');
    }

    public function updateProduct(Request $request)
    {
        $input = $this->getInput($request);
        $input['merchant_id'] = $request->session()->get('user_id');
        $input['points'] = 0;
        $branch_count = count($this->branchService->getListByMerchant($input['merchant_id']));

        if(count($input['branch_ids']) >= $branch_count){
            $input['branch_ids'] = "[0]";
        } else {
            $input['branch_ids'] = '[' . implode(',', $input['branch_ids']) . ']';
        }

        $this->productService->update($input['product_id'], $input);

        return $this->redirect('punchcard#mechanics-per-product');
    }

    public function deleteProduct($product_id){
        $this->productService->delete($product_id);

        return $this->redirect('punchcard#mechanics-per-product');
    }

    public function getProductStamps(Request $request){
        $product = $this->productService->getById($request->product_id);

        return response()->json([ 'data' => $product->stamps]);
    }

}
