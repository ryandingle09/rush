<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Rush\Modules\Merchant\Services\MerchantService;
use Rush\Modules\Billing\Services\BillingService;
use Rush\Modules\Alert\Events\ChangePasswordEvent;
use App\Http\Requests;
use Carbon\Carbon;
use PDF;
use Barryvdh\Snappy;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Event;

class AccountController extends Controller
{
    public function index(Request $request, MerchantService $merchantService, BillingService $billingService)
    {
      $merchant_id = $request->session()->get( 'user_id' );
      $billing = $billingService->getLatestBilling( $merchant_id );
      if ( empty($billing) )
      {
        $billingService->generateBilling( $merchant_id );
      }
      $billing = $billingService->getLatestBilling( $merchant_id );
      $data['merchant'] = $merchant = $merchantService->getById( $merchant_id );
      $data['billing'] = $billing;
      $data['payments'] = $billingService->getPayments( $merchant_id );
      $data['billing_history'] = $billingService->getBillingHistory( $merchant_id );
      $data['menu'] = ( $merchant->packageId == 1 ) ? 'menu.loyalty' : 'menu.punchcard' ;
      $data['login_name'] = $request->session()->get('login_name');
      $data['quickstart'] = 'account';
      $data['module'] = 'index';
      $data['header_text'] = 'Billing';
      return view( 'account.index', $data );
    }

    public function billing(Request $request, MerchantService $merchantService, BillingService $billingService )
    {
      $billing_id = $request->id;
      $merchant_id = $request->session()->get('user_id');
      $data['merchant'] = $merchant = $merchantService->getById($merchant_id);
      $data['billing'] = $billing = $billingService->getById($billing_id);
      if ( empty($billing) ) return redirect()->action('AccountController@index');
      $data['previous_bill'] = $billingService->getPreviousBilling( $merchant_id, $billing_id );
      $addons_id = explode('|', $merchant->PackageAddons);
      foreach ( $addons_id as $id )
      {
        if ( $id == 1 ) $data['additional_sms_credits'] = $billingService->additional_sms_credits( $merchant, $billing->billing_period_start, $billing->billing_period_end );
        if ( $id == 2 ) $data['app_zero_rating'] = $billingService->app_zero_rating( $merchant, $billing->billing_period_start, $billing->billing_period_end );
        if ( $id == 3 ) $data['white_labeled_card'] = $billingService->white_labeled_card( $merchant, $billing->billing_period_start, $billing->billing_period_end );
      }
      $data['menu'] = ( $merchant->packageId == 1 ) ? 'menu.loyalty' : 'menu.punchcard' ;
      $data['login_name'] = $request->session()->get('login_name');
      $data['quickstart'] = 'account';
      $data['module'] = 'billing';
      $data['header_text'] = 'Billing';
      if ( $billing->merchant_id == $merchant_id ) {
        return view( 'account.bill', $data );
      } else {
        return view( 'account.access-fail', $data);
      }

    }

    public function statement(Request $request, MerchantService $merchantService, BillingService $billingService)
    {

      $billing_id = $request->id;
      $merchant_id = $request->session()->get('user_id');
      $data['merchant'] = $merchant = $merchantService->getById($merchant_id);
      $data['billing'] = $billing = $billingService->getById($billing_id);
      $data['previous_bill'] = $billingService->getPreviousBilling( $merchant_id, $billing_id );
      $addons_id = explode('|', $merchant->PackageAddons);
      foreach ( $addons_id as $id )
      {
        if ( $id == 1 ) $data['additional_sms_credits'] = $billingService->additional_sms_credits( $merchant, $billing->billing_period_start, $billing->billing_period_end );
        if ( $id == 2 ) $data['app_zero_rating'] = $billingService->app_zero_rating( $merchant, $billing->billing_period_start, $billing->billing_period_end );
        if ( $id == 3 ) $data['white_labeled_card'] = $billingService->white_labeled_card( $merchant, $billing->billing_period_start, $billing->billing_period_end );
      }
      $data['menu'] = ( $merchant->packageId == 1 ) ? 'menu.loyalty' : 'menu.punchcard' ;
      $data['login_name'] = $request->session()->get('login_name');
      $data['quickstart'] = 'account';
      $data['module'] = 'statement';
      $data['header_text'] = 'Billing';
      // return view('account.statement', $data );
      if ( $billing->merchant_id == $merchant_id ) {
        $pdf = PDF::loadView('account.statement', $data );
        return $pdf->download($merchant_id . '-soa-'. time() . '.pdf');
      } else {
        $menu = ( $merchant->packageId == 1 ) ? 'menu.loyalty' : 'menu.punchcard' ;
        return view( 'account.access-fail', $data );
      }
    }

    public function payment(Request $request, MerchantService $merchantService, BillingService $billingService)
    {
      $billing_id = $request->id;
      $merchant_id = $request->session()->get('user_id');
      $data['merchant'] = $merchant = $merchantService->getById($merchant_id);
      $data['billing'] = $billing = $billingService->getById($billing_id);
      if ( $billing->is_paid == true ) return redirect()->action('AccountController@index');
      $data['menu'] = ( $merchant->packageId == 1 ) ? 'menu.loyalty' : 'menu.punchcard' ;
      $data['login_name'] = $request->session()->get('login_name');
      $data['quickstart'] = 'account';
      $data['module'] = 'payment';
      $data['header_text'] = 'Billing';
      $data['payment_url'] = getenv('PAYNAMICS_URL');
      if ( $billing->merchant_id == $merchant_id ) {
        return view( 'account.payment', $data );
      } else {
        return view( 'account.access-fail', $data );
      }
    }

    public function process_payment(Request $request, MerchantService $merchantService, BillingService $billingService)
    {
        $merchant_id = $request->session()->get('user_id');
        $input = $request->all();
        $billing_id = $input['billing_id'];
        if ( $input['paytMethod'] == 'credit_card' )
        {
          $this->validate($request, [
              'paytMethod' => 'required',
              'amount' => 'required',
          ]);
          $merchant = $merchantService->getById($merchant_id);
          $billing = $billingService->getById($billing_id);
          $paynamics_hash = $this->processPaynamics($merchant, $billing, $input );
          return response()->json([
            'paymentrequest' => $paynamics_hash
          ]);
        }
        else
        {
          $this->validate($request, [
              'paytMethod' => 'required',
              'payment_date' => 'required',
              'amount' => 'required',
              'refNo' => 'required',
              'upload' => 'required',
          ]);

          if ( $request->hasFile('upload') && $request->file('upload')->isValid() ) {
            $file = $request->file('upload');
            $orig_file = $file->getClientOriginalName();
            $filename = pathinfo($orig_file, PATHINFO_FILENAME);
            $extension = pathinfo($orig_file, PATHINFO_EXTENSION);
            $new_filename = $merchant_id . '-' . Carbon::now()->timestamp . '-' . sprintf('%02d', rand(1,99)) . '.' . $extension;
            $path = $request->file('upload')->move( public_path() . '/', $new_filename );
            $old_path =  public_path() . '/' . $new_filename;
            $new_path =  base_path().'/../repository/merchant/slip/';
            if ( !file_exists( base_path().'/../repository/merchant/slip/') ) {
              mkdir( base_path().'/../repository/merchant/slip/', 0777, true);
            }
            copy($old_path, $new_path . $new_filename);
            unlink($old_path);
            $input['official_receipt'] = $new_filename;
          }
          $billing = $billingService->payBilling($billing_id, $input);
          return redirect()->action('AccountController@index');
        }
    }

    protected function processPaynamics( $merchant, $billing, $input )
    {
      $app_url = getenv('RET_URL');

      $_mid = getenv('PAYNAMICS_MID');
      $_requestid = $this->paynamics_env( $input['billing_id'] );
      $_ipaddress = "127.0.0.1";
      $_noturl = $app_url ."/app/paynamics/payment-listener";
      $_resurl = $app_url ."/app/payment-return";
      $_cancelurl = $app_url ."/app/payment-cancel";
      $_fname = $merchant->contactPerson;
      $_mname = "";
      $_lname = $merchant->contactPerson;
      $_addr1 = "Address1";
      $_addr2 = "Address2";
      $_city = $merchant->location;
      $_state = "MM";
      $_country = "PH";
      $_zip = "1100";
      $_sec3d = "try3d";
      $_email = $merchant->contactPersonEmail;
      $_phone = $merchant->contactPersonNumber;
      $_mobile = "";
      $_clientip = $_SERVER['REMOTE_ADDR'];
      $_amount = number_format( $input['amount'], 2, ".", "");
      $_currency = "PHP";
      $forSign = $_mid . $_requestid . $_ipaddress . $_noturl . $_resurl .  $_fname . $_lname . $_mname . $_addr1 . $_addr2 . $_city . $_state . $_country . $_zip . $_email . $_phone . $_clientip . $_amount . $_currency . $_sec3d;
      $cert = getenv('PAYNAMICS_CERT');

      $_sign = hash("sha512", $forSign.$cert);

      $strxml = "";
      $strxml = $strxml . "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
      $strxml = $strxml . "<Request>";
      $strxml = $strxml . "<orders>";
      $strxml = $strxml . "<items>";
      $strxml = $strxml . "<Items><itemname>". $billing->package->name ."</itemname><quantity>1</quantity><amount>".$_amount."</amount></Items>";
      $strxml = $strxml . "</items>";
      $strxml = $strxml . "</orders>";
      $strxml = $strxml . "<mid>" . $_mid . "</mid>";
      $strxml = $strxml . "<request_id>" . $_requestid . "</request_id>";
      $strxml = $strxml . "<ip_address>" . $_ipaddress . "</ip_address>";
      $strxml = $strxml . "<notification_url>" . $_noturl . "</notification_url>";
      $strxml = $strxml . "<response_url>" . $_resurl . "</response_url>";
      $strxml = $strxml . "<cancel_url>" . $_cancelurl . "</cancel_url>";
      $strxml = $strxml . "<mtac_url>mtac-url</mtac_url>";
      $strxml = $strxml . "<descriptor_note>GOCERY</descriptor_note>";
      $strxml = $strxml . "<fname>" . $_fname . "</fname>";
      $strxml = $strxml . "<lname>" . $_lname . "</lname>";
      $strxml = $strxml . "<mname>" . $_mname . "</mname>";
      $strxml = $strxml . "<address1>" . $_addr1 . "</address1>";
      $strxml = $strxml . "<address2>" . $_addr2 . "</address2>";
      $strxml = $strxml . "<city>" . $_city . "</city>";
      $strxml = $strxml . "<state>" . $_state . "</state>";
      $strxml = $strxml . "<country>" . $_country . "</country>";
      $strxml = $strxml . "<zip>" . $_zip . "</zip>";
      $strxml = $strxml . "<secure3d>" . $_sec3d . "</secure3d>";
      $strxml = $strxml . "<trxtype>sale</trxtype>";
      $strxml = $strxml . "<email>" . $_email . "</email>";
      $strxml = $strxml . "<phone>" . $_phone . "</phone>";
      $strxml = $strxml . "<mobile>" . $_mobile . "</mobile>";
      $strxml = $strxml . "<client_ip>" . $_clientip . "</client_ip>";
      $strxml = $strxml . "<amount>" . $_amount . "</amount>";
      $strxml = $strxml . "<currency>" . $_currency . "</currency>";
      $strxml = $strxml . "<mlogo_url></mlogo_url>";
      $strxml = $strxml . "<pmethod></pmethod>";
      $strxml = $strxml . "<signature>" . $_sign . "</signature>";
      $strxml = $strxml . "</Request>";
      $paymentrequest = base64_encode($strxml);
      return $paymentrequest;
    }

    protected function paynamics_env( $id, $method = "encode" )
    {
      $paynamics_env = getenv('PAYNAMICS_ENV');
      $request_prefix = "aaa-";
      if ( $paynamics_env == "staging" ) {
        $request_prefix = "staging-";
      }

      if ( $method == "decode" ) {
        return str_replace("staging-","", $id);
      } else {
        return $request_prefix.$id;
      }
    }

    public function payment_listener(Request $request, BillingService $billingService)
    {
      try{
        $input = $request->all();
        $body = $input["paymentresponse"];
        $body = str_replace(" ", "+", $body);
        $Decodebody = base64_decode($body);
        $ServiceResponseWPF = new \SimpleXMLElement($Decodebody);
        $application = $ServiceResponseWPF->application;
        $responseStatus = $ServiceResponseWPF->responseStatus;

        $response_code = $responseStatus->response_code;

        $request_id = $application->request_id;
        $request_id = trim($request_id);
        $request_id = $this->paynamics_env($request_id, "decode");

        $response_id = $application->response_id;
        $response_id = trim($response_id);

        $rebill_id = $application->rebill_id;
        $amount = trim($rebill_id);

        if ( trim($response_code) == 'GR001' || trim($response_code) == 'GR002')
        {
          $billing = $billingService->getById($request_id);
          $input = [
                  'paytMethod' => 'credit_card',
                  'official_receipt' => null,
                  'refNo' => $response_id,
                  'amount' => $rebill_id,
                  'payment_date' => date('m-d-Y')
                    ];
          $billingService->payBilling($request_id, $input);
        }
      }
      catch(Exception $ex)
      {
        echo $ex->getMessage();
      }
    }

    public function help(Request $request)
    {
      $data['menu'] = ( $request->session()->get('package_id') == 1 ) ? 'menu.loyalty' : 'menu.punchcard' ;
      $data['login_name'] = $request->session()->get('login_name');
      $data['module'] = 'help';
      $data['helpPdf'] = ($request->session()->get('package_id') == 1) ? 'page/assets/pdf/RUSH_Merchant_User_Manual-POINTS_100317.pdf' : 'page/assets/pdf/RUSH_Merchant_User_Manual-PUNCHCARD_092617.pdf';
      $data['header_text'] = 'Help';
      return view( 'account.help', $data );
    }

    public function changepass(Request $request)
    {
      $data['menu'] = ( $request->session()->get('package_id') == 1 ) ? 'menu.loyalty' : 'menu.punchcard' ;
      $data['login_name'] = $request->session()->get('login_name');
      $data['module'] = 'account';
      $data['message'] = $request->session()->get('message') ? $request->session()->get('message') : null;
      $data['header_text'] = 'Change Password';
      return view( 'account.changepass', $data );
    }

    public function process_changepass(Request $request, MerchantService $merchantService)
    {
      $user_id = $request->session()->get('user_id');
      $package_id = $request->session()->get('package_id');
      $user_type = $request->session()->get('user_type');
      if ( $user_type != 1 ) {
        $subuser_id = $request->session()->get('subuser_id');
      }
      $merchant = $merchantService->getById($user_id);

      $old_password = $request->old_password;
      $new_password = $request->new_password;
      $confirm_password = $request->confirm_password;

      $new_password = trim($new_password);
      $old_password = trim($old_password);
      $confirm_password = trim($confirm_password);

      if ( $user_type != 1 ) {
        $sub_user = $merchantService->getSubUser( $subuser_id );
        $password = $sub_user->password;
      } else {
        $password = $merchant->password;             
      }

      if ($password != null && $password == hash('sha512',$old_password)) {
          if ($new_password == $confirm_password)  {
            $new_password_length = strlen($new_password);
            if ($new_password_length > 6) {
                
                if ( $user_type != 1 ) {
                    $process = $merchantService->updateMerchantUserPassword( $subuser_id, hash('sha512',$new_password) );
                } else {
                    $process = $merchantService->updateMerchantPassword($user_id, hash('sha512',$new_password) );
                }
                if ( $process ) 
                  {
                    $request->session()->flash('message','Password has been changed successfully!');
                  // trigger email alert changePassword
                  // $laravelEventEndpoint = MigrationHelper::getAppBaseUrl().'alert/changePassword';
                  // MigrationHelper::postCURL($laravelEventEndpoint, ['merchantId' => $this->cpanel->getId(), 'newPassword' => $new_password]);
                  Event::fire(new ChangePasswordEvent( $merchant, $new_password ) );
                  }
              } else {
                $request->session()->flash('message','Supplied password is too short! Minimum password length is 6.');
              }
          } else {
            $request->session()->flash('message', 'Error changing password! new password and confirm password does not match!');
          }
      } else {
        $request->session()->flash('message', 'Error changing password! old password does not match current password');
      }

      return redirect( url('changepass') );
    }
}
