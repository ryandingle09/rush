<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Carbon\Carbon;
use Event;
use File;
use Illuminate\Http\Request;
use Intervention\Image\ImageManager;
use Rush\Modules\Alert\Events\LoyaltyCardOrderEvent;
use Rush\Modules\Helpers\Facades\StorageHelper;
use Rush\Modules\Merchant\Services\MerchantService;

class AddonsController extends Controller
{
    public function index(Request $request, MerchantService $merchantService)
    {
        $login_id = $request->session()->get('user_id');
        $data['merchant'] = $merchant = $merchantService->getById($login_id);
        $data['pending_orders'] = $merchant->LoyaltyCardOrder()->where('status', 'Pending')->whereDay('created_at', '=', date('d'))->count();
        $data['loyalty_card_order'] = $merchant->LoyaltyCardOrder()->orderBy('id', 'desc')->get();
        $data['card_design']['logoURL'] = ( $merchant->loyaltyCardDesign ) ? $merchant->loyaltyCardDesign['logoURL'] : false;
        $data['card_design']['backgroundURL'] = ( $merchant->loyaltyCardDesign ) ? $merchant->loyaltyCardDesign['backgroundURL'] : false;
        $data['card_design']['backgroundColorHEX'] = ( $merchant->loyaltyCardDesign ) ? $merchant->loyaltyCardDesign['backgroundColorHEX'] : false;
        $data['menu'] = ( $request->session()->get('package_id') == 1 ) ? 'menu.loyalty' : 'menu.punchcard' ;
        $data['login_name'] = $request->session()->get('login_name');
        $data['quickstart'] = 'addons';
        $data['module'] = 'addons';
        $data['success_prompt'] = ( $request->session()->get('success_prompt') ) ? $request->session()->get('success_prompt') : false;
        $data['header_text'] = 'Add-ons';
        return view('addons.index', $data);
    }

    public function saveCardDesign(Request $request, MerchantService $merchantService)
    {
        $user_id = $request->session()->get('user_id');
        $logo = $request->file('logo', false);
        $background = $request->file('background', false);
        $cardbgcolor = $request->input('cardbgcolor', null);
        $disk = StorageHelper::getRepositoryDisk();

        if ($logo && $this->check_image_supported($logo->getMimeType())) {
            $ext = $logo->getClientOriginalExtension();
            $logo_filename =  $user_id . '_' . Carbon::now()->timestamp . '_' . sprintf('%02d', rand(1, 99)) . '.' . $ext;
            $logo_path = 'merchant/logo/';
            $save_path = $logo_path . $logo_filename;

            $manager = new ImageManager();
            $image = $manager->make($logo)->resize(200, 200)->stream();
            $disk->put($save_path, $image->__toString(), 'public');
        }

        if ($background && $this->check_image_supported($background->getMimeType())) {
            $ext = $background->getClientOriginalExtension();
            $background_filename =  $user_id . '_' . Carbon::now()->timestamp . '_' . sprintf('%02d', rand(1, 99)) . '.' . $ext;
            $background_path = 'merchant/background/';
            $save_path = $background_path . $background_filename;

            $manager = new ImageManager();
            $image = $manager->make($background)->resize(1136, 640)->stream();
            $disk->put($save_path, $image->__toString(), 'public');
        }

        $data = array();
        if ($logo) {
            $data['logoURL'] = StorageHelper::repositoryUrl($logo_path . $logo_filename);
        }
        if ($background) {
            $data['backgroundURL'] = StorageHelper::repositoryUrl($background_path . $background_filename);
        }
        $data['backgroundColorHEX'] = $cardbgcolor;

        $oldMerchantPackageRecord = $merchantService->getMerchantPackage($user_id, 2);
        $result = $merchantService->updateMerchantPackage($user_id, $data, 2);
        if ($result) {
            if ($logo && $oldLogoURL = $oldMerchantPackageRecord['logoURL']) {
                StorageHelper::deleteFromRepository($oldLogoURL);
            }
            if ($background && $oldBackgroundURL = $oldMerchantPackageRecord['backgroundURL']) {
                StorageHelper::deleteFromRepository($oldBackgroundURL);
            }
        }
        return redirect('/addons');
    }

    protected function check_image_supported($mime_type)
    {
        if (in_array($mime_type, array('image/jpeg','image/png'))) {
            return true;
        }
        return false;
    }

    public function orderCard(Request $request, MerchantService $merchantService)
    {
        $login_id = $request->session()->get('user_id');
        $order = $merchantService->generateLoyaltyCardOrder($login_id, $request);
        if ($order) {
            $merchant = $merchantService->getById($login_id);
            Event::fire(new LoyaltyCardOrderEvent($merchant, $request->input('quantity')));
            $request->session()->flash('success_prompt', 'Your order has been submitted!');
        }
        return redirect('/addons#order-card');
    }
}
