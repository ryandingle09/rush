<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Rush\Modules\Merchant\Services\MerchantService;
use Rush\Modules\Merchant\Services\MerchantSettingService;

class SettingsController extends AbstractCmsController
{
    protected $inputMap = [
        'program_name' => 'programName',
        'points_name' => 'pointsName',
        'app_name' => 'appName',
        'app_description' => 'appDescription'
    ];

    /**
     * @var Rush\Modules\Merchant\Services\MerchantService
     */
    protected $merchantService;

    /**
     * @var Rush\Modules\Merchant\Services\MerchantSettingService
     */
    protected $merchantSettingService;

    public function __construct(
        MerchantService $merchantService,
        MerchantSettingService $merchantSettingService
    ) {
        $this->merchantService = $merchantService;
        $this->merchantSettingService = $merchantSettingService;
    }

    public function index(Request $request)
    {
        $merchantId = $request->session()->get('user_id');
        $merchant = $this->merchantService->getById($merchantId);
        $settings = $merchant->settings;

        if ($merchant->isPro()) {
            $stampPointsName = 'Points';
        } else {
            $stampPointsName = 'Stamp';
        }

        return view(
            'settings.index',
            [   'login_name' => $request->session()->get('login_name'),
                'module' => 'settings',
                'programName' => $settings['program_name'],
                'pointsName' => $settings['points_name'],
                'appName' => $settings['app_name'],
                'appDescription' => $settings['app_description'],
                'logo' => $settings['app_logo'],
                'stampPointsName' => $stampPointsName,
                'quickstart' => 'loyalty',
                'points_transfer' => $settings['points_transfer'],
                'earn_transaction_limit' => $settings['earn_transaction_limit'],
                'earn_points_limit' => $settings['earn_points_limit'],
                'redemption_transaction_limit' => $settings['redemption_transaction_limit'],
                'paypoint_transaction_limit' => $settings['paypoint_transaction_limit'],
                'paypoint_points_limit' => $settings['paypoint_points_limit'],
                'menu' => ($merchant->isPro()) ? 'menu.loyalty' : 'menu.punchcard',
                'header_text' => 'Settings'
            ]
        );
    }

    public function store(Request $request)
    {
        $input = $this->getInput($request);
        $input['merchant_id'] = $request->session()->get('user_id');

        $this->process(
            $this->merchantSettingService->store(['input' => $input]),
            'Update',
            'program and app settings'
        );

        return $this->redirect('settings#program');
    }

    public function storeCapping(Request $request)
    {
        $input = $this->getInput($request);
        $input['merchant_id'] = $request->session()->get('user_id');

        $this->process(
            $this->merchantSettingService->storeCapping(['input' => $input]),
            'Update',
            'transaction capping settings'
        );

        return $this->redirect('settings#capping');
    }
}
