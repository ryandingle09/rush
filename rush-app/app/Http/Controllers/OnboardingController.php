<?php

namespace App\Http\Controllers;

use Event;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Rush\Modules\Alert\Events\RequestDemoEvent;
use Rush\Modules\Alert\Events\ForgotPasswordEvent;
use Rush\Modules\Alert\Events\MerchantSignupEvent;
use Rush\Modules\Alert\Events\MerchantWelcomeEvent;
use Rush\Modules\Admin\Services\AdminService;
use Rush\Modules\Merchant\Services\MerchantService;
use Rush\Modules\OnboardingCms\Services\OnboardingCmsService;
use Rush\Citolaravel\Helpers\MigrationHelper;

class OnboardingController extends Controller
{

    protected $adminService;
    protected $merchantService;
    protected $onboardingCmsService;

    public function __construct(AdminService $adminService, MerchantService $merchantService, OnboardingCmsService $onboardingCmsService)
    {
        $this->adminService = $adminService;
        $this->merchantService = $merchantService;
        $this->onboardingCmsService = $onboardingCmsService;
    }

    public function index(Request $request)
    {
        $data['clientLogos'] = $this->onboardingCmsService->getAllOnboardingClientLogos();
        return view('onboarding.index', $data);
    }

    public function about(Request $request)
    {
        return view('onboarding.about');
    }

    public function auntieannes(Request $request)
    {
        return view('onboarding.auntieannes');
    }

    public function theathletesfoot(Request $request)
    {
        return view('onboarding.theathletesfoot');
    }

    public function unilever(Request $request)
    {
        return view('onboarding.unilever');
    }

    public function goldsgym(Request $request)
    {
        return view('onboarding.goldsgym');
    }

    public function mathemagis(Request $request)
    {
        return view('onboarding.mathemagis');
    }

    public function soulscape(Request $request)
    {
        return view('onboarding.soulscape');
    }

    public function yogaplus(Request $request)
    {
        return view('onboarding.yogaplus');
    }

    public function how_it_works(Request $request)
    {
        return view('onboarding.how_it_works');
    }

    public function products(Request $request)
    {
        return view('onboarding.products');
    }
    
    public function request_demo(Request $request)
    {
        $data['form'] = 'show';
        $data['success'] = false;
        if ( $request->isMethod('post') ) {
            
            $rules = array(
              //'fname' => 'required',
              //'lname' => 'required',
              'mobile' => 'required',
              'email' => 'required|email',
              'companyname' => 'required',
              //'industry' => 'required',
              'g-recaptcha-response' => 'required|recaptcha_check'
            );

            $error_messages = [ 'recaptcha_check' => 'The Captcha field is invalid.' ];

            //$input['fname'] = $request->fname;
            //$input['lname'] = $request->lname;
           // $input['name'] = $request->fname . ' ' . $request->lname;
            $input['companyname'] = $request->company;
           // $input['industry'] = $request->industry;
            $input['mobile'] = isset($request->mobile) ? '+63' . $request->mobile : '';
            $input['email'] = $request->email;
            //$input['objective'] = $request->program;
            //$input['request_message'] = $request->details;
            //$input['package'] = isset($request->product) ? implode(', ', $request->product) : "";
            $input['g-recaptcha-response'] = $request['g-recaptcha-response'];

            $input['url'] = 'RUSH DEMO';
            $input['ip_address'] = $request->ip();
            
            Validator::extend('recaptcha_check', function ($attribute, $value, $parameters, $validator) {
                return $this->recaptcha_check($value);
            });

            $validator = Validator::make( $input, $rules, $error_messages );

            if ( $validator->fails() )
            {
                $data['errors'] = $validator->messages();
                $request->flash();
            } else {
                Event::fire(new RequestDemoEvent( $input ) );
                $data['success'] = true;
                return redirect('demo-success');
            }
            
            
        }

        return view('onboarding.request_demo', $data);
    }

     public function requestBasic(Request $request)
    {
        $data['form'] = 'show';
        $data['success'] = false;
        if ( $request->isMethod('post') ) {
            
            $rules = array(
              'fname' => 'required',
              'lname' => 'required',
              'mobile' => 'required',
              'email' => 'required|email',
              'companyname' => 'required',
              'industry' => 'required',
              'g-recaptcha-response' => 'required|recaptcha_check'
            );

            $error_messages = [ 'recaptcha_check' => 'The Captcha field is invalid.' ];

            $input['fname'] = $request->fname;
            $input['lname'] = $request->lname;
            $input['name'] = $request->fname . ' ' . $request->lname;
            $input['companyname'] = $request->company;
            $input['industry'] = $request->industry;
            $input['mobile'] = isset($request->mobile) ? '+63' . $request->mobile : '';
            $input['email'] = $request->email;
            $input['objective'] = $request->program;
            $input['request_message'] = $request->details;
            $input['package'] = isset($request->product) ? implode(', ', $request->product) : "";
            $input['g-recaptcha-response'] = $request['g-recaptcha-response'];
            $input['admin_email_subject'] = '[Punch Card] Request A Demo';
            $input['user_email_subject'] = 'Thank you for your interest in Punch Card!';
            $input['user_email_content'] = 'Thank you for your interest in Punch Card package!';

            $input['url'] = 'Punch Card';
            $input['ip_address'] = $request->ip();
            
            Validator::extend('recaptcha_check', function ($attribute, $value, $parameters, $validator) {
                return $this->recaptcha_check($value);
            });

            $validator = Validator::make( $input, $rules, $error_messages );

            if ( $validator->fails() )
            {
                $data['errors'] = $validator->messages();
                $request->flash();
            } else {
                Event::fire(new RequestDemoEvent( $input ) );
                $data['success'] = true;
            }
        }
        return view('onboarding.request_demo_basic', $data);
    }

     public function requestPro(Request $request)
    {
        $data['form'] = 'show';
        $data['success'] = false;
        if ( $request->isMethod('post') ) {
            
            $rules = array(
              'fname' => 'required',
              'lname' => 'required',
              'mobile' => 'required',
              'email' => 'required|email',
              'companyname' => 'required',
              'industry' => 'required',
              // 'g-recaptcha-response' => 'required|recaptcha_check'
            );

            $error_messages = [ 'recaptcha_check' => 'The Captcha field is invalid.' ];

            $input['fname'] = $request->fname;
            $input['lname'] = $request->lname;
            $input['name'] = $request->fname . ' ' . $request->lname;
            $input['companyname'] = $request->company;
            $input['industry'] = $request->industry;
            $input['mobile'] = isset($request->mobile) ? '+63' . $request->mobile : '';
            $input['email'] = $request->email;
            $input['objective'] = $request->program;
            $input['request_message'] = $request->details;
            $input['package'] = isset($request->product) ? implode(', ', $request->product) : "";
            $input['g-recaptcha-response'] = $request['g-recaptcha-response'];
            $input['admin_email_subject'] = '[Points] Request A Demo';
            $input['user_email_subject'] = 'Thank you for your interest in Points!';
            $input['user_email_content'] = 'Thank you for your interest in Points package!';

            $input['url'] = 'Points';
            $input['ip_address'] = $request->ip();
            
            Validator::extend('recaptcha_check', function ($attribute, $value, $parameters, $validator) {
                return $this->recaptcha_check($value);
            });

            $validator = Validator::make( $input, $rules, $error_messages );

            if ( $validator->fails() )
            {
                $data['errors'] = $validator->messages();
                $request->flash();
            } else {
                Event::fire(new RequestDemoEvent( $input ) );
                $data['success'] = true;
            }
        }
        return view('onboarding.request_demo_pro', $data);
    }

    public function terms(Request $request)
    {
        return view('onboarding.terms');
    }

    public function privacy(Request $request)
    {
        return view('onboarding.privacy');
    }

    public function faqs(Request $request)
    {
        return view('onboarding.faqs');
    }

    public function recaptcha_check($captcha)
    {
        $verify_captcha = json_decode(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='. getenv('G_RECAPTCHA_SECRET') .'&response='. $captcha));
        if($verify_captcha) {
            if (isset($verify_captcha->success) && $verify_captcha->success) {
                return true;
            }
        }
        return false;
    }

    private function process_login( $request, $prelogin = true )
    {
        $username = trim($request->email); $username_length = strlen($username);
        $password = trim($request->password); $password_length = strlen($password);

        $message = 'Invalid username or password';
        $redirect = null;
        $authenticated = false;

        $user_data = array(
            'user_id' => 0,
            'user_type' => 0,
            'login_name' => '',
            'forceQuickSetup' => 0,
            'package_id' => 0,
            'app_id' => getenv('APP_PREFIX')
        );

        if (($username_length >= 3 && $username_length <= 128) && ($password_length >= 6 && $password_length <= 32)) {
            if (filter_var($username,FILTER_VALIDATE_EMAIL)) {
                
                /* Check if Superadmin */
                $result = $this->adminService->login($username,$password);
                $info = count($result) > 0 ? $result : null;
                if ( $info ) {
                    if ( $info['is_active'] ) {
                        $user_data['admin_id'] = $info->id;
                        $user_data['admin_name'] = $info->firstname . ' ' . $info->lastname;
                        $user_data['admin_type'] = 1;
                        $user_data['is_admin'] = TRUE;
                        $authenticated = true;
                        $redirect = url('app/admin');
                    }
                } else {

                    /* If not Superadmin, check if BD accounts */
                    $result = $this->adminService->bd_login($username,$password);
                    $info = count($result) > 0 ? $result : null;
                    if ( $info ) {
                        $user_data['admin_id'] = $info->id;
                        $user_data['admin_name'] = $info->name;
                        $user_data['admin_type'] = 2;
                        $user_data['is_admin'] = TRUE;
                        $authenticated = true;
                        $redirect = url('app/admin');
                    } else {

                        /* If not BD Account, check if Merchant */
                        $result = $this->merchantService->login($username,$password);
                        $info = count($result) > 0 ? $result : null;
                        if ( $info ) {
                            $expired = false;
                            if ($info->status == 'Trial') {
                                if ( $info->trial_end <= date('Y-m-d') ) {
                                    $message = 'Your trial has expired';
                                    $expired = true;
                                }
                            }

                            if ($info->settings && $info->settings->maintenance == true) {
                                $message = $info->settings->maintenance_text;
                                $expired = true;
                            }
                            if ( $info->enabled == 0 ) {
                                $message = 'Your account is disabled.';
                                $expired = true;
                            }
                            if ($expired == false) {
                                $user_data['user_id'] = $info->merchantid;
                                $user_data['login_name'] = $info->businessName;
                                $user_data['user_type'] = 1;
                                $user_data['forceQuickSetup'] = $info->shouldLaunchQuicksetup;
                                $user_data['package_id'] = $info->packageId;
                                $user_data['is_admin'] = FALSE;
                                $authenticated = true;
                                $redirect = url('app/dashboard');
                          }
                        } else {

                            /* If not Merchant, check if Sub-user */
                            $result = $this->merchantService->subuserlogin($username,$password);
                            $info = count($result) > 0 ? $result : null;
                            if ( $info ) {
                                $expired = false;
                                if ($info->merchant->status == 'Trial') {
                                    if ( $info->merchant->trial_end <= date('Y-m-d') ) {
                                        $message = 'Your trial has expired';
                                        $expired = true;
                                    }
                                }
                                if ($info->merchant->settings && $info->merchant->settings->maintenance == true) {
                                    $message = $info->merchant->settings->maintenance_text;
                                    $expired = true;
                                }
                                if ( $info->merchant->enabled == 0 ) {
                                    $message = 'Your merchant is disabled.';
                                    $expired = true;
                                }
                                if ( $info->is_deleted == 1 ) {
                                    $message = 'Your account is disabled.';
                                    $expired = true;
                                }
                                if ($expired == false) {
                                    $user_data['user_id'] = $info->merchant_id;
                                    $user_data['login_name'] = $info->name;
                                    $user_data['user_type'] = 2;
                                    $user_data['subuser_id'] = $info->id;
                                    $user_data['forceQuickSetup'] = false;
                                    $user_data['package_id'] = $info->package_id;
                                    $user_data['is_admin'] = FALSE;
                                    $authenticated = true;
                                    $redirect = url('app/dashboard');
                                }
                            }
                        }
                    }
                }
            }
        } else {

            /* If not Email, check if BCU */
            $result = $this->merchantService->bculogin($username,$password);
            $info = count($result) > 0 ? $result : null;
            if ( $info ) {
                $expired = false;
                if ( $info->merchant->status == 'Trial' ) {
                    if ( $info->merchant->trial_end <= date('Y-m-d') ) {
                        $message = "Your merchant's trial has expired";
                        $expired = true;
                    }
                }
                if ( $info->merchant->enabled == 0 ) {
                    $message = 'Your merchant is disabled.';
                    $expired = true;
                }
                if ( $info->merchant->bcu_enabled == 0 ) {
                    $message = 'Your merchant is not BCU enabled.';
                    $expired = true;
                }
                if ( $info->merchant->settings && $info->merchant->settings->maintenance == true ) {
                    $message = $info->merchant->settings->maintenance_text;
                    $expired = true;
                }

                if ( $expired == false ) {
                    $user_data['user_id'] = $info->merchantId;
                    $user_data['login_name'] = $info->employeeName;
                    $user_data['user_type'] = 3;
                    $user_data['bcu_id'] = $info->userId;
                    $user_data['forceQuickSetup'] = false;
                    $user_data['package_id'] = $info->merchant->packageId;
                    $user_data['is_admin'] = FALSE;
                    $authenticated = true;
                    $redirect = url('app/dashboard');
                }
            }
        }

        return  [  'authenticated' => $authenticated,
                   'user_data' => $user_data,
                   'message' => $message,
                   'redirect' => $redirect
                ];
    }

    public function validate_login(Request $request)
    {
        $return = $this->process_login( $request, true );
        
        if( $return['authenticated'] ) {
            $response = [
                'login_status' => 'success'
            ];
        } else {
            $response = [
                'login_status' => 'failed',
                'error_message' => $return['message']
            ];
        }
        return response()->json( $response );
    }

    public function login(Request $request)
    {
        $return = $this->process_login( $request );
        if ( $return['authenticated'] ) {
            session( $return['user_data'] );
            return redirect( $return['redirect'] );
        } else {
            return redirect( $return['redirect'] );
        }
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        $user_data['is_admin'] = FALSE;
        session($user_data);
        return redirect( url('../') );
    }

    public function validate_signup(Request $request)
    {
        $input = $request->all();

        $rules = array(
            'person_fname' => 'required',
            'person_lname' => 'required',
            'person_position' => 'required',
            'person_contact' => 'required|min:11',
            'person_email' => 'required|email|unique:Merchant,username',
            'business_location' => 'required',
            'business_name' => 'required|unique:Merchant,businessName',
            'business_type' => 'required',
            'program_objective' => 'required',
            'business_branches' => 'required|integer',
            'customer_no' => 'required|integer',
            'g-recaptcha-response' => 'required|recaptcha_check'
        );

        $error_messages = [ 'recaptcha_check' => 'The Captcha field is invalid.' ];

        Validator::extend('recaptcha_check', function ($attribute, $value, $parameters, $validator) {
            return $this->recaptcha_check($value);
        });

        $validator = Validator::make( $input, $rules, $error_messages );

        if ( $validator->fails() )
        {
            $data['signup_status'] = 'failed';
            $data['errors'] = $validator->messages();
        } else {
            $data['signup_status'] = 'success';
        }

        return response()->json( $data );

    }

    public function select_package(Request $request)
    {
        $data['input'] = $input = $request->all();

        if ( $request->isMethod('post') ) {

            $rules = array(
                'person_fname' => 'required',
                'person_lname' => 'required',
                'person_position' => 'required',
                'person_contact' => 'required',
                'person_email' => 'required|email|unique:Merchant,username',
                'business_location' => 'required',
                'business_name' => 'required|unique:Merchant,businessName',
                'business_type' => 'required',
                'program_objective' => 'required',
                'business_branches' => 'required|integer',
                'customer_no' => 'required|integer',
            );

            $validator = Validator::make( $input, $rules );
            if ( $validator->fails() )
            {
                $request->session()->flash('errors', $validator->messages() );
                return redirect( url('/') );
            } else {
                return view('onboarding.select-package', $data);
            }
        }

        
    }

    public function register(Request $request)
    {

        $removed = array(".", "'", "!", "(", ")", "@", "+", '"', ",", "/", "?");
        $business_system_link = str_replace($removed, "", trim( $request->business_name));
        $business_system_link = str_replace(' ', '-', $business_system_link);
        $business_system_link = strtolower($business_system_link); 
        
        if ($request->package == 'Points') {
            $package_id = 1;
            $status = 'Trial';
        } else if ($request->package == 'Punch Card') {
            $package_id = 2;
            $status = 'Trial';
        }

        $packageAddons = '';
        if( $request->addon ) {
            $i = 0;
            foreach( $request->addon as $item ) {
                $packageAddons .= '|'. $item;
            }
        }

        $password = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 8);

        $data = [
            'packageId' => $package_id,
            'business_system_link' => $business_system_link,
            'businessName' => addslashes( $request->business_name ),
            'businessType' => $request->business_type,
            'businessProgramObjective' =>  addslashes( $request->program_objective ),
            'PackageAddons' => $packageAddons,
            'business_branches' => $request->business_branches,
            'address' =>  addslashes( $request->business_location ),
            'averageCustomer' => $request->customer_no,
            'personName' => $request->person_fname . ' ' . $request->person_lname,
            'personPosition' => $request->person_position,
            'contactPerson' => $request->person_fname . ' ' . $request->person_lname,
            'contactPersonEmail' => $request->person_email,
            'contactPersonNumber' => $request->person_contact,
            'username' => $request->person_email,
            'password' => hash("sha512", $password),
            'timestamp' => date('Y-m-d H:i:s'),
            'trial_end' => date('Y-m-d H:i:s', strtotime("+30 days")),
            'status' => $status,
            'shouldLaunchQuicksetup' => 1
            ];

        $response = $this->merchantService->registerMerchant( $data );

        if ( $response )  {
            Event::fire(new MerchantSignupEvent( $response ) );

            $welcome = [
                'person_name' => $request->person_fname . ' ' . $request->person_lname,
                'business_name' => $request->business_name,
                'business_package' => $request->package,
                'person_email' => $request->person_email,
                'person_password' => $password
                ];
            $welcome = (object) $welcome;
            Event::fire(new MerchantWelcomeEvent( $welcome ) );
           // $request->session()->flash('register_success', 'true' );
        }

        return redirect('signup-success');
    }

    public function validate_forgotpassword(Request $request)
    {
        $email =  $request->email;
        $result = $this->merchantService->checkEmail( $email );
        if ( $result ) {
            $data['status'] = 'success';
            $data['type'] = $result;
        } else {
            $data['status'] = 'failed';
            $data['message'] = 'User information is not existing.';
        }
        return response()->json( $data );
    }

    public function forgotpassword(Request $request)
    {
        $data['email'] =  $request->email;
        $type = $request->type;
        $data['password'] = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 8);
        $forgotpass = $this->merchantService->forgotpassword( $data['email'], $data['password'], $type);
        if ( $forgotpass ) { $data['name'] = $forgotpass; }
        Event::fire(new ForgotPasswordEvent( $data ) );
        $request->session()->flash('forgotpass_success', 'true' );
        return redirect( url('/') );
    }

    public function verifyAccount( Request $request )
    {
        $username = $request->verifyaccount_name;
        $password = $request->verifyaccount_password; 
        $merchant_id = $request->verifyaccount_merchant_id; 
        $current_url = $request->verifyaccount_current_url; 

        $result = $this->merchantService->login($username,$password);
        $info = count($result) > 0 ? $result : null;
        if ( $info ) {
            $redirect_url = $request->verifyaccount_redirect_url; 
            if( $info->merchantid == $merchant_id ) {
                return redirect( $redirect_url );
            }
        }

        $result = $this->merchantService->subuserlogin( $username, $password);
        $info = count($result) > 0 ? $result : null;
        if ( $info ) {
            $redirect_url = $request->verifyaccount_redirect_url; 
            return redirect( $redirect_url );
        }
        
        return redirect( $current_url )->with('message', 'Account verification failed.');;
    }

    public function newsroom(Request $request)
    {
        $news = $this->onboardingCmsService->getAllOnboardingNews();
        $data['big_news'] = $news->first();
        $data['two_news'] = array($news[1],$news[2]);
        $data['more_news'] = $news->slice(3);
        return view('onboarding.newsroom', $data);
    }

    public function newsroom_article(Request $request)
    {
        $slug = $request->url;
        $data['news'] = $this->onboardingCmsService->getNewsbySlug($slug);
        $data['more_news'] = $this->onboardingCmsService->getAllOnboardingNewsExceptSlug($slug);
        return view('onboarding.newsroom_article', $data);   
    }

    public function signup_success(Request $request)
    {
        $slug = $request->url;
        $data['news'] = $this->onboardingCmsService->getNewsbySlug($slug);
        $data['more_news'] = $this->onboardingCmsService->getAllOnboardingNewsExceptSlug($slug);
        return view('onboarding.signup_success', $data);
    }

    public function demo_success(Request $request)
    {
        $slug = $request->url;
        $data['news'] = $this->onboardingCmsService->getNewsbySlug($slug);
        $data['more_news'] = $this->onboardingCmsService->getAllOnboardingNewsExceptSlug($slug);
        return view('onboarding.request_demo_success', $data);
    }

    public function merchant_signup(Request $request)
    {
        $slug = $request->url;
        $data['news'] = $this->onboardingCmsService->getNewsbySlug($slug);
        $data['more_news'] = $this->onboardingCmsService->getAllOnboardingNewsExceptSlug($slug);
        return view('onboarding.merchant_signup', $data);
    }
}
