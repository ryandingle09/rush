<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Event;
use File;
use Illuminate\Http\Request;
use Intervention\Image\ImageManager;
use Rush\Modules\Alert\Events\QuickstartCompletionEvent;
use Rush\Modules\Design\Services\DesignService;
use Rush\Modules\Helpers\Facades\StorageHelper;
use Rush\Modules\Merchant\Services\MerchantService;

class HomeController extends Controller
{
    protected $merchantService;
    protected $designService;

    public function __construct( MerchantService $merchantService, DesignService $designService ) {
        $this->merchantService = $merchantService;
        $this->designService = $designService;
    }

    public function index(Request $request)
    {
        $merchant_id = session('user_id');
        $package_id = session('package_id');
        $user_type = session('user_type');

        if ( $user_type == 1 ) {
            $data['modules'] = $this->merchantService->getMerchantModules( $merchant_id);
            $data['merchant'] = $merchant = $this->merchantService->getById($merchant_id);

            if ( $merchant->requireChangePassword == 1 ) {
                $request->session()->flash('message', 'Please change your password first!');
                return redirect( url('changepass') );
            }

            if ( $merchant->new_billing_calculation ) {
                if ( $merchant->settings && $merchant->settings->allow_registration ) {
                    if ( !session('tier_alert') ) {
                        $data['member_tier'] = $this->checkMemberTier( $merchant );
                        session(['tier_alert' => true]);
                    }
                }

                if ( $request->admin == 'test' ) { $data['member_tier'] = ['alert' => true, 'customer_max_number' => '1']; }
            }

            if( $merchant->isfirstlogin == 1 ) {
                $this->merchantService->createQuicksetupProgressTrack( $merchant_id, $package_id, 3);
                $this->merchantService->clearFirstLoginFlag( $merchant_id );
            }

            if ( session('forceQuickSetup') ) {
                $completion = $this->merchantService->getCompletionState( $merchant_id );
                if ( $completion->completed < 100 )
                {
                    $palettes = array(
                        0 => array(
                            "name" => "Palette 1",
                            "r" => "#222532",
                            "g" => "#009573",
                            "b" => "#8CE994"
                        ),
                        1 => array(
                            "name" => "Palette 2",
                            "r" => "#43062E",
                            "g" => "#922648",
                            "b" => "#F90047"
                        ),
                        2 => array(
                            "name" => "Palette 3",
                            "r" => "#2F2F2C",
                            "g" => "#005B67",
                            "b" => "#00A8A0"
                        ),
                        3 => array(
                            "name" => "Palette 4",
                            "r" => "#FFF7EA",
                            "g" => "#00ECFF",
                            "b" => "#00CBD6"
                        )
                    );

                    isset($data['merchantAppPalette']) ?: $data['merchantAppPalette'] = view('quickstart.templates.merchantAppPaletteTemplate', array('palettes' => $palettes) );
                    isset($data['customerAppPalette']) ?: $data['customerAppPalette'] = view('quickstart.templates.customerAppPaletteTemplate', array('palettes' => $palettes) );

                    $data['quickstart_view'] = $package_id == 1 ? 'quickstart.loyalty' : 'quickstart.punchcard';
                    $settings = $this->merchantService->getSavedProgramSettings( $merchant_id );
                    $data = array_merge( $data, $settings);

                    $settings = $this->merchantService->getSavedApplicationDetails( $merchant_id );
                    $data = array_merge( $data, $settings);

                    $settings['merchant_app'] = $this->merchantService->getSavedMerchantApplication( $merchant_id );
                    $data = array_merge( $data, $settings);

                    $settings['customer_app'] = $this->merchantService->getSavedCustomerAppDesign( $merchant_id );
                    $data = array_merge( $data, $settings);
                }
            }
        } else if ( $user_type == 2 ) {
            $subuser_id = session('subuser_id');
            $data['modules'] = $this->merchantService->getUserModules($subuser_id);
            $data['merchant'] = $this->merchantService->getById($merchant_id);
        } else if ( $user_type == 3) {
            $data['modules'] = $this->merchantService->getModuleByCode(['tra','cls'], $package_id);
            $data['merchant'] = $this->merchantService->getById($merchant_id);
        }

        $data['menu'] = ( session('package_id') == 1 ) ? 'menu.loyalty' : 'menu.punchcard' ;
        $data['quickstart'] = ( session('package_id') == 1 ) ? 'loyalty' : 'punchcard' ;
        $data['login_name'] = session('login_name'); 
        $data['header_text'] = "Dashboard";
        $data['module'] = "dashboard";
        $data['message'] = session('message');
        return view('home.index', $data);
    }

    public function setupProgram(Request $request)
    {
        $programName = $request->programName;
        $pointsName = $request->pointsName;
        $this->merchantService->saveProgramSettings( session('user_id'), $programName, $pointsName );
        $this->merchantService->trackProgress( session('user_id'), 1 );
    }

    public function saveMerchantAppDesign(Request $request)
    {
        $merchant_id = session('user_id');
        $package_id = session('package_id');
        $merchantAppData = array();
        $upload_error = false;
        $logo = $request->file('logo', FALSE);
        $background = $request->file('background', FALSE);
        $stamp = $request->file('stamp', FALSE);
        $disk = StorageHelper::getRepositoryDisk();

        if ($logo) {
            if ($this->check_image_supported($logo->getMimeType())) {
                $ext = $logo->getClientOriginalExtension();
                $logo_filename =  $merchant_id . '_' . Carbon::now()->timestamp . '_' . sprintf('%02d', rand(1,99)) . '.' . $ext;
                $logo_path = 'merchant/logo/';
                $save_path = $logo_path . $logo_filename;

                $manager = new ImageManager();
                $image = $manager->make($logo)->resize(200, 200)->stream();
                $disk->put($save_path, $image->__toString(), 'public');
                $merchantAppData['logoURL'] = StorageHelper::repositoryUrl($save_path);
            } else {
                $upload_error = true;
            }
        }

        if ($background) {
            if ($this->check_image_supported($background->getMimeType())) {
                $ext = $background->getClientOriginalExtension();
                $background_filename =  $merchant_id . '_' . Carbon::now()->timestamp . '_' . sprintf('%02d', rand(1,99)) . '.' . $ext;
                $background_path = 'merchant/background/';
                $save_path = $background_path . $background_filename;

                $manager = new ImageManager();
                $image = $manager->make($background)->resize(1136, 640)->stream();
                $disk->put($save_path, $image->__toString(), 'public');
                $merchantAppData['backgroundURL'] = StorageHelper::repositoryUrl($save_path);
            } else {
                $upload_error = true;
            }
        }

        if ($stamp) {
            if ($this->check_image_supported($stamp->getMimeType())) {
                $ext = $stamp->getClientOriginalExtension();
                $background_filename =  $merchant_id . '_' . Carbon::now()->timestamp . '_' . sprintf('%02d', rand(1,99)) . '.' . $ext;
                $background_path = 'merchant/stamps/';
                $save_path = $background_path . $background_filename;

                $manager = new ImageManager();
                $image = $manager->make($stamp)->resize(1136, 640)->stream();
                $disk->put($save_path, $image->__toString(), 'public');
                $merchantAppData['backgroundURL'] = StorageHelper::repositoryUrl($save_path);
            } else {
                $upload_error = true;
            }
        }

        if ($upload_error == false) {
            $text = $request->textColor;
            $merchantAppData['textColor'] = $text;

            $buttons = $request->buttonsColor;
            $merchantAppData['buttonsColor'] = $buttons;

            $process = $this->designService->storeDesignDetails($merchant_id,$package_id,1,$merchantAppData);
            if ( $process ) {
                $this->merchantService->trackProgress( session('user_id'), 2 );
            }
        }
    }

    public function saveCustomerAppDesign(Request $request)
    {
        $merchant_id = $request->session()->get('user_id');
        $package_id = $request->session()->get('package_id');
        $designData = array();
        $error = false;
        $logo = $request->file('logo', FALSE);
        $background = $request->file('background', FALSE);
        $stamp = $request->file('stamp', FALSE);
        $disk = StorageHelper::getRepositoryDisk();

        if ($logo) {
            if ($this->check_image_supported($logo->getMimeType())) {
                $ext = $logo->getClientOriginalExtension();
                $logo_filename =  $merchant_id . '_' . Carbon::now()->timestamp . '_' . sprintf('%02d', rand(1,99)) . '.' . $ext;
                $logo_path = 'merchant/logo/';
                $save_path = $logo_path . $logo_filename;

                $manager = new ImageManager();
                $image = $manager->make($logo)->resize(200, 200)->stream();
                $disk->put($save_path, $image->__toString(), 'public');
                $designData['logoURL'] = StorageHelper::repositoryUrl($save_path);
            } else {
                $error = true;
            }
        }

        if ($background) {
            if ($this->check_image_supported($background->getMimeType())) {
                $ext = $background->getClientOriginalExtension();
                $background_filename =  $merchant_id . '_' . Carbon::now()->timestamp . '_' . sprintf('%02d', rand(1,99)) . '.' . $ext;
                $background_path = 'merchant/background/';
                $save_path = $background_path . $background_filename;

                $manager = new ImageManager();
                $image = $manager->make($background)->resize(640, 1136)->stream();
                $disk->put($save_path, $image->__toString(), 'public');
                $designData['backgroundURL'] = StorageHelper::repositoryUrl($save_path);
            } else {
                $error = true;
            }
        }

        if ($stamp) {
            if ($this->check_image_supported($stamp->getMimeType())) {
                $ext = $stamp->getClientOriginalExtension();
                $stamp_filename =  $merchant_id . '_' . Carbon::now()->timestamp . '_' . sprintf('%02d', rand(1,99)) . '.' . $ext;
                $stamp_path = 'merchant/stamps/';
                $save_path = $stamp_path . $stamp_filename;

                $manager = new ImageManager();
                $image = $manager->make($stamp)->resize(200, 200)->stream();
                $disk->put($save_path, $image->__toString(), 'public');
                $designData['stampURL'] = StorageHelper::repositoryUrl($save_path);
            } else {
                $error = true;
            }
        }

        if ($error == false) {
            $designData['merchantName'] = $request->merchantName;

            $overlay = $request->overlayColor;
            $designData['overlayColor'] = $overlay;

            $text = $request->textColor;
            $designData['textColor'] = $text;

            $buttons = $request->buttonsColor;
            $designData['buttonsColor'] = $buttons;

            $process = $this->designService->storeDesignDetails( $merchant_id, $package_id, 3, $designData );

            // save the stamp for MerchantDesign
            if (isset($designData['stampURL'])) {
                $this->designService->storeDesignDetails( $merchant_id, $package_id, 1, ['stampURL' => $designData['stampURL']  ]);
            }

            $this->merchantService->trackProgress( session('user_id'), 2 );
        }

    }

    public function saveApplicationDetails(Request $request)
    {
        $upload_error = false;

        $appName = $request->appName;
        $appDescription = $request->appDescription;
        $appLogo = "";
        $logo = $request->file('logo', FALSE);
        $disk = StorageHelper::getRepositoryDisk();

        if ($logo) {
            if ($this->check_image_supported($logo->getMimeType())) {
                $ext = $logo->getClientOriginalExtension();
                $logo_filename = session('user_id') . '_' . Carbon::now()->timestamp . '_' . sprintf('%02d', rand(1,99)) . '.' . $ext;
                $logo_path = 'merchant/logo/';
                $save_path = $logo_path . $logo_filename;

                $manager = new ImageManager();
                $image = $manager->make($logo)->resize(1024, 1024)->stream();
                $disk->put($save_path, $image->__toString(), 'public');
                $appLogo = StorageHelper::repositoryUrl($save_path);
            } else {
                $error = true;
            }
        }

        $this->merchantService->saveApplicationDetails( session('user_id') , $appName, $appDescription, $appLogo);
        $this->merchantService->trackProgress( session('user_id'), 3);
        $this->merchantService->clearQuicksetupLaunchFlag( session('user_id') );

        session(['forceQuickSetup' => 0]);

        $merchant = $this->merchantService->getById(session('user_id'));
        Event::fire(new QuickstartCompletionEvent($merchant));
    }

    private function check_image_supported( $mime_type )
    {
      if ( in_array( $mime_type, array('image/jpeg','image/png')) )
      {
          return true;
      }
      return false;
    }

    private function checkMemberTier( $merchant )
    {
        $data = [ 'alert' => false ];

        $customer_count = $merchant->customers->count();
        if ( $customer_count >= 9900 && $customer_count < 10000 )
        {
            $data = ['alert' => true, 'customer_max_number' => '10,000'];
        }
        else if ( $customer_count >= 24900 && $customer_count < 25000 )
        {
            $data = ['alert' => true, 'customer_max_number' => '25,000'];
        }
        else if ( $customer_count >= 49900 && $customer_count < 50000 )
        {
            $data = ['alert' => true, 'customer_max_number' => '50,000'];
        }
        else if ( $customer_count >= 99900 && $customer_count < 100000 )
        {
            $data = ['alert' => true, 'customer_max_number' => '100,000'];
        }

        return $data;
    }

    public function memberTier( Request $request )
    {
        $merchant_id = session('user_id');
        $user_type = session('user_type');

        if ( $user_type == 1 || !empty($merchant_id) )
        {
            $return = $this->merchantService->memberTierStop( $merchant_id );
            if ( $return )
            {
                $request->session()->flash('message', 'You will now remain with your current subscription! Additional member registrations after the maximum count allowed will not be processed.');
            } else {
                $request->session()->flash('message', 'An error has occured. Please try again or contact the administrators.');
            }
        }
        return redirect( url('dashboard') );
    }
}
