<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\ImageManager;
use Rush\Modules\Broadcasting\Services\SegmentService;
use Rush\Modules\Email\Services\EmailService;
use Rush\Modules\Merchant\Services\BranchService;
use Rush\Modules\Merchant\Services\MerchantService;
use Rush\Modules\Posts\Services\PostsService;
use Rush\Modules\Push\Library\Push;
use Rush\Modules\Push\Services\PushCredentialService;
use Rush\Modules\Push\Services\PushService;
use Rush\Modules\Sms\Library\Sms;
use Rush\Modules\Sms\Services\SmsCredentialService;
use Rush\Modules\Sms\Services\SmsService;
use Session;

class BroadcastingController extends AbstractCmsController
{
    public function __construct(
        MerchantService $merchantService,
        SmsService $smsService,
        PushService $pushService,
        EmailService $emailService,
        SegmentService $segmentService,
        PushCredentialService $pushCredentialService,
        SmsCredentialService $smsCredentialService,
        PostsService $postsService,
        BranchService $branchService
    )
    {
        $this->merchantService = $merchantService;
        $this->smsService = $smsService;
        $this->pushService = $pushService;
        $this->emailService = $emailService;
        $this->segmentService = $segmentService;
        $this->pushCredentialService = $pushCredentialService;
        $this->smsCredentialService = $smsCredentialService;
        $this->postsService = $postsService;
        $this->branchService = $branchService;
    }

    /**
     * Display list of all SMS
     * @param  Request $request
     * @return view
     */
    public function index(Request $request)
    {
        $merchantId = $request->session()->get('user_id');
        $merchant = $this->merchantService->getById($merchantId);

        $enable_post_sub_category = $merchant->settings->enable_post_sub_category;

        $activeSegments = $merchant->activeSegments;
        $merchant_events = $merchant->posts()->get();
        $events_count = count($merchant_events);

        $user_type = $request->session()->get( 'user_type' );

        if($user_type === 3){ //if bcu
            $employee = $merchant->employees()->where('userId', $request->session()->get('bcu_id'))->first();

            $branch = $merchant->branches()->where('id', $employee->branchId)->first();
            $branch_option = collect([ $branch['id'] => $branch['branchName']]);

            $segments = $this->merchantService->getSegmentByScopeString($merchantId, '"branchTransaction":{"id":' . $branch['id'] . ',"name":"' . $branch['branchName'] . '"}');
            $activeSegments = $segments;
            $segment_id = ($segments->first() !== null ) ? $segments->first()->id : -1;

            $smss = $this->smsService->getBySegmentId($segment_id);
            $pushs = $this->pushService->getBySegmentId($segment_id);
            $emails = $this->emailService->getBySegmentId($segment_id);

        } else {
            $branch_option = $this->branchService->getListByMerchant($merchantId);
            $segments = $merchant->segments;

            $smss = $this->smsService->allWithSegment($merchantId);
            $pushs = $this->pushService->allWithSegment($merchantId);
            $emails = $this->emailService->allWithSegment($merchantId);
        }

        return view(
            'broadcasting.index',
            [   'login_name' => $request->session()->get('login_name'),
                'durationStart' => date('Y-m-d'),
                'quickstart' => 'loyalty',
                'module' => 'broadcasting',
                'smss' => $smss,
                'smsSentAll' => $this->smsService->getSmsSentAll($merchantId),
                'smsSentThisMonth' => $this->smsService->getSmsSentThisMonth($merchantId),
                'pushs' => $pushs,
                'emails' => $emails,
                'segments' => $segments,
                'activeSegments' => $activeSegments,
                'merchant' => $merchant,
                'header_text' => 'SMS Broadcast',
                'menu' => ($merchant->isPro()) ? 'menu.loyalty' : 'menu.punchcard',
                'merchant_events' => $merchant_events,
                'event_sequence_options_add' => array_combine(range(1, $events_count+1), range(1, $events_count+1)),
                'event_sequence_options_edit' => array_combine(range(1, $events_count), range(1, $events_count)),
                'event_categories' => $this->postsService->getAllCategory($merchantId),
                'message' => session('message') ? session('message') : null,
                'branchOptions' => $branch_option,
                'user_type' => $user_type,
                'enable_post_sub_category' => $enable_post_sub_category
            ]
        );
    }

    /**
     * Create SMS
     * @param  Request $request
     */
    public function smsStore(Request $request)
    {
        $smsInput = $this->getInput($request);
        $smsInput['merchant_id'] = $request->session()->get('user_id');
        $this->result = $this->smsService->store(['sms' => $smsInput]);
        $this->setFlashMessage('Create', 'sms');

        return $this->redirect('broadcast/sms#sms');
    }

    /**
     * Delete SMS
     * @param  Request $request
     */
    public function smsDelete($id)
    {
        $this->result = $this->smsService->delete(['id' => $id]);
        $this->setFlashMessage('Delete', 'sms');

        return redirect('broadcast/sms#sms');
    }

    /**
     * Create Push
     * @param  Request $request
     */
    public function pushStore(Request $request)
    {
        $pushInput = $this->getInput($request);
        $pushInput['merchant_id'] = $request->session()->get('user_id');
        $this->result = $this->pushService->store(['push' => $pushInput]);
        $this->setFlashMessage('Create', 'push notification');

        return $this->redirect('broadcast/sms#pushnotif');
    }

    /**
     * Delete Push
     * @param  Request $request
     */
    public function pushDelete($id)
    {
        $this->result = $this->pushService->delete(['id' => $id]);
        $this->setFlashMessage('Delete', 'push notification');

        return redirect('broadcast/sms#pushnotif');
    }

    /**
     * Create Email
     * @param  Request $request
     */
    public function emailStore(Request $request)
    {
        $emailInput = $this->getInput($request);
        $emailInput['merchant_id'] = $request->session()->get('user_id');
        $this->result = $this->emailService->store(['email' => $emailInput]);
        $this->setFlashMessage('Create', 'email');

        return $this->redirect('broadcast/sms#emailbroad');
    }

    /**
     * Delete Email
     * @param  Request $request
     */
    public function emailDelete($id)
    {
        $this->result = $this->emailService->delete(['id' => $id]);
        $this->setFlashMessage('Delete', 'email');

        return redirect('broadcast/sms#emailbroad');
    }

    /**
     * Create Segment
     * @param  Request $request
     */
    public function segmentStore(Request $request)
    {
        $segmentInput = $this->getInput($request);
        $segmentInput['merchant_id'] = $request->session()->get('user_id');
        $this->result = $this->segmentService->store(['segment' => $segmentInput]);
        $this->setFlashMessage('Create', 'segment');

        return $this->redirect('broadcast/sms#targetseg');
    }

    /**
     * Delete Segment
     * @param  Request $request
     */
    public function segmentDelete($id)
    {
        $this->result = $this->segmentService->delete(['id' => $id]);
        $this->setFlashMessage('Delete', 'segment');

        return redirect('broadcast/sms#targetseg');
    }

    //MTODO: transfer this in Super Admin
    public function pushCredentialIndex(Request $request)
    {
        $merchantId = $request->session()->get('user_id');
        $merchant = $this->merchantService->getById($merchantId);
        return view(
            'broadcasting.push',
            [   'login_name' => $request->session()->get('login_name'),
                'quickstart' => 'loyalty',
                'module' => 'broadcasting',
                'pushCredential' => $this->pushCredentialService->getByMerchantId($merchantId),
                'menu' => ($merchant->isPro()) ? 'menu.loyalty' : 'menu.punchcard',
                'header_text' => 'Push Credential'
            ]
        );
    }

    public function pushCredentialStore(Request $request)
    {
        $merchantId = $request->session()->get('user_id');
        $credentialInput = $this->getInput($request);
        $credentialInput['merchant_id'] = $merchantId;

        if (Input::hasFile('certificate')) {
            $certPath = env('BROADCASTING_IOS_PUSH_CERT_PATH');
            $destinationPath = base_path($certPath);
            $fileName = 'ios_push_cert_'.$merchantId.'.pem';
            File::delete($destinationPath.$fileName);
            Input::file('certificate')->move($destinationPath, $fileName);
            unset($credentialInput['certificate']);
            $credentialInput['ios_cert'] = $fileName;
        }
        $this->result = $this->pushCredentialService->update(['credential' => $credentialInput]);
        $this->setFlashMessage('Update', 'push credential');

        return $this->redirect('broadcast/pushCredential');
    }

    public function pushAndroidTry(Request $request)
    {
        $merchantId = $request->session()->get('user_id');
        $credential = $this->pushCredentialService->getByMerchantId($merchantId);
        $input = $this->getInput($request);
        $devices = json_decode($input['android_devices'], true);
        $devices = ($devices) ?: [];
        $message = $input['message'];
        $result = Push::sendAndroid(
            $credential,
            $devices,
            $message
        );
        Session::flash('alert-success', "Android Push: ".print_r($result, 1));

        return $this->redirect('broadcast/pushCredential');
    }

    public function pushIosTry(Request $request)
    {
        $merchantId = $request->session()->get('user_id');
        $credential = $this->pushCredentialService->getByMerchantId($merchantId);
        $input = $this->getInput($request);
        $devices = json_decode($input['ios_devices'], true);
        $devices = ($devices) ?: [];
        $message = $input['message'];
        $result = Push::sendIos(
            $credential,
            $devices,
            $message,
            'debug'
        );

        if (isset($result['debug'])) {
            Session::flash('alert-debug', "Ios Push: ".print_r($result, 1));
        }
        else {
            Session::flash('alert-success', "Ios Push: ".print_r($result, 1));
        }

        return $this->redirect('broadcast/pushCredential');
    }

    public function smsCredentialIndex(Request $request)
    {
        $merchantId = $request->session()->get('user_id');
        $merchant = $this->merchantService->getById($merchantId);
        return view(
            'broadcasting.sms',
            [   'login_name' => $request->session()->get('login_name'),
                'quickstart' => 'loyalty',
                'module' => 'broadcasting',
                'smsCredential' => $this->smsCredentialService->getByMerchantId($merchantId),
                'menu' => ($merchant->isPro()) ? 'menu.loyalty' : 'menu.punchcard',
                'header_text' => 'Sms Credential'
            ]
        );
    }

    public function smsCredentialStore(Request $request)
    {
        $merchantId = $request->session()->get('user_id');
        $credentialInput = $this->getInput($request);
        $credentialInput['merchant_id'] = $merchantId;
        $this->result = $this->smsCredentialService->update(['credential' => $credentialInput]);
        $this->setFlashMessage('Update', 'sms credential');

        return $this->redirect('broadcast/smsCredential');
    }

    public function smsTry(Request $request)
    {
        $merchantId = $request->session()->get('user_id');
        $credential = $this->pushCredentialService->getByMerchantId($merchantId);
        $input = $this->getInput($request);
        $sms = new Sms($merchantId, $this->smsCredentialService);
        $result = $sms->send($input['message'], $input['mobile'], 'Sms Test');
        Session::flash('alert-success', "Sms:".print_r($result, 1));

        return $this->redirect('broadcast/smsCredential');
    }

    public function emailPreview(Request $request)
    {
        $merchantId = $request->session()->get('user_id');
        $merchantLogo = $this->merchantService->getById($merchantId)->merchant_design->logoURL;

        return view(
            'preview.index',
            [   'subject' => '',
                'emailMessage' => '',
                'banner' => '',
                'salutation' => '',
                'merchantLogo' => $merchantLogo
            ]
        );
    }

    public function addEvent(Request $request)
    {


        $merchantId = $request->session()->get('user_id');
        $input = $this->getInput($request);

        $data['post_title'] = $input['aEventTitle'];
        $data['post_content'] = $input['aEventDetails'];
        $data['post_order'] = $input['aEventSequence'];
        $data['category'] = $input['aEventCategory'];
        $data['merchant_id'] = $merchantId;
        $data['event_color1'] = $input['aEventColor1'];
        $data['event_color2'] = $input['aEventColor2'];
        $data['url'] = $input['aEventUrl'];
        $data['post_type'] = $input['aEventType'];
        if(isset($input['aEventSubCategory'])){
            $data['post_sub_category'] = $input['aEventSubCategory'];
        }

        $process = $this->postsService->store($data);
        if ( $process )
        {
            $request->session()->flash('message', 'Event added successfully!');
        }

        return $this->redirect('broadcast/sms#targetEvent');
    }

    public function editEvent(Request $request)
    {
        $input = $this->getInput($request);
        $data['id'] = $request->id;

        $merchantId = $request->session()->get('user_id');
        $data['post_title'] = $input['eEventTitle'];
        $data['post_content'] = $input['eEventDetails'];
        $data['post_order'] = $input['eEventSequence'];
        $data['category'] = $input['eEventCategory'];
        $data['event_color1'] = $input['eEventColor1'];
        $data['event_color2'] = $input['eEventColor2'];
        $data['merchant_id'] = $merchantId;
        $data['url'] = $input['eEventUrl'];
        $data['post_type'] = $input['aEventType'];
        if(isset($input['eEventSubCategory'])){
          $data['post_sub_category'] = $input['eEventSubCategory'];
        }

        $process = $this->postsService->store($data);
        if ( $process )
        {
            $request->session()->flash('message', 'Event modified successfully!');
        }

        return $this->redirect('broadcast/sms#targetEvent');

    }

    public function check_image_supported( $mime_type )
    {
      if ( in_array( $mime_type, array('image/jpeg','image/png','image/gif')) )
      {
          return true;
      }
      return false;
    }

    public function publishEvent(Request $request)
    {
        $id = $request->id;
        $process = $this->postsService->publishEvent(true, $id);
        if ( $process )
        {
            $request->session()->flash('message', 'Event published successfully!');
        }

        return $this->redirect('broadcast/sms#targetEvent');
    }

    public function unpublishEvent(Request $request)
    {
        $id = $request->id;
        $process = $this->postsService->publishEvent(false, $id);
        if ( $process )
        {
            $request->session()->flash('message', 'Event unpublished successfully!');
        }

        return $this->redirect('broadcast/sms#targetEvent');
    }
}
