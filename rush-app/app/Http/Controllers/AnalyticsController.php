<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Rush\Citolaravel\Models\Transaction;
use Rush\Modules\Analytics\Services\AnalyticsService;
use Rush\Modules\Analytics\Services\PunchcardAnalyticsService;

class AnalyticsController extends Controller
{
    /**
     * @var Rush\Modules\Analytics\Services\AnalyticsService
     * @var Rush\Modules\Analytics\Services\PunchcardAnalyticsService
     */
    protected $analyticsService;
    protected $punchcardAnalyticsService;

    public function __construct(AnalyticsService $analyticsService, PunchcardAnalyticsService $punchcardAnalyticsService)
    {
        $this->analyticsService = $analyticsService;
        $this->punchcardAnalyticsService = $punchcardAnalyticsService;
    }

    public function index(Request $request)
    {
        $merchantId = $request->session()->get('user_id');
        $packageId = $request->session()->get('package_id');

        list($view, $data) = ($packageId == 1) ? $this->analyticsService->getControllerViewData($merchantId, $request) : $this->punchcardAnalyticsService->getControllerViewData($merchantId, $request);

        return view($view, $data);
    }
}
