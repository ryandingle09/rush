<?php

namespace App\Http\Controllers;

use Event;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Rush\Modules\Alert\Events\RequestDemoEvent;

class MyBusinessDemoController extends Controller
{
    public function index(Request $request){
    	
    	$data['form'] = 'show';
        if ( $request->isMethod('post') ) {
            
            $rules = array(
              'fname' => 'required',
              'lname' => 'required',
              'mobile' => 'required',
              'email' => 'required|email',
              'companyname' => 'required',
              'industry' => 'required',
              'g-recaptcha-response' => 'required|recaptcha_check'
            );

            $error_messages = [ 'recaptcha_check' => 'The Captcha field is invalid.' ];

            $input['fname'] = $request->fname;
            $input['lname'] = $request->lname;
            $input['name'] = $request->fname . ' ' . $request->lname;
            $input['companyname'] = $request->company;
            $input['industry'] = $request->industry;
            $input['mobile'] = isset($request->mobile) ? '+63' . $request->mobile : '';
            $input['email'] = $request->email;
            $input['objective'] = $request->program;
            $input['request_message'] = $request->details;
            $input['package'] = isset($request->product) ? implode(', ', $request->product) : "";
            $input['g-recaptcha-response'] = $request['g-recaptcha-response'];

            $input['from_sg'] = true;
            
            $input['url'] = 'Globe SG';
            $input['ip_address'] = $request->ip();

            Validator::extend('recaptcha_check', function ($attribute, $value, $parameters, $validator) {
                return $this->recaptcha_check($value);
            });

            $validator = Validator::make( $input, $rules, $error_messages );

            if ( $validator->fails() )
            {
                $data['errors'] = $validator->messages();
                $request->flash();
            } else {
                Event::fire(new RequestDemoEvent( $input ) );
                $data['form'] = 'hide';
                $data['input_success'] = true;
            }
        }

    	return view('my-business-demo.index', $data);
    }

    public function recaptcha_check($captcha)
    {
        $verify_captcha = json_decode(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='. getenv('G_RECAPTCHA_SECRET') .'&response='. $captcha));
        if($verify_captcha) {
            if (isset($verify_captcha->success) && $verify_captcha->success) {
                return true;
            }
        }
        return false;
    }
}
