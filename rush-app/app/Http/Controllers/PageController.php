<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Rush\Modules\Email\Services\EmailService;
use Rush\Modules\Merchant\Services\MerchantService;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PageController extends Controller
{
    /**
     * @var Rush\Modules\Email\Services\EmailService
     */
    protected $emailService;

    /**
     * @var Rush\Modules\Merchant\Services\MerchantService
     */
    protected $merchantService;

    public function __construct(
        EmailService $emailService,
        MerchantService $merchantService
    ) {
        $this->emailService = $emailService;
        $this->merchantService = $merchantService;
    }

    public function emailPreview(Request $request)
    {
        $email = $this->emailService->getEmailBySlug($request->emailId, $request->slug);
        $merchantLogo = $this->merchantService->getById($email->merchant_id)->merchant_design->logoURL;

        if (!$email) {
            throw new NotFoundHttpException("Error Processing Request");
        }
        return view(
            'preview.index',
            [   'subject' => $email->subject,
                'emailMessage' => $email->message,
                'banner' => $email->banner,
                'salutation' => $request->session()->get('login_name'),
                'merchantLogo' => $merchantLogo,
                'viewMode' => 'browser'
            ]
        );
    }
}
