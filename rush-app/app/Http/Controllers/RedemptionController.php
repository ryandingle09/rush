<?php

namespace App\Http\Controllers;

use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Rush\Modules\Admin\Services\AdminService;
use Rush\Modules\Helpers\Facades\StorageHelper;
use Rush\Modules\Merchant\Services\MerchantService;
use Validator;
use Intervention\Image\ImageManager;

class RedemptionController extends Controller
{
    protected $merchantService;  
    protected $adminService;  

    public function __construct(MerchantService $merchantService, AdminService $adminService)
    {
      $this->merchantService = $merchantService;
      $this->adminService = $adminService;

    }


    public function index(Request $request)
    {
      $merchant_id = $request->session()->get( 'user_id' );
      $data['merchant'] = $merchant = $this->merchantService->getById( $merchant_id );
      $data['branches'] = $merchant->branches()->where('is_deleted',0)->get()->toArray();
      $data['redemptions'] = $merchant->rewards;
      $data['vouchers'] = $this->merchantService->getVouchers( $data['branches'] );
      $data['admin_rewards'] = $this->adminService->get_admin_rewards();
      $data['redemption_peso'] = 1;
      $data['redemption_points'] = 1;
      if ( $merchant->conversion_settings ) {
      	$data['redemption_peso'] = $merchant->conversion_settings->redemption_peso;
      	$data['redemption_points'] = $merchant->conversion_settings->redemption_points;
      }
      $data['merchant_admin_rewards'] = array();
      if( $merchant->admin_rewards ) {
        foreach( $merchant->admin_rewards as $rewards )
        {
          $data['merchant_admin_rewards'][] = $rewards->admin_item_id;
        }
      }

      $data['message'] = $request->session()->get( 'message' );
      $data['menu'] = ( $merchant->packageId == 1 ) ? 'menu.loyalty' : 'menu.punchcard' ;
      $data['login_name'] = $request->session()->get('login_name');
      $data['module'] = 'redemption';
      $data['header_text'] = 'Rewards Catalogue';
      return view( 'redemption.index', $data );
    }

    public function save(Request $request)
    {
      $merchant_id = $request->session()->get( 'user_id' );

      $item['redeemItemId'] = $request['redeem_id'];
      $item['name'] = $request['product_name'];
      $merchant = $this->merchantService->getById( $merchant_id );
      $branches_count = $merchant->branches()->where('is_deleted',0)->count();
      if ( count($request['branches']) == $branches_count ) { $item['branch_ids'] = '[0]'; }
      else { $item['branch_ids'] = ( $request['branches'] ) ? json_encode($request['branches'], JSON_NUMERIC_CHECK) : '[0]'; }
      $item['details'] = $request['product_desc'];
      $item['pointsRequired'] = $request['points'];
      $item['timestamp'] = date("Y-m-d H:i:s");
      $item['merchantid'] = $merchant_id;

      if ( $request['file_change'] ) {
        $file = $request->file('img_file');
        $allowed_types = ['png', 'jpg'];

        if ( in_array( strtolower($file->getClientOriginalExtension()), $allowed_types ) )
        {
          $disk = StorageHelper::getRepositoryDisk();
          $filename  = $merchant_id.'-'.time().'-'.sprintf('%03d', rand(0,999)).'.'.$file->getClientOriginalExtension();
          $path = 'merchant/product/';
          $save_path = $path . $filename;
          $manager = new ImageManager();
          $image = $manager->make($file)->stream();
          $disk->put($save_path, $image->__toString(), 'public');
          $item['imageURL'] = 'merchant/product/' . $filename;
        }
      }

      $item['status'] = 1;
      $save = $this->merchantService->saveMerchantRewards( $merchant_id, $item );
      return redirect( url('/') . '/redemption');
    }

    public function delete(Request $request)
    {
      $merchant_id = $request->session()->get( 'user_id' );
      $item_id = $request['redeemId'];
      $this->merchantService->deleteItemForRedeem( $merchant_id, $item_id);
      return redirect( url('/') . '/redemption');
    }

    public function enable(Request $request)
    {
      if ( !isset($request['id']) || !isset($request['enabled']) )
      {
        return response()->json([
                                'error' => 'Missing important data.'
                            ]);
      } else {
        $merchant_id = $request->session()->get( 'user_id' );
        $id = $request['id'];
        $enable = $request['enabled'];
        $enable = ( $enable == 1 ) ? 0 : 1;
        $enable_keyword = ( $enable == 1 ) ? 'enabled' : 'disabled';

        if ( $this->merchantService->toggleAdminRewards( $merchant_id, $id, $enable) ) {
          return response()->json([
                                'success' => true,
                                'id' => $id,
                                'enabled_keyword' => $enable_keyword
                            ]);
        } else {
          return response()->json([
                                'error' => 'Something wrong happened.'
                            ]);
        }
        
      }
    }

    public function voucher_add(Request $request)
    {
      
      $validator = Validator::make($request->all(), [
            'voucher-name' => 'required',
            'voucher-description' => 'required',
            'branch_ids' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('redemption#voucher')
                ->withErrors($validator)
                ->withInput();
        } else {  

          if ( ! $request->threshold ) { $request->threshold = 1; }
          $this->merchantService->addVoucher( $request );

          return redirect('redemption#voucher')
            ->with('message', 'New voucher has been added!');
        }
    }

    public function voucher_edit(Request $request)
    {
      $validator = Validator::make($request->all(), [
            'voucher_id' => 'required',
            'evoucher-name' => 'required',
            'evoucher-description' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('redemption#voucher')
                ->withErrors($validator)
                ->withInput();
        } else {

          if ( ! $request->ethreshold ) { $request->ethreshold = 1; }
          $this->merchantService->editVoucher( $request );

          return redirect('redemption#voucher')
            ->with('message', 'Voucher has been modified!');
        }
    }

    public function voucher_delete(Request $request)
    {
      $validator = Validator::make($request->all(), [
            'voucher_id' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('redemption#voucher')
                ->withErrors($validator)
                ->withInput();
        } else {
          $this->merchantService->deleteVoucher( $request->voucher_id );
          return redirect('redemption#voucher')
            ->with('message', 'Voucher has been deleted!');
        }
    }

    public function voucher_uploadcodes(Request $request)
    {
      $validator = Validator::make($request->all(), [
            'voucher_id' => 'required',
            'voucher-import' => 'required|file|mimes:txt,csv',
        ]);

        if ($validator->fails()) {
            return redirect('redemption#voucher')
                ->withErrors($validator)
                ->withInput();
        } else {
          $this->submitImportVoucherCodesForm( $request );
          return redirect('redemption#voucher')
            ->with('message', 'Voucher codes has been uploaded!');
        }
    }

    private function submitImportVoucherCodesForm( $request )
    {
        $file = $request->file('voucher-import');
        $extension = $file->extension();
        if ($file->isValid() &&
            ($extension == 'txt' || $extension == 'csv')) {
            $path = $file->path();
            $reader = ReaderFactory::create(Type::CSV);
            $reader->open($path);
            foreach ($reader->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $index => $row) {
                    if ($index != 1) {
                        if ( $row[0] != '' && $row[1] != '' ) {
                          $expiration = Carbon::parse($row[1]);
                          $voucher_code = new \Rush\Modules\Merchant\Models\VoucherCodeModel;
                          $voucher_code->voucher_item_id = $request->voucher_id;
                          $voucher_code->code = $row[0];
                          $voucher_code->expiration = $expiration->toDateTimeString();
                          $voucher_code->save();
                        }
                    }
                }
            }
            $reader->close();
            return true;
        }
        return false;
    }

}
