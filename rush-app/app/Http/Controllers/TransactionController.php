<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Rush\Modules\Attendance\Helpers\AttendanceHelper;
use Rush\Modules\Attendance\Jobs\ClientEarnJob;
use Rush\Modules\Attendance\Services\AttendanceService;
use Rush\Modules\Attendance\Services\ClientAttendanceUploadService;
use Rush\Modules\Customer\Services\CustomerService;
use Rush\Modules\Customer\Services\CustomerTransactionService;
use Rush\Modules\Merchant\Services\EmployeeService;
use Rush\Modules\Merchant\Services\MerchantService;
use Rush\Modules\Merchant\Services\ProductService;
use Rush\Modules\Merchant\Services\NotificationService;
use Carbon\Carbon;
use File;
use Validator;
use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory;

class TransactionController extends AbstractCmsController
{
    protected $merchantService;

    /**
     * @var Rush\Modules\Customer\Services\CustomerService
     */
    protected $customerService;

    /**
     * @var Rush\Modules\Account\Services\AttendanceService
     */
    protected $attendanceService;

    /**
     * @var Rush\Modules\Attendance\Services\ClientAttendanceUploadService
     */
    protected $clientAttendanceUploadService;

    /**
     * @var Rush\Modules\Merchant\Services\EmployeeService
     */
    protected $employeeService;

    /**
     * @var Rush\Modules\Customer\Services\CustomerTransactionService
     */
    protected $customerTransactionService;

    /**
     * @var Rush\Modules\Merchant\Services\ProductService
     */
    protected $productService;
    
    protected $notificationService;

    public function __construct(
        MerchantService $merchantService,
        CustomerService $customerService,
        AttendanceService $attendanceService,
        ClientAttendanceUploadService $clientAttendanceUploadService,
        EmployeeService $employeeService,
        CustomerTransactionService $customerTransactionService,
        ProductService $productService,
        NotificationService $notificationService
    ) {
        $this->merchantService = $merchantService;
        $this->customerService = $customerService;
        $this->attendanceService = $attendanceService;
        $this->clientAttendanceUploadService = $clientAttendanceUploadService;
        $this->employeeService = $employeeService;
        $this->customerTransactionService = $customerTransactionService;
        $this->productService= $productService;
        $this->notificationService= $notificationService;
    }

    public function index(Request $request)
    {
        $merchant_id = $request->session()->get( 'user_id' );
        $user_type = $request->session()->get( 'user_type' );
        $data['merchant'] = $merchant = $this->merchantService->getById( $merchant_id );
        $data['is_enable_votes_table_column'] = $merchant->settings->enable_votes_table_column;
        $data['is_enable_product_module'] = $merchant->settings->enable_product_module;
        $data['is_enable_transaction_method'] = $merchant->settings->gcash_transaction;
        $data['product_transactions'] = $this->productService->getAllProductTransaction($merchant_id);
        $data['menu'] = ( $merchant->packageId == 1 ) ? 'menu.loyalty' : 'menu.punchcard' ;
        $data['login_name'] = $request->session()->get('login_name');
        $data['module'] = 'transactionhistory';
        $data['guestTransactions'] = $this->customerService->getNonMemberTransactions($merchant_id);
        $data['header_text'] = "Transaction History";
        return view( 'transaction.index', $data );
    }

    public function attendanceAjaxlist(Request $request)
    {
        $data = $this->attendanceService->all()->values();
        return response()->json(['data' => $data]);
    }

    public function attendanceStore(Request $request)
    {
        $attendanceInput = $this->getInput($request);
        $attendanceInput['merchant_id'] = $request->session()->get('user_id');
        $this->result = $this->attendanceService->store(['attendance' => $attendanceInput]);
        $this->setFlashMessage('Create', 'attendance');

        return $this->redirect('transactionhistory#attendance');
    }

    public function attendeesAjaxList(Request $request)
    {
        $data = $this->clientAttendanceUploadService->getListByAttendance($request->attendanceId)->values();
        return response()->json(['data' => $data]);
    }

    public function attendanceClientEarn(Request $request)
    {
        $merchantId = $request->session()->get('user_id');

        // get the MerchantSystemEmployee
        $employee = $this->employeeService->getMerchantSystemEmployee($merchantId);

        if ($employee) {
            $attendanceInput = $this->getInput($request);
            $attendance = $this->attendanceService->getById($attendanceInput['attendanceId']);
            if ($attendance && $attendance->status == AttendanceHelper::INACTIVE) {
                $this->dispatch(new ClientEarnJob($attendance, $employee));
                $attendance->status = AttendanceHelper::ACTIVE;
                $attendance->save();
                $this->result = true;
            } else {
                $this->result->errors()->add('Attendance', 'The selected attendance must be INACTIVE!');
            }
        } else {
            $this->result->errors()->add('Employee', 'Merchant System Employee not found!');
        }
        $this->setFlashMessage('Process', 'stamps');

        return $this->redirect('transactionhistory#attendance');
    }

    public function voidEarn($transactionReferenceCode)
    {
        $this->result = (boolean) $this->customerTransactionService->voidEarnByTransactionReferenceCode($transactionReferenceCode);
        $this->setFlashMessage('Void', 'earn');
        return $this->redirect('transactionhistory#transactions');
    }

    public function voidPointsPackage($receiptReferenceNo){
        $this->result = (boolean) $this->customerTransactionService->voidPointsPackage($receiptReferenceNo);
        $this->setFlashMessage('Void', 'purchase-package');

        return $this->redirect('transactionhistory#transactions');
    }

    public function voidTransactionPunchcard($transactionReferenceCode){
        $merchant_id = request()->session()->get('user_id');

        $this->result = (boolean) $this->customerService->voidPunchcardTransaction($transactionReferenceCode, $merchant_id);
        $this->setFlashMessage('Void', 'purchase-package');

        return $this->redirect('transactionhistory#transactions');
    }

    public function pointsSeedingUpload(Request $request)
    {
        $file = $request->file('point_seeding_file');
            if ( $file ) {
            $extension = $file->extension();
                if ( $file->isValid() && ($extension == 'csv' || $extension == 'txt') ) {

                    $data['merchant_id'] = $merchant_id = request()->session()->get('user_id');
                    $data['user_type'] = $user_type = request()->session()->get('user_type');
                    if ( $user_type == 2 )
                    {
                        $data['subuser_id'] = request()->session()->get('subuser_id');
                    } else if ( $user_type == 3 ) {
                        $data['bcu_id'] = request()->session()->get('bcu_id');
                    }
                    
                    $row_count = count(file($file, FILE_SKIP_EMPTY_LINES));
                    $success = false;
                    
                    if( $row_count >= 2 ) {    
                        $batch = $this->merchantService->addPointsSeedingBatch( $data );
                        $path = $file->path();
                        $reader = ReaderFactory::create(Type::CSV);
                        $reader->open($path);
                        foreach ($reader->getSheetIterator() as $sheet) {
                            foreach ($sheet->getRowIterator() as $index => $row) {
                                if ($index != 1) {
                                    $data['merchant'] = $merchant = $this->merchantService->getById( $merchant_id );
                                    if ( $merchant->settings->customer_login_param == "mobile" ) {
                                        $customer = $this->customerService->getCustomerByMobile( $row[1], $merchant_id);
                                        $row[1] = ( strlen($row[1]) == 11 ) ? $row[1] : '0' . $row[1];
                                    } else if ( $merchant->settings->customer_login_param == "email" ) {
                                        $customer = $this->customerService->getCustomerByEmail( $row[1], $merchant_id);
                                    }
                                        $branch = $this->merchantService->getBranchIdByName( $row[4], $merchant_id);
                                        if ( $customer && $branch ) {
                                            $transaction_date = Carbon::parse($row[0]);
                                            $batch_data = new \Rush\Modules\Merchant\Models\PointsSeedingDataModel;
                                            $batch_data->transaction_date = $transaction_date->toDateTimeString();
                                            $batch_data->customer = $row[1];
                                            $batch_data->or_number = $row[2];
                                            $batch_data->transaction_amount = $row[3];
                                            $batch_data->branch = $branch;
                                            $batch_data->status = $this->merchantService->checkSeedingDataStatus( $row[2], $merchant_id );
                                            $batch_data->batch_id = $batch->id;
                                            $batch_data->save();
                                            $success = true;
                                        }
                                    }
                                }
                            }
                        $reader->close();
                    }
                    if ( $success ) {
                        return redirect('transactionhistory#pointsSeeding')->with(['message' => 'Batch has been successfully uploaded.']);
                    } else {
                        return redirect('transactionhistory#pointsSeeding')->withErrors(['message' => 'Error in uploading file!']);
                    }
                }
        } else {
            return redirect('transactionhistory#pointsSeeding')->withErrors(['invalid_file' => 'Error in uploading file!']);
        }
    }

    public function points_seeding( Request $request )
    {
        $merchant_id = $request->session()->get( 'user_id' );
        $data['merchant'] = $merchant = $this->merchantService->getById( $merchant_id );
        $data['batch_id'] = $batch_id = $request->id;
        
        $data['menu'] = ( $merchant->packageId == 1 ) ? 'menu.loyalty' : 'menu.punchcard' ;
        $data['login_name'] = $request->session()->get('login_name');
        $data['module'] = 'transactionhistory';
        $data['header_text'] = "Points Seeding";
        $data['message'] = $request->session()->get('message') ? $request->session()->get('message') : null;
        return view( 'transaction.points-seeding', $data );
    }

    public function points_seeding_batch( Request $request )
    {
        $merchant_id = $request->session()->get('user_id');
        $request->request->add(['merchant_id' => $merchant_id]);
        $response = $this->merchantService->gatherPointsSeedingBatchTableData($request);
        return response()->json($response);
    }

    public function points_seeding_data( Request $request )
    {
        $merchant_id = $request->session()->get('user_id');
        $request->request->add(['merchant_id' => $merchant_id]);
        $response = $this->merchantService->gatherPointsSeedingDataTableData($request);
        return response()->json($response);
    }

    public function points_seeding_seed(Request $request)
    {
        $merchant_id = $request->session()->get('user_id');
        $id = $request->id;
        $seed_data = $this->merchantService->getPointsSeedingDataById( $id );
        $data['amount'] = $seed_data['transaction_amount'];
        $data['or_no'] = $seed_data['or_number'];
        $data['seed_branch'] = $seed_data['branch'];

        // get the MerchantSystemEmployee
        $employee = $this->employeeService->getMerchantSystemEmployee( $merchant_id );
        $data['employee_uuid'] = $employee->uuid;
        $data['merchant'] = $merchant = $this->merchantService->getById( $merchant_id );
        if ( $merchant->settings->customer_login_param == "mobile" ) {
            $customer = $this->customerService->getCustomerByMobile( $seed_data['customer'], $merchant_id );
        } else {
            $customer = $this->customerService->getCustomerByEmail( $seed_data['customer'], $merchant_id);
        }

        $transaction = $this->customerService->earnPointsSeeding( $employee, $customer, $data, 'point-seeding' );

        if ( $transaction )
        {
            $process = $this->merchantService->updateSeedingData( $id );
            $this->notificationService->earnPoints( $merchant, $transaction, $data['amount'] );
            $request->session()->flash('message','Points has been successfully seeded!');
            return redirect('points-seeding/' . $seed_data->batch_id);
        } else {
            $request->session()->flash('message','Points seeding error, duplicate OR number!');
            return redirect('points-seeding/' . $seed_data->batch_id);
        }
    }

    public function get_receipt_data(Request $request)
    {
        $response = $this->merchantService->getReceiptData( $request->id );
        return response()->json($response);
    }

    public function getHistory(Request $request)
    {
        $transactions = $this->merchantService->getMerchantTransactionsDaTaTable( $request );
        $response_data['draw'] = $request->draw;
        $response_data['recordsFiltered'] = $this->merchantService->countFilterTransactions( $request );
        $response_data['recordsTotal'] = $this->merchantService->countAllTransactions( $request );

        $merchant = $this->merchantService->getById( $request->session()->get( 'user_id' ) );
        
        $transactions = collect($transactions)->map(function($transaction) use ($merchant) {
            if(!isset($transaction->id)){
                return $transaction;
            }
            $transaction_model = $merchant->transactions()->where('id', $transaction->id)->first();
            
            if ($merchant->settings->enable_stamps_summary_punchcard_redemption) {
                if ($transaction_model->reward && $transaction->transactionType == 'redeem' && $merchant->default_reward_name == $transaction_model->reward->reward) {
                    $transaction->stamps = $transaction_model->stamps . ' stamps - ' . $transaction_model->promo_punchcard->promo_title;
                }
            }
            if($merchant->packageId == 2 && $transaction->transactionType == 'purchase-package'){
                $card = $transaction_model->class_package->card;
                $transaction->stamps = $card->earned_stamps->count();
            }

            if(file_exists( base_path() . '/../repository/customers/photos/'. $transaction->customer_uuid . '/vouchers/' . $transaction->voucher_code . '.png' )){
                $transaction->voucher_img = config('app.url') . '/repository/customers/photos/'. $transaction->customer_uuid . '/vouchers/' . $transaction->voucher_code . '.png';
            }

        return $transaction;
        })->toArray();

        $response_data['data'] = $transactions;
        return response()->json($response_data);
    }

    public function getAllHistory(Request $request){
        $request->merge([
            'length' => -1,
            'start' => 0,
            'order' => [
                [
                    'column' => 0,
                    'dir' => 'desc'
                ]
            ]
        ]);

        ini_set('memory_limit', '512M');
        
        $transactions = $this->merchantService->getMerchantTransactionsDaTaTable( $request );

        return response()->json($transactions);
    }

    public function votesDtAjax(Request $request)
    {
        $merchant_id = $request->session()->get('user_id');
        $request->request->add(['merchant_id' => $merchant_id]);
        $response = $this->merchantService->gatherVotesDtAjaxTableData($request);
        return response()->json($response);
    }

    public function pointsSeedingMassSeed(Request $request)
    {
        $merchant_id = $request->session()->get('user_id');
        $batch_id = $request->id;
        $seed_data_all = $this->merchantService->getPointsSeedingDataOrm($batch_id);
        $seed_count = 0;
        foreach($seed_data_all as $seed_data)
        {
            if ($seed_data->status == true)
            {
                $data['amount'] = $seed_data->transaction_amount;
                $data['or_no'] = $seed_data->or_number;
                $data['seed_branch'] = $seed_data->branch;

                // get the MerchantSystemEmployee
                $employee = $this->employeeService->getMerchantSystemEmployee($merchant_id);
                $data['employee_uuid'] = $employee->uuid;
                $data['merchant'] = $merchant = $this->merchantService->getById($merchant_id);
                if ( $merchant->settings->customer_login_param == "mobile" ) {
                    $customer = $this->customerService->getCustomerByMobile( $seed_data->customer, $merchant_id );
                } else {
                    $customer = $this->customerService->getCustomerByEmail( $seed_data->customer, $merchant_id);
                }

                $transaction = $this->customerService->earnPointsSeeding( $employee, $customer, $data, 'point-seeding' );

                if ( $transaction )
                {
                    $process = $this->merchantService->updateSeedingData( $seed_data->id );
                    $this->notificationService->earnPoints( $merchant, $transaction, $data['amount'] );
                }
                $seed_count++;
            }
        }

        if ($seed_count >0) {
            $request->session()->flash('message','Mass seed has been successfully performed!');
        }

        return redirect('points-seeding/' . $batch_id);
    }
}
