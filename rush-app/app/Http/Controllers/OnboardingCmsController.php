<?php

namespace App\Http\Controllers;

use Event;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Rush\Citolaravel\Helpers\MigrationHelper;
use Rush\Modules\OnboardingCms\Services\OnboardingCmsService;
use Intervention\Image\ImageManager;
use File;

class OnboardingCmsController extends Controller
{
    protected $request;
    protected $onboardingCmsService;
    protected $data;

    public function __construct(Request $request, OnboardingCmsService $onboardingCmsService)
    {
        $this->request = $request;
        $this->onboardingCmsService = $onboardingCmsService;
        $this->data['modules'] = $this->buildMenu();
    }

    public function index()
    {        
        $this->data['cms_userName'] = session('userName');
        $this->data['cms_pageTitle'] = 'RUSH Onboarding CMS | Home';
        $this->data['cms_sectionTitle'] = 'RUSH Onboarding CMS';
        return view('onboarding-cms.index', $this->data);
    }

    public function login()
    {
        return view('onboarding-cms.login');
    }

    public function logout()
    {
        session()->flush();
        return redirect(url('onboarding-cms/login'))->with('message', 'You have successfully logout.');
    }

    public function processLogin()
    {
        $this->data['username'] = $this->request->username;
        $this->data['password'] = $this->request->password;

        $validator = Validator::make( $this->data, [
            'username' => 'required|email|exists:onboarding_cms_user,email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            $this->request->flash();
            return redirect(url('onboarding-cms/login'))->withErrors($validator)->withInput();
        } else {
            $user = $this->onboardingCmsService->checkExistingLogin($this->data['username'], $this->data['password']);
            if  ($user)
            {
                /* set session values */
                session([ 'isLogin' => true,
                            'userName' => $user->firstname . ' ' . $user->lastname,
                            'userType' => $user->user_type,
                            'userId' => $user->id,
                            'permissions' => $user->type->modules,
                    ]);
                return redirect(url('onboarding-cms'));
            } else {
                $validator->errors()->add('not_existing_info', 'User not found. Please check the username and password.');
                return redirect(url('onboarding-cms/login'))->withErrors($validator)->withInput();
            }
        }
    }

    private function buildMenu()
    {
        $menu = [];
        if (!session('isLogin')) return $menu;
        $ids = json_decode(session('permissions'));
        $modules = $this->onboardingCmsService->getModules($ids);
        return $modules;
    }

    public function news()
    {
      $this->data['title']  = 'RUSH Admin - News';
      $this->data['header'] = 'News';
      $this->data['news'] = $this->onboardingCmsService->getAllNews();
      return view( 'onboarding-cms.news.index', $this->data );
    }
    public function createNews()
    {
      $this->data['title']  = 'RUSH Admin - News';
      $this->data['header'] = 'News';
      return view( 'onboarding-cms.news.create', $this->data );
    }

    public function processCreateNews()
    {
        $this->data['title'] = $this->request->newsTitle;
        $this->data['merchant_name'] = $this->request->newsMerchantName;
        $this->data['description'] = $this->request->newsDescription;
        $this->data['background'] = $this->request->newsheadBG;
        $this->data['background_image'] = $this->request->file('news_bgimg');
        $this->data['logo'] = $this->request->file('news_logo');
        $this->data['app_image_top'] = $this->request->file('news_appimg');
        $this->data['android_link'] = $this->request->android_link;
        $this->data['ios_link'] = $this->request->ios_link;
        $this->data['content'] = $this->request->news_content;
        $this->data['app_name'] = $this->request->app_name;
        $this->data['banner_image'] = $this->request->file('banner_image');
        $this->data['mobile_app_content'] = $this->request->mobile_app_content;
        $this->data['app_image1'] = $this->request->file('app_image1');
        $this->data['app_image1_label'] = $this->request->app_image1_label;
        $this->data['app_image2'] = $this->request->file('app_image2');
        $this->data['app_image2_label'] = $this->request->app_image2_label;
        $this->data['app_image3'] = $this->request->file('app_image3');
        $this->data['app_image3_label'] = $this->request->app_image3_label;
        $this->data['publish'] = ( $this->request->news_publish ) ? true : false;
        $this->data['publish_date'] = ( $this->request->news_publish ) ? Carbon::now() : null;
        $this->data['visibility'] = ( $this->request->news_publish ) ? true : false;
        $this->data['user_id'] = session('userId');

        //$max_content = $this->stripHtmlTags($this->request->news_content,1000,"max_content");
        $max_app_lbl1 = $this->stripHtmlTags($this->request->app_image1_label,100,"max_app_lbl");
        $max_app_lbl2 = $this->stripHtmlTags($this->request->app_image2_label,100,"max_app_lbl");
        $max_app_lbl3 = $this->stripHtmlTags($this->request->app_image3_label,100,"max_app_lbl");

        $validator = Validator::make( $this->data, [
            'title' => 'required|unique:onboarding_cms_news,title,NULL,id,deleted_at,NULL|max:150',
            'merchant_name' => 'required|max:150',
            'description' => 'required|max:500',
            'background_image' => 'image|dimensions:max_width=750,max_height=900|mimes:jpeg,png',
            'logo' => 'image|dimensions:max_width=300,max_height=300|mimes:jpeg,png',
            'app_image_top' => 'image|dimensions:max_width=530,max_height=900|mimes:jpeg,png,svg',
            'android_link' => 'max:150',
            'ios_link'     => 'max:150',
            //'content' => $max_content,
            'app_name' => 'max:150',
            'banner_image' => 'required|image|dimensions:max_width=360,max_height=200|mimes:jpeg,png',
            'mobile_app_content' => 'max:500',
            'app_image1' => 'image|dimensions:max_width=500,max_height=700|mimes:jpeg,png,svg',
            'app_image2' => 'image|dimensions:max_width=500,max_height=700|mimes:jpeg,png,svg',
            'app_image3' => 'image|dimensions:max_width=500,max_height=700|mimes:jpeg,png,svg',
            'app_image1_label' => $max_app_lbl1,
            'app_image2_label' => $max_app_lbl2,
            'app_image3_label' => $max_app_lbl3

        ]);


        // enclose label with <p> tag
        if (strpos($this->data['app_image1_label'], '<p>') !== false) {
            
        }else{
            $this->data['app_image1_label'] = ($this->data['app_image1_label'] != "") ? "<p>" . $this->data['app_image1_label']. "</p>" : $this->data['app_image1_label'];
        }

        if (strpos($this->data['app_image2_label'], '<p>') !== false) {
            
        }else{
            $this->data['app_image2_label'] = ($this->data['app_image2_label'] != "") ? "<p>" . $this->data['app_image2_label']. "</p>" : $this->data['app_image2_label'];
        }

        if (strpos($this->data['app_image3_label'], '<p>') !== false) {
            
        }else{
            $this->data['app_image3_label'] = ($this->data['app_image3_label'] != "") ? "<p>" . $this->data['app_image3_label']. "</p>" : $this->data['app_image3_label'];
        }


        if ($validator->fails()) {
            $this->request->flash();
            return redirect(url('onboarding-cms/news/create'))->withErrors($validator)->withInput();
        } else {

            $image_array =  array(['name' => 'background_image', 'suffix' => 'bgimg'],
                                ['name' => 'logo', 'suffix' => 'logo'],
                                ['name' => 'app_image_top', 'suffix' => 'app_image'],
                                ['name' => 'banner_image', 'suffix' => 'banner_image'],
                                ['name' => 'app_image1', 'suffix' => 'app_image1'],
                                ['name' => 'app_image2', 'suffix' => 'app_image2'],
                                ['name' => 'app_image3', 'suffix' => 'app_image3']
                            );
            foreach ($image_array as $arr) {
                if ($this->data[$arr['name']]) {
                    $this->data[$arr['name']] = $this->uploadNewImage( $this->data[$arr['name']], $arr['suffix']);
                }
            }

            $process = $this->onboardingCmsService->createNews( $this->data );
            if ($process) {
                return redirect(url('onboarding-cms/news'))->with('message', 'News has been successfully created.');
            } else {
                $validator->errors()->add('error_saving', 'An error has occured. Please try again.');
                return redirect(url('onboarding-cms/news/create'))->withErrors($validator)->withInput();
            }
        }
    }

    private function uploadNewImage($file, $suffix)
    {
        $filename  = time().'-'.$suffix.'.'.$file->getClientOriginalExtension();
        $path = public_path('page/onboarding-cms/images/newsroom/' . $filename);
        $image = new ImageManager();
        $image->make( $file->getRealPath())->save($path);
        return $filename;
    }

    public function newsVisibility()
    {
        $this->data['id'] = $this->request->id;
        $this->data['visibility'] = $this->request->value;

        $validator = Validator::make( $this->data, [
            'id' => 'required',
            'visibility' => 'required'
        ]);

        if ($validator->fails()) {
            $response['error'] = $validator->errors()->all();
            return response()->json($response, 404);
        } else {
            $response['status'] = $this->onboardingCmsService->newsVisibility( $this->data );
            return response()->json($response);
        }
    }
    
    public function editNews()
    {
      $this->data['title']  = 'RUSH Admin - News';
      $this->data['header'] = 'Edit News';
      $this->data['news'] = $this->onboardingCmsService->getNews( $this->request->id );
      return view( 'onboarding-cms.news.edit', $this->data );
    }

    public function processEditNews()
    {
        $this->data['id'] = $this->request->id;
        $this->data['title'] = $this->request->newsTitle;
        $this->data['merchant_name'] = $this->request->newsMerchantName;
        $this->data['description'] = $this->request->newsDescription;
        $this->data['background'] = $this->request->newsheadBG;
        $this->data['news_bgimg-filename'] = $this->request->input('news_bgimg-filename');
        $this->data['background_image'] = $this->request->file('news_bgimg');
        $this->data['logo'] = $this->request->file('news_logo');
        $this->data['app_image_top'] = $this->request->file('news_appimg');
        $this->data['android_link'] = $this->request->android_link;
        $this->data['ios_link'] = $this->request->ios_link;
        $this->data['content'] = $this->request->news_content;
        $this->data['app_name'] = $this->request->app_name;
        $this->data['banner_image'] = $this->request->file('banner_image');
        $this->data['banner_image-filename'] = $this->request->input('banner_image-filename');
        $this->data['mobile_app_content'] = $this->request->mobile_app_content;
        $this->data['app_image1'] = $this->request->file('app_image1');
        $this->data['app_image1_label'] = $this->request->app_image1_label;
        $this->data['app_image2'] = $this->request->file('app_image2');
        $this->data['app_image2_label'] = $this->request->app_image2_label;
        $this->data['app_image3'] = $this->request->file('app_image3');
        $this->data['app_image3_label'] = $this->request->app_image3_label;
        $this->data['publish'] = ( $this->request->news_publish ) ? true : false;
        $this->data['publish_date'] = ( $this->request->news_publish ) ? Carbon::now() : null;
        $this->data['visibility'] = ( $this->request->news_publish ) ? true : false;
        $this->data['user_id'] = session('userId');

        //$max_content = $this->stripHtmlTags($this->request->news_content,1000,"max_content");
        $max_app_lbl1 = $this->stripHtmlTags($this->request->app_image1_label,100,"max_app_lbl");
        $max_app_lbl2 = $this->stripHtmlTags($this->request->app_image2_label,100,"max_app_lbl");
        $max_app_lbl3 = $this->stripHtmlTags($this->request->app_image3_label,100,"max_app_lbl");

        $req = '';
        if(empty($this->data['banner_image-filename'])){
            $req = "required";
        }

        
        $validator = Validator::make( $this->data, [

            'title' => 'required|unique:onboarding_cms_news,title,'.$this->data['id'].',id,deleted_at,null|max:150',
            'merchant_name' => 'required|max:150',
            'description' => 'required|max:500',
            'background_image' => 'image|dimensions:max_width=750,max_height=900|mimes:jpeg,png',
            'logo' => 'image|dimensions:max_width=300,max_height=300|mimes:jpeg,png',
            'app_image_top' => 'image|dimensions:max_width=530,max_height=900|mimes:jpeg,png,svg',
            'android_link' => 'max:150',
            'ios_link'     => 'max:150',
            //'content' => $max_content,
            'app_name' => 'max:150',
            'banner_image' => 'image|dimensions:max_width=360,max_height=200|mimes:jpeg,png,svg|'.$req,
            'mobile_app_content' => 'max:500',
            'app_image1' => 'image|dimensions:max_width=500,max_height=700|mimes:jpeg,png,svg',
            'app_image2' => 'image|dimensions:max_width=500,max_height=700|mimes:jpeg,png,svg',
            'app_image3' => 'image|dimensions:max_width=500,max_height=700|mimes:jpeg,png,svg',
            'app_image1_label' => $max_app_lbl1,
            'app_image2_label' => $max_app_lbl2,
            'app_image3_label' => $max_app_lbl3

        ]);

        // enclose label with <p> tag
        if (strpos($this->data['app_image1_label'], '<p>') !== false) {
            
        }else{
            $this->data['app_image1_label'] = ($this->data['app_image1_label'] != "") ? "<p>" . $this->data['app_image1_label']. "</p>" : $this->data['app_image1_label'];
        }

        if (strpos($this->data['app_image2_label'], '<p>') !== false) {
            
        }else{
            $this->data['app_image2_label'] = ($this->data['app_image2_label'] != "") ? "<p>" . $this->data['app_image2_label']. "</p>" : $this->data['app_image2_label'];
        }

        if (strpos($this->data['app_image3_label'], '<p>') !== false) {
            
        }else{
            $this->data['app_image3_label'] = ($this->data['app_image3_label'] != "") ? "<p>" . $this->data['app_image3_label']. "</p>" : $this->data['app_image3_label'];
        }


        if ($validator->fails()) {
            $this->request->flash();
            return redirect(url('onboarding-cms/news/edit/'. $this->data['id']))->withErrors($validator)->withInput();

        } else {

            $news = $this->onboardingCmsService->getNews( $this->data['id'] );

            $image_array =  array(['name' => 'background_image', 'suffix' => 'bgimg'],
                                ['name' => 'logo', 'suffix' => 'logo'],
                                ['name' => 'app_image_top', 'suffix' => 'app_image'],
                                ['name' => 'banner_image', 'suffix' => 'banner_image'],
                                ['name' => 'app_image1', 'suffix' => 'app_image1'],
                                ['name' => 'app_image2', 'suffix' => 'app_image2'],
                                ['name' => 'app_image3', 'suffix' => 'app_image3']
                            );

            foreach ($image_array as $arr) {

                if ($this->data[$arr['name']]) {
                    if ($news->{$arr['name']}) { 
                        $this->deleteExistingImage($news->{$arr['name']});
                    }
                    $this->data[$arr['name']] = $this->uploadNewImage( $this->data[$arr['name']], $arr['suffix']);
                } else {
                    if($arr['name'] === "background_image" && empty($this->data['news_bgimg-filename'])){
                        continue;
                    }

                    $this->data[$arr['name']] = $news->{$arr['name']} ? $news->{$arr['name']} : null;
                }

                
            }
            
            $process = $this->onboardingCmsService->updateNews( $this->data );
            if ($process) {
                return redirect(url('onboarding-cms/news'))->with('message', 'News has been successfully updated.');
            } else {
                $validator->errors()->add('error_saving', 'An error has occured. Please try again.');
                return redirect(url('onboarding-cms/news/edit/' . $this->data['id'] ))->withErrors($validator)->withInput();
            }
        }
    }

    public function stripHtmlTags($strToStrip,$maxChar,$validationMsg){


        $content_html_length = strlen($strToStrip);
        $content_decoded = html_entity_decode(str_replace('\r\n','',str_replace('&nbsp;',' ',strip_tags($strToStrip))), ENT_COMPAT, 'UTF-8');
        $content_decoded_length = strlen(utf8_decode(trim(preg_replace('/\s+/', ' ', $content_decoded))));
        $max = intval($content_html_length) - intval($content_decoded_length) + intval($maxChar); 
        $max_content = "$validationMsg:".$max;
        return $max_content;
    }

    private function deleteExistingImage($file)
    {
        if  (File::exists(public_path('page/onboarding-cms/images/newsroom/' . $file))) {
            File::delete(public_path('page/onboarding-cms/images/newsroom/' . $file));
        }
    }
    
    public function deleteNews()
    {
        $this->data['id'] = $this->request->news_id;

        $validator = Validator::make( $this->data, [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            $this->request->flash();
            return redirect(url('onboarding-cms/news'))->withErrors($validator)->withInput();
        } else {

            $process = $this->onboardingCmsService->deleteNews( $this->data );
            if ($process) {
                return redirect(url('onboarding-cms/news'))->with('message', 'News has been successfully deleted.');
            } else {
                $validator->errors()->add('error_saving', 'An error has occured. Please try again.');
                return redirect(url('onboarding-cms/news' ))->withErrors($validator)->withInput();
            }
        }
    }

    public function clientLogos()
    {
      $this->data['title']  = 'RUSH Admin - Client Logos';
      $this->data['header'] = 'Client Logos';
      $this->data['logos'] = $this->onboardingCmsService->getAllClientLogos();
      return view( 'onboarding-cms.clientlogos.index', $this->data );
    }

    public function clientLogosVisibility()
    {
        $this->data['id'] = $this->request->id;
        $this->data['visibility'] = $this->request->value;

        $validator = Validator::make( $this->data, [
            'id' => 'required',
            'visibility' => 'required'
        ]);

        if ($validator->fails()) {
            $response['error'] = $validator->errors()->all();
            return response()->json($response, 404);
        } else {
            $response['status'] = $this->onboardingCmsService->clientLogosVisibility( $this->data );
            return response()->json($response);
        }
    }

    public function addClientLogo()
    {
        $this->data['client_name'] = $this->request->clientName;
        $this->data['logo'] = $this->request->file('clientimg');
        $this->data['user_id'] = session('userId');

        $validator = Validator::make( $this->data, [
            'client_name' => 'required|unique:onboarding_cms_clientlogos,client_name,NULL,id,deleted_at,NULL',
            'logo' => 'required|image|dimensions:width=200,height=250|mimes:jpeg,png'
        ]);

        if ($validator->fails()) {
            $this->request->flash();
            return redirect(url('onboarding-cms/clientlogos'))->withErrors($validator)->withInput();
        } else {

            if ($this->data['logo']) {
                $filename  = time() . '-logo.' . $this->data['logo']->getClientOriginalExtension();
                $path = public_path('page/onboarding-cms/images/partners/' . $filename);
                $logo = new ImageManager();
                $logo->make( $this->data['logo']->getRealPath())->resize(200, 250)->save($path);
                $this->data['logo'] = $filename;
            }

            $process = $this->onboardingCmsService->createClientLogo( $this->data );
            if ($process) {
                return redirect(url('onboarding-cms/clientlogos'))->with('message', 'Client Logo has been successfully created.');
            } else {
                $validator->errors()->add('error_saving', 'An error has occured. Please try again.');
                return redirect(url('onboarding-cms/clientlogos'))->withErrors($validator)->withInput();
            }
        }
    }

    public function editClientLogo()
    {
        $this->data['id'] = $this->request->clientlogos_id;
        $this->data['client_name'] = $this->request->clientNameE;
        $this->data['logo'] = $this->request->file('clientimgE');
        $this->data['user_id'] = session('userId');

        $validator = Validator::make( $this->data, [
            'id' => 'required',
            'client_name' => 'required|unique:onboarding_cms_clientlogos,client_name,'.$this->data['id'].',id,deleted_at,NULL',
            'logo' => 'image|dimensions:width=200,height=250|mimes:jpeg,png'
        ]);

        if ($validator->fails()) {
            $this->request->flash();
            return redirect(url('onboarding-cms/clientlogos'))->withErrors($validator)->withInput();
        } else {   

            $clientlogos = $this->onboardingCmsService->getClientLogo( $this->data['id'] );

            if ($this->data['logo']) {
                if ( $clientlogos->logo ) {
                    if  (File::exists(public_path('page/onboarding-cms/images/partners/' . $clientlogos->logo))) {
                        File::delete(public_path('page/onboarding-cms/images/partners/' . $clientlogos->logo));
                    }
                }
                $filename  = time() . '-logo.' . $this->data['logo']->getClientOriginalExtension();
                $path = public_path('page/onboarding-cms/images/partners/' . $filename);
                $logo = new ImageManager();
                $logo->make( $this->data['logo']->getRealPath())->resize(300, 300)->save($path);
                $this->data['logo'] = $filename;
            } else {
                $this->data['logo'] = $clientlogos->logo ? $clientlogos->logo : null;
            }

            $this->data['visibility'] = $clientlogos->visibility;

            $process = $this->onboardingCmsService->editClientLogo( $this->data );
            if ($process) {
                return redirect(url('onboarding-cms/clientlogos'))->with('message', 'Client Logo has been successfully edited.');
            } else {
                $validator->errors()->add('error_saving', 'An error has occured. Please try again.');
                return redirect(url('onboarding-cms/clientlogos'))->withErrors($validator)->withInput();
            }
        }
    }

    public function deleteClientLogo()
    {
        $this->data['id'] = $this->request->clientlogos_id;

        $validator = Validator::make( $this->data, [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            $this->request->flash();
            return redirect(url('onboarding-cms/clientlogos'))->withErrors($validator)->withInput();
        } else {

            $process = $this->onboardingCmsService->deleteClientLogo( $this->data );
            if ($process) {
                return redirect(url('onboarding-cms/clientlogos'))->with('message', 'Client Logo has been successfully deleted.');
            } else {
                $validator->errors()->add('error_saving', 'An error has occured. Please try again.');
                return redirect(url('onboarding-cms/clientlogos' ))->withErrors($validator)->withInput();
            }
        }
    }

}