<?php
namespace App\Http\Controllers;

use PDF;
use Validator;
use Ramsey\Uuid\Uuid;
use Box\Spout\Common\Type;
use Illuminate\Http\Request;
use Box\Spout\Reader\ReaderFactory;
use Rush\Modules\Customer\Models\CustomerModel;
use Rush\Modules\Merchant\Services\MerchantService;
use Rush\Modules\Merchant\Services\BranchService;
use Rush\Modules\Helpers\Facades\StorageHelper;

class CustomerController extends Controller
{
    public function generatePdf(Request $request, MerchantService $merchantService)
    {
        ini_set('memory_limit','256M');
        $merchantId = $request->session()->get('user_id');
        $merchant = $merchantService->getById($merchantId);
        if ($merchant) {
            $pdf = PDF::loadView('customer.pdf', ['customers' => $merchant->customers]);
//            return view('customer.pdf', ['customers' => $merchant->customers]);
            return $pdf
                    ->setPaper('a4')
                    ->setOrientation('portrait')
                    ->setOption('margin-top', 0)
                    ->setOption('margin-left', 0)
                    ->setOption('margin-right', 0)
                    ->setOption('margin-bottom', 0)
                    ->download(str_slug($merchant->business) . '-customers.pdf');
        } else {
            return response()->json(['error' => 'Not authorized.'],403);
        }
    }

    public function generateCustomerPdf(Request $request, MerchantService $merchantService)
    {
        $merchantId = $request->session()->get('user_id');
        $merchant = $merchantService->getById($merchantId);
        if ($merchant) {
            $mobile_no = $request->mobile_no;
            $customer = $merchant->customers->filter(function($customer) use ($mobile_no) {
                return $customer->mobile_no == $mobile_no;
            });
            $pdf = PDF::loadView('customer.pdf', ['customers' => $customer]);
            return $pdf->download(str_slug($merchant->business) . '-customers.pdf');
        } else {
            return response()->json(['error' => 'Not authorized.'],403);
        }
    }

    public function qr_code_download( Request $request, MerchantService $merchantService, BranchService $branchService, $branch_name, $name, $points, $scan_limit = 1 ){

        $merchant_id = $request->session()->get('user_id');


        $merchant = $merchantService->getById( $merchant_id );
        if( ! $merchant ) return response()->json(['error' => 'Not authorized.'], 403);

        $branch = $branchService->findByBranchName($branch_name, $merchant_id);
        if( ! $branch )  return response()->json(['error' => 'Invalid branch.'], 403);

        if( ! is_numeric($points) || (float) $points < 0 ) return response()->json(['error' => 'Invalid points.'], 403);

        if( ! is_numeric($scan_limit) || (float) $scan_limit < -1 ) return response()->json(['error' => 'Invalid scan limit.'], 403);


        $branch_ids = "[{$branch->id}]";
        $load = -1;

        $qr_code = $merchantService->qr_code_generate( $merchant_id, $branch_ids, $name, $points, $load, $scan_limit );
        $qr_code_pdf = PDF::loadView('customer.qr_code_pdf', ['qr_code' => $qr_code, 'branch_name' => $branch->branchName, 'qr_code_image_url' => StorageHelper::repositoryUrlForView($qr_code->image_url) ]);
        return $qr_code_pdf->download( str_slug($name) . '.pdf' );

    }

    public function registrationForm(Request $request, MerchantService $merchantService)
    {
        $merchantId = $request->session()->get('user_id');
        $merchant = $merchantService->getById($merchantId);
        if ($merchant) {
            return view('customer.registration');
        } else {
            return response()->json(['error' => 'Not authorized.'],403);
        }
    }

    public function submitRegistration(Request $request, MerchantService $merchantService)
    {
        $merchantId = $request->session()->get('user_id');
        $merchant = $merchantService->getById($merchantId);
        if ($merchant) {
            $inputs = $request->all();
            $inputs['mobile_no'] = '0' . $inputs['mobile_no'];
            $validator = Validator::make($inputs, [
                'first_name' => 'required',
                'last_name' => 'required',
                'mobile_no' => 'required|digits:11|unique:Customer,mobileNumber,NULL,customerId,merchantId,'
                                . $merchant->id,
                'email' => 'required|email|unique:Customer,email,NULL,customerId,merchantId,'
                            . $merchant->id,
                'company' => 'required',
                'designation' => 'required',
                'industry' => 'required'
            ]);
            if ($validator->fails()) {
                return redirect('customers/register')
                    ->withErrors($validator)
                    ->withInput();
            }
            // interim
            $customer = new \Rush\Modules\Customer\Models\CustomerModel;
            $customer->merchantId = $merchant->id;
            $customer->firstName = $inputs['first_name'];
            $customer->lastName = $inputs['last_name'];
            $customer->mobileNumber = $inputs['mobile_no'];
            $customer->PIN = '0000';
            $customer->uuid = Uuid::uuid1()->toString();
            $customer->email = $inputs['email'];
            $customer->fullName = $inputs['first_name'] . ' ' . $inputs['last_name'];
            $customer->registrationChannel = 'CMS';
            $customer->middleName = $inputs['company'];
            $customer->address = $inputs['designation'];
            $customer->timestamp = date("Y-m-d H:i:s");
            $customer->grade_level = $inputs['industry'];
            $customer->save();

            $year_substr = date('y');
            $merchant_id_substr = substr('0000' . $merchant->id, -4);
            $customer_id_substr = substr('000000' . $customer->id, -6) ;
            $customer->profile_id = $year_substr . '-' . $merchant_id_substr . '-' . $customer_id_substr;
            $customer->save();


            $qr = new \Rush\Modules\Customer\Models\CustomerQrCodesModel;
            $qr->QRCode = $customer->uuid;
            $qr->save();
            $qr->generate();
            $message = $customer->name . '('. $customer->mobile_no .') has been added! <br />'
                     . 'Click <b><a href="'. url('customers/generatePdf/'. $customer->mobile_no) .'">here</a></b> to download pdf.';
            return redirect('customers/register')
                    ->with('success', $message);
        } else {
            return response()->json(['error' => 'Not authorized.'],403);
        }
    }

    public function importFromCSVForm(Request $request, MerchantService $merchantService)
    {
        $merchantId = $request->session()->get('user_id');
        $merchant = $merchantService->getById($merchantId);
        if ($merchant) {
            return view('customer.csv_import');
        } else {
            return response()->json(['error' => 'Not authorized.'],403);
        }
    }

    public function submitImportCSV(Request $request, MerchantService $merchantService)
    {
        $merchantId = $request->session()->get('user_id');
        $merchant = $merchantService->getById($merchantId);
        if ($merchant) {
            $file = $request->file('csv_import');
            $extension = $file->extension();
            $path = $file->path();

            if ($file->isValid() &&
                ($extension == 'txt' || $extension == 'csv')
            ) {
                $reader = ReaderFactory::create(Type::CSV);
                $reader->open($path);
                foreach ($reader->getSheetIterator() as $sheet) {
                    // first validate rows
                    $errors = [];
                    foreach ($sheet->getRowIterator() as $index => $row) {
                        if ($index != 1) {
                            // add customer
                            $inputs = [
                                'first_name' => $row[0],
                                'last_name' => $row[1],
                                'mobile_no' => $row[3],
                                'email' => $row[2],
                                'company' => $row[4],
                                'designation' => $row[5],
                                'industry' => $row[6]
                            ];
                            $validator = Validator::make($inputs, [
                                'first_name' => 'required',
                                'last_name' => 'required',
                                'mobile_no' => 'required|digits:11|unique:Customer,mobileNumber,NULL,customerId,merchantId,'
                                    . $merchant->id,
                                'email' => 'required|email|unique:Customer,email,NULL,customerId,merchantId,'
                                    . $merchant->id,
                                'company' => 'required',
                                'designation' => 'required',
                                'industry' => 'required'
                            ]);
                            if ($validator->fails()) {
                                $errors[] = ['error_row_' . $index => $validator->messages()];
                            }
                        }
                    }
                    if (count($errors) == 0) {
                        // insert customers
                        foreach ($sheet->getRowIterator() as $index => $row) {
                            if ($index != 1) {
                                // add customer
                                $inputs = [
                                    'first_name' => $row[0],
                                    'last_name' => $row[1],
                                    'mobile_no' => $row[3],
                                    'email' => $row[2],
                                    'company' => $row[4],
                                    'designation' => $row[5],
                                    'industry' => $row[6]
                                ];
                                // interim
                                $customer = new \Rush\Modules\Customer\Models\CustomerModel;
                                $customer->merchantId = $merchant->id;
                                $customer->firstName = $inputs['first_name'];
                                $customer->lastName = $inputs['last_name'];
                                $customer->mobileNumber = $inputs['mobile_no'];
                                $customer->PIN = '0000';
                                $customer->uuid = Uuid::uuid1()->toString();
                                $customer->email = $inputs['email'];
                                $customer->fullName = $inputs['first_name'] . ' ' . $inputs['last_name'];
                                $customer->registrationChannel = 'CMS';
                                $customer->middleName = $inputs['company'];
                                $customer->address = $inputs['designation'];
                                $customer->timestamp = date("Y-m-d H:i:s");
                                $customer->grade_level = $inputs['industry'];
                                $customer->save();

                                $year_substr = date('y');
                                $merchant_id_substr = substr('0000' . $merchant->id, -4);
                                $customer_id_substr = substr('000000' . $customer->id, -6);
                                $customer->profile_id = $year_substr . '-' . $merchant_id_substr . '-' . $customer_id_substr;
                                $customer->save();

                                $qr = new \Rush\Modules\Customer\Models\CustomerQrCodesModel;
                                $qr->QRCode = $customer->uuid;
                                $qr->save();
                                $qr->generate();
                            }
                        }

                        return redirect('customers/importFromCSV')
                            ->with('success', 'Customers successfully imported!');
                    } else {
                        return redirect('customers/importFromCSV')
                            ->withErrors($errors)
                            ->withInput();
                    }
                }
                $reader->close();
            } else {
                return redirect('customers/importFromCSV')
                    ->withErrors(['invalid_file' => 'Invalid file'])
                    ->withInput();
            }
        } else {
            return response()->json(['error' => 'Not authorized.'],403);
        }
    }

    public function generateCustomerPdfByBranch(Request $request, MerchantService $merchantService, BranchService $branchService, $branch_name)
    {
        $merchant_id = $request->session()->get('user_id');

        $merchant = $merchantService->getById( $merchant_id );
        if ($merchant) {
            $branch = $branchService->findByBranchName($branch_name, $merchant_id);
            if( ! $branch ) {
                return response()->json(['error' => 'Invalid branch.'], 403);
            } else {
                $customers = $merchant->customers->filter(function($customer) use ($branch) {
                    $latest_transaction_with_branch = $customer->transactions
                            ->sortByDesc(function($transaction) {
                                return $transaction->created_at;
                            })
                            ->filter(function($transaction) {
                                return $transaction->employee || $transaction->branch;
                            })
                            ->first();

                    $transaction_branch = null;
                    if (!is_null($latest_transaction_with_branch)) {
                        if ($latest_transaction_with_branch->employee) {
                            $transaction_branch = $latest_transaction_with_branch->employee->branch;
                        } else {
                            $transaction_branch = $latest_transaction_with_branch->branch;
                        }
                    }

                    return $transaction_branch && $transaction_branch->id == $branch->id;
                });
                if ($customers->count()) {
                    $pdf = PDF::loadView('customer.pdf', ['customers' => $customers]);
                    return $pdf->download(str_slug($merchant->business) . '-customers.pdf');
                } else {
                    return response()->json(['error' => 'No customers.'], 403);
                }
            }
        } else {
            return response()->json(['error' => 'Not authorized.'],403);
        }
    }
}