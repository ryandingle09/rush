<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AbstractCmsController;
use Illuminate\Http\Request;
use Rush\Modules\Customer\Services\CustomerService;

class EventAttendeesController extends AbstractCmsController
{
    protected $inputMap = [
        'uuid' => 'attendee',
        'fullName' => 'attendeeName',
        'email' => 'attendeeEmail',
        'mobileNumber' => 'attendeeMobile',
        'attendee_status' => 'attendeeAttendance',
    ];

    protected $customerService;

    public function __construct(CustomerService $customerService)
    {
        $this->customerService = $customerService;
    }

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function index(Request $request)
    {
        return view(
            'event-attendees.index',
            [   'login_name' => $request->session()->get('login_name'),
                'header_text' => 'Event Attendees Management',
                'module' => 'attendees'
            ]
        );
    }

    public function confirmAttendance(Request $request)
    {
        // TODO
    }

    public function storeAttendee(Request $request)
    {
        $attendeeInput = $this->getInput($request);

        $attendeeInput['merchant_id'] = $request->session()->get('user_id');
        $this->result = $this->customerService->storeAttendee(['attendee' => $attendeeInput]);
        $this->setFlashMessage('Create', 'attendee');

        return $this->redirect('event-attendees');
    }

    public function updateAttendee(Request $request)
    {
        $this->inputMap = [
            'uuid' => 'e_attendee',
            'fullName' => 'e_attendeeName',
            'email' => 'e_attendeeEmail',
            'mobileNumber' => 'e_attendeeMobile',
            'attendee_status' => 'e_attendeeAttendance',
        ];

        $attendeeInput = $this->getInput($request);
        $attendeeInput['merchant_id'] = $request->session()->get('user_id');
        $this->result = $this->customerService->storeAttendee(['attendee' => $attendeeInput]);
        $this->setFlashMessage('Update', 'attendee');

        return $this->redirect('event-attendees');
    }

    public function uploadAttendee(Request $request)
    {
        $attendeeInput = $this->getInput($request);
        $attendeeInput['merchant_id'] = $request->session()->get('user_id');
        $this->result = $this->customerService->uploadAttendee(['attendee' => $attendeeInput]);
        $this->setFlashMessage('Upload', 'attendee');

        return $this->redirect('event-attendees');
    }

    public function deleteAttendee(Request $request)
    {
        $attendeeInput = $this->getInput($request);
        $attendeeInput['merchant_id'] = $request->session()->get('user_id');
        $attendeeInput['uuid'] = $attendeeInput['attendee'];
        $this->result = $this->customerService->deleteAttendee(['attendee' => $attendeeInput]);
        $this->setFlashMessage('Delete', 'attendee');

        return $this->redirect('event-attendees');
    }

    public function confirmAttendee(Request $request)
    {
        $this->inputMap = [
            'uuid' => 'c_attendee',
        ];
        $attendeeInput = $this->getInput($request);
        $attendeeInput['merchant_id'] = $request->session()->get('user_id');
        $this->result = $this->customerService->confirmAttendee(['attendee' => $attendeeInput]);
        $this->setFlashMessage('Confirm', 'attendee');

        return $this->redirect('event-attendees');
    }
}