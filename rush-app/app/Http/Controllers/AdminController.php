<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Rush\Citolaravel\Helpers\MigrationHelper;
use Rush\Modules\Admin\Services\AdminService;
use Rush\Modules\Billing\Services\BillingService;
use Rush\Modules\Merchant\Services\MerchantService;
use PDF;
use Barryvdh\Snappy;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class AdminController extends Controller
{
  protected $adminService;
  protected $billingService;
  protected $merchantService;

  public function __construct(Request $request, AdminService $adminService, BillingService $billingService, MerchantService $merchantService)
  {
    $this->adminService = $adminService;
    $this->billingService = $billingService;
    $this->merchantService = $merchantService;
  }

  public function index(Request $request)
  {
    $data['login_name'] = $request->session()->get('admin_name');
    $admin_type = $request->session()->get('admin_type');
    $admin_id = $request->session()->get('admin_id');
    $data['modules'] = $this->adminService->getAdminModules( $admin_type, $admin_id );
    $data['title']  = 'RUSH Admin - Home';
    $data['header'] = 'Home';
    $request->session()->forget('user_id');
    $request->session()->forget('package_id');
    $request->session()->forget('login_name');
    return view( 'admin.index', $data );
  }

  public function logout(Request $request)
  {
    $request->session()->flush();
    return redirect( url( '../') );
  }

  public function merchant_index(Request $request)
  {
    $data['login_name'] = $request->session()->get('admin_name');
    $data['merchants'] = $this->adminService->getMerchants();
    $data['title']  = 'RUSH Admin - Merchants';
    $data['header'] = 'Merchants';
    $request->session()->forget('user_id');
    $request->session()->forget('package_id');
    $request->session()->forget('login_name');
    return view( 'admin.merchant.index', $data );
  }

  public function merchant(Request $request)
  {
    $data['login_name'] = $request->session()->get('admin_name');
    $data['merchant_id'] = $merchant_id = $request->id;
    if ( !$this->adminService->merchantExist($merchant_id) ) return redirect('../'.MigrationHelper::getAppPrefix().'/admin/');
    $package_id = $this->adminService->getMerchantPackageId($merchant_id);
    if ( $request->isMethod('post') ) {
      $data['updated'] = $this->merchant_update( $request, $merchant_id, $package_id );
    }

    $data['merchant'] = $merchant = $this->adminService->getMerchantById($merchant_id);
    $data['header'] = $merchant->businessName;
    $data['title']  = 'RUSH Admin - ' . $merchant->businessName;
    $data['business_type'] = $this->getBusinessTypesOptions(); //Get Business Type Data for Multi-Dropdown Options
    $data['business_type_selected'] = array();
    if ( substr( $merchant->businessType, 0, 1) === "|"  ) {
      $data['business_type_selected'] = explode("|", $merchant->businessType);
    }
    $data['packages'] = $this->getPackagesOptions(); //Get Packages Data for Dropdown Options
    $data['trial_end_date'] = ( $merchant->trial_end ) ? Carbon::parse( $merchant->trial_end )->format('d/m/Y') : null;
    $data['cms_modules'] = $this->getModules( $merchant->packageId );
    $data['merchant_cms_modules'] = $this->getMerchantModules( $merchant );
    $data['resellers'] = $this->adminService->getResellersOptions();
    $data['broadcast_tools_modules'] = $this->getBroadcastToolsModules();
    $data['merchant_broadcast_tools'] = $this->getBroadcastToolsMerchant( $merchant );
    $data['class_management_tools_modules'] = $this->getClassManagementToolsModules();
    $data['merchant_class_management_tools'] = $this->getClassManagementToolsMerchant( $merchant );
    $data['enabled_logs'] = $this->adminService->getEnabledlogs( $merchant_id );
    $data['address'] = [ 'Alaminos' => 'Alaminos', 'Angeles' => 'Angeles', 'Antipolo' => 'Antipolo', 'Bacolod' => 'Bacolod', 'Bacoor' => 'Bacoor', 'Bago' => 'Bago', 'Baguio' => 'Baguio', 'Bais' => 'Bais', 'Balanga' => 'Balanga', 'Batac' => 'Batac', 'Batangas City' => 'Batangas City', 'Bayawan' => 'Bayawan', 'Baybay' => 'Baybay', 'Bayugan' => 'Bayugan', 'Biñan' => 'Biñan', 'Bislig' => 'Bislig', 'Bogo' => 'Bogo', 'Borongan' => 'Borongan', 'Butuan' => 'Butuan', 'Cabadbaran' => 'Cabadbaran', 'Cabanatuan' => 'Cabanatuan', 'Cabuyao' => 'Cabuyao', 'Cadiz' => 'Cadiz', 'Cagayan de Oro' => 'Cagayan de Oro', 'Calamba' => 'Calamba', 'Calapan' => 'Calapan', 'Calbayog' => 'Calbayog', 'Caloocan' => 'Caloocan', 'Candon' => 'Candon', 'Canlaon' => 'Canlaon', 'Carcar' => 'Carcar', 'Catbalogan' => 'Catbalogan', 'Cauayan' => 'Cauayan', 'Cavite City' => 'Cavite City', 'ManCebu Cityila' => 'Cebu City', 'Cotabato City' => 'Cotabato City', 'Dagupan' => 'Dagupan', 'Danao' => 'Danao', 'Dapitan' => 'Dapitan', 'Dasmariñas' => 'Dasmariñas', 'Davao City' => 'Davao City', 'Digos' => 'Digos', 'Dipolog' => 'Dipolog', 'Dumaguete' => 'Dumaguete', 'El Salvador' => 'El Salvador', 'Escalante' => 'Escalante', 'Gapan' => 'Gapan', 'General Santos' => 'General Santos', 'General Trias' => 'General Trias', 'Gingoog' => 'Gingoog', 'Guihulngan' => 'Guihulngan', 'Himamaylan' => 'Himamaylan', 'Ilagan' => 'Ilagan', 'Iligan' => 'Iligan', 'Iloilo City' => 'Iloilo City', 'Imus' => 'Imus', 'Iriga' => 'Iriga', 'Isabela' => 'Isabela', 'Kabankalan' => 'Kabankalan', 'Kidapawan' => 'Kidapawan', 'Koronadal' => 'Koronadal', 'La Carlota' => 'La Carlota', 'Lamitan' => 'Lamitan', 'Laoag' => 'Laoag', 'Lapu-Lapu' => 'Lapu-Lapu', 'Las Piñas' => 'Las Piñas', 'Legazpi' => 'Legazpi', 'Ligao' => 'Ligao', 'Lipa' => 'Lipa', 'Lucena' => 'Lucena', 'Maasin' => 'Maasin', 'Mabalacat' => 'Mabalacat', 'Makati' => 'Makati', 'Malabon' => 'Malabon', 'Malaybalay' => 'Malaybalay', 'Malolos' => 'Malolos', 'Mandaluyong' => 'Mandaluyong', 'Mandaue' => 'Mandaue', 'Manila' => 'Manila', 'Marawi' => 'Marawi', 'Marikina' => 'Marikina', 'Masbate City' => 'Masbate City', 'Mati' => 'Mati', 'Meycauayan' => 'Meycauayan', 'Muñoz' => 'Muñoz', 'Muntinlupa' => 'Muntinlupa', 'Naga' => 'Naga', 'Navotas' => 'Navotas', 'Olongapo' => 'Olongapo', 'Ormoc' => 'Ormoc', 'Oroquieta' => 'Oroquieta', 'Ozamiz' => 'Ozamiz', 'Pagadian' => 'Pagadian', 'Palayan' => 'Palayan', 'Panabo' => 'Panabo', 'Parañaque' => 'Parañaque', 'Pasay' => 'Pasay', 'Pasig' => 'Pasig', 'Passi' => 'Passi', 'Puerto Princesa' => 'Puerto Princesa', 'Quezon City' => 'Quezon City', 'Roxas' => 'Roxas', 'Sagay' => 'Sagay', 'Samal' => 'Samal', 'San Carlos' => 'San Carlos', 'San Carlos' => 'San Carlos', 'San Fernando' => 'San Fernando', 'San Fernando' => 'San Fernando', 'San Jose' => 'San Jose', 'San Jose del Monte' => 'San Jose del Monte', 'San Juan' => 'San Juan', 'San Pablo' => 'San Pablo', 'San Pedro' => 'San Pedro', 'Santa Rosa' => 'Santa Rosa', 'Santiago' => 'Santiago', 'Silay' => 'Silay', 'Sipalay' => 'Sipalay', 'Sorsogon City' => 'Sorsogon City', 'Surigao City' => 'Surigao City', 'Tabaco' => 'Tabaco', 'Tabuk' => 'Tabuk', 'Tacloban' => 'Tacloban', 'Tacurong' => 'Tacurong', 'Tagaytay' => 'Tagaytay', 'Tagbilaran' => 'Tagbilaran', 'Taguig' => 'Taguig', 'Tagum' => 'Tagum', 'Talisay' => 'Talisay', 'Tanauan' => 'Tanauan', 'Tandag' => 'Tandag', 'Tangub' => 'Tangub', 'Tanjay' => 'Tanjay', 'Tarlac City' => 'Tarlac City', 'Tayabas' => 'Tayabas', 'Toledo' => 'Toledo', 'Trece Martires' => 'Trece Martires', 'Tuguegarao' => 'Tuguegarao', 'Urdaneta' => 'Urdaneta', 'Valencia' => 'Valencia', 'Vlenzuela' => 'Valenzuela', 'Victorias' => 'Victorias', 'Vigan' => 'Vigan', 'Zamboanga City' => 'Zamboanga City' ];
    $data['billing_tiers'] = [
        [ 'name'=> 'tier1', 'member_count' => '1 - 10,000', 'msf' => '15,000', 'sms' => '2,500', 'label' => 'Member: 1 - 10,000' ],
        [ 'name'=> 'tier2', 'member_count' => '10,001 - 25,000', 'msf' => '35,000', 'sms' => '6,250', 'label' => 'Member: 10,001 - 25,000' ],
        [ 'name'=> 'tier3', 'member_count' => '25,001 - 50,000', 'msf' => '60,000', 'sms' => '12,500', 'label' => 'Member: 25,000 - 50,000' ],
        [ 'name'=> 'tier4', 'member_count' => '50,001 - 100,000', 'msf' => '80,000', 'sms' => '20,000', 'label' => 'Member: 50,001 - 100,000' ],
        [ 'name'=> 'tier5', 'member_count' => '100,001 and up', 'msf' => '100,000', 'sms' => '25,000', 'label' => 'Member: 100,001 and up' ]
      ];

    $data['merchant_billing_tiers'] = json_decode( $merchant->new_billing_tier_options );
    $data['merchant_allow_registration'] = ( $merchant->settings ) ? $merchant->settings->allow_registration : 1;
    $data['merchant_voucher_module'] = ( $merchant->settings ) ? $merchant->settings->voucher_module : 0;
    $data['merchant_code_management'] = ( $merchant->settings ) ? $merchant->settings->code_management : 0;
    $data['merchant_enable_feedback_category'] = ( $merchant->settings ) ? $merchant->settings->enable_feedback_category : 0;
    $data['merchant_add_member'] = ( $merchant->settings ) ? $merchant->settings->add_member : 0;
    $data['merchant_customer_login_param'] = ( $merchant->settings ) ? $merchant->settings->customer_login_param : 'mobile';
    $data['merchant_maintenance'] = ( $merchant->settings ) ? $merchant->settings->maintenance : false;
    $data['merchant_maintenance_text'] = ( $merchant->settings ) ? $merchant->settings->maintenance_text : "Please contact customer support.";
    $data['merchant_toggle_branch_feature'] = ( $merchant->settings ) ? $merchant->settings->toggle_branch_feature : false;
    $data['merchant_accrued_billing'] = ( $merchant->settings ) ? $merchant->settings->accrued_billing : true;

    $request->session()->put('user_id', $merchant_id);
    $request->session()->put('package_id', $package_id);
    $request->session()->put('login_name', $merchant->firstName . ' ' . $merchant->lastName);
    return view( 'admin.merchant.info', $data );
  }

  private function merchant_update( $request, $merchant_id, $package_id )
  {
    $v = Validator::make($request->all(), [
      'status' => 'required',
      'businessName' => 'required',
      'contactPerson' => 'required',
      'contactPersonNumber' => 'required',
      'contactPersonEmail' => 'required|email',
      'package_availed' => 'required',
      'enabled' => 'required',
      'trial_date_end' => 'required_if:status,Trial|date_format:m/d/Y',
      'address' => 'required',
      'reseller_email' => 'sometimes|required_with:reseller|email'
    ]);

    if ($v->fails()) {
      return back()->withErrors($v);
    } else {
      return $this->adminService->updateMerchant( $merchant_id, $request->all(), $package_id, $request->session()->get('admin_id') );
    }
  }

  public function merchant_branches(Request $request)
  {
    $data['login_name'] = $request->session()->get('admin_name');
    $data['merchant_id'] = $merchant_id = $request->id;
    if ( !$this->adminService->merchantExist($merchant_id) ) return redirect('../'.MigrationHelper::getAppPrefix().'/admin/');
    $data['branches'] = $this->adminService->getBranchesByMerchant($merchant_id);
    $data['menu'] = 'merchant';
    $header_merchant = $this->adminService->getMerchantNameForDisplay( $merchant_id );
    $data['title']  = 'RUSH Admin - ' . $header_merchant;
    $data['header'] = $header_merchant . ' - Branches';
    return view( 'admin.merchant.branches', $data );
  }

  public function merchant_employees(Request $request)
  {
    $data['login_name'] = $request->session()->get('admin_name');
    $data['merchant_id'] = $merchant_id = $request->id;
    if ( !$this->adminService->merchantExist($merchant_id) ) return redirect('../'.MigrationHelper::getAppPrefix().'/admin/');
    $data['employees'] = $this->adminService->getEmployeesByMerchant($merchant_id);
    $data['menu'] = 'merchant';
    $header_merchant = $this->adminService->getMerchantNameForDisplay( $merchant_id );
    $data['title']  = 'RUSH Admin - ' . $header_merchant;
    $data['header'] = $header_merchant . ' - Employees';
    return view( 'admin.merchant.employees', $data );
  }

  public function merchant_customers(Request $request)
  {
    $data['login_name'] = $request->session()->get('admin_name');
    $data['merchant_id'] = $merchant_id = $request->id;
    if ( !$this->adminService->merchantExist($merchant_id) ) return redirect('../'.MigrationHelper::getAppPrefix().'/admin/');
    $data['customers'] = $this->adminService->getCustomersByMerchant($merchant_id);
    $data['menu'] = 'merchant';
    $header_merchant = $this->adminService->getMerchantNameForDisplay( $merchant_id );
    $data['title']  = 'RUSH Admin - ' . $header_merchant;
    $data['header'] = $header_merchant . ' - Customers';
    return view( 'admin.merchant.customers', $data );
  }

  public function merchant_points(Request $request)
  {
    $data['login_name'] = $request->session()->get('admin_name');
    $data['merchant_id'] = $merchant_id = $request->id;
    if ( !$this->adminService->merchantExist($merchant_id) ) return redirect('../'.MigrationHelper::getAppPrefix().'/admin/');
    $data['customers'] = $this->adminService->getCustomersByMerchant($merchant_id);
    $data['menu'] = 'merchant';
    $header_merchant = $this->adminService->getMerchantNameForDisplay( $merchant_id );
    $data['title']  = 'RUSH Admin - ' . $header_merchant;
    $data['header'] = $header_merchant . ' - POINTS';
    return view( 'admin.merchant.points', $data );
  }

  public function merchant_transactions(Request $request)
  {
    $data['login_name'] = $request->session()->get('admin_name');
    $data['merchant_id'] = $merchant_id = $request->id;
    if ( !$this->adminService->merchantExist($merchant_id) ) return redirect('../'.MigrationHelper::getAppPrefix().'/admin/');
    $data['package_id'] = $this->adminService->getMerchantPackageId($merchant_id);
    $data['transactions'] = $this->adminService->getTransactionsByMerchant($merchant_id);
    // dd( $data['transactions'] );
    $data['menu'] = 'merchant';
    $header_merchant = $this->adminService->getMerchantNameForDisplay( $merchant_id );
    $data['title']  = 'RUSH Admin - ' . $header_merchant;
    $data['header'] = $header_merchant . ' - TRANSACTIONS';
    return view( 'admin.merchant.transactions', $data );
  }

  public function enable(Request $request)
  {
    $merchant = $this->adminService->getMerchantById( $request->id );
    if ( $request->enabled == 'enabled' ) {
      if ( $this->adminService->disableMerchant( $request->id, $request->text ) )  { $response = [ 'enabled' => FALSE ]; }
    } else {
      if ( $this->adminService->enableMerchant( $request->id, $request->text ) ) { $response = [ 'enabled' => TRUE ]; }
    }
    $this->adminService->logStatusChange( $request->id, $request->text, $request->enabled, $request->session()->get('admin_id') );
    return response()->json( $response );
  }

  public function settings(Request $request)
  {
    $data['login_name'] = $data['header'] = $request->session()->get('admin_name');
    $admin_id = $request->session()->get('admin_id');
    if ( $request->isMethod('post') ) {
      $data['updated'] = $this->adminService->updateAdmin( $admin_id, $request->all(), $admin_id);
    }
    $data['admin'] = $admin = $this->adminService->getAdminById($admin_id);
    $data['menu'] = 'settings';
    $data['title'] = "RUSH - Admin Settings";

    $request->session()->forget('user_id');
    $request->session()->forget('package_id');
    $request->session()->forget('login_name');

    return view( 'admin.settings', $data );
  }

  public function management_branches(Request $request)
  {
    $data['login_name'] = $request->session()->get('admin_name');
    $admin_id = $request->session()->get('admin_id');
    $data['branches'] = $this->adminService->getAllBranches();
    $data['menu'] = 'management';
    $data['title']  = 'RUSH Admin - Branches';
    $data['header'] = 'Management - Branches';
    return view( 'admin.management.branches', $data );
  }

  public function management_employees(Request $request)
  {
    // $data['customers'] = $this->adminService->getAllCustomers();
    $data['login_name'] = $request->session()->get('admin_name');
    $admin_id = $request->session()->get('admin_id');
    $data['employees'] = $this->adminService->getAllEmployees();
    $data['menu'] = 'management';
    $data['title']  = 'RUSH Admin - Employees';
    $data['header'] = 'Management - Employees';
    $data['is_admin'] = TRUE;
    return view( 'admin.management.employees', $data );
  }

  public function management_customers(Request $request)
  {
    $data['login_name'] = $request->session()->get('admin_name');
    $admin_id = $request->session()->get('admin_id');
    $data['customers'] = $this->adminService->getAllCustomers();
    $data['menu'] = 'management';
    $data['title'] =  'RUSH Admin - Customers';
    $data['header'] = 'Management - Customers';
    $data['is_admin'] = TRUE;
    return view( 'admin.management.customers', $data );
  }

  public function management_points(Request $request)
  {
    $data['login_name'] = $request->session()->get('admin_name');
    $admin_id = $request->session()->get('admin_id');
    $data['points'] = $this->adminService->getPoints();
    $data['menu'] = 'management';
    $data['title'] =  'RUSH Admin - Points';
    $data['header'] = 'Management - Points';
    $data['is_admin'] = TRUE;
    return view( 'admin.management.points', $data );
  }

  private function getBusinessTypesOptions()
  {
    $data = array();
    $results = $this->adminService->getBusinessTypes();
    foreach( $results as $result ) {
      $data[$result->id] = $result->name;
    }
    $data['Others'] = 'Others';
    return $data;
  }

  private function getPackagesOptions()
  {

    $data = array();
    $results = $this->adminService->getPackages();
    foreach( $results as $result ) {
      $data[$result->packageId] = $result->name;
    }
    return $data;
  }

  private function getBroadcastToolsModules()
  {
    return $this->adminService->getBroadcastToolsModules();
  }

  private function getClassManagementToolsModules()
  {
    return $this->adminService->getClassManagementToolsModules();
  }

  private function getBroadcastToolsMerchant( $merchant )
  {
    $data = array();
    foreach( $merchant->broadcast_tools as $result ) {
      $data[] = $result->broadcast_tools_id;
    }
    return $data;
  }

  private function getClassManagementToolsMerchant( $merchant )
  {
    $data = array();
    foreach( $merchant->class_management_tools as $result )
    {
      $data[] = $result->class_management_tool_id;
    }
    return $data;
  }

  private function getModules($package_id = 2)
  {
    return $this->adminService->getModulesByPackage($package_id);
  }

  private function getMerchantModules( $merchant )
  {
    $data = array();
    foreach( $merchant->modules_merchant as $result ) {
      $data[] = $result->module_id;
    }
    return $data;
  }

  public function rewards(Request $request)
  {
    $data['login_name'] = $request->session()->get('admin_name');
    $admin_id = $request->session()->get('admin_id');
    $data['rewards'] = $this->adminService->get_admin_rewards( true );    
    $data['menu'] = 'rewards';
    $data['title'] =  'RUSH Admin - Rewards';
    $data['header'] = 'Management - Rewards';
    $data['is_admin'] = TRUE;
    return view( 'admin.rewards.index', $data );
  }

  public function reward(Request $request)
  {
    $data['login_name'] = $request->session()->get('admin_name');
    $admin_id = $request->session()->get('admin_id');
    $reward_id = $request->id;
    $data['reward'] = $this->adminService->get_reward_info( $reward_id );
    $data['subscription_types'] = json_decode( $this->adminService->getSubscriptionType() );
    $data['subscription_types_array'] = array();
    foreach( $data['subscription_types'] as $type )
    {
      $data['subscription_types_array'][] = $type->brand;
    }
    $data['menu'] = 'rewards';
    $data['title'] =  'RUSH Admin - Rewards';
    $data['header'] = 'Management - Rewards';
    $data['is_admin'] = TRUE;

    if ($request->isMethod('post'))
    {

      $rules = array(
        'name' => 'required',
        'points' => 'required|integer',
        'type' => 'required',
      );

      $data['name'] = $request['name'];
      $data['details'] = $request['details'];
      $data['points'] = $request['points'];
      $data['type'] = $request['type'];
      $data['status'] = $request['status'];

      if ( $request->file('image') ){
        $rules['image'] = 'required|dimensions:width=350,height=200|mimes:jpg,jpeg,png,gif';
        $data['image'] = $request->file('image');
      }

      $validator = Validator::make( $data, $rules );

      if ($validator->fails())
      {
        $data['errors'] = $validator->messages();
        $request->flash();
      } else {
        return $this->process_reward( $data, $reward_id );
      }
    }

    return view( 'admin.rewards.reward', $data );
  }

  public function rewards_add(Request $request)
  {
    $data['login_name'] = $request->session()->get('admin_name');
    $admin_id = $request->session()->get('admin_id');
    $id = ( isset($request->id ) ) ? $request->id : null;
    $data['subscription_types'] = $this->adminService->getSubscriptionType();
    $data['menu'] = 'rewards';
    $data['title'] =  'RUSH Admin - Rewards';
    $data['header'] = 'Management - Add Reward';
    $data['is_admin'] = TRUE;

    if ($request->isMethod('post'))
    {
      $rules = array(
        'name' => 'required',
        'points' => 'required|integer',
        'image' => 'required|dimensions:width=350,height=200|mimes:jpg,jpeg,png,gif',
        'type' => 'required'
      );
      
      $data['name'] = $request['name'];
      $data['details'] = $request['details'];
      $data['points'] = $request['points'];
      $data['type'] = $request['type'];
      $data['status'] = $request['status'];
      $data['image'] = $request->file('image');

      $validator = Validator::make( $data, $rules );

      if ($validator->fails())
      {
        $data['errors'] = $validator->messages();
        $request->flash();
      } else {
        return $this->process_reward( $data, $id );
      }
    }
    return view('admin.rewards.add', $data);
  }

  private function process_reward( $data, $id = null )
  {
    
    $save['name'] = $data['name'];
    $save['details'] = $data['details'];
    $save['pointsRequired'] = $data['points'];
    $save['status'] = $data['status'];
    $save['type'] = ( $data['type'] ) ? json_encode( $data['type'] ) : null;
    
    
    if ( isset($data['image']) ){
      $filename = $data['image']->getClientOriginalName();
      $extension = pathinfo($filename, PATHINFO_EXTENSION);
      $allowed_types = array('png', 'jpg','jpeg');

      if ( in_array( strtolower($extension), $allowed_types ) )
      {
        $new_filename = 'AR-' . time() . '-' . sprintf('%03d', rand(0,999)) . '.' . $extension;
        $directory = base_path() . '/../repository/merchant/product/';
        $data['image']->move( base_path() . '/../repository/merchant/product/', $new_filename );  
        $data['image'] = '/repository/merchant/product/' . $new_filename;
      }
      $save['imageURL'] = $data['image'];
    }
    
    $id = $this->adminService->reward_save( $save, $id );

    return redirect("/admin/rewards");
  }

  public function reward_status(Request $request)
  {
    $id = $request['id'];
    $result = $this->adminService->reward_status( $id );
    if ( $result )
    {
      return redirect('/admin/rewards');
    }
  }

  public function rewards_redemption(Request $request)
  {
    $data['login_name'] = $request->session()->get('admin_name');
    $admin_id = $request->session()->get('admin_id');
    $data['transactions'] = $this->adminService->getAdminRewardsTransactions();
    $data['menu'] = 'rewards';
    $data['title'] =  'RUSH Admin - Rewards Redemption';
    $data['header'] = 'Management - Rewards Redemption';
    $data['is_admin'] = TRUE;
    return view( 'admin.rewards.redemption', $data );
  }

  public function merchant_billing(Request $request)
  {
    $data['login_name'] = $request->session()->get('admin_name');
    $data['merchant_id'] = $merchant_id = $request->id;
    if ( !$this->adminService->merchantExist($merchant_id) ) return redirect('../'.MigrationHelper::getAppPrefix().'/admin/');
    $data['package_id'] = $this->adminService->getMerchantPackageId($merchant_id);
    $data['billing_history'] = $this->billingService->getBillingHistory( $merchant_id );
    $data['menu'] = 'merchant';
    $header_merchant = $this->adminService->getMerchantNameForDisplay( $merchant_id );
    $data['title']  = 'RUSH Admin - ' . $header_merchant;
    $data['header'] = $header_merchant . ' - BILLING';
    return view( 'admin.merchant.billing', $data );
  }

  public function billing(Request $request)
  {
    $billing_id = $request->billing_id;
    $data['merchant_id'] = $merchant_id = $request->merchant_id;

    $data['merchant'] = $merchant = $this->merchantService->getById($merchant_id);
    $data['billing'] = $billing = $this->billingService->getById($billing_id);
    if ( empty($billing) ) return redirect()->action('AccountController@index');
    $data['previous_bill'] = $this->billingService->getPreviousBilling( $merchant_id, $billing_id );
    $addons_id = explode('|', $merchant->PackageAddons);
    foreach ( $addons_id as $id )
    {
      if ( $id == 1 ) $data['additional_sms_credits'] = $this->billingService->additional_sms_credits( $merchant, $billing->billing_period_start, $billing->billing_period_end );
      if ( $id == 2 ) $data['app_zero_rating'] = $this->billingService->app_zero_rating( $merchant, $billing->billing_period_start, $billing->billing_period_end );
      if ( $id == 3 ) $data['white_labeled_card'] = $this->billingService->white_labeled_card( $merchant, $billing->billing_period_start, $billing->billing_period_end );
    }
    
    $data['menu'] = 'merchant';
    $header_merchant = $this->adminService->getMerchantNameForDisplay( $merchant_id );
    $data['title']  = 'RUSH Admin - ' . $header_merchant;
    $data['header'] = $header_merchant . ' - Billing ID ' . $billing_id;
  
    if ( $billing->merchant_id == $merchant_id ) {
      return view( 'admin.merchant.billing.bill', $data );
    } else {
      return view( 'admin.merchant.billing.access-fail', $data);
    }
  }

  public function statement(Request $request )
    {
      $billing_id = $request->billing_id;
      $data['merchant_id'] = $merchant_id = $request->merchant_id;

      $data['merchant'] = $merchant = $this->merchantService->getById($merchant_id);
      $data['billing'] = $billing = $this->billingService->getById($billing_id);
      $data['previous_bill'] = $this->billingService->getPreviousBilling( $merchant_id, $billing_id );
      $addons_id = explode('|', $merchant->PackageAddons);
      foreach ( $addons_id as $id )
      {
        if ( $id == 1 ) $data['additional_sms_credits'] = $this->billingService->additional_sms_credits( $merchant, $billing->billing_period_start, $billing->billing_period_end );
        if ( $id == 2 ) $data['app_zero_rating'] = $this->billingService->app_zero_rating( $merchant, $billing->billing_period_start, $billing->billing_period_end );
        if ( $id == 3 ) $data['white_labeled_card'] = $this->billingService->white_labeled_card( $merchant, $billing->billing_period_start, $billing->billing_period_end );
      }
      $data['menu'] = ( $merchant->packageId == 1 ) ? 'menu.loyalty' : 'menu.punchcard' ;
      $data['login_name'] = $request->session()->get('login_name');
      $data['quickstart'] = 'account';
      $data['module'] = 'statement';
      $data['header_text'] = 'Billing';
      // return view('account.statement', $data );
      if ( $billing->merchant_id == $merchant_id ) {
        $pdf = PDF::loadView('account.statement', $data );
        return $pdf->download($merchant_id . '-soa-'. time() . '.pdf');
      } else {
        $menu = ( $merchant->packageId == 1 ) ? 'menu.loyalty' : 'menu.punchcard' ;
        return view( 'account.access-fail', $data );
      }
    }
    
}
