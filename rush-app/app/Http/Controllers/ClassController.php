<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ClassController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function index(Request $request)
    {
        return view(
            'class.index',
            [   'login_name' => $request->session()->get('login_name'),
                'header_text' => 'Class Management'
            ]
        );
    }
}