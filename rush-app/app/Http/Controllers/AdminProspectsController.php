<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Rush\Modules\Admin\Services\AdminService;
use Event;
use Rush\Modules\Alert\Events\UpdateProspectEvent;
use Illuminate\Support\Facades\Validator;

class AdminProspectsController extends Controller
{
  protected $adminService;
  protected $request;

  public function __construct(Request $request, AdminService $adminService)
  {
    $this->request = $request;
    $this->adminService = $adminService;
  }

  public function index()
  {
    $data['login_name'] = $this->request->session()->get('admin_name');
    $data['admin_type'] = $admin_type = $this->request->session()->get('admin_type');
    if ( $admin_type == 1 ) {
      $data['prospects'] = $this->adminService->getProspects();
      $data['bd_accounts'] = $this->adminService->getBdAccounts();
    } else {
      $admin_id = $this->request->session()->get('admin_id');
      $data['prospects'] = $this->adminService->getProspects( $admin_id );
    }
    $data['title']  = 'RUSH Admin - Prospects';
    $data['header'] = 'Prospects';
    $data['menu'] = 'prospects';
    return view( 'admin.prospects.index', $data );
  }

  public function info()
  {
    $data['login_name'] = $this->request->session()->get('admin_name');
    $prospect_id = $this->request->id;

    if ( $this->request->isMethod('post') ) {
        $bd_id = $this->request->bd_assigned;
        $process = $this->adminService->assignBD( $prospect_id ,$bd_id );
        if ( $process ) {
          $data['success'] = true;
          $prospect = $this->adminService->getProspectsInfo( $prospect_id );
          $email_input = [
                          'name' => $prospect->fullname,
                          'fname' => $prospect->firstname,
                          'lname' => $prospect->lastname,
                          'companyname' => $prospect->company,
                          'mobile' => $prospect->mobile,
                          'email' => $prospect->email,
                          'request_message' => $prospect->message,
                          'package' => $prospect->package,
                          'landing_page' => $prospect->url,
                          'ip_address' => $prospect->ip_address,
                          'industry' => json_decode( $prospect->industry ),
                          'objective' => json_decode( $prospect->objective ),
                          'bd_email' => isset($prospect->bd_account) ? $prospect->bd_account['email'] : false,
                          'bd_name' => isset($prospect->bd_account) ? $prospect->bd_account['name'] : false,
                          ];
          Event::fire(new UpdateProspectEvent( $email_input ) );
        }
    }
    if ( !isset($data['prospect']) ) { $data['prospect'] = $prospect = $this->adminService->getProspectsInfo( $prospect_id ); }
    $bd_accounts = $this->adminService->getBDaccounts();
    foreach( $bd_accounts as $bd_account )
    {
     $data['bd_accounts'][ $bd_account->id ] = $bd_account->name;
    }
    $data['title']  = 'RUSH Admin - Prospect ID: ' . $prospect_id;
    $data['header'] = 'Prospect ID: ' . $prospect_id;
    $data['menu'] = 'prospects';
    $this->request->session()->forget('user_id');
    $this->request->session()->forget('package_id');
    $this->request->session()->forget('login_name');
    return view( 'admin.prospects.info', $data );
  }

  public function bdaccounts()
  {
    $id = $this->request->id;
    $data['bdaccount'] = $this->adminService->getBDaccountInfo( $id );
    $data['bdaccount_modules'] = $this->getBDModules( $id );
    $data['admin_modules'] = $this->adminService->getAdminModules();
    
    $data['login_name'] = $this->request->session()->get('admin_name');
    $data['title']  = 'RUSH Admin - BD Account ID: ' . $id;
    $data['header'] = 'BD Account ID: ' . $id;
    $data['menu'] = 'prospects';
    $data['success'] = $this->request->session()->get('success');
    $data['message'] = $this->request->session()->get('message');
    return view( 'admin.prospects.bdaccounts', $data );
  }

  public function bdaccounts_update()
  {
    $id = $this->request->id;
    $data['name'] = $this->request->name;
    $data['admin_modules'] = $this->request->admin_modules;

    $rules = array(
        'name' => 'required',
        'admin_modules' => 'required|min:1',
      );

    $validator = Validator::make( $data, $rules );

    if ($validator->fails())
    {
      $this->request->session()->flash( 'errors', $validator->messages() );
      $this->$request->flash();
    } else {
      $this->adminService->updateBDAccount( $id, $data ); 
      $this->request->session()->flash( 'success', true );
      $this->request->session()->flash( 'message', 'BD Account was successfully edited.' );
    }
    return redirect("/admin/bdaccounts/".$id."/info");
  }

  public function add_bdaccount()
  {
    $data['admin_modules'] = $this->adminService->getAdminModules();
    $data['login_name'] = $this->request->session()->get('admin_name');
    $data['title']  = 'RUSH Admin - Add BD Account';
    $data['header'] = 'Add BD Account';
    $data['menu'] = 'prospects';
    return view( 'admin.prospects.bdaccountsadd', $data );
  }

  public function bdaccounts_add()
  {
    $data['name'] = $this->request->name;
    $data['email'] = $this->request->email;
    $data['password'] = $this->request->password;
    $data['admin_modules'] = $this->request->admin_modules;

    $rules = array(
        'name' => 'required',
        'email' => 'required|email|unique:bd_accounts,email',
        'admin_modules' => 'required|min:1',
        'password' => 'required|min:6'
      );
    
    $validator = Validator::make( $data, $rules );
    
    if ($validator->fails())
    {
      $this->request->session()->flash( 'errors', $validator->messages() );
      $this->request->flash();
      return redirect("/admin/bdaccounts/add");
    } else {
      $id = $this->adminService->updateBDAccount( null , $data ); 
      $this->request->session()->flash( 'success', true );
      $this->request->session()->flash( 'message', 'BD Account was successfully added.' );
      return redirect("/admin/bdaccounts/".$id."/info");
    }    
  }

  public function bdaccounts_delete()
  {
    $id = $this->request->id;
    $this->adminService->deleteBDAccount( $id );
    return redirect("admin/prospects");
  }

  private function getBDModules( $id )
  {
    $data = array();
    $modules = $this->adminService->getAdminModules( 2, $id );
    foreach( $modules as $result ) {
      $data[] = $result->module_id;
    }
    return $data;
  }
 
}
