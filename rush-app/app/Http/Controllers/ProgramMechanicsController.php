<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AbstractCmsController;
use Illuminate\Http\Request;
use Rush\Modules\Merchant\Services\BranchService;
use Rush\Modules\Merchant\Services\MembershipService;
use Rush\Modules\Merchant\Services\MerchantConversionSettingService;
use Rush\Modules\Merchant\Services\MerchantService;
use Rush\Modules\Merchant\Services\ProductService;
use Rush\Modules\Merchant\Services\ProductQRService;

class ProgramMechanicsController extends AbstractCmsController
{
    protected $inputMap = [
        'earning_peso' => 'earningPointsAmount',
        'earning_points' => 'earningPoints',
        'redemption_points' => 'redemptionPoints',
        'redemption_peso' => 'redemptionPointsAmount',
        'name' => 'name',
        'level' => 'level',
        'frequency_redemption' => 'frequencyRedemption'
    ];

    /**
     * @var Rush\Modules\Merchant\Services\MerchantService
     */
    protected $merchantService;

    /**
     * @var Rush\Modules\Merchant\Services\MerchantConversionSettingService
     */
    protected $conversionService;

    /**
     * @var Rush\Modules\Merchant\Services\BranchService
     */
    protected $branchService;

    /**
     * @var Rush\Modules\Merchant\Services\MembershipService
     */
    protected $membershipService;

    /**
     * @var Rush\Modules\Merchant\Services\ProductService
     */
    protected $productService;

    /**
     * @var Rush\Modules\Merchant\Services\ProductQRService
     */
    protected $productQRService;

    public function __construct(
        MerchantService $merchantService,
        MerchantConversionSettingService $conversionService,
        BranchService $branchService,
        MembershipService $membershipService,
        ProductService $productService,
        ProductQRService $productQRService
    ){
        $this->merchantService = $merchantService;
        $this->conversionService = $conversionService;
        $this->branchService = $branchService;
        $this->membershipService = $membershipService;
        $this->productService = $productService;
        $this->productQRService = $productQRService;
    }

    public function index(Request $request)
    {
        $merchantId = $request->session()->get('user_id');
        $merchant = $this->merchantService->getById($merchantId);

        if (!$merchant->isPro()) {
            return redirect()->route('home');
        }

        $settings = $merchant->conversion_setting;
        $conversions = $this->conversionService->getByMerchantId($merchantId);
        $branchOptions = $this->branchService->getListByMerchant($merchantId);
        $membershipOptions = $this->membershipService->getListByMerchant($merchantId);
        $memberships = $this->membershipService->getByMerchant($merchantId);
        $levelOptions = $this->membershipService->getLevelsByMerchant($merchantId);
        $products = $this->productService->getAll($merchantId);
        $products_qr = $this->productQRService->getAll($merchantId);
        $is_enable_product_module = $merchant->settings->enable_product_module;

        return view(
            'promos.index',
            array_merge(
                [   'earningPoints' => 0,
                    'redemptionPoints' => 0,
                    'header_text' => 'Program Mechanics'
                ],
                $this->getOutput($settings),
                [   'login_name' => $request->session()->get('login_name'),
                    'module' => 'promos',
                    'conversions' => $conversions,
                    'memberships' => $memberships,
                    'branchOptions' => $branchOptions,
                    'membershipOptions' => $membershipOptions,
                    'levelOptions' => $levelOptions,
                    'products' => $products,
                    'products_qr' => $products_qr,
                    'is_enable_product_module' => $is_enable_product_module]
            )
        );
    }

    public function store(Request $request)
    {
        $input = $this->getInput($request);
        $input['merchant_id'] = $request->session()->get('user_id');
        $input['modified_by'] = $input['merchant_id'];
        $input['updated_by'] = $input['merchant_id'];
        $input['package_id'] = $request->session()->get('package_id');

        $this->process(
            $this->conversionService->store(['input' => $input]),
            'Update',
            'program mechanics'
        );

        return $this->redirect('promos');
    }

    public function storeMembership(Request $request)
    {
        $input = $this->getInput($request);
        $input['merchant_id'] = $request->session()->get('user_id');

        $this->process(
            $this->membershipService->store(['input' => $input]),
            'Update',
            'membership'
        );

        return $this->redirect('promos#membership');
    }

    public function storeProduct(Request $request)
    {
        $input = $this->getInput($request);
        $input['merchant_id'] = $request->session()->get('user_id');
        $branch_count = count($this->branchService->getListByMerchant($input['merchant_id']));

        if(count($input['branch_ids']) >= $branch_count){
            $input['branch_ids'] = "[0]";
        } else {
            $input['branch_ids'] = '[' . implode(',', $input['branch_ids']) . ']';
        }

        $this->productService->store($input);

        return $this->redirect('promos#mechanics-per-product');
    }

    public function updateProduct(Request $request)
    {
        $input = $this->getInput($request);
        $input['merchant_id'] = $request->session()->get('user_id');
        $branch_count = count($this->branchService->getListByMerchant($input['merchant_id']));

        if(count($input['branch_ids']) >= $branch_count){
            $input['branch_ids'] = "[0]";
        } else {
            $input['branch_ids'] = '[' . implode(',', $input['branch_ids']) . ']';
        }

        $this->productService->update($input['product_id'], $input);

        return $this->redirect('promos#mechanics-per-product');
    }

    public function deleteProduct($product_id){
        $this->productService->delete($product_id);

        return $this->redirect('promos#mechanics-per-product');
    }

    public function storeProductQR(Request $request)
    {
        $input = $this->getInput($request);
        $input['merchant_id'] = $request->session()->get('user_id');

        $this->productQRService->storeProductQR($input);

        return $this->redirect('promos#mechanics-product-qr');
    }

    public function updateProductQR(Request $request)
    {
        $input = $this->getInput($request);
        $input['merchant_id'] = $request->session()->get('user_id');

        $this->productQRService->updateProductQR($input['product_qr_id'],$input);

        return $this->redirect('promos#mechanics-product-qr');
    }

    public function deleteProductQR($product_qr_id){
        $this->productQRService->delete($product_qr_id);

        return $this->redirect('promos#mechanics-product-qr');
    }
}
