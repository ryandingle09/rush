<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Rush\Modules\Admin\Services\AdminService;

class AdminBillingController extends Controller
{
  protected $adminService;

  public function __construct(Request $request, AdminService $adminService)
  {
    $this->adminService = $adminService;
  }

  public function index(Request $request)
  {
    $data['login_name'] = $request->session()->get('admin_name');
    $data['billings'] = $this->adminService->getBillings();
    $data['title']  = 'RUSH Admin - Billing';
    $data['header'] = 'Billing';
    $data['menu'] = 'billing';
    $request->session()->forget('user_id');
    $request->session()->forget('package_id');
    $request->session()->forget('login_name');
    return view( 'admin.billing.index', $data );
  }

  public function billing(Request $request)
  {
    $id = $request->id;

    if ( $request->isMethod('post') ) {
      $data['updated'] = $this->adminService->tagPayment( $id, $request->all() );
    }

    $data['login_name'] = $request->session()->get('admin_name');
    $data['billing'] = $this->adminService->getBillingById( $id );
    $data['title']  = 'RUSH Admin - Tag Payment';
    $data['header'] = 'Billing';
    $data['menu'] = 'billing';
    return view( 'admin.billing.info', $data );
  }
  
}
