<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use PDF;
use Rush\Citolaravel\Helpers\MigrationHelper;

class CardsQrcodeController extends Controller
{
    public function index($membership, $count, Request $request)
    {
        $merchant_id = $request->session()->get('user_id');
        $cards_path = MigrationHelper::getServerUrl(). "api/merchant/{$merchant_id}/cards/membership/{$membership}/count/$count";
        $json = json_decode(file_get_contents($cards_path), true);
        if($json['data']) {
            $pdf = PDF::loadView('cards.qrcode', ['cards' => $json['data']]);
            return $pdf->download("{$merchant_id}-cards-qrcode-{$count}.pdf");
        } else {
            return 'No available cards';
        }
    }
}
