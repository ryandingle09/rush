<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Rush\Modules\Merchant\Services\MerchantService;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    protected $merchantService;

    public function __construct( MerchantService $merchantService )
    {
        $this->merchantService = $merchantService;
    }

    public function index(Request $request)
    {
        $merchant_id = $request->session()->get( 'user_id' );
        $package_id = $request->session()->get( 'package_id' );
        $data['message'] = $request->session()->get( 'message' );

        if ( $request->isMethod('post') )
        {
            $rules = array(
              'modules' => 'required'
            );

            $input['modules'] = $request->modules;
            $input['functions'] = $request->cms_functions;

            $validator = Validator::make( $input, $rules );
            
            if ( $validator->fails() )
            {
                $data['errors'] = $validator->messages();
                $data['message'] = 'Error!';
                $request->flash();
            } else {
                $process = $this->merchantService->saveBcuOptions( $input['modules'], $input['functions'], $merchant_id );
                if ( $process )
                {
                    $request->session()->flash('message', 'CDE Options Saved Successfully!');
                    return redirect( url('users'));
                }
            }
        }


        $data['merchant'] = $merchant = $this->merchantService->getById( $merchant_id );
        $data['modules'] = $this->merchantService->getMerchantModules( $merchant_id );

        $merchant_bcu_modules = $this->merchantService->getMerchantBcuModules( $merchant_id );

        $data['merchant_bcu_modules'] = [];
        foreach( $merchant_bcu_modules as $module )
        {
          $data['merchant_bcu_modules'][] = $module->modules_id;
        }

        $data['sub_users'] = $merchant->sub_users()->get();
        $data['menu'] = ( $merchant->packageId == 1 ) ? 'menu.loyalty' : 'menu.punchcard' ;
        $data['login_name'] = $request->session()->get('login_name');
        $data['module'] = 'user';
        $data['header_text'] = "User Management";
        return view( 'user.index', $data );
    }

    public function add(Request $request)
    {
        $merchant_id = $request->session()->get( 'user_id' );
        $package_id = $request->session()->get( 'package_id' );

        if ( $request->isMethod('post') )
        {
            $rules = array(
              'name' => 'required',
              'email' => 'required|email|unique:merchant_users,email',
              'password' => 'required|min:6',
              'modules' => 'required'
            );

            $input['name'] = $request->name;
            $input['email'] = $request->email;
            $input['password'] = $request->password;
            $input['modules'] = $request->modules;

            $validator = Validator::make( $input, $rules );
            
            if ( $validator->fails() )
            {
                $data['errors'] = $validator->messages();
                $data['message'] = 'Error!';
                $request->flash();
            } else {
                $process = $this->merchantService->saveUser( $request, $merchant_id, $package_id );
                if ( $process )
                {
                    $request->session()->flash('message', 'User Added Successfully!');
                    return redirect( url('users'));
                }
            }
        }

        $data['modules'] = $this->merchantService->getMerchantModules( $merchant_id );
        $data['menu'] = ( $package_id == 1 ) ? 'menu.loyalty' : 'menu.punchcard' ;
        $data['login_name'] = $request->session()->get('login_name');
        $data['module'] = 'user';
        $data['header_text'] = "Add User";
        return view( 'user.add', $data );
    }

    public function edit(Request $request)
    {
        $merchant_id = $request->session()->get( 'user_id' );
        $package_id = $request->session()->get( 'package_id' );
        $request['id'] = $data['id'] = $id = $request->id;

        if ( $request->isMethod('post') )
        {

            $rules = array(
              'name' => 'required',
              'modules' => 'required'
            );

            $input['name'] = $request->name;
            $input['modules'] = $request->modules;
            if ( isset($request->password) && $request->password != '' )  {
                $input['password'] = $request->password;
                $rules['password'] = 'required|min:6';
            }

            $validator = Validator::make( $input, $rules );
            
            if ( $validator->fails() )
            {
                $data['errors'] = $validator->messages();
                $data['message'] = 'Error!';
                $request->flash();
            } else {
                $process = $this->merchantService->saveUser( $request, $merchant_id, $package_id );
                if ( $process )
                {
                    $request->session()->flash('message', 'User updated Successfully!');
                    return redirect( url('users'));
                }
            }
        }

        $data['user'] = $this->merchantService->getSubUser($id);
        $user_modules = $this->merchantService->getUserModules($id);
        $data['user_modules'] = null;
        foreach( $user_modules as $module )
        {
          $data['user_modules'][] = $module->modules_id;
        }
        $data['modules'] = $this->merchantService->getMerchantModules( $merchant_id );
        $data['menu'] = ( $package_id == 1 ) ? 'menu.loyalty' : 'menu.punchcard' ;
        $data['login_name'] = $request->session()->get('login_name');
        $data['module'] = 'user';
        $data['header_text'] = "Add User";
        return view( 'user.edit', $data );
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $process = $this->merchantService->disableUser( $id );
        if ( $process )
        {
            $request->session()->flash('message', 'User disabled Successfully!');
        } else {
            $request->session()->flash('message', 'Something wrong came up. Please try again.');
        }
        return redirect( url('users'));
    }

    public function enable(Request $request)
    {
        $id = $request->id;
        $process = $this->merchantService->enableUser( $id );
        if ( $process )
        {
            $request->session()->flash('message', 'User enabled Successfully!');
        } else {
            $request->session()->flash('message', 'Something wrong came up. Please try again.');
        }
        return redirect( url('users'));
    }
}
