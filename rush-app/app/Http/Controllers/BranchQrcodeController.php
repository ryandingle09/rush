<?php

namespace App\Http\Controllers;

use PDF;
use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Http\Request;
use Rush\Citolaravel\Helpers\MigrationHelper;
use Rush\Modules\Merchant\Services\BranchService;

class BranchQrcodeController extends Controller
{

    public function index(Request $request, BranchService $branchService)
    {
        $merchantId = $request->session()->get('user_id');
        $branches = $branchService->getBranchesByMerchantId($merchantId);
        if (!$branches->isEmpty()) {
            $branches = $branches->map(function ($branch) use ($branchService) {
                /** @var \Rush\Modules\Merchant\Models\MerchantBranchesModel $branch */
                $code = $branchService->getDateQrCode($branch, Carbon::now(), false);
                if (!$code) {
                    $code = $branchService->getDateQrCode($branch, false, false);
                }
                /** @var \Rush\Modules\Merchant\Models\MerchantBranchesDateQrModel $code */
                return [
                    'name' => $branch->name,
                    'date' => $code ? $code->date_generated->format('m-d-Y') : null,
                    'date_qr_url' => $code ? $code->qr->url : '',
                ];
            });
        }
        $pdf = PDF::loadView('branch.qrcode', ['branches' => $branches]);

        return $pdf->download($merchantId . '-branch-qrcodes.pdf');
    }

    public function generate(Request $request, BranchService $branchService)
    {
        $merchantId = $request->session()->get('user_id');
        $branches = $branchService->getBranchesByMerchantId($merchantId);
        if (!$branches->isEmpty()) {
            $branches = $branches->map(function ($branch) use ($branchService) {
                /** @var \Rush\Modules\Merchant\Models\MerchantBranchesModel $branch */
                $code = $branchService->getDateQrCode($branch, Carbon::now(), true);
                return [
                    'name' => $branch->name,
                    'date' => $code->date_generated->format('m-d-Y'),
                    'date_qr_url' => $code->qr->url
                ];
            });
        }
        $pdf = PDF::loadView('branch.qrcode', ['branches' => $branches]);

        return $pdf->download($merchantId . '-branch-qrcodes.pdf');
    }
}
