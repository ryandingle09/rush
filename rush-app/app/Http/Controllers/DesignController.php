<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use Intervention\Image\ImageManager;
use Rush\Modules\Design\Services\DesignService;
use Rush\Modules\Helpers\Facades\StorageHelper;
use Rush\Modules\Merchant\Services\MerchantService;

class DesignController extends Controller
{
    protected $merchantService;
    protected $designService;

    public function __construct( MerchantService $merchantService, DesignService $designService ) {
        $this->merchantService = $merchantService;
        $this->designService = $designService;
    }

    public function index(Request $request)
    {
        $merchant_id = $request->session()->get('user_id');
        $merchant = $this->merchantService->getById($merchant_id);

        $buttons = array(
            0 => array(
                'id' => 'btnTablet',
                'label' => 'Tablet'
            ),
            1 => array(
                'id' => 'btnCard',
                'label' => 'Card'
            ),
            2 => array(
                'id' => 'btnMobile',
                'label' => 'Mobile'
            )
        );

        $palettes = array(
            0 => array(
                "name" => "Palette 1",
                "r" => "#222532",
                "g" => "#009573",
                "b" => "#8CE994"
            ),
            1 => array(
                "name" => "Palette 2",
                "r" => "#43062E",
                "g" => "#922648",
                "b" => "#F90047"
            ),
            2 => array(
                "name" => "Palette 3",
                "r" => "#2F2F2C",
                "g" => "#005B67",
                "b" => "#00A8A0"
            ),
            3 => array(
                "name" => "Palette 4",
                "r" => "#FFF7EA",
                "g" => "#00ECFF",
                "b" => "#00CBD6"
            )
        );

        $button_index = 0;

        $merchant_app_data = $this->designService->getSavedMerchantApplication( $merchant_id );
        $card_design_data = $this->designService->getSavedLoyaltyCardDesign( $merchant_id );
        $customer_app_data = $this->designService->getSavedCustomerAppDesign( $merchant_id );
        $settings = $this->designService->getMerchantSettings( $merchant_id );

        $data = array(
            'module' => 'design',
            'design_button_id' => $buttons[$button_index]['id'],
            'design_button_label' => $buttons[$button_index]['label'],
            'palettes' => $palettes,

            'merchant_app_logo' => $merchant_app_data['logo'],
            'merchant_app_background' => $merchant_app_data['background'],
            'merchant_app_stamp' => $merchant_app_data['stamp'],
            'merchant_app_overlayColor' => $merchant_app_data['overlayColor'],
            'merchant_app_textColor' => $merchant_app_data['textColor'],
            'merchant_app_buttonsColor' => $merchant_app_data['buttonsColor'],

            'card_design_logo' => $card_design_data['logo'],
            'card_design_background' => $card_design_data['background'],
            'card_design_cardbgcolor' => $card_design_data['cardbgcolor'],

            'customer_app_logo' => $customer_app_data['logo'],
            'customer_app_background' => $customer_app_data['background'],
            'customer_app_stamp' => $customer_app_data['stamp'],
            'customer_app_name' => $customer_app_data['name'],
            'customer_app_overlayColor' => $customer_app_data['overlayColor'],
            'customer_app_textColor' => $customer_app_data['textColor'],
            'customer_app_buttonsColor' => $customer_app_data['buttonsColor'],
            'settings_program_name' => (isset( $settings->program_name )) ? $settings->program_name : '',
            'message' => $request->session()->get('message') ? $request->session()->get('message') : null,
            'merchantAppPalette' => 'design.template.merchantapp'
        );

        $view = 'design.punchcard';
        if ( $merchant->isPro() ) {
            $view = 'design.loyalty';
            $data['customerAppPalette'] = 'design.template.customerapp';
        }

        $data['menu'] = ( $merchant->packageId == 1 ) ? 'menu.loyalty' : 'menu.punchcard' ;
        $data['login_name'] = $request->session()->get('login_name'); 
        $data['header_text'] = "Design Management";
        return view($view, $data);
    }

    public function saveCustomerAppDesign(Request $request)
    {
        $user_id = $request->session()->get('user_id');
        $package_id = $request->session()->get('package_id');
        $designData = array();
        $error = false;
        $logo = $request->file('logo', FALSE);
        $background = $request->file('background', FALSE);
        $stamp = $request->file('stamp', FALSE);
        $disk = StorageHelper::getRepositoryDisk();

        if ($logo) {
            if ($this->check_image_supported($logo->getMimeType())) {
                $ext = $logo->getClientOriginalExtension();
                $logo_filename =  $user_id . '_' . Carbon::now()->timestamp . '_' . sprintf('%02d', rand(1,99)) . '.' . $ext;
                $logo_path = 'merchant/logo/';
                $save_path = $logo_path . $logo_filename;

                $manager = new ImageManager();
                $image = $manager->make($logo)->resize(200, 200)->stream();
                $disk->put($save_path, $image->__toString(), 'public');
                $designData['logoURL'] = StorageHelper::repositoryUrl($save_path);
            } else {
                $error = true;
            }
        }

        if ($background) {
            if ($this->check_image_supported($background->getMimeType())) {
                $ext = $background->getClientOriginalExtension();
                $background_filename =  $user_id . '_' . Carbon::now()->timestamp . '_' . sprintf('%02d', rand(1,99)) . '.' . $ext;
                $background_path = 'merchant/background/';
                $save_path = $background_path . $background_filename;

                $manager = new ImageManager();
                $image = $manager->make($background)->resize(640, 1136)->stream();
                $disk->put($save_path, $image->__toString(), 'public');
                $designData['backgroundURL'] = StorageHelper::repositoryUrl($save_path);
            } else {
                $error = true;
            }
        }

        if ($stamp) {
            if ($this->check_image_supported($stamp->getMimeType())) {
                $ext = $stamp->getClientOriginalExtension();
                $stamp_filename =  $user_id . '_' . Carbon::now()->timestamp . '_' . sprintf('%02d', rand(1,99)) . '.' . $ext;
                $stamp_path = 'merchant/stamps/';
                $save_path = $stamp_path . $stamp_filename;

                $manager = new ImageManager();
                $image = $manager->make($stamp)->resize(200, 200)->stream();
                $disk->put($save_path, $image->__toString(), 'public');
                $designData['stampURL'] = StorageHelper::repositoryUrl($save_path);
            } else {
                $error = true;
            }
        }

        if ($error == false) {
            $designData['merchantName'] = $request->merchantName;

            $overlay = $request->overlayColor;
            $designData['overlayColor'] = $overlay;

            $text = $request->textColor;
            $designData['textColor'] = $text;

            $buttons = $request->buttonsColor;
            $designData['buttonsColor'] = $buttons;

            $process = $this->designService->storeDesignDetails($user_id,$package_id,3,$designData);
            if ($process) {
                $request->session()->flash('message', 'Design Added Successfully!');
            }

        }

        return redirect( url('design#mobile') );
    }

    public function saveMerchantAppDesign(Request $request) {
        $user_id = $request->session()->get('user_id');
        $package_id = $request->session()->get('package_id');
        $merchantAppData = array();
        $error = false;
        $logo = $request->file('logo', FALSE);
        $background = $request->file('background', FALSE);
        $disk = StorageHelper::getRepositoryDisk();

        if ($logo) {
            if ($this->check_image_supported($logo->getMimeType())) {
                $ext = $logo->getClientOriginalExtension();
                $logo_filename =  $user_id . '_' . Carbon::now()->timestamp . '_' . sprintf('%02d', rand(1,99)) . '.' . $ext;
                $logo_path = 'merchant/logo/';
                $save_path = $logo_path . $logo_filename;

                $manager = new ImageManager();
                $image = $manager->make($logo)->resize(200, 200)->stream();
                $disk->put($save_path, $image->__toString(), 'public');
                $merchantAppData['logoURL'] = StorageHelper::repositoryUrl($save_path);
            } else {
                $error = true;
            }
        }

        if ($background) {
            if ($this->check_image_supported($background->getMimeType())) {
                $ext = $background->getClientOriginalExtension();
                $background_filename =  $user_id . '_' . Carbon::now()->timestamp . '_' . sprintf('%02d', rand(1,99)) . '.' . $ext;
                $background_path = 'merchant/background/';
                $save_path = $background_path . $background_filename;

                $manager = new ImageManager();
                $image = $manager->make($background)->resize(1136, 640)->stream();
                $disk->put($save_path, $image->__toString(), 'public');
                $merchantAppData['backgroundURL'] = StorageHelper::repositoryUrl($save_path);
            } else {
                $error = true;
            }
        }

        if ($error == false) {
            $text = $request->textColor;
            $merchantAppData['textColor'] = $text;

            $buttons = $request->buttonsColor;
            $merchantAppData['buttonsColor'] = $buttons;

            $process = $this->designService->storeDesignDetails($user_id, $package_id, 1, $merchantAppData);
            if ($process) {
                $request->session()->flash('message', 'Design Added Successfully!');
            }
        }

        return redirect( url('design#tablet') );
    }

    public function saveStampIcon(Request $request)
    {
        $user_id = $request->session()->get('user_id');
        $package_id = $request->session()->get('package_id');
        $stamp = $request->file('stamp', FALSE);
        $error = false;
        $stampURL = null;
        $disk = StorageHelper::getRepositoryDisk();

        if ($stamp) {
            if ($this->check_image_supported($stamp->getMimeType())) {
                $ext = $stamp->getClientOriginalExtension();
                $stamp_filename =  $user_id . '_' . Carbon::now()->timestamp . '_' . sprintf('%02d', rand(1,99)) . '.' . $ext;
                $stamp_path = 'merchant/stamps/';
                $save_path = $stamp_path . $stamp_filename;

                $manager = new ImageManager();
                $image = $manager->make($stamp)->resize(200, 200)->stream();
                $disk->put($save_path, $image->__toString(), 'public');
                $stampURL = StorageHelper::repositoryUrl($save_path);
            } else {
                $error = true;
            }
        }

        if($error == false) {
            $process = $this->designService->updateMerchantStamp($user_id, $stampURL);
            if ($process) {
                $request->session()->flash('message', 'Stamp Added Successfully!');
            }
        }

        return redirect('design#stamp-icon');
    }

    public function check_image_supported( $mime_type )
    {
      if ( in_array( $mime_type, array('image/jpeg','image/png')) )
      {
          return true;
      }
      return false;
    }
}
