<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Validator;
use Box\Spout\Common\Type;
use Illuminate\Http\Request;
use Box\Spout\Reader\ReaderFactory;

class VoucherController extends Controller
{
    public function addVoucherForm()
    {
        return view('voucher.add-voucher');
    }

    public function submitAddVoucherForm(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'branch_ids' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect('vouchers/addVoucher')
                ->withErrors($validator)
                ->withInput();
        }
        $branches = explode(',', $request->branch_ids);
        foreach($branches as $branch) {
            $voucher = new \Rush\Modules\Merchant\Models\VoucherModel;
            $voucher->name = $request->name;
            $voucher->branch_id = $branch;
            $voucher->save();
        }

        return redirect('vouchers/addVoucher')
            ->with('success', $request->name .' has been added!');
    }

    public function importVoucherCodesForm()
    {
        return view('voucher.import-voucher-codes');
    }

    public function submitImportVoucherCodesForm(Request $request)
    {
        $file = $request->file('fileCSV');
        $extension = $file->extension();
        if ($file->isValid() &&
            ($extension == 'txt' || $extension == 'csv')) {
            $path = $file->path();
            $reader = ReaderFactory::create(Type::CSV);
            $reader->open($path);
            foreach ($reader->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $index => $row) {
                    if ($index != 1) {
                        $expiration = Carbon::parse($row[1]);
                        $voucher_code = new \Rush\Modules\Merchant\Models\VoucherCodeModel;
                        $voucher_code->voucher_item_id = $request->voucher_id;
                        $voucher_code->code = $row[0];
                        $voucher_code->expiration = $expiration->toDateTimeString();
                        $voucher_code->save();
                    }
                }
            }
            $reader->close();
            return redirect('vouchers/importVoucherCodes')
                ->with('success', 'Voucher codes successfully imported!');
        } else {
            return redirect('customers/importFromCSV')
                ->withErrors(['invalid_file' => 'Invalid file'])
                ->withInput();
        }
    }
}
