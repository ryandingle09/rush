<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Rush\Modules\Achievements\Repositories\AchievementsInterface;
use Rush\Modules\Achievements\Services\AchievementsService;
use Rush\Modules\Merchant\Services\MerchantService;

class AchievementUnlockController extends Controller
{
    public $achievementService;
    public $merchantService;
    private $merchantId;
    private $packageId;
    private $package_type;
    private $menuView;

    public function __construct(AchievementsService $achievementsService, MerchantService $merchantService, Request $request)
    {
        $this->achievementService = $achievementsService;
        $this->merchantService = $merchantService;
        $this->merchantId = $request->session()->get('user_id');
        $this->packageId = $request->session()->get('package_id');
        $this->package_type = ($this->packageId == 1) ? 'loyalty' : 'punchcard';
        $this->menuView = ($this->packageId == 1) ? 'menu.loyalty' : 'menu.punchcard';
    }

    public function index(Request $request)
    {
        $achievements = $this->achievementService->getAllByMerchantId($this->merchantId);
        
        $merchant_id = $request->session()->get( 'user_id' );
        $merchant = $this->merchantService->getById( $merchant_id );
        $is_enable_transaction_method = $merchant->settings->gcash_transaction;
        
        return view('achievement-unlock.index-'. $this->package_type, [
            'menu' => $this->menuView,
            'header_text' => "Achievements Unlock",
            'login_name' => $request->session()->get('login_name'),
            'quickstart' => $this->package_type,
            'module' => 'achievement-unlock',
            'achievements' => $achievements,
            'is_enable_transaction_method' => $is_enable_transaction_method
        ]);
    }

    public function updateAchievementList(Request $request)
    {
        $achievements = $this->achievementService->updateAchievementsList($request->only(['achievements']), $this->merchantId);
        
        return redirect()->back()->with('status', 'Option lists has been updated.');
    }
    
    public function givePoints(Request $request)
    {
        $achievement_name =  $request->name;
        $achievement = $this->achievementService->getByName($achievement_name, $this->merchantId);

        if ($achievement_name) {
            $params = [
                'merchant_id' => $this->merchantId
            ];
            $this->achievementService->addCustomerAchievement($achievement_name, $params);

            $options = [
                'enabled' => $achievement->options->enabled,
                'points' => $achievement->options->points,
                'name' => '',
                'date' => '',
                'customers' => '',
                'repeat' => 1
            ];
            $this->achievementService->updateAchievement($achievement->id, ['options' => json_encode($options)]);

            return response()->json(['status' => true]);
        }
        
        return response()->json(['status' => false]);
    }
    
    public function giveStamps(Request $request)
    {
        $achievement_name =  $request->name;
        $achievement = $this->achievementService->getByName($achievement_name, $this->merchantId);

        if ($achievement_name) {
            $params = [
                'merchant_id' => $this->merchantId
            ];
            $this->achievementService->addCustomerAchievement($achievement_name, $params);

            $options = [
                'enabled' => $achievement->options->enabled,
                'stamps' => $achievement->options->stamps,
                'name' => '',
                'date' => '',
                'customers' => '',
                'repeat' => 1
            ];
            $this->achievementService->updateAchievement($achievement->id, ['options' => json_encode($options)]);

            return response()->json(['status' => true]);
        }
        
        return response()->json(['status' => false]);
    }
}
