<?php

namespace App\Http\Controllers;

use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use Intervention\Image\ImageManager;
use Rush\Modules\Customer\Services\CustomerService;
use Rush\Modules\Helpers\Facades\StorageHelper;
use Rush\Modules\Merchant\Services\MembershipService;
use Rush\Modules\Merchant\Services\MerchantService;
use Validator;

class ManagementController extends AbstractCmsController
{
    protected $merchantService;
    protected $customerService;
    protected $membershipService;

    public function __construct( MerchantService $merchantService, CustomerService $customerService, MembershipService $membershipService ) {
        $this->merchantService = $merchantService;
        $this->customerService = $customerService;
        $this->membershipService = $membershipService;
    }

    public function index(Request $request)
    {
        $merchant_id = $data['merchant_id'] = $request->session()->get('user_id');
        $user_type = $data['user_type'] = $request->session()->get( 'user_type' );
        $merchant = $this->merchantService->getById($merchant_id);
        $data['add_member'] = $merchant->settings->add_member;
        $data['enable_package_summary'] = $merchant->settings->enable_package_summary;
        $data['grade_level'] = $merchant->settings->grade_level;

        if($data['user_type'] == 3){ //if bcu
            $employee = $merchant->employees()->where('userId', $request->session()->get('bcu_id'))->first();
            $client_codes = [];

            if( $merchant->isPro() ) {
                $customer_ids = array_unique($this->customerService->getCustomerIdsWithBranchPointsTransaction($employee->branchId));
            } else {
                $customer_ids = array_unique($this->customerService->getCustomerIdsWithBranchPunchcardTransaction($employee->branchId));
            }

            $data['reports'] = $merchant->reports()->whereHas('customer', function($query) use ($customer_ids){
                $query->whereIn('customerId', $customer_ids);
            })->get();

            foreach($merchant->client as $client_code){
                if($client_code->status === "Available"){
                    $client_codes[] = $client_code;
                    continue;
                }

                if(in_array($client_code->customer->customerId, $customer_ids)){
                    $client_codes[] = $client_code;
                }
            }

            $data['branches'] = $merchant->branches()->where('is_deleted',0)->where('id', $employee->branchId)->get()->toArray();
        } else {
            $data['reports'] = $merchant->reports;
            $data['branches'] = $merchant->branches()->where('is_deleted',0)->get()->toArray();
        }

        if( $merchant->isPro() ) {
            $data['customers'] = $merchant->customers()->where('is_deleted',0)->get();
            $data['membership_levels'] = $this->membershipService->getByMerchant($merchant_id);
            $view = 'management.loyalty';
        } else {
            $data['checkins'] = [];
            if ( $merchant->checkin_tab == TRUE ) {
                if ( count($data['branches']) > 0 ) {
                    foreach( $data['branches'] as $branch ) {
                        $data['checkins'][$branch['id']] =  $this->merchantService->getCheckIn( $merchant_id, $branch['id'] );   
                    }
                }
            }
            $data['checkin_tab'] = $merchant->checkin_tab;
            $data['active_punchcard'] = $merchant->promo->ongoing;
            $data['punchcards'] = $merchant->punchcards;
            $data['packages'] = $merchant->class_packages;
            $view = 'management.punchcard';
            $data['stamps'] = $this->merchantService->getStamps($merchant_id);
        }

        $data['merchant'] = $merchant;
        $data['menu'] = ( $merchant->packageId == 1 ) ? 'menu.loyalty' : 'menu.punchcard' ;
        $data['login_name'] = $request->session()->get('login_name');
        $data['module'] = 'management';
        $data['message'] = $request->session()->get('message') ? $request->session()->get('message') : null;
        $data['header_text'] = 'Store &amp; Customer Management';

        return view($view, $data);
    }

    public function getCustomerPoints(Request $request){
        $merchant_id = $request->session()->get('user_id');
        $merchant = $this->merchantService->getById($merchant_id);
        $user_type = $request->session()->get( 'user_type' );
        $branch = '';

        if($user_type == 3){ //if bcu
            $employee = $merchant->employees()->where('userId', $request->session()->get('bcu_id'))->first();
            $branch = $merchant->branches()->where('id', $employee->branchId)->first();
        }

        $request->request->add(['merchant_id' => $merchant_id]);

        $response = $this->customerService->gatherCustomerPointsDataTableData($request, $branch);

        return response()->json($response);
    }

    public function getCustomerWithReserationsDetails(Request $request)
    {
        $response_data = $this->customerService->getCustomerWithReserationsDetails($request->customer_id);

        return response()->json($response_data);        
    }

    public function getCustomer(Request $request){
        $merchant_id = $request->session()->get('user_id');$merchant = $this->merchantService->getById($merchant_id);
        $user_type = $request->session()->get( 'user_type' );
        $branch = '';

        if($user_type == 3){ //if bcu
            $employee = $merchant->employees()->where('userId', $request->session()->get('bcu_id'))->first();
            $branch = $merchant->branches()->where('id', $employee->branchId)->first();
        }

        $request->request->add(['merchant_id' => $merchant_id]);

        $response = $this->customerService->gatherCustomerDataTableData($request, $branch);

        return response()->json($response);
    }

    public function addbranch(Request $request)
    {
        $branchLogo = $this->getBranchLogo( $request );
        $merchant_id = $request->session()->get('user_id');

        $data = array(
            'branchName' => $request->branchName,
            'street_name' => $request->streetName,
            'district' => $request->district,
            'city' => $request->city,
            'zipcode' => $request->zipcode,
            'longitude' => 0,
            'latitude' => 0,
            'status' => 1,
            'merchantId' => $merchant_id,
            'show' => $request->show ? $request->show : true
        );
        if ($branchLogo) {
            $data['branch_logo'] = $branchLogo;
        }

        $this->result = $this->merchantService->addBranch($data);
        $this->setFlashMessage('Create', 'Branch', 1);

        return $this->redirect('management#branch');
    }

    public function editbranch(Request $request)
    {
        $id = $request->id;
        $oldBranchLogo = $this->merchantService->getBranchLogo($id);
        $branchLogo = $this->getBranchLogo( $request );
        $merchant_id = $request->session()->get('user_id');

        $data = array(
            'branchName' => $request->branchName,
            'street_name' => $request->streetName,
            'district' => $request->district,
            'city' => $request->city,
            'zipcode' => $request->zipcode,
            'id' => $id,
            'show' => $request->show ? $request->show : true
        );

        if ($branchLogo) {
            $data['branch_logo'] = $branchLogo;
        }

        $process = $this->merchantService->updateBranch($merchant_id, $id, $data);
        if ($process) {
            StorageHelper::deleteFromRepository($oldBranchLogo);
            $request->session()->flash('message', 'Branch Updated Successfully!');
        }

        return redirect( url('management#branch') );
    }

    public function deletebranch(Request $request)
    {
        if ( $request->id ) {
            $id = $request->id;
            $merchant_id = $request->session()->get('user_id');
            $oldBranchLogo = $this->merchantService->getBranchLogo($id);
            $process = $this->merchantService->deleteBranch( $merchant_id, $id);
            if ($process) {
                StorageHelper::deleteFromRepository($oldBranchLogo);
                $request->session()->flash('message', 'Branch Deleted Successfully!');
            }
        }

        return redirect( url('management#branch') );
    }

    public function addEmployee(Request $request) {
       
        $employeeName = $request->employeeName;
        $branch = $request->selectBranch;
        $fourDigit = $request->fourDigit;
        $requirePassword = $request->requirePassword;

        $employeeData = array(
            'employeeName' => $employeeName,
            'branchId' => $branch,
            'fourDigit' => $fourDigit,
            'requirePassword' => ($requirePassword == "on" ? 1 : 0),
            'merchantId' => $request->session()->get('user_id')
        );
        $process = $this->merchantService->addEmployee( $employeeData );
        if ( $process )
        {
          $request->session()->flash('message', 'Employee Added Successfully!');
        }
        return redirect( url('management#employee') );
    }

    public function editEmployee(Request $request) {
        
        $employeeName = $request->employeeName;
        $branch = $request->selectBranch;
        $fourDigit = $request->fourDigit;
        $employeeId = $request->employeeId;
        $requirePassword = $request->requirePassword;
        $merchant_id = $request->session()->get('user_id');

        $employeeData = array(
            'employeeName' => $employeeName,
            'branchId' => $branch,
            'requirePassword' => ($requirePassword == "on" ? 1 : 0),
            'fourDigit' => $fourDigit
        );
        
        $process = $this->merchantService->updateEmployee( $merchant_id, $employeeId ,$employeeData );
        if ( $process )
        {
          $request->session()->flash('message', 'Employee Updated Successfully!');
        }

        return redirect( url('management#employee') );
    }

    public function deleteEmployee(Request $request)
    {
        if ( $request->id ) {
            $id = $request->id;
            $merchant_id = $request->session()->get('user_id');
            $process = $this->merchantService->deleteEmployee( $merchant_id, $id);
            if ( $process )
            {
              $request->session()->flash('message', 'Employee Deleted Successfully!');
            }
        }

        return redirect( url('management#employee') );
    }

    public function getBranchLogo( $request )
    {
        $branchLogo = null;
        $logo = $request->file('branch_logo', FALSE);
        $disk = StorageHelper::getRepositoryDisk();

        if ( $logo ) {
            if ( $this->check_image_supported( $logo->getMimeType() ) ) {
                $user_id = $request->session()->get('user_id');

                $ext = $logo->getClientOriginalExtension();
                $logo_filename =  $user_id . '_' . Carbon::now()->timestamp . '_' . sprintf('%02d', rand(1,99)) . '.' . $ext;
                $logo_path = 'merchant/branch/';
                $save_path = $logo_path . $logo_filename;

                $manager = new ImageManager();
                $image = $manager->make($logo)->resize(200, 200)->stream();
                $disk->put($save_path, $image->__toString(), 'public');
                $branchLogo = StorageHelper::repositoryUrl($save_path);
            }
        }

        return $branchLogo;
    }

    public function check_image_supported( $mime_type )
    {
      if ( in_array( $mime_type, array('image/jpeg','image/png')) )
      {
          return true;
      }
      return false;
    }

    public function addCustomerData(Request $request)
    {
        $customerInput = $this->getInput($request);
        $customerInput['merchantId'] = $request->session()->get('user_id');

        $this->result = $this->customerService->addCustomerData(['customer' => $customerInput]);
        $this->setFlashMessage('Add', 'member');

        return $this->redirect('management#customer');
    }

    public function uploadCustomerData(Request $request)
    {
        $customerInput = $this->getInput($request);
        $customerInput['merchantId'] = $request->session()->get('user_id');

        $this->result = $this->customerService->uploadCustomerData(['customer' => $customerInput]);
        $this->setFlashMessage('Upload', 'members');

        return $this->redirect('management#customer');
    }

    public function editCustomerData(Request $request){
        $merchant_id = $request->session()->get('user_id');
        $request->request->add(['merchant_id' => $merchant_id]);

        $response = $this->customerService->editCustomerData($request);

        return response()->json($response);
    }

    public function getCustomerStampsSummary(Request $request)
    {
        $merchant_id = $request->session()->get('user_id');
        $request->request->add(['merchant_id' => $merchant_id]);
        $merchant = $this->merchantService->getById($merchant_id);

        $promo_stamps_card_summary = collect([]);
        $promo_stamp_cards = collect([]);
        $user_type = $request->session()->get( 'user_type' );

        $branch_id = null;
        $punchcard_id = null;

        if ($request->punchcard_id) {
            $temp = explode('_', $request->punchcard_id);
            $request->request->add(['punchcard_id' => $temp[1]]);
        }

        if($user_type == 3){ //if bcu
            $employee = $merchant->employees()->where('userId', $request->session()->get('bcu_id'))->first();
            $branch_id = $employee->branchId;
        }

        $promo_stamp_cards_dt = $this->merchantService->getPromoStampsSummaryDT($request, $branch_id, $merchant->packageId);

        return response()->json($promo_stamp_cards_dt);
    }

    public function getClassSummary(Request $request){
        $merchant_id = $request->session()->get('user_id');
        $request->request->add(['merchant_id' => $merchant_id]);
        $merchant = $this->merchantService->getById($merchant_id);
        $package_id = $merchant->packageId;

        $branch_id = null;
        $user_type = $request->session()->get( 'user_type' );

        if($user_type == 3){ //if bcu
            $employee = $merchant->employees()->where('userId', $request->session()->get('bcu_id'))->first();
            $branch_id = $employee->branchId;
        }

        $package_stamp_cards = $this->merchantService->getPackageStampsSummaryDT($request, $branch_id, $package_id);

        $package_stamp_cards['data'] = $package_stamp_cards['data']->map(function($package) use ($package_id){

            if($package_id === 1){
                $package_transaction = $this->customerService->getLoyaltyTransactionByClassPackageId($package['customerClassPackageId']);

                $package['branch'] = $package_transaction->branch->name;    
            } else {
                $punchcard_transaction = $this->customerService->getPunchcardTransactionByClassPackageId($package['customerClassPackageId']);

                $package['branch'] = $punchcard_transaction->branch->name;

            }
            
            
            return $package;

        });

        return response()->json($package_stamp_cards);
    }

    public function postRedeemStamps(Request $request)
    {
        $merchant_id = $request->session()->get('user_id');
        $merchant = $this->merchantService->getById($merchant_id);
        if ($request->punchcard_id) {
            $temp = explode('_', $request->punchcard_id);
            $punchcard_id = $temp[1];
            $promo_stamp_cards = $this->merchantService->getPromoStampsSummary($merchant_id, $punchcard_id);
            $punchcard = $merchant->punchcards->filter(function($punchcard) use ($punchcard_id) {
                return $punchcard->id == $punchcard_id;
            })->first();
            if ($punchcard->status_text !== 'Expired') {
                return response()->json([
                    'error_code' => '0x1',
                    'message' => 'Sorry, redemption of stamps are not allowed on active punchcard.']);
            }
            $promo_stamp_cards_group = $promo_stamp_cards->filter(function($card) {
                    return $card->stamps->count() && !$card->redemption_status;
                })
                ->groupBy(function($card, $key) {
                    return $card->customer_id . '_' . $card->punchcard->id;
                });
            $data = [];
            foreach($promo_stamp_cards_group as $cards) {
                $stamps = 0;
                foreach($cards as $card) {
                    $stamps += $card->stamps->count();
                }
                $data[] = [
                    'customer' => $cards->first()->customer,
                    'stamps' => $stamps,
                    'cards'  => $cards,
                    'punchcard' => $cards->first()->punchcard
                ];
            }
            $transactions = collect($data)->map(function($item) {
                $transaction = $this->customerService->redeemStampsToCredit($item);
                return $transaction;
            });

            return response()->json([
                    'error_code' => '0x0',
                    'data' => $transactions->count()]);
        } else {
            return response()->json([
                'error_code' => '0x1',
                'message' => 'Please select a punchcard.']);
        }
    }

    public function expireCustomerPackage(Request $request)
    {
        $this->merchantService->expireCustomerClassPackage($request->customer_class_package_id);

        return $this->redirect('management#stamps-summary');
    }

    public function expirePointsCustomerPackage(Request $request)
    {
        $this->merchantService->expireCustomerClassPackage($request->customer_class_package_id);

        return $this->redirect('management#package-summary');
    }

    public function addClientCode(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'client-code' => 'required|unique:client,code'
        ]);

        if ($validator->fails() ) {
            return redirect('management#code-management')->with('message', $validator->errors()->first('client-code') );
        } else {
            $merchant_id = $request->session()->get('user_id');
            if ( $this->merchantService->addClientCode( $merchant_id, $request->{'client-code'} ) )
            {
                return redirect('management#code-management')->with('message', 'New client code has been added!');
            } else {
                return redirect('management#code-management')->with('message', 'An error has occured. Please try again.');
            }
        }
    }

    public function editClientCode(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'eclient-code-id' => 'required'
            ],
            [
                'eclient-code.required' => 'The Client Code is required.',
                'eclient-code.unique' => 'The Client Code is already taken.'
            ]
            );

        $data['eclient-code'] = $request->{'eclient-code'};
        $data['eclient-code-id'] = $request->{'eclient-code-id'};
        $data['ecustomer'] = $request->ecustomer ? $request->ecustomer : null;

        if ($validator->fails()) {
            if ( $validator->errors()->has('eclient-code') ) {
                $message = $validator->errors()->first('eclient-code');
            }
            if ( $validator->errors()->has('eclient-code-id') ) {
                $message = $validator->errors()->first('eclient-code-id');
            }
            return redirect('management#code-management')->with('message', $message );
        } else {
            $merchant_id = $request->session()->get('user_id');
            if ( $this->merchantService->editClientCode( $merchant_id, $data ) )
            {
                return redirect('management#code-management')->with('message', 'Client code has been modified!');
            } else {
                return redirect('management#code-management')->with('message', 'An error has occured. Please try again.');
            }
        }
    }

    public function importClientCode(Request $request)
    {
        $file = $request->file('import-code');
        $extension = $file->extension();
        if ($file->isValid() &&
            ($extension == 'txt' || $extension == 'csv')) {
            $data_to_upload = array();
            $path = $file->path();
            $reader = ReaderFactory::create(Type::CSV);
            $reader->open($path);
            $merchant_id = $request->session()->get('user_id');
            foreach ($reader->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $index => $row) {
                    if ($index != 1) {
                        $data_to_upload[] = ['merchant_id' => $merchant_id, 'code' => $row[0]];
                    }
                }
            }
            $reader->close();
            if ($this->merchantService->importClientCode($data_to_upload)) {
                return redirect('management#code-managemen')->with('success', 'Client codes successfully imported!');
            } else {
                return redirect('management#code-management')->with('message', 'An error has occured. Please try again.');
            }
        } else {
            return redirect('management#code-management')->with('message', 'An error has occured. Please try again.');
        }
    }

    public function getClientCode(Request $request)
    {
        $response = $this->merchantService->getClientCode($request);
        return response()->json($response);
    }

    public function getBranches(Request $request)
    {
        $response = $this->merchantService->getBranches($request);
        return response()->json($response);
    }

    public function getEmployees(Request $request)
    {
        $response = $this->merchantService->getEmployees($request);
        return response()->json($response);
    }

}
