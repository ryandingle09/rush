<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;

abstract class AbstractCmsController extends Controller
{
    /**
     * generic variable for storing results from service calls
     * @var boolean
     */
    protected $result = false;

    /**
     * generic variable for storing user input
     * @var mixed
     */
    protected $input = null;

    /**
     * generic variable for storing output variables
     * @var mixed
     */
    protected $output = null;

    /**
     * array mapping of formField names to entity properties
     * @var array
     */
    protected $inputMap = null;

    /**
     * Get all user inputs
     * @param  Request $request
     * @return array
     */
    public function getInput($request)
    {
        $this->input = $request->all();

        if ($this->inputMap) {
            foreach($this->inputMap as $key => $value) {
                if (isset($this->input[$value])) {
                    $this->input[$key] = $this->input[$value];
                }
            }
        }

        return $this->input;
    }

    public function getOutput($output)
    {
        $this->output = [];

        if ($this->inputMap) {
            foreach($this->inputMap as $key => $value) {
                if (isset($output[$key])) {
                    $this->output[$value] = $output[$key];
                }
            }
        }

        return $this->output;
    }

    /**
     * include Errors and Input when necessary
     * @param  string $dest defined route
     * @return null
     */
    public function redirect($dest)
    {
        if ($this->result === true) {
            return redirect($dest);
        } else {
            return redirect($dest)->withErrors($this->result)->withInput($this->input);
        }
    }

    /**
     * Set the flash message on the result (success|failure) of an action
     * @param string $action The action
     * @param string $object The receiver of the action
     * @param int    $mode   Maps the flash mesage key to use
     */
    public function setFlashMessage($action, $object, $mode = null)
    {
        $flashKeys = [
            0 => ['success' => 'alert-success', 'fail' => 'alert-danger'],
            1 => ['success' => 'message', 'fail' => 'message'],
        ];
        if ($this->result === true) {
            Session::flash($flashKeys[(int) $mode]['success'], "$action $object successful!");
        } else {
            Session::flash($flashKeys[(int) $mode]['fail'], "$action $object failed.");
        }
    }

    /**
     * combine getting the result and setFlashMessage
     * @param  mixed $result Result from a Service call
     * @return void
     */
    public function process($result, $action, $object)
    {
        $this->result = $result;
        $this->setFlashMessage($action, $object);
    }
}
