<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Rush\Modules\ClassManagement\Services\ClassManagementService;
use Rush\Modules\ClassManagement\Services\PackageService;
use Rush\Modules\ClassManagement\Services\ClassScheduleService;
use Rush\Modules\ClassManagement\Services\ClassListService;
use Rush\Modules\ClassManagement\Services\CLassScheduleReservationService;
use Rush\Modules\Merchant\Services\MerchantService;

use Rush\Modules\ClassManagement\Repositories\ClassRepository;
use Rush\Modules\ClassManagement\Repositories\InstructorRepository;

class ClassManagementController extends Controller
{
    protected $merchantService;
    protected $packageService;
    protected $classScheduleSerivce;
    protected $classListService;
    protected $classScheduleReservationService;

    public function __construct(MerchantService $merchantService,
        PackageService $packageService,
        ClassScheduleService $classScheduleSerivce,
        ClassListService $classListService,
        CLassScheduleReservationService $classScheduleReservationService)
    {
        $this->merchantService = $merchantService;
        $this->packageService = $packageService;
        $this->classScheduleSerivce = $classScheduleSerivce;
        $this->classListService = $classListService;
        $this->classScheduleReservationService = $classScheduleReservationService;
    }

    public function index(Request $request)
    {
        $merchant_id = $request->session()->get('user_id');
        $merchant = $this->merchantService->getById($merchant_id);
        $request->request->add(['merchant_id' => $merchant_id]);

        $cm_instructor = new ClassManagementService(new InstructorRepository);
        $cm_class = new ClassManagementService(new ClassRepository);

        $data['packages'] = $this->packageService->getAll($merchant_id);
        $data['intructors'] = $cm_instructor->getAll($merchant_id);
        $data['intructors_with_branch_only'] = $cm_instructor->getAllWithBranch($merchant_id);
        $data['classes'] = $cm_class->getAll($merchant_id);
        $data['classes_with_branch'] = $cm_class->getAllWithBranch($merchant_id);
        $data['class_schedules'] = $this->classScheduleSerivce->getAll($merchant_id);
        $data['class_lists'] = $this->classListService->generateClassList($request);

        if(isset($request->start_date_rage) || isset($request->end_date_rage))
        {
            $data['start_date_rage'] = $request->start_date_rage;
            $data['end_date_rage'] = $request->end_date_rage;
        }

        $data['user_type'] = $request->session()->get( 'user_type' );
        $data['branches'] = $merchant->branches()->where('is_deleted',0)->get()->toArray();
        
        if($data['user_type'] == 3){
            $data['employee'] = $merchant->employees()->where('userId', $request->session()->get('bcu_id'))->first();

            $data['class_lists'] = $data['class_lists']->filter(function ($value, $key) use ($data) {
                return $value->schedule->branch_id == $data['employee']->branchId;
            });

            $data['branches'] = $merchant->branches()->where('id', $data['employee']->branchId)->get()->toArray();
        }
        
        $data['login_name'] = $request->session()->get('login_name');
        
        $data['header_text'] = 'Class Management';
        $data['merchant'] = $merchant;

        return view('class.index', $data);
    }

    public function storePackage(Request $request)
    {
        $merchant_id = $request->session()->get('user_id');
        $request->request->add(['merchant_id' => $merchant_id]);

        $this->packageService->save($request);

        return redirect('class/package#package');
    }

    public function deletePackage(Request $request)
    {
        $packge_id = $request->package_id;

        $this->packageService->delete($packge_id);

        return redirect('class/package#package');
    }

    public function publishStamp(Request $request)
    {
        $merchant_id = $request->session()->get('user_id');
        $request->request->add(['merchant_id' => $merchant_id]);

        $this->packageService->savePackageStamps($request);

        return redirect('class/package#package');
    }

    public function getPackageStamp(Request $request)
    {
        $package_id = $request->package_id;
        $stamp_no = $request->stamp_no;
        
        $stamp = $this->packageService->getPackageStamp($package_id, $stamp_no);

        return response()->json($stamp);
    }

    public function storeClass(Request $request)
    {
        $merchant_id = $request->session()->get('user_id');
        $request->request->add(['merchant_id' => $merchant_id]);
        $cm_class = new ClassManagementService(new ClassRepository);

        $cm_class->save($request);

        return redirect()->back();
    }

    public function deleteClass(Request $request)
    {
        $class_id = $request->class_id;
        $cm_class = new ClassManagementService(new ClassRepository);
        
        $cm_class->delete($class_id);

        return redirect()->back();
    }

    public function storeInstructor(Request $request)
    {
        $merchant_id = $request->session()->get('user_id');
        $request->request->add(['merchant_id' => $merchant_id]);
        $cm_instructor = new ClassManagementService(new InstructorRepository);

        $cm_instructor->save($request);

        return redirect()->back();
    }

    public function deleteInstructor(Request $request)
    {
        $instructor_id = $request->instructor_id;
        $cm_instructor = new ClassManagementService(new InstructorRepository);
        
        $cm_instructor->delete($instructor_id);

        return redirect()->back();
    }

    public function getClassInstructorAjax(Request $request)
    {
        $class_id = $request->class_id;
        $cm_class = new ClassManagementService(new ClassRepository);
        
        $instructors = $cm_class->get($class_id)->instructors;

        return response()->json($instructors);
    }

    public function storeClassSchedule(Request $request)
    {
        $merchant_id = $request->session()->get('user_id');
        $request->request->add(['merchant_id' => $merchant_id]);

        $this->classScheduleSerivce->save($request);

        return redirect('class/classsched#classlists');
    }

    public function deleteClassSchedule(Request $request)
    {
        $schedule_id = $request->schedule_id;
        
        $this->classScheduleSerivce->delete($schedule_id);

        return redirect('class/classsched#classsched');
    }

    public function validateClassScheduleList(Request $request)
    {
        $merchant_id = $request->session()->get('user_id');
        $request->request->add(['merchant_id' => $merchant_id]);
        
        $schedules = $this->classScheduleSerivce->validateClassScheduleList($request);

        return response()->json($schedules);
    }

    public function updateClassListSchedule(Request $request)
    {
        $merchant_id = $request->session()->get('user_id');
        $request->request->add(['merchant_id' => $merchant_id]);

        $this->classListService->updateClassListSchedule($request);

        return redirect('class/classlist#classlists');
    }

    public function cancelClassListSchedule(Request $request)
    {
        $merchant_id = $request->session()->get('user_id');
        $request->request->add(['merchant_id' => $merchant_id]);

        $this->classListService->cancelClassListSchedule($request);

        return redirect('class/classlist#classlists');
    }

    public function registerCustomerToClass(Request $request)
    {
        $merchant_id = $request->session()->get('user_id');
        $response_data = $this->classScheduleReservationService->registerCustomerToClass($merchant_id, $request);

        return response()->json($response_data);
    }

    public function getClassListPageData(Request $request)
    {
        $class_list = $this->classListService->getClassListById($request->class_list_id);

        $response_data['class_name'] = $class_list->schedule->classes->name;

        $response_data['header_text'] = $class_list->start_date_time->format('l') . ', ';
        $response_data['header_text'] .= $class_list->start_date_time->format('F d, Y') . ' ' . $class_list->start_time_label . ', ';
        $response_data['header_text'] .= $class_list->schedule->classes->name . ', ';
        $response_data['header_text'] .= $class_list->instructor->name . ', ';
        $response_data['header_text'] .= $class_list->schedule->branch->name;

        $response_data['booked'] = $class_list->reservations->count();
        $response_data['capacity'] = $class_list->attendance_capacity;
        $response_data['no_show'] = $class_list->reservations()->where('showed_up', 0)->count();
        $response_data['showed_up'] = $class_list->reservations()->where('showed_up', 1)->count();

        return response()->json($response_data);
    }

    public function getAllCustomerFromClass(Request $request)
    {
        $response_data = $this->classScheduleReservationService->getAllCustomerFromClass($request->class_list_id);

        return response()->json($response_data);
    }

    public function deleteReservation(Request $request)
    {
        $response_data = $this->classScheduleReservationService->deleteReservation($request->reservation_id);

        return response()->json($response_data);
    }

    public function confirmAttendanceReservation(Request $request)
    {
        $response_data = $this->classScheduleReservationService->confirmAttendanceReservation($request->reservation_id, $request->toggle);

        return response()->json($response_data);
    }

    public function getInstructorOnGoingClassScheduleList(Request $request)
    {
        $response_data = $this->classListService->getInstructorOnGoingClassScheduleList($request);

        return response()->json($response_data);
    }

    public function getClassOnGoingClassScheduleList(Request $request)
    {
        $response_data = $this->classListService->getClassOnGoingClassScheduleList($request);

        return response()->json($response_data);
    }

    public function getMultibookFormDates(Request $request)
    {
        $response_data = $this->classListService->getMultibookFormDates($request);

        return response()->json($response_data);
    }

    public function multipleBookCustomerToClass(Request $request)
    {
        $merchant_id = $request->session()->get('user_id');
        $response_data = $this->classScheduleReservationService->multipleBookCustomerToClass($merchant_id, $request);

        return response()->json($response_data);
    }
}
