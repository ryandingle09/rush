<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Rush\Modules\Admin\Services\AdminService;

class AuthenticateAdmin
{
    
    protected $adminService;

    public function __construct(AdminService $adminService)
    {
      $this->adminService = $adminService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
      $is_admin = $request->session()->get('is_admin') ? $request->session()->get('is_admin') : NULL;
      if ( $is_admin == TRUE ) {
        $admin_type = $request->session()->get('admin_type');
        if ( $admin_type == 1 ) {
          return $next($request);
        } else {
          $admin_id = $request->session()->get('admin_id');
          $route_name = $request->route()->getName();
          $moduleNames = $this->adminService->getAdminModulesRoute( $admin_id, true);
          if ( in_array( $route_name, $moduleNames ) ) {
            return $next($request);
          } else {
            return redirect( url('admin') );
          }
        }
      } else {
        return redirect( url('dashboard') );
      }
    }
}
