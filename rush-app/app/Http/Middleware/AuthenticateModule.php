<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Rush\Modules\Merchant\Services\MerchantService;

class AuthenticateModule
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */

    protected $merchantService;

    public function __construct(MerchantService $merchantService)
    {
        $this->merchantService = $merchantService;
    }

    public function handle($request, Closure $next, $guard = null)
    {
      if ( $request->session()->get('user_type') == 1 ) {
        $merchant_id = $request->session()->get('user_id');
        $route_name = $request->route()->getName();
        $moduleNames = $this->merchantService->getMerchantModulesRoute( $merchant_id, true);
        if ( in_array( $route_name, $moduleNames ) ) {
            return $next($request);
        } else {
            return redirect( url('dashboard') );
        }
      } else if ( $request->session()->get('user_type') == 2 ) {
        $subuser_id = $request->session()->get('subuser_id');
        $route_name = $request->route()->getName();
        $moduleNames = $this->merchantService->getUserModulesRoute( $subuser_id, true );
        if ( in_array( $route_name, $moduleNames ) ) {
            return $next($request);
        } else {
            return redirect( url('dashboard') );
        }
      } else {
        $merchant_id = $request->session()->get('user_id');
        $route_name = $request->route()->getName();
        $moduleNames = $this->merchantService->getMerchantBcuModulesRoute( $merchant_id, true );
        if ( in_array( $route_name, $moduleNames ) ) {
            return $next($request);
        } else {
            return redirect( url('dashboard') );
        }
      }
    }
}
