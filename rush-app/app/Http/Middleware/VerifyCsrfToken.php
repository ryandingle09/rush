<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'alert/*',
        'paynamics/payment-listener',
        'redemption/delete',
        'request-demo',
        'rush-demo',
        'prelogin',
        'login',
        'presignup',
        'select-package',
        'register',
        'preforgot',
        'forgot-password',
        'request-demo/basic',
        'request-demo/pro',
    ];
}
