<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthenticateCms
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ( !session('login_name') ) {
            return redirect( url('../') );
        }

        $quicksetup_url = [ 'dashboard', 'changepass', 'quicksetup/setupProgram', 'quicksetup/saveMerchantAppDesign', 'quicksetup/saveCustomerAppDesign', 'quicksetup/saveApplicationDetails'];
        if( !in_array( $request->path(), $quicksetup_url ) ) 
        {
            if ( session('forceQuickSetup') ) {
                return redirect( url('dashboard') );
            }
        }
        
        return $next($request);
    }
}
