<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Rush\Modules\Design\Services\DesignService;
use Rush\Modules\Merchant\Services\MerchantService;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(DesignService $designService, MerchantService $merchantService)
    {
        view()->composer(['menu.punchcard', 'menu.loyalty'], function($view) use ($designService){
            $request = app(\Illuminate\Http\Request::class);
            $merchant_id = (int) $request->session()->get('user_id');
            if($merchant_id){
                $merchant_logo =  $designService->getSavedMerchantApplication($merchant_id)['logo'];

                $view->merchant_logo = $merchant_logo;
            }
        });

        view()->composer('layouts.top', function($view) use ($merchantService){
            $request = app(\Illuminate\Http\Request::class);
            $merchant_id = (int) $request->session()->get('user_id');
            if($merchant_id){
                $merchant = $merchantService->getById($merchant_id);
                $program_name = $merchant->settings['program_name'];

                $view->program_name = $program_name;
            }
        });

        Validator::extend('alpha_num_space', function($attribute, $value, $parameters, $validator) {
            if(preg_match("/^[a-z\d\-.,@`(<>\/)’;?:!'+%&\s]+$/i",$value)){

                return true;
            }
                return false;
        });

        Validator::extend('max_content', function($attribute, $value, $parameters, $validator) {

            if(strlen($value) > $parameters[0]){
                return false;
            }
            return true;    
        });

        Validator::extend('max_app_lbl', function($attribute, $value, $parameters, $validator) {

            if(strlen($value) > $parameters[0]){
                return false;
            }
            return true;    
        });
            
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
