<?php

return [
    'Achievements',
    'Admin',
    'Alert',
    'Analytics',
    'Attendance',
    'Billing',
    'Broadcasting',
    'ClassManagement',
    'Customer',
    'Design',
    'Email',
    'Helpers',
    'Merchant',
    'Posts',
    'Push',
    'Sms',
    'OnboardingCms'
];
