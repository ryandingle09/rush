@extends('layouts.app')

@section('view')
<div class="container">
    <div id="content" class="design spacing">
        @if ( $message )
            <div class='alert alert-success' role='alert'><p>{{$message}}</p></div><style>.promo-list .new-punchcard{margin-bottom:-225px;}</style>
        @endif

        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger"> {{ $error }} </div>
            @endforeach
        @endif
        
        <div class="row" id="merchantAppDesign">
            <div class="mobileTablet col-md-12 clearfix">
                <button class="btn btn-default" id="btnTablet">Merchant App</button>
                <button class="btn btn-primary" id="btnMobile">Customer App</button>
                <button class="btn btn-primary active" id="btnStampIcon">Stamp Icon</button>
            </div>
            <form action="{{ url('design/saveStampIcon') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
                <div class="stampIconTab">
                    <div class="col-xs-12 clearfix upStampC">
                        <h5 class="withIco">Upload Stamp Design</h5>
                        <span class="sp">Colored</span>
                        <span id="appLogoMsg" class="error-msg hide">Image does not meet the image size requirement. Please check again.<br></span>
                        <img src="{{ $merchant_app_stamp }}" class="imgPrev" alt="">
                        <div class="pull-left">
                        <span class="btn btn-default btn-file">
                        Choose File <input type="file" name="stamp" class="fuImage" ew="200" eh="200">
                        </span>
                            <i>Required Size: 200px x 200px</i>
                        </div>
                    </div>
                    <div class="col-xs-12 clearfix upStampG">
                        <span class="sp">Grayscale</span>
                        <img src="{{ $merchant_app_stamp }}" class="imgPrev stamp-grayscale" alt="">
                    </div>
                    <div class="col-xs-12">
                        <button class="btn btn-primary saveDesign">Save Design</button>
                    </div>
                </div>
			</form>
            <form action="{{ url('design/saveCustomerAppDesign') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
                <div class="mobileTab" id="mobileAppDesign">
                    <div class="col-md-4">
                        <div class="row left">
                            <div class="col-xs-12 clearfix merchName">
                                <h5 class="withIco"><i class="number">1</i>Merchant Name</h5>
                                <input type="text" class="form-control" name="merchantName" id="merchantName" value="{{ $customer_app_name }}">
                            </div>
                            <div class="col-xs-12 clearfix uploadLogo">
                                <h5 class="withIco"><i class="number">2</i>Upload Logo</h5>
                                <span class="error-msg hide">Image does not meet the image size requirement. Please check again.<br></span>
                                <img src="{{ $customer_app_logo }}" class="imgPrev" alt="">
                                <div class="pull-left">
                                    <span class="btn btn-default btn-file">
                                    Choose File <input type="file" name="logo" class="fuImage" ew="200" eh="200">
                                    </span>
                                        <i>Required image size: 200px x 200px</i>
                                </div>
                            </div>
                            <div class="col-xs-12 clearfix addBackImage">
                                <h5 class="withIco"><i class="number">3</i>Add Background Image</h5>
                                <span class="error-msg hide">Image does not meet the image size requirement. Please check again.<br></span>
                                <img src="{{ $customer_app_background }}" class="imgPrev" alt="">
                                <div class="pull-left">
                                    <span class="btn btn-default btn-file">
                                    Choose File <input type="file" name="background" class="fuImage" ew="640" eh="1136">
                                    </span>
                                        <i>Required image size: 640px x 1136px</i>
                                </div>
                            </div>
                            <div class="col-xs-12 clearfix choosePalete">
                                <h5 class="withIco"><i class="number">4</i>Choose Color Palette</h5>
                                <div class="row">
                                    <div class="col-xs-12">
                                        @include($merchantAppPalette)
                                        <button class="btn btn-default savePalette" id="saveCustomerAppPalette" type="button">Save Palette</button>
                                    </div>
                                    <div class="customPaletteBox">
                                        <div class="col-xs-6">
                                            <i>Theme</i>
                                            <input type='hidden' id="pOverlay" name="overlayColor" value="{{ $customer_app_overlayColor }}"/>
                                            <span class="hex" id="cThemeVal">#00A8A4</span>
                                        </div>
                                        <div class="col-xs-6">
                                            <i>Text</i>
                                            <input type='hidden' id="pText" name="textColor" value="{{ $customer_app_textColor }}"/>
                                            <span class="hex" id="cTextVal">#00E6C5</span>
                                        </div>
                                        <div class="col-xs-6">
                                            <i>Buttons</i>
                                            <input type='hidden' id="pButtons" name="buttonsColor" value="{{ $customer_app_buttonsColor }}"/>
                                            <span class="hex" id="cButtonsVal">#8BFF6F</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <button class="btn btn-primary saveDesign">Save Design</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 right">
                        <div class="mobileFrame">
                            <div class="col-xs-6">
                                <div class="screen1">
                                    <div class="customMobileBgImg">
                                        <div class="customMobileBgColor">
                                            <div class="inside"><img src="{{MigrationHelper::getBaseUrl()}}assets/images/fidelityLogo.png" alt="" class="customLogo"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="screen2">
                                    <div class="screen2box">
                                        <div class="topPane"><span id="topPaneLabel"></span></div>
                                        <div class="qrcodeWrap">
                                        </div><!--qrcodeWrap-->
                                        <div>
                                            <span class="pts_value">1286<span class="pts"> PTS</span></span>
                                            <p class="pts_date">As of Nov 26, 2015</p>
                                        </div>
                                        <button class="redeem btn" type="button">REDEEM</button>
                                    </div>
                                </div>
                            </div>
                        </div><!--mobileFrame-->
                    </div><!--right-->
                </div><!--mobileTab-->
            </form>
			<form action="{{ url('design/saveMerchantAppDesign') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
                <div class="tabletTab">
                    <div class="col-md-4">
                        <div class="row left">
                            <div class="col-xs-12 clearfix tabletUploadLogo">
                                <h5 class="withIco"><i class="number">1</i>Upload Logo</h5>
                                <div class="row">
                                    <span id="appLogoMsg" class="error-msg hide">Image does not meet the image size requirement. Please check again.<br></span>
                                    <div class="col-md-4">
                                        <img src="{{ $merchant_app_logo }}" class="imgPrev" alt="">
                                    </div>
                                    <div class="col-md-8">
                                        <span class="btn btn-default btn-file">
                                        Choose File <input type="file" name="logo" class="fuImage" ew="200" eh="200">
                                        </span>
                                        <i>Required Size: 200px x 200px</i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 clearfix tabletAddBackImage">
                                <h5 class="withIco"><i class="number">2</i>Add Background Image</h5>
                                <div class="row">
                                    <span id="appLogoMsg" class="error-msg hide">Image does not meet the image size requirement. Please check again.<br></span>
                                    <div class="col-md-4">
                                        <img src="{{ $merchant_app_background }}" class="imgPrev" alt="">
                                    </div>
                                    <div class="col-md-8">
                                        <span class="btn btn-default btn-file">
                                            Choose File <input type="file" name="background" class="fuImage" ew="1136" eh="640">
                                        </span>
                                        <i>Required Size: 1136px x 640px</i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 clearfix tabletChoosePalete">
                                <h5 class="withIco"><i class="number">3</i>Choose Color Palette</h5>
                                <div class="row">
                                    <div class="col-xs-12">
                                        @include($merchantAppPalette)
                                        <button class="btn btn-default savePalette" id="saveMerchantAppPalette" type="button">Save Palette</button>
                                    </div>
                                    <div class="customPaletteBox">
                                        <div class="col-xs-6">
                                            <i>Text</i>
                                            <input type='text' id="tpText" name="textColor" value="{{ $merchant_app_textColor }}"/>
                                            <span class="hex" id="mTextVal">#00E6C5</span>
                                        </div>
                                        <div class="col-xs-6">
                                            <i>Buttons</i>
                                            <input type='text' id="tpButtons" name="buttonsColor" value="{{ $merchant_app_buttonsColor}}"/>
                                            <span class="hex" id="mButtonsVal">#8BFF6F</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <button class="btn btn-primary saveDesign">Save Design</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 right">
                        <div class="tabletFrame">
                            <div class="tabletLayout">
                                <div class="customTablet">
                                    <img src="{{MigrationHelper::getBaseUrl()}}assets/images/fidelityLogo.png" alt="" class="tabletCustomLogo">
                                    <button class="btn btn-default customButton">Login</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</form>
        </div><!--row-->
    </div><!--/content-->
</div> <!-- /container -->
@endsection