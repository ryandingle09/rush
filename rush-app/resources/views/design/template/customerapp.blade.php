<div class="dropdown presetPalette">
    <button class="btn btn-default dropdown-toggle" type="button" id="prePalette" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        <span class="p1"></span><span class="p2"></span><span class="p3"></span>
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu prePaletteMnu" aria-labelledby="prePalette" id="customerAppPalette">
        <?php foreach($palettes as $palette) { ?>
        <li>
            <a href="" onclick="return false">
                <i style="background-color: <?php echo $palette['r']; ?>"></i>
                <i style="background-color: <?php echo $palette['g']; ?>"></i>
                <i style="background-color: <?php echo $palette['b']; ?>"></i>
            </a>
            <span><?php echo $palette['name']; ?></span>
        </li>
        <?php } ?>
    </ul>
</div>