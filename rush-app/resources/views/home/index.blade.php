@extends('layouts.app')

@section('view')
<div class="container">
   <div id="content" class="home spacing">
      @if ( $merchant->status == "Trial" )
      <div class="row">
         <div class="col-md-12">
            Your trial ends on {{ Carbon\Carbon::parse( $merchant->trial_end )->format('F j, Y') }}.
         </div>
      </div>
      @endif
   <div class="row">
   	@foreach( $modules as $module_item )
		<div class="module">
			<a class="module-icon icon-{{ $module_item->code }}" href="{{ url( $module_item->url ) }}" title="{{ $module_item->name }}">
			<span class="module-text">{{ $module_item->name }}</span>
			</a>
		</div>
   	@endforeach
   </div>
   </div>
</div> <!-- /container -->

@if( isset( $member_tier) && $member_tier['alert'] )
<div id="member_tier_alert" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            {{-- <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span> --}}
            <div class="modal-header">Member Tier Alert</div>
            <div class="modal-body">
                <form method="POST" action="{{ url('dashboard/member-tier') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-12">
                           <p>You will soon exceed the maximum number of member registrations in your current RUSH subscription. You will automatically be upgraded to the next subscription tier for the following month. If you wish to remain in your current subscription plan, please click "STAY". Please note that your current subscription will not be able to accommodate additional member registrations exceeding {{ $member_tier['customer_max_number'] }}.</p>
                           <br />
                        </div>
                        <div class="col-md-12">
                           <button class="btn btn-primary pull-right">STAY</button>
                           <button class="btn btn-default" data-dismiss="modal" >CONTINUE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endif
@endsection


@section('footerPlaceholder')
@if( isset( $member_tier) && $member_tier['alert'] )
   <script type="text/javascript">
      $(document).ready( function() {
         $("#member_tier_alert").modal('show');
      });
   </script>
@endif
@endsection