@extends('layouts.app')
@section('view')
<div class="container">
    <form action="{{ url('punchcard/add') }}" method="POST" role="form" data-toggle="validator" enctype="multipart/form-data" id="savePromoMechanics">
    {{ csrf_field() }}
      <div id="content" class="promos spacing">
        <section>
          <h4 class="text-center">Add New Punchcard</h4>
          @if ( $errors )
          @foreach( $errors as $error )
            <div class='alert alert-warning' role='alert'><p>{{ $error }}</p></div>
          @endforeach
          @endif
          <h5 class="withIco">STEP 1: General Mechanics</h5>
          <div class="form-group">
            <label for="promoTitle">Promo Title</label>
            <input type="text" class="form-control" name="promoTitle" id="promoTitle" placeholder="Enter promo title" maxlength="100" value="{{ old('promoTitle', null) }}" required>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label>Duration of Promo</label>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class='input-group date durationStart datetimepicker' id='promoDurationStart'>
                      <input type='text' class="form-control" name="durationStart" id="durationStart" placeholder="Start"  data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="{{ old('durationStart', null) }}" required>
                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                    <div class="help-block with-errors"></div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class='input-group date durationEnd datetimepicker' id='promoDurationEnd'>
                      <input type='text' class="form-control" name="durationEnd" id="durationEnd" placeholder="End" data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="{{ old('durationEnd', null) }}"  required>
                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                    <div class="help-block with-errors"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Redemption Period</label>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <div class='input-group date redemptionDurationStart datetimepicker' id="promoRedemptionDurationStart">
                        <input type='text' class="form-control" name="redemptionDurationStart" id="redemptionDurationStart" placeholder="Start" data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="{{ old('redemptionDurationStart', null) }}"  required>
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                      </div>
                      <div class="help-block with-errors"></div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <div class='input-group date redemptionDurationEnd datetimepicker' id="promoRedemptionDurationEnd">
                        <input type='text' class="form-control" name="redemptionDurationEnd" id="redemptionDurationEnd" placeholder="End" data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="{{ old('redemptionDurationEnd', null) }}"  required>
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                      </div>
                      <div class="help-block with-errors"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section>
          <h5>STEP 2: Stamp Conversion</h5>
          <div class="stamp-conversion">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <div class="clearfix">
                    <div class="stamp-no">
                      <label><strong>1</strong> Stamp = Php </label>
                    </div>
                    <div class="stamp-amount">
                      <div class="form-group">
                        <input class="form-control" name="amount" placeholder="0.00" data-error="Invalid amount" pattern="^[-+]?[0-9]*\.?[0-9]+$" value="{{ old('amount', null) }}" required>
                        <div class="help-block with-errors"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <label for="stamps">Click on stamp below to add reward. </i> </label>
                <div class="" id="stamps">
                  <ul class="">
                    @for ($i = 1; $i <= 3; $i++)
                        <li>
                            <a href="#stampModal{{ $i }}" data-toggle="modal" data-target="#stampModal{{ $i }}"><?php echo $i; ?></a>
                            <div class="modal fade punchcardModal" id="stampModal{{ $i }}" tabindex="-1" role="dialog">
                              <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                          <h9 class="modal-title" id="stampModal{{ $i }}Label">STAMP {{$i}}</h9>
                                      </div>
                                      <div class="modal-body">
                                        <div class="col-left">
                                          <div class="form-group">
                                              <label>Reward Name</label>
                                              <textarea class="stamp_reward_description_{{ $i }} form-control modal-field" name="stamp_reward_description_{{ $i }}" maxlength="75"></textarea>
                                          </div>
                                          <div class="form-group">
                                                <label>Reward Description</label>
                                                <textarea maxlength="300" class="stamp_reward_details_{{ $i }} form-control modal-field" name="stamp_reward_details_{{ $i }}" maxlength="1000" style="margin: 0;"></textarea>
                                                <span style="font-size: 12px;color:#a3a3a3;"><i>Maximum of 300 characters</i></span>
                                            </div>
                                            <div class="form-group branches">
                                              <label class="input-label">Branches</label>
                                              <label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0" style="float: right;height:0;cursor:pointer;">
                                                  <input type="checkbox" class="custom-control-input select-all" data-branches="{{ $i }}">
                                                  <span class="custom-control-indicator"></span>
                                                  <span class="custom-control-description">Select All</span>
                                              </label>
                                              <div class="branch-check-wrap branches_{{ $i }}">
                                                  @if ( $branches )
                                                  @foreach( $branches as $branch)
                                                  <div class="form-check">
                                                      <input type="checkbox" value="{{ $branch['id'] }}" name="stamp_reward_branch_{{ $i }}[]" class="checkbox options" id="branch_{{ $branch['id'] }}"/>
                                                      <label class="form-check-label branch-label" for="branch_{{ $branch['id'] }}"> {{ $branch['branchName']}}
                                                          <i class="fa fa-check" aria-hidden="true" style="display: none;"></i>
                                                      </label>
                                                  </div>
                                                  @endforeach
                                                  @endif
                                              </div>
                                          </div>
                                        </div>
                                        <div class="col-right">
                                          <div class='alert alert-danger hide'>
                                              <p>Image does not meet the image size requirement. Please check again.</p>
                                          </div>
                                          <label class="input-label">Image</label>
                                          <span class="btn btn-success fileinput-button">
                                              <span>Click to add/change image <br /> <i>Required size is 350px x 200px</i></span>
                                              <input id="stamp_reward_image_{{ $i }}" type="file" name="stamp_reward_image_{{ $i }}">
                                          </span>
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                          <div class="row">
                                              <div class="col-md-6">
                                                  <button type="button" class="btn btn-default clearStamp" onclick="clearForm()">Clear</button>
                                              </div>
                                              <div class="col-md-6">
                                                  <button type="button" class="btn btn-primary saveStamp" >Save</button>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                        </li>
                        @endfor
                  </ul>
                </div>
                    {!! $addRemoveButton !!}
              </div>
            </div>
          </div>
        </section>
        <div class="form-group">
          <input type="hidden" name="status" value="1">
          <input type="hidden" name="rewards" value="[]">
          <button class="btn btn-warning saveButton savePromo" type="submit">Draft</button>
          <button class="btn btn-primary saveButton publishPromo" type="submit">Publish</button>
        </div>
      </div><!--/content-->
    </form>
</div> <!-- /container -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script> 
  function clearForm(){
    $(".modal-field").val('');
    $(".select-all, .options").prop('checked', false); 
    $("#file").val('');
  }
  
  $('.select-all').click(function(event) {    
    var _num = $(this).data('branches');
    if(this.checked) {
        // Iterate each checkbox
        $('.branches_' + _num + ' :checkbox').each(function() {
            this.checked = true;
            $(".branch-label").children('.fa-check').css("display","block");
        });
    }
    else {
      $('.branches_' + _num + ' :checkbox').each(function() {
            this.checked = false;
            $(".branch-label").children('.fa-check').css("display","none");
        });
    }
  });

  $(".options").change(function() {
      if(this.checked==false) {
           $(".select-all").prop('checked', false); 
      }
      var _check = $(this).siblings(".branch-label").children('.fa-check');
        if ( $(this).is(':checked') == true ) _check.css("display", "block");
        else _check.css("display","none");
  });
</script>
@endsection
