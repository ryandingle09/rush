@extends('layouts.app')
@include('citolaravel::menu.punchcard', ['serverUrl' => MigrationHelper::getServerUrl(), 'baseUrl' => MigrationHelper::getBaseUrl()])
@section('view')
<div id="rightcontent">
    <div class="header">
        <div class="row">
            <div class="col-xs-7">
                <h3>Dashboard</h3>
            </div>
            <div class="col-xs-5">
                <div class="howdy">

                    <!-- Split button -->
                    <div class="btn-group">
                        <!-- Large button group -->
                        <div class="btn-group">
                            <button class="btn btn-default btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="{{ MigrationHelper::getBaseUrl() }}assets/images/avatar.png" alt=""/> <span>Hi, {{ $login_name }}</span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                              <li><a href="{{ MigrationHelper::getBaseUrl() }}account">Change Password</a></li>
                              <li><a href="{{ MigrationHelper::getBaseUrl() }}logout">Logout</a></li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>


    </div>
    <div class="container">

        <div id="content" class="dashboard">
            <h4>Daily Reports: as of {{ $current_date }}</h4>
            <div class="row counts">
                <div class="col-sm-6">
                    <ul class="count">
                        <li>
                            <p>DAILY</BR>SIGNUPS</p>
                            <h3>{{ $count_daily_signups }}</h3>
                        </li>
                        <li>
                            <p>DAILY</BR>REDEEMED</p>
                            <h3>{{ $count_daily_redeemed }}</h3>
                        </li>
                        <li>
                            <p>DAILY</BR>TRANSACTIONS</p>
                            <h3>{{ $count_daily_transactions }}</h3>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-6"></div>
            </div>
            </BR>
            <h4>Monthly Reports: {{ $current_month_year }}</h4>
            <div class="row counts">
                <div class="col-sm-6">
                    <ul class="count">
                        <li>
                            <p>MONTHLY</BR>SIGNUPS</p>
                            <h3>{{ $count_monthly_signups }}</h3>
                        </li>
                        <li>
                            <p>MONTHLY</BR>REDEEMED</p>
                            <h3>{{ $count_monthly_redeemed }}</h3>
                        </li>
                        <li>
                            <p>MONTHLY</BR>TRANSACTIONS</p>
                            <h3>{{ $count_monthly_transactions }}</h3>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-6"></div>
            </div>
        </div><!--/content-->
    </div> <!-- /container -->
</div> <!--rightcontent-->
@endsection