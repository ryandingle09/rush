@section('headerPlaceholder')
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css"> -->
@endsection
@if($is_enable_product_module)
<button href="#addMechanicsPprod" id='addMechanicsPprodButton' class="btn btn-primary smsAdd" data-toggle="modal" onclick="return false">Add Product</button>
<table id="mechanicsPprodTable">
    <thead>
        <tr>
            <th>Item Code</th>
            <th>Item Name</th>
            <th>Price</th>
            <th>Branch</th>
            <th>Date Created</th>
            <th>Date Updated</th>
            <th>Updated By</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($products as $product)
        <tr data-product-id="{{ $product->id }}">
            <td data-code="{{ $product->code }}">{{ $product->code }}</td>
            <td data-name="{{ $product->name }}">{{ $product->name }}</td>
            <td data-amount="{{ $product->amount }}">{{ $product->amount }}</td>
            <td data-branch-ids="{{ $product->branch_ids }}">{{ $product->branches_name }}</td>
            <td>{{ $product->created_at }}</td>
            <td>{{ $product->updated_at }}</td>
            <td>{{ $product->user_last_updated->personName or " " }}</td>
            <td>
                <a href="#" data-action-url="{{MigrationHelper::getAppBaseUrl()}}punchcard/product/{{ $product->id }}" onclick="return false" class="delete delMechanicsPprod"><i class="fa fa-trash"></i>Delete</a>
                <a href="#" class="edit editMechanicsPprod"><i class="fa fa-pencil"></i>Edit</a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@endif

<!-- Mechanics per Product -->
<div id="addMechanicsPprod" class="modal fade mechanicsPprod" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Add Product</div>
            <div class="modal-body">
                <form action="{{MigrationHelper::getAppBaseUrl()}}punchcard/product" method="post">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <label for="prodCode">Product Code</label>
                            <input type="text" name="code" id="prodCode" class="form-control" required>
                        </div>
                        <div class="col-md-6">
                            <label for="prodName">Vehicle</label>
                            <input type="text" name="name" id="prodName" class="form-control" required>
                        </div>
                        <div class="col-md-6">
                            <label for="prodPrice">Price</label>
                            <input type="text" name="amount" id="prodPrice" class="form-control num-only" >
                        </div>
                        <div class="col-md-6">
                            <label for="punch_branches">Branches</label>
                            <select required class="selectpicker form-control" name="branch_ids[]" id="punch_branches" multiple title="Select Branches" data-actions-box="true">
                                @if(!empty($branches))
                                    @foreach($branches as $branch)
                                        <option value="{{ $branch['id'] }}">{{ $branch['branchName'] }}</option>
                                    @endforeach
                                @else
                                    <option>No Registered Branch</option>
                                @endif
                            </select>
                        </div>
                        <div class="prod_stamps_default">
                            <div class="prod_stamps_item">
                                <div class="col-md-6">
                                    <label for="stamp_name_1">Days</label>
                                    <input type="text" name="stamp[name][]" id="stamp_name_1" class="form-control" required>
                                </div>
                                <div class="col-md-5">
                                    <label for="stamp_number_1">Stamps</label>
                                    <input type="text" name="stamp[number][]" id="stamp_number_1" class="form-control num-only" required>
                                </div>
                            </div>
                            <div class="prod_add_stamps col-md-12">
                                <button type="button" class="add_prod_stamp">
                                    <i class="fa fa-plus"></i> Add
                                </button>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button type="submit" class="btn btn-primary pull-right">SAVE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="editMechanicsPprod" class="modal fade mechanicsPprod" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Edit Product</div>
            <div class="modal-body">
                <form action="{{MigrationHelper::getAppBaseUrl()}}punchcard/product" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    <input type="hidden" name="product_id">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="eprodCode">Product Code</label>
                            <input type="text" name="code" id="eprodCode" class="form-control" value="" required>
                        </div>
                        <div class="col-md-6">
                            <label for="eprodName">Vehicle</label>
                            <input type="text" name="name" id="eprodName" class="form-control" value="" required>
                        </div>
                        <div class="col-md-6">
                            <label for="eprodPrice">Price</label>
                            <input type="text" name="amount" id="eprodPrice" value="" class="form-control num-only" >
                        </div>
                        <div class="col-md-6">
                            <label for="epunch_branches">Branches</label>
                            <select required class="selectpicker form-control" name="branch_ids[]" id="epunch_branches" multiple title="Select Branches" data-actions-box="true">
                                @if(!empty($branches))
                                    @foreach($branches as $branch)
                                        <option value="{{ $branch['id'] }}">{{ $branch['branchName'] }}</option>
                                    @endforeach
                                @else
                                    <option>No Registered Branch</option>
                                @endif
                            </select>
                        </div>
                        <div class="prod_stamps_default">
                            <div class="prod_stamps_item">
                                <div class="col-md-6">
                                    <label for="stamp_name_1">Days</label>
                                    <input type="text" name="stamp[name][]" id="stamp_name_1" class="form-control" value="" required>
                                </div>
                                <div class="col-md-5">
                                    <label for="stamp_number_1">Stamps</label>
                                    <input type="text" name="stamp[number][]" id="stamp_number_1" class="form-control num-only" value="" required>
                                </div>
                            </div>
                            <div class="prod_add_stamps col-md-12">
                                <button type="button" class="add_prod_stamp">
                                    <i class="fa fa-plus"></i> Add
                                </button>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button type="submit" class="btn btn-primary pull-right">SAVE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="delMechanicsPprod" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Delete Product</div>
            <div class="modal-body">
                <p>Are you sure you want to delete this item?</p>
                <strong id="prodToDelete">#XF2145</strong>
                <form action="" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <br>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <button class="btn btn-default  pull-right" data-dismiss="modal">CANCEL</button>
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-primary">DELETE</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@section('footerPlaceholder')
@parent
<script type="text/javascript">

$(document).ready(function(){

    /* Data Table Specs */

    var emptyTableMessage = 'No matching records found';
    var excelExportButton = '<div class="text-center"><button type="button" class="btn btn-default exportButton">Export to Excel</button></div>';
    
    var mechanicsPprodTableSpecs = {
        'initComplete': function() {
            $('#mechanicsPprodTable').wrap(function(){
                return "<div class='bootstrap-table'></div>";
            });
            $("#mechanics-per-product .search input").unwrap("label");
        },
        'sDom': '<"fixed-table-toolbar"f><"excelButton hide"B><"customButton">rtl<"bottom"ip><"clear">',
        'buttons': [ {
            extend: 'excel' 
        }],
            'language': {
                'emptyTable': emptyTableMessage                        
        },
            'oLanguage': {
                'sSearch': ''
        },
        'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
        'bProcessing': true
    };

    // initialize code management datatable
    $.extend($.fn.dataTableExt.oStdClasses, {
        'sWrapper': 'bootstrap-table dataTables_wrapper',
        'sFilter': 'pull-right search',
        'sInfo': 'pagination-detail',
        'sLength': 'page-list'
    });

    $('#mechanicsPprodTable').DataTable(mechanicsPprodTableSpecs);

    $('#mechanicsPprodTable_wrapper input').attr({'type': 'text', 'placeholder': 'Search'}).addClass('form-control');

});

/* Add Rows on Days and Stamps */

$("#addMechanicsPprod").on("click", ".add_prod_stamp", function(){

    var modal_parent = "#" + $(this).closest(".modal").attr("id");

    var prod_stamps_count = $(modal_parent).find(".prod_stamps_default .prod_stamps_item").length;

    prod_stamps_count++;

    var stamp_item_component = '<div class="prod_stamps_item">\
        <div class="col-md-6">\
            <input type="text" name="stamp[name][]" id="prodDays_' + prod_stamps_count + '" class="form-control" required>\
        </div>\
        <div class="col-md-5">\
            <input type="text" name="stamp[number][]" id="prodStamps_' + prod_stamps_count + '" class="form-control num-only" required>\
        </div>\
        <span class="delete_stamp_row">\
            <i class="fa fa-close"></i>\
        </span>\
    </div>';

    var insert_here = $(modal_parent).find(".prod_stamps_default");

    $(stamp_item_component).appendTo(insert_here).insertBefore(modal_parent + " .prod_add_stamps");

});

$("#editMechanicsPprod").on("click", ".add_prod_stamp", function(){

    var modal_parent = "#" + $(this).closest(".modal").attr("id");

    var prod_stamps_count = $(modal_parent).find(".prod_stamps_default .prod_stamps_item").length;

    prod_stamps_count++;

    var stamp_item_component = '<div class="prod_stamps_item">\
        <div class="col-md-6">\
            <input type="text" name="stamp[name][]" id="eprodDays_' + prod_stamps_count + '" class="form-control" required>\
        </div>\
        <div class="col-md-5">\
            <input type="text" name="stamp[number][]" id="eprodStamps_' + prod_stamps_count + '" class="form-control" required>\
        </div>\
        <span class="delete_stamp_row">\
            <i class="fa fa-close"></i>\
        </span>\
    </div>';

    var insert_here = $(modal_parent).find(".prod_stamps_default");

    $(stamp_item_component).appendTo(insert_here).insertBefore(modal_parent + " .prod_add_stamps");

});

/* Delete Rows ON Days and Stamps */
$(".modal").on("click", ".delete_stamp_row", function(){

    var parent_modal = "#" + $(this).closest(".modal").attr("id");

    var parent_row = $(this).parent();

    parent_row.remove();

});

/* Trigger Modals for Edit and Delete */

$('#mechanicsPprodTable').on('click', '.editMechanicsPprod', function(e) {
    e.preventDefault();

    var loading_gif = $('#loading');

    var parent_tr = $(this).parents('tr');
    var product_id = parent_tr.attr('data-product-id');
    var target_modal = $('#editMechanicsPprod');

    var code = parent_tr.find('[data-code]').attr('data-code');
    var name = parent_tr.find('[data-name]').attr('data-name');
    var amount = parent_tr.find('[data-amount]').attr('data-amount');
    var branch_ids = parent_tr.find('[data-branch-ids]').attr('data-branch-ids').replace('[','').replace(']', '');
    var type = parent_tr.find('[data-type]').attr('data-type');

    target_modal.find("[name='product_id']").val(product_id);
    target_modal.find("[name='code']").val(code);
    target_modal.find("[name='name']").val(name);
    target_modal.find("[name='amount']").val(amount);
    target_modal.find("[name='type']").val(type);

    if(branch_ids === "0"){
        target_modal.find("[name='branch_ids[]']").selectpicker("selectAll");
    } else {
        target_modal.find("[name='branch_ids[]']").selectpicker('val', branch_ids.split(','));
    }

    loading_gif.removeClass('hidden');

    $.ajax({
        url : 'punchcard/product/getProductStamps',
        data : { product_id: product_id},
        method : 'get',
        success : function(response){
            var stamp_item_component = "";
            for(var i in response['data']){
                stamp_item_component += '<div class="prod_stamps_item">\
                    <div class="col-md-6">\
                        <input type="text" name="stamp[name][]" id="eprodDays_' + (i + 1) + '" value="' + response['data'][i]['stamp_name'] + '" class="form-control" required>\
                    </div>\
                    <div class="col-md-5">\
                        <input type="text" name="stamp[number][]" id="eprodStamps_' + (i + 1) + '" value="' + response['data'][i]['stamp_number'] + '" class="form-control" required>\
                    </div>\
                    <span class="delete_stamp_row">\
                        <i class="fa fa-close"></i>\
                    </span>\
                </div>';
            }

            target_modal.find('div.prod_stamps_item div').remove();
            target_modal.find('div.prod_stamps_item').append(stamp_item_component);
        },
        complete : function(){
            loading_gif.addClass('hidden');
            target_modal.modal('show');
        }
    });
});

$('#mechanicsPprodTable').on('click', '.delMechanicsPprod', function(e) {
    e.preventDefault();

    var parent_tr = $(this).parents('tr');
    var action_url = $(this).attr('data-action-url');
    var target_modal = $('#delMechanicsPprod');

    var code = parent_tr.find('[data-code]').attr('data-code');

    target_modal.find("form").attr('action', action_url);
    target_modal.find('#prodToDelete').text(code);
    target_modal.modal('show');
});

// Number Only
$('body').on('keypress', '.num-only', function (e) {
    //if the letter is not digit then sdisplay error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        return false;
    }
});

</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
@endsection