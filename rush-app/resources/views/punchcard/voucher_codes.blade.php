@extends('layouts.app')

@section('headerPlaceholder')
<!-- Voucher Management css -->
<link rel="stylesheet" href="../../page/voucher/css/style.css">
<link rel="stylesheet" href="../../page/voucher/css/sweetalert.css">
@endsection

@section('view')
<div class="container">
    <div id="content" class="voucherCodes">
        <div class="tab-content">
            <div id="voucherCodesArea">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if(session('message'))
                    <div class="alert alert-success">
                        <p>{{ session('message') }}</p>
                    </div>
                @endif

                @if(!empty(session('data')['insert_response']['inserted']))
                    <div class="alert alert-success">
                        <p>{{ session('data')['message'] }}</p>
                    </div>
                @endif

                <h4>View Voucher Codes</h4>
                <label>Voucher Name</label> {{ $voucher_data->name }} <br />
                <label>Branch</label> {{ $voucher_data->branch->branchName }} <br />
                <a href="#addVoucherCode" data-toggle="modal" onclick="return false" class="btn btn-primary">Add Voucher Code</a>
                <a href="#voucherCodeImport" id="voucherCodeImportButton" data-toggle="modal" data-id="{{$voucher_data->id}}" data-vc-name="{{ $voucher_data->name }}" data-vc-branch="{{ $voucher_data->branch->branchName }}" onclick="return false" class="btn btn-primary">Import Vouchers Code</a>
                <a href="#transferVoucherCode" data-toggle="modal" onclick="return false" class="btn btn-primary">Transfer Voucher Code</a>
                <table id="voucherCodesTable" class="table">
                    <thead>
                    <tr>
                        <th class="no-sort"><input type="checkbox" name="select_all"></th>
                        <th>Codes</th>
                        <th>Valid Until</th>
                        <th>Issued</th>
                        <th class="text-right no-sort">action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach( $voucher_codes as $voucher_code )
                            <tr data-id="{{ $voucher_code->id }}">
                                <td data-id="{{ $voucher_code->id }}"><input <?php echo ($voucher_code->issued == 'Issued') ? 'disabled="disabled" class="v_c_id disabled"' : 'class="v_c_id active"' ?> type="checkbox" name="v_c_id[]" value="{{ $voucher_code->id }}"></td>
                                <td data-code="{{ $voucher_code->code }}">{{ $voucher_code->code }}</td>
                                <td data-expiration="{{ $voucher_code->expiration }}">{{ $voucher_code->expiration }}</td>
                                <td>{{ $voucher_code->issued }}</td>
                                <td class="text-right">
                                    <a href="#" class="edit edit-voucher-code"><i class="fa fa-edit"></i> Edit</a>
                                    <a href="#" class="delete delete-voucher-code"><i class="fa fa-trash"></i> Delete</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
<div id="voucherCodeImport" class="modal fade voucher-modal" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Import Voucher Codes</div>
            <div class="modal-body">
                <form action="{{ url('punchcard/voucher/uploadcodes') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="voucher_id" id="upload_voucher_id" />
                    <div class="form-group row">
                        <div class="col-md-10"><label>Voucher Name</label> <span id="ivc-voucher-name" /></div>
                        <div class="col-md-10"><label>Branch</label> <span id="ivc-voucher-branch" /></div>
                        <div class="col-md-12">&nbsp;</div>
                        <div class="col-md-2">
                            <button type="button" class="browse-vc padding-lr-12">Browse</button>
                        </div>
                        <div class="col-md-10">
                            <input type="text" name="vc-filename" id="vc-filename" class="form-control filename-vc">
                            <input type="file" name="voucher-import" id="voucher-import" class="form-control file-vc" accept=".csv">
                        </div>
                        <div class="col-md-12">
                            <label><a href="{{ url('page/voucher/voucher-code-upload.csv')}}">**Sample CSV Format</a></label>
                            <br /><br />
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-default clear-voucher">Clear</button>
                            <button class="btn btn-primary pull-right">Import</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<div id="addVoucherCode" class="modal fade voucher-modal" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Add Voucher Code</div>
            <div class="modal-body">
                <form action="{{ url('punchcard/voucher-codes/' . $voucher_data->id) }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="code">Code: </label>
                            <input type="text" name="code" class="form-control" required>
                        </div>
                        <div class="col-md-12">
                            <label for="code">Valid Until <small>(Format: YYY-MM-DD hh:mm:ss)</small></label>
                            <input type="text" class="form-control" name="expiration">
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button class="btn btn-primary pull-right">Save</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<div id="editVoucherCode" class="modal fade voucher-modal" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Edit Voucher Code</div>
            <div class="modal-body">
                <form action="{{ url('punchcard/voucher-codes/' . $voucher_data->id) }}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="voucher_code_id">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="code">Code: </label>
                            <input type="text" name="code" class="form-control" required>
                        </div>
                        <div class="col-md-12">
                            <label for="code">Valid Until <small>(Format: YYY-MM-DD hh:mm:ss)</small></label>
                            <input type="text" class="form-control" name="expiration">
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button class="btn btn-primary pull-right">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="deleteVoucherCode" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Delete Voucher Code</div>
            <div class="modal-body">
                <p>Are you sure you want to delete this item?</p>
                <form action="{{ url('punchcard/voucher-codes/' . $voucher_data->id) }}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="voucher_code_id" id="voucher_code_id" />
                    <input type="hidden" name="_method" value="DELETE">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button class="btn btn-primary pull-right" id="delete_class_btn">DELETE</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<div id="transferVoucherCode" class="modal fade voucher-modal" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Transfer Codes</div>
            <div class="modal-body">
                <form class="form-transfer" action="{{ url('punchcard/voucher/transfer') }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="code">Transfer to: </label>
                            <select name="voucher" class="form-control">
                                <option value=""> Select Voucher </option>
                                @foreach( $vouchers as $voucher)
                                @if($voucher->id != request()->route('id'))
                                    <option value="{{ $voucher->id }}">{{ $voucher->id }} {{ $voucher->name }} </option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label for="code">Branch: </label>
                            <select name="branch" class="form-control">
                                <option value=""> Select Branch </option>
                            </select>
                        </div>
                        <input type="hidden" name="codes">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-default clear-voucher">Clear</button>
                            <button type="submit" class="btn btn-primary pull-right">Transfer</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
@endsection

@section('footerPlaceholder')
@parent
<script type="text/javascript" src="{{ url('page/voucher/js/scripts.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){

    var emptyTableMessage = 'No matching records found';
    var excelExportButton = '<div class="text-center"><button type="button" class="btn btn-default exportButton">Export to Excel</button></div>';
    // Client Code
    var clientCodeTableSpecs = {
        'initComplete': function() {
            $('#voucherCodesTable').wrap(function(){
                return "<div class='bootstrap-table'></div>";
            });
        },
        'sDom': '<"fixed-table-toolbar"f><"excelButton hide"B><"customButton">rtl<"bottom"ip><"clear">',
        'buttons': [ {
            extend: 'excel' 
        }],
            'language': {
                'emptyTable': emptyTableMessage                        
        },
            'oLanguage': {
                'sSearch': ''
        },
        'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
        'bProcessing': true,
        "columnDefs": [ {
              "targets": 'no-sort',
              "orderable": false,
        } ]
    };

    // initialize code management datatable
    $.extend($.fn.dataTableExt.oStdClasses, {
        'sWrapper': 'bootstrap-table dataTables_wrapper',
        'sFilter': 'pull-right search',
        'sInfo': 'pagination-detail',
        'sLength': 'page-list'
    });
    $('#voucherCodesTable').DataTable(clientCodeTableSpecs);
    if ($('#voucherCodesTable tbody tr.odd td').text() != emptyTableMessage) {
        $('#voucherCodesArea').append(excelExportButton);
    }
    $('#voucherCodesTable_wrapper #voucherCodesTable_filter input').attr({'type': 'text', 'placeholder': 'Search'}).addClass('form-control');

    $('.tab-content').on('click', '.exportButton', function(event) {
        $(this).parent().parent().find('.dataTables_wrapper .excelButton a').click();
    });

    $("#voucherCodeImportButton").on("click", function() {
        $('#upload_voucher_id').val( $(this).data('id'));
        $('#ivc-voucher-name').html( $(this).data('vc-name'));
        $('#ivc-voucher-branch').html( $(this).data('vc-branch'));
    });

    // Bulk Import Voucher Code
    $('.browse-vc').on('click', function(){
        $(this).closest('.form-group').find('.file-vc').click();
    });
    $('.file-vc').on('change', function(e){
        var _voucherCode = e.target.files[0].name;
        $(this).siblings('.filename-vc').val(_voucherCode);
    });

    $('.edit-voucher-code').on('click', function(){
        var parent_tr = $(this).parents('tr');
        var voucher_code_id = parent_tr.attr('data-id');
        var code = parent_tr.find('[data-code]').attr('data-code');
        var expiration = parent_tr.find('[data-expiration]').attr('data-expiration');
        var target_modal = $('#editVoucherCode');

        target_modal.find('input[name="voucher_code_id"]').val(voucher_code_id);
        target_modal.find('input[name="code"]').val(code);
        target_modal.find('input[name="expiration"]').val(expiration);

        target_modal.modal('show');
    });

    $('.delete-voucher-code').on('click', function(){
        var parent_tr = $(this).parents('tr');
        var voucher_code_id = parent_tr.attr('data-id');
        var target_modal = $('#deleteVoucherCode');

        target_modal.find('input[name="voucher_code_id"]').val(voucher_code_id);

        target_modal.modal('show');
    });

    $('[name="expiration"]').datetimepicker({
        format : "YYYY-MM-DD hh:mm:ss"
    });

    var insert_response_existed = '{!! json_encode(session('data')['insert_response']['existed']) !!}';

    if(insert_response_existed !== "null"){
        console.log(jQuery.parseJSON(insert_response_existed));
        swal("Error", jQuery.parseJSON(insert_response_existed).join(', ') + ' code already exists', "error");
    }
});

// Clear Fields
$('.clear-voucher').on('click', function(){
    $(this).closest('form').find('select').val('');
});

$(document).on('click','input[name="select_all"]', function(){
    var codes = $('input[name="v_c_id[]"]').serialize();
    if($(this).is(':checked')) {
        $('input.active').prop('checked', true);
        $('input[name="codes"]').val(codes);
    } else {
        $('input.active').prop('checked', false);
        $('input[name="codes"]').val('');
    }
});

$(document).on('click','input[name="v_c_id[]"]', function(){
    var codes = $('input[name="v_c_id[]"]').serialize();
    if($(this).is(':checked')) {
        $('input[name="codes"]').val(codes);
    } else {
        $('input[name="codes"]').val(codes);
    }
});

$(document).on('change','select[name="voucher"]', function(){
    var id = $(this).val();
    var codes = $('input[name="v_c_id[]"]').serialize();
    $('input[name="codes"]').val(codes);
    $('select[name="branch"]').html('<option value="">Loading branches...</option>');
    $.get('{{ url('punchcard/voucher-branches') }}', function(response){
        var branches = '<option value=""> Select Branch</option>';
        $.each(response, function(key, val){
            branches += '<option value="'+val.id+'">'+val.branchName+'</option>';
        });
        $('select[name="branch"]').html(branches);
    });
});
</script>
@endsection