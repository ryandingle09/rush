@if (count($errors) > 0)
    <div class="alert alert-danger spacing">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<button href="#addVoucherItem" class="btn btn-primary" data-toggle="modal" onclick="return false">Add Item</button>
<button href="#importVoucherItems" class="btn btn-primary" data-toggle="modal" onclick="return false">Import</button>
<table id="vouchersTable">
    <thead>
    <tr>
        <th data-sortable="true">ID</th>
        <th data-sortable="true">Voucher Name</th>
        <th>Description</th>
        <th data-sortable="true">Branch</th>
        <th>Issued</th>
        <th>Available</th>
        <!-- <th>Threshold</th> -->
        <th data-sortable="true">Status</th>
        <th class="action">Action</th>
        <th>Codes</th>
    </tr>
    </thead>
    @if ( $vouchers )
    <tbody>
    @foreach( $vouchers as $voucher)
        <tr>
            <td>{{ $voucher->id }}</td>
            <td>{{ $voucher->name }}</td>
            <td>{{ $voucher->description }}</td>
            <td>{{ $voucher->branch->branchName }}</td>
            <td class="text-center">{{ $voucher->issued }}</td>
            <td class="text-center">{{ $voucher->available }}</td>
            <!-- <td class="text-center">{{ $voucher->threshold }}</td> -->
            <td>{{ $voucher->enabled_label }}</td>
            <td>
                <a href="#editVoucherItem" id="editVoucherItemButton" data-toggle="modal" onclick="return false" class="edit" data-id="{{$voucher->id}}" data-name="{{ $voucher->name }}" data-desc="{{ $voucher->description }}" data-threshold="{{ $voucher->threshold }}" data-discount="{{ $voucher->discount }}" data-discount_type="{{ $voucher->discount_type }}" data-voucher_image="{{ $voucher->voucher_image }}"><i class="fa fa-edit"></i> Edit</a>
                <a href="#deleteVoucherItem" id="deleteVoucherItemButton" data-toggle="modal" onclick="return false" class="delete" data-id="{{$voucher->id}}"><i class="fa fa-trash"></i> Delete</a>
            </td>
            <td>
                <a href="#verifyAccountModal" data-toggle="modal" data-url="{{ url('punchcard/voucher-codes/' . $voucher->id ) }}" id="voucherCodesViewButton" class="import"><i class="fa fa-eye"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
    @endif
</table>

@section('catalog_modals')
@parent
<!-- Modals -->
<div id="addVoucherItem" class="modal fade voucher-modal" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Item Details</div>
            <div class="modal-body">
                <form action="{{ url('punchcard/voucher/add') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <label for="voucher-name">Voucher Name</label>
                            <input type="text" name="voucher-name" id="voucher-name" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for="threshold">Threshold</label>
                            <input type="number" name="threshold" id="threshold" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="voucher-description">Description</label>
                            <textarea name="voucher-description" id="voucher-description" class="form-control" rows="5"></textarea>
                        </div>
                        <div class="col-md-6">
                            <label for="discount">Discount</label>
                            <input type="number" name="discount" id="discount" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default">
                                    <input type="radio" name="discount_type" id="discount-type-opt1" autocomplete="off" value="amount">Amount
                                </label>
                                <label class="btn btn-default">
                                    <input type="radio" name="discount_type" id="discount-type-opt2" autocomplete="off" value="percentage">Percentage
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group branches">
                                <label class="voucherBranch">Branches</label>
                                <label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0" style="float: right;height:0;cursor:pointer;">
                                    <input type="checkbox" class="custom-control-input select-all" name="select-all">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Select All</span>
                                </label>
                                <div class="branch-check-wrap">
                                    @if ( $branches )
                                        @foreach( $branches as $branch)
                                        <div class="form-check">
                                            <input type="checkbox" name="branch_ids[]" class="checkbox options branch-options" value="{{ $branch['id'] }}" id="branch_{{ $branch['id'] }}" />
                                            <label class="form-check-label branch-label" for=""> {{ $branch['branchName'] }}
                                                <i class="fa fa-check" aria-hidden="true" style="display: none;"></i>                                      
                                            </label>
                                        </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="input-label">Image</label>
                            <div class="voucher-preview-img" style="background: #f7f7f7;height: 200px; max-width: 100%;">
                                <label for="voucher_image" class="btn" style="border:none;">Browse</label>
                            </div>
                            <div style="margin-top: 12px;">
                                <label class="custom-file" style="display: inline-block;width:100%;">
                                    <input type="file" name="voucher_image" id="voucher_image" style="display:none;" accept="image/*">
                                    <br />
                                    <span style="font-size: 12px;"><i>Required size is 350px x 200px</i></span>
                                </label>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <button type="button" class="btn btn-default clear-voucher">Clear</button>
                        <button type="submit" class="btn btn-primary pull-right">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="editVoucherItem" class="modal fade voucher-modal" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Item Details</div>
            <div class="modal-body">
                <form action="{{ url('punchcard/voucher/edit') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="voucher_id" id="evoucher_id" />
                    <div class="row">
                        <div class="col-md-6">
                            <label for="evoucher-name">Voucher Name</label>
                            <input type="text" name="evoucher-name" id="evoucher-name" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for="ethreshold">Threshold</label>
                            <input type="number" name="ethreshold" id="ethreshold" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="evoucher-description">Description</label>
                            <textarea name="evoucher-description" id="evoucher-description" class="form-control" rows="5"></textarea>
                        </div>
                        <div class="col-md-6">
                            <label for="ediscount">Discount</label>
                            <input type="number" name="ediscount" id="ediscount" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default">
                                    <input type="radio" name="ediscount_type" id="ediscount-type-opt1" autocomplete="off" value="amount">Amount
                                </label>
                                <label class="btn btn-default">
                                    <input type="radio" name="ediscount_type" id="ediscount-type-opt2" autocomplete="off" value="percentage">Percentage
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="input-label">Image</label>
                            <div class="evoucher-preview-img" style="background: #f7f7f7;height: 200px; width: 350px;">
                                <label for="evoucher_image" class="btn" style="border:none;">Browse</label>
                            </div>
                            <div style="margin-top: 12px;">
                                <label class="custom-file" style="display: inline-block;width:100%;">
                                    <input type="hidden" name="evoucher_image_changed" id="evoucher_image_changed" value="0" />
                                    <input type="file" name="evoucher_image" id="evoucher_image" style="display:none;" accept="image/*">
                                    <br />
                                    <span style="font-size: 12px;"><i>Required size is 350px x 200px</i></span>
                                </label>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <button type="button" class="btn btn-default clear-voucher">Clear</button>
                        <button type="submit" class="btn btn-primary pull-right">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="deleteVoucherItem" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Delete Voucher</div>
            <div class="modal-body">
                <p>Are you sure you want to delete this item?</p>
                <form action="{{ url('punchcard/voucher/delete') }}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="voucher_id" id="dvoucher_id" />
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button class="btn btn-primary pull-right" id="delete_class_btn">DELETE</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<div id="verifyAccountModal" class="modal fade voucher-modal" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-body">
                <form action="{{ url('verifyaccount') }}" method="POST" id="verifyAccountForm">
                    {{ csrf_field() }}
                    <input type="hidden" name="verifyaccount_redirect_url" id="verifyaccount_redirect_url" />
                    <input type="hidden" name="verifyaccount_current_url" id="verifyaccount_current_url" value="{{ url()->current() }}"/>
                    <input type="hidden" name="verifyaccount_merchant_id" value="{{ session('user_id') }}" />
                    <div class="form-group row">
                        <div class="col-md-12 text-center">
                            <h3>Please verify your account</h3>
                        </div>
                        <div class="col-md-12 text-center">
                            <input type="text" id="verifyaccount_name" name="verifyaccount_name" class="form-control" placeholder="Email" required />
                        </div>
                        <div class="col-md-12 text-center">
                            <input type="password" class="form-control" id="verifyaccount_password" name="verifyaccount_password" placeholder="Password" required />
                        </div>
                        <div class="col-md-12 text-center">
                            <button class="btn btn-primary">Continue</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<div id="importVoucherItems" class="modal fade voucher-modal" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Import Voucher Items</div>
            <div class="modal-body">
                <form action="{{ url('punchcard/voucher/uploaditems') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-12">&nbsp;</div>
                        <div class="col-md-2">
                            <button type="button" class="browse-vc padding-lr-12">Browse</button>
                        </div>
                        <div class="col-md-10">
                            <input type="text" name="vi-filename" id="vi-filename" class="form-control filename-vc">
                            <input type="file" name="voucheritem-import" id="voucheritem-import" class="form-control file-vc" accept=".csv">
                        </div>
                        <div class="col-md-12">
                            <label><a href="{{ url('page/voucher/voucher-item-upload.csv')}}">**Sample CSV Format</a></label>
                            <br /><br />
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-default clear-voucher">Clear</button>
                            <button class="btn btn-primary pull-right">Import</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
@endsection

@section('footerPlaceholder')
@parent
<script type="text/javascript">
// Toggle Select All
$('.voucher-modal .branches .select-all').on('click', function() {
    if($('.voucher-modal .branches .select-all').is(':checked')){
        $('.voucher-modal .branches .checkbox').prop('checked', true);
        $(".voucher-modal .branches .fa-check").css("display", "block");
    }
    else
    {
        $('.voucher-modal .branches .checkbox').prop('checked', false);
        $(".voucher-modal .branches .fa-check").css("display", "none");
    }
});
$('.voucher-modal .ebranches .select-all').on('click', function() {
    if($('.voucher-modal .ebranches .select-all').is(':checked')){
        $('.voucher-modal .ebranches .checkbox').prop('checked', true);
        $(".voucher-modal .ebranches .fa-check").css("display", "block");
    }
    else
    {
        $('.voucher-modal .ebranches .checkbox').prop('checked', false);
        $(".voucher-modal .ebranches .fa-check").css("display", "none");
    }
});
// Select Branch Individualy
$('.voucher-modal .branch-label').on('click', function(){
    var _checkBranch = $(this).siblings('.branch-options');
    var _check = $(this).find('.fa-check');
    
    if( _checkBranch.is(':checked') == false ) {
        _checkBranch.prop('checked', true);
        _check.css('display', 'block');
    }
    else {
        _checkBranch.prop('checked', false);
        _check.css('display', 'none');
    }
});
// Mechanics Prod Default on Load
$('#editVoucherItem').on('show.bs.modal', function(){
    if($('#editVoucherItem .ebranches .select-all').is(':checked')){
        $('#editVoucherItem .ebranches .checkbox').prop('checked', true);
        $("#editVoucherItem .ebranches .fa-check").css("display", "block");
    }
    else
    {
        $('#editVoucherItem .ebranches .checkbox').prop('checked', false);
        $("#editVoucherItem .ebranches .fa-check").css("display", "none");
    }
    $('#editVoucherItem .ebranches .checkbox').each(function(){
        if( $(this).attr('checked') ) $(this).siblings('label').click();
    });

    $("#evoucher_image_changed").val(0);
});
// Clear Fields
$('.clear-voucher').on('click', function(){
    $(this).closest('form').find('input, textarea, input[type="file"]').val('');
    $(this).closest('form').find('.select-all').prop('checked', false);
    $(this).closest('form').find('.fa-check').css('display', 'none');
});
// Bulk Import Voucher Code
$('.browse-vc').on('click', function(){
    $(this).closest('.form-group').find('.file-vc').click();
});
$('.file-vc').on('change', function(e){
    var _voucherCode = e.target.files[0].name;
    $(this).siblings('.filename-vc').val(_voucherCode);
});
//Toggle Status
$(document).ready(function(){

    var emptyTableMessage = 'No matching records found';
    var excelExportButton = '<div class="text-center"><button type="button" class="btn btn-default exportButton">Export to Excel</button></div>';
    // Client Code
    var clientCodeTableSpecs = {
        'initComplete': function() {
            $('#vouchersTable').wrap(function(){
                return "<div class='bootstrap-table'></div>";
            });
        },
        'sDom': '<"fixed-table-toolbar"f><"excelButton hide"B><"customButton">rtl<"bottom"ip><"clear">',
        'buttons': [ {
            extend: 'excel' 
        }],
            'language': {
                'emptyTable': emptyTableMessage                        
        },
            'oLanguage': {
                'sSearch': ''
        },
        'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
        'bProcessing': true
    };

    // initialize code management datatable
    $.extend($.fn.dataTableExt.oStdClasses, {
        'sWrapper': 'bootstrap-table dataTables_wrapper',
        'sFilter': 'pull-right search',
        'sInfo': 'pagination-detail',
        'sLength': 'page-list'
    });
    $('#vouchersTable').DataTable(clientCodeTableSpecs);

    if ($('#vouchersTable tbody tr.odd td').text() != emptyTableMessage) {
        $('#voucher').append(excelExportButton);
    }
    $('#vouchersTable_wrapper input').attr({'type': 'text', 'placeholder': 'Search'}).addClass('form-control');

    $('.tab-pane').on('click', '.exportButton', function(event) {
        $(this).parent().parent().find('.dataTables_wrapper .excelButton a').click();
    });

    $('.toggle-status').on('click', function(){
        if( $(this).hasClass('enabled') ) $(this).removeClass('enabled').addClass('disabled').text('Disabled');
        else $(this).removeClass('disabled').addClass('enabled').text('Enabled');
    });

    $("#vouchersTable").on("click", "#editVoucherItemButton", function() {
        $('#evoucher_id').val( $(this).data('id'));
        $('#evoucher-name').val( $(this).data('name'));
        $('#evoucher-description').val( $(this).data('desc'));
        $('#ethreshold').val( $(this).data('threshold'));
        $('#ediscount').val( $(this).data('discount'));

        if ( $(this).data('discount_type') == "amount") {
            $("#ediscount-type-opt1").prop('checked', true);
            $("#ediscount-type-opt1").parent().addClass('active');

        }
        if ( $(this).data('discount_type') == "percentage") {
            $("#ediscount-type-opt2").prop('checked', true);
            $("#ediscount-type-opt2").parent().addClass('active');
        }
        $('.evoucher-preview-img').css('background-image', 'url("../repository/merchant/voucher/'+ $(this).data("voucher_image") +'")');
    });

    $("#vouchersTable").on("click", "#deleteVoucherItemButton", function() {
        $('#dvoucher_id').val( $(this).data('id'));
    });

    $("#vouchersTable").on("click", "#voucherCodeImportButton", function() {
        $('#upload_voucher_id').val( $(this).data('id'));
        $('#ivc-voucher-name').html( $(this).data('vc-name'));
        $('#ivc-voucher-branch').html( $(this).data('vc-branch'));        
    });

    var _searchAlert = $('.search');
    setTimeout(function(){ if ( $('.alert').is(':visible') ) _searchAlert.addClass('adjustTop'); }, 300);

    $('#addVoucherItem').on("change", "#voucher_image", function() {
        if ( this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.voucher-preview-img').css('background-image', 'url('+e.target.result+')');
            };
            reader.readAsDataURL(this.files[0]);
        }
    });

    $('#editVoucherItem').on("change", "#evoucher_image", function() {
        if ( this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.evoucher-preview-img').css('background-image', 'url('+e.target.result+')');
            };
            reader.readAsDataURL(this.files[0]);
            $("#evoucher_image_changed").val(1);
        }
    });

    $("#vouchersTable").on("click",  "#voucherCodesViewButton", function() {
        $("#verifyaccount_redirect_url").val( $(this).data('url') );
    });
    
    $("#verifyAccountModal").on('hidden.bs.modal', function () {
        $("#verifyaccount_redirect_url").val('');
    });

});
$('.modal').on('show.bs.modal', function(event){
    setTimeout(function(){
        var _openedModal = $(event.relatedTarget).attr('href');
        var _adjustModalPosition =  parseFloat($(_openedModal + ' .modal-dialog').css('margin-top'));
        var _newPosition = (_adjustModalPosition / 2);

        $(_openedModal + ' .modal-dialog').css('margin-top', _newPosition + 'px');
    }, 100);
});

</script>
@endsection