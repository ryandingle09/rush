@extends('layouts.app')

@section('headerPlaceholder')
<!-- Additional Header items -->
<style>
    label.error { color: red !important; }
    #punchcard .search { margin-top: -43px !important; }
    #punchcard .new-punchcard { position: relative; top: 15px; }
    #punchcard #punchcardView { font-size: 14px; }
    #punchcard .fixed-table-body { height: initial; }
</style>
<!-- Voucher Management css -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
<link rel="stylesheet" href="page/voucher/css/style.css">
<link rel="stylesheet" href="page/punchcard/css/style.css">
@endsection

@section('view')
<div class="container">
    <div id="content" class="marginTop">

        <!-- Nav tabs -->
        <div class="navTabsWrap">
            <ul class="nav nav-tabs" role="tablist" id="tabs">
                <li role="presentation" class="active"><a href="#punchcard" aria-controls="punchcard" role="tab" data-toggle="tab">PUNCHCARD</a></li>
                @if ( $merchant->settings->voucher_module )
                    <li role="presentation"><a href="#voucher" aria-controls="voucher" role="tab" data-toggle="tab">VOUCHER</a></li>
                @endif
                @if($is_enable_product_module)
                <li role="presentation"><a href="#mechanics-per-product" aria-controls="mechanics-per-product" role="tab" data-toggle="tab">Mechanics per Product</a></li>
                @endif
            </ul>
        </div>
        
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="punchcard">
                <!-- <div class="row">
                    <div class="promos promo-list"> -->
                        <a href="{{ url('punchcard/add') }}" class="btn btn-primary btn-large new-punchcard">+ New Punchcard</a>
                        <table id="punchcardView" data-toggle="table" class="data-table table punchcard" data-pagination="true" data-search="true">
                            <thead>
                                <tr>
                                    <th>Promo</th>
                                    <th>Status</th>
                                    <th>Duration</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ( $promos as $promo )
                                <tr>
                                    <td>{{ $promo['title'] }}</td>
                                    <td>{{ $promo['status'] }}</td>
                                    <td>{{ $promo['duration_from_date'] }} - {{ $promo['duration_to_date'] }}</td>
                                    <td>
                                        <a href="#" data-toggle="modal" data-target="#deletePromo{{ $promo['id'] }}" class="delete deletePromo">
                                            <i class="fa fa-trash"></i>Delete
                                        </a>
                                        <a href="{{ url('punchcard/update/'. $promo['id']) }}" class="edit promoEdit" style="margin-left:10px;">
                                            <i class="fa fa-pencil"></i>Edit
                                        </a>
                                        <!-- Modal -->
                                        <div class="modal fade" id="deletePromo{{ $promo['id'] }}" tabindex="-1" role="dialog">
                                            <div class="modal-dialog" role="document">
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                  <button type="button" class="close"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                  <h4 class="modal-title">Confirm Delete</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Are you sure you want to delete "{{ $promo['title'] }}"?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                    <a href="{{ url('punchcard/delete')}}/{{ $promo['id'] }}" class="btn btn-danger">Yes</a>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                        <!-- ./ Modal -->
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>  
                    <!-- </div>
                </div> /container -->
            </div>
            
            @if ( $merchant->settings->voucher_module )
            <div role="tabpanel" class="tab-pane" id="voucher">
                <!-- Vouchers Start -->
                @include('punchcard.vouchers')
                <!-- Vouchers End -->
            </div>
            @endif

            <div role="tabpanel" class="tab-pane" id="mechanics-per-product">
                @include('punchcard.mechanics_per_product')
            </div>
        
    </div>
</div>
@endsection

@section('catalog_modals')
@show

@section('footerPlaceholder')
@parent
<script>
    $(document).ready(function(tag){
        var _punchardTable = $('#punchcardView tbody td').length;
        var _fixedtablebody = $('#punchcard .fixed-table-body');
        if( _punchardTable > 9 ) $(_fixedtablebody).css('height', '100%');
        else $(_fixedtablebody).css('height', 'initial');
    });
    $('.navTabsWrap .nav li').on('click', function(){
        $('body').scrollTop(0);
    });
</script>

<div id="loading" class="hidden" style="position: absolute; z-index: 10000; left: 50%; top: 50%;">
    <i class="fa fa-circle-o-notch fa-spin" style="font-size:24px"></i>
</div>
@endsection

