@extends('layouts.app')

@section('view')
<div class="container">
    @include('flash.message')
    <div id="content">
        <!-- Nav tabs -->
        <div class="navTabsWrap">
            <ul class="nav nav-tabs" role="tablist" id="tabs">
                <li role="presentation" class="active"><a href="#program" aria-controls="program" role="tab" data-toggle="tab">Program Details</a></li>
                <li role="presentation"><a href="#capping" aria-controls="capping" role="tab" data-toggle="tab">Transaction Capping</a></li>
            </ul>
        </div>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="program">
                <div class="noTab promos spacing settings">
                    <form action="{{MigrationHelper::getAppBaseUrl()}}settings" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Program Name</h4>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label for="programName">Program Name</label>
                                    <input type="text" class="form-control" name="programName" id="programName" value="{{$programName}}" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label for="pointsName">{{$stampPointsName}} Name</label>
                                    <input type="text" class="form-control" name="pointsName" id="pointsName" value="{{$pointsName}}" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4>App Details</h4>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label for="programName">App Name</label>
                                    <input type="text" class="form-control" name="appName" id="appName" value="{{$appName}}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label for="pointsName">Description</label>
                                    <input type="text" class="form-control" name="appDescription" id="appDescription" value="{{$appDescription}}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-10 form-group">
                                    <label for="pointsName">Appstore Logo</label>
                                    <div class="appstoreLogo">
                                        <span id="appLogoMsg" class="error-msg hide">Image does not meet the image size requirement. Please check again.<br></span>
                                        <img src="{{$logo}}" class="imgPrev" alt="">
                                        <span class="btn btn-default btn-file">
                                        Choose File <input type="file" name="logo" class="fuImage" ew="1024" eh="1024" value="Choose File">
                                        </span>
                                        <input class="btn btn-default btn-file" type="submit" value="Submit">
                                        <br><i>Required Size: 1024px x 1024px</i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="capping">
                <div class="noTab promos spacing settings">
                    <form action="{{MigrationHelper::getAppBaseUrl()}}settings/capping" method="POST">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <h4>Capping Settings</h4>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label for="earn_transaction_limit">Earn Transaction Limit</label>
                                    <input type="text" class="form-control" name="earn_transaction_limit" id="earn_transaction_limit" value="{{$earn_transaction_limit}}" required>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="earn_points_limit">Earn Points Limit</label>
                                    <input type="text" class="form-control" name="earn_points_limit" id="earn_points_limit" value="{{$earn_points_limit}}" required>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="redemption_transaction_limit">Redemption Transaction Limit</label>
                                    <input type="text" class="form-control" name="redemption_transaction_limit" id="redemption_transaction_limit" value="{{$redemption_transaction_limit}}" required>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="paypoint_transaction_limit">Paypoint Transaction Limit</label>
                                    <input type="text" class="form-control" name="paypoint_transaction_limit" id="paypoint_transaction_limit" value="{{$paypoint_transaction_limit}}" required>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="paypoint_points_limit">Paypoint Points Limit</label>
                                    <input type="text" class="form-control" name="paypoint_points_limit" id="paypoint_points_limit" value="{{$paypoint_points_limit}}" required>
                                </div>
                                <div class="col-md-6 form-group">
                                    {{Form::label('points_transfer', 'Allow Points Transfer?')}}
                                    {{Form::select('points_transfer', ['0' => 'no', '1' => 'yes'], $points_transfer, array('class' => 'form-control', 'required' => null))}}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{Form::submit('Submit', array('class' => 'btn btn-default btn-file'))}}
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> <!-- /container -->
@endsection
