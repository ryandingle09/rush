@extends('layouts.app')

@section('headerPlaceholder')
    @parent
    <link rel="stylesheet" href="page/analytics/css/chartist.min.css">
    <link rel="stylesheet" href="page/analytics/css/style.css">
@endsection

@section('view')
<div id="export-capture-page-1">
<div class="container">
    <div class="row charts-wrapper">
        <div class="col-md-4">
            <div class="tile">
                <div class="tile-icon red">
                    <img src="page/analytics/images/card.png" alt="card">
                </div>
                <div class="tile-content">
                    <h3>{{number_format($transactionTotal)}}</h3>
                    <small>TOTAL TRANSACTION (LIFETIME)</small>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="tile">
                <div class="tile-icon orange">
                    <img src="page/analytics/images/trophy.png" alt="trophy">
                </div>
                <div class="tile-content">
                    <h3>{{number_format($pointsSeededTotal)}}</h3>
                    <small>TOTAL POINTS SEEDED</small>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="tile">
                <div class="tile-icon gray">
                    <img src="page/analytics/images/star.png" alt="star">
                </div>
                <div class="tile-content">
                    <h3>{{number_format($pointsUsedTotal)}}</h3>
                    <small>TOTAL POINTS USED</small>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<!--  -->
<div class="container">
    <div class="row">
        <form action="" method="get">
            <div class="col-md-2">
                <div class='input-group date runDate analytics-date-picker' id='analyticsDateContainer'>
                    <input type='text' class="form-control" name="analyticsDate" id="analyticsDate" placeholder="Select Date"  data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])$" value="{{ $dateOfFilter }}" required >
                    <span class="input-group-addon out"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
            <div class="col-md-1">
                <div class="input-group">
                    <button type="submit" class="btn btn-primary actionBtn">Filter</button>
                </div>
            </div>
            <div class="col-md-1">
                <div class="input-group">
                    <button id="exportPDF" class="btn btn-primary actionBtn analytics-export-pdf">Export to PDF</button>
                </div>
            </div>
        </form>
    </div>
    <br>
    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <h3 class="card-header">TOTAL TXNS THIS MONTH (PHP)</h3>
                <div class="card-content">
                    {{number_format($monthTransactionAmountTotal)}}
                    <span class="label label-primary red">Monthy</span>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <h3 class="card-header">AVG REVENUE PER CUSTOMER (PHP)</h3>
                <div class="card-content">
                    {{number_format($averageRevenuePerCustomer)}}
                    <span class="label label-primary orange">Weekly</span>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <h3 class="card-header">AVERAGE PTS EARNED PER CUSTOMER</h3>
                <div class="card-content">
                    {{number_format($averagePointsEarnedPerCustomer)}}
                    <span class="label label-primary red">Monthly</span>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <h3 class="card-header">AVERAGE PTS BURNED PER CUSTOMER</h3>
                <div class="card-content">
                    {{number_format($averagePointsBurnedPerCustomer)}}
                    <span class="label label-primary orange">Monthly</span>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<div class="container">
    <div class="row charts-wrapper">
        <div class="col-md-6">
            <div class="chart-details">
                <h3 class="chart-title border-bottom">TRANSACTION VALUE</h3>
                <div class="c-select">
                    <label class="label-1">Earning</label>
                    <ul class="pie-legends">
                        <li class="txn-update-set" data-dataset='0'>Earning</li>
                        <li class="txn-update-set" data-dataset='1'>Pay with points</li>
                        <li class="txn-update-set" data-dataset='2'>Redemptions</li>
                    </ul>
                </div>
                <ul class="txn-legends">
                    <li>Given Month</li>
                    <li>Previous Month</li>

                </ul>
            </div>
            <div class="txn-chart-wrapper">
                <canvas id="txn-value"  width="400" height="300"></canvas>
            </div>
        </div>
        <div class="col-md-6">
            <div class="chart-details">
                <h3 class="chart-title border-bottom">TRANSACTION CHANNEL</h3>
                <ul class="pie-legends">
                    <li>Earning</li>
                    <li>Pay with points</li>
                    <li>Redemptions</li>
                    <li>Given Month #</li>
                    <li>Previous Month #</li>
                </ul>
            </div>
            <div class="txn-chart-wrapper">
                <canvas id="txn-channel"  width="400" height="300"></canvas>
            </div>
        </div>
    </div>
</div>
</div>
<div id="export-capture-page-2">
<div class="container">
    <div class="row charts-wrapper">
        <div class="col-md-6">
            <div class="chart-details">
                <h3 class="chart-title">CHANNELS</h3>
                <h3 class="chart-sub-title">TOTAL MEMBERS</h3>
                <ul class="pie-legends">
                    <li>SMS: {{$channelsData->sms or '0'}}</li>
                    <li>Merchant App: {{$channelsData->merchantApp or '0'}}</li>
                    <li>Customer App: {{$channelsData->customerApp or '0'}}</li>
                    <li>Customer Web: {{$channelsData->customerWeb or '0'}}</li>
                    <li>Card: {{$channelsData->card or '0'}}</li>
                </ul>
            </div>
            <div class="chart-wrapper" style="padding: 28px;">
                <div class="ct-chart ct-perfect-fourth" style="height: 215px; width: 216px;">
                    <canvas id="member-channel" width="400" height="400"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="chart-details">
                <h3 class="chart-title">APP</h3>
                <h3 class="chart-sub-title">Net Download Lifetime</h3>
                <ul class="pie-legends">
                    <li>iOS : {{ number_format($appDownloadDataLifetime[PushHelper::PLATFORM_IOS]) }}</li>
                    <li>Android : {{ number_format($appDownloadDataLifetime[PushHelper::PLATFORM_ANDROID]) }}</li>
                    <li>Users : {{ number_format($appDownloadDataLifetime[PushHelper::PLATFORM_BOTH]) }}</li>
                </ul>
            </div>
            <div class="bar-chart-wrapper">
                <canvas id="myChart" width="400" height="400"></canvas>
            </div>
        </div>
    </div>
    <div class="row charts-wrapper">
        <div class="col-md-6">
            <div class="chart-details">
                <h3 class="chart-title">CARD</h3>
                <h3 class="chart-sub-title">TOTAL MEMBERS</h3>
                <ul class="pie-legends">
                    <li># of Cards</li>
                    <li>Active Users</li>
                </ul>
            </div>
            <div class="bar-chart-wrapper">
                <canvas id="card-chart" width="400" height="400"></canvas>
            </div>
        </div>
        <div class="col-md-6">
            <div class="chart-details">
                <h3 class="chart-title border-bottom">TRANSACTION FREQUENCY</h3>
                <div class="c-select">
                    <label class="label-1">Earning</label>
                    <ul class="pie-legends">
                        <li class="txn-freq-update-set" data-dataset='0'>Earning</li>
                        <li class="txn-freq-update-set" data-dataset='1'>Pay with points</li>
                        <li class="txn-freq-update-set" data-dataset='2'>Redemptions</li>
                    </ul>
                </div>
                <ul class="txn-legends">
                    <li>Given Month</li>
                    <li>Previous Month</li>

                </ul>
            </div>
            <div class="txn-chart-wrapper">
                <canvas id="txn-freq-value"  width="400" height="300"></canvas>
            </div>
        </div>
    </div>
</div>
<br>

<!--  -->
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <h3 class="card-header">Total Member</h3>
                <div class="card-content">
                    {{number_format($membersCount)}}
                    <span class="label label-primary">All Users</span>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="card">
                <h3 class="card-header">New Member</h3>
                <div class="card-content">
                    {{number_format($newMembersThisMonthCount)}}
                    <span class="label label-primary red">Monthly</span>
                </div>
            </div>
        </div>
        <div class="col-md-4 charts-wrapper">
            <div class="card pie-legend ">
				<div class="chart-wrapper small-pie" style="position: relative; bottom: 36px;">
                    <div class="ct-member_sum ct-perfect-fourth">
                        <canvas id="account-pie" width="400" height="400"></canvas>
                    </div>
                </div>
                <ul class="pie-legends">
                    <li>Return in the past month: {{$customerMonthlyTransactionData['returnInThePastMonth'] or 0}}</li>
                    <li>New this month: {{$customerMonthlyTransactionData['newThisMonth'] or 0}}</li>
                    <li>Has not returned in past month: {{$customerMonthlyTransactionData['hasNotReturned'] or 0}}</li>
                </ul>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <h3 class="card-header">ave. visits per Customer</h3>
                <div class="card-content">
                    {{number_format($averageVisitsPerCustomer)}}
                    <span class="label label-primary red">Monthly</span>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<br>
<div id="export-capture-page-3">
<div class="container">
    <div class="row charts-wrapper">
        <div class="col-md-4">
            <div class="chart-details top-branches" style="height: 100% !important">
                <h3 class="chart-title border-bottom">TOP 20 REWARDS REDEEMED</h3>
                <table class="table" >
                    <thead>
                        <tr>
                            <td width="100px">Reward</td>
                            <td>total redemptions</td>
                        </tr>
                    </thead>
                    @if (count($topRedeemedRewards) > 0)
                    <tbody style="max-height: 100%;">
                        @foreach ($topRedeemedRewards as $topRedeemedReward)
                        @if($topRedeemedReward->name)
                        <tr>
                            <td width="100px">{{ $topRedeemedReward->name }}</td>
                            <td>{{ $topRedeemedReward->totalRedemption }}</td>
                        </tr>
                        @endif
                        @endforeach
                    </tbody>
                    @endif
                </table>
            </div>
        </div>
        <div class="col-md-4">
            <div class="chart-details top-branches" style="height: 100% !important">
                <h3 class="chart-title border-bottom">TOP 20 CUSTOMER</h3>
                <table class="table" >
                    <thead>
                        <tr>
                            <td width="100px">Name</td>
                            <td>total visits</td>
                            <td>total points earned</td>
                        </tr>
                    </thead>
                    @if (count($topCustomers) > 0)
                    <tbody style="max-height: 100%;">
                        @foreach ($topCustomers as $topCustomer)
                        <tr>
                            <td width="100px">{{ str_limit($topCustomer->fullName, 20) }}</td>
                            <td width="100px">{{ $topCustomer->totalVisit }}</td>
                            <td width="100px">{{ $topCustomer->totalPointsEarned }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
            </div>
        </div>
        <div class="col-md-4">
            <div class="chart-details top-branches" style="height: 100% !important">
                <h3 class="chart-title border-bottom">TOP 20 BRANCHES</h3>
                <table class="table" >
                    <thead>
                        <tr>
                            <td width="80px">Branch</td>
                            <td width="130px">total earning transactions</td>
                            <td width="120px">total redemption transactions</td>
                        </tr>
                    </thead>
                    @if (count($topBranches) > 0)
                    <tbody style="max-height: 100%;">
                        @foreach ($topBranches as $topBranch)
                        <tr>
                            <td width="80px">{{ $topBranch->branchName }}</td>
                            <td width="130px">{{ $topBranch->totalEarning }}</td>
                            <td width="120px">{{ $topBranch->totalRedemption }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
            </div>
        </div>
    </div>
</div>
</div>
<br>
<div id="export-capture-page-4">
<div class="container">
    <div class="row charts-wrapper">
        <div class="col-md-12">
            <div class="chart-details tpb-chart-details">
                <h3 class="chart-title border-bottom">PROFILE</h3>
                <div class="profile-d">
                    <h3>% of males</h3>
                    <small>vs total population</small>
                </div>
            </div>
            <div class="profile-chart-wrapper">
                <div class="profile-chart-img">
                    <img src="page/analytics/images/female-pct.png" alt="">
                    <img src="page/analytics/images/male-pct.png" alt="">
                </div>
                <div class="profile-chart-details">
                    <br>
                    <ul class="profile-ul fm">
                        @foreach ($profileAgeBrackets as $profileAgeBracket)
                        <li>
                            <div style="height:{{number_format((( @($profileData['unknown'][$profileAgeBracket]/$profileData['total'][$profileAgeBracket]) ) ?: 0)*100, 0)}}%">
                                <label>{{$profileData['unknown'][$profileAgeBracket]}}</label>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                    <ul class="profile-bwn">
                        <li>not set</li>
                        <li>18-24</li>
                        <li>25-34</li>
                        <li>35-44</li>
                        <li>45+</li>
                    </ul>
                    <ul class="profile-ul m">
                        @foreach ($profileAgeBrackets as $profileAgeBracket)
                        <li>
                            <div style="height:{{number_format((( @($profileData['male'][$profileAgeBracket]/$profileData['total'][$profileAgeBracket]) ) ?: 0)*100, 0)}}%">
                                <label>{{$profileData['male'][$profileAgeBracket]}}</label>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<div class="container">
    <div class="row charts-wrapper">
        <div class="col-md-12">
            <div class="chart-details visit-chart-details">
                <h3 class="chart-title border-bottom">VISITS</h3>
                <ul class="pie-legends">
                    <li>New</li>
                    <li>Recurring</li>
                </ul>
            </div>
            <div class="visit-chart-wrapper">
                <canvas id="visit-chart"></canvas>
            </div>
        </div>

    </div>
</div>
</div>
<br>
<div id="export-capture-page-5">
<div class="container">
    <div class="row charts-wrapper">
        <div class="col-md-12">
            <div class="chart-details tpb-chart-details">
                <h3 class="chart-title">TOP PRODUCTS BOUGHT</h3>
                <h3 class="chart-sub-title">(Earning)</h3>
            </div>
            <div class="tpb-chart-wrapper">
                <div class="tpb-chart-img">
                    <img src="page/analytics/images/female-pct.png" alt="">
                    <img src="page/analytics/images/male-pct.png" alt="">
                </div>
                <div class="tbp-chart-details">
                    <ul class="tpb-chart fm">
                        <li>
                            <ul>
                                @foreach($topProductsBought['not_set']['female'] as $product_transaction)
                                    <li>{{ $product_transaction->product->name }} - {{ $product_transaction->total }}</li>
                                @endforeach
                            </ul>
                        </li>
                        <li>
                            <ul>
                                @foreach($topProductsBought['18_24']['female'] as $product_transaction)
                                    <li>{{ $product_transaction->product->name }} - {{ $product_transaction->total }}</li>
                                @endforeach
                            </ul>
                        </li>
                        <li>
                            <ul>
                                @foreach($topProductsBought['25_34']['female'] as $product_transaction)
                                    <li>{{ $product_transaction->product->name }} - {{ $product_transaction->total }}</li>
                                @endforeach
                            </ul>
                        </li>
                        <li>
                            <ul>
                                @foreach($topProductsBought['35_44']['female'] as $product_transaction)
                                    <li>{{ $product_transaction->product->name }} - {{ $product_transaction->total }}</li>
                                @endforeach
                            </ul>
                        </li>
                        <li>
                            <ul>
                                @foreach($topProductsBought['45_up']['female'] as $product_transaction)
                                    <li>{{ $product_transaction->product->name }} - {{ $product_transaction->total }}</li>
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                    <ul class="tbp-bwn">
                        <li>not set</li>
                        <li>18-24</li>
                        <li>25-34</li>
                        <li>35-44</li>
                        <li>45+</li>
                    </ul>
                    <ul class="tpb-chart m">
                        <li>
                            <ul>
                                @foreach($topProductsBought['not_set']['male'] as $product_transaction)
                                    <li>{{ $product_transaction->product->name }} - {{ $product_transaction->total }}</li>
                                @endforeach
                            </ul>
                        </li>
                        <li>
                            <ul>
                                @foreach($topProductsBought['18_24']['male'] as $product_transaction)
                                    <li>{{ $product_transaction->product->name }} - {{ $product_transaction->total }}</li>
                                @endforeach
                            </ul>
                        </li>
                        <li>
                            <ul>
                                @foreach($topProductsBought['25_34']['male'] as $product_transaction)
                                    <li>{{ $product_transaction->product->name }} - {{ $product_transaction->total }}</li>
                                @endforeach
                            </ul>
                        </li>
                        <li>
                            <ul>
                                @foreach($topProductsBought['35_44']['male'] as $product_transaction)
                                    <li>{{ $product_transaction->product->name }} - {{ $product_transaction->total }}</li>
                                @endforeach
                            </ul>
                        </li>
                        <li>
                            <ul>
                                @foreach($topProductsBought['45_up']['male'] as $product_transaction)
                                    <li>{{ $product_transaction->product->name }} - {{ $product_transaction->total }}</li>
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<br>
</div>
@endsection

@section('footerPlaceholder')
@parent
<script src="https://raw.githack.com/leighquince/Chart.js/master/Chart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
<script src="page/analytics/js/chartist.min.js"></script>
<script src="page/analytics/js/Chart.js"></script>
@include('analytics.data')
<script type="text/javascript">
$(window).load(function(){
    $('.analytics-date-picker').datetimepicker( {
        format: 'YYYY-MM'
    });

    $('#exportPDF').on('click', function(e){
        e.preventDefault();

        var doc = new jsPDF('landscape');
        var actionBtn = $('.actionBtn');
        var ctr = 1;

        actionBtn.addClass('hidden');
        startConvert(doc, actionBtn, ctr);
    });

    function startConvert(doc, actionBtn, ctr){
        var targetElem = $('#export-capture-page-' + ctr);
        html2canvas(targetElem, {
            allowTaint: true,
            logging: true,
            onrendered : function(canvas){
                var img = canvas.toDataURL("image/jpeg");
                
                var width = doc.internal.pageSize.width;
                
                if(ctr > 1){
                    doc.addPage();
                }

                doc.addImage(img, 'jpeg', 0, 10, width, targetElem.height() / 4);

                ctr++;
                
                if($('#export-capture-page-' + ctr).length){
                    startConvert(doc, actionBtn, ctr);
                } else {
                    doc.save('Export.pdf');
                    actionBtn.removeClass('hidden');
                }
            }
        });
    }
});
</script>
@endsection