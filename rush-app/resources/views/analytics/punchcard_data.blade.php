<!--ANALYTICS DATA-->
<script type="text/javascript">
// Pie Chart
var data = {
  series: [
      {
          value: {{$channelsData->sms or '0'}},
          name: 'SMS',
          className: "pie-chart-v1"
      },
      {
          value: {{$channelsData->merchantApp or '0'}},
          name: 'Merchant App',
          className: "pie-chart-v2"
      },
      {
          value: {{$channelsData->customerApp or '0'}},
          name: 'Customer App',
          className: "pie-chart-v3"
      },
      {
          value: {{$channelsData->customerWeb or '0'}},
          name: 'Customer Web',
          className: "pie-chart-v4"
      },
      {
          value: {{$channelsData->card or '0'}},
          name: 'Card',
          className: "pie-chart-v5"
      }
  ]
};

var sum = function(a, b) { return a + b };
var options = {
  width: '216px',
  height: '215px',
  ignoreEmptyValues: true
};
//new Chartist.Pie('.ct-chart', data, options);

var context = document.getElementById('member-channel').getContext('2d');

var myChart = new Chart(context, {
  type: 'pie',
  data: {
    labels: ["SMS", "merchant App", "Customer App", "customer Web", "Card"],
    datasets: [{
      backgroundColor: [
        "#d41f39",
        "#f57f20",
        "#5c5c5c",
        "#acadcc",
        "#8a8ba4"
      ],
      data: [{{$channelsData->sms or '0'}}, 
            {{$channelsData->merchantApp or '0'}},
             {{$channelsData->customerApp or '0'}},
             {{$channelsData->customerWeb or '0'}},
             {{$channelsData->card or '0'}}],
             borderWidth : 0,
    }],

  },
    options: {
        legend: {
            display: false
        },
        responsive: true,
    }
});

// member Pie Chart
var member_data = {
    series: [{
        value: {{$customerMonthlyTransactionData['returnInThePastMonth'] or 0}},
        name: 'Mobile # Only',
        className: "pie-chart-v1"
    }, {
        value: {{$customerMonthlyTransactionData['newThisMonth'] or 0}},
        name: 'App',
        className: "pie-chart-v2"
    }, {
        value: {{$customerMonthlyTransactionData['hasNotReturned'] or 0}},
        name: 'Card',
        className: "pie-chart-v3"
    }]
};
var member_sum = function(a, b) {
    return a + b };
var member_options = {
    width: '86px',
    height: '85px'
};
//new Chartist.Pie('.ct-member_sum', member_data, member_options);

var context = document.getElementById('account-pie').getContext('2d');

var myChart = new Chart(context, {
  type: 'pie',
  data: {
    labels: ["Return last month", "New this month", "Has not returned"],
    datasets: [{
      backgroundColor: [
        "#d41f39",
        "#f57f20",
        "#5c5c5c"
      ],
      data: [{{$customerMonthlyTransactionData['returnInThePastMonth'] or 0}}, 
            {{$customerMonthlyTransactionData['newThisMonth'] or 0}},
             {{$customerMonthlyTransactionData['hasNotReturned'] or 0}}],
             borderWidth : 0,
    }],

  },
    options: {
        legend: {
            display: false
        },
        responsive: true,
        tooltips : {
            enabled: false      
        }
    }
});


/* App Chart */
var barChartData = {
    labels: ["Week 1", "Week 2", "Week 3", "Week 4"],
    datasets: [{
        data: [{{$appDownloadData['active']}}],
        type: 'line',
        label: 'Active Users',
        fill: false,
        backgroundColor: "#5c5c5c",
        borderColor: "#d6d6d6",
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        lineTension: 0,
        pointBackgroundColor: "#5c5c5c",
        pointBorderColor: "#d6d6d6",
        pointBorderWidth: 5,
        pointHoverRadius: 10,
        pointHoverBackgroundColor: "#d6d6d6",
        pointHoverBorderColor: "#d6d6d6",
        pointHoverBorderWidth: 2,
        pointRadius: 6,
        pointHitRadius: 10,
        yAxisID: "bar-y-axis",
    }, {
        label: 'Android',
        backgroundColor: [
            'rgba(245, 127, 32, 1)',
            'rgba(245, 127, 32, 1)',
            'rgba(245, 127, 32, 1)',
            'rgba(245, 127, 32, 1)',
        ],
        borderColor: [
            'rgba(245, 127, 32, 1)',
            'rgba(245, 127, 32, 1)',
            'rgba(245, 127, 32, 1)',
            'rgba(245, 127, 32, 1)',
        ],
        yAxisID: "bar-y-axis",
        data: [{{$appDownloadData['android']}}],
    }, {
        label: 'iOS',
        backgroundColor: [
            'rgba(212, 31, 57, 1)',
            'rgba(212, 31, 57, 1)',
            'rgba(212, 31, 57, 1)',
            'rgba(212, 31, 57, 1)',
        ],
        borderColor: [
            'rgba(212, 31, 57, 1)',
            'rgba(212, 31, 57, 1)',
            'rgba(212, 31, 57, 1)',
            'rgba(212, 31, 57, 1)',
        ],
        yAxisID: "bar-y-axis",
        data: [{{$appDownloadData['ios']}}],
    }]
};

 window.onload = function() {
     var ctx = document.getElementById("myChart").getContext('2d');
     var myChart = new Chart(ctx, {
     type: 'bar',
     data: barChartData,
     options: {
         legend: {
           display: false
        },
       title: {
         display: true,
         text: "# of downloads this month",
         position:'left'
       },
       tooltips: {
         mode: 'label',
       },
       responsive: true,
       scales: {
         xAxes: [{
           stacked: true,
           gridLines : {
            display : false
           },
         }],
         yAxes: [{
           stacked: false,
           ticks: {
             beginAtZero: true,
         },
           display: false,

         }, {
           id: "bar-y-axis",
           stacked: true,
           gridLines : {
            display : false
           },
           ticks: {
             beginAtZero: true,
           },
           display: false,
           type: 'linear'
         }]
       }
     }
   });
 };


/* Card Chart */
var CarBarChartData = {
    labels: ["Week 1", "Week 2", "Week 3", "Week 4"],
    datasets: [{
        data: [0, 0, 0, 0],
        type: 'line',
        label: 'This Year',
        fill: true,
        backgroundColor: "rgba(223, 223,223, 0.6)",
        borderColor: "#d6d6d6",
        borderCapStyle: 'butt',
        borderDashOffset: 0.0,
        lineTension: 0,
        pointBackgroundColor: "#5c5c5c",
        pointBorderWidth: 5,
        pointHoverRadius: 10,
        pointHoverBackgroundColor: "#d6d6d6",
        pointHoverBorderColor: "#d6d6d6",
        pointHoverBorderWidth: 2,
        pointRadius: 6,
        pointHitRadius: 10,
        yAxisID: "bar-y-axis",
    }, {
        label: 'Promoters',
        backgroundColor: [
            'rgba(245, 127, 32, 1)',
            'rgba(212, 31, 57, 1)',
            'rgba(245, 127, 32, 1)',
            'rgba(212, 31, 57, 1)',
        ],
        borderColor: [
            'rgba(245, 127, 32, 1)',
            'rgba(212, 31, 57, 1)',
            'rgba(245, 127, 32, 1)',
            'rgba(212, 31, 57, 1)',
        ],
        yAxisID: "bar-y-axis",
        data: [0, 0, 0, 0],
    }]
};

var cardChart = document.getElementById("card-chart").getContext('2d');
var myCardChart = new Chart(cardChart, {
    type: 'bar',
    data: CarBarChartData,
    options: {
        legend: {
            display: false
        },
        title: {
            display: true,
            text: "# of Card Issued",
            position: 'left'
        },
        tooltips: {
            mode: 'label',
        },
        responsive: true,
        scales: {
            xAxes: [{
                stacked: true,
                gridLines: {
                    display: false
                },
            }],
            yAxes: [{
                stacked: false,
                ticks: {
                    beginAtZero: true,
                },
                display: false,

            }, {
                id: "bar-y-axis",
                stacked: true,
                gridLines: {
                    display: false
                },
                ticks: {
                    beginAtZero: true,
                },
                display: false,
                type: 'linear'
            }]
        }
    }
});


/* Visit Chart */
var visitChartData = {
    labels: ["M", "T", "W", "TH", "F", "SAT", "SUN"],
    datasets: [{
        data: [{{$visitsData['total']}}],
        type: 'line',
        label: 'Total',
        fill: false,
        backgroundColor: "#5c5c5c",
        borderColor: "#d6d6d6",
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        lineTension: 0,
        pointBackgroundColor: "#5c5c5c",
        pointBorderColor: "#d6d6d6",
        pointBorderWidth: 5,
        pointHoverRadius: 10,
        pointHoverBackgroundColor: "#d6d6d6",
        pointHoverBorderColor: "#d6d6d6",
        pointHoverBorderWidth: 2,
        pointRadius: 6,
        pointHitRadius: 10,
        yAxisID: "bar-y-axis",
    }, {
        label: 'New',
        backgroundColor: [
            'rgba(212, 31, 57, 1)',
            'rgba(212, 31, 57, 1)',
            'rgba(212, 31, 57, 1)',
            'rgba(212, 31, 57, 1)',
            'rgba(212, 31, 57, 1)',
            'rgba(212, 31, 57, 1)',
            'rgba(212, 31, 57, 1)',
        ],
        borderColor: [
            'rgba(212, 31, 57, 1)',
            'rgba(212, 31, 57, 1)',
            'rgba(212, 31, 57, 1)',
            'rgba(212, 31, 57, 1)',
            'rgba(212, 31, 57, 1)',
            'rgba(212, 31, 57, 1)',
            'rgba(212, 31, 57, 1)',
        ],
        yAxisID: "bar-y-axis",
        data: [{{$visitsData['new']}}],
    }, {
        label: 'Recurring',
        backgroundColor: [
            'rgba(245, 127, 32, 1)',
            'rgba(245, 127, 32, 1)',
            'rgba(245, 127, 32, 1)',
            'rgba(245, 127, 32, 1)',
            'rgba(245, 127, 32, 1)',
            'rgba(245, 127, 32, 1)',
            'rgba(245, 127, 32, 1)',
        ],
        borderColor: [
            'rgba(245, 127, 32, 1)',
            'rgba(245, 127, 32, 1)',
            'rgba(245, 127, 32, 1)',
            'rgba(245, 127, 32, 1)',
            'rgba(245, 127, 32, 1)',
            'rgba(245, 127, 32, 1)',
            'rgba(245, 127, 32, 1)',
        ],
        yAxisID: "bar-y-axis",
        data: [{{$visitsData['recurring']}}],
    }]
};
var visitCtx = document.getElementById("visit-chart").getContext('2d');
var visitChart = new Chart(visitCtx, {
    type: 'bar',
    data: visitChartData,
    options: {
        legend: {
            display: false
        },
        title: {
            display: false,
            text: "",
            position: 'left'
        },
        tooltips: {
            mode: 'label',
        },
        responsive: true,
        scales: {
            xAxes: [{
                stacked: true,
                gridLines: {
                    display: false
                },
                categoryPercentage: 0.4,
                barPercentage: 0.5,
            }],
            yAxes: [{
                stacked: false,
                ticks: {
                    beginAtZero: true,
                },
                display: false,
            }, {
                id: "bar-y-axis",
                stacked: true,
                gridLines: {
                    display: false
                },
                ticks: {
                    beginAtZero: true,
                },
                display: false,
                type: 'linear'
            }]
        }
    }
});

/* Txn Channel Chart */
var txnChartData = {
    labels: ["SMS", "M-App", "C-App", "C-Web",'Card'],
    datasets: [{
        label: 'Earning',
        backgroundColor: 'rgba(212, 31, 57, 1)',
        data: [{{$transactionChannelData['earn']}}],
    }, {
        label: 'Redemptions',
        backgroundColor: 'rgba(92, 92, 92, 1)',
        data: [{{$transactionChannelData['redeem']}}],
    }, {
        label: 'Given Month #',
        backgroundColor: 'rgba(172, 173, 204, 1)',
        data: [{{$transactionChannelData['givenMonth']}}],
    }, {
        label: 'Previous Month #',
        backgroundColor: 'rgba(138, 139, 164, 1)',
        data: [{{$transactionChannelData['previousMonth']}}],
    }]
};

var ctx = document.getElementById("txn-channel").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'horizontalBar',
    data: txnChartData,
    options: {
        legend: {
            display: false
        },
        title: {
            display: false,
            text: "# of downloads",
            position:'left'
        },
        tooltips: {
            mode: 'label',
        },
        responsive: true,
        scales: {
            xAxes: [{
                stacked: true,
                gridLines : {
                    display : false,
                },
                ticks: {
                    beginAtZero: true,
                },
                display: false,
            }],
            yAxes: [{
                categoryPercentage: 0.6,
                barPercentage: 0.5,
                stacked: true,
                ticks: {
                    beginAtZero: true,
                },
                gridLines : {
                    display : false
                },
            }]
        }
    }
});
/* Txn Value */
function initTxnValue(obj,val){
    var txnValueChartData = {
            labels: ["M", "T", "W", "TH", "F", "SAT", "SUN"],
            datasets: [
                {
                    label: "Previous Month",
                    fill: true,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(213, 213, 213, 0.5)',
                    borderColor: "#5c5c5c",
                    borderCapStyle: 'butt',
                    borderDash: [6],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    lineTension: 0,
                    pointBackgroundColor: "#ffffff",
                    pointBorderColor: "#5c5c5c",
                    pointBorderWidth: 3,
                    pointHoverRadius: 6,
                    pointHoverBackgroundColor: "#5c5c5c",
                    pointHoverBorderColor: "#5c5c5c",
                    pointHoverBorderWidth: 2,
                    pointRadius: 5,
                    pointHitRadius: 5,
                    data: obj.data[val].previousMonth,
                },
                {
                    label: "Given Month",
                    fill: true,
                    lineTension: 0.1,
                    backgroundColor: obj.data[val].backgroundColor,
                    borderColor: "#d6d6d6",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    lineTension: 0,
                    pointBackgroundColor: "#5c5c5c",
                    pointBorderColor: "#d6d6d6",
                    pointBorderWidth: 3,
                    pointHoverRadius: 6,
                    pointHoverBackgroundColor: "#d6d6d6",
                    pointHoverBorderColor: "#d6d6d6",
                    pointHoverBorderWidth: 2,
                    pointRadius: 5,
                    pointHitRadius: 5,
                    data: obj.data[val].givenMonth,
                }

            ]
    }

    txnValue = document.getElementById("txn-value").getContext('2d');
    myLineChart = new Chart(txnValue, {
        type: 'line',
        data: txnValueChartData,
        options: {
            legend: {
                display: false
            },
            title: {
                display: false,
                text: "Transaction Value (PHP)",
                position:'left'
            },
            tooltips: {
                mode: 'label',
            },
            responsive: true,
            scales: {
                xAxes: [{
                    // stacked: true,
                    gridLines : {
                        display : false,
                    },
                    ticks: {
                        beginAtZero: true,
                        stepSize: 50,

                    },
                    // display: false,
                }],
                yAxes: [{
                    // stacked: true,
                    ticks: {
                        beginAtZero: true
                    },
                    gridLines : {
                        display : false
                    },
                }]
            }
        }
    });
}

var datasets = {
    data: [
        {
            backgroundColor: 'rgba(212, 31, 57, 1)',
            givenMonth:[{{$transactionValueData['earn']['givenMonth']}}],
            previousMonth:[{{$transactionValueData['earn']['previousMonth']}}]
        }
    ]
}
initTxnValue(datasets,0);

/* Txn Frq Value */
function initTxnFreqValue(obj,val){
    var txnFrqValueChartData = {
            labels: ["M", "T", "W", "TH", "F", "SAT", "SUN"],
            datasets: [
                {
                    label: "Previous Month",
                    fill: true,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(213, 213, 213, 0.5)',
                    borderColor: "#5c5c5c",
                    borderCapStyle: 'butt',
                    borderDash: [6],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    lineTension: 0,
                    pointBackgroundColor: "#ffffff",
                    pointBorderColor: "#5c5c5c",
                    pointBorderWidth: 3,
                    pointHoverRadius: 6,
                    pointHoverBackgroundColor: "#5c5c5c",
                    pointHoverBorderColor: "#5c5c5c",
                    pointHoverBorderWidth: 2,
                    pointRadius: 5,
                    pointHitRadius: 5,
                    data: obj.data[val].previousMonth,
                },
                {
                    label: "Given Month",
                    fill: true,
                    lineTension: 0.1,
                    backgroundColor: obj.data[val].backgroundColor,
                    borderColor: "#d6d6d6",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    lineTension: 0,
                    pointBackgroundColor: "#5c5c5c",
                    pointBorderColor: "#d6d6d6",
                    pointBorderWidth: 3,
                    pointHoverRadius: 6,
                    pointHoverBackgroundColor: "#d6d6d6",
                    pointHoverBorderColor: "#d6d6d6",
                    pointHoverBorderWidth: 2,
                    pointRadius: 5,
                    pointHitRadius: 5,
                    data: obj.data[val].givenMonth,
                }

            ]
    }

    txnFrqValue = document.getElementById("txn-freq-value").getContext('2d');
    myLineChart2 = new Chart(txnFrqValue, {
        type: 'line',
        data: txnFrqValueChartData,
        options: {
            legend: {
                display: false
            },
            title: {
                display: false,
                text: "Transaction Value (PHP)",
                position:'left'
            },
            tooltips: {
                mode: 'label',
            },
            responsive: true,
            scales: {
                xAxes: [{
                    // stacked: true,
                    gridLines : {
                        display : false,
                    },
                    ticks: {
                        beginAtZero: true,
                        stepSize: 50,

                    },
                    // display: false,
                }],
                yAxes: [{
                    // stacked: true,
                    ticks: {
                        beginAtZero: true
                    },
                    gridLines : {
                        display : false
                    },
                }]
            }
        }
    });
}

var txnfreqdatasets = {
    data: [
        {
            backgroundColor: 'rgba(212, 31, 57, 1)',
            givenMonth:[{{$transactionFrequencyData['earn']['givenMonth']}}],
            previousMonth:[{{$transactionFrequencyData['earn']['previousMonth']}}]
        },
        {
            backgroundColor: 'rgba(92, 92, 92, 1)',
            givenMonth:[{{$transactionFrequencyData['redeem']['givenMonth']}}],
            previousMonth:[{{$transactionFrequencyData['redeem']['previousMonth']}}]
        }
    ]
}
initTxnFreqValue(txnfreqdatasets,0);

$('.c-select').on('click',function(){
    $(this).find('.pie-legends').toggle();
})
$('.txn-update-set').on('click',function(e){
    myLineChart.destroy();
    $(this).closest('.c-select').find('label').attr('class','label-'+(parseInt($(this).data('dataset'))+1)).text($(this).text())
    initTxnValue(datasets,$(this).data('dataset'));
})
$('.txn-freq-update-set').on('click',function(e){
    myLineChart2.destroy();
    $(this).closest('.c-select').find('label').attr('class','label-'+(parseInt($(this).data('dataset'))+1)).text($(this).text())
    initTxnFreqValue(txnfreqdatasets,$(this).data('dataset'));
})
</script>