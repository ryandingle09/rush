<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body style="padding: 0; margin: 0; font-family: Arial, Helvetica, sans-serif; line-height: 23px;">
        <table cellpadding="0" cellspacing="0" align="center" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">
            <tr>
                <td align="center">
                    <table cellpadding="0" cellspacing="0" align="left" width="100%" style="max-width: 400px; padding: 10px 25px 15px;">
                        <tr>
                            <td><img src="{{$merchantLogo}}" border="0" alt="Logo" style="display: block;width:100px;height:100px;"></td>
                        </tr>
                    </table>
                    <table cellpadding="0" cellspacing="0" align="center" width="100%" style="max-width: 200px; padding: 10px 25px 5px;">
                        <tr>
                            <td align="center" style="font-size: 11px;"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <img src="{{$banner}}" border="0" alt="Banner" width="100%" id="putbanner" style="display: block;">
                </td>
            </tr>
            <tr>
                <td align="center">
                    <h2 style="margin-top: 50px; margin-bottom: 0; text-transform: uppercase; color: #393939; font-weight: 100;" id="putsubject">{{$subject}}</h2>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <p style="padding: 0; margin: 10px auto 20px; border-top: solid 3px #a3080c; width: 26px;">&nbsp;</p>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table cellpadding="0" cellspacing="0" align="center" width="100%" style="padding: 0 10%;">
                        <tr>
                            <td align="center" style="color: #636363; padding-bottom: 15px; font-size: 18px;">Dear <span id="putname"></span>{{$salutation}}</td>
                        </tr>
                        <tr>
                            <td align="center" style="color: #838181; font-size: 15px; padding: 0 0 60px;" id="putmessage">{!!$emailMessage!!}</td>
                        </tr>
                    </table>
                </td>
            </tr>
            @if(!isset($viewMode))
            <tr>
                <td align="center">
                    <table cellpadding="0" cellspacing="0" align="left" width="100%" style="max-width: 400px; padding: 10px 25px 15px;">
                        <tr>
                            <td></td>
                        </tr>
                    </table>
                    <table cellpadding="0" cellspacing="0" align="center" width="100%" style="max-width: 200px; padding: 10px 25px 5px;">
                        <tr>
                            <td align="center" style="font-size: 11px;"><a href="@if(isset($id)){{MigrationHelper::getAppBaseUrl()."e/$id/$slug"}}@endif" style="color: #a2a2a2; text-decoration: none;">Can't load? View in browser</a></td>
                        </tr>
                    </table>
                </td>
            </tr>
            @endif
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" align="center" width="100%" style="padding: 10px 0 30px; background: #f8f8f8;">
                        <tr>
                            <td align="center" style="padding: 25px 0 0; color: #a6a6a6; font-size: 11px;">Powered by <img src="{{URL::asset('/assets/images/rush-foot-logo.png')}}" border="0" alt="RUSH" style="display: inline-block; vertical-align: text-top;"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script>
        var url = window.location.href;
        var urlval = url.split('?');

        $('#putsubject').html(unescape((urlval[1].split('='))[1]));
        $('#putmessage').html(unescape((urlval[2].split('='))[1]));
        $('#putbanner').attr('src', (urlval[3].split('='))[1]);
        $('#putname').html(unescape((urlval[4].split('='))[1]));

    </script>
</html>
