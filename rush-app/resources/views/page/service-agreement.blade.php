<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="Fidelity" content="">
    <title>RUSH | Service Agreement</title>
    <link rel="stylesheet" href="{{ url('app/page/service-agreement/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('app/page/service-agreement/css/bootstrap-theme.min.css') }}"/>
    <link rel="stylesheet" href="{{ url('app/page/service-agreement/css/font-awesome.min.css') }}"/>
    <link rel="stylesheet" href="{{ url('app/page/service-agreement/stylesheets/style.css') }}"/>
    <link rel="stylesheet" href="{{ url('app/page/service-agreement/css/chosen.css') }}"/>

    <script src="{{ url('app/page/service-agreement/js/jquery-1.11.1.min.js') }}"></script>
    <script src="{{ url('app/page/service-agreement/js/bootstrap.min.js') }}"></script>
    <script src="{{ url('app/page/service-agreement/js/chosen.jquery.js') }}"></script>
    <script src="{{ url('app/page/service-agreement/js/custom.js') }}"></script>
</head>
<body>

<div id="wrapper">
    <section class="content revamp">
        <div class="container">
                <p id="introduction">THIS AGREEMENT GOVERNS YOUR ACQUISITION AND USE OF THE SOLUTION AND/OR THE SERVICES.</p>
                <p class="special"></p>
                <p>IF YOU REGISTER FOR A FREE TRIAL OF THE SOLUTION AND/OR THE SERVICES, THE PROVISIONS OF THIS AGREEMENT WILL ALSO GOVERN THAT FREE TRIAL.</p>    
                <p class="special"></p>
                <p>BY ACCEPTING THIS AGREEMENT, BY CLICKING “I ACCEPT”, YOU AGREE TO THE TERMS OF THIS AGREEMENT. IF YOU ARE ENTERING INTO THIS AGREEMENT ON BEHALF OF A COMPANY OR OTHER LEGAL ENTITY, YOU REPRESENT THAT YOU HAVE THE AUTHORITY TO BIND SUCH ENTITY AND ITS AFFILIATES TO THE TERMS AND CONDITIONS OF THIS AGREEMENT, IN WHICH CASE THE TERMS "YOU" OR "YOUR" SHALL REFER TO SUCH ENTITY AND ITS AFFILIATES. IF YOU DO NOT HAVE SUCH AUTHORITY, OR IF YOU DO NOT AGREE WITH THESE TERMS AND CONDITIONS, YOU MUST NOT ACCEPT THIS AGREEMENT AND MAY NOT USE THE SOLUTION AND/OR THE SERVICES.</p>
                <p class="special"></p>
                <p>You may not use the Solution or access the Services if You are Our direct competitor, except with Our prior written consent. In addition, You may not use the Solution or access the Services for purposes of monitoring their availability, performance or functionality, or for any other benchmarking or competitive purposes.</p>
                <p class="special"></p>
                <p>This Agreement was last updated on <a>January 1, 2018</a>. It is effective between You and Us as of the date of You accepting this Agreement. We reserve the right to revise and modify this Agreement as the Solution or Services may reasonably require according to Our sole discretion. Said revisions or modifications shall be posted in the RUSH website (rush.ph) and, once published therein, shall become binding on You. It shall be your obligation to be informed thereof by accessing, from time to time, such website where the latest version of this Agreement may be found. Your continued use of the Solution or the Services shall be deemed an acceptance of any revision or modification of this Agreement.</p>
                <h3 id="definitions">1. Definitions</h3>
                <p class="special"></p>
                    <table style="border: 1px solid #dcdcdc; color:#6b6c6c;">
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Affiliate</td>
                            <td style="padding: 10px 8px 10px 8px;">Any entity that directly or indirectly controls, is controlled by, or is under common control with the subject entity. "Control," for purposes of this definition, means direct or indirect ownership or control of more than 50% of the voting interests of the subject entity.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Agreement</td>
                            <td style="padding: 10px 8px 10px 8px;">This Agreement for the provision of the Solution and/or the Services to the Merchant and its End-User(s).</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Ancillary Fees</td>
                            <td style="padding: 10px 8px 10px 8px;">Amounts due to Globe for availing of any optional add-ons, such as, but not limited to, App Zero-rating.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Authorized Company Representative</td>
                            <td style="padding: 10px 8px 10px 8px;">The duly recognized representative of the Merchant who is authorized to transact with Globe on behalf of the Merchant.</td>
                        </tr>
                            <tr style="border-bottom:1px solid #dcdcdc;">
                                <td width="20%" class="definitions-text">Component</td>
                                <td style="padding: 10px 8px 10px 8px;">Refers to any or all of the following channels for accessing the various features of the Loyalty Program: 
                                    <ol>
                                        <li>(1) Merchant Mobile Application,</li>
                                        <li>(2) Merchant Dashboard,</li>
                                        <li>(3) Customer Mobile Application, or</li>
                                        <li>(4) Customer Web Portal.</li>
                                    </ol>
                                </td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Confidential Information</td>
                            <td style="padding: 10px 8px 10px 8px;">Shall include, but not be limited to, products or planned products, processes and/or procedures, technological achievements and interests, customers and potential customers, business prospects, financial statements and information, financial situation and corporate plans, internal activities, future plans of both parties, and other information deemed proprietary or confidential by the Disclosing Party or any other matter in which the Disclosing Party may have any interest whatsoever.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Content Management System (CMS)</td>
                            <td style="padding: 10px 8px 10px 8px;">The tool within the Merchant Dashboard which enables Merchant to blast SMS or Mobile App Notifications, among others.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Customer</td>
                            <td style="padding: 10px 8px 10px 8px;">Refers to Merchant’s customer who purchases and/or avails of Merchant’s goods and/or services.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Customer Database</td>
                            <td style="padding: 10px 8px 10px 8px;">The repository for all information extracted from Merchant and End-Users via the Loyalty Program using the Solution.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Customer Mobile Application</td>
                            <td style="padding: 10px 8px 10px 8px;">Refers to the Component of a Loyalty Program that allows Customers to, among others, enroll in the Loyalty Program, earn Points, track Points, and burn Points. This is also alternatively referred to as “Customer App.”</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Customer Registration</td>
                            <td style="padding: 10px 8px 10px 8px;">The action by Member that entails the download and registration for use of the Customer Mobile App. Merchant will be charged a one-time fee for each Member registered.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Data</td>
                            <td style="padding: 10px 8px 10px 8px;">Any Merchant, Customer, Member, or End-User information that will be collected through or by the Solution.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Data Analytics</td>
                            <td style="padding: 10px 8px 10px 8px;">The feature of the Solution which converts raw data into useful charts and output for better analysis of Merchant.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Employee</td>
                            <td style="padding: 10px 8px 10px 8px;">Merchant’s employee authorized by Merchant to use the Solution for Merchant’s Loyalty Program.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">End-user/s</td>
                            <td style="padding: 10px 8px 10px 8px;">Either a Member or Employee that uses the Solution in relation to the Loyalty Program and who has a unique account accessible via his/her own username and password.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Free Trial Inclusions</td>
                            <td style="padding: 10px 8px 10px 8px;">The free trial includes use of the RUSH service, excluding add-ons, with all the Transaction Fees within the first thirty (30) days waived, starting from issuance by RUSH of temporary log-in credentials for the Merchant’s account.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Gross Transaction Amount</td>
                            <td style="padding: 10px 8px 10px 8px;">The selling price of the purchased product or service with use of the loyalty Solution (either Points Earning or Points Burning).</td>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Initial Activation</td>
                            <td style="padding: 10px 8px 10px 8px;">The instance when the Merchant Mobile Application, the Customer Mobile Application, and Web Portals are ready for use by the Merchant and Customers respectively.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Loyalty Program</td>
                            <td style="padding: 10px 8px 10px 8px;">The loyalty program defined by the Merchant in the Program Mechanics using the Solution.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Member</td>
                            <td style="padding: 10px 8px 10px 8px;">Merchant’s Customer that is enrolled in Merchant’s Loyalty Program and using the Solution.</td>
                        </tr>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Merchant</td>
                            <td style="padding: 10px 8px 10px 8px;">The company or other legal entity availing of the loyalty Solution.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Merchant Account</td>
                            <td style="padding: 10px 8px 10px 8px;">The login credentials of a Merchant to the Solution which includes, but is not limited to, username, password, profile details, and program information. This account can access the Merchant Dashboard where Employees will be enrolled and given individual Employee accounts.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Merchant Dashboard</td>
                            <td style="padding: 10px 8px 10px 8px;">This refers to the management system Component of the Solution that allows a Merchant to create and control the Loyalty Program and the Solution. This is alternatively referred to as the “Merchant Web Portal.”</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Merchant Mobile Application</td>
                            <td style="padding: 10px 8px 10px 8px;">This refers to the Component of the Solution that allows Merchant’s Employees to, among others, access Customer/Member accounts, seed Points, and accept Points for payment. This is alternatively referred to as “Merchant App.”</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Merchant-branded/branding</td>
                            <td style="padding: 10px 8px 10px 8px;">Items, content, interface, and the like that makes use of Merchant’s markings (including, but not limited to, logos, trademarks, copyrights, etc.).</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Mobile App Notifications</td>
                            <td style="padding: 10px 8px 10px 8px;">In-app alerts, badges, and notifications which inform Member of activities including, but not limited to, transactions and promos.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Party</td>
                            <td style="padding: 10px 8px 10px 8px;">Refers individually either to Merchant or to Globe Telecom, Inc.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Parties</td>
                            <td style="padding: 10px 8px 10px 8px;">Refer collectively to Merchant and Globe Telecom, Inc.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Password</td>
                            <td style="padding: 10px 8px 10px 8px;">A system-generated or user-generated security key for a Merchant or End-User’s Account that enables the Merchant and the End-User access into at least one of the Components of the Loyalty Program using the Solution.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Personal Information</td>
                            <td style="padding: 10px 8px 10px 8px;">An End-User’s credentials including, but not limited to, name, age, gender, mobile number, email address, and other information from which the identity  of an End-User is apparent or can reasonably and directly be ascertained, or when  put together with other information would directly and certainly identify an End-User.
                            </td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Points</td>
                            <td style="padding: 10px 8px 10px 8px;">Refers to the currency within a loyalty program or system which can be used in accordance with the Program Mechanics defined by the Merchant for its Loyalty Program using the Solution.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Points Burning</td>
                            <td style="padding: 10px 8px 10px 8px;">Refers to the process of using Points as payment or for redemption of reward/s using Points in accordance with the Program Mechanics.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Points Earning</td>
                            <td style="padding: 10px 8px 10px 8px;">Refers to the process of accumulating Points in accordance with the Program Mechanics.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Points Seeding</td>
                            <td style="padding: 10px 8px 10px 8px;">The crediting of points by Merchant or Employee to a Member’s account in accordance with the Loyalty Program.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Program Mechanics</td>
                            <td style="padding: 10px 8px 10px 8px;">The rules or methodologies set by Merchant for enrolling and/or registering to the Loyalty Program, Points Earning, Points Seeding, and Points Burning.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Service Fee (SF)</td>
                            <td style="padding: 10px 8px 10px 8px;">The monthly amount due to Globe from Merchant arising from use of the Solution which includes Transaction Fees and (if any) Ancillary Fees.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Incident Service Level Agreement (SLA)</td>
                            <td style="padding: 10px 8px 10px 8px;">Refers to the agreement detailing the required response time in case of unexpected downtime of the Solution as described in Annex 2.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Short Message Service (SMS)</td>
                            <td style="padding: 10px 8px 10px 8px;">RUSH’s CMS tool enables this function.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Sender Identification (ID)</td>
                            <td style="padding: 10px 8px 10px 8px;">The label that will appear as sender when Merchant sends SMS to its Customers consisting of 11-alphanumeric characters (no special characters such as space, comma, period, etc.).</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Services</td>
                            <td style="padding: 10px 8px 10px 8px;">The provision of a white-label loyalty suite which includes the Solution or Software as well as any other services which may be agreed upon by the parties.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Solution/s or Software</td>
                            <td style="padding: 10px 8px 10px 8px;">Refers to RUSH Platform and its Components  as described in Annex 1: Features </td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Subscription</td>
                            <td style="padding: 10px 8px 10px 8px;">The right to access the Solution at corresponding fees for a defined period of time.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Transaction Log</td>
                            <td style="padding: 10px 8px 10px 8px;">The repository for all transactions made using the Loyalty Program Solution (Points Seeding, Points Earning, payment with Points, etc.).</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Free Trial Period</td>
                            <td style="padding: 10px 8px 10px 8px;">The first 30-days starting from the issuance by RUSH of temporary log-in credentials for the Merchant’s Account wherein all Transaction Fees for the period are waived. All other applicable Ancillary Fees will apply unless otherwise agreed upon by the parties.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Us or We or Our</td>
                            <td style="padding: 10px 8px 10px 8px;">Globe Telecom, Inc.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Username</td>
                            <td style="padding: 10px 8px 10px 8px;">A unique identifier for a user’s account in the loyalty Solution.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">You or Your</td>
                            <td style="padding: 10px 8px 10px 8px;">The Merchant or the company or other legal entity for which you are accepting this Agreement including Affiliates of that company.</td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text">Zero-rating</td>
                            <td style="padding: 10px 8px 10px 8px;">The service of making the Customer Mobile Application accessible without WiFi or mobile <data class=""></data></td>
                        </tr>
                    </table>
                <h3 id="free-trial">2. Free Trial</h3>
                <p class="special"></p>
                <ul>
                    <li>2.1. By signing up for a RUSH free trial, We will make the RUSH Solution available to You and your End-Users during the Free Trial Period and waive all Transaction Fees during the Free Trial Period. </li>
                    <p class="special"></p>
                    <li>2.2 The Free Trial Period will run for a period of thirty (30) days starting from issuance by RUSH of temporary log-in credentials for the Merchant’s Account.</li>
                    <p class="special"></p>
                    <li>2.3. Merchant Account credentials for login will be transmitted electronically via email to Your Authorized Company Representative.</li>
                    <p class="special"></p>
                    <li>2.4. Any Services availed beyond the Free Trial Inclusions will be charged accordingly. For any such Services, We will issue You a Statement of Account (SOA) via the Merchant Dashboard or Your Authorized Company Representative’s email address for applicable fees incurred which must be settled within seven (7) calendar days from issuance of SOA. If fees are not settled by due date, We will terminate Your account and delete all data associated with such.  Fees and Payment Terms can be found in Section 5.</li>
                    <p class="special"></p>
                    <li>2.5. Your failure to request for termination of Your account on or before the end of the Free Trial Period will automatically trigger the continuation of your Merchant Account into a full Subscription of RUSH upon the lapse of the Free Trial Period. Accordingly, you will be liable to pay the Service Fees which may include Transaction Fees and any applicable Ancillary Fees.</li>
                    <p class="special"></p>
                    <li>2.6. Your Merchant Account can be terminated via Merchant Dashboard or email to RUSH (support@rush.ph) at any time. Your termination of Your Merchant Account will not relieve you of any fees payable to us for Your use of the Solution and/or Services.</li>
                    <p class="special"></p>
                    <li>2.7. Any data collected by the Solution during Free Trial Period will be permanently deleted unless You continue into a full subscription of RUSH.</li>
                </ul>
                <h3 id="our-responsibilities">3. Our Responsibilities</h3>
                <p class="special"></p>
                <ul>
                    <li>3.1. Provision of Services. We will (a) make the Services available to You pursuant to this Agreement, (b) provide applicable maintenance and support for the Solution to You, and (c) use commercially reasonable efforts to make the Services available 24 hours a day, 7 days a week, except for: (i) planned downtime (of which We shall give advanced electronic notice), and (ii) any unavailability caused by circumstances beyond Our reasonable control, including, for example, an act of God, act of government, flood, fire, earthquake, civil unrest, act of terror, strike, or other labor problem , internet service provider failure or delay, or denial of service attack.</li>
                    <p class="special"></p>
                    <li>3.2. Protection of Your Data. We will maintain administrative, physical, and technical safeguards for protection of the security, confidentiality and integrity of Your Data. Those safeguards will include, but will not be limited to, measures for preventing access, use, modification or disclosure of Your Data by Our personnel except (a) to provide the Services and prevent or address service or technical problems, and (b) when compelled by law.</li>
                </ul>
                <h3 id="use-of-services">4. Use of the Solution and Services</h3>
                <p class="special"></p>
                <ul>
                    <li>4.1. Subscription. The right to access and/or use the RUSH package will be granted via a full subscription for a corresponding Service Fee monthly, unless such access and/or use is done within the Free Trial Period.</li>
                    <p class="special"></p>
                    <li>4.2. Terms of Use. End-Users of the Solution will be governed by the corresponding Terms of Use which they need to agree to, in order to use the Solution.</li>
                    <p class="special"></p>
                    <li>4.3. Your responsibilities.</li>
                    <p class="special"></p>
                        <ol style="padding-left: 20px;" type="A">
                            <li><h5>4.3.1. Management of the Solution and the Loyalty Program.</h5></li>
                            <p class="special"></p>
                                <ol style="padding-left: 20px;" type="1">
                                    <li>4.3.1.1. You shall be responsible for setting up, managing, and modifying your Loyalty Program, which includes but is not limited to defining the Program Mechanics, uploading mobile application and web user interface design assets and rewards catalogue, and using the functions and tools available in the Merchant Dashboard.</li>
                                    <p class="special"></p>
                                    <li>4.3.1.2. You shall be responsible for providing access to the Solution to Your Employees via the Merchant Dashboard.</li>
                                    <p class="special"></p>
                                    <li>4.3.1.3. You warrant that any user who makes any transaction or modification to any of Your Loyalty Program Components is authorized to do so.</li>
                                    <p class="special"></p>
                                    <li>4.3.1.4. You shall be solely responsible for any and all necessary government or legal permits required to offer your Loyalty Program to your Customers.</li>
                                </ol>
                            <p class="special"></p>
                            <li><h5>4.3.2. Branding and Marketing.</h5></li>
                            <p class="special"></p>
                                <ol style="padding-left: 20px;" type="1">
                                    <li>4.3.2.1. You grant Globe permission to use your trademarks, logos, service marks, or other designations for the design and branding of the white-label solution and promotion of the your Loyalty Program and Solution  for the Term of the Subscription.</li>
                                    <p class="special"></p>
                                    <li>4.3.2.2. You undertake and warrant that you shall provide and grant access to non-infringing branding materials and/or content to Globe for the Solution’s branding and/or aesthetic purposes. In the event of a claim that any content submitted infringes any intellectual property, you undertake and warrant that you shall hold Globe free, harmless, and indemnified against any and all liability arising from, in relation to, or in connection with such claim.</li>
                                    <p class="special"></p>
                                    <li>4.3.2.3. You are responsible for the content and transmission of your messages to End-Users via Content Management System (CMS), which includes but is not limited to SMS and Mobile App Notifications. You agree to abide by the applicable laws, rules, regulations, guidelines and policies of government including, but not limited to, the National Telecommunications Commission (NTC) relating to broadcast messaging services or SMS to End-Users for advisory, advertising, and promotional uses. Accordingly, you hereby undertake to hold Globe free, harmless and indemnified against any and all liabilities, of whatever nature, arising from, in relation to, or in connection with your violation of or failure to comply with applicable laws, rules, regulations guidelines and policies of the government.</li>
                                    <p class="special"></p>
                                    <ol style="padding-left: 20px;">
                                        <li>4.3.2.3.1. You can send promotional messages via CMS Tool only to Members within the database of Customers registered to Your Loyalty Program and enrolled in RUSH.</li>
                                        <p class="special"></p>
                                        <li>4.3.2.3.2. App Notifications.</li>
                                        <p class="special"></p>
                                        <ol style="padding-left: 30px;">
                                            <li>4.3.2.3.2.1. You agree not to publish content containing or depicting any of the following:</li>
                                            <p class="special"></p>
                                            <ol style="padding-left: 40px;">
                                                <li>4.3.2.3.2.1.1. Adult Content - any advertisement containing, depicting, or promoting nudity/ profanity/ violence;</li>
                                                <p class="special"></p>
                                                <li>4.3.2.3.2.1.2. Alcohol - any advertisement promoting the use of alcohol;</li>
                                                <p class="special"></p>
                                                <li>4.3.2.3.2.1.3. Drugs - any advertisement depicting or promoting the use of drugs and other illegal substances;</li>
                                                <p class="special"></p>
                                                <li>4.3.2.3.2.1.4. Gambling - any advertisement promoting gambling; and</li>
                                                <p class="special"></p>
                                                <li>4.3.2.3.2.1.5. Tobacco - any advertisement promoting the use of tobacco.</li>
                                            </ol>
                                            <p class="special"></p>
                                            <li>4.3.2.3.2.2. You agree to limit Mobile App Notifications to two (2) per day per Member.</li>
                                        </ol>
                                        <p class="special"></p>
                                        <li>4.3.2.3.3. SMS Blast/Messaging.</li>
                                        <p class="special"></p>
                                        <ol style="padding-left: 30px;">
                                            <li>4.3.2.3.3.1. You will be charged PHP0.50 for every 160-character SMS sent.</li>
                                            <p class="special"></p>
                                            <li>4.3.2.3.3.2. You are responsible for inputting Sender Identification that will be used as Your identifier when sending SMS notifications to Your Members.</li>
                                            <p class="special"></p>
                                            <li>4.3.2.3.3.3. You agree not to publish content containing any of the following:</li>
                                            <p class="special"></p>
                                            <ol style="padding-left: 40px;">
                                                <li>4.3.2.3.3.3.1. Adult Content - any advertisement containing, depicting, or promoting nudity/profanity/ violence;</li>
                                                <p class="special"></p>
                                                <li>4.3.2.3.3.3.2.  Alcohol - any advertisement promoting the use of alcohol;</li>
                                                <p class="special"></p>
                                                <li>4.3.2.3.3.3.3.  Drugs - any advertisement depicting or promoting the use of drugs and other illegal substances;</li>
                                                <p class="special"></p>
                                                <li>4.3.2.3.3.3.4.  Gambling - any advertisement promoting gambling; and</li>
                                                <p class="special"></p>
                                                <li>4.3.2.3.3.3.5.  Tobacco -  any advertisement promoting the use of tobacco.</li>
                                            </ol>
                                            <p class="special"></p>
                                            <li>4.3.2.3.3.4. You agree to send Your members SMSs from 7AM - 7PM only.</li>
                                            <p class="special"></p>
                                            <li>4.3.2.3.3.5. You agree to be limited to two (2) SMS per week per Member.</li>
                                            <p class="special"></p>
                                            <li>4.3.2.3.3.6. You agree that the maximum character count per SMS is 459 characters.</li>
                                        </ol>
                                    </ol>
                                    <p class="special"></p>
                                    <li>4.3.2.4. You will neither use nor allow or cause the Service to be used to post, transmit, distribute, link to, or solicit content that creates a false identity for the purpose of misleading others as to the identity of the sender or the origin of a message.</li>
                                </ol>
                            <p class="special"></p>
                            <li><h5>4.3.3. Content, Data, and Security.</h5></li>
                            <p class="special"></p>
                                <ol style="padding-left: 20px;" type="1">
                                    <li>4.3.3.1. You  agree and warrant that you shall not upload, post, email or otherwise transmit any content that:</li>
                                    <p class="special"></p>
                                        <ol style="padding-left: 20px;" type="a">
                                            <li>4.3.3.1.1.  Is unlawful, harmful, threatening, abusive, harassing, tortious, defamatory, indecent, offensive, vulgar, obscene, libelous, invasive of another’s privacy, hateful, defamatory or racially, ethnically objectionable; and</li>
                                            <p class="special"></p>
                                            <li>4.3.3.1.2.  Infringes any copyrights patent, trademark, trade secret, copyright or any other intellectual property, personal rights of any person, or violates any obligation of confidence or any other proprietary right of any party; violates any regulatory requirement or codes applicable under Philippine Laws, or other material protected by intellectual property laws, rights of privacy or publicity or any other applicable law.</li>
                                        </ol>
                                    <p class="special"></p>
                                    <li>4.3.3.2. You consent that We may use End-User data for planning, research, design, and marketing of the Solution and/or the Services or any future product and/or service of Globe and its subsidiaries and affiliates. You acknowledge that upon access to the Solution, We may collect and process the following information:</li>
                                    <p class="special"></p>
                                        <ol style="padding-left: 20px;" type="1">
                                            <li>4.3.3.2.1.  Anonymous web and mobile application statistics collected as End-users browse and use the Solution/s</li>
                                            <p class="special"></p>
                                            <li>4.3.3.2.2.  Personal Information that the Merchant and its End-Users and Customers knowingly provide via the Solution.</li>
                                        </ol>
                                    <p class="special"></p>
                                    <li>4.3.3.3. Username and password.</li>
                                    <p class="special"></p>
                                        <ol style="padding-left: 20px;" type="1">
                                            <li>4.3.3.3.1. After sign-up, You will be issued a temporary password via the email address of your Authorized Company Representative to access Your account. Once You have accessed Your account, You will be prompted to change Your password to one that You prefer.</li>
                                            <p class="special"></p>
                                            <li>4.3.3.3.2. You understand that while the Solution has various internal security measures designed to protect its users, You hereby assume sole responsibility for the security of Merchant Account, as well as the confidentiality of any email address, Username and/or Password associated with such email and/or Merchant Account. All access to the Solution and any Service availed using such email address, Username and/or Password shall be presumed made or authorized by You.  Accordingly, you shall ensure that access to the Solution shall only be limited to the designated individuals whose credential are enrolled to minimize the probability of hacking and/or fraudulent or unauthorized transactions.</li>
                                            <p class="special"></p>
                                            <li>4.3.3.3.3. You and Your Employees shall not disclose to any person the password to login and shall not permit or authorize any other person to use the login credentials and password (access security) for any purpose whatsoever. You shall hold Globe free, harmless and indemnified against  any and all damage, claim, or liability arising from the failure to enforce such access security policy by institutionalizing login and password security policy. Globe shall not be liable for security breaches for Your failure to implement the same. </li>
                                            <p class="special"></p>
                                            <li>4.3.3.3.4. You shall adopt a policy, whereby your Employees upon learning that any other person has acquired knowledge of the password to login or has used the password provided to access or use the Solution or effect any transaction (whether with or without prior knowledge and consent), You or Your Employee shall:</li>
                                            <p class="special"></p>
                                                <ol style="padding-left: 30px;" type="A">
                                                    <li>4.3.3.3.4.1. Immediately notify  Globe thereof (within 2 hours from suspicion);</li>
                                                    <p class="special"></p>
                                                    <li>4.3.3.3.4.2. Provide Globe any other information relating thereto as Globe may require so it may suspend log-in credentials and supply temporary credentials; and</li>
                                                    <p class="special"></p>
                                                    <li>4.3.3.3.4.3. Change the temporary password as desired. </li>
                                                </ol>
                                            <p class="special"></p>
                                            <li>4.3.3.3.5. Merchant shall be bound by any transaction effected by any person using the password prior to Globe’s receipt of notice thereof under Section 4.3.3.3.4.1 above or prior to Merchant changing the password as required under Section 4.3.3.3.4.3, whichever comes first.</li>
                                            <p class="special"></p>
                                            <li>4.3.3.3.6. You agree to use the Solution in a manner consistent with all applicable laws and regulations and are solely responsible for all acts or omissions that occur under Your or Your employees’ respective account or password.</li>
                                            <p class="special"></p>
                                            <li>4.3.3.3.7. Merchant and its Employees will exercise the highest degree of effort to keep usernames and password private and exclusive for use on a one-is-to-one basis. </li>
                                        </ol>
                                    <p class="special"></p>
                                    <li>4.3.3.4. Parties shall adhere to and abide by applicable data privacy and data protection laws, rules, and  regulations, and Globe’s data privacy policies.</li>
                                    <p class="special"></p>
                                    <li>4.3.3.5. Parties shall obtain all required consents under the applicable privacy and data protection law before providing Personal Information. This includes the obligation to obtain the End User’s acceptance of the Solution. Parties shall exercise strict confidentiality and employ reasonable measures to ensure to hold such customer data information with strictest confidence in accordance with this Agreement.</li>
                                    <p class="special"></p>
                                    <li>4.3.3.6. You acknowledge that We may use Your and your Members’ Personal Information to the extent necessary to comply with the requirements of the law and legal processes.</li>
                                    <p class="special"></p>
                                    <li>4.3.3.7. You allow Us to access and disclose data from, about or related to you, pursuant to any order of any court or tribunal, to any law enforcement or other government authority , court or tribunal.</li>
                                </ol>
                            <p class="special"></p>
                            <li>4.3.4. You shall pay Us the service fees as stated in the SOA in accordance with the payment terms.</li>
                            <p class="special"></p>
                            <li>4.3.5. You agree and understand that We may update/change the Software version from time to time as part of Software upgrades.</li>
                            <p class="special"></p>
                            <li>4.3.6. Subject to the terms and conditions of the Agreement, You accept/consent that some portions of the Solution may require that Personal Information be provided by End-users. Such Personal Information refers to any and all information relating to the End Users (including, but not limited to the name, contact number, email address, mailing address, and transaction information) obtained via access to or use of the Solutions.</li>
                            <p class="special"></p>
                            <li>4.3.7. You shall allow Us to gain access to data tied to Your account as required for operations.</li>
                        </ol>
                </ul>
                <p class="special"></p>
                <h3 id="fees-and-payment-terms">5. Fees and Payment Terms</h3>
                <div class="maxWidth">
                    <table style="border: 1px solid #dcdcdc; color:#6b6c6c;" width="100%">
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text" style="border-right: solid 1px #dcdcdc;">Service Fee/s</td>
                            <td style="padding: 10px 8px 10px 8px;">
                                <p>Comprises of Monthly Fee and Ancillary Fees</p>
                                <ul style="padding-left: 20px;">
                                    <li style="list-style: disc;">Monthly Service Fee (per loyalty program)</li>
                                        <ol style="padding-left: 20px;" type="A">
                                            <p class="special"></p>
                                            <li style="list-style: circle;">Tiered price below depends on total number of members. <br>Below price is inclusive of vat. Comes with free SMS</li>
                                            <p class="special"></p>
                                            <table width="96%" style="border: solid 1px #dcdcdc;">
                                                <tr>
                                                    <th width="30%" style="border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; text-align: center; padding: 2px; vertical-align: middle;"><h5>Package name</h5></th>
                                                    <th style="border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; text-align: center; padding: 2px; vertical-align: middle;"><h5># of Registered Members</h5></th>
                                                    <th style="border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; text-align: center; padding: 2px; vertical-align: middle;"><h5>Monthly Service Fee</h5></th>
                                                    <th style="border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; text-align: center; padding: 2px; vertical-align: middle;"><h5>Price Per Member</h5></th>
                                                    <th style="border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; text-align: center; padding: 2px; vertical-align: middle;"><h5>Free SMS Per Month</h5></th>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;">*RUSH Micro (Does NOT include customer mobile app)</td>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;">0 – 5,000</td>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;">Php 2,500.00</td>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;">Php 0.50</td>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;">500</td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;">RUSH Lite</td>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;">0 – 10,000</td>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;">Php 7,500.00</td>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;">Php 0.75</td>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;">1,000</td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;">RUSH Starter</td>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;">10,001 – 25,000</td>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;">Php 17,500.00</td>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;">Php 0.70</td>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;">2,500</td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;">RUSH Growth</td>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;">25,001 – 50,000</td>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;">Php 32,500.00</td>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;">Php 0.65</td>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;">5,000</td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;">RUSH Pro</td>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;">50,001 – 100,000</td>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;">Php 60,000.00</td>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;">Php 0.60</td>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;">10,000</td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;">RUSH Enterprise</td>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;">100,001 above</td>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;">Php 83,333.33</td>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;"></td>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; vertical-align: middle; text-align: center;">15,000</td>
                                                </tr>
                                            </table>
                                            <!-- old:feb8 <table width="90%" style="border: solid 1px #dcdcdc;">
                                                <tr>
                                                    <th width="50%" style="border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc; text-align: center; padding: 2px;"><h5>Members</h5></th>
                                                    <th width="50%" style="border-bottom: solid 1px #dcdcdc; text-align: center; padding: 2px;"><h5>MSF</h5></th>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc;">1 to 10000</td>
                                                    <td style="padding: 5px; border-bottom: solid 1px #dcdcdc;">&#8369;15,000</td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc;">10001 to 25000</td>
                                                    <td style="padding: 5px; border-bottom: solid 1px #dcdcdc;">&#8369;35,000</td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc;">25001 to 50000</td>
                                                    <td style="padding: 5px; border-bottom: solid 1px #dcdcdc;">&#8369;60,000</td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc;">50001 to 100000</td>
                                                    <td style="padding: 5px; border-bottom: solid 1px #dcdcdc;">&#8369;80,000</td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 5px; border-right: solid 1px #dcdcdc; border-bottom: solid 1px #dcdcdc;">> 100000</td>
                                                    <td style="padding: 5px; border-bottom: solid 1px #dcdcdc;">&#8369;100,000</td>
                                                </tr>
                                            </table> old:feb8-->
                                            <p class="special"></p>
                                            <li style="list-style: circle;">This is inclusive of sending one (1) SMS to Customer to confirm each successful transaction.</li>
                                        </ol>
                                    <p class="special"></p>
                                    <li style="list-style: disc;">Ancillary Fees for optional features – please check items that you would want to avail</li>
                                        <ol style="padding-left: 20px;" type="A">
                                            <p class="special"></p>
                                            <li style="list-style: circle;">SMS for Notifications, Registration, Points Inquiry, Points Transfer/Sharing, Referral: Php500 for every 1,000 SMS (0.50 per 160-character SMS)</li>
                                            <p class="special"></p>
                                            <li style="list-style: circle;">SMS Broadcast: Php 500 for every 1,000 SMS (Php0.50 per  160-character SMS); Can only send out to members registered in (Company Name)’s RUSH Database</li>
                                            <p class="special"></p>
                                            <li style="list-style: circle;">Reward Redemption of Globe SKUs: Corresponding fee (VAT Inc.) will be charged for each redemption of the ff SKUs via Rewards Catalogue:</li>
                                            <p class="special"></p>                                        
                                            <ol style="padding-left: 20px;" type="A">
                                                <p class="special"></p>
                                                <li style="list-style: square;">Free 10MB Surf - PHP10.00</li>
                                                <p class="special"></p>
                                                <li style="list-style: square;">Free 15MB Surf - PHP15.00</li>
                                                <p class="special"></p>
                                                <li style="list-style: square;">GOSurf50 - PHP50.00</li>
                                                <p class="special"></p>
                                            </ol>                                   
                                        </ol>
                                    <p class="special"></p>
                                    <li style="list-style: disc;">Sponsored Data Access: Php2.00 per MB</li>
                                    <p class="special"></p>
                                    <li style="list-style: disc;">Online booking (free of charge)</li>
                                </ul>
                                <p>Any customization in user experience or features may be subject to fees (Php30,000 per man-day)</p>
                            </td>
                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text" style="border-right: solid 1px #dcdcdc;">Billing</td>
                            <td style="padding: 10px 8px 10px 8px;">
                            <ul style="padding-left: 20px;">
                                <li style="list-style: disc;">At the start of each month, a Statement of Account (SOA) covering use of Solution for the previous month will be uploaded by Globe to the Merchant Dashboard and sent to Merchant via email to the Authorized Company Representative.</li>
                                <p class="special"></p>
                                <li style="list-style: disc;">The amount in the SOA must be settled by Merchant within 7 calendar days (or within 4 calendar days if payment is via check issuance). Payment may be made via cash/check deposit or credit card.</li>
                                <p class="special"></p>
                                    <ol style="padding-left: 20px;" type="A">
                                        <li style="list-style: circle;">Cash/Check Deposits</li>
                                        <p class="special"></p>
                                            <ol style="padding-left: 20px;" type="1">
                                                <li style="list-style: square;">Payment may be deposited at any BPI branch with the following details:</li>
                                                <p class="special"></p>
                                                    <ol style="padding-left: 30px;" type="a">
                                                        <li style="list-style: disc;">Bank: Bank of the Philippine Islands (BPI)</li>
                                                        <li style="list-style: disc;">Account Name: Globe Telecom, Inc.</li>
                                                        <li style="list-style: disc;">Bank Account No: 1891004601</li>
                                                        <li style="list-style: disc;">Bank Account Type: Current</li>
                                                        <li style="list-style: disc;">Reference number:</li>
                                                            <ol style="padding-left: 40px;" type="I">
                                                                <p class="special"></p>
                                                                <li style="list-style: circle;">Format: PNC/PR23/YYYYMMDD</li>
                                                                <ol style="padding-left: 20px;">
                                                                    <li style="list-style: square;"><span style="font-style: italic;"> &lt;Year of Deposit&gt; / &lt;Month of Deposit&gt; / &lt;Date of Deposit&gt;</span></li>
                                                                </ol>
                                                                <li style="list-style: circle;">Example:</li>
                                                                <ol style="padding-left: 20px;">
                                                                    <li style="list-style: square;">If payment is deposited on Feb 20, 2016 reference number will be: PNC/PR23/20160220</li>
                                                                </ol>
                                                            </ol>
                                                    </ol>
                                                <p class="special"></p>
                                                <li style="list-style: square;">Check payments must be payable to "Globe Telecom, Inc." At the back of the check, write RUSH account number, account name, authorized contact person, and telephone number.</li>
                                                <p class="special"></p>
                                                <li style="list-style: square;">Deposits via check from other issuing banks must be paid at least 3 days before due date for clearing.</li>
                                            </ol>
                                        <p class="special"></p>
                                        <li style="list-style: circle;">Credit Card</li>
                                            <ol style="padding-left: 30px;" type="A">
                                                <li style="list-style: square;">Online settlement via Merchant Dashboard</li>
                                            </ol>
                                    </ol>
                                <p class="special"></p>
                                <li style="list-style: disc;">Once payment has been made, Merchant will upload supporting documents (such as deposit or payment slip) to the Merchant Dashboard or email it to <a>billing@rush.ph</a>.</li>
                                <p class="special"></p>
                                <li style="list-style: disc;">Once payment is confirmed by Globe, Official Receipt generation will be triggered.</li>
                                <p class="special"></p>
                                <li style="list-style: disc;">Official Receipt will be uploaded by Globe to Merchant Dashboard or sent electronically via email to Authorized Company Representative at least seven (7) working days after payment confirmation.</li>
                                <p class="special"></p>
                                <li style="list-style: disc;">Free Trial Period: At the end of the 30-day Free Trial Period, Merchant will be sent a SOA for any consumed services beyond the Free Trial Inclusions. The amount must be settled within seven (7) calendar days via any of the payment processes above.</li>
                            </ul>
                            </td>

                        </tr>
                        <tr style="border-bottom:1px solid #dcdcdc;">
                            <td width="20%" class="definitions-text" style="border-right: solid 1px #dcdcdc;">Non-payment and Default</td>
                            <td style="padding: 10px 8px 10px 8px;">
                            <ul style="padding-left: 20px;">
                                <li style="list-style: disc;">If Merchant has not settled its outstanding balance after seven (7) calendar days from billing/invoice date, Merchant’s account will be considered in default. Merchant’s and Members’ access to the Solution may be temporarily suspended at Our sole discretion.</li>
                                <p class="special"></p>
                                <li style="list-style: disc;">If Merchant still has not settled their outstanding balance for another seven (7) calendar days from date of default, for a total of fourteen (14) days from billing/invoice date, Merchant’s account and service will be terminated. Likewise, once Merchant’s account is terminated, all information stored that relates to the account may be permanently deleted at the sole discretion of Globe.</li>
                                <p class="special"></p>
                                <li style="list-style: disc;">Solution access will be restored after Merchant settles its outstanding balance.</li>
                            </ul>
                            </td>
                        </tr>
                    </table>
                </div>
                <p class="special"></p>
                <h3 id="proprietary-rights-and-licenses">6. Proprietary Rights and Licenses</h3>
                <p class="special"></p>
                <ul>
                    <li>6.1. “Intellectual Property (IP)” shall mean any present or future development work, copyright, patent, trade-mark, trade name, service mark, design, program, procedure and method of computation, trade secret, data model, invention, drawing, plan, specification, process or similar property. Without limiting the generality of the foregoing, this may include patents, trademarks, service marks, design right (whether registerable or otherwise), domain names, applications for any of the foregoing, trade secrets, copyright, know-how, trade or business names and other similar rights or obligations whether registerable or not in any country; any and all technical or commercial information, including, but not limited to the following: software (object and source code), manufacturing techniques and designs; specifications and formulae; know-how, data, systems and processes; production methods; trade secrets; undisclosed inventions; financial and marketing information; as well as registered or unregistered intellectual property in the form of patents, trademarks, designs, and copyright in any works, including  Applications, Software, hardware,  and other Intellectual Property for which Globe or its Third Party vendors has rights and interests to.</li>
                    <p class="special"></p>
                    <li>6.2. All Intellectual Property and IP rights created prior to the commencement of this Agreement shall be and remain the property and right of the Party which created the same or for which it has license to or right thereto and shall be referred to as “Intellectual Property” (IP).</li>
                    <p class="special"></p>
                    <li>6.3. The Merchant agrees and acknowledges that it shall not acquire any right, title or interest in any copyright or other proprietary rights in the design of the Solution/s including modification, agrees not to remove, suppress or modify in any way any proprietary marking, including any trademark or copyright notice, on or in the Solution/s or which is visible during its operation.</li>
                    <p class="special"></p>
                    <li>6.4. Except as permitted by this Agreement, the Merchant and its End-Users shall not:</li>
                    <p class="special"></p>
                        <ol style="padding-left: 20px;" type="A">
                            <li>6.4.1. Attempt to decompile, disassemble, modify the source code of, or reverse engineer the IP;</li>
                            <p class="special"></p>
                            <li>6.4.2. Use, reproduce, transmit, modify, adapt or translate the IP;</li>
                            <p class="special"></p>
                            <li>6.4.3. Rent, lease, license, transfer, assign, sell or otherwise provide access to the IP on a temporary or permanent basis;</li>
                            <p class="special"></p>
                            <li>6.4.4. Use or cause or allow a Third Party to use the Solution and Services in any way to develop competing solution or services;</li>
                            <p class="special"></p>
                            <li>6.4.5. Use any Third Party Intellectual Property or components on a standalone basis unless such standalone use is authorized expressly by Globe or Globe’s Third Party Vendors; nor alter, remove or cover proprietary notices in or on the IP.</li>
                        </ol>
                </ul>
                <p class="special"></p>
                <h3 id="confidentiality">7. Confidentiality</h3>
                <p class="special"></p>
                <ul>
                    <li>7.1. All communications or data, in any form, whether tangible or intangible, which are disclosed or furnished by any director, officer, employee, agent, or consultant of any department or business area of any party hereto, including their affiliates and subsidiaries, (hereinafter “Disclosing Party”) to the other party, including their affiliates and subsidiaries, (hereinafter “Receiving Party”) and which are to be protected hereunder against unrestricted disclosure or competitive use by the receiving party shall be deemed to be “Confidential Information”. The Receiving Party shall not disclose, reproduce, or disseminate such confidential information to anyone, except to those employees and consultants (including employees and consultants of its parent, subsidiaries and affiliates) who have a need to know such Confidential Information for the purpose for which it is disclosed.</li>
                    <p class="special"></p>
                    <li>7.2. As used herein, the term "Confidential Information" shall mean all non-public, confidential or proprietary information disclosed hereunder, in any tangible or intangible form, such as but not limited to written, oral, visual, audio, those produced by electronic media, or through any other means, that is designated as confidential or that by its nature or circumstances surrounding its disclosure, should be reasonably considered as confidential.  </li>
                    <p class="special"></p>
                    <li>7.3. Confidential Information shall include, but not be limited to, products or planned products, processes and/or procedures, technological achievements and interests, customers and potential customers, business prospects, financial statements and information, financial situation and corporate plans, internal activities, future plans of both parties, and other information deemed proprietary or confidential by the Disclosing Party or any other matter in which the Disclosing Party may have any interest whatsoever.</li>
                    <p class="special"></p>
                    <li>7.4. Each Disclosing Party hereby represents and warrants to the Receiving Party that it has lawful rights to provide the confidential information.</li>
                </ul>
                <p class="special"></p>
                <h3 id="representation-warranties-exclusive-remedies-and-disclaimers">8. Representations, Warranties, Exclusive Remedies and Disclaimers</h3>
                <p class="special"></p>
                <ul>
                    <li>8.1. Globe makes no warranties of merchantability, fitness for a particular purpose (including Merchant’s compliance with its statutory or regulatory obligations), or arising from a course of performance, dealing or usage of trade.</li>
                    <p class="special"></p>
                    <li>8.2. Globe does not warrant that the Solution and the Services are error-free. Globe will use reasonable commercial efforts to effect the rectification.</li>
                    <p class="special"></p>
                    <li>8.3. There is no such thing as perfect security, and Globe cannot guarantee or warrant the security of any Data (including Confidential Information or Personal Information) that Globe receives and stores on the Globe network systems.</li>
                    <p class="special"></p>
                    <li>8.4. The Merchant will remain duly organized and validly existing in good standing under the laws of the jurisdiction of its incorporation and has all requisite power and authority to conduct its business, own its properties, and execute, deliver and perform its duties, obligations, undertakings, warranties and covenants under this Agreement.</li>
                    <p class="special"></p>
                    <li>8.5. The execution, delivery, and performance by the Merchant of this Agreement have been duly authorized by all necessary corporate action, and do not and will not contravene any provision of the Merchant's constitutional documents or any indenture, contract or agreement to which the Merchant is a party or by which it or its properties may be bound, or any law, rule, regulation, order, writ, judgment, injunction, decree, determination or award presently in effect applicable to the Merchant.</li>
                    <p class="special"></p>
                    <li>8.6. All actions, conditions and things required by any applicable law or regulation to be taken, fulfilled and done, including the obtaining of any necessary authorizations, approvals, permits, licenses and consents, in order to enable the Merchant lawfully to enter into, exercise its rights and perform and comply with its obligations under this Agreement, to ensure that those obligations are valid, legally binding and enforceable and to make this Agreement admissible in evidence in any court of competent jurisdiction have been taken, fulfilled and done in all material respects.</li>
                    <p class="special"></p>
                    <li>8.7. In entering into this Agreement, the Merchant has relied on its own judgment and has not relied upon any representations, warranties or statements made or purported to be made by Globe Telecom, Inc. and/or its subsidiaries and/or Affiliates (other than other expressly set out in this Agreement).</li>
                    <p class="special"></p>
                    <li>8.8. So far as the Merchant is aware, no litigation, arbitration or administrative proceeding is current, pending or threatened to restrain the entry into, exercise of any of its rights under and/or performance or enforcement of or compliance with any of its obligations under this Agreement, and the Merchant is not subject to any outstanding judgment, rule, order, statement of claim, injunction or decree of any court, governmental or regulatory authority or body acting in an arbitral or adjudicative capacity, that may affect its ability to perform its obligations under this Agreement.</li>
                    <p class="special"></p>
                    <li>8.9. Any and all documents, certificates, statements, accounts, and other information provided to Globe Telecom, Inc. by or on behalf of the Merchant in connection with the Merchant’s affairs and business, any services and/or this Agreement are true, accurate and complete in all respects (and the Merchant acknowledges that Globe Telecom, Inc. had relied on such information in deciding to enter into this Agreement with the Merchant).</li>
                </ul>
                <p class="special"></p>
                <h3 id="indemnification-and-liability">9. Indemnification and Liability</h3>
                <p class="special"></p>
                <ul>
                    <li>9.1. Merchant agrees to indemnify, defend and hold harmless Globe, its parent companies, business partners, directors, officers, employees, agents, and any third-party vendors  from and against any and all claims, losses, expenses, damages and costs, expenses demands (including, but not limited to, direct, incidental, consequential, exemplary and indirect damages), and reasonable attorneys’ fees, resulting from or arising out of Merchant’s use of these Solution/s or Services, misuse, improper use of data or content, unlawful exposure of data or inability to use the Solution/s and/or the Services, or unlawful use of content, or any breach by the Merchant of the terms and conditions of this Agreement or Merchant’s act, omission, fault, negligence, gross negligence, willful misconduct or for violation of any rights of another. These obligations will survive any termination of Merchant relationship with Globe or Merchant use of the Solution/s and/or the Services.</li>
                    <p class="special"></p>
                    <li>9.2. Globe Telecom’s possible liability is strictly limited to the extent of this Agreement. Globe Telecom shall not be subjected to any form of liability arising out of or in connection with the ordinary course of business of the Merchant.</li>
                    <p class="special"></p>
                    <li>9.3. Notwithstanding any other provision of this Agreement, the maximum aggregate liability of Globe Telecom to the Merchant arising out of or in connection with this Agreement, whether based on breach of contract, statutory warranty or otherwise, shall be strictly limited to an amount equal to the Service Fees actually received by Globe Telecom under this Agreement for the year immediately preceding the event which gave rise to such liability.</li>
                    <p class="special"></p>
                    <li>9.4. In no event will Globe be liable for any damages whatsoever, including, but not limited to any indirect, incidental, consequential, special, exemplary or other indirect damages arising out of (i) the use of or inability to use of the Solution/s and/or the Service, or the content, (ii) any transaction conducted through or facilitated by the Solution/s; (iii) any claim attributable to errors, omissions, or other inaccuracies in the Solution/s, the Services and/or the content, (iv) unauthorized access to or alteration of Customer transmissions or data, or (v) any other matter relating to the Solution/s, the Services, or the content, even if advised of the possibility of such damages.</li>
                    <p class="special"></p>
                    <li>9.5. Globe reserves the right to refuse liability outside of jurisdictions in which it cannot be held accountable.</li>
                </ul>
                <p class="special"></p>
                <h3 id="term-and-termination">10. Term and Termination</h3>
                <p class="special"></p>
                <ul>
                    <li>10.1. This Agreement shall remain in effect indefinitely from acceptance of such, unless Merchant terminates Subscription (via Merchant Dashboard or an email to support@rush.ph) or Globe terminates the service.</li>
                    <p class="special"></p>
                    <li>10.2. Globe reserves the right to terminate the service with prior written notice to the Merchant in the event that circumstances arise which prevents Globe from providing the Solution/s. In such an event, the Merchant shall be allowed to download its data from the Solution/s prior to the termination of the service takes effect.</li>
                    <p class="special"></p>
                    <li>10.3. Terms and conditions relating to fees and commercial terms shall be subject to change and/or adjustment due to prevailing market price and other commercial terms or conditions not within the control of Globe.</li>
                    <p class="special"></p>
                    <li>10.4. The Merchant should have downloaded and deleted its data from the Solution/s by the time the contract expires or is terminated.  Any Merchant data remaining in the Solution/s after the expiration or termination of the contract will be deleted.</li>
                    <p class="special"></p>
                    <li>10.5. Upon any termination of this Agreement, Merchant shall immediately discontinue the use of the Solution. All provisions regarding indemnification, warranty, liability and limits thereon, and confidentiality and/or protection of proprietary rights and trade secrets shall survive indefinitely or until the expiration of any time period specified elsewhere in this Agreement with respect to the provision in question, and termination of this Agreement shall not relieve Merchant of its obligations to pay accrued fees.</li>
                </ul>
                <p class="special"></p>
                <h3 id="contracting-notices-governing-law-jurisdiction">11. Who Are You Contracting With, Notices, Governing Law and Jurisdiction</h3>
                <p class="special"></p>
                <ul>
                    <li>11.1. You are contracting with Globe Telecom, Inc. with office address at The Globe Tower, 32nd Street corner 7th Avenue, Bonifacio Global City, Taguig, Philippines.</li>
                    <p class="special"></p>
                    <li>11.2. All notices, demands or other communications required or permitted to be given or made hereunder shall be in writing and may be delivered personally or sent via electronic means such as email or through the Merchant Dashboard. </li>
                    <p class="special"></p>
                    <li>11.3. Philippine Law governs the interpretation of this Agreement and applies to claims for breach of it. All other claims, including claims regarding breach of contract, breach of warranty, consumer protection laws, will be subject to Philippine laws. Any dispute arising out of or in connection with this Agreement, including any question regarding its existence, validity, or termination, will be referred to by exhausting good faith negotiations.</li>
                </ul>
                <p class="special"></p>
                <h3 id="miscellaneous-provisions">12. Miscellaneous Provisions</h3>
                <p class="special"></p>
                <ul>
                    <li>12.1. Fees and charges indicated herein are VAT inclusive. In addition to the Fees and charges, VAT and other applicable taxes shall be charged to the Merchant.</li>
                    <p class="special"></p>
                    <li>12.2. Neither party shall assign this contract without the prior written consent of the other.  Consent however, shall not be unreasonably withheld.</li>
                    <p class="special"></p>
                    <li>12.3. No variation of this Agreement shall be effective unless made in writing and signed by or on behalf of Globe Telecom, Inc.</li>
                    <p class="special"></p>
                    <li>12.4. No failure on the part of Globe Telecom, Inc. to exercise, and no delay on its part in exercising, any right or remedy under this Agreement will operate as a waiver thereof, nor will any single or partial exercise of any right or remedy preclude any other or further exercise thereof or the exercise of any other right or remedy. The rights and remedies provided in this Agreement are cumulative and not exclusive of any other rights or remedies (whether provided by law or otherwise).</li>
                    <p class="special"></p>
                    <li>12.5. No Party shall be held responsible for any delay or failure in performance of any part of its obligations under this Agreement caused beyond its reasonable control and without the fault or negligence of the delayed or non-conforming Party.</li>
                    <p class="special"></p>
                    <li>12.6. Each Party shall bear all costs incurred by it in connection with the preparation, negotiation and entry into of this Agreement.</li> 
                </ul>
                <p class="special"></p>
                <h3 id="features">Annex 1: Features</h3>
                <p class="special"></p>
                <table style="border: 1px solid #dcdcdc; color:#6b6c6c;" width="100%">
                    <tr>
                        <td style="padding: 0;">
                            <table width="100%" cellspacing="0" cellpadding="0" style="border-right: 1px solid #dcdcdc;">
                                <tr style="border-bottom:1px solid #dcdcdc;">
                                    <td width="50%" style="border-right: solid 1px #dcdcdc;"></td>
                                    <td width="50%" style="padding: 2px; text-align: center;"><b>RUSH</b></td>
                                </tr>
                                <tr style="border-bottom: solid 1px #dcdcdc;">
                                    <td colspan="2" style="padding: 5px;"><h5 style="margin: 0;">CHANNELS</h5></td>
                                </tr>
                                <tr style="border-bottom: solid 1px #dcdcdc;">
                                    <td style="border-right: solid 1px #dcdcdc; padding: 5px;">Android Merchant Mobile App</td>
                                    <td style="padding: 5px;">Included</td>
                                </tr>
                                <tr style="border-bottom: solid 1px #dcdcdc;">
                                    <td style="border-right: solid 1px #dcdcdc; padding: 5px;">Android Customer Mobile App</td>
                                    <td style="padding: 5px;">Included</td>
                                </tr>
                                <tr style="border-bottom: solid 1px #dcdcdc;">
                                    <td style="border-right: solid 1px #dcdcdc; padding: 5px;">iOS Customer Mobile App</td>
                                    <td style="padding: 5px;">Included</td>
                                </tr>
                                <tr style="border-bottom: solid 1px #dcdcdc;">
                                    <td style="border-right: solid 1px #dcdcdc; padding: 5px;">Customer Web Portal</td>
                                    <td style="padding: 5px;">Included</td>
                                </tr>
                                <tr style="border-bottom: solid 1px #dcdcdc;">
                                    <td colspan="2" style="padding: 5px;"><h5 style="margin: 0;">MANAGEMENT</h5></td>
                                </tr>
                                <tr style="border-bottom: solid 1px #dcdcdc;">
                                    <td style="border-right: solid 1px #dcdcdc; padding: 5px;">Web Dashboard</td>
                                    <td style="padding: 5px;">Included</td>
                                </tr>
                                <tr style="border-bottom: solid 1px #dcdcdc;">
                                    <td style="border-right: solid 1px #dcdcdc; padding: 5px;">Member Database</td>
                                    <td style="padding: 5px;">Included</td>
                                </tr>
                                <tr style="border-bottom: solid 1px #dcdcdc;">
                                    <td style="border-right: solid 1px #dcdcdc; padding: 5px;">Data Report &amp; Analytics</td>
                                    <td style="padding: 5px;">Included</td>
                                </tr>
                                <tr style="border-bottom: solid 1px #dcdcdc;">
                                    <td style="border-right: solid 1px #dcdcdc; padding: 5px;">CMS Tool for SMS</td>
                                    <td style="padding: 5px;">Included</td>
                                </tr>
                                <tr style="border-bottom: solid 1px #dcdcdc;">
                                    <td style="border-right: solid 1px #dcdcdc; padding: 5px;">Rewards System</td>
                                    <td style="padding: 5px;">White-label Points</td>
                                </tr>
                                <tr style="border-bottom:1px solid #dcdcdc;">
                                    <td style="border-right: solid 1px #dcdcdc; padding: 5px;">Membership Cap</td>
                                    <td style="padding: 5px;">Unlimited</td>
                                </tr>
                                <tr style="border-bottom: solid 1px #dcdcdc;">
                                    <td colspan="2" style="border-right: solid 1px #dcdcdc; padding: 5px;"><h5 style="margin: 0;">MARKETING SUPPORT</h5></td>
                                </tr>
                                <tr style="border-bottom: solid 1px #dcdcdc;">
                                    <td style="border-right: solid 1px #dcdcdc; padding: 5px;">Rush Website Feature</td>
                                    <td style="padding: 5px;">Included</td>
                                </tr>
                                <tr style="border-bottom: solid 1px #dcdcdc;">
                                    <td style="border-right: solid 1px #dcdcdc; padding: 5px;">In-store Merchandising</td>
                                    <td style="padding: 5px;">Included</td>
                                </tr>
                                <tr style="border-bottom: solid 1px #dcdcdc;">
                                    <td style="border-right: solid 1px #dcdcdc; padding: 5px;">SMS Credits</td>
                                    <td style="padding: 5px;">PHP 0.50 per 160-character SMS</td>
                                </tr>
                                <tr>
                                    <td style="border-right: solid 1px #dcdcdc; padding: 5px;">App Zero-rating</td>
                                    <td style="padding: 5px;">PHP 2.00 per MB</td>
                                </tr>
                            </table>
                        </td>
                        <td></td>
                    </tr>                
                </table>
                <p class="special"></p>
                <h3 id="incident-service-level-agreement">ANNEX 2: Incident Service Level Agreement</h3>
                <p class="special"></p>
                <p>Incident Service Level Agreement refers to failure of our Merchant Loyalty System, RUSH. The table below shows contact information and response times.</p>
                <p class="special"></p>
                <div class="maxWidth">
                    <table style="border: 1px solid #dcdcdc; color:#6b6c6c;" width="100%">
                        <tr>
                            <td style="padding: 0;">
                                <table width="100%" cellspacing="0" cellpadding="0">
                                    <tr style="background: #6b6c6c; text-align: center; padding: 10px 8px 10px 8px;">
                                        <td colspan="2" style="color: #fff;"><b>Priority Level: 1</b></td>
                                    </tr>
                                    <tr style="border-bottom:1px solid #dcdcdc;">
                                        <td colspan="2">Full service failure, user cannot access the RUSH Web Dashboard, user unable to use the Merchant Mobile App,  Customer Mobile App, or Customer Web Portal</td>
                                    </tr>
                                    <tr style="border-bottom:1px solid #dcdcdc;">
                                        <td width="50%">Response Time from Globe’s Receipt of Report</td>
                                        <td width="50%">2 hours</td>
                                    </tr>
                                    <tr style="border-bottom:1px solid #dcdcdc;">
                                        <td>Response Channel</td>
                                        <td>
                                            Landline: +63 2 2119792<br/>
                                            Mobile: +63 917 5434594<br/>
                                            Email: support@rush.ph
                                        </td>
                                    </tr>
                                    <tr style="border-bottom:1px solid #dcdcdc;">
                                        <td>Resolution Time</td>
                                        <td>4 hours</td>
                                    </tr>
                                    <tr>
                                        <td>Example</td>
                                        <td>Server down, website cannot be accessed</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 0;">
                                <table width="100%" cellspacing="0" cellpadding="0">
                                    <tr style="background: #6b6c6c; text-align: center; padding: 10px 8px 10px 8px;">
                                        <td colspan="2" style="color: #fff;"><b>Priority Level: 2</b></td>
                                    </tr>
                                    <tr style="border-bottom:1px solid #dcdcdc;">
                                        <td colspan="2">Merchant partners can access RUSH Web Dashboard, user can use the Merchant Mobile App, Customer Mobile App, or Customer Web Portal, but there is a critical error or bug</td>
                                    </tr>
                                    <tr style="border-bottom:1px solid #dcdcdc;">
                                        <td width="50%">Response Time from Globe’s Receipt of Report</td>
                                        <td width="50%">24 hours</td>
                                    </tr>
                                    <tr style="border-bottom:1px solid #dcdcdc;">
                                        <td>Response Channel</td>
                                        <td>
                                            Landline: +63 2 2119792<br/>
                                            Mobile: +63 917 5434594<br/>
                                            Email: support@rush.ph
                                        </td>
                                    </tr>
                                    <tr style="border-bottom:1px solid #dcdcdc;">
                                        <td>Resolution Time</td>
                                        <td>2 business days</td>
                                    </tr>
                                    <tr>
                                        <td>Example</td>
                                        <td>System configuration not correct, promotion not displayed</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 0;">
                                <table width="100%" cellspacing="0" cellpadding="0">
                                    <tr style="background: #6b6c6c; text-align: center; padding: 10px 8px 10px 8px;">
                                        <td colspan="2" style="color: #fff;"><b>Priority Level: 3</b></td>
                                    </tr>
                                    <tr style="border-bottom:1px solid #dcdcdc;">
                                        <td colspan="2">Merchant partners can access RUSH Web Dashboard, user can use the Merchant Mobile App Customer Mobile App, or Customer Web Portal, but there is a non-critical error or bug. There is also a work around to the non-critical error</td>
                                    </tr>
                                    <tr style="border-bottom:1px solid #dcdcdc;">
                                        <td width="50%">Response Time from Globe’s Receipt of Report</td>
                                        <td width="50%">1 business day</td>
                                    </tr>
                                    <tr style="border-bottom:1px solid #dcdcdc;">
                                        <td>Response Channel</td>
                                        <td>Email: support@rush.ph</td>
                                    </tr>
                                    <tr style="border-bottom:1px solid #dcdcdc;">
                                        <td>Resolution Time</td>
                                        <td>5 business days</td>
                                    </tr>
                                    <tr>
                                        <td>Example</td>
                                        <td>Unable to send SMS messages from system, no access to report</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 0;">
                                <table width="100%" cellspacing="0" cellpadding="0">
                                    <tr style="background: #6b6c6c; text-align: center; padding: 10px 8px 10px 8px;">
                                        <td colspan="2" style="color: #fff;"><b>Priority Level: 4</b></td>
                                    </tr>
                                    <tr style="border-bottom:1px solid #dcdcdc;">
                                        <td colspan="2">Minor UI fixes, text fixes, full functionality still available to the Merchant and user is able to transact using the Merchant or Customer App, RUSH Web Dashboard, or Customer Web Portal</td>
                                    </tr>
                                    <tr style="border-bottom:1px solid #dcdcdc;">
                                        <td width="50%">Response Time from Globe’s Receipt of Report</td>
                                        <td width="50%">3 business days</td>
                                    </tr>
                                    <tr style="border-bottom:1px solid #dcdcdc;">
                                        <td>Response Channel</td>
                                        <td>Email: support@rush.ph</td>
                                    </tr>
                                    <tr style="border-bottom:1px solid #dcdcdc;">
                                        <td>Resolution Time</td>
                                        <td>Next available full cycle release</td>
                                    </tr>
                                    <tr>
                                        <td>Example</td>
                                        <td>All other non-urgent tasks</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
        </div>
    </section>
</div>
</body>
</html>