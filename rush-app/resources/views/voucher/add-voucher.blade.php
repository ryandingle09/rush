<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,300,300italic,400italic,600italic,700,700italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ url('css/vendor/bootstrap.css') }}">
    <title>{{ $title or "CMS - RUSH" }}</title>
</head>
<body>
    <div class="container" style="margin-top: 20px;">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @elseif (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                <form method="post" action="{{ url('/vouchers/addVoucher') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="inputVoucherName">Voucher Name</label>
                        <input type="text"
                               class="form-control"
                               id="inputVoucherName"
                               name="name"
                               placeholder="Enter Voucher Name"
                               value="{{ old('name') }}"
                               required>
                    </div>
                    <div class="form-group">
                        <label for="inputBranchIds">Branch Ids</label>
                        <input type="text"
                               class="form-control"
                               id="inputBranchIds"
                               name="branch_ids"
                               placeholder="Enter Branch Ids"
                               value="{{ old('branch_ids') }}"
                               required>
                    </div>
                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="{{ url('page/cms/js/jquery-1.11.3.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('page/cms/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript">
    </script>
</body>
</html>