@foreach($cards as $card)
    <div class="page" style="page-break-after:always;text-align: center">
        <h2 style="text-align: center">Card No: {{ $card['card_no'] }}</h2>
        <h2 style="text-align: center">Membership: {{ $card['membership'] }}</h2>
        <br />
        <img src="{{ $card['qr'] }}"/>
    </div>
@endforeach