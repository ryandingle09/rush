@extends('layouts.app')
@section('view')
<div class="container">
  <div id="content">
    <!-- Nav tabs -->
    <div class="navTabsWrap">
        <ul class="nav nav-tabs" role="tablist" id="tabs">
            <li role="presentation" class="active"><a href="#design" aria-controls="design" role="tab" data-toggle="tab">Design</a></li>
            <li role="presentation"><a href="#order-card" aria-controls="order-card" role="tab" data-toggle="tab">Order</a></li>
        </ul>
    </div>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="design">
          <div class="noTab">
            <div class="row">
            <div id="content" class="design spacing">
              <form action="{{MigrationHelper::getAppBaseUrl()}}addons/saveCardDesign" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="cardTab">
                    <div class="col-md-4">
                        <div class="row left">
                            <div class="col-xs-12 clearfix cardUploadLogo">
                                <h5 class="withIco"><i class="number">1</i>Upload Logo</h5>
                                <span class="error-msg hide">Image does not meet the image size requirement. Please check again.</br></span>
                                <img src="{{$card_design['logoURL']}}" class="imgPrev" alt="">
                                <div class="pull-left">
                                <span class="btn btn-default btn-file">
                                Choose File <input type="file" name="logo" class="fuImage" ew="200" eh="200">
                                </span>
                                    <i>Required image size: 200px x 200px</i>
                                </div>
                            </div>
                            <div class="col-xs-12 clearfix cardAddBackImage">
                                <h5 class="withIco"><i class="number">2</i>Add Background Image</h5>
                                <span class="error-msg hide">Image does not meet the image size requirement. Please check again.</br></span>
                                <img src="{{$card_design['backgroundURL']}}" class="imgPrev" alt="">
                                <div class="pull-left">
                                <span class="btn btn-default btn-file">
                                Choose File <input type="file" name="background" class="fuImage" ew="500" eh="200">
                                </span>
                                    <i>Required image size: 500px x 200px</i>
                                </div>
                            </div>
                            <div class="col-xs-12 clearfix cardAddBackColor">
                                <h5 class="withIco"><i class="number">3</i>Choose Background Color</h5>
                                <div class="hexwrap">
                                    <input type='text' id="cardBgColor" name="cardbgcolor" value="{{$card_design['backgroundColorHEX']}}"/>
                                    <span class="hex" id="cardBackgroundColor">#00A8A4</span>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <button class="btn btn-primary saveDesign">Save Design</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 right">
                        <div class="cardFrame">
                            <div class="cardWrap">
                                <div class="overlay">
                                    <img src="{{MigrationHelper::getBaseUrl()}}assets/images/cardLogo.png" class="cardLogo" alt="">
                                    <img src="{{MigrationHelper::getBaseUrl()}}assets/images/qr.png" alt="" class="qr">
                                </div>
                            </div><!--cardWrap-->
                        </div><!--cardFrame-->
                    </div><!--right-->
                </div><!--cardTab-->
              </form>
            </div>
          </div>
        </div>
      </div>
      <div role="tabpanel" class="tab-pane" id="order-card">
        <div class="noTab">
          <div class="row">
            <div class="col-md-12"><h4>Order History</h4></div>
            <div class="col-md-12"><button class="btn btn-primary" id="order-card-button">Order</button></div>
          </div>
          <div class="row">
              <div class="col-md-12">
                  <table data-toggle="table" class="data-table account" data-pagination="false" data-search="false">
                      <thead>
                          <tr>
                              <th>Order Number</th>
                              <th>Quantity</th>
                              <th>Total Amount</th>
                              <th>Date</th>
                              <th>Status</th>
                          </tr>
                      </thead>
                      <tbody>
                        @foreach ( $loyalty_card_order as $order)
                        <tr>
                            <td class="text-center">{{ $order->id }}</td>
                            <td class="text-center">{{ number_format( $order->quantity, 0, ".",",") }}</td>
                            <td class="text-center">{{ number_format( $order->total, 2, ".", ",") }}</td>
                            <td class="text-center">{{ Carbon\Carbon::parse($order->created_at)->format('F j, Y h:i:s A') }}</td>
                            <td class="text-center">{{ $order->status }}</td>
                        </tr>
                        @endforeach
                      </tbody>
                  </table>
              </div>
              <div class="col-md-4">
                  <div class="row">
                    <div class="noTab">

                    </div>
                  </div>
              </div>
            </div>
        </div>
      </div>
      </div>
    </div>
</div> <!-- /container -->
@endsection

@section('modal')
<!--  MODALS -->
<div id="orderCard" class="modal">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">Order Loyalty Card</div>
          <div class="modal-body">
            <form action="{{MigrationHelper::getAppBaseUrl()}}addons/orderCard" method="POST" id="card_order_form">
              {{ csrf_field() }}
              <div class="row">
                  <div class="col-md-6 form-group">
                      <label for="programName">Quantity</label>
                      <input type="number" class="form-control" name="quantity" id="card_order_quantity" value="" min="5000" required>
                      <span class="error-msg hide">Quantity should be more or equal to 5000.</br></span>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-3">
                      <a class="btn btn-primary" id="order_card_submit" role="button">Submit</a>
                  </div>
                  <div class="col-md-3">
                      <a class="btn btn-warning" id="order_card_modal_cancel" role="button">Cancel</a>
                  </div>
              </div>
            </form>
          </div>
      </div>
  </div>
</div>

<div id="orderCardConfirmation" class="modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12 form-group">
              You already placed an order today. Are you sure you want to order again?
            </div>
            <div class="col-md-3 form-group">
              <button type="button" class="btn btn-success" id="orderCardConfirmationOrder" >Order</button>
            </div>
            <div class="col-md-3 form-group">
              <button type="button" class="btn btn-warning" id="orderCardConfirmationCancel" >Cancel</button>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

@if ( $success_prompt )
<div id="orderCardSuccessPrompt" class="modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row text-center">
            <div class="col-md-12 form-group">
              {{ $success_prompt }}
            </div>
            <div class="col-md-12 form-group">
              <button type="button" class="btn btn-success" id="orderCardSuccessPromptClose" >Close</button>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endif

@if ( !$card_design['logoURL'] || !$card_design['backgroundURL'] || !$card_design['backgroundColorHEX'] )
<div id="orderCardNoDesign" class="modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row text-center">
            <div class="col-md-12 form-group">
              You need to finish your card design first before ordering.
            </div>
            <div class="col-md-12 form-group">
              <button type="button" class="btn btn-success" id="orderCardNoDesignClose" >Close</button>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endif

<div id="addTablet" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Add Tablet</div>
            <div class="modal-body">
                <form method="POST" action="{{ MigrationHelper::getAppBaseUrl() }}addons/addTablet">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="tabletID">Tablet ID (IMEI)</label>
                            <input type="text" class="form-control" name="tabletID" id="tabletID">
                        </div>
                        <div class="col-md-12">
                            <label for="tabletBrand">Tablet Brand</label>
                            <select name="tabletBrand" id="tabletBrand" class="form-control">
                                <option value="samsung">Samsung</option>
                                <option value="apple">Apple</option>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label for="branchAssigned">Branch Assigned to</label>
                            <select name="branchAssigned" id="branchAssigned" class="form-control">
                                <option value="megamall">Megamall</option>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button class="btn btn-primary pull-right">ADD</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footerPlaceholder')
@parent
<script type="text/javascript">
  $(document).ready(function() {
    $("#order-card-button").click(function(event) {
      @if ( $card_design['logoURL'] && $card_design['backgroundURL'] && $card_design['backgroundColorHEX'] )
        $('#card_order_quantity').val(5000);
        $("#orderCard").css('display', "block");
      @else
        $("#orderCardNoDesign").css("display", "block");
      @endif
    });

    $('#order_card_modal_cancel').click( function() {
        $("#orderCard").css('display', "none");
    });

    $('#order_card_submit').click( function() {
      var _pending_orders = {{$pending_orders}};
      if ( _pending_orders > 0 )
      {
        $("#orderCardConfirmation").css("display", "block");
        return false;
      }
      else
      {
        $("#orderCard").css('display', "none");
        $("#card_order_form").submit();
      }
    });

    $("#orderCardConfirmationCancel").click( function() {
      $("#orderCardConfirmation").css("display", "none");
    });

    $("#orderCardConfirmationOrder").click( function() {
      $("#orderCardConfirmation").css("display", "none");
      $("#card_order_form").submit();
    });

    @if ( $success_prompt )
      $("#orderCardSuccessPrompt").css("display", "block");

      $("#orderCardSuccessPromptClose").click( function() {
        $("#orderCardSuccessPrompt").css("display", "none");
      });
    @endif

    @if ( !$card_design['logoURL'] || !$card_design['backgroundURL'] || !$card_design['backgroundColorHEX'] )
      $("#orderCardNoDesignClose").click( function() {
        $("#orderCardNoDesign").css("display", "none");
      });
    @endif
  });
</script>
@endsection
