<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>RUSH</title>

        <!-- Bootstrap -->
        <link href="{{ asset('page/session-timeout/css/bootstrap.min.css') }}" rel="stylesheet">

        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ asset('page/session-timeout/css/font-awesome.min.css') }}">

        <link href="{{ asset('page/session-timeout/css/style.css') }}" rel="stylesheet">

        <link rel="shortcut icon" href="{{ asset('page/onboarding/images/favicon.png') }}">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <div class="site-wrapper">

            <div class="site-wrapper-inner">

                <div class="cover-container">

                    <div class="inner cover">
                        <img src="{{ asset('page/session-timeout/images/003-hourglass.svg') }}" alt="" class="img-responsive hour-glass">
                        <h2 class="cover-heading">Your session has timed out.</h2>
                        <section class="timeout-body">
                            <p class="timeout-p">For your security, RUSH sessions expire automatically due to 120 minutes of inactivity or a submitted form has expired.</p>
                            <p class="timeout-p">To return to RUSH site, please log in.</p>
                            <p class="timeout-p">
                                <a href="/#signin" class="btn btn-lg btn-default">Log In</a>
                            </p>
                        </section>
                    </div>

                </div>

            </div>

        </div>
    </body>

</html>