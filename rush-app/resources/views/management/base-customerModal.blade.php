@if($add_member)
<div id="addCustomer" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <form method="POST" id="deleteSegmentForm" action="management/addCustomerData">
            {{ csrf_field() }}
            <div class="modal-header">Add Member</div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="fullName">Name</label>
                        <input required type="text" class="form-control" placeholder="Firstname, Lastname" name="fullName" id="fullName" value="{{ old('fullName') }}" />
                    </div>
                    <div class="col-md-6">
                        <label for="mobileNumber">Mobile Number</label>
                        <input {{ ($merchant->settings->customer_login_param == 'email') ? '' : 'required' }} type="text" class="form-control" placeholder="09123456789" name="mobileNumber" id="mobileNumber" value="{{ old('mobileNumber') }}" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        <label for="email">Email Address</label>
                        <input required  type="text" class="form-control" placeholder="user@email.com" name="email" id="email" value="{{ old('email') }}" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="client_code">Code</label>
                        <input type="text" class="form-control" name="client_code" id="client_code" value="{{ old('client_code') }}" />
                    </div>
                    <div class="col-md-6">
                    @if ( $merchant->settings->customer_login_param == 'email' )
                        <label for="PIN">PIN</label>
                        <input required type="password" class="form-control" name="PIN" id="PIN" maxlength="4"/>
                    @else
                        <label>Default 4-Digit PIN is the last 4 digit of customer's mobile number</label>
                    @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="birthDate">Birthday</label>
                        <input type="text" class="form-control" placeholder="MM/DD/YYYY" name="birthDate" id="birthDate" value="{{ old("birthDate") }}" />
                    </div>
                    <div class="col-md-6">
                        <label for="gender">Gender</label>
                        {{Form::select('gender', ['m' => 'male', 'f' => 'female'], null, ['placeholder' => 'choose...', 'class' => 'form-control'])}}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-group row">
                    <div class="col-md-12">
                        <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                        <button class="btn btn-primary pull-right" id="update-btn">ADD</button>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<div id="uploadCustomer" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Upload Members</div>
            <div class="modal-body">
                <form method="POST" action="{{ url('management/uploadCustomerData') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="form-group row">
                            <div class="col-md-6"></div>
                            <div class="col-md-6">
                                <a href="{{ url('page/management/assets/members_upload_template.xlsx') }}" class="btn btn-info">
                                    <i class="" aria-hidden="true"></i> Download Sample Template
                                </a>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="customerData">Upload Customer Data</label>
                        </div>
                        <div class="col-md-12 margT20">
                            <span class="btn btn-default btn-file">
                                Choose File
                                <input type="file" accept=".xlsx,.xls" name="customerData" required>
                            </span> &nbsp;
                            <span id="data_filename"></span>
                        </div>
                        <div class="col-md-12 margT20">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button type="submit" class="btn btn-primary pull-right">ADD</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@section('footerPlaceholder')
@parent
<script type="text/javascript">
$( document ).ready(function() {
$('#birthDate').datetimepicker({
    format: 'YYYY-MM-DD'
});
});
</script>
@endsection
@endif