@extends('layouts.app')

@section('headerPlaceholder')
    @parent
    <link rel="stylesheet" href="page/management/css/style.css">
@endsection

@section('view')
<div class="container">
    @include('flash.message')
    <div id="content" class="management marginTop">
        <!-- Nav tabs -->
        <div class="navTabsWrap">
            <ul class="nav nav-tabs" role="tablist" id="tabs">
                <li role="presentation" class="active"><a href="#branch" aria-controls="branch" role="tab" data-toggle="tab">BRANCH</a></li>
                <li role="presentation"><a href="#employee" aria-controls="employee" role="tab" data-toggle="tab">EMPLOYEE</a></li>
                <li role="presentation"><a href="#customer" aria-controls="customer" role="tab" data-toggle="tab">CUSTOMER</a></li>
                <li role="presentation"><a href="#points" aria-controls="points" role="tab" data-toggle="tab">POINTS SUMMARY</a></li>
                <li role="presentation"><a href="#feedback" aria-controls="feedback" role="tab" data-toggle="tab">CUSTOMER FEEDBACK</a></li>
                @if(isset($enable_package_summary) && $enable_package_summary)
                    <li role="presentation"><a href="#class-summary" aria-controls="class-summary" role="tab" data-toggle="tab">CLASS SUMMARY</a></li>
                @endif
                @if( $merchant->settings->code_management )
                    <li role="presentation"><a href="#code-management" aria-controls="code-management" role="tab" data-toggle="tab">CODE MANAGEMENT</a></li>
                @endif
            </ul>
            <a href="#addBranch" data-toggle="modal" class="addNew addBranch" onclick="return false"><i class="fa fa-plus"></i> Add Branch</a>
        </div>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="branch">
                <table id="branchTable" class="data-table management">
                    <thead>
                    <tr>
                        <!-- <th>Branch ID</th> -->
                        <th>Branch Name</th>
                        <th>Branch Address</th>
                        <th>Branch Logo</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="employee">
                <table id="employeeTable" data-toggle="table" class="data-table management" data-pagination="true" data-search="true">
                    <thead>
                    <tr>
                        <th>Employee ID No.</th>
                        <th>Employee Name</th>
                        <th>Branch Assigned</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                    </thead>
                </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="customer">
                <table id="customerTable" class="data-table management loyalty">
                    <thead>
                    <tr>
                        <th data-sortable="true">Date Registered</th>
                        <th data-sortable="true">Customer Name</th>
                        <th>Client Code</th>
                        <th>Membership Level</th>
                        <th>Mobile No.</th>
                        <th>Email Address</th>
                        <th>Birthdate</th>
                        <th>Gender</th>
                        <th>Registration Channel</th>
                        <th>Branch</th>
                    </tr>
                    </thead>
                </table>
                @if($add_member)
                <div id="customerTableCustom" class="datatableCustom">
                    <br>
                    <br>
                    <br>
                    <button href="#addCustomer" class="btn btn-primary smsAdd" data-toggle="modal" onclick="return false">Add Member</button>
                    <button href="#uploadCustomer" class="btn btn-primary smsAdd" data-toggle="modal" onclick="return false">Upload Members</button>
                </div>
                @endif
            </div>
            <div role="tabpanel" class="tab-pane" id="points">
                <table id="pointsTable" class="data-table management loyalty">
                    <thead>
                    <tr>
                        <th data-sortable="true">Date Registered</th>
                        <th data-sortable="true">Customer Name</th>
                        <th data-sortable="true">Mobile No.</th>
                        <th data-sortable="true">Earned</th>
                        <th data-sortable="true">Burned</th>
                        <th data-sortable="true">Available</th>
                    </tr>
                    </thead>
                </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="feedback">
                @if( $merchant->settings->enable_feedback_category )
                <table id="reportsTable" class="data-table management" data-fbcat="true">
                @else
                <table id="reportsTable" class="data-table management">
                @endif
                    <thead>
                    <tr>
                        <th data-sortable="true" class="text-left">Date</th>
                        @if($merchant->settings->enable_feedback_category)
                        <th data-sortable="true" class="text-left">Category</th>
                        @endif
                        <th data-sortable="true" class="text-left">Customer Name</th>
                        <th data-sortable="true" class="text-left">Mobile No.</th>
                        <th data-sortable="true" class="text-left">Action</th>
                        <th class="text-center">Type</th>
                        <th class="text-left">Message</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($reports as $report)
                    <tr data-id="{{  $report->id }}" data-date="{{ $report->created_at or '' }}" data-customer="{{ $report->customer->fullName or '' }}" data-mobile="{{ $report->customer->mobileNumber or '' }}" data-type="{{ $report->type }}" data-message="{{ $report->message }}" data-merchantid="{{ $report->merchant_id }}">
                        <td class="text-left">{{ $report->created_at or '' }}</td>
                        @if($merchant->settings->enable_feedback_category)
                        <td class="text-left">{{ $report->category ? $report->category->title : null}}</td>
                        @endif
                        <td class="text-left">{{ $report->customer->fullName or '' }}</td>
                        <td class="text-left">{{ $report->customer->mobileNumber or '' }}</td>
                        <td class="text-center">
                            <a href="#" data-toggle="modal" data-target="#viewMessage" class="view-message" onclick="return false"><i class="fa {{ $report->class }}"></i></a>
                        </td>
                        <td class="text-center">{{ $report->type }}</td>
                        <td class="text-left">
                            @if( $report->type == "text")
                                {{ $report->message }}
                            @else
                                {{ MigrationHelper::getServerUrl() . 'repository/merchant/voice_mails/' . $report->merchant_id . '/' . $report->message }}
                            @endif
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            @if ( $merchant->settings->code_management )
            <div role="tabpanel" class="tab-pane" id="code-management">
                <table id="codeMgmtTable" class="data-table management" data-pagination="true">
                    <thead>
                    <tr>
                        <th>Client Code</th>
                        <th>Status</th>
                        <th>Customer</th>
                        <th>Date Created</th>
                        <th>Date Updated</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
                <div class="code-btn-wrap">
                    <button href="#addClientCode" id='addClientCodeButton' class="btn btn-primary smsAdd" data-toggle="modal" onclick="return false">Add Client Code</button>
                    <button href="#importClientCode" id='addClientCodeButton' class="btn btn-primary smsAdd" data-toggle="modal" onclick="return false">Import CSV</button>
                </div>
            </div>
            @endif
            @if(isset($enable_package_summary) && $enable_package_summary)
                <div role="tabpanel" class="tab-pane" id="class-summary">
                    <table id="classSummaryTable" class="display nowrap" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th data-sortable="true">Date Registered</th>
                            <th data-sortable="true">Customer Name</th>
                            <th data-sortable="true">Mobile No.</th>
                            <th data-sortable="true">Package</th>
                            <th data-sortable="true">Valid Until</th>
                            <th data-sortable="true">Status</th>
                            <th data-sortable="true">Target</th>
                            <th data-sortable="true">Accomplished</th>
                            <th data-sortable="true">Balance</th>
                            <th data-sortable="true">Branch</th>
                            <th data-sortable="false">Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            @endif
        </div>
    </div><!--/content-->
</div> <!-- /container -->
@endsection

@section('modal')
<div id="viewMessage" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">View Feedback</div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-md-4"><b>Date</b></div>
                    <div class="col-md-6" id="view-report-date"></div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4"><b>Customer Name</b></div>
                    <div class="col-md-6" id="view-report-customer"></div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4"><b>Mobile Number</b></div>
                    <div class="col-md-6" id="view-report-mobile"></div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4"><b>Message</b></div>
                    <div class="col-md-6" id="view-report-message"></div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="addBranch" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Add Branch</div>
            <div class="modal-body">
                <form method="POST" action="{{ url('management/addbranch') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label for="branchName">Branch Name</label>
                                    <input type="text" class="form-control" name="branchName" id="branchName"/>
                                </div>
                                <div class="col-md-12">
                                    <label for="streetName">Street Name/Unit</label>
                                    <textarea class="form-control" name="streetName" id="streetName" cols="30" rows="5"></textarea>
                                </div>
                                <div class="col-md-12">
                                    <label for="district">District</label>
                                    <input type="text" class="form-control" name="district" id="district"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class='form-group row'>
                                <div class="col-md-12">
                                    <label for="city">City/Province</label>
                                    <input type="text" class="form-control" name="city" id="city"/>
                                </div>
                                <div class="col-md-12">
                                    <label for="zipcode">Zipcode</label>
                                    <input type="text" class="form-control" name="zipcode" id="zipcode"/>
                                </div>
                                <div class="col-md-12 clearfix addBranchLogo">
                                    <label for="zipcode">Branch Logo</label>
                                    <br><span class="error-msg hide">Image does not meet the image size requirement. Please check again.</span>
                                    <br><img src="{{MigrationHelper::getBaseUrl()}}assets/images/addImage.jpg" class="imgPrev" alt="" height="64" width="64" />
                                        <input type="file" name="branch_logo" class="fuImage" ew="200" eh="200" id="branchLogoFile">
                                        <i>Required image size: 200px x 200px</i>
                                </div>
                            </div>
                        </div>
                        @if( $merchant->settings && $merchant->settings->toggle_branch_feature )
                        <div class="col-md-6">
                            <div class='form-group row'>
                                <div class="col-md-12">
                                    <label for="show">Show/Hide in App</label>
                                    {{ Form::select('show', ['0'=> 'Hide','1' => 'Show'], 1, ['class' => 'form-control', 'id'=>'show']) }}
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button class="btn btn-primary pull-right">ADD</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="editBranch" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Edit Branch</div>
            <div class="modal-body">
                <form method="POST" action="{{ url('management/editbranch') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label for="branchName">Branch Name</label>
                                    <input type="text" class="form-control" name="branchName" id="ebranchName"/>
                                </div>
                                <div class="col-md-12">
                                    <label for="eStreetName">Street Name/Unit</label>
                                    <textarea class="form-control" name="streetName" id="eStreetName" cols="30" rows="5"></textarea>
                                </div>
                                <div class="col-md-12">
                                    <label for="eDistrict">District</label>
                                    <input type="text" class="form-control" name="district" id="eDistrict"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class='form-group row'>
                                <div class="col-md-12">
                                    <label for="eCity">City/Province</label>
                                    <input type="text" class="form-control" name="city" id="eCity"/>
                                </div>
                                <div class="col-md-12">
                                    <label for="eZipcode">Zipcode</label>
                                    <input type="text" class="form-control" name="zipcode" id="eZipcode"/>
                                </div>
                                <div class="col-md-12 clearfix editBranchLogo">
                                    <label for="zipcode">Branch Logo</label>
                                    <br><span class="error-msg hide">Image does not meet the image size requirement. Please check again.</span>
                                    <br><img src="{{MigrationHelper::getBaseUrl()}}assets/images/addImage.jpg" class="imgPrev" alt="branch logo" height="64" width="64" id="eBranchLogo" />
                                        <input type="file" name="branch_logo" class="fuImage" ew="200" eh="200" style="display:none;" id="eBranchLogoFile">
                                        <input type="button" value='Choose File' onclick="document.getElementById('eBranchLogoFile').click();">
                                        <i>Required image size: 200px x 200px</i>
                                </div>
                            </div>
                        </div>
                        @if( $merchant->settings && $merchant->settings->toggle_branch_feature )
                        <div class="col-md-6">
                            <div class='form-group row'>
                                <div class="col-md-12">
                                    <label for="eShow">Show/Hide in App</label>
                                    {{ Form::select('show', ['0'=> 'Hide','1' => 'Show'], 1, ['class' => 'form-control', 'id'=>'eShow']) }}
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button class="btn btn-primary pull-right">UPDATE</button>
                        </div>
                    </div>
                    <input type="hidden" name="id" id="ebranchId">
                </form>
            </div>
        </div>
    </div>
</div>
<div id="deleteBranch" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Delete Branch</div>
            <div class="modal-body">
                <p>Are you sure you want to delete this branch?</>
                <span id="branchToDelete"></span>
                <form method="POST" id="deleteBranchForm">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button class="btn btn-primary pull-right">DELETE</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
    <div id="addEmployee" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content modlogin">
                <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
                <div class="modal-header">Add Employee</div>
                <div class="modal-body">
                    <form method="POST" action="{{ url('management/addEmployee') }}" name="postaddEmployee" id="postaddEmployee">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="employeeName">Employee Name</label>
                                <input type="text" class="form-control" name="employeeName" id="employeeName" />
                            </div>
                            <div class="col-md-8">
                                <label for="selectBranch">Select Branch</label>
                                <select class="form-control" name="selectBranch" id="selectBranch">
                                @foreach ( $branches as $b )
                                    <option value="{{ $b['id'] }}">{{ $b['branchName'] }}</option>
                                @endforeach
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label for="fourDigit">4 Digit Code <a href="#"><i class="fa fa-question-circle" data-toggle="tooltip" title="4-Digit PIN to grant employee access to merchant app"></i></a></label>
                                <input type="text" class="form-control" name="fourDigit" id="fourDigit" maxlength="4"/>
                            </div>
                            <div class="col-md-12">
                                <label for="requirePassword">Require password upon login?</label>
                                <input type="checkbox" name="requirePassword" id="requirePassword" checked>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                                <button class="btn btn-primary pull-right">ADD</button>
                            </div>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>
    <div id="editEmployee" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content modlogin">
                <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
                <div class="modal-header">Edit Employee</div>
                <div class="modal-body">
                    <form method="POST" action="{{ url('management/editEmployee') }}" id="eEmployeeSubmit">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="eEmployeeName">Employee Name</label>
                                <input type="text" class="form-control" name="employeeName" id="eEmployeeName" />
                            </div>
                            <div class="col-md-8">
                                <label for="eSelectBranch">Select Branch</label>
                                <select class="form-control" name="selectBranch" id="eSelectBranch">
                                @foreach ( $branches as $b )
                                    <option value="{{ $b['id'] }}">{{ $b['branchName'] }}</option>
                                @endforeach
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label for="eFourDigit">4 Digit Code <a href=""><i class="fa fa-question-circle"></i></a></label>
                                <input type="text" class="form-control" name="fourDigit" id="eFourDigit" maxlength="4"/>
                            </div>
                            <div class="col-md-12">
                                <label for="requirePassword">Require password upon login?</label>
                                <input type="checkbox" name="requirePassword" id="eRequirePassword" checked>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                                <button class="btn btn-primary pull-right">UPDATE</button>
                            </div>
                        </div>
                        <input type="hidden" name="employeeId" id="eEmployeeId">
                    </form>

                </div>

            </div>
        </div>
    </div>
<div id="editCustomer" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Edit Customer</div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="client_name">Name</label>
                        <input type="text" class="form-control" name="client_name" id="client_name" />
                    </div>
                    <div class="col-md-6">
                        <label for="mobile_number">Mobile Number</label>
                        <input type="text" class="form-control" name="mobile_number" id="mobile_number" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        <label for="email_address">Email Address</label>
                        <input type="text" class="form-control" name="email_address" id="email_address" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="client_code">Code</label>
                        <input type="text" class="form-control" name="client_code" id="client_code" />
                    </div>
                    @if($grade_level)
                        <div class="col-md-6">
                            <label for="grade_level">Level</label>
                            <input type="text" class="form-control" name="grade_level" id="grade_level" />
                        </div>
                    @endif
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="PIN">PIN</label>
                        <div style="display: inline-block;font-weight: normal;color: #a3a3a3;margin-left: 12px;">
                            <input type="checkbox" id="edit-pin" style="position:relative;top: 1px;"> Edit PIN
                        </div>
                        <input type="password" class="form-control" name="PIN" id="PIN" disabled/>
                    </div>

                    <div class="col-md-6">
                        <label for="membership_level_id">Membership Level</label>
                        <select name="membership_level_id" id="membership_level_id">
                            <option value="0">Regular Membership</option>
                            @foreach($membership_levels as $membership_level)
                                <option value="{{ $membership_level->id }}">{{ $membership_level->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-group row">
                    <div class="col-md-12">
                        <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                        <button class="btn btn-primary pull-right" id="update-btn">UPDATE</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="expirePackage" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Expire Package</div>
            <div class="modal-body">
                <p>Are you sure you want to expire this package? </p>
                <p>Once you expire, you may not undo it and you will need to assign a new one</p>
            </div>
            <div class="modal-footer">
                <form method="POST" action="{{ url('management/expirePointsCustomerPackage') }}" id="expirePackage">
                    {{ csrf_field() }}
                    <input type="hidden" name="customer_class_package_id">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button class="btn btn-danger pull-right">Expire</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('management.loyalty-customerModal')
<div id="deleteEmployee" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Delete Employee</div>
            <div class="modal-body">
                <p>Are you sure you want to delete this employee?</>
                <span id="employeeToDelete"></span>
                <form method="POST" id="deleteEmployeeForm">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button class="btn btn-primary pull-right">DELETE</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

@if ( $merchant->settings->code_management )
<!-- Code Management -->
<div id="addClientCode" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Add</div>
            <div class="modal-body">
                <form action="{{ url('management/addClientCode') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="client-code">Client Code</label>
                            <input type="text" name="client-code" id="client-code" class="form-control" required>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button class="btn btn-primary pull-right">SAVE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="editClientCode" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Edit</div>
            <div class="modal-body">
                <form action="{{ url('management/editClientCode') }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="eclient-code-id" id="eclient-code-id" />
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="eclient-code">Client Code</label>
                            <input type="text" name="eclient-code" id="eclient-code" class="form-control" value="" disabled>
                        </div>
                        <div class="col-md-12">
                            <label for="estatus">Status</label>
                            <select name="estatus" id="estatus" class="form-control">
                                <option value="Available">Available</option>
                                <option value="Reserved">Reserved</option>
                            </select>
                        </div>
                        <div class="col-md-12 ecustomer-field">
                            <label for="ecustomer">Reserved to Customer</label>
                            <input type="text" name="ecustomer" id="ecustomer" class="form-control">
                        </div>
                        <div class="col-md-12 assigned-customer-field">
                            <label for="assigned_customer">Assigned to Customer</label>
                            <input type="text" name="assigned_customer" id="assigned_customer" class="form-control">
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button class="btn btn-primary pull-right">SAVE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="importClientCode" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Import CSV</div>
            <div class="modal-body">
                <form action="{{ url('management/importClientCode') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{ url('page/client-code/client-code-upload.csv') }}" class="btn btn-info">
                                        <i class="" aria-hidden="true"></i> Download Sample Template
                                    </a>
                                <br /><br />
                                </div>
                                <div class="col-md-3">
                                    <button type="button" class="btn btn-browse-import">Browse</button>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" name="filename" id="filename" readonly="readonly" class="form-control">
                                    <input type="file" name="import-code" id="import-code" accept=".csv" style="visibility: hidden;">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button class="btn btn-primary pull-right">SUBMIT</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endif

@endsection

@section('footerPlaceholder')
    <script>
        var csrf_token = '{{ csrf_token() }}';
        var customer_package_summary_base_url = '{{ url('management') }}';

        $(document).ready(function () {
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
                $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
           });

            var emptyTableMessage = 'No matching records found';
            var excelExportButton = '<div class="text-center" id="export-btn-container"><button type="button" class="btn btn-default exportButton">Export to Excel</button></div>';

            $.extend($.fn.dataTableExt.oStdClasses, {
                'sWrapper': 'bootstrap-table dataTables_wrapper',
                'sFilter': 'pull-right search'
            });

            var datatableSpecs = {
                'order': [[ 0, 'desc' ]],
                'sDom': '<"fixed-table-toolbar"f><"excelButton hide"B><"customButton">rt<"bottom"ilp><"clear">',
                'buttons': [
                    'excel'
                ],
                'language': {
                    'emptyTable': emptyTableMessage
                },
                'oLanguage': {
                   'sSearch': ''
                },
                'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
                'scrollX': true,
                'sScrollX' : '100%',
                'scrollY': '300px',
                'sScrollY' : '100%',
                'autoWidth': false
            };

            var dataTableAjax = {
                processing: true,
                serverSide: true
            };

            var classSummaryTable = $('#classSummaryTable').DataTable($.extend({}, datatableSpecs, dataTableAjax, {
                ajax: {
                    'url': customer_package_summary_base_url + '/class-package-summary',
                },
                columns: [
                    {data: 'timestamp', name: 'Date Registered'},
                    {data: 'fullName', name: 'Customer Name'},
                    {data: 'mobileNumber', name: 'Mobile No.'},
                    {data: 'punchCard', name: 'Package'},
                    {data: 'validUntil', name: 'Valid Until'},
                    {data: 'status', name: 'Status'},
                    {data: 'target', name: 'Target'},
                    {data: 'accomplished', name: 'Accomplished'},
                    {data: 'balance', name: 'Balance'},
                    {data: 'branch', name: 'Branch'},
                    { data: 'status', name: 'Action',
                        render : function(data, type, row){
                            var status = row.status;

                            if(status === "Active"){
                                return "<a href='#' data-package-id='" + row.customerClassPackageId + "' class='expire-customer-package btn btn-xs btn-danger'>Expire</a>"
                            }

                            return '';
                        }
                    }
                ],
                initComplete: function () {
                    $('select[name=classSummaryTable_length]').unwrap();
                    $('#classSummaryTable_length').addClass('dataTables_info');
                    var api = this.api();
                }
            }));
            $('#class-summary').append(excelExportButton);

            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
                $($.fn.dataTable.tables(true)).DataTable()
                    .columns.adjust();
            });

            $('#classSummaryTable').on('click', '.expire-customer-package', function(){
                var me = $(this);
                var customer_package_id = me.attr('data-package-id');
                var target_modal = $('#expirePackage');

                target_modal.find('[name="customer_class_package_id"]').val(customer_package_id);
                target_modal.modal('show');
            });

            var Edit_Customer = {
                reference_element : '',

                editable_properties : {
                    customer_id : '',
                    client_name : '',
                    mobile_number : '',
                    email_address : '',
                    client_code : '',
                    grade_level : '',
                    membership_level : '',
                    membership_level_id : '',
                    PIN : '',
                    change_pin : false
                },

                init : function(){
                    var _edit_customer_obj = Edit_Customer;

                    var edit_modal = $('#editCustomer');
                    var edit_modal_update_btn = edit_modal.find('#update-btn');

                    $('body').on('click', '.editCustomerBtn', function(e){
                        e.preventDefault();

                        _edit_customer_obj.reference_element = $(this);
                        _edit_customer_obj.assignObjectPropertiesFromRow();
                        _edit_customer_obj.processEditCustomerModal();
                    });

                    $('#edit-pin').on('click', function(){
                        var me = $(this);
                        var pin_textfield = $('input[name="PIN"]');

                        if(me.prop('checked')){
                            pin_textfield.attr('type', 'text').removeAttr('disabled').val("");
                            _edit_customer_obj.editable_properties.change_pin = true;
                        } else {
                            pin_textfield.val(_edit_customer_obj.editable_properties.PIN)
                                .attr('type', 'password')
                                .attr('disabled', 'disabled');
                            _edit_customer_obj.editable_properties.change_pin = false;
                        }

                    });

                    edit_modal_update_btn.on('click', function(e){
                        _edit_customer_obj.saveCustomerProperties();
                    });
                },

                assignObjectPropertiesFromRow : function() {
                    var _reference_element = Edit_Customer.reference_element;
                    var _editable_properties = Edit_Customer.editable_properties;

                    var parent_tr = _reference_element.parents('tr');

                    _editable_properties.customer_id = parent_tr.find('.editable.client-name').attr('data-customer-id');
                    _editable_properties.PIN = parent_tr.find('.editable.client-name').attr('data-customer-pin');
                    _editable_properties.membership_level_id = parent_tr.find('.editable.client-name').attr('data-membership-id');
                    _editable_properties.membership_level = parent_tr.find('.editable.membership-level').text();
                    _editable_properties.client_name = parent_tr.find('.editable.client-name').text();
                    _editable_properties.mobile_number = parent_tr.find('.editable.mobile-number').text();
                    _editable_properties.email_address = parent_tr.find('.editable.email-address').text();
                    _editable_properties.client_code = parent_tr.find('.editable.client-code').text();
                    _editable_properties.grade_level = parent_tr.find('.editable.grade-level').text();
                },

                assignObjectPropertiesFromModal : function() {
                    var _editable_properties = Edit_Customer.editable_properties;

                    var target_modal = $('#editCustomer');

                    _editable_properties.client_name = target_modal.find('[name="client_name"]').val();
                    _editable_properties.membership_level_id = target_modal.find('[name="membership_level_id"]').val();
                    _editable_properties.membership_level = target_modal.find('[name="membership_level_id"] option:selected').text();
                    _editable_properties.mobile_number = target_modal.find('[name="mobile_number"]').val();
                    _editable_properties.email_address = target_modal.find('[name="email_address"]').val();
                    _editable_properties.client_code = target_modal.find('[name="client_code"]').val();
                    _editable_properties.grade_level = target_modal.find('[name="grade_level"]').val();
                    _editable_properties.PIN = target_modal.find('[name="PIN"]').val();
                },

                assignObjectPropertiesToRow : function(){
                    var _edit_customer_obj = Edit_Customer;
                    var _editable_properties = _edit_customer_obj.editable_properties;
                    var _reference_element = _edit_customer_obj.reference_element;

                    var parent_tr = _reference_element.parents('tr');

                    parent_tr.find('.editable.client-name').attr('data-customer-pin', _editable_properties.PIN);
                    parent_tr.find('.editable.client-name').attr('data-membership-id', _editable_properties.membership_level_id);
                    parent_tr.find('.editable.client-name').text(_editable_properties.client_name);
                    parent_tr.find('.editable.membership-level').text(_editable_properties.membership_level);
                    parent_tr.find('.editable.mobile-number').text(_editable_properties.mobile_number);
                    parent_tr.find('.editable.email-address').text(_editable_properties.email_address);
                    parent_tr.find('.editable.client-code').text(_editable_properties.client_code);
                    parent_tr.find('.editable.grade-level').text(_editable_properties.grade_level);
                },

                assignObjectPropertiesToModal : function(){
                    var _editable_properties = Edit_Customer.editable_properties;

                    var target_modal = $('#editCustomer');

                    target_modal.find('[name="client_name"]').val(_editable_properties.client_name);
                    target_modal.find('[name="membership_level_id"]').val(_editable_properties.membership_level_id);
                    target_modal.find('[name="mobile_number"]').val(_editable_properties.mobile_number);
                    target_modal.find('[name="email_address"]').val(_editable_properties.email_address);
                    target_modal.find('[name="client_code"]').val(_editable_properties.client_code);
                    target_modal.find('[name="grade_level"]').val(_editable_properties.grade_level);
                    target_modal.find('[name="PIN"]').val(_editable_properties.PIN);
                },

                processEditCustomerModal : function(){
                    var _edit_customer_obj = Edit_Customer;

                    var target_modal = $('#editCustomer');

                    _edit_customer_obj.assignObjectPropertiesToModal();

                    target_modal.modal('show');
                },

                saveCustomerProperties : function(){
                    var _edit_customer_obj = Edit_Customer;

                    var target_modal = $('#editCustomer');

                    target_modal.find('#update-btn').prop('disabled', true);

                    _edit_customer_obj.assignObjectPropertiesFromModal();

                    $.ajax({
                        method : 'post',
                        data : _edit_customer_obj.editable_properties,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url : 'management/editCustomerData',
                        success : function(response){
                            if(typeof response.message !== "undefined"){
                                alert(response.message);
                                target_modal.find('#update-btn').prop('disabled', false);
                                _edit_customer_obj.assignObjectPropertiesFromRow();
                            } else {
                                target_modal.find('#update-btn').prop('disabled', false);
                                target_modal.find('#edit-pin').prop('checked', false);
                                target_modal.find('input[name="PIN"]').attr('type', 'password').attr('disabled', 'disabled');
                                target_modal.modal('hide');

                                _edit_customer_obj.editable_properties.change_pin = false;
                                _edit_customer_obj.assignObjectPropertiesToRow();
                            }
                        }
                    });
                }
            };

            Edit_Customer.init();

            @if ( $merchant->settings->code_management )
            
            $(document).on('click', '.editClientCodeButton', function() {
                $("#eclient-code").val( $(this).data('code') );
                $("#eclient-code-id").val( $(this).data('id') );

                if ( $(this).data('status') == "Reserved" ) {
                    $(".ecustomer-field").show();
                    $(".assigned-customer-field").hide();
                    $("#ecustomer").attr( 'disabled', false );
                    $("#ecustomer").val( $(this).data('rcustomer') );
                    $("#estatus").attr( 'disabled', false );
                    $("#estatus option[value='Assigned']").remove();
                }
                if ( $(this).data('status') == "Available" ) {
                    $(".ecustomer-field").hide();
                    $(".assigned-customer-field").hide();
                    $("#estatus").attr( 'disabled', false );
                    $("#estatus option[value='Assigned']").remove();
                }

                if ( $(this).data('status') == "Assigned" ) {
                    $("#estatus").attr( 'disabled', true );
                    $(".ecustomer-field").show();
                    $(".assigned-customer-field").show();
                    $("#ecustomer").val( $(this).data('rcustomer') );
                    $("#ecustomer").attr( 'disabled', true );
                    $("#assigned_customer").val( $(this).data('acustomer') );
                    $("#assigned_customer").attr( 'disabled', true );
                    $("#estatus").append('<option value="Assigned">Assigned</option>');
                }
                $("#estatus").val( $(this).data('status') );
            });

            $(document).on('change', '#estatus', function() {

                if ( $('#estatus').val() == "Available") {
                    $(".ecustomer-field").hide();
                    $(".assigned-customer-field").hide();
                    $("#ecustomer").val('');
                    $("#ecustomer").attr( 'required', false);
                }

                if ( $('#estatus').val() == "Reserved") {
                    $(".ecustomer-field").show();
                    $("#ecustomer").attr( 'disabled', false );
                    $("#ecustomer").val('');
                    $("#ecustomer").attr( 'required', true);
                }
                
            });

            // Code Management
            $('.btn-browse-import').on('click', function(){
                $('#import-code').click();
            });
            $('#import-code').on('change', function(){
                var _filename = $(this)[0].files[0].name;
                $('#filename').val(_filename);
            });
            @endif
        });
    </script>
@endsection