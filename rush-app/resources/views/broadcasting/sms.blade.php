@extends('layouts.app')

@section('view')
<div class="container">
    @include('flash.message')
    <div id="content" class="promos spacing settings">
        <div class="noTab">
            <div class="row">
                {{ Form::open(array('url' => 'broadcast/smsCredential', 'method' => 'post', 'files' => true)) }}
                {{ csrf_field() }}
                <div class="col-md-6">
                    <h4>Shortcode and Globelabs Keys</h4>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            {{ Form::label('shortcode', 'shortcode') }}
                            {{ Form::text('shortcode',  $smsCredential->shortcode, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            {{ Form::label('app_id', 'app_id') }}
                            {{ Form::text('app_id',  $smsCredential->app_id, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            {{ Form::label('app_secret', 'app_secret') }}
                            {{ Form::text('app_secret',  $smsCredential->app_secret, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            {{ Form::label('passphrase', 'passphrase') }}
                            {{ Form::text('passphrase',  $smsCredential->passphrase, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            {{ Form::submit('update') }}
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
            <div class="col-md-6">
                {{ Form::open(array('url' => 'broadcast/smsCredential/smsTry')) }}
                <h4>Try Sms</h4>
                <div class="row">
                    <div class="col-md-6 form-group">
                        {{ Form::label('mobile', 'mobile') }}
                        {{ Form::text('mobile',  $smsCredential->mobile, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        <label for="pointsName">message</label>
                        {{ Form::textarea(
                            'message',
                            date('Y-m-d H:i:s', time()),
                            [   'rows' => '5',
                                'placeholder' => 'enter sms message'
                        ]) }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        {{ Form::submit('send') }}
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div> <!-- /container -->
@endsection

@section('footerPlaceholder')
@parent
@endsection
