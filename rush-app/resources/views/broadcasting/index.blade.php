@extends('layouts.app')

@section('view')
    <div class="container">
        @include('flash.message')
        <div id="content" class="smstool marginTop">
            <!-- Nav tabs -->
            <div class="navTabsWrap">
                <ul class="nav nav-tabs" role="tablist" id="tabs">
                  @if (ToolHelper::isAllowed(ToolHelper::TOOL_SMS_ID, $merchant))
                    <li role="presentation"><a href="#sms" class="sms-broadcast-tab" aria-controls="sms" role="tab" data-toggle="tab">SMS BROADCAST</a></li>
                  @endif
                  @if (ToolHelper::isAllowed(ToolHelper::TOOL_PUSH_ID, $merchant))
                    <li role="presentation"><a href="#pushnotif" class="push-notif-tab"  aria-controls="pushnotif" role="tab" data-toggle="tab">PUSH NOTIFICATIONS</a></li>
                  @endif
                  @if (ToolHelper::isAllowed(ToolHelper::TOOL_EMAIL_ID, $merchant))
                    <li role="presentation"><a href="#emailbroad" class="email-broadcast-tab" aria-controls="emailbroad" role="tab" data-toggle="tab">EMAIL BROADCAST</a></li>
                  @endif
                  @if (ToolHelper::isAllowed(ToolHelper::TOOL_SEGMENT_ID, $merchant))
                    <li role="presentation"><a href="#targetseg" class="target-segment-tab" aria-controls="targetseg" role="tab" data-toggle="tab">TARGET SEGMENT</a></li>
                  @endif
                  @if (ToolHelper::isAllowed(ToolHelper::TOOL_EVENTS_PROMOS_ID, $merchant))
                    <li role="presentation"><a href="#targetEvent" class="broadcast-events-tab" aria-controls="targetEvent" role="tab" data-toggle="tab">EVENTS</a></li>
                  @endif
                </ul>
            </div>

            <!-- Tab panes -->
            <div class="tab-content">
                @if (ToolHelper::isAllowed(ToolHelper::TOOL_SMS_ID, $merchant))
                <div role="tabpanel" class="tab-pane" id="sms">
                    <button href="#addSMS" class="btn btn-primary smsAdd" data-toggle="modal" onclick="return false">Add SMS Broadcast</button> Total SMS Sent [Life time: {{$smsSentAll or 0}} | This Month: {{$smsSentThisMonth or 0}}]
                    <table id="smsTable" data-toggle="table" class="data-table management" data-pagination="true" data-search="true">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th data-sortable="true">Date Published</th>
                            <th>Segment</th>
                            <th>Message</th>
                            <th>Sent</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if (count($smss) > 0)
                        @foreach ($smss as $sms)
                        <tr>
                            <td>{{$sms->id}}</td>
                            <td>{{$sms->publish_date}}</td>
                            <td>{{$sms->segment_name or 'All'}}</td>
                            <td>{{str_limit($sms->message, 21)}}</td>
                            <td>{{$sms->sent_count}}</td>
                            <td><span class="stat s{{BroadcastHelper::getStatus($sms->status)}}">{{BroadcastHelper::getStatus($sms->status)}}</span></td>
                            <td>
                                @unless($sms->status != BroadcastHelper::INACTIVE)
                                <a href="#" data-toggle="modal" data-target="#deleteSMS" data-action="sms/{{$sms->id}}" onclick="return false" class="delete smsDelete"><i class="fa fa-trash"></i>Delete</a>
                                @endunless
                            </td>
                        </tr>
                        @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                @endif

                @if (ToolHelper::isAllowed(ToolHelper::TOOL_PUSH_ID, $merchant))
                <div role="tabpanel" class="tab-pane" id="pushnotif">
                    <button href="#addPushNotif" class="btn btn-primary smsAdd" data-toggle="modal" onclick="return false">New Notification</button>
                    <table id="pushTable" data-toggle="table" class="data-table management" data-pagination="true" data-search="true">
                        <thead>
                        <tr>
                            <th>Campaign Name</th>
                            <th>Segment</th>
                            <th>Message</th>
                            <th>Device</th>
                            <th>Schedule</th>
                            <th>Notified</th>
                            <th>Failed</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if (count($pushs) > 0)
                        @foreach ($pushs as $push)
                        <tr>
                            <td>{{$push->campaign}}</td>
                            <td>{{$push->segment_name or 'All'}}</td>
                            <td>{{str_limit($push->message, 21)}}</td>
                            <td>{{$push->device}}</td>
                            <td>{{$push->publish_date}}</td>
                            <td>{{$push->sent_count}}</td>
                            <td>{{$push->failed_count}}</td>
                            <td><span class="stat s{{BroadcastHelper::getStatus($push->status)}}">{{BroadcastHelper::getStatus($push->status)}}</span></td>
                            <td>
                                @unless($push->status != BroadcastHelper::INACTIVE)
                                <a href="#" data-toggle="modal" data-target="#deletePushNotif" data-action="push/{{$push->id}}" onclick="return false" class="delete pushDelete"><i class="fa fa-trash"></i> Delete</a>
                                @endunless
                            </td>
                        </tr>
                        @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                @endif

                @if (ToolHelper::isAllowed(ToolHelper::TOOL_EMAIL_ID, $merchant))
                <div role="tabpanel" class="tab-pane" id="emailbroad">
                    <button href="#addEmail" class="btn btn-primary smsAdd" data-toggle="modal" onclick="return false">Add Email Broadcast</button>
                    <table id="emailTable" data-toggle="table" class="data-table management" data-pagination="true" data-search="true">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th data-sortable="true">Date Published</th>
                            <th>Segment</th>
                            <th>Message</th>
                            <th>Sent</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if (count($emails) > 0)
                        @foreach ($emails as $email)
                        <tr>
                            <td>{{$email->id}}</td>
                            <td>{{$email->publish_date}}</td>
                            <td>{{$email->segment_name or 'All'}}</td>
                            <td>{{str_limit($email->message, 21)}}</td>
                            <td>{{$email->sent_count}}</td>
                            <td><span class="stat s{{BroadcastHelper::getStatus($email->status)}}">{{BroadcastHelper::getStatus($email->status)}}</span></td>
                            <td>
                                @if($email->subject)
                                <a href="{{MigrationHelper::getAppBaseUrl()."e/$email->id/$email->url_slug"}}" target='_blank'><i class="fa fa-file-o"></i>Preview</a>
                                @endif
                                @unless($email->status != BroadcastHelper::INACTIVE)
                                <a href="#" data-toggle="modal" data-target="#deleteEmail" data-action="email/{{$email->id}}" onclick="return false" class="delete emailDelete"><i class="fa fa-trash"></i>Delete</a>
                                @endunless
                            </td>
                        </tr>
                        @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                @endif

                @if (ToolHelper::isAllowed(ToolHelper::TOOL_SEGMENT_ID, $merchant))
                <div role="tabpanel" class="tab-pane" id="targetseg">
                    <button href="#addTargetSeg" class="btn btn-primary smsAdd target-segment-button" data-toggle="modal" onclick="return false">New Target Segment</button>
                    <table id="segmentTable" data-toggle="table" class="data-table management" data-pagination="true" data-search="true">
                        <thead>
                        <tr>
                            <th>Segment Name</th>
                            <th>Filter</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if (count($segments) > 0)
                        @foreach ($segments as $segment)
                        <tr>
                            <td>{{$segment->name}}</td>
                            <td>{{$segment->scopes}}</td>
                            <td>
                                @if ($segment->status)
                                <span class="stat sActive">Active</span>
                                @else
                                <span class="stat sInactive">Inactive</span>
                                @endif
                            </td>
                            <td>
                                <a href="#" data-toggle="modal" data-target="#deleteTargetSeg" data-action="segment/{{$segment->id}}" onclick="return false" class="delete segmentDelete"><i class="fa fa-trash"></i> Delete</a>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                @endif

                @if (ToolHelper::isAllowed(ToolHelper::TOOL_EVENTS_PROMOS_ID, $merchant))
                <div role="tabpanel" class="tab-pane" id="targetEvent">
                    <button href="#addEvent" class="btn btn-primary smsAdd eventAdd" data-action="{{ url('broadcast/event/add') }}" data-toggle="modal" onclick="return false">New Event</button>
                    <table id="eventTable" data-toggle="table" class="data-table management" data-pagination="true" data-search="true">
                        <thead>
                        <tr>
                            <th data-sortable="true">ID</th>
                            <th data-sortable="true">Title</th>
                            <th data-sortable="true">Category</th>
                            @if($enable_post_sub_category != '0')
                                <th data-sortable="true">Sub-Category</th>
                            @endif
                            <th data-sortable="true">Sequence</th>
                            <th>Status</th>
                            <th class="text-right">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if ( count($merchant_events) > 0 )
                        @foreach ($merchant_events as $event)
                        <tr>
                            <td>{{ $event->id }}</td>
                            <td>{{ $event->post_title }}</td>
                            <td>
                                @if( $event->categories )
                                    @foreach( $event->categories as $category)
                                        {{ $category->category->name }}
                                    @endforeach
                                @endif
                            </td>
                            @if($enable_post_sub_category != '0')
                                <td>{{ ucwords(str_replace('_', ' ', $event->post_sub_category)) }}</td>
                            @endif
                            <td>{{ $event->post_order }}</td>
                            <td>{{ $event->post_status }}</td>
                            <td>
                                @if( $event->post_status == 'draft')
                                    <a href="#" data-toggle="modal" data-target="#publishTargetEvent" data-action="{{ url('broadcast/event/publish/' . $event->id) }}" onclick="return false" class="delete eventPublish"><i class="fa fa-toggle-up"></i>Publish</a>
                                @else
                                    <a href="#" data-toggle="modal" data-target="#deleteTargetEvent" data-action="{{ url('broadcast/event/delete/' . $event->id) }}" onclick="return false" class="delete eventDelete"><i class="fa fa-trash"></i> Unpublished</a>
                                @endif

                                <a href="#" data-toggle="modal" data-type="{{ $event->post_type }}" data-category="{{ $category->category->id }}" data-sub-category="{{ $event->post_sub_category }}" data-color1="{{ $event->event_color1 }}" data-color2="{{ $event->event_color2 }}" data-target="#editEvent" data-img="{{ $event->post_thumbnail }}" data-details="{{ $event->post_content }}" data-action="{{ url('broadcast/event/edit/' . $event->id ) }}" data-url="{{ $event->url }}"onclick="return false" class="edit eventEdit"><i class="fa fa-edit"></i> Edit</a>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                @endif

            </div>
        </div>
    </div> <!-- /container -->
@endsection

@section('modal')
<!--  MODALS -->
<!-- Modal: SMS -->
<div id="addSMS" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Add SMS Broadcast</div>
            <div class="modal-body">
                <form method="POST" action="sms">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="row">
                                <h4 class="col-md-6">Information</h4>
                                <h4 class="col-md-6">Filter Information</h4>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="smsPublishDate">Publish Date</label>
                                <div class='input-group date publishDate datetimepicker' id='smsPublishDateStart'>
                                  <input type='text' class="form-control" name="smsPublishDate" id="smsPublishDate" placeholder="Select Date"  data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="{{ $durationStart }}" required>
                                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                            <div class="col-md-6 filterInfo">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="targetSegment">Target Segment</label>
                                        @if($user_type == 3)
                                            <select class="form-control" name="targetSegment" id="targetSegment" required>
                                                @if (count($activeSegments) > 0)
                                                    @foreach ($activeSegments as $activeSegment)
                                                        <option value="{{$activeSegment->id}}">{{$activeSegment->name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        @else
                                            <select class="form-control" name="targetSegment" id="targetSegment">
                                                <option>All Segment</option>
                                                @if (count($activeSegments) > 0)
                                                @foreach ($activeSegments as $activeSegment)
                                                <option value="{{$activeSegment->id}}">{{$activeSegment->name}}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="smsPublishTime">Publish Time</label>
                                <select required class="form-control" name="smsPublishTime" id="smsPublishTime">
                                    <option value="">Select Time</option>
                                    <option>12:00 AM</option>
                                    <option>01:00 AM</option>
                                    <option>02:00 AM</option>
                                    <option>03:00 AM</option>
                                    <option>04:00 AM</option>
                                    <option>05:00 AM</option>
                                    <option>06:00 AM</option>
                                    <option>07:00 AM</option>
                                    <option>08:00 AM</option>
                                    <option>09:00 AM</option>
                                    <option>10:00 AM</option>
                                    <option>11:00 AM</option>
                                    <option>12:00 PM</option>
                                    <option>01:00 PM</option>
                                    <option>02:00 PM</option>
                                    <option>03:00 PM</option>
                                    <option>04:00 PM</option>
                                    <option>05:00 PM</option>
                                    <option>06:00 PM</option>
                                    <option>07:00 PM</option>
                                    <option>08:00 PM</option>
                                    <option>09:00 PM</option>
                                    <option>10:00 PM</option>
                                    <option>11:00 PM</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="message">Message</label>
                                <textarea required class="form-control withCounter" name="message" id="message" cols="30" rows="3" placeholder="Enter SMS message" maxlength="480">{{ Input::old('message') }}</textarea>
								<small><span class="textCounter">0</span>/480</small>
                                <small style="float:right">Part<span class="messagePart"> 1</span> of 3</small>
							</div>
                            </br>
                            <div class="col-md-6">
                                <div class="alert alert-danger alert-dismissible" id="messageExceed" style="display: none">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    You will be charged an SMS for every 160-characters.
                                  </div>
                            </div>
                        </div>
                    <div class="col-md-12 margT20">
                        <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                        <button class="btn btn-primary pull-right add-sms-button">ADD</button>
                    </div>
                </form>
            </div>
        </div>
        </div>
    </div>
</div>

<div id="deleteSMS" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Delete SMS</div>
            <div class="modal-body">
                <p>Are you sure you want to delete this SMS Broadcast?</>
                <span id="smsToDelete"></span>
                {{ Form::open(array('id' => 'deleteSmsForm')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button class="btn btn-primary pull-right">DELETE</button>
                        </div>
                    </div>
                {{ Form::close() }}
            </div>

        </div>
    </div>
</div>

<!-- Modal: Push Notifications -->
<div id="addPushNotif" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Add Notification</div>
            <div class="modal-body">
                <form method="POST" action="{{MigrationHelper::getAppBaseUrl()}}broadcast/push">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-6">
                            <h4>Basic Information</h4>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="notifCampaignName">Campaign Name</label>
                                    <input required type="text" class="form-control" placeholder="Enter notification name" name="notifCampaignName" id="notifCampaignName" value="{{  Input::old('notifCampaignName') }}"/>
                                </div>
                                <div class="col-md-12">
                                    <label for="notifCampaignDesc">Description</label>
                                    <textarea class="form-control" name="notifCampaignDesc" id="notifCampaignDesc" cols="30" rows="3" placeholder="Enter notification description">{{  Input::old('notifCampaignDesc') }}</textarea>
                                </div>
                                <div class="col-md-12">
                                    <label>Device Type</label>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label><input required type="radio" name="notifDeviceType" id="notifDeviceTypeA" value="both"/> Both</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input required type="radio" name="notifDeviceType" id="notifDeviceTypeB" value="android"/> Android</label>
                                        </div>
                                        <div class="col-md-3">
                                            <label><input required type="radio" name="notifDeviceType" id="notifDeviceTypeC" value="ios"/> iOS</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="sched-run notifSchedRun">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="notifRunDate">Select Date</label>
                                                <div class='input-group date runDate datetimepicker' id='notifRunDateStart'>
                                                    <input type='text' class="form-control dAble" name="notifRunDate" id="notifRunDate" placeholder="Select Date"  data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="{{ $durationStart }}" required >
                                                    <span class="input-group-addon out"><i class="fa fa-calendar"></i></span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="notifRunTime">Select Time</label>
                                                <select required class="form-control dAble" name="notifRunTime" id="notifRunTime" >
                                                    <option value="">Select Time</option>
                                                    <option>12:00 AM</option>
                                                    <option>01:00 AM</option>
                                                    <option>02:00 AM</option>
                                                    <option>03:00 AM</option>
                                                    <option>04:00 AM</option>
                                                    <option>05:00 AM</option>
                                                    <option>06:00 AM</option>
                                                    <option>07:00 AM</option>
                                                    <option>08:00 AM</option>
                                                    <option>09:00 AM</option>
                                                    <option>10:00 AM</option>
                                                    <option>11:00 AM</option>
                                                    <option>12:00 PM</option>
                                                    <option>01:00 PM</option>
                                                    <option>02:00 PM</option>
                                                    <option>03:00 PM</option>
                                                    <option>04:00 PM</option>
                                                    <option>05:00 PM</option>
                                                    <option>06:00 PM</option>
                                                    <option>07:00 PM</option>
                                                    <option>08:00 PM</option>
                                                    <option>09:00 PM</option>
                                                    <option>10:00 PM</option>
                                                    <option>11:00 PM</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h4>Notification Details</h4>
                            <hr>

                            <div class="row">
                                <div class="col-md-12">
                                    <label for="targetSegment">Target Segment</label>
                                    @if($user_type == 3)
                                        <select class="form-control" name="targetSegment" id="targetSegment" required>
                                            @if (count($activeSegments) > 0)
                                                @foreach ($activeSegments as $activeSegment)
                                                    <option value="{{$activeSegment->id}}">{{$activeSegment->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    @else
                                        <select class="form-control" name="targetSegment" id="targetSegment">
                                            <option>All Segment</option>
                                            @if (count($activeSegments) > 0)
                                            @foreach ($activeSegments as $activeSegment)
                                            <option value="{{$activeSegment->id}}">{{$activeSegment->name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    @endif
                                </div>
                                <div class="col-md-12">
                                    <label for="notifMessage">Message</label>
                                    <textarea required class="form-control" name="notifMessage" id="notifMessage" cols="30" rows="3" placeholder="Enter notification message">{{ Input::old('notifMessage') }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 margT20">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button class="btn btn-primary pull-right add-push-notif-button">ADD</button>
                        </div>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>
<div id="editPushNotif" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Edit Notification</div>
            <div class="modal-body">
                <form method="POST" action="/sms">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <h4>Basic Information</h4>
                            <hr>

                            <div class="row">
                                <div class="col-md-12">
                                    <input type="checkbox" name="enotifActivate" id="enotifActivate"><label for="enotifActivate">&nbsp; Activate</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <label for="enotifCampaignName">Campaign Name</label>
                                    <input type="text" class="form-control" placeholder="Enter notification name" name="enotifCampaignName" id="enotifCampaignName"/>
                                </div>
                                <div class="col-md-12">
                                    <label for="enotifCampaignDesc">Description</label>
                                    <textarea class="form-control" name="enotifCampaignDesc" id="enotifCampaignDesc" cols="30" rows="3" placeholder="Enter notification description"></textarea>
                                </div>
                                <div class="col-md-12">
                                    <label>Device Type</label>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label><input type="radio" name="enotifDeviceType" id="enotifDeviceTypeA"/> Both</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" name="enotifDeviceType" id="enotifDeviceTypeB"/> Android</label>
                                        </div>
                                        <div class="col-md-3">
                                            <label><input type="radio" name="enotifDeviceType" id="enotifDeviceTypeC"/> iOS</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <input type="checkbox" name="enotifSchedRun" id="enotifSchedRun" class="enableOption" /><label for="enotifSchedRun">&nbsp; Schedule Run</label>

                                    <div class="sched-run enotifSchedRun">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="enotifRunDate">Select Date</label>
                                                <div class='input-group date runDate datetimepicker' id='enotifRunDateStart'>
                                                    <input type='text' class="form-control dAble" name="enotifRunDate" id="enotifRunDate" placeholder="Select Date"  data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="{{ $durationStart }}" required disabled >
                                                    <span class="input-group-addon out" ><i class="fa fa-calendar"></i></span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="enotifRunTime">Select Time</label>
                                                <select class="form-control dAble" name="enotifRunTime" id="enotifRunTime" disabled >
                                                    <option>Select Time</option>
                                                    <option>12:00 AM</option>
                                                    <option>01:00 AM</option>
                                                    <option>02:00 AM</option>
                                                    <option>03:00 AM</option>
                                                    <option>04:00 AM</option>
                                                    <option>05:00 AM</option>
                                                    <option>06:00 AM</option>
                                                    <option>07:00 AM</option>
                                                    <option>08:00 AM</option>
                                                    <option>09:00 AM</option>
                                                    <option>10:00 AM</option>
                                                    <option>11:00 AM</option>
                                                    <option>12:00 PM</option>
                                                    <option>01:00 PM</option>
                                                    <option>02:00 PM</option>
                                                    <option>03:00 PM</option>
                                                    <option>04:00 PM</option>
                                                    <option>05:00 PM</option>
                                                    <option>06:00 PM</option>
                                                    <option>07:00 PM</option>
                                                    <option>08:00 PM</option>
                                                    <option>09:00 PM</option>
                                                    <option>10:00 PM</option>
                                                    <option>11:00 PM</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h4>Notification Details</h4>
                            <hr>

                            <div class="row">
                                <div class="col-md-12">
                                    <label for="etargetSegment">Target Segment</label>
                                    <select class="form-control" name="etargetSegment" id="etargetSegment">
                                        <option>All Segment</option>
                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <label for="enotifMessage">Message</label>
                                    <textarea class="form-control" name="enotifMessage" id="enotifMessage" cols="30" rows="3" placeholder="Short description"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 margT20">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button class="btn btn-primary pull-right">ADD</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<div id="deletePushNotif" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Delete Push Notification</div>
            <div class="modal-body">
                <p>Are you sure you want to delete this Push Notification?</>
                <span id="notifToDelete"></span>
                <form method="POST" id="deleteNotifForm">
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button class="btn btn-primary pull-right">DELETE</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<div id="addTargetSeg" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Add Segment Details</div>
            <div class="modal-body">
                <form method="POST" action="{{MigrationHelper::getAppBaseUrl()}}broadcast/segment">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-6">
                            <h4>Segment Information</h4>
                            <hr>

                            <div class="row">
                                <div class="col-md-12">
                                    <input type="checkbox" name="segActive" checked id="segActive"><label for="segActive">&nbsp; Active</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <label for="segName">Segment Name</label>
                                    <input required type="text" class="form-control" placeholder="Enter segment name" name="segName" id="segName"/>
                                </div>
                            </div>
                            @if($merchant->merchantid == env('SMAC_ID', 0))
                            <br>
                            <br>

                            <div class="row filterInfo">
                                <div class="col-md-12">
                                    <input type="checkbox" name="targFilterMembershipType" id="targFilterMembershipType"  class="enableOption checkMe" />
                                    <label for="targFilterMembershipType">Membership Type</label>
                                    <div class="row">
                                        <div class="col-md-12 targFilterMembershipType">
                                            <div class="col-md-6">
                                                <label>{{Form::select('targFilterMembershipType0', ['Regular' => 'Regular', 'Prestige' => 'Prestige'], null, ['disabled' => '', 'class' => 'dAble checkMe'])}}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <input type="checkbox" name="targFilterAmountSpent" id="targFilterAmountSpent"  class="enableOption checkMe" />
                                    <label for="targFilterAmountSpent">Amount Spent</label>
                                    <div class="row">
                                        <div class="col-md-12 targFilterAmountSpent">
                                            <div class="col-md-6">
                                                <label>{{Form::select('targFilterAmountSpent0', ['5,000-10,000' => '5,000 - 10,000', '10,001-15,000' => '10,001 - 15,000', '15,001-20,000' => '15,001 - 20,000', '20,001+' => '20,001 and above'], null, ['disabled' => '', 'class' => 'dAble checkMe'])}}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <input type="checkbox" name="targFilterIndustry" id="targFilterIndustry"  class="enableOption checkMe" />
                                    <label for="targFilterIndustry">Industry</label>
                                    <div class="row">
                                        <div class="col-md-12 targFilterIndustry">
                                            <div class="col-md-6">
                                                <label>{{Form::select('targFilterIndustry0', ['shopping' => 'Shopping', 'lifestyle' => 'Life Style', 'transpo' => 'Transpo', 'travel' => 'Travel'], null, ['disabled' => '', 'class' => 'dAble checkMe'])}}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <input type="checkbox" name="targFilterLocation" id="targFilterLocation"  class="enableOption checkMe" />
                                    <label for="targFilterLocation">Location</label>
                                    <div class="row">
                                        <div class="col-md-12 targFilterLocation">
                                            <div class="col-md-6">
                                                <label>
                                                    <select name="targetFilterLocation0" id="targetFilterLocation0" disabled class="dAble checkMe">
                                                        <option value=''>Select location</option>
                                                        <option value='Alaminos'>Alaminos</option>
                                                        <option value='Angeles'>Angeles</option>
                                                        <option value='Antipolo'>Antipolo</option>
                                                        <option value='Bacolod'>Bacolod</option>
                                                        <option value='Bacoor'>Bacoor</option>
                                                        <option value='Bago'>Bago</option>
                                                        <option value='Baguio'>Baguio</option>
                                                        <option value='Bais'>Bais</option>
                                                        <option value='Balanga'>Balanga</option>
                                                        <option value='Batac'>Batac</option>
                                                        <option value='Batangas City'>Batangas City</option>
                                                        <option value='Bayawan'>Bayawan</option>
                                                        <option value='Baybay'>Baybay</option>
                                                        <option value='Bayugan'>Bayugan</option>
                                                        <option value='Biñan'>Biñan</option>
                                                        <option value='Bislig'>Bislig</option>
                                                        <option value='Bogo'>Bogo</option>
                                                        <option value='Borongan'>Borongan</option>
                                                        <option value='Butuan'>Butuan</option>
                                                        <option value='Cabadbaran'>Cabadbaran</option>
                                                        <option value='Cabanatuan'>Cabanatuan</option>
                                                        <option value='Cabuyao'>Cabuyao</option>
                                                        <option value='Cadiz'>Cadiz</option>
                                                        <option value='Cagayan de Oro'>Cagayan de Oro</option>
                                                        <option value='Calamba'>Calamba</option>
                                                        <option value='Calapan'>Calapan</option>
                                                        <option value='Calbayog'>Calbayog</option>
                                                        <option value='Caloocan'>Caloocan</option>
                                                        <option value='Candon'>Candon</option>
                                                        <option value='Canlaon'>Canlaon</option>
                                                        <option value='Carcar'>Carcar</option>
                                                        <option value='Catbalogan'>Catbalogan</option>
                                                        <option value='Cauayan'>Cauayan</option>
                                                        <option value='Cavite City'>Cavite City</option>
                                                        <option value='ManCebu Cityila'>Cebu City</option>
                                                        <option value='Cotabato City'>Cotabato City</option>
                                                        <option value='Dagupan'>Dagupan</option>
                                                        <option value='Danao'>Danao</option>
                                                        <option value='Dapitan'>Dapitan</option>
                                                        <option value='Dasmariñas'>Dasmariñas</option>
                                                        <option value='Davao City'>Davao City</option>
                                                        <option value='Digos'>Digos</option>
                                                        <option value='Dipolog'>Dipolog</option>
                                                        <option value='Dumaguete'>Dumaguete</option>
                                                        <option value='El Salvador'>El Salvador</option>
                                                        <option value='Escalante'>Escalante</option>
                                                        <option value='Gapan'>Gapan</option>
                                                        <option value='General Santos'>General Santos</option>
                                                        <option value='General Trias'>General Trias</option>
                                                        <option value='Gingoog'>Gingoog</option>
                                                        <option value='Guihulngan'>Guihulngan</option>
                                                        <option value='Himamaylan'>Himamaylan</option>
                                                        <option value='Ilagan'>Ilagan</option>
                                                        <option value='Iligan'>Iligan</option>
                                                        <option value='Iloilo City'>Iloilo City</option>
                                                        <option value='Imus'>Imus</option>
                                                        <option value='Iriga'>Iriga</option>
                                                        <option value='Isabela'>Isabela</option>
                                                        <option value='Kabankalan'>Kabankalan</option>
                                                        <option value='Kidapawan'>Kidapawan</option>
                                                        <option value='Koronadal'>Koronadal</option>
                                                        <option value='La Carlota'>La Carlota</option>
                                                        <option value='Lamitan'>Lamitan</option>
                                                        <option value='Laoag'>Laoag</option>
                                                        <option value='Lapu-Lapu'>Lapu-Lapu</option>
                                                        <option value='Las Piñas'>Las Piñas</option>
                                                        <option value='Legazpi'>Legazpi</option>
                                                        <option value='Ligao'>Ligao</option>
                                                        <option value='Lipa'>Lipa</option>
                                                        <option value='Lucena'>Lucena</option>
                                                        <option value='Maasin'>Maasin</option>
                                                        <option value='Mabalacat'>Mabalacat</option>
                                                        <option value='Makati'>Makati</option>
                                                        <option value='Malabon'>Malabon</option>
                                                        <option value='Malaybalay'>Malaybalay</option>
                                                        <option value='Malolos'>Malolos</option>
                                                        <option value='Mandaluyong'>Mandaluyong</option>
                                                        <option value='Mandaue'>Mandaue</option>
                                                        <option value='Manila'>Manila</option>
                                                        <option value='Marawi'>Marawi</option>
                                                        <option value='Marikina'>Marikina</option>
                                                        <option value='Masbate City'>Masbate City</option>
                                                        <option value='Mati'>Mati</option>
                                                        <option value='Meycauayan'>Meycauayan</option>
                                                        <option value='Muñoz'>Muñoz</option>
                                                        <option value='Muntinlupa'>Muntinlupa</option>
                                                        <option value='Naga'>Naga</option>
                                                        <option value='Navotas'>Navotas</option>
                                                        <option value='Olongapo'>Olongapo</option>
                                                        <option value='Ormoc'>Ormoc</option>
                                                        <option value='Oroquieta'>Oroquieta</option>
                                                        <option value='Ozamiz'>Ozamiz</option>
                                                        <option value='Pagadian'>Pagadian</option>
                                                        <option value='Palayan'>Palayan</option>
                                                        <option value='Panabo'>Panabo</option>
                                                        <option value='Parañaque'>Parañaque</option>
                                                        <option value='Pasay'>Pasay</option>
                                                        <option value='Pasig'>Pasig</option>
                                                        <option value='Passi'>Passi</option>
                                                        <option value='Puerto Princesa'>Puerto Princesa</option>
                                                        <option value='Quezon City'>Quezon City</option>
                                                        <option value='Roxas'>Roxas</option>
                                                        <option value='Sagay'>Sagay</option>
                                                        <option value='Samal'>Samal</option>
                                                        <option value='San Carlos'>San Carlos</option>
                                                        <option value='San Carlos'>San Carlos</option>
                                                        <option value='San Fernando'>San Fernando</option>
                                                        <option value='San Fernando'>San Fernando</option>
                                                        <option value='San Jose'>San Jose</option>
                                                        <option value='San Jose del Monte'>San Jose del Monte</option>
                                                        <option value='San Juan'>San Juan</option>
                                                        <option value='San Pablo'>San Pablo</option>
                                                        <option value='San Pedro'>San Pedro</option>
                                                        <option value='Santa Rosa'>Santa Rosa</option>
                                                        <option value='Santiago'>Santiago</option>
                                                        <option value='Silay'>Silay</option>
                                                        <option value='Sipalay'>Sipalay</option>
                                                        <option value='Sorsogon City'>Sorsogon City</option>
                                                        <option value='Surigao City'>Surigao City</option>
                                                        <option value='Tabaco'>Tabaco</option>
                                                        <option value='Tabuk'>Tabuk</option>
                                                        <option value='Tacloban'>Tacloban</option>
                                                        <option value='Tacurong'>Tacurong</option>
                                                        <option value='Tagaytay'>Tagaytay</option>
                                                        <option value='Tagbilaran'>Tagbilaran</option>
                                                        <option value='Taguig'>Taguig</option>
                                                        <option value='Tagum'>Tagum</option>
                                                        <option value='Talisay'>Talisay</option>
                                                        <option value='Tanauan'>Tanauan</option>
                                                        <option value='Tandag'>Tandag</option>
                                                        <option value='Tangub'>Tangub</option>
                                                        <option value='Tanjay'>Tanjay</option>
                                                        <option value='Tarlac City'>Tarlac City</option>
                                                        <option value='Tayabas'>Tayabas</option>
                                                        <option value='Toledo'>Toledo</option>
                                                        <option value='Trece Martires'>Trece Martires</option>
                                                        <option value='Tuguegarao'>Tuguegarao</option>
                                                        <option value='Urdaneta'>Urdaneta</option>
                                                        <option value='Valencia'>Valencia</option>
                                                        <option value='Valenzuela'>Valenzuela</option>
                                                        <option value='Victorias'>Victorias</option>
                                                        <option value='Vigan'>Vigan</option>
                                                        <option value='Zamboanga City'>Zamboanga City</option>
                                                    </select>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="col-md-6 filterInfo">
                            <h4>Filter Information</h4>
                            <hr>

                            <div class="row">
                                <div class="col-md-12 targFilterAll">
                                    <input type="checkbox" name="targFilterGender" id="targFilterGender" class="enableOption checkMe" />
                                    <label for="targFilterGender">Gender</label>
                                    <div class="row">
                                        <div class="col-md-12 targFilterGender">
                                            <div class="col-md-6">
                                                <label><input type="radio" name="targFilterGenderO" id="targFilterGenderA" disabled class="dAble checkMe" value="both" /> Both</label>
                                            </div>
                                            <div class="col-md-6">
                                                <label><input type="radio" name="targFilterGenderO" id="targFilterGenderB" disabled class="dAble" value="female" /> Female</label>
                                            </div>
                                            <div class="col-md-6">
                                                <label><input type="radio" name="targFilterGenderO" id="targFilterGenderC" disabled class="dAble" value="male" /> Male</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 targFilterAll">
                                    <input type="checkbox" name="targFilterAge" id="targFilterAge" class="enableOption checkMe" />
                                    <label for="targFilterAge">Age Group</label>
                                    <div class="row">
                                        <div class="col-md-12 targFilterAge">
                                            <div class="col-md-6">
                                                <label><input type="checkbox" name="targFilterAgeO[]" id="targFilterAgeA" disabled class="dAble checkMe" value="18-24" /> 18-24 years</label>
                                            </div>
                                            <div class="col-md-6">
                                                <label><input type="checkbox" name="targFilterAgeO[]" id="targFilterAgeB" disabled class="dAble checkMe" value="25-34" /> 25-34 years</label>
                                            </div>
                                            <div class="col-md-6">
                                                <label><input type="checkbox" name="targFilterAgeO[]" id="targFilterAgeC" disabled class="dAble checkMe" value="35-44" /> 35-44 years</label>
                                            </div>
                                            <div class="col-md-6">
                                                <label><input type="checkbox" name="targFilterAgeO[]" id="targFilterAgeD" disabled class="dAble checkMe" value="45" /> 45 years and over</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 targFilterAll">
                                    <input type="checkbox" name="targFilterBirthday" id="targFilterBirthday" class="enableOption checkMe" />
                                    <label for="targFilterBirthday">Birthday Month</label>
                                    <div class="row">
                                        <div class="col-md-12 targFilterBirthday">
                                            <div class="col-md-6">
                                                <label>{{ Form::selectMonth('targFilterBirthMonthO', \Carbon\Carbon::now()->month, ['disabled' => '', 'class' => 'dAble checkMe']) }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if($merchant->merchantid == env('NATCON_ID', 0))
                                <div class="col-md-12 targFilterAll dontFilterAll">
                                    <input type="checkbox" name="targFilterFirstCheckIn" value="1000" id="targFilterFirst" class="enableOption checkMe" />
                                    <label for="targFilterFirst" style="display: inline;">First 1000 check-ins at Breakout Session</label>
                                </div>
                                @endif
                                @if($merchant->merchantid != env('SMAC_ID', 0))
                                <div class="col-md-12">
                                    <input type="checkbox" name="targFilterProspect" id="targFilterProspect" />
                                    <label for="targFilterProspect">Prospect</label>
                                </div>
                                @endif
                                <div class="col-md-12">
                                    <input type="checkbox" name="targFilterNewMember" id="targFilterNewMember" />
                                    <label for="targFilterNewMember">New Member</label>
                                </div>
                                <div class="col-md-12">
                                    <input type="checkbox" name="targFilterTopCustomer" id="targFilterTopCustomer"  class="enableOption checkMe" />
                                    <label for="targFilterTopCustomer">Top Customer</label>
                                    <div class="row">
                                        <div class="col-md-12 targFilterTopCustomer">
                                            <div class="col-md-6">
                                                <label>{{Form::select('targFilterTopCustomer0', ['50' => 50, '100' => 100, '250' => 250, '500' => 500, '1000' => 1000], null, ['disabled' => '', 'class' => 'dAble checkMe'])}}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <input type="checkbox" name="targFilterNoPreviousMonthTransaction" id="targFilterNoPreviousMonthTransaction" />
                                    <label for="targFilterNoPreviousMonthTransaction">No Previous Month Transaction</label>
                                </div>
                                <div class="col-md-12">
                                    @if($user_type == 3)
                                        <input type="checkbox" name="targFilterBranchTransaction" id="targFilterBranchTransaction" checked disabled class="enableOption" />
                                    @else
                                        <input type="checkbox" name="targFilterBranchTransaction" id="targFilterBranchTransaction"  class="enableOption checkMe" />
                                    @endif
                                    <label disabled for="targFilterBranchTransaction">Branch Transaction</label>
                                    <div class="row">
                                        <div class="col-md-12 targFilterBranchTransaction">
                                            <div class="col-md-6">
                                                <label>
                                                    @if($user_type == 3)
                                                        {{Form::select('targFilterBranchTransaction0', $branchOptions, null, ['readonly' => ''])}}
                                                    @else
                                                        {{Form::select('targFilterBranchTransaction0', $branchOptions, null, ['placeholder' => 'choose...', 'disabled' => '', 'class' => 'dAble checkMe'])}}
                                                    @endif
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 margT20">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button class="btn btn-primary pull-right">ADD</button>
                        </div>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>
<div id="editTargetSeg" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Edit Segment Details</div>
            <div class="modal-body">
                <form method="POST" action="">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <h4>Segment Information</h4>
                            <hr>

                            <div class="row">
                                <div class="col-md-12">
                                    <input type="checkbox" name="esegActive" id="esegActive"><label for="esegActive">&nbsp; Active</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <label for="esegName">Segment Name</label>
                                    <input type="text" class="form-control" placeholder="Enter segment name" name="esegName" id="esegName"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 filterInfo">
                            <h4>Filter Information</h4>
                            <hr>

                            <div class="row">
                                <div class="col-md-12 etargFilterAll">
                                    <input type="checkbox" name="etargFilterGender" id="etargFilterGender" class="enableOption checkMe" />
                                    <label for="etargFilterGender">Gender</label>
                                    <div class="row">
                                        <div class="col-md-12 etargFilterGender">
                                            <div class="col-md-6">
                                                <label><input type="radio" name="etargFilterGenderO" id="etargFilterGenderA" disabled class="dAble checkMe" /> Both</label>
                                            </div>
                                            <div class="col-md-6">
                                                <label><input type="radio" name="etargFilterGenderO" id="etargFilterGenderB" disabled class="dAble" /> Female</label>
                                            </div>
                                            <div class="col-md-6">
                                                <label><input type="radio" name="etargFilterGenderO" id="etargFilterGenderC" disabled class="dAble" /> Male</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 etargFilterAll">
                                    <input type="checkbox" name="etargFilterAge" id="etargFilterAge" class="enableOption checkMe" />
                                    <label for="etargFilterAge">Age Group</label>
                                    <div class="row">
                                        <div class="col-md-12 etargFilterAge">
                                            <div class="col-md-6">
                                                <label><input type="checkbox" name="etargFilterAgeO" id="etargFilterAgeA" disabled class="dAble checkMe" /> 15-24 years</label>
                                            </div>
                                            <div class="col-md-6">
                                                <label><input type="checkbox" name="etargFilterAgeO" id="etargFilterAgeB" disabled class="dAble checkMe" /> 55-64 years</label>
                                            </div>
                                            <div class="col-md-6">
                                                <label><input type="checkbox" name="etargFilterAgeO" id="etargFilterAgeC" disabled class="dAble checkMe" /> 25-54 years</label>
                                            </div>
                                            <div class="col-md-6">
                                                <label><input type="checkbox" name="etargFilterAgeO" id="etargFilterAgeD" disabled class="dAble checkMe" /> 65 years and over</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 etargFilterAll">
                                    <input type="checkbox" name="etargFilterBirthday" id="etargFilterBirthday" class="enableOption checkMe" />
                                    <label for="etargFilterBirthday">Birthday</label>
                                    <div class="row">
                                        <div class="col-md-12 etargFilterBirthday">
                                            <div class="col-md-6">
                                                <label><input type="radio" name="etargFilterBirthdayO" id="etargFilterBirthdayA" disabled class="dAble checkMe" /> Today</label>
                                            </div>
                                            <div class="col-md-6">
                                                <label><input type="radio" name="etargFilterBirthdayO" id="etargFilterBirthdayB" disabled class="dAble" /> This Month</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 margT20">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button class="btn btn-primary pull-right">ADD</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<div id="deleteTargetSeg" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Delete Segment Details</div>
            <div class="modal-body">
                <p>Are you sure you want to delete this Segment Details?</>
                <span id="segmentToDelete"></span>
                <form method="POST" id="deleteSegmentForm" action="">
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button class="btn btn-primary pull-right">DELETE</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<!-- Modal: Email Blast -->
<div id="addEmail" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Add Email Broadcast</div>
            <div class="modal-body">
                <form method="POST" action="{{MigrationHelper::getAppBaseUrl()}}broadcast/email" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-6">
                            <h4>Information</h4>
                            <hr>

                            <div class="row">
                                <div class="col-md-12">
                                    <label for="emailPublishDate">Publish Date</label>
                                    <div class='input-group date publishDate datetimepicker' id='emailPublishDateStart'>
                                      <input type='text' class="form-control" name="emailPublishDate" id="emailPublishDate" placeholder="Select Date"  data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="{{ $durationStart }}" required>
                                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label for="emailPublishTime">Publish Time</label>
                                        <select required class="form-control" name="emailPublishTime" id="emailPublishTime">
                                            <option value="">Select Time</option>
                                            <option>12:00 AM</option>
                                            <option>01:00 AM</option>
                                            <option>02:00 AM</option>
                                            <option>03:00 AM</option>
                                            <option>04:00 AM</option>
                                            <option>05:00 AM</option>
                                            <option>06:00 AM</option>
                                            <option>07:00 AM</option>
                                            <option>08:00 AM</option>
                                            <option>09:00 AM</option>
                                            <option>10:00 AM</option>
                                            <option>11:00 AM</option>
                                            <option>12:00 PM</option>
                                            <option>01:00 PM</option>
                                            <option>02:00 PM</option>
                                            <option>03:00 PM</option>
                                            <option>04:00 PM</option>
                                            <option>05:00 PM</option>
                                            <option>06:00 PM</option>
                                            <option>07:00 PM</option>
                                            <option>08:00 PM</option>
                                            <option>09:00 PM</option>
                                            <option>10:00 PM</option>
                                            <option>11:00 PM</option>
                                        </select>
                                </div>
                                <div class="col-md-12">
                                    <label for="message">Message</label>
                                    <textarea required class="form-control messages" name="message" id="message" cols="30" rows="3" placeholder="Enter email message">{{ Input::old('message') }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 filterInfo">
                            <h4>Filter Information</h4>
                            <hr>

                            <div class="row">
                                <div class="col-md-12">
                                    <label for="targetSegment">Target Segment</label>
                                    @if($user_type == 3)
                                        <select class="form-control" name="targetSegment" id="targetSegment" style="margin-bottom: 20px!important;" required>
                                            @if (count($activeSegments) > 0)
                                                @foreach ($activeSegments as $activeSegment)
                                                    <option value="{{$activeSegment->id}}">{{$activeSegment->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    @else
                                        <select class="form-control" name="targetSegment" id="targetSegment" style="margin-bottom: 20px!important;">
                                            <option>All Segment</option>
                                            @if (count($activeSegments) > 0)
                                                @foreach ($activeSegments as $activeSegment)
                                                    <option value="{{$activeSegment->id}}">{{$activeSegment->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    @endif

                                </div>
                                <div class="col-md-12">
                                    <label for="subject">Subject</label>
                                    <input type="text" class="form-control" id="subject"  name="subject" placeholder="Enter Subject" style="margin-bottom: 20px!important;" required>
                                </div>
                                <div class="col-md-12">
                                    <label for="imgbanner">Image Banner</label>
                                    <div class="row">
                                        <div class="col-md-7">
                                            <img src="{{ url('page/cms/images/addImage2.jpg') }}" id="imgprev" style="width: 151px;">
                                        </div>
                                        <div class="col-md-5 design" style="padding: 20px 15px;">
                                            <span class="btn btn-default btn-file">
                                                Choose File
                                                <input type="file" accept="image/gif, image/jpeg, image/png" id="imgbanner" name="imgbanner" class="fuImage" ew="600" eh="250" required>
                                            </span>
                                            <div id="disp_tmp_path" style="display: none;"></div>
                                        </div>
                                        <div class="col-md-12">
                                            <span class="error-msg hide" style="font-size: 11px; padding: 5px 0;">Image does not meet the image size requirement. Please check again.</span>
                                        </div>
                                        <div class="col-md-12">
                                            <em style="font-size: 11px;">Required image size: 600px x 250px</em>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 row">
                            <div class="col-md-6" style="text-align: center;">
                                <a class="btn btn-default" id="previewmessage" onclick="myPreview()">Preview Message</a>
                            </div>
                        </div>
                        <div class="col-md-12 margT20">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button class="btn btn-primary pull-right">ADD</button>
                        </div>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>
<div id="deleteEmail" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Delete Email Broadcast</div>
            <div class="modal-body">
                <p>Are you sure you want to delete this Email Broadcast?</>
                <span id="emailToDelete"></span>
                {{ Form::open(array('id' => 'deleteEmailForm')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button class="btn btn-primary pull-right">DELETE</button>
                        </div>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

<div id="deleteTargetEvent" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Unpublish Event</div>
            <div class="modal-body">
                <p>Are you sure you want to unpublish this Event?</>
                <span id="eventToDelete"></span>
                {{ Form::open(array('id' => 'deleteEventForm')) }}
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button class="btn btn-primary pull-right">UNPUBLISH</button>
                        </div>
                    </div>
                {{ Form::close() }}
            </div>

        </div>
    </div>
</div>
<div id="publishTargetEvent" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Publish Event</div>
            <div class="modal-body">
                <p>Are you sure you want to publish this Event?</>
                <span id="eventToPublish"></span>
                {{ Form::open(array('id' => 'publishEventForm')) }}
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button class="btn btn-primary pull-right">PUBLISH</button>
                        </div>
                    </div>
                {{ Form::close() }}
            </div>

        </div>
    </div>
</div>

<div id="addEvent" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Add Event</div>
            <div class="modal-body">
                {{ Form::open(array('id' => 'addEventForm', 'files' => true)) }}
                {{ Form::hidden('_method', 'POST') }}
                    <div class="form-group row">
                        <div class="col-md-12">
                            <h4>Information</h4>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="aEventTitle">Event Title</label>
                                    <input type="text" class="form-control" placeholder="Enter event title" name="aEventTitle" id="aEventTitle" required/>
                                </div>
                                <div class="col-md-12">
                                    <label for="aEventDetails">Event Details</label>
                                    <textarea class="form-control" placeholder="" name="aEventDetails" id="aEventDetails" required></textarea>
                                </div>
                                <div class="col-md-6">
                                    <label for="aEventImg">Event Image</label>
                                    <div class="row">
                                        <div class="col-md-7">
                                            <img src="{{ url('page/cms/images/addImage2.jpg') }}" id="aEventImg_imgprev" style="width: 151px;">
                                        </div>
                                        <div class="col-md-5 design" style="padding: 20px 15px;">
                                            <span class="btn btn-default btn-file">
                                                Choose File
                                                <input type="file" accept="image/gif, image/jpeg, image/png" id="aEventImg" name="aEventImg" class="fuImage" ew="720" eh="720" required>
                                            </span>
                                            <div id="aEventImg_disp_tmp_path" style="display: none;"></div>
                                        </div>
                                        <div class="col-md-12">
                                            <span class="error-msg hide" style="font-size: 11px; padding: 5px 0;">Image does not meet the image size requirement. Please check again.</span>
                                        </div>
                                        <div class="col-md-12">
                                            <em style="font-size: 11px;">Required image size: 720px x 720px</em>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="aEventSequence">Sequence</label>
                                    {{ Form::select('aEventSequence', $event_sequence_options_add, null, ['class' => 'form-control', 'id' => 'aEventSequence']) }}
                                </div>
                                <div class="col-md-6">
                                    <label for="aEventCategory">Category</label>
                                    <select name="aEventCategory" id="aEventCategory">
                                        @foreach( $event_categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        @if($enable_post_sub_category != '0')
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-offset-6 col-md-6">
                                    <label for="aEventSubCategory">Sub-category</label>
                                    <select name="aEventSubCategory" id="aEventSubCategory">
                                        <option value="">Select Sub-Category</option>
                                        <option value="Travel Junkie">Travel Junkie</option>
                                        <option value="Health Buff">Health Buff</option>
                                        <option value="Foodie">Foodie</option>
                                        <option value="Style Star">Style Star</option>
                                        <option value="Techie">Techie</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="aEventColor1">Event Color 1</label>
                                    <input type="text" class="form-control" name="aEventColor1" id="aEventColor1" value="#FFFFFF"/>
                                </div>
                                <div class="col-md-6">
                                    <label for="aEventColor2">Event Color 2</label>
                                    <input type="text" class="form-control" name="aEventColor2" id="aEventColor2" value="#FFFFFF"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="aEventUrl">URL</label>
                                    <input type="text" class="form-control" name="aEventUrl" id="aEventUrl" />
                                </div>
                                <div class="col-md-6">
                                    <label for="aEventType">Type</label>
                                    <div>
                                        <select name="aEventType">
                                            <option value="posts" selected="selected">Text</option>
                                            <option value="video">Video</option>
                                            <option value="spotify">Spotify</option>
                                            <option value="vote">Vote</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 margT20">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button class="btn btn-primary pull-right">ADD</button>
                        </div>
                    </div>
                {{ Form::close() }}
            </div>

        </div>
    </div>
</div>


<div id="editEvent" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Edit Event</div>
            <div class="modal-body">
                {{ Form::open(array('id' => 'editEventForm', 'files' => true)) }}
                {{ Form::hidden('_method', 'POST') }}
                    <div class="form-group row">
                        <div class="col-md-12">
                            <h4>Information</h4>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="eEventTitle">Event Title</label>
                                    <input type="text" class="form-control" placeholder="Enter event title" name="eEventTitle" id="eEventTitle" required/>
                                </div>
                                <div class="col-md-12">
                                    <label for="eEventDetails">Event Details</label>
                                    <textarea class="form-control" placeholder="" name="eEventDetails" id="eEventDetails" required></textarea>
                                </div>
                                <div class="col-md-6">
                                    <label for="eEventImg">Event Image</label>
                                    <div class="row">
                                        <div class="col-md-7">
                                            <img src="{{ url('page/cms/images/addImage2.jpg') }}" id="eEventImg_imgprev" style="width: 151px;">
                                        </div>
                                        <div class="col-md-5 design" style="padding: 20px 15px;">
                                            <span class="btn btn-default btn-file">
                                                Choose File
                                                <input type="file" accept="image/gif, image/jpeg, image/png" id="eEventImg" name="eEventImg" class="fuImage" ew="720" eh="720">
                                            </span>
                                            <div id="eEventImg_disp_tmp_path" style="display: none;"></div>
                                        </div>
                                        <div class="col-md-12">
                                            <span class="error-msg hide" style="font-size: 11px; padding: 5px 0;">Image does not meet the image size requirement. Please check again.</span>
                                        </div>
                                        <div class="col-md-12">
                                            <em style="font-size: 11px;">Required image size: 720px x 720px</em>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="eEventSequence">Sequence</label>
                                    {{Form::select('eEventSequence', $event_sequence_options_edit, null, ['class' => 'form-control', 'id' => 'eEventSequence'])}}
                                </div>
                                <div class="col-md-6">
                                    <label for="eEventCategory">Category</label>
                                    <select name="eEventCategory" id="eEventCategory">
                                        @foreach( $event_categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        @if($enable_post_sub_category != '0')
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-offset-6 col-md-6">
                                    <label for="eEventSubCategory">Sub-category</label>
                                    <select name="eEventSubCategory" id="eEventSubCategory">
                                        <option value="">Select Sub-Category</option>
                                        <option value="Travel Junkie">Travel Junkie</option>
                                        <option value="Health Buff">Health Buff</option>
                                        <option value="Foodie">Foodie</option>
                                        <option value="Style Star">Style Star</option>
                                        <option value="Techie">Techie</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="eEventColor1">Event Color 1</label>
                                    <input type="text" class="form-control" name="eEventColor1" id="eEventColor1" />
                                </div>
                                <div class="col-md-6">
                                    <label for="eEventColor2">Event Color 2</label>
                                    <input type="text" class="form-control" name="eEventColor2" id="eEventColor2" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="eEventUrl">URL</label>
                                    <input type="text" class="form-control" name="eEventUrl" id="eEventUrl" />
                                </div>
                                <div class="col-md-6">
                                    <label for="aEventType">Type</label>
                                    <div>
                                        <select name="aEventType" id="aEventType">
                                            <option value="posts" selected="selected">Text</option>
                                            <option value="video">Video</option>
                                            <option value="spotify">Spotify</option>
                                            <option value="vote">Vote</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 margT20">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button class="btn btn-primary pull-right">EDIT</button>
                        </div>
                    </div>
                {{ Form::close() }}
            </div>

        </div>
    </div>
</div>
@endsection

@section('footerPlaceholder')
@parent
<script type="text/javascript" src="{{ url('page/cms/js/main.js') }}"></script>
<script type="text/javascript">
    $(".enableOption").click(function(){
        if ($('#' + this.id).prop('checked')) {
            $("." + this.id + " .dAble").attr('disabled', false);

            if($("#notifSchedRun").prop('checked') || $("#enotifSchedRun").prop('checked')) {
                $("." + this.id + " .input-group-addon.out").removeClass('out');
            }
        }
        else {
            $("." + this.id + " .dAble").attr('disabled', true);
            $("." + this.id + " .input-group-addon").addClass('out');
        }
    });

    /* Preview Email Broadcast */
    $('#imgbanner').on('showPreview', function(event) {
        var tmppath = URL.createObjectURL(event.target.files[0]);
        if ($(this).attr('validationStatus') != 'invalid') {
            $('#imgprev').fadeIn('fast').attr('src',tmppath);
            $('#disp_tmp_path').html(tmppath);
        } else {
            $('#imgprev').fadeIn('fast').attr('src', '{{ url('page/cms/images/addImage2.jpg') }}');
        }
    });

    var isExceeded = false;
	$('.withCounter').on('keyup', function(){
		$el = $(this);
		$el.parent().find('.textCounter').html($el.val().length);
        $el.parent().find('.messagePart').html(" " + (Math.trunc(($el.val().length -1)/160) +1));

        if(!isExceeded && $el.val().length > 160){
            $('#messageExceed').show("slow");
            isExceeded = true;
        }else if($el.val().length <= 160){
            isExceeded = false;
            $('#messageExceed').hide("slow");
        }

	});

    function myPreview() {
        var wihe = 'width='+screen.availWidth+',height='+screen.availHeight;
        var subject = document.getElementById('subject').value;
        var message = $('.messages').val();
        var imgbanner = $('#disp_tmp_path').html();
        var username = $('#usrname').html();

        window.open('preview/email?subject=' + subject + '?message=' + message + '?banner=' + imgbanner + '?name=' + username,'','screenX=1,screenY=1,left=1,top=1,' + wihe);
    }

    /* Preview Event Broadcast */
    $('#aEventImg').on('showPreview', function(event) {
        var tmppath = URL.createObjectURL(event.target.files[0]);
        if ($(this).attr('validationStatus') != 'invalid') {
            $('#aEventImg_imgprev').fadeIn('fast').attr('src',tmppath);
            $('#aEventImg_disp_tmp_path').html(tmppath);
        } else {
            $('#aEventImg_imgprev').fadeIn('fast').attr('src', '{{ url('page/cms/images/addImage2.jpg') }}');
        }
    });

    $('#eEventImg').on('showPreview', function(event) {
        var tmppath = URL.createObjectURL(event.target.files[0]);
        if ($(this).attr('validationStatus') != 'invalid') {
            $('#eEventImg_imgprev').fadeIn('fast').attr('src',tmppath);
            $('#eEventImg_disp_tmp_path').html(tmppath);
        } else {
            $('#eEventImg_imgprev').fadeIn('fast').attr('src', '{{ url('page/cms/images/addImage2.jpg') }}');
        }
    });

    $("#aEventColor1, #aEventColor2").spectrum({
            preferredFormat: "hex",
            showInitial: true,
            showInput: true,
            showButtons: true,
            chooseText: "Select",
            cancelText: "Cancel",
            appendTo: "#addEvent"
        });

    $('#eventTable').on('click', '.eventEdit', function() {
        var $row = $(this).closest('tr');
        var $cell = $row.find('td');
        $("#editEventForm").attr('action', $(this).data('action'));
        $("#eEventTitle").val($cell.eq(1).text());
        @if($enable_post_sub_category != '0')
        $("#eEventSequence").val($cell.eq(4).text());
        @else
        $("#eEventSequence").val($cell.eq(3).text());
        @endif
        $("#eEventDetails").val( $(this).data('details') );
        $("#eEventImg_imgprev").attr( 'src', $(this).data('img') );
        $("#eEventColor1").val( $(this).data('color1') );
        $("#eEventColor2").val( $(this).data('color2') );
        $("#eEventUrl").val( $(this).data('url') );
        $("#eEventCategory").val( $(this).data('category') );
        $("#aEventType").val( $(this).data('type') );
        $("#eEventSubCategory").val( $(this).data('sub-category') );
        console.log($(this).data());
        $("#eEventColor1, #eEventColor2").spectrum({
            preferredFormat: "hex",
            showInitial: true,
            showInput: true,
            showButtons: true,
            chooseText: "Select",
            cancelText: "Cancel",
            appendTo: "#editEvent"
        });
    });

    $('#eventTable').on('click', '.eventDelete', function() {
        var $row = $(this).closest('tr');
        var $cell = $row.find('td');
        $("#eventToDelete").text($cell.eq(0).text());
        $("#deleteEventForm").attr('action', $(this).data('action'));
    });

    $('#eventTable').on('click', '.eventPublish', function() {
        var $row = $(this).closest('tr');
        var $cell = $row.find('td');
        $("#eventToPublish").text($cell.eq(0).text());
        $("#publishEventForm").attr('action', $(this).data('action'));
    });

    // $('#editEvent').on('submit', '#editEventForm', function() {
    //     alert('1');
    // });

    $( "#editEventForm" ).validate({
      rules: {
        eEventUrl: {
          url: true
        }
      }
    });

    $( "#addEventForm" ).validate({
      rules: {
        aEventUrl: {
          url: true
        }
      }
    });

</script>
@endsection
