@extends('layouts.app')

@section('view')
<div class="container">
    @include('flash.message')
    <div id="content" class="promos spacing settings">
        <div class="noTab">
            <div class="row">
                {{ Form::open(array('url' => 'broadcast/pushCredential', 'method' => 'post', 'files' => true)) }}
                {{ csrf_field() }}
                <div class="col-md-6">
                    <h4>Android Credential</h4>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="pointsName">android_key</label>
                            {{ Form::text('android_key',  $pushCredential->android_key, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            {{ Form::submit('update') }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <h4>Ios Credential</h4>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="programName">ios push certificate</label>
                            {{ Form::text('ios_cert',  $pushCredential->ios_cert, ['class' => 'form-control', 'disabled' => 'disabled']) }}
                            {{ Form::file('certificate','',array('id'=>'','class'=>'')) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="programName">ios secret</label>
                            {{ Form::text('ios_secret',  $pushCredential->ios_secret, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="pointsName">ios bundle</label>
                            {{ Form::text('ios_bundle',  $pushCredential->ios_bundle, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            {{ Form::submit('update') }}
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
            <div class="row">
                <div class="col-md-6">
                    {{ Form::open(array('url' => 'broadcast/pushCredential/pushAndroidTry')) }}
                    <h4>Try Push Android</h4>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="pointsName">devices</label>
                            {{ Form::textarea(
                                'android_devices',
                                null ,
                                [   'rows' => '5',
                                    'placeholder' => '[{"device_id":"DEVICEA_DEVICE_ID","token":"DEVICEA_TOKEN"},{"device_id":"DEVICEB_DEVICE_ID","token":"DEVICEB_TOKEN"}]'
                            ]) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="pointsName">message</label>
                            {{ Form::textarea(
                                'message',
                                date('Y-m-d H:i:s', time()),
                                [   'rows' => '5',
                                    'placeholder' => 'enter push message'
                            ]) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            {{ Form::submit('send') }}
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
                <div class="col-md-6">
                    {{ Form::open(array('url' => 'broadcast/pushCredential/pushIosTry')) }}
                    <h4>Try Push Ios</h4>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="programName">devices</label>
                            {{ Form::textarea(
                                'ios_devices',
                                null ,
                                [   'rows' => '5',
                                    'placeholder' => '[{"device_id":"DEVICEA_DEVICE_ID","token":"DEVICEA_TOKEN"},{"device_id":"DEVICEB_DEVICE_ID","token":"DEVICEB_TOKEN"}]'
                            ]) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="pointsName">message</label>
                            {{ Form::textarea(
                                'message',
                                date('Y-m-d H:i:s', time()),
                                [   'rows' => '5',
                                    'placeholder' => 'enter push message'
                            ]) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            {{ Form::submit('send') }}
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div> <!-- /container -->
@endsection

@section('footerPlaceholder')
@parent
@endsection