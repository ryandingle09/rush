<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>RUSH</title>


    <meta property="og:title" content="RUSH - Loyalty Made Easy">
    <meta property="og:image" content="http://rush.ph/app/assets/images/rush-image1.jpg">
    <meta property="og:image" content="http://rush.ph/app/assets/images/rush-logo.jpg">
    <meta property="og:description" content="Conveniently develop and deploy a custom-branded mobile app in just three days. Easily build, track, and manage your own customer loyalty program with RUSH. Sign up and get a 30-day free trial.">
    <meta property="og:type" content="website" />
    <meta property="og:image:type" content="image/jpeg" />
    
    <!-- Bootstrap -->
    <link href="{{URL::to('/')}}/app/page/onboarding/css/bootstrap.css" rel="stylesheet">
    <link href="{{URL::to('/')}}/app/page/onboarding/css/animate.css" rel="stylesheet">
    <link href="{{URL::to('/')}}/app/page/onboarding/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,500,700" rel="stylesheet">
    <link rel="icon" href="{{URL::to('/')}}/app/page/onboarding/images/favicon.png" type="image/png">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
    @if(getenv('APP_ENV') == 'production')
    <script>
       (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
       (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
       m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
       })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

       ga('create', 'UA-87488508-1', 'auto');
       ga('send', 'pageview');
    </script>
    @endif
</head>

<body>
    <div class="loading"></div>
    <!-- Sign In -->
    <div id="signin" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal"><img src="{{URL::to('/')}}/app/page/onboarding/images/close-button.png"></div>
                    <h4 class="modal-title"><img src="{{URL::to('/')}}/app/page/onboarding/images/rush-logo.svg" width="170"></h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('login') }}" method="post" id="form_signin">
                        <input id="form_signin_username" type="text" class="df-field" placeholder="Email" name="email" required>
                        <input id="form_signin_password" type="password" class="df-field" placeholder="Password" name="password" required>
                        <div class="error-label hide" id="loginmessage"></div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button id="form_signin_button" type="submit" class="bt-bg continue">Continue</button>
                    <div class="forgot"><a id="forgot_password_button" style="cursor: pointer;">Forgot your password?</a></div>
                </div>
            </div>
        </div>
    </div>
    
      <!-- Request Demo Confirmation  -->
    <div class="modal fade" id="success" tabindex="-1" role="dialog" aria-labelledby="successLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <img src="{{URL::to('/')}}/app/page/onboarding/images/success-icon.svg" alt="" class="success-img">
            <h3><b>Thank you for your<br> interest in RUSH!</b></h3>
            <small>A member of our team will get in touch with you within 1 business day.</small>
            <br>
            <br>
            <a href="#" data-dismiss="modal">DONE</a>
        </div>
    </div>
</div>

    <!-- Request Demo Confirmation -->
    <div id="rqdemo-confirm" class="modal fade" role="dialog" tabindex="-1">
        <div class="modal-dialog su-success">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header aCenter">
                    <div type="button" class="close" data-dismiss="modal"><img src="{{URL::to('/')}}/app/page/onboarding/images/close-button.png"></div> 
                    <img src="{{URL::to('/')}}/app/page/onboarding/images/success-icon.svg" class="img-responsive">
                </div>
                <div class="modal-body">
                    <p>Thank you for your interest in RUSH!</p>
                    <p>A member of our team will get in touch with you within 1 business day.</p>
                </div>
                <div class="modal-footer">
                    &nbsp;
                </div>
            </div>
        </div>
    </div>

    <section>
        <nav class="navbar navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#defaultNavbar1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar top-bar"></span>
                        <span class="icon-bar middle-bar"></span>
                        <span class="icon-bar bottom-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ url('/') }}"><img src="{{URL::to('/')}}/app/page/onboarding/images/rush-logo.svg" width="100"></a>
                </div>
                <div class="collapse navbar-collapse" id="defaultNavbar1">
                    <div class="full-mobile">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="{{ url('about') }}">About Us</a></li>
                            <li><a href="{{ url('newsroom') }}">Newsroom</a></li>
                            <li><a href="{{ url('how-it-works') }}">How It Works</a></li>
                            <li><a href="{{ url('products') }}">Products</a></li>
                            <li><a href="{{ url('request-demo#request-demo-form') }}">Request Demo</a></li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <button type="button" class="no-bg-br sign-in" data-toggle="modal" data-target="#signin">Sign In</button>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <div class="full rqdemo wow fadeIn" data-wow-duration="1s" data-wow-delay=".4s">
            <div class="container">
                <div class="col-md-5 content">
                    <h2 class="wow fadeIn" data-wow-duration="1s" data-wow-delay=".6s">Loyalty made easy.</h2>
                    <span class="wow fadeIn" data-wow-duration="1s" data-wow-delay=".8s">
Build &amp; foster long-lasting customer relationships with a
RUSH-powered loyalty program.<br><br>
Create, design and manage your own customer loyalty
program easily with RUSH. Conveniently develop and
deploy a custom-branded mobile app in as short as 3 days.
</span>
                </div>
                <div class="col-md-7 paddingt30 wow fadeIn" data-wow-duration="1s" data-wow-delay="1s"><img src="{{URL::to('/')}}/app/page/onboarding/images/multi-platform-small.png" class="img-responsive"></div>
            </div>
        </div>
    </section>
    <section>
        <div class="container rq-lowerpart wow fadeIn" data-wow-duration="1s" data-wow-delay="1.2s">
            <div class="col-md-6 col-sm-6 col-xs-12 opt-desc">
                <h2 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="1.4s">POINTS</h2>
                <span>
                White label points-based loyalty program<?php /* <br>Ideal for medium to large enterprises */ ?>
                </span>
                <br><br>
               <div><b class="title">Issue Points</b></div>
Customers can earn points with every purchase and can redeem a reward once they reach a set number of points.
<br><br>
<i>*Points are issued for every transaction, purchase or pre-determined action.</i>
<br><br>
<img src="{{URL::to('/')}}/app/page/onboarding/images/pro-tablet.png" width="250">
<br>
 <h2 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="1.4s">FEATURES</h2>
 <br>
<div><b class="title">Channels</b></div>
In-store POS widget<Br>
In-store Android Tablet App<br>
Custom-branded Mobile App<bR>
Web portal<br>
SMS<br>
Physical Loyalty Card<br><br>
<div><b class="title">Management</b></div>
Real-Time Online Dashboard<br>
Member Database Management<br>
Reports and Analytics<br>
Broadcast Tools<br>
SMS Credits<br>
App Zero-Rating<br>
 <br>
<div><b class="title">Marketing Support</b></div>
Website Feature<br>
Social Media Promotion<br><br><br>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 aCenter wow fadeIn" data-wow-duration="1s" data-wow-delay="1.2s">
                <form id="request-demo-form" action="{{ Request::url() }}" method="post" class="{{ $form }}">
                    <h2><b>Request Demo</b></h2>
                    <div class="col-md-12 paddingb10 group-field">
                        <label>Contact Information</label>
                    </div>
                    <div class="col-md-12 paddingb10 group-field">
                        <div class="col-md-6 group-field">
                            <label>First Name*</label>
                            <input type="text" class="df-field" name="fname" required>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-5 group-field">
                            <label>Last Name*</label>
                            <input type="text" class="df-field" name="lname" required>
                        </div>
                        @if( $errors->has('name'))<div class="error-label">{{ $errors->first('fname') }}</div> @endif
                        @if( $errors->has('name'))<div class="error-label">{{ $errors->first('lname') }}</div> @endif
                    </div>
                    <div class="col-md-12 paddingb10 group-field">
                        <label>Mobile*</label>
                        <div class="input-group">
                            <span class="input-group-addon">+63</span>
                            <input type="text" class="df-field mobile" name="mobile" required maxlength="10" placeholder="9xxxxxxxxx">
                        </div>
                        @if( $errors->has('mobile'))<div class="error-label">{{ $errors->first('mobile') }}</div> @endif
                    </div>
                    <div class="col-md-12 paddingb10 group-field">
                        <label>Email*</label>
                        <input type="text" class="df-field" name="email" required>
                        @if( $errors->has('email'))<div class="error-label">{{ $errors->first('email') }}</div> @endif
                    </div>
                    <div class="col-md-12 paddingb10 group-field">
                        <label>Company Name*</label>
                        <input type="text" class="df-field" name="company" required>
                        @if( $errors->has('company'))<div class="error-label">{{ $errors->first('company') }}</div> @endif
                    </div>
                    <div class="col-md-12 paddingb10 group-field ">
                        <label>Industry*</label>
                        <select class="df-field" name="industry[]" required>
                            <option selected="selected" value="">Select Industry</option>
                            <option data-option-array-index="0">Automotive Dealers and Manufacturers</option>
                            <option data-option-array-index="1">Automotive Services</option>
                            <option data-option-array-index="2">Entertainment</option>
                            <option data-option-array-index="3">Fitness</option>
                            <option data-option-array-index="4">Food and Beverage Establishment</option>
                            <option data-option-array-index="5">Fuel and Petroleum</option>
                            <option data-option-array-index="6">Grocery and Supermarket</option>
                            <option data-option-array-index="7">Grooming and Wellness</option>
                            <option data-option-array-index="8">Hospitals and Healthcare Services</option>
                            <option data-option-array-index="9">Insurance</option>
                            <option data-option-array-index="10">Petroleum</option>
                            <option data-option-array-index="11">Retail - Apparel</option>
                            <option data-option-array-index="12">Retail - Electronics</option>
                            <option data-option-array-index="13">Retail - Household and Hardware</option>
                            <option data-option-array-index="14">Retail - Sporting Goods</option>
                            <option data-option-array-index="15">Telecommunication and Utilities</option>
                            <option data-option-array-index="16">Transportation</option>
                            <option data-option-array-index="17">Travel and Leisure</option>
                            <option data-option-array-index="18">Others</option>
                        </select>
                        @if( $errors->has('industry'))<div class="error-label">{{ $errors->first('industry') }}</div> @endif
                    </div>
                    <div class="col-md-12 paddingb20 group-field multiselect">
                        <label>Preferred product(Optional)</label>
                        <div class="col-md-12 nopadding">
                            <div class="col-md-4 nopadding">
                                <input type="checkbox" id="pro" name="product[]" value="Points" checked disabled>
                                <label for="pro"> Points</label></div>
                                <input type="hidden" name="product[]" value="Points">
                        </div>
                    </div>
                    <div class="col-md-12 paddingb20 group-field">
                        <label>Objective of Loyalty Program (Optional)</label>
                        <div class="multiselect">
                            <div class="selectBox">
                                <select class="df-field" name="objective[]">
                                    <option selected="selected">Select Objective</option>
                                </select>
                                <div class="overSelect"></div>
                            </div>
                            <div id="checkboxes">
                                <div class="col-md-12">
                                    <input type="checkbox" id="one" name="program[]" value="Enhance customer engagement" />
                                    <label for="one">Enhance customer engagement</label>
                                </div>
                                <div class="col-md-12">
                                    <input type="checkbox" id="two" name="program[]" value="Improve customer retention" />
                                    <label for="two">Improve customer retention</label>
                                </div>
                                <div class="col-md-12">
                                    <input type="checkbox" id="three" name="program[]" value="Increase frequency of customer visits"/>
                                    <label for="three">Increase frequency of customer visits</label>
                                    <input type="checkbox" id="four" name="program[]" value="Drive revenue per transaction" />
                                    <label for="four">Drive revenue per transaction</label>
                                    <input type="checkbox" id="five" name="program[]" value="Gather insights and understand customer behavior"/>
                                    <label for="five">Gather insights and understand customer behavior</label>
                                    <input type="checkbox" id="six" name="program[]" value="Promote and push new products and services"/>
                                    <label for="six">Promote and push new products and services</label>
                                    <input type="checkbox" id="seven" name="program[]" value="Others"/>
                                    <label for="seven">Others</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 paddingb20 group-field">
                        <textarea class="form-control" placeholder="Tell us more about your business (optional)" rows="3" name="details"></textarea>
                    </div>
                    <div class="col-md-12 paddingb10 group-field">
                        <div class="g-recaptcha" data-sitekey="6LeSJSkTAAAAAJqYJJczKaKyhU6G_0DXkRsZM_1-"></div>
                        <small class="text-danger hidden hidden-text">Please verify captcha</small>
                        @if( $errors->has('g-recaptcha-response'))<div class="error-label">{{ $errors->first('g-recaptcha-response') }}</div> @endif
                    </div>
                    <div class="col-md-12">
                        <br>
                        <input type="submit" name="submit" class="bt-bg continue" value="Submit" />
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </section>
    <section class="footer-sec">
        <div class="footer wow fadeIn" data-wow-duration="1s" data-wow-delay=".2s">
            <div class="container">
                <div class="col-md-8">
                    <ul class="f-links col-md-6">
                        <li><a href="{{ url('terms-of-use') }}">Terms of use</a></li>
                        <li><a href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
                        <li><a href="{{ url('faqs') }}">FAQS</a></li>
                    </ul>
                    <ul class="s-icons col-md-6">
                        <li>
                            <a href="https://www.facebook.com/rushloyaltyh" target="_blank"><img src="{{URL::to('/')}}/app/page/onboarding/images/facebook-icon-footer.svg"></a>
                        </li>
                        <li>
                            <a href="https://twitter.com/rushrewardsph" target="_blank"><img src="{{URL::to('/')}}/app/page/onboarding/images/twitter-icon-footer.svg"></a>
                        </li>
                        <li>
                            <a href="https://www.linkedin.com/company/rushloyalty/" target="_blank"><img src="app/page/onboarding/images/linkedin-logo.png"></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 copyright">Copyright © RUSH 2017. All Rights Reserved</div>
            </div>
        </div>
    </section>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{URL::to('/')}}/app/page/onboarding/js/jquery-1.11.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{URL::to('/')}}/app/page/onboarding/js/bootstrap.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="{{URL::to('/')}}/app/page/onboarding/js/wow.min.js"></script>
    <script>    
        $(window).load(function() {
            setTimeout(function(){
                $('.loading').remove();
            }, 10)
        });
        new WOW().init();
    </script>
    <script type="text/javascript">
    $(document).ready(function() {

        var checkboxes = $('#checkboxes');
        var checkoption = $("#checkboxes input[type='checkbox']");
        var count = 0;

        // Uncheck when Reload
        $('#checkboxes input[type="checkbox"]:checked').prop('checked', false);


        // Hide Dropdown
        checkboxes.slideUp();


        // Open Dropdown
        $('.overSelect').click(function() {

            checkboxes.slideToggle('fast');
        });

        // Count Objective
        $(checkoption).click(function() {



            count = 0;

            $(this).toggleClass('selected')

            checkoption.each(function() {

                if ($(this).hasClass('selected')) {

                    count++;

                }
                $('.selectBox option:selected').text("Selected " + count);


            });


            if (!checkoption.is(":checked")) {
                $('.selectBox option:selected').text("Select Objective");
            }


        });

    });
    </script>
    <script type="text/javascript">
        $(document).ready( function() {

            $('#form_signin_button').on('click', function() {
                $.ajax({
                  type: "POST",
                  url: '{{ url('prelogin') }}',
                  dataType: "json",
                  data: $('#form_signin').serialize()
                }).done( function( response ) {
                    if ( response.login_status == "success" ) {
                        $('#form_signin').submit();
                    } else if( response.login_status == 'failed' ) {
                        $("#loginmessage").removeClass('hide').html( response.error_message )
                    } else {
                        $("#loginmessage").removeClass('hide').html( 'An error has occured. Please try again.' )
                    }
                });
                return false;  
            });

            $('#forgot_password_button').on('click', function() {
                if ( $('#form_signin_username').val() ) {
                    $.ajax({
                      type: "POST",
                      url: '{{ url('preforgot') }}',
                      dataType: "json",
                      data: $('#form_signin').serialize()
                    }).done( function( response ) {
                        if ( response.status == "success" ) {
                            $('<form method="post" id="forgot_pass_form" action="{{ url('forgot-password') }}"></form>').appendTo('body')
                            $('<input>').attr({
                                type: 'hidden',
                                name: 'email',
                                value: $('#form_signin_username').val()
                            }).appendTo('#forgot_pass_form');
                            $('<input>').attr({
                                type: 'hidden',
                                name: 'type',
                                value: response.type
                            }).appendTo('#forgot_pass_form');
                            $('#forgot_pass_form').submit();
                        } else if( response.status == 'failed' ) {
                            $("#loginmessage").removeClass('hide').html( response.message )
                        } else {
                            $("#loginmessage").removeClass('hide').html( 'An error has occured. Please try again.' )
                        }
                    });
                    return false;
                } else {
                    $("#loginmessage").removeClass('hide').html( 'Email is required.' );
                }
            });

            $('#form_signup_button').on('click', function() {
                $('.error-label').remove();
                $.ajax({
                  type: "POST",
                  url: '{{ url('presignup') }}',
                  dataType: "json",
                  data: $('#form_signup').serialize()
                }).done( function( response ) {
                    if ( response.signup_status == "success" ) {
                        $('#form_signup').submit();
                    } else if( response.signup_status == 'failed' ) {
                        $.each( response.errors , function( key, value ) {
                            $('input[name='+ key +'], select[name='+ key +']').parent().append('<div class="error-label">'+value+'</div>');
                        });
                        grecaptcha.reset();
                    } else {
                        $('<div class="error-label">An error has occured. Please try again.</div>').appendTo('#form_signup')
                    }
                });
                return false;  
            });

            $('#request-demo-form').on('keyup', 'input[name="mobile"]', function () { 
                this.value = this.value.replace(/[^0-9\.]/g,'');
            });

            $('#form_signin').on('keypress', '#form_signin_password', function (e) {
              if (e.which == 13) {
                $("#form_signin_button").click();
                return false;
              }
            });

            $('#checkboxes').click(function(e) {
                e.stopPropagation();
            });

            $('.overSelect').on('click', function(e){
                e.stopPropagation();
            });

            $(document.body).on('click', function(){
                $('#checkboxes').hide();
            });

            <?php if ( $success ): ?>
                $('#success').modal('show');
            <?php endif; ?>
        });
    </script>
</body>

</html>
