<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>RUSH</title>
    <link rel="stylesheet" href="app/page/onboarding/scrollify/main.css">
    <!-- Bootstrap -->
    <link href="app/page/onboarding/css/bootstrap.css" rel="stylesheet">
    <link href="app/page/onboarding/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,500,700" rel="stylesheet">
    <link rel="icon" href="app/page/onboarding/images/favicon.png" type="image/png">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
    @if(getenv('APP_ENV') == 'production')
    <script>
       (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
       (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
       m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
       })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

       ga('create', 'UA-87488508-1', 'auto');
       ga('send', 'pageview');
    </script>
    @endif
</head>

<body>
    <!-- Sign In -->
    <div id="signin" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal"><img src="app/page/onboarding/images/close-button.png"></div>
                    <h4 class="modal-title"><img src="app/page/onboarding/images/rush-logo.svg" width="170"></h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('login') }}" method="post" id="form_signin">
                        <input id="form_signin_username" type="text" class="df-field" placeholder="Email" name="email" required>
                        <input id="form_signin_password" type="password" class="df-field" placeholder="Password" name="password" required>
                        <div class="error-label hide" id="loginmessage"></div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button id="form_signin_button" type="submit" class="bt-bg continue">Continue</button>
                    <div class="forgot"><a id="forgot_password_button" style="cursor: pointer;">Forgot your password?</a></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Sign Up-->
    <div id="signup" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal"><img src="app/page/onboarding/images/close-button.png"></div>
                    <h4 class="modal-title">Sign Up</h4>
                </div>
                <div class="modal-body">
                    <form id="form_signup" action="{{ url('select-package') }}" method="post" data-toggle="validator">
                        <div class="col-md-6 nopadding">
                            <div class="col-md-12 paddingb10 group-field">
                                <label>Name</label>
                                <input type="text" class="df-field" name="person_name" required>
                                {{-- @if( $errors && $errors->has('person_name')) <div class="error-label">{{ $errors->first('person_name') }}</div> @endif --}}
                            </div>
                            <div class="col-md-12 paddingb10 group-field">
                                <label>Designation/Position</label>
                                <input type="text" class="df-field" name="person_position" required>
                                @if( $errors && $errors->has('person_position')) <div class="error-label">{{ $errors->first('person_position') }}</div> @endif
                            </div>
                            <div class="col-md-12 paddingb10 group-field">
                                <label>Mobile No.</label>
                                <input type="text" class="df-field" name="person_contact" required maxlength="11">
                                @if( $errors && $errors->has('person_contact')) <div class="error-label">{{ $errors->first('person_contact') }}</div> @endif
                            </div>
                            <div class="col-md-12 paddingb10 group-field">
                                <label>Email</label>
                                <input type="text" class="df-field" name="person_email" required>
                                @if( $errors && $errors->has('person_email')) <div class="error-label">{{ $errors->first('person_email') }}</div> @endif
                            </div>
                            <div class="col-md-12 paddingb10 group-field">
                                <label>Location</label>
                                <select name="business_location" class="df-field" required>
                                  <option value=''>Select location</option>
                                    <option value='Alaminos'>Alaminos</option>
                                    <option value='Angeles'>Angeles</option>
                                    <option value='Antipolo'>Antipolo</option>
                                    <option value='Bacolod'>Bacolod</option>
                                    <option value='Bacoor'>Bacoor</option>
                                    <option value='Bago'>Bago</option>
                                    <option value='Baguio'>Baguio</option>
                                    <option value='Bais'>Bais</option>
                                    <option value='Balanga'>Balanga</option>
                                    <option value='Batac'>Batac</option>
                                    <option value='Batangas City'>Batangas City</option>
                                    <option value='Bayawan'>Bayawan</option>
                                    <option value='Baybay'>Baybay</option>
                                    <option value='Bayugan'>Bayugan</option>
                                    <option value='Biñan'>Biñan</option>
                                    <option value='Bislig'>Bislig</option>
                                    <option value='Bogo'>Bogo</option>
                                    <option value='Borongan'>Borongan</option>
                                    <option value='Butuan'>Butuan</option>
                                    <option value='Cabadbaran'>Cabadbaran</option>
                                    <option value='Cabanatuan'>Cabanatuan</option>
                                    <option value='Cabuyao'>Cabuyao</option>
                                    <option value='Cadiz'>Cadiz</option>
                                    <option value='Cagayan de Oro'>Cagayan de Oro</option>
                                    <option value='Calamba'>Calamba</option>
                                    <option value='Calapan'>Calapan</option>
                                    <option value='Calbayog'>Calbayog</option>
                                    <option value='Caloocan'>Caloocan</option>
                                    <option value='Candon'>Candon</option>
                                    <option value='Canlaon'>Canlaon</option>
                                    <option value='Carcar'>Carcar</option>
                                    <option value='Catbalogan'>Catbalogan</option>
                                    <option value='Cauayan'>Cauayan</option>
                                    <option value='Cavite City'>Cavite City</option>
                                    <option value='ManCebu Cityila'>Cebu City</option>
                                    <option value='Cotabato City'>Cotabato City</option>
                                    <option value='Dagupan'>Dagupan</option>
                                    <option value='Danao'>Danao</option>
                                    <option value='Dapitan'>Dapitan</option>
                                    <option value='Dasmariñas'>Dasmariñas</option>
                                    <option value='Davao City'>Davao City</option>
                                    <option value='Digos'>Digos</option>
                                    <option value='Dipolog'>Dipolog</option>
                                    <option value='Dumaguete'>Dumaguete</option>
                                    <option value='El Salvador'>El Salvador</option>
                                    <option value='Escalante'>Escalante</option>
                                    <option value='Gapan'>Gapan</option>
                                    <option value='General Santos'>General Santos</option>
                                    <option value='General Trias'>General Trias</option>
                                    <option value='Gingoog'>Gingoog</option>
                                    <option value='Guihulngan'>Guihulngan</option>
                                    <option value='Himamaylan'>Himamaylan</option>
                                    <option value='Ilagan'>Ilagan</option>
                                    <option value='Iligan'>Iligan</option>
                                    <option value='Iloilo City'>Iloilo City</option>
                                    <option value='Imus'>Imus</option>
                                    <option value='Iriga'>Iriga</option>
                                    <option value='Isabela'>Isabela</option>
                                    <option value='Kabankalan'>Kabankalan</option>
                                    <option value='Kidapawan'>Kidapawan</option>
                                    <option value='Koronadal'>Koronadal</option>
                                    <option value='La Carlota'>La Carlota</option>
                                    <option value='Lamitan'>Lamitan</option>
                                    <option value='Laoag'>Laoag</option>
                                    <option value='Lapu-Lapu'>Lapu-Lapu</option>
                                    <option value='Las Piñas'>Las Piñas</option>
                                    <option value='Legazpi'>Legazpi</option>
                                    <option value='Ligao'>Ligao</option>
                                    <option value='Lipa'>Lipa</option>
                                    <option value='Lucena'>Lucena</option>
                                    <option value='Maasin'>Maasin</option>
                                    <option value='Mabalacat'>Mabalacat</option>
                                    <option value='Makati'>Makati</option>
                                    <option value='Malabon'>Malabon</option>
                                    <option value='Malaybalay'>Malaybalay</option>
                                    <option value='Malolos'>Malolos</option>
                                    <option value='Mandaluyong'>Mandaluyong</option>
                                    <option value='Mandaue'>Mandaue</option>
                                    <option value='Manila'>Manila</option>
                                    <option value='Marawi'>Marawi</option>
                                    <option value='Marikina'>Marikina</option>
                                    <option value='Masbate City'>Masbate City</option>
                                    <option value='Mati'>Mati</option>
                                    <option value='Meycauayan'>Meycauayan</option>
                                    <option value='Muñoz'>Muñoz</option>
                                    <option value='Muntinlupa'>Muntinlupa</option>
                                    <option value='Naga'>Naga</option>
                                    <option value='Navotas'>Navotas</option>
                                    <option value='Olongapo'>Olongapo</option>
                                    <option value='Ormoc'>Ormoc</option>
                                    <option value='Oroquieta'>Oroquieta</option>
                                    <option value='Ozamiz'>Ozamiz</option>
                                    <option value='Pagadian'>Pagadian</option>
                                    <option value='Palayan'>Palayan</option>
                                    <option value='Panabo'>Panabo</option>
                                    <option value='Parañaque'>Parañaque</option>
                                    <option value='Pasay'>Pasay</option>
                                    <option value='Pasig'>Pasig</option>
                                    <option value='Passi'>Passi</option>
                                    <option value='Puerto Princesa'>Puerto Princesa</option>
                                    <option value='Quezon City'>Quezon City</option>
                                    <option value='Roxas'>Roxas</option>
                                    <option value='Sagay'>Sagay</option>
                                    <option value='Samal'>Samal</option>
                                    <option value='San Carlos'>San Carlos</option>
                                    <option value='San Carlos'>San Carlos</option>
                                    <option value='San Fernando'>San Fernando</option>
                                    <option value='San Fernando'>San Fernando</option>
                                    <option value='San Jose'>San Jose</option>
                                    <option value='San Jose del Monte'>San Jose del Monte</option>
                                    <option value='San Juan'>San Juan</option>
                                    <option value='San Pablo'>San Pablo</option>
                                    <option value='San Pedro'>San Pedro</option>
                                    <option value='Santa Rosa'>Santa Rosa</option>
                                    <option value='Santiago'>Santiago</option>
                                    <option value='Silay'>Silay</option>
                                    <option value='Sipalay'>Sipalay</option>
                                    <option value='Sorsogon City'>Sorsogon City</option>
                                    <option value='Surigao City'>Surigao City</option>
                                    <option value='Tabaco'>Tabaco</option>
                                    <option value='Tabuk'>Tabuk</option>
                                    <option value='Tacloban'>Tacloban</option>
                                    <option value='Tacurong'>Tacurong</option>
                                    <option value='Tagaytay'>Tagaytay</option>
                                    <option value='Tagbilaran'>Tagbilaran</option>
                                    <option value='Taguig'>Taguig</option>
                                    <option value='Tagum'>Tagum</option>
                                    <option value='Talisay'>Talisay</option>
                                    <option value='Tanauan'>Tanauan</option>
                                    <option value='Tandag'>Tandag</option>
                                    <option value='Tangub'>Tangub</option>
                                    <option value='Tanjay'>Tanjay</option>
                                    <option value='Tarlac City'>Tarlac City</option>
                                    <option value='Tayabas'>Tayabas</option>
                                    <option value='Toledo'>Toledo</option>
                                    <option value='Trece Martires'>Trece Martires</option>
                                    <option value='Tuguegarao'>Tuguegarao</option>
                                    <option value='Urdaneta'>Urdaneta</option>
                                    <option value='Valencia'>Valencia</option>
                                    <option value='Valenzuela'>Valenzuela</option>
                                    <option value='Victorias'>Victorias</option>
                                    <option value='Vigan'>Vigan</option>
                                    <option value='Zamboanga City'>Zamboanga City</option>
                                </select>
                                @if( $errors && $errors->has('wah')) <div class="error-label">{{ $errors->first('wah') }}</div> @endif
                            </div>
                        </div>
                        <div class="col-md-6 nopadding">
                            <div class="col-md-12 paddingb10 group-field">
                                <label>Company Name</label>
                                <input type="text" class="df-field" name="business_name" required>
                                @if( $errors && $errors->has('business_name')) <div class="error-label">{{ $errors->first('business_name') }}</div> @endif
                            </div>
                            <div class="col-md-12 paddingb10 group-field group-field">
                                <label>Industry</label>
                                <select class="df-field" name="business_type" required>
                                    <option value="">Select Industry</option>
                                    <option value="|1">Automotive Dealers and Manufacturers</option>
                                    <option value="|2">Automotive Services</option>
                                    <option value="|3">Entertainment</option>
                                    <option value="|4">Fitness</option>
                                    <option value="|5">Food and Beverage Establishment</option>
                                    <option value="|6">Fuel and Petroleum</option>
                                    <option value="|7">Grocery and Supermarket</option>
                                    <option value="|8">Grooming and Wellness</option>
                                    <option value="|9">Hospitals and Healthcare Services</option>
                                    <option value="|10">Insurance</option>
                                    <option value="|11">Petroleum</option>
                                    <option value="|12">Retail - Apparel</option>
                                    <option value="|18">Retail - Electronics</option>
                                    <option value="|13">Retail - Household and Hardware</option>
                                    <option value="|14">Retail - Sporting Goods</option>
                                    <option value="|15">Telecommunication and Utilities</option>
                                    <option value="|16">Transportation</option>
                                    <option value="|17">Travel and Leisure</option>
                                    <option value="|Others">Others</option>
                                </select>
                                @if( $errors && $errors->has('business_type')) <div class="error-label">{{ $errors->first('business_type') }}</div> @endif
                            </div>
                            <div class="col-md-12 paddingb10 group-field">
                                <label>Business Objective</label>
                                <select name="program_objective" class="df-field" required>
                                    <option value="">Select Objective</option>
                                    <option value="|1">Enhance customer engagement</option>
                                    <option value="|2">Improve customer retention</option>
                                    <option value="|3">Increase frequency of customer visits</option>
                                    <option value="|4">Drive revenue per transaction</option>
                                    <option value="|5">Gather insights and understand customer behavior</option>
                                    <option value="|6">Promote and push new products and services</option>
                                    <option value="|Others">Others</option>
                                </select>
                                @if( $errors && $errors->has('program_objective')) <div class="error-label">{{ $errors->first('program_objective') }}</div> @endif
                            </div>
                            <div class="col-md-12 paddingb10 group-field">
                                <div class="col-xs-6 nopadding branches">
                                    <label>No. of Branches</label>
                                    <input type="text" class="df-field" name="business_branches" required>
                                    @if( $errors && $errors->has('business_branches')) <div class="error-label">{{ $errors->first('business_branches') }}</div> @endif
                                </div>
                                <div class="col-xs-6 nopadding ave  pull-right">
                                    <label>Ave. Customer/Month</label>
                                    <input type="text" class="df-field" name="customer_no" required>
                                    @if( $errors && $errors->has('customer_no')) <div class="error-label">{{ $errors->first('customer_no') }}</div> @endif
                                </div>
                            </div>
                            <div class="col-md-12 paddingb10 group-field text-center">
                                <div class="g-recaptcha" data-sitekey="6LeSJSkTAAAAAJqYJJczKaKyhU6G_0DXkRsZM_1-"></div>
                                <small class="text-danger hidden hidden-text">Please verify captcha</small>
                                @if( $errors && $errors->has('g-recaptcha-response')) <div class="error-label">{{ $errors->first('g-recaptcha-response') }}</div> @endif
                            </div>
                            <input type="submit" class="hide" value="Submit" />
                    </form>
                            <div class="col-md-12">
                                <br>
                                <input id="form_signup_button" type="submit" class="bt-bg continue" value="Continue" />
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    <!-- Continue to Select Package -->
    <div id="su-select-package" class="modal fade" role="dialog">
        <div class="modal-dialog su-packages">
            <div class="modal-content">
                <div class="modal-header aCenter">
                    <div type="button" class="close" data-dismiss="modal"><img src="app/page/onboarding/images/close-button-white.png"></div>
                    <img src="app/page/onboarding/images/multi-platform.png" class="img-reponsive">
                </div>
                <div class="modal-body aCenter">
                    <h3>Choose which loyalty program suits your business needs.</h3>
                </div>
                <div class="modal-footer aCenter">
                    <button type="button" class="bt-bg continue">Continue</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end: Continue to Select Package -->

    <section>
        <nav class="navbar navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#defaultNavbar1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar top-bar"></span>
                        <span class="icon-bar middle-bar"></span>
                        <span class="icon-bar bottom-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ url('/') }}"><img src="app/page/onboarding/images/rush-logo.svg" width="100"></a>
                </div>
                <div class="collapse navbar-collapse" id="defaultNavbar1">
                    <div class="full-mobile">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="{{ url('about') }}">About Us</a></li>
                            <li><a href="{{ url('newsroom') }}">Newsroom</a></li>
                            <li><a href="{{ url('how-it-works') }}">How It Works</a></li>
                            <li><a href="{{ url('products') }}">Products</a></li>
                            <li><a href="{{ url('request-demo#request-demo-form') }}">Request Demo</a></li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <button type="button" class="no-bg-br sign-in" data-toggle="modal" data-target="#signin">Sign In</button>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </section>
    <section>
        <div class="jumbotron info-page-head">
            <div class="container aCenter">
                <h2>Terms of Use</h2>
            </div>
        </div>
        <div class="container info-page-body">
            <div class="col-md-12">
                <p>These Terms and Conditions ("Terms") govern the use of the RUSH Loyalty Platform (as defined below). Please read these Terms carefully. By executing the Registration Form or using the RUSH Loyalty Platform, You (as defined below) acknowledge that You understand and agree to be bound by these Terms. If You do not agree to these Terms, do not use the RUSH Loyalty Platform.</p>
                <p>Globe Telecom, Inc. ("Globe", the "Platform Provider", "we") reserves the right to modify these Terms at any time at its sole discretion with or without prior notice, and any changes made will become effective immediately upon posting of the revised Terms in the RUSH website (rush.ph). It shall be your obligation to be informed of such modifications by accessing, from time to time, such website where the latest version of these Terms may be found. Your continued use of the RUSH Loyalty Platform shall be deemed an acceptance of any revision or modification of these Terms.</p>
                <p>Globe shall have the right to suspend or terminate the operation, in whole or in part, of the RUSH Loyalty Platform for whatever reason. In case of suspension or termination of the RUSH Loyalty Platform, you understand that it shall be the Merchant’s discretion to continue offering its Loyalty Program using a different platform and that it is the Merchant’s responsibility to notify you regarding downtimes, suspension or discontinuation of the Loyalty Program via RUSH. All Loyalty Credits earned prior to the suspension or termination and Loyalty Rewards offered by the Merchant may be cancelled at the sole option of Globe. In the event of any suspension or termination of the RUSH Loyalty Platform, You agree to hold Globe free and harmless from any claim, damage, loss, expense, suit or liability whatsoever, arising from such suspension or termination.</p>
            </div>
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#definitions">
                                <span class="glyphicon glyphicon-minus"></span>
                                Definitions and Interpretations
                            </a>
                        </h4>
                    </div>
                    <div id="definitions" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <p><strong>1.1.</strong> Unless otherwise defined herein, capitalized terms shall have the following meanings:</p>
                            <table id="definitions" width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>Business Days</td>
                                    <td>Refers to any day when commercial banks are open for business in Metro Manila, except Saturday or Sunday or any legal holiday not falling on either a Saturday or Sunday.</td>
                                </tr>
                                <tr>
                                    <td>Content</td>
                                    <td>Includes, without limitation, Merchant information, Merchant loyalty program offers, special offers, interactive features, Merchant terms and conditions of loyalty program, user profiles, information, photos, data, and any text.</td>
                                </tr>
                                <tr>
                                    <td>Customer</td>
                                    <td>Refers to Merchant’s customer who purchases and/or avails of Merchant’s goods and/or services.</td>
                                </tr>
                                <tr>
                                    <td>Customer Loyalty Card</td>
                                    <td>Refers to a physical card which can be used to access the User’s Account in lieu of the Customer Mobile Application which has a QR code specific to the User’s Account. The Customer Loyalty Card is only available for RUSH Pro and Ultimate.</td>
                                </tr>
                                <tr>
                                    <td>Customer Mobile App</td>
                                    <td>Refers to the application powered by RUSH, which is used by the User to register and transact with the Merchant for the Loyalty Program.</td>
                                </tr>
                                <tr>
                                    <td>Customer Web Portal</td>
                                    <td>Refers to the web portal powered by RUSH, which is used by the User to register and transact with the Merchant for the Loyalty Program.</td>
                                </tr>
                                <tr>
                                    <td>Globe</td>
                                    <td>Refers to Globe Telecom, Inc., is a company duly organized and existing under the laws of the Philippines, including any of its subsidiaries, affiliates, agents or representatives.</td>
                                </tr>
                                <tr>
                                    <td>Loyalty Credits</td>
                                    <td>Refer to both Loyalty Program Points or Stamps which are earned by a User under the Loyalty Program.</td>
                                </tr>
                                <tr>
                                    <td>Loyalty Program</td>
                                    <td>Refers to the customer retention and rewards program of Merchant which is powered by RUSH.</td>
                                </tr>
                                <tr>
                                    <td>Loyalty Program Points or Stamps</td>
                                    <td>Refers to points or stamps that are recognized and valid for use on the RUSH platform.</td>
                                </tr>
                                <tr>
                                    <td>Loyalty Program Transaction</td>
                                    <td>Refers to any transaction made that involves the earning or use of Loyalty Credits or Loyalty Rewards.</td>
                                </tr>
                                <tr>
                                    <td>Loyalty Rewards</td>
                                    <td>Refer to the rewards that may be offered by Merchant for redemption using Loyalty Points or Stamps that a User has accrued from transactions under the Loyalty Program.</td>
                                </tr>
                                <tr>
                                    <td>Merchant</td>
                                    <td>Refers to any business establishment duly licensed to do business in the Philippines that has contracted Globe to provide RUSH to enable Merchant’s Loyalty Program.</td>
                                </tr>
                                <tr>
                                    <td>Merchant Mobile App or Transaction Confirmation</td>
                                    <td>Refers to a notification or message displayed on the Merchant Mobile App or POS Terminal and sent to User’s mobile number, email address, or in-app notification, that evidences the earning of Loyalty Program Points or Stamps or redemption of Loyalty Credits by You at such terminal. It may contain relevant data such as transaction amount, credits, and transaction certificate number, among others, for approved Loyalty Program Transactions, or an error message, as the case may be.</td>
                                </tr>
                                <tr>
                                    <td>Merchant Mobile App</td>
                                    <td>Refers to the application powered by RUSH, which is used by the Merchant in store to operate (transfer points, accept points as payment, etc.) the Loyalty Program.</td>
                                </tr>
                                <tr>
                                    <td>Point of Sale (POS)</td>
                                    <td>Refers to the event in which a purchase is made.</td>
                                </tr>
                                <tr>
                                    <td>Point of Sale (POS) Terminal</td>
                                    <td>Refers to the machine which records and processes a purchase transaction.</td>
                                </tr>
                                <tr>
                                    <td>Registration Form</td>
                                    <td>Shall mean the application form accomplished via Merchant Mobile App, Customer Web Portal, Customer Mobile App, or SMS, which shall be executed by an End-User to be eligible to use the Loyalty Program and its functionalities powered by RUSH.</td>
                                </tr>
                                <tr>
                                    <td>RUSH or RUSH Loyalty Platform</td>
                                    <td>Refers to the RUSH system wherein Users can earn and avail of points, stamps, and rewards from Merchants. This includes, but is not limited to, all User and Merchant Accounts, Loyalty Program Points or Stamps, online information system, Merchant Mobile App, Customer Mobile App, Customer Loyalty Card, Customer Web Portal, and marketing communications system used in connection with RUSH.</td>
                                </tr>
                                <tr>
                                    <td>Short Message Service or SMS</td>
                                    <td>Refers to a communication protocol allowing the interchange of short text messages between mobile phone devices.</td>
                                </tr>
                                <tr>
                                    <td>SMS Confirmation</td>
                                    <td>Refers to an SMS sent to Your mobile number that evidences the earning of Loyalty Program Points or Stamps or redemption of Loyalty Rewards by You. It may contain relevant data such transaction amount, credits, and transaction certificate number, among others, for approved Loyalty Program Transactions, or an error message, as the case may be.</td>
                                </tr>
                                <tr>
                                    <td>Terms</td>
                                    <td>Refer to these Terms and Conditions, as amended from time to time.</td>
                                </tr>
                                <tr>
                                    <td>Transaction</td>
                                    <td>Refers to a purchase or sales transaction between a User and Merchant that earns Loyalty Program Points or Stamps that are honored on RUSH.</td>
                                </tr>
                                <tr>
                                    <td>User or You</td>
                                    <td>Refers to an individual or entity that executes the Registration Form or uses the Loyalty Program powered by RUSH.</td>
                                </tr>
                                <tr>
                                    <td>User Activity</td>
                                    <td>Includes, without limitation, user stamping and collecting, app refreshing and updating of version, earning of achievements via medals and leadership circle, inviting and adding of friends, viewing other users’ profile, social media postings, setting up profiles, and downloading and claiming of rewards. Further included are seeding and acceptance of stamps/points as payment.</td>
                                </tr>
                                <tr>
                                    <td>User ID</td>
                                    <td>Refers to a User’s identifier for any RUSH platform, application, or website, or the User’s mobile number, email address, or social network account duly registered in RUSH. It is used to identify the User and interact with the Merchant Mobile App, Customer Mobile App and/or Customer Web Portal.</td>
                                </tr>
                            </table>
                            <p><strong>1.2.</strong> Unless the context otherwise requires, words importing a gender include every gender and words imputing the singular include the plural and vice versa. Words denoting persons shall include individuals, corporations, partnerships, joint ventures, trusts, unincorporated organizations and any political subdivision, agency or instrumentality.</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#eligibility">
                                <span class="glyphicon glyphicon-plus"></span>
                                Eligibility and Participation
                            </a>
                        </h4>
                    </div>
                    <div id="eligibility" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p><strong>2.1.</strong> You must be 18 years of age or older to apply for a User Account by executing the necessary Registration Form:</p>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td><strong>2.1.1.</strong> through the Merchant Mobile App;</td>
                                </tr>
                                <tr>
                                    <td><strong>2.1.2.</strong> through the Customer Mobile App;</td>
                                </tr>
                                <tr>
                                    <td><strong>2.1.3.</strong> through Customer Web Portal; or</td>
                                </tr>
                                <tr>
                                    <td><strong>2.1.4.</strong> through SMS</td>
                                </tr>
                            </table>
                            <p><strong>2.2.</strong> When accomplishing the Registration Form, you must provide correct and accurate information in User Account.</p>
                            <p><strong>2.3.</strong> You are responsible for keeping your User Account secured which includes, but is not limited to, keeping your login credentials private. RUSH or Globe shall not be liable in case Your User ID is used for other purposes.</p>
                            <p><strong>2.4.</strong> You are responsible for any User Activity (earning and collecting digital stamps or points, setting up your personal password and profile, earning achievements, downloading and claiming rewards, updating and refreshing the app, among others) that occurs under your Loyalty Program account. All Loyalty Program transactions made using your User ID are conclusively presumed to be made by You and You shall be liable therefor.</p>
                            <p><strong>2.5.</strong> You may not use Loyalty Program or RUSH for any illegal or unauthorized purpose. You shall abide by all applicable local, national, and international laws and regulations.</p>
                            <p><strong>2.6.</strong> You are solely responsible for your conduct and any data, comment, information, names, photos, ("User Content") that you post or display on Your profile or account.</p>
                            <p><strong>2.7.</strong> You must not modify, adapt or hack RUSH Loyalty Platform or modify another website or mobile app so as to falsely imply that it is associated with RUSH.</p>
                            <p><strong>2.8.</strong> You must not crawl, scrape, or otherwise cache any Content, including but not limited to Merchant listings, Merchant information, offers and rewards, and Users’ information.</p>
                            <p><strong>2.9.</strong> You must not send out any worms or viruses or any code of a destructive nature.</p>
                            <p><strong>2.10.</strong> You must not, in the use of RUSH, violate any laws in your jurisdiction (including but not limited to copyright laws).</p>
                            <p><strong>2.11.</strong> Violation of any of these Terms will result in the termination of your User Account. While Globe strictly prohibits such conduct on its app and website, you understand and agree that Globe is not responsible for the Content displayed on the app and the website and you nonetheless acknowledge that you may be exposed to such materials and that you use RUSH at your own risk.</p>
                            <p><strong>2.12.</strong> You agree to share your activities and transactions with Globe and Merchant</p>
                            <p><strong>2.13.</strong> You consent to Your profile being viewed by other Users.</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#earning">
                                <span class="glyphicon glyphicon-plus"></span>
                                Earning of Loyalty Program Points or Stamps
                            </a>
                        </h4>
                    </div>
                    <div id="earning" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p><strong>3.1.</strong> You must provide Your User ID to the Merchant when paying for any goods and/or services under the Loyalty Program.</p>
                            <p><strong>3.2.</strong> For every eligible Transaction, You shall earn the corresponding Loyalty Program Points or Stamps based on the total price paid to the Merchant and the equivalent amount of Loyalty Program Points or Stamps set by the Merchant. The total amount of Loyalty Program Points or Stamps earned from each Transaction will be reflected in the Transaction Confirmation, Merchant Mobile App, Customer Mobile App, and/or Customer Web Portal.</p>
                            <p><strong>3.3.</strong> Globe reserves the right to add or deduct credits from Your User Account, following reconciliation of data from the Merchants and other sources or other justifiable reason based solely on its discretion.</p>
                            <p><strong>3.4.</strong> You must not obtain Loyalty Program Points, Stamps, and or Loyalty Rewards through unauthorized, fraudulent or illegal means.</p>
                            <p><strong>3.5.</strong> You must not use any stamps, points, and/or rewards obtained from technical glitches of the Merchant Mobile App, Customer Mobile App, Customer Web Portal, or any unauthorized, fraudulent, or illegal means.</p>
                            <p><strong>3.6.</strong> You must claim Loyalty Program Points or Stamps at the time of purchase. The Merchant reserves the right to refuse to give out stamps or points as it deems reasonable.</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#redemption">
                                <span class="glyphicon glyphicon-plus"></span>
                                Redemption of Loyalty Rewards
                            </a>
                        </h4>
                    </div>
                    <div id="redemption" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p><strong>4.1.</strong> You must provide Your User ID to the Merchant before redeeming any of Your earned Loyalty Program Points or Stamps.</p>
                            <p><strong>4.2.</strong> Redemption of Loyalty Program Points or Stamps constitutes an exchange of earned Loyalty Program Points or Stamps with the equivalent value of goods and/or services from the Merchant in accordance with the Loyalty Program mechanics. Loyalty Program Points or Stamps may only be redeemed at the Merchant that issued the aforementioned Loyalty Program Points or Stamps.</p>
                            <p><strong>4.3.</strong> Once a valid redemption of the Loyalty Program Points or Stamps has been made, as confirmed by a Transaction Confirmation, the same cannot be cancelled, revoked or changed unless otherwise provided by these Terms.</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#disputes">
                                <span class="glyphicon glyphicon-plus"></span>
                                Disputes and Erroneous Transactions
                            </a>
                        </h4>
                    </div>
                    <div id="disputes" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p><strong>5.1.</strong> The details in a Transaction Confirmation are presumed to be true and correct unless You notify Merchant in writing of any disputes thereon within seven (7) days from the date of the relevant Transaction. If no dispute is reported in writing within such period, the details in said Transaction Confirmation are considered conclusively true and correct.</p>
                            <p><strong>5.2.</strong> Disputed transactions shall only be credited back to the Your User Account once Merchant and Globe are reasonably satisfied that the claim or dispute has been properly processed, investigated and proven in Your favor.</p>
                            <p><strong>5.3.</strong> Notwithstanding the foregoing, there shall be no reversal of Transactions made through Your error, mistake or negligence.</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#rights">
                                <span class="glyphicon glyphicon-plus"></span>
                                Our Rights
                            </a>
                        </h4>
                    </div>
                    <div id="rights" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p><strong>6.1.</strong> We reserve the right to modify, terminate, or discontinue RUSH for any reason, without notice at any time.</p>
                            <p><strong>6.2.</strong> We reserve the right to share your activities and transactions with Merchant.</p>
                            <p><strong>6.3.</strong> We reserve the right to invalidate Loyalty Program Points or Stamps that are earned from technical glitch or error or unusual activity.</p>
                            <p><strong>6.4.</strong> We reserve the right to invalidate Loyalty Program Points or Stamps and/or Loyalty Rewards obtained through unauthorized, fraudulent or illegal means as proven by our investigation.</p>
                            <p><strong>6.5.</strong> We will not be held liable should a Merchant fail/refuse to honor the Loyalty Program Points or Stamps of a User.</p>
                            <p><strong>6.6.</strong> We will not be held liable in case Loyalty Program Points or Stamps and/or Loyalty Rewards get "lost" due to Your negligence.</p>
                            <p><strong>6.7.</strong> We may, but have no obligation to, remove Content, User Content and RUSH accounts containing User Content, Content and User Activity that we determine in our sole discretion as unlawful, offensive, fraudulent, threatening, libelous, defamatory, obscene or otherwise objectionable or violate any party's intellectual property or these Terms of Use.</p>
                            <p><strong>6.8.</strong> We reserve the right to reclaim RUSH accounts on behalf of businesses or individuals that hold legal claims or trademark on those RUSH accounts.</p>
                            <p><strong>6.9.</strong> We are not responsible for any third party fees (including, without limitation, Internet, mobile phone, or text messaging fees) which you may incur in the use of RUSH.</p>
                            <p><strong>6.10.</strong> RUSH makes it possible for you to post to outside websites images and text through or in connection with the RUSH service. Pages on other websites which display data provided through the RUSH service must provide a link back to RUSH's App Store or Play Store page or RUSH's website. If you decide to access outside websites you do so at your own risk. We do not endorse or take responsibility for the content on other websites or the availability of other websites and you agree that we are not liable for any loss or damage that you may suffer by using other websites.</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#content">
                                <span class="glyphicon glyphicon-plus"></span>
                                Content and Usage
                            </a>
                        </h4>
                    </div>
                    <div id="content" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p><strong>7.1.</strong> We own the Content that we created including but not limited to visual interfaces, interactive features, graphics, design, compilation, computer code, products, software, stamping feature, and all other elements and components of RUSH excluding the content and photos of Merchants’ loyalty offers, promos, its mechanics and third-party content. We also own the copyrights, trademarks, service marks, trade names, and other intellectual and proprietary rights throughout the world ("IP Rights) associated with RUSH, the RUSH service, the RUSH Loyalty Platform and our Content on RUSH, which are protected by copyright, trade dress, patent, trademark laws and all other applicable intellectual and proprietary rights and laws. As such, you may not modify, reproduce, distribute, create derivative works or adaptations of, publicly display or in any way exploit any of RUSH's Content in whole or in part except as expressly authorized by us. Except as expressly and unambiguously provided herein, we do not grant you any express or implied rights, and all rights in and to RUSH, the RUSH service, the RUSH Loyalty Platform, and RUSH's Content are retained by us.</p>
                            <p><strong>7.2.</strong> You should NOT modify, adapt, appropriate, reproduce, distribute, translate, create derivative works or adaptations of, publicly display, republish, repurpose, sell, trade, or in any way exploit RUSH, the RUSH service, the RUSH Loyalty Platform or any of the Content except as expressly authorized by us.</p>
                            <p><strong>7.3.</strong> Although RUSH, the RUSH service and/or the RUSH Loyalty Platform are available in online and offline modes, there will be occasions when RUSH, the RUSH service and/or the RUSH Loyalty Platform will be interrupted for scheduled maintenance or upgrades, for emergency repairs, or due to failure of telecommunications links and equipment that are beyond the control of Globe. Also, although Globe will normally only delete Content, User Content, or social media postings that violate this Agreement, Globe reserves the right to delete any Content, User Content, or social media postings for any reason, without prior notice. Deleted Content and/or User Content may be stored by Globe in order to comply with certain legal obligations and shall not be retrievable by you or any other person without a valid court order. Consequently, Globe encourages you to maintain your own backup of your own data by refreshing (updating) your RUSH app. Globe will NOT be liable to you for any modification, suspension, or discontinuation of the service, or the loss of any Content and/or User Content.</p>
                            <p><strong>7.4.</strong> You may request to deactivate your User Account at any time by contacting the Merchant.</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#communications">
                                <span class="glyphicon glyphicon-plus"></span>
                                Communications from RUSH and other Users
                            </a>
                        </h4>
                    </div>
                    <div id="communications" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p><strong>8.1.</strong> By creating a User Account, you agree to receive certain communications in connection with the RUSH service whether via SMS, Email, App Notifications, among others. For example, you might receive notifications about other Users you added as Loyalty Program friends, announcements and alerts from Globe or Merchants or third party. You also might receive a weekly email related to the RUSH service and its Content.</p>
                            <p><strong>8.2.</strong> Unless You expressly notify Merchant or Globe otherwise, You agree to receive promotional messages either by email, SMS or other communication channels from Merchant or Globe, including its subsidiaries, affiliates, partners, agents and assigns, which communication and promotional messages may be based on Your location and/or other information.</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#exclusions">
                                <span class="glyphicon glyphicon-plus"></span>
                                Exclusions from Liability
                            </a>
                        </h4>
                    </div>
                    <div id="exclusions" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p><strong>9.1.</strong> Your User ID is only valid for the purpose of recording valid and eligible Transactions with Merchant.</p>
                            <p><strong>9.2.</strong> We makes no warranty, express or implied, regarding the RUSH Loyalty Platform and its functionalities. The use of the RUSH Loyalty Platform is offered on as "AS IS", "AS AVAILABLE" basis without warranties of any kind, other than warranties that are incapable of exclusion, waiver or limitation under the laws applicable to these Terms. Without limiting the foregoing, Globe makes no warranty (1) as to the content, quality, or accuracy of data or information provided by Globe hereunder or received or transmitted using the RUSH Loyalty Platform; (2) as to any service or product obtained using the RUSH Loyalty Platform; (3) that the RUSH Loyalty Platform will be uninterrupted or error-free; (4) that any particular result or information will be obtained; or (5) that the service, content, or website servers are free of viruses or other harmful components. Therefore, you should exercise caution in the use and downloading of any such content or materials and use industry-recognized software to detect and disinfect viruses. Without limiting the foregoing, you understand and agree that you download or otherwise obtain content, material or data from or through the service at your own discretion and risk and that you will be solely responsible for your use thereof and for any damage to your mobile device or computer system, loss of data or other harm of any kind that may result. In no event will Globe or its directors, employees or agents be liable to you or any third person for any indirect, consequential, exemplary, incidental, special or punitive damages, including for any lost profits or lost data arising from your use of the RUSH website or the RUSH service, or any of the app and website content or other materials on, accessed through or downloaded from the RUSH service, even if Globe is aware or has been advised of the possibility of such damages. Notwithstanding anything to the contrary contained herein, Globe's liability to you for any cause whatsoever, and regardless of the form of the action, will at all times be limited to the amount paid, if any, by you to Globe for the RUSH service during the term of your membership to the Loyalty Program.</p>
                            <p><strong>9.3.</strong> You acknowledge that if no fees are paid to Globe for the RUSH service, your sole and exclusive remedy shall be limited to injunctive relief only, unless otherwise permitted by law, and you shall not be entitled to damages of any kind from Globe, regardless of the cause of action. You expressly agree that you will assume the entire risk as to the quality and the performance of the RUSH services and the accuracy or completeness of our content.</p>
                            <p><strong>9.4.</strong> We shall not be liable for any undelivered goods or services, defects, damages and after-sales services offered or provided by Merchant. Globe does not give any warranty whatsoever in connection with the goods or services of the Merchant.</p>
                            <p><strong>9.5.</strong> Globe shall not be liable to You or any third party for any loss, compensation, damage (whether direct, indirect, consequential including loss of profit) or liability, arising, directly or indirectly, from or as a result of:</p>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td><strong>9.5.1.</strong> the refusal of Merchant or any establishment to issue, accept or honor Loyalty Program Points or Stamps;</td>
                                </tr>
                                <tr>
                                    <td><strong>9.5.2.</strong> the failure of a Transaction to be completed or honored by a Merchant for whatever reason whatsoever including damage or malfunction of the device running the Merchant Mobile App or POS Terminal, or if the POS Terminal or Merchant Mobile App is offline or unavailable;</td>
                                </tr>
                                <tr>
                                    <td><strong>9.5.3.</strong> You are unable to complete a transaction due to unavailability of the RUSH Loyalty Platform for whatever reason;</td>
                                </tr>
                                <tr>
                                    <td><strong>9.5.4.</strong> any delay, interruption or termination of a Transaction whether caused by administrative error, technical, mechanical, electrical or electrical fault or difficulty or any other reason whatsoever beyond the control of Globe, including but not limited to acts of God, fire, flood, lighting, weather conditions, labor disputes, disturbance, action of government, interference or damage by third parties or any change in legislation;</td>
                                </tr>
                                <tr>
                                    <td><strong>9.5.5.</strong> the unauthorized, illegal or fraudulent use or acquisition of a User ID, User Account or Loyalty Program Points or Stamps; or</td>
                                </tr>
                                <tr>
                                    <td><strong>9.5.6.</strong> any misrepresentation or fraud by or misconduct of any third party.</td>
                                </tr>
                            </table>
                            <p><strong>9.6.</strong> Subject to the dispute procedure in Section 5 under these Terms, Globe and the Merchant shall not be liable for Loyalty Program Points or Stamps not recorded in Your User Account for whatever reason, including but not limited to, malfunction of the device running the Merchant Mobile App or POS Terminal or force majeure beyond the control of all parties concerned.</p>
                            <p><strong>9.7.</strong> The use of the RUSH Loyalty Platform and the accrual and redemption of Loyalty Program Points or Stamps are intended solely for the purpose of tracking and rewarding patronage of Merchants. Earning and accumulation of Loyalty Program Points or Stamps for the purpose of allowing You to exchange the same for the value of goods and services are purely discretionary on the part of Merchant. Globe shall not have any obligation to You in so far as the use of the RUSH Loyalty Platform and Loyalty Program Points and Stamps are concerned. Globe shall not be deemed the supplier of the goods and/or services purchased by You from the Merchant using Loyalty Program Points or Stamps.</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#indemnity">
                                <span class="glyphicon glyphicon-plus"></span>
                                Indemnity
                            </a>
                        </h4>
                    </div>
                    <div id="indemnity" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p><strong>10.1.</strong> You agree to hold Globe, its officials and employees, free and harmless from any liability to any third party, and to indemnify Globe against all liability, damages, losses, claims, costs and expenses, penalties and taxes, including legal costs, incurred in connection with Your use of the RUSH Loyalty Platform. You further agree that Globe shall not be held liable for any loss or damage resulting from any and all errors unintentionally committed, any computer software or hardware errors (including computer viruses) resulting in the failure to complete a transaction. Neither shall Globe be held liable for any loss or damage caused by any act or omission of any third party, such as a provider of telecommunication services, internet service provider, or for any circumstances beyond Globe’s control such as severe weather, flood, earthquake or other natural disasters, fire, war, strike, acts of civil or military authority, equipment failure, interruption or failure of electricity and communication facilities.</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#data">
                                <span class="glyphicon glyphicon-plus"></span>
                                Data Collection
                            </a>
                        </h4>
                    </div>
                    <div id="data" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p><strong>11.1.</strong> You authorize Globe to collect any information obtained by Globe and/or Merchant in relation to Your use of the RUSH Loyalty Platform. Such information may include, but shall not be limited to, Your demographics, behavior, usage of RUSH, location, or photo. You hereby irrevocably and unconditionally consent that Merchant and/or Globe, including its subsidiaries, affiliates, partners, agents and assigns may use, process, disclose, transfer or otherwise deal with all such information in whatever manner and for whatever purpose as Globe may deem fit and proper, including but not limited to curating special offers based on your profile and usage, without further notice to You.</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#miscellaneous">
                                <span class="glyphicon glyphicon-plus"></span>
                                Miscellaneous
                            </a>
                        </h4>
                    </div>
                    <div id="miscellaneous" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p><strong>12.1.</strong> Globe shall not be liable for any failure to perform its obligations hereunder where such failure results from any cause beyond Globe's reasonable control, including, without limitation, mechanical, electronic and communications failure. These Terms of Use are personal to you, and are not assignable, transferable or sublicensable by you except with Globe's prior written consent. Globe may assign, transfer or delegate any of its rights and obligations hereunder without your consent. No agency, partnership, joint venture, or employment relationship is created as a result of these Terms of Use and neither party has any authority of any kind to bind the other in any respect.</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#suggestions">
                                <span class="glyphicon glyphicon-plus"></span>
                                Suggestions and Improvements
                            </a>
                        </h4>
                    </div>
                    <div id="suggestions" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p><strong>13.1.</strong> By providing us any ideas, suggestions, documents or proposals ("Feedback"), you warrant and agree that (i) your Feedback does not contain the confidential or proprietary information of third parties, (ii) we are under no obligation of confidentiality, express or implied, with respect to the Feedback, (iii) we may have something similar to the Feedback already under consideration or in development, and (iv) you grant us an irrevocable, non-exclusive, royalty-free, perpetual, worldwide license to use, modify, publish, distribute and sublicense the Feedback.</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#nonwaiver">
                                <span class="glyphicon glyphicon-plus"></span>
                                Non-Waiver of Rights
                            </a>
                        </h4>
                    </div>
                    <div id="nonwaiver" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p><strong>14.1.</strong> Failure, omission or delay on the part of Globe to exercise its rights or remedies under these Terms shall not operate as a waiver of rights.</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#separability">
                                <span class="glyphicon glyphicon-plus"></span>
                                Separability
                            </a>
                        </h4>
                    </div>
                    <div id="separability" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p><strong>15.1.</strong> If any clause or part of a clause of these Terms shall be, or be found by any authority or court of competent jurisdiction to be, invalid or unenforceable, such invalidity or unenforceability shall not affect the other clauses or parts of such clauses of these Terms, all of which shall remain in full force and effect.</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#governing">
                                <span class="glyphicon glyphicon-plus"></span>
                                Governing Law and Venue
                            </a>
                        </h4>
                    </div>
                    <div id="governing" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p><strong>16.1.</strong> These Terms shall be governed by and construed in accordance with the laws of the Philippines. Any dispute, controversy, or claim arising out of or relating to this Agreement, or the breach, termination or invalidity thereof shall be settled by arbitration in the Philippines by a panel of three (3) arbitrators in accordance with the Philippine Dispute Resolution Center, Inc. ("PDRCI") Arbitration Rules as at present in force. Should interim judicial relief be necessary in connection with these Terms, the parties may seek appropriate relief before the courts of Taguig City to the exclusion of other venues.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="ini-footer2">
            <div class="container">
                <div class="col-md-7"><span class="l-label">Start your RUSH-powered loyalty program.</span></div>
                <div class="col-md-5">
                    <button type="button" class="no-bg-brw r-demo" onclick="window.location.href='{{ url('request-demo#request-demo-form') }}'">Request Demo</button>&nbsp;
                    <button type="button" class="bt-bg f-trial" data-toggle="modal" data-target="#signup">Start Free Trial</button>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="container">
                <div class="col-md-8">
                    <ul class="f-links col-md-6">
                        <li><a href="{{ url('terms-of-use') }}">Terms of use</a></li>
                        <li><a href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
                        <li><a href="{{ url('faqs') }}">FAQS</a></li>
                    </ul>
                    <ul class="s-icons col-md-6">
                        <li>
                            <a href="https://www.facebook.com/rushloyaltyh" target="_blank"><img src="app/page/onboarding/images/facebook-icon-footer.svg"></a>
                        </li>
                        <li>
                            <a href="https://twitter.com/rushrewardsph" target="_blank"><img src="app/page/onboarding/images/twitter-icon-footer.svg"></a>
                        </li>
                        <li>
                            <a href="https://www.linkedin.com/company/rushloyalty/" target="_blank"><img src="app/page/onboarding/images/linkedin-logo.png"></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 copyright">Copyright © RUSH 2017. All Rights Reserved</div>
            </div>
        </div>
    </section>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="app/page/onboarding/js/jquery-1.11.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="app/page/onboarding/js/bootstrap.js"></script>
    <script>
        // Accordion
        $('.collapse').on('shown.bs.collapse', function() {
            $(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
        }).on('hidden.bs.collapse', function() {
            $(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
        });
    </script>

    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script type="text/javascript">
        $(document).ready( function() {

            $('#form_signin_button').on('click', function() {
                $.ajax({
                  type: "POST",
                  url: '{{ url('prelogin') }}',
                  dataType: "json",
                  data: $('#form_signin').serialize()
                }).done( function( response ) {
                    if ( response.login_status == "success" ) {
                        $('#form_signin').submit();
                    } else if( response.login_status == 'failed' ) {
                        $("#loginmessage").removeClass('hide').html( response.error_message )
                    } else {
                        $("#loginmessage").removeClass('hide').html( 'An error has occured. Please try again.' )
                    }
                });
                return false;  
            });

            $('#forgot_password_button').on('click', function() {
                if ( $('#form_signin_username').val() ) {
                    $.ajax({
                      type: "POST",
                      url: '{{ url('preforgot') }}',
                      dataType: "json",
                      data: $('#form_signin').serialize()
                    }).done( function( response ) {
                        if ( response.status == "success" ) {
                            $('<form method="post" id="forgot_pass_form" action="{{ url('forgot-password') }}"></form>').appendTo('body')
                            $('<input>').attr({
                                type: 'hidden',
                                name: 'email',
                                value: $('#form_signin_username').val()
                            }).appendTo('#forgot_pass_form');
                            $('<input>').attr({
                                type: 'hidden',
                                name: 'type',
                                value: response.type
                            }).appendTo('#forgot_pass_form');
                            $('#forgot_pass_form').submit();
                        } else if( response.status == 'failed' ) {
                            $("#loginmessage").removeClass('hide').html( response.message )
                        } else {
                            $("#loginmessage").removeClass('hide').html( 'An error has occured. Please try again.' )
                        }
                    });
                    return false;
                } else {
                    $("#loginmessage").removeClass('hide').html( 'Email is required.' );
                }
            });

            $('#form_signup_button').on('click', function() {
                $('.error-label').remove();
                $.ajax({
                  type: "POST",
                  url: '{{ url('presignup') }}',
                  dataType: "json",
                  data: $('#form_signup').serialize()
                }).done( function( response ) {
                    if ( response.signup_status == "success" ) {
                        $("#su-select-package").modal('show');
                        $("#signup").modal('hide');
                    } else if( response.signup_status == 'failed' ) {
                        $.each( response.errors , function( key, value ) {
                            $('input[name='+ key +'], select[name='+ key +']').parent().append('<div class="error-label">'+value+'</div>');
                        });
                        grecaptcha.reset();
                    } else {
                        $('<div class="error-label">An error has occured. Please try again.</div>').appendTo('#form_signup')
                    }
                });
                return false;  
            });

            $('#form_signup').on('keyup', 'input[name="person_contact"], input[name="business_branches"], input[name="customer_no"]', function () { 
                this.value = this.value.replace(/[^0-9\.]/g,'');
            });

            $('#form_signin').on('keypress', '#form_signin_password', function (e) {
              if (e.which == 13) {
                $("#form_signin_button").click();
                return false;
              }
            });

            $("#su-select-package").on("hidden.bs.modal", function(){
                $("body").css("padding-right", "initial");
            });

            $("#su-select-package button.continue").on("click", function() {
                $('#form_signup').submit();
            });
            
        });
    </script>
</body>

</html>
