<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:title" content="RUSH - Loyalty Made Easy">
    <meta property="og:image" content="{{ url('app/assets/images/rush-image1.jpg') }}">
    <meta property="og:image" content="{{ url('app/assets/images/rush-logo.jpg') }}">
    <meta property="og:description" content="Conveniently develop and deploy a custom-branded mobile app in just three days. Easily build, track, and manage your own customer loyalty program with RUSH. Sign up and get a 30-day free trial.">
    <meta property="og:type" content="website" />
    <meta property="og:image:type" content="image/jpeg" />
    <title>RUSH</title>
    <link rel="stylesheet" href="app/page/onboarding/scrollify/main.css">
    <!-- Bootstrap -->
    <link href="app/page/onboarding/css/bootstrap.css" rel="stylesheet">
    <link rel='shortcut icon' href="app/page/onboarding/images/favicon.png">
    <link href="app/page/onboarding/css/animate.css" rel="stylesheet">
    <link href="app/page/onboarding/css/slick.css" rel="stylesheet">
    <link href="app/page/onboarding/css/slick-theme.css" rel="stylesheet">
    <link href="app/page/onboarding/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,500,700" rel="stylesheet">
    <link rel="icon" href="app/page/onboarding/images/favicon.png" type="image/png">
    <script src="https://use.fontawesome.com/594fdbf427.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    @if(getenv('APP_ENV') == 'production')
    <script>
       (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
       (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
       m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
       })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

       ga('create', 'UA-87488508-1', 'auto');
       ga('send', 'pageview');
    </script>
    @endif
</head>

<body>
    <div class="loading"></div>
    <!-- Loyaly Video -->
    <div id="loyalty" class="modal fade" role="dialog" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content text-center">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <video id="homepage-video" width="590" controls>
                  <source src="{{ MigrationHelper::getServerUrl() }}repository/onboarding/rush-homepage-video.mp4" type="video/mp4">
                Your browser does not support the video tag.
                </video>
            </div>
        </div>
    </div>
    <!-- Sign In -->
    <div id="signin" class="modal fade" role="dialog" tabindex="-1">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal"><img src="app/page/onboarding/images/close-button.png"></div>
                    <h4 class="modal-title"><img src="app/page/onboarding/images/rush-logo.svg" width="170"></h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('login') }}" method="post" id="form_signin">
                        <input id="form_signin_username" type="text" class="df-field" placeholder="Email" name="email" required>
                        <input id="form_signin_password" type="password" class="df-field" placeholder="Password" name="password" required>
                        <div class="error-label hide" id="loginmessage"></div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button id="form_signin_button" type="submit" class="bt-bg continue">Continue</button>
                    <div class="forgot"><a id="forgot_password_button" style="cursor: pointer;">Forgot your password?</a></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Sign Up-->
    <div id="signup" class="modal fade" role="dialog" tabindex="-1">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal"><img src="app/page/onboarding/images/close-button.png"></div>
                    <h4 class="modal-title">Sign Up</h4>
                </div>
                <div class="modal-body">
                    <form id="form_signup" action="{{ url('select-package') }}" method="post" data-toggle="validator">
                        <div class="col-md-6 nopadding">
                            <!--<div class="col-md-12 paddingb10 group-field">
                                <label>Name</label>
                                <input type="text" class="df-field" name="person_name" required>-->
                                {{-- @if( $errors && $errors->has('person_name')) <div class="error-label">{{ $errors->first('person_name') }}</div> @endif --}}
                            <!--</div>-->
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6 paddingb10 group-field">
                                        <label>First Name</label>
                                        <input type="text" class="df-field" name="person_fname" required>
                                    </div>
                                    <div class="col-md-6 paddingb10 group-field">
                                        <label>Last Name</label>
                                        <input type="text" class="df-field" name="person_lname" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 paddingb10 group-field">
                                <label>Designation/Position</label>
                                <input type="text" class="df-field" name="person_position" required>
                                @if( $errors && $errors->has('person_position')) <div class="error-label">{{ $errors->first('person_position') }}</div> @endif
                            </div>
                            <div class="col-md-12 paddingb10 group-field">
                                <label>Mobile No.</label>
                                <input type="text" class="df-field" name="person_contact" required maxlength="11">
                                @if( $errors && $errors->has('person_contact')) <div class="error-label">{{ $errors->first('person_contact') }}</div> @endif
                            </div>
                            <div class="col-md-12 paddingb10 group-field">
                                <label>Email</label>
                                <input type="text" class="df-field" name="person_email" required>
                                @if( $errors && $errors->has('person_email')) <div class="error-label">{{ $errors->first('person_email') }}</div> @endif
                            </div>
                            <div class="col-md-12 paddingb10 group-field">
                                <label>Location</label>
                                <select name="business_location" class="df-field" required>
                                  <option value=''>Select location</option>
                                    <option value='Alaminos'>Alaminos</option>
                                    <option value='Angeles'>Angeles</option>
                                    <option value='Antipolo'>Antipolo</option>
                                    <option value='Bacolod'>Bacolod</option>
                                    <option value='Bacoor'>Bacoor</option>
                                    <option value='Bago'>Bago</option>
                                    <option value='Baguio'>Baguio</option>
                                    <option value='Bais'>Bais</option>
                                    <option value='Balanga'>Balanga</option>
                                    <option value='Batac'>Batac</option>
                                    <option value='Batangas City'>Batangas City</option>
                                    <option value='Bayawan'>Bayawan</option>
                                    <option value='Baybay'>Baybay</option>
                                    <option value='Bayugan'>Bayugan</option>
                                    <option value='Biñan'>Biñan</option>
                                    <option value='Bislig'>Bislig</option>
                                    <option value='Bogo'>Bogo</option>
                                    <option value='Borongan'>Borongan</option>
                                    <option value='Butuan'>Butuan</option>
                                    <option value='Cabadbaran'>Cabadbaran</option>
                                    <option value='Cabanatuan'>Cabanatuan</option>
                                    <option value='Cabuyao'>Cabuyao</option>
                                    <option value='Cadiz'>Cadiz</option>
                                    <option value='Cagayan de Oro'>Cagayan de Oro</option>
                                    <option value='Calamba'>Calamba</option>
                                    <option value='Calapan'>Calapan</option>
                                    <option value='Calbayog'>Calbayog</option>
                                    <option value='Caloocan'>Caloocan</option>
                                    <option value='Candon'>Candon</option>
                                    <option value='Canlaon'>Canlaon</option>
                                    <option value='Carcar'>Carcar</option>
                                    <option value='Catbalogan'>Catbalogan</option>
                                    <option value='Cauayan'>Cauayan</option>
                                    <option value='Cavite City'>Cavite City</option>
                                    <option value='ManCebu Cityila'>Cebu City</option>
                                    <option value='Cotabato City'>Cotabato City</option>
                                    <option value='Dagupan'>Dagupan</option>
                                    <option value='Danao'>Danao</option>
                                    <option value='Dapitan'>Dapitan</option>
                                    <option value='Dasmariñas'>Dasmariñas</option>
                                    <option value='Davao City'>Davao City</option>
                                    <option value='Digos'>Digos</option>
                                    <option value='Dipolog'>Dipolog</option>
                                    <option value='Dumaguete'>Dumaguete</option>
                                    <option value='El Salvador'>El Salvador</option>
                                    <option value='Escalante'>Escalante</option>
                                    <option value='Gapan'>Gapan</option>
                                    <option value='General Santos'>General Santos</option>
                                    <option value='General Trias'>General Trias</option>
                                    <option value='Gingoog'>Gingoog</option>
                                    <option value='Guihulngan'>Guihulngan</option>
                                    <option value='Himamaylan'>Himamaylan</option>
                                    <option value='Ilagan'>Ilagan</option>
                                    <option value='Iligan'>Iligan</option>
                                    <option value='Iloilo City'>Iloilo City</option>
                                    <option value='Imus'>Imus</option>
                                    <option value='Iriga'>Iriga</option>
                                    <option value='Isabela'>Isabela</option>
                                    <option value='Kabankalan'>Kabankalan</option>
                                    <option value='Kidapawan'>Kidapawan</option>
                                    <option value='Koronadal'>Koronadal</option>
                                    <option value='La Carlota'>La Carlota</option>
                                    <option value='Lamitan'>Lamitan</option>
                                    <option value='Laoag'>Laoag</option>
                                    <option value='Lapu-Lapu'>Lapu-Lapu</option>
                                    <option value='Las Piñas'>Las Piñas</option>
                                    <option value='Legazpi'>Legazpi</option>
                                    <option value='Ligao'>Ligao</option>
                                    <option value='Lipa'>Lipa</option>
                                    <option value='Lucena'>Lucena</option>
                                    <option value='Maasin'>Maasin</option>
                                    <option value='Mabalacat'>Mabalacat</option>
                                    <option value='Makati'>Makati</option>
                                    <option value='Malabon'>Malabon</option>
                                    <option value='Malaybalay'>Malaybalay</option>
                                    <option value='Malolos'>Malolos</option>
                                    <option value='Mandaluyong'>Mandaluyong</option>
                                    <option value='Mandaue'>Mandaue</option>
                                    <option value='Manila'>Manila</option>
                                    <option value='Marawi'>Marawi</option>
                                    <option value='Marikina'>Marikina</option>
                                    <option value='Masbate City'>Masbate City</option>
                                    <option value='Mati'>Mati</option>
                                    <option value='Meycauayan'>Meycauayan</option>
                                    <option value='Muñoz'>Muñoz</option>
                                    <option value='Muntinlupa'>Muntinlupa</option>
                                    <option value='Naga'>Naga</option>
                                    <option value='Navotas'>Navotas</option>
                                    <option value='Olongapo'>Olongapo</option>
                                    <option value='Ormoc'>Ormoc</option>
                                    <option value='Oroquieta'>Oroquieta</option>
                                    <option value='Ozamiz'>Ozamiz</option>
                                    <option value='Pagadian'>Pagadian</option>
                                    <option value='Palayan'>Palayan</option>
                                    <option value='Panabo'>Panabo</option>
                                    <option value='Parañaque'>Parañaque</option>
                                    <option value='Pasay'>Pasay</option>
                                    <option value='Pasig'>Pasig</option>
                                    <option value='Passi'>Passi</option>
                                    <option value='Puerto Princesa'>Puerto Princesa</option>
                                    <option value='Quezon City'>Quezon City</option>
                                    <option value='Roxas'>Roxas</option>
                                    <option value='Sagay'>Sagay</option>
                                    <option value='Samal'>Samal</option>
                                    <option value='San Carlos'>San Carlos</option>
                                    <option value='San Carlos'>San Carlos</option>
                                    <option value='San Fernando'>San Fernando</option>
                                    <option value='San Fernando'>San Fernando</option>
                                    <option value='San Jose'>San Jose</option>
                                    <option value='San Jose del Monte'>San Jose del Monte</option>
                                    <option value='San Juan'>San Juan</option>
                                    <option value='San Pablo'>San Pablo</option>
                                    <option value='San Pedro'>San Pedro</option>
                                    <option value='Santa Rosa'>Santa Rosa</option>
                                    <option value='Santiago'>Santiago</option>
                                    <option value='Silay'>Silay</option>
                                    <option value='Sipalay'>Sipalay</option>
                                    <option value='Sorsogon City'>Sorsogon City</option>
                                    <option value='Surigao City'>Surigao City</option>
                                    <option value='Tabaco'>Tabaco</option>
                                    <option value='Tabuk'>Tabuk</option>
                                    <option value='Tacloban'>Tacloban</option>
                                    <option value='Tacurong'>Tacurong</option>
                                    <option value='Tagaytay'>Tagaytay</option>
                                    <option value='Tagbilaran'>Tagbilaran</option>
                                    <option value='Taguig'>Taguig</option>
                                    <option value='Tagum'>Tagum</option>
                                    <option value='Talisay'>Talisay</option>
                                    <option value='Tanauan'>Tanauan</option>
                                    <option value='Tandag'>Tandag</option>
                                    <option value='Tangub'>Tangub</option>
                                    <option value='Tanjay'>Tanjay</option>
                                    <option value='Tarlac City'>Tarlac City</option>
                                    <option value='Tayabas'>Tayabas</option>
                                    <option value='Toledo'>Toledo</option>
                                    <option value='Trece Martires'>Trece Martires</option>
                                    <option value='Tuguegarao'>Tuguegarao</option>
                                    <option value='Urdaneta'>Urdaneta</option>
                                    <option value='Valencia'>Valencia</option>
                                    <option value='Valenzuela'>Valenzuela</option>
                                    <option value='Victorias'>Victorias</option>
                                    <option value='Vigan'>Vigan</option>
                                    <option value='Zamboanga City'>Zamboanga City</option>
                                </select>
                                @if( $errors && $errors->has('wah')) <div class="error-label">{{ $errors->first('wah') }}</div> @endif
                            </div>
                        </div>
                        <div class="col-md-6 nopadding">
                            <div class="col-md-12 paddingb10 group-field">
                                <label>Company Name</label>
                                <input type="text" class="df-field" name="business_name" required>
                                @if( $errors && $errors->has('business_name')) <div class="error-label">{{ $errors->first('business_name') }}</div> @endif
                            </div>
                            <div class="col-md-12 paddingb10 group-field group-field">
                                <label>Industry</label>
                                <select class="df-field" name="business_type" required>
                                    <option value="">Select Industry</option>
                                    <option value="|1">Automotive Dealers and Manufacturers</option>
                                    <option value="|2">Automotive Services</option>
                                    <option value="|3">Entertainment</option>
                                    <option value="|4">Fitness</option>
                                    <option value="|5">Food and Beverage Establishment</option>
                                    <option value="|6">Fuel and Petroleum</option>
                                    <option value="|7">Grocery and Supermarket</option>
                                    <option value="|8">Grooming and Wellness</option>
                                    <option value="|9">Hospitals and Healthcare Services</option>
                                    <option value="|10">Insurance</option>
                                    <option value="|11">Petroleum</option>
                                    <option value="|12">Retail - Apparel</option>
                                    <option value="|18">Retail - Electronics</option>
                                    <option value="|13">Retail - Household and Hardware</option>
                                    <option value="|14">Retail - Sporting Goods</option>
                                    <option value="|15">Telecommunication and Utilities</option>
                                    <option value="|16">Transportation</option>
                                    <option value="|17">Travel and Leisure</option>
                                    <option value="|Others">Others</option>
                                </select>
                                @if( $errors && $errors->has('business_type')) <div class="error-label">{{ $errors->first('business_type') }}</div> @endif
                            </div>
                            <div class="col-md-12 paddingb10 group-field">
                                <label>Business Objective</label>
                                <select name="program_objective" class="df-field" required>
                                    <option value="">Select Objective</option>
                                    <option value="|1">Enhance customer engagement</option>
                                    <option value="|2">Improve customer retention</option>
                                    <option value="|3">Increase frequency of customer visits</option>
                                    <option value="|4">Drive revenue per transaction</option>
                                    <option value="|5">Gather insights and understand customer behavior</option>
                                    <option value="|6">Promote and push new products and services</option>
                                    <option value="|Others">Others</option>
                                </select>
                                @if( $errors && $errors->has('program_objective')) <div class="error-label">{{ $errors->first('program_objective') }}</div> @endif
                            </div>
                            <div class="col-md-12 paddingb20 group-field">
                                <div class="row">
                                    <div class="col-sm-6 paddingb10 branches">
                                        <label>No. of Branches</label>
                                        <input type="text" class="df-field" name="business_branches" required>
                                        @if( $errors && $errors->has('business_branches')) <div class="error-label">{{ $errors->first('business_branches') }}</div> @endif
                                    </div>
                                    <div class="col-sm-6 ave paddingb10">
                                        <label>Ave. Customer/Month</label>
                                        <input type="text" class="df-field" name="customer_no" required>
                                        @if( $errors && $errors->has('customer_no')) <div class="error-label">{{ $errors->first('customer_no') }}</div> @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 paddingb10 group-field">
                                <div class="g-recaptcha" data-sitekey="6LeSJSkTAAAAAJqYJJczKaKyhU6G_0DXkRsZM_1-"></div>
                                <small class="text-danger hidden hidden-text">Please verify captcha</small>
                                @if( $errors && $errors->has('g-recaptcha-response')) <div class="error-label">{{ $errors->first('g-recaptcha-response') }}</div> @endif
                            </div>
                            <input type="submit" class="hide" value="Submit" />
                        </div>
                    </form>
                    <div class="col-md-12 paddingb10 group-field text-center">
                        <br>
                        <input id="form_signup_button" type="submit" class="bt-bg continue" value="Continue" />
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    <!-- Continue to Select Package -->
    <div id="su-select-package" class="modal fade" role="dialog">
        <div class="modal-dialog su-packages">
            <div class="modal-content">
                <div class="modal-header aCenter">
                    <div type="button" class="close" data-dismiss="modal"><img src="app/page/onboarding/images/close-button-white.png"></div>
                    <img src="app/page/onboarding/images/multi-platform.png" class="img-reponsive">
                </div>
                <div class="modal-body aCenter">
                    <h3>Choose which loyalty program suits your business needs.</h3>
                </div>
                <div class="modal-footer aCenter">
                    <button type="button" class="bt-bg continue">Continue</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end: Continue to Select Package -->

    @if ( session('register_success') )
    <!-- Sign Up Success -->
    <div id="signup-success" class="modal fade" role="dialog" tabindex="-1">
        <div class="modal-dialog su-success">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header aCenter">
                    <img src="app/page/onboarding/images/success-icon.svg" class="img-responsive">
                </div>
                <div class="modal-body">
                    <h2>Congratulations!</h2>
                    <p>Your account details has been sent to your email</p>
                    <div class="su-sign-btn">
                        <button type="button" class="bt-bg continue" data-dismiss="modal" data-toggle="modal" data-target="#signin">Sign In</button>
                    </div>
                </div>
                <div class="modal-footer">
                    <em>If you don't see the account details email in your inbox, please check your Junk or Spam folders.</em>
                </div>
            </div>
        </div>
    </div>
    @endif

    @if ( session('forgotpass_success') )
    <!-- Forgotpassword Success -->
    <div id="signup-success" class="modal fade" role="dialog" tabindex="-1">
        <div class="modal-dialog su-success">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header aCenter">
                    <img src="app/page/onboarding/images/success-icon.svg" class="img-responsive">
                </div>
                <div class="modal-body">
                    <p>Please check your email for password reset instructions</p>
                </div>
                <div class="modal-footer">
                    <em>If you don't see the account details email in your inbox, please check your Junk or Spam folders.</em>
                </div>
            </div>
        </div>
    </div>
    @endif

    <section class="panel home" data-section-name="home">
        <nav class="navbar navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#defaultNavbar1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar top-bar"></span>
                        <span class="icon-bar middle-bar"></span>
                        <span class="icon-bar bottom-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ url('/') }}"><img src="app/page/onboarding/images/rush-logo.svg" width="100"></a>
                </div>
                <div class="collapse navbar-collapse" id="defaultNavbar1">
                    <div class="full-mobile">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="{{ url('about') }}">About Us</a></li>
                            <li><a href="{{ url('newsroom') }}">Newsroom</a></li>
                            <li><a href="{{ url('how-it-works') }}">How It Works</a></li>
                            <li><a href="{{ url('products') }}">Products</a></li>
                            <li><a href="{{ url('request-demo#request-demo-form') }}">Request Demo</a></li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <button type="button" class="no-bg-br sign-in" data-toggle="modal" data-target="#signin">Sign In</button>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <div class="full hp1">
            <div class="container">
                <div class="col-md-5 col-sm-7 hp1-content">
                    <div class="col-md-12 loyalty-vid wow fadeIn" data-wow-duration="1s" data-wow-delay=".4s">
                        <a href="#" data-toggle="modal" data-target="#loyalty"><img src="app/page/onboarding/images/play-button.svg"></a>
                    </div>
                    <div class="col-md-12 wow fadeIn" data-wow-duration="1s" data-wow-delay=".6s">
                        <h2>Loyalty Made Easy</h2></div>
                    <div class="col-md-12 wow fadeIn" data-wow-duration="1s" data-wow-delay=".8s">
                        <span class="hp1-i">Conveniently develop and deploy a custom-branded mobile app in just three days.</span>
                    </div>
                    <div class="col-md-12 bots wow fadeIn" data-wow-duration="1s" data-wow-delay="1s">
                        <button type="button" class="no-bg-br r-demo" onclick="window.location.href='{{ url('request-demo#request-demo-form') }}'">Request Demo</button>&nbsp;
                        <button type="button" class="bt-bg f-trial" data-toggle="modal" data-target="#signup">Start Free Trial</button>
                    </div>
                </div>
                <div class="col-md-7 col-sm-5 hp1-hero-img wow fadeIn" data-wow-duration="1s" data-wow-delay="1.2s">
                    <img src="app/page/onboarding/images/multi-platform.png" class="image-responsive">
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="ini-footer wow fadeIn" data-wow-duration="1s" data-wow-delay="1.4s">
            <div class="full">
                <div class="container">
                    <div class="partners-slider">
                        @if ($clientLogos)
                        @foreach ($clientLogos as $logo)
                            <div><img src="{{ url('page/onboarding-cms/images/partners/' . $logo->logo) }}"></div>
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="panel overview" data-section-name="overview">
        <div class="full hp2">
            <div class="container">
                <div class="col-md-offset-6 col-md-6 hp2-content">
                    <ul>
                        <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay=".2s">
                            <div class="col-md-2 col-xs-2 dm-icon">
                                <img src="app/page/onboarding/images/gear-icon.svg">
                            </div>
                            <div class="col-md-10 col-xs-10">
                                <h2>Quick and easy setup</h2>
                                <span>Start rewarding your customers with stamps or points in a jiff.</span>
                            </div>
                        </li>
                        <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay=".4s">
                            <div class="col-md-2 col-xs-2 dm-icon">
                                <img src="app/page/onboarding/images/star-icon.svg">
                            </div>
                            <div class="col-md-10 col-xs-10">
                                <h2>Highly-customizable reward system</h2>
                                <span>Create and update your reward mechanics depending on your business goals.</span>
                            </div>
                        </li>
                        <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay=".6s">
                            <div class="col-md-2 col-xs-2 dm-icon">
                                <img src="app/page/onboarding/images/layers-icon.svg">
                            </div>
                            <div class="col-md-10 col-xs-10">
                                <h2>Multiple in-store integrations</h2>
                                <span>Seamlessly integrate your Rush-powered loyalty program to your POS system via a widget or full-API integration.</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="panel options" data-section-name="options">
        <div class="full hp3">
            <div class="container">
                <div class="col-md-6 hp3-content">
                    <ul>
                        <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay=".2s">
                            <div class="col-md-2 col-xs-2 dm-icon">
                                <img src="app/page/onboarding/images/graph-icon.svg">
                            </div>
                            <div class="col-md-10 col-xs-10">
                                <h2>Real-time dashboard</h2>
                                <span>Capture customer data through their transactions and gain actionable insights.</span>
                            </div>
                        </li>
                        <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay=".4s">
                            <div class="col-md-2 col-xs-2 dm-icon">
                                <img src="app/page/onboarding/images/mega-phone-icon.svg">
                            </div>
                            <div class="col-md-10 col-xs-10">
                                <h2>Multi-channel broadcast tool</h2>
                                <span>Reach your customers via built-in marketing tools.</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="ini-footer2 wow fadeIn" data-wow-duration="1s" data-wow-delay=".2s">
                <div class="container">
                    <div class="col-md-7"><span class="l-label">Start your RUSH-powered loyalty program.</span></div>
                    <div class="col-md-5">
                        <button type="button" class="no-bg-brw r-demo" onclick="window.location.href='{{ url('request-demo#request-demo-form') }}'">Request Demo</button>&nbsp;
                        <button type="button" class="bt-bg f-trial" data-toggle="modal" data-target="#signup">Start Free Trial</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="footer-section" data-section-name="footer">
        <div class="footer wow fadeIn" data-wow-duration="1s" data-wow-delay=".2s">
            <div class="container">
                <div class="col-md-8">
                    <ul class="f-links col-md-6">
                        <li><a href="{{ url('terms-of-use') }}">Terms of use</a></li>
                        <li><a href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
                        <li><a href="{{ url('faqs') }}">FAQS</a></li>
                    </ul>
                    <ul class="s-icons col-md-6">
                        <li>
                            <a href="https://www.facebook.com/rushloyaltyh" target="_blank"><img src="app/page/onboarding/images/facebook-icon-footer.svg"></a>
                        </li>
                        <li>
                            <a href="https://twitter.com/rushrewardsph" target="_blank"><img src="app/page/onboarding/images/twitter-icon-footer.svg"></a>
                        </li>
                        <li>
                            <a href="https://www.linkedin.com/company/rushloyalty/" target="_blank"><img src="app/page/onboarding/images/linkedin-logo.png"></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 copyright">Copyright © RUSH 2017. All Rights Reserved</div>
            </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="app/page/onboarding/js/jquery-1.11.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="app/page/onboarding/js/bootstrap.js"></script>
    {{-- <script src="app/page/onboarding/scrollify/jquery-2.js"></script> --}}
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="app/page/onboarding/js/wow.min.js"></script>    
    <script src="app/page/onboarding/js/slick.js" type="text/javascript" charset="utf-8"></script>
    <script>
        $(window).load(function() {
            $('.loading').remove();
        });
        new WOW().init();
    </script>
    <script type="text/javascript">
        $(document).ready( function() {
            //Partners Slide
            $('.partners-slider').slick({
                arrows: true,
                nextArrow: '<i class="fa fa-angle-right partner-next"></i>',
                prevArrow: '<i class="fa fa-angle-left partner-prev"></i>',
                speed: 500,
                slidesToShow: 6,
                slidesToScroll: 6,
                autoplay: true,
                autoplaySpeed: 3000,
                cssEase: 'cubic-bezier(0.950, 0.050, 0.795, 0.035)',
                infinite: true,
                responsive: [
                    {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 5,
                        infinite: true,
                        dots: true
                    }
                    },
                    {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4
                    }
                    },
                    {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });
            if (window.innerWidth <= 768) {  $.scrollify.disable(); }

            if (window.location.hash) {
                var hash = window.location.hash;
                $(hash).modal('toggle');
            }

            $('#form_signin_button').on('click', function() {
                $.ajax({
                  type: "POST",
                  url: '{{ url('prelogin') }}',
                  dataType: "json",
                  data: $('#form_signin').serialize()
                }).done( function( response ) {
                    if ( response.login_status == "success" ) {
                        $('#form_signin').submit();
                    } else if( response.login_status == 'failed' ) {
                        $("#loginmessage").removeClass('hide').html( response.error_message )
                    } else {
                        $("#loginmessage").removeClass('hide').html( 'An error has occured. Please try again.' )
                    }
                });
                return false;  
            });

            $('#forgot_password_button').on('click', function() {
                if ( $('#form_signin_username').val() ) {
                    $.ajax({
                      type: "POST",
                      url: '{{ url('preforgot') }}',
                      dataType: "json",
                      data: $('#form_signin').serialize()
                    }).done( function( response ) {
                        if ( response.status == "success" ) {
                            $('<form method="post" id="forgot_pass_form" action="{{ url('forgot-password') }}"></form>').appendTo('body')
                            $('<input>').attr({
                                type: 'hidden',
                                name: 'email',
                                value: $('#form_signin_username').val()
                            }).appendTo('#forgot_pass_form');
                            $('<input>').attr({
                                type: 'hidden',
                                name: 'type',
                                value: response.type
                            }).appendTo('#forgot_pass_form');
                            $('#forgot_pass_form').submit();
                        } else if( response.status == 'failed' ) {
                            $("#loginmessage").removeClass('hide').html( response.message )
                        } else {
                            $("#loginmessage").removeClass('hide').html( 'An error has occured. Please try again.' )
                        }
                    });
                    return false;
                } else {
                    $("#loginmessage").removeClass('hide').html( 'Email is required.' );
                }
            });

            $('#form_signup_button').on('click', function() {
                $('.error-label').remove();
                $.ajax({
                  type: "POST",
                  url: '{{ url('presignup') }}',
                  dataType: "json",
                  data: $('#form_signup').serialize()
                }).done( function( response ) {
                    if ( response.signup_status == "success" ) {
                        $("#su-select-package").modal('show');
                        $("#signup").modal('hide');
                    } else if( response.signup_status == 'failed' ) {
                        $.each( response.errors , function( key, value ) {
                            $('input[name='+ key +'], select[name='+ key +']').parent().append('<div class="error-label">'+value+'</div>');
                        });
                        grecaptcha.reset();
                    } else {
                        $('<div class="error-label">An error has occured. Please try again.</div>').appendTo('#form_signup')
                    }
                });
                return false;  
            });

            @if ( session('register_success') )
                $('#signup-success').modal('show');
            @endif

            @if ( session('forgotpass_success') )
                $('#signup-success').modal('show');
            @endif

            $('#form_signup').on('keyup', 'input[name="person_contact"], input[name="business_branches"], input[name="customer_no"]', function () { 
                this.value = this.value.replace(/[^0-9\.]/g,'');
            });

            $('#form_signin').on('keypress', '#form_signin_password', function (e) {
              if (e.which == 13) {
                $("#form_signin_button").click();
                return false;
              }
            });

            $('#loyalty').on('hidden.bs.modal', function () {
                var vid = document.getElementById("homepage-video"); 
                vid.pause();
            });           

            $("#su-select-package, #signup-success").on("hidden.bs.modal", function() {
                $("body").css("padding-right", "initial");
            });

            $("#su-select-package button.continue").on("click", function() {
                $('#form_signup').submit();
            });
        });
    </script>
</body>

</html>
