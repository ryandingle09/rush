<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>RUSH</title>
    <link rel="stylesheet" href="app/page/onboarding/scrollify/main.css">
    <!-- Bootstrap -->
    <link href="app/page/onboarding/css/bootstrap.css" rel="stylesheet">
    <link href="app/page/onboarding/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,500,700" rel="stylesheet">
    <link rel="icon" href="app/page/onboarding/images/favicon.png" type="image/png">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
<script src="app/page/onboarding/https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="app/page/onboarding/https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
    @if(getenv('APP_ENV') == 'production')
    <script>
       (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
       (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
       m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
       })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

       ga('create', 'UA-87488508-1', 'auto');
       ga('send', 'pageview');
    </script>
    @endif
</head>

<body>
    <!-- Sign In -->
    <div id="signin" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal"><img src="app/page/onboarding/images/close-button.png"></div>
                    <h4 class="modal-title"><img src="app/page/onboarding/images/rush-logo.svg" width="170"></h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('login') }}" method="post" id="form_signin">
                        <input id="form_signin_username" type="text" class="df-field" placeholder="Email" name="email" required>
                        <input id="form_signin_password" type="password" class="df-field" placeholder="Password" name="password" required>
                        <div class="error-label hide" id="loginmessage"></div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button id="form_signin_button" type="submit" class="bt-bg continue">Continue</button>
                    <div class="forgot"><a id="forgot_password_button" style="cursor: pointer;">Forgot your password?</a></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Sign Up-->
    <div id="signup" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal"><img src="app/page/onboarding/images/close-button.png"></div>
                    <h4 class="modal-title">Sign Up</h4>
                </div>
                <div class="modal-body">
                    <form id="form_signup" action="{{ url('select-package') }}" method="post" data-toggle="validator">
                        <div class="col-md-6 nopadding">
                            <div class="col-md-12 paddingb10 group-field">
                                <label>Name</label>
                                <input type="text" class="df-field" name="person_name" required>
                                {{-- @if( $errors && $errors->has('person_name')) <div class="error-label">{{ $errors->first('person_name') }}</div> @endif --}}
                            </div>
                            <div class="col-md-12 paddingb10 group-field">
                                <label>Designation/Position</label>
                                <input type="text" class="df-field" name="person_position" required>
                                @if( $errors && $errors->has('person_position')) <div class="error-label">{{ $errors->first('person_position') }}</div> @endif
                            </div>
                            <div class="col-md-12 paddingb10 group-field">
                                <label>Mobile No.</label>
                                <input type="text" class="df-field" name="person_contact" required maxlength="11">
                                @if( $errors && $errors->has('person_contact')) <div class="error-label">{{ $errors->first('person_contact') }}</div> @endif
                            </div>
                            <div class="col-md-12 paddingb10 group-field">
                                <label>Email</label>
                                <input type="text" class="df-field" name="person_email" required>
                                @if( $errors && $errors->has('person_email')) <div class="error-label">{{ $errors->first('person_email') }}</div> @endif
                            </div>
                            <div class="col-md-12 paddingb10 group-field">
                                <label>Location</label>
                                <select name="business_location" class="df-field" required>
                                  <option value=''>Select location</option>
                                    <option value='Alaminos'>Alaminos</option>
                                    <option value='Angeles'>Angeles</option>
                                    <option value='Antipolo'>Antipolo</option>
                                    <option value='Bacolod'>Bacolod</option>
                                    <option value='Bacoor'>Bacoor</option>
                                    <option value='Bago'>Bago</option>
                                    <option value='Baguio'>Baguio</option>
                                    <option value='Bais'>Bais</option>
                                    <option value='Balanga'>Balanga</option>
                                    <option value='Batac'>Batac</option>
                                    <option value='Batangas City'>Batangas City</option>
                                    <option value='Bayawan'>Bayawan</option>
                                    <option value='Baybay'>Baybay</option>
                                    <option value='Bayugan'>Bayugan</option>
                                    <option value='Biñan'>Biñan</option>
                                    <option value='Bislig'>Bislig</option>
                                    <option value='Bogo'>Bogo</option>
                                    <option value='Borongan'>Borongan</option>
                                    <option value='Butuan'>Butuan</option>
                                    <option value='Cabadbaran'>Cabadbaran</option>
                                    <option value='Cabanatuan'>Cabanatuan</option>
                                    <option value='Cabuyao'>Cabuyao</option>
                                    <option value='Cadiz'>Cadiz</option>
                                    <option value='Cagayan de Oro'>Cagayan de Oro</option>
                                    <option value='Calamba'>Calamba</option>
                                    <option value='Calapan'>Calapan</option>
                                    <option value='Calbayog'>Calbayog</option>
                                    <option value='Caloocan'>Caloocan</option>
                                    <option value='Candon'>Candon</option>
                                    <option value='Canlaon'>Canlaon</option>
                                    <option value='Carcar'>Carcar</option>
                                    <option value='Catbalogan'>Catbalogan</option>
                                    <option value='Cauayan'>Cauayan</option>
                                    <option value='Cavite City'>Cavite City</option>
                                    <option value='ManCebu Cityila'>Cebu City</option>
                                    <option value='Cotabato City'>Cotabato City</option>
                                    <option value='Dagupan'>Dagupan</option>
                                    <option value='Danao'>Danao</option>
                                    <option value='Dapitan'>Dapitan</option>
                                    <option value='Dasmariñas'>Dasmariñas</option>
                                    <option value='Davao City'>Davao City</option>
                                    <option value='Digos'>Digos</option>
                                    <option value='Dipolog'>Dipolog</option>
                                    <option value='Dumaguete'>Dumaguete</option>
                                    <option value='El Salvador'>El Salvador</option>
                                    <option value='Escalante'>Escalante</option>
                                    <option value='Gapan'>Gapan</option>
                                    <option value='General Santos'>General Santos</option>
                                    <option value='General Trias'>General Trias</option>
                                    <option value='Gingoog'>Gingoog</option>
                                    <option value='Guihulngan'>Guihulngan</option>
                                    <option value='Himamaylan'>Himamaylan</option>
                                    <option value='Ilagan'>Ilagan</option>
                                    <option value='Iligan'>Iligan</option>
                                    <option value='Iloilo City'>Iloilo City</option>
                                    <option value='Imus'>Imus</option>
                                    <option value='Iriga'>Iriga</option>
                                    <option value='Isabela'>Isabela</option>
                                    <option value='Kabankalan'>Kabankalan</option>
                                    <option value='Kidapawan'>Kidapawan</option>
                                    <option value='Koronadal'>Koronadal</option>
                                    <option value='La Carlota'>La Carlota</option>
                                    <option value='Lamitan'>Lamitan</option>
                                    <option value='Laoag'>Laoag</option>
                                    <option value='Lapu-Lapu'>Lapu-Lapu</option>
                                    <option value='Las Piñas'>Las Piñas</option>
                                    <option value='Legazpi'>Legazpi</option>
                                    <option value='Ligao'>Ligao</option>
                                    <option value='Lipa'>Lipa</option>
                                    <option value='Lucena'>Lucena</option>
                                    <option value='Maasin'>Maasin</option>
                                    <option value='Mabalacat'>Mabalacat</option>
                                    <option value='Makati'>Makati</option>
                                    <option value='Malabon'>Malabon</option>
                                    <option value='Malaybalay'>Malaybalay</option>
                                    <option value='Malolos'>Malolos</option>
                                    <option value='Mandaluyong'>Mandaluyong</option>
                                    <option value='Mandaue'>Mandaue</option>
                                    <option value='Manila'>Manila</option>
                                    <option value='Marawi'>Marawi</option>
                                    <option value='Marikina'>Marikina</option>
                                    <option value='Masbate City'>Masbate City</option>
                                    <option value='Mati'>Mati</option>
                                    <option value='Meycauayan'>Meycauayan</option>
                                    <option value='Muñoz'>Muñoz</option>
                                    <option value='Muntinlupa'>Muntinlupa</option>
                                    <option value='Naga'>Naga</option>
                                    <option value='Navotas'>Navotas</option>
                                    <option value='Olongapo'>Olongapo</option>
                                    <option value='Ormoc'>Ormoc</option>
                                    <option value='Oroquieta'>Oroquieta</option>
                                    <option value='Ozamiz'>Ozamiz</option>
                                    <option value='Pagadian'>Pagadian</option>
                                    <option value='Palayan'>Palayan</option>
                                    <option value='Panabo'>Panabo</option>
                                    <option value='Parañaque'>Parañaque</option>
                                    <option value='Pasay'>Pasay</option>
                                    <option value='Pasig'>Pasig</option>
                                    <option value='Passi'>Passi</option>
                                    <option value='Puerto Princesa'>Puerto Princesa</option>
                                    <option value='Quezon City'>Quezon City</option>
                                    <option value='Roxas'>Roxas</option>
                                    <option value='Sagay'>Sagay</option>
                                    <option value='Samal'>Samal</option>
                                    <option value='San Carlos'>San Carlos</option>
                                    <option value='San Carlos'>San Carlos</option>
                                    <option value='San Fernando'>San Fernando</option>
                                    <option value='San Fernando'>San Fernando</option>
                                    <option value='San Jose'>San Jose</option>
                                    <option value='San Jose del Monte'>San Jose del Monte</option>
                                    <option value='San Juan'>San Juan</option>
                                    <option value='San Pablo'>San Pablo</option>
                                    <option value='San Pedro'>San Pedro</option>
                                    <option value='Santa Rosa'>Santa Rosa</option>
                                    <option value='Santiago'>Santiago</option>
                                    <option value='Silay'>Silay</option>
                                    <option value='Sipalay'>Sipalay</option>
                                    <option value='Sorsogon City'>Sorsogon City</option>
                                    <option value='Surigao City'>Surigao City</option>
                                    <option value='Tabaco'>Tabaco</option>
                                    <option value='Tabuk'>Tabuk</option>
                                    <option value='Tacloban'>Tacloban</option>
                                    <option value='Tacurong'>Tacurong</option>
                                    <option value='Tagaytay'>Tagaytay</option>
                                    <option value='Tagbilaran'>Tagbilaran</option>
                                    <option value='Taguig'>Taguig</option>
                                    <option value='Tagum'>Tagum</option>
                                    <option value='Talisay'>Talisay</option>
                                    <option value='Tanauan'>Tanauan</option>
                                    <option value='Tandag'>Tandag</option>
                                    <option value='Tangub'>Tangub</option>
                                    <option value='Tanjay'>Tanjay</option>
                                    <option value='Tarlac City'>Tarlac City</option>
                                    <option value='Tayabas'>Tayabas</option>
                                    <option value='Toledo'>Toledo</option>
                                    <option value='Trece Martires'>Trece Martires</option>
                                    <option value='Tuguegarao'>Tuguegarao</option>
                                    <option value='Urdaneta'>Urdaneta</option>
                                    <option value='Valencia'>Valencia</option>
                                    <option value='Valenzuela'>Valenzuela</option>
                                    <option value='Victorias'>Victorias</option>
                                    <option value='Vigan'>Vigan</option>
                                    <option value='Zamboanga City'>Zamboanga City</option>
                                </select>
                                @if( $errors && $errors->has('wah')) <div class="error-label">{{ $errors->first('wah') }}</div> @endif
                            </div>
                        </div>
                        <div class="col-md-6 nopadding">
                            <div class="col-md-12 paddingb10 group-field">
                                <label>Company Name</label>
                                <input type="text" class="df-field" name="business_name" required>
                                @if( $errors && $errors->has('business_name')) <div class="error-label">{{ $errors->first('business_name') }}</div> @endif
                            </div>
                            <div class="col-md-12 paddingb10 group-field group-field">
                                <label>Industry</label>
                                <select class="df-field" name="business_type" required>
                                    <option value="">Select Industry</option>
                                    <option value="|1">Automotive Dealers and Manufacturers</option>
                                    <option value="|2">Automotive Services</option>
                                    <option value="|3">Entertainment</option>
                                    <option value="|4">Fitness</option>
                                    <option value="|5">Food and Beverage Establishment</option>
                                    <option value="|6">Fuel and Petroleum</option>
                                    <option value="|7">Grocery and Supermarket</option>
                                    <option value="|8">Grooming and Wellness</option>
                                    <option value="|9">Hospitals and Healthcare Services</option>
                                    <option value="|10">Insurance</option>
                                    <option value="|11">Petroleum</option>
                                    <option value="|12">Retail - Apparel</option>
                                    <option value="|18">Retail - Electronics</option>
                                    <option value="|13">Retail - Household and Hardware</option>
                                    <option value="|14">Retail - Sporting Goods</option>
                                    <option value="|15">Telecommunication and Utilities</option>
                                    <option value="|16">Transportation</option>
                                    <option value="|17">Travel and Leisure</option>
                                    <option value="|Others">Others</option>
                                </select>
                                @if( $errors && $errors->has('business_type')) <div class="error-label">{{ $errors->first('business_type') }}</div> @endif
                            </div>
                            <div class="col-md-12 paddingb10 group-field">
                                <label>Business Objective</label>
                                <select name="program_objective" class="df-field" required>
                                    <option value="">Select Objective</option>
                                    <option value="|1">Enhance customer engagement</option>
                                    <option value="|2">Improve customer retention</option>
                                    <option value="|3">Increase frequency of customer visits</option>
                                    <option value="|4">Drive revenue per transaction</option>
                                    <option value="|5">Gather insights and understand customer behavior</option>
                                    <option value="|6">Promote and push new products and services</option>
                                    <option value="|Others">Others</option>
                                </select>
                                @if( $errors && $errors->has('program_objective')) <div class="error-label">{{ $errors->first('program_objective') }}</div> @endif
                            </div>
                            <div class="col-md-12 paddingb10 group-field">
                                <div class="col-xs-6 nopadding branches">
                                    <label>No. of Branches</label>
                                    <input type="text" class="df-field" name="business_branches" required>
                                    @if( $errors && $errors->has('business_branches')) <div class="error-label">{{ $errors->first('business_branches') }}</div> @endif
                                </div>
                                <div class="col-xs-6 nopadding ave  pull-right">
                                    <label>Ave. Customer/Month</label>
                                    <input type="text" class="df-field" name="customer_no" required>
                                    @if( $errors && $errors->has('customer_no')) <div class="error-label">{{ $errors->first('customer_no') }}</div> @endif
                                </div>
                            </div>
                            <div class="col-md-12 paddingb10 group-field text-center">
                                <div class="g-recaptcha" data-sitekey="6LeSJSkTAAAAAJqYJJczKaKyhU6G_0DXkRsZM_1-"></div>
                                <small class="text-danger hidden hidden-text">Please verify captcha</small>
                                @if( $errors && $errors->has('g-recaptcha-response')) <div class="error-label">{{ $errors->first('g-recaptcha-response') }}</div> @endif
                            </div>
                            <input type="submit" class="hide" value="Submit" />
                    </form>
                            <div class="col-md-12">
                                <br>
                                <input id="form_signup_button" type="submit" class="bt-bg continue" value="Continue" />
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    <!-- Continue to Select Package -->
    <div id="su-select-package" class="modal fade" role="dialog">
        <div class="modal-dialog su-packages">
            <div class="modal-content">
                <div class="modal-header aCenter">
                    <div type="button" class="close" data-dismiss="modal"><img src="app/page/onboarding/images/close-button-white.png"></div>
                    <img src="app/page/onboarding/images/multi-platform.png" class="img-reponsive">
                </div>
                <div class="modal-body aCenter">
                    <h3>Choose which loyalty program suits your business needs.</h3>
                </div>
                <div class="modal-footer aCenter">
                    <button type="button" class="bt-bg continue">Continue</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end: Continue to Select Package -->

    <section>
        <nav class="navbar navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#defaultNavbar1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar top-bar"></span>
                        <span class="icon-bar middle-bar"></span>
                        <span class="icon-bar bottom-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ url('/') }}"><img src="app/page/onboarding/images/rush-logo.svg" width="100"></a>
                </div>
                <div class="collapse navbar-collapse" id="defaultNavbar1">
                    <div class="full-mobile">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="{{ url('about') }}">About Us</a></li>
                            <li><a href="{{ url('newsroom') }}">Newsroom</a></li>
                            <li><a href="{{ url('how-it-works') }}">How It Works</a></li>
                            <li><a href="{{ url('products') }}">Products</a></li>
                            <li><a href="{{ url('request-demo#request-demo-form') }}">Request Demo</a></li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <button type="button" class="no-bg-br sign-in" data-toggle="modal" data-target="#signin">Sign In</button>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </section>
    <section>
        <div class="jumbotron info-page-head">
            <div class="container aCenter">
                <h2>Privacy Policy</h2>
            </div>
        </div>
        <div class="container info-page-body">
            <div class="col-md-12">
                <p>Welcome to Loyalty Program powered by RUSH ("RUSH," "we," "us," "our," or "service"). RUSH is a loyalty platform used by Merchants to enable a fun and easy way to collect Loyalty Points or Stamps and earn exciting rewards.</p>
                <p>Our Privacy Policy explains how we collect, use and protect information in relation to our mobile services, website, and/or any software provided on or in connection with RUSH services (collectively, the "Service").</p>
                <p>By continuing to use the Service, you signify that you have read, understand and agree to the collection and use of information in accordance with this Privacy Policy.</p>
                <p>This Privacy Policy applies to all visitors, users, and other persons who access the Service ("Users").</p>
            </div>
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#information">
                                <span class="glyphicon glyphicon-minus"></span>
                                Information We Collect
                            </a>
                        </h4>
                    </div>
                    <div id="information" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <p>We collect the following types of information:</p>
                            <p><strong>1.1.</strong> Information you provide us directly:</p>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td><strong>1.1.1.</strong> Your name, username, gender, date of birth, address, telephone or mobile number, email address, proof of identification and password when you register for a Loyalty Program account.</td>
                                </tr>
                                <tr>
                                    <td><strong>1.1.2.</strong> User Content (e.g., photos, comments, and other materials) that you post to the Service.</td>
                                </tr>
                            </table>
                            <p><strong>1.2.</strong> Communications between you and the Loyalty Program administrator or RUSH in relation to the Service such as, but not limited to service-related emails, SMS, or mobile application notifications (e.g., account verification, changes/updates to features of the Service, technical and security notices). You agree to receive service-related emails for as long as you are using the Service.</p>
                            <p><strong>1.3.</strong> Collecting Loyalty Points or Stamps</p>
                            <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td><strong>1.3.1.</strong> When you collect points or stamps from the Merchant, we store the amount purchased, merchant information, date and time. The information will be used to validate the point or stamp.</td>
                                </tr>
                            </table>
                            <p><strong>1.4.</strong> Referring Program and Transferring or Gifting Points or Rewards</p>
                            <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td><strong>1.4.1.</strong> You can opt to use the feature to refer the Loyalty Program or transfer or gift points and rewards to other persons either through (i) your contacts list, or (ii) third-party social media sites.</td>
                                </tr>
                                <tr>
                                    <td><strong>1.4.2.</strong> If you opt to find your friends through a third-party social media site, then you may be prompted to set up a link to a third-party service. You understand and agree that any information that any such third-party service provider may provide us will be governed by this Privacy Policy.</td>
                                </tr>
                                <tr>
                                    <td><strong>1.4.3.</strong> If you opt to invite someone to the Service through our "Invite friends" feature, you may select a person directly from the contacts list on your device and send a text or email from your personal account. You understand and agree that you are responsible for any charges that apply to communications sent from your device, and because this invitation is coming directly from your personal account, RUSH does not have access to or control this communication.</td>
                                </tr>
                            </table>
                            <p><strong>1.5.</strong> Analytics information:</p>
                            <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td><strong>1.5.1.</strong> We use analytics tools that collect information sent by your device to our servers , including the web pages you visit, add-ons, and other information to help us measure traffic and usage trends for the Service and to assist us in improving the Service. We collect and use this analytics information with analytics information of other Users so that it cannot reasonably be used to identify any particular individual User.</td>
                                </tr>
                            </table>
                            <p><strong>1.6.</strong> Cookies and similar technologies:</p>
                            <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td><strong>1.6.1.</strong> When you visit the Service, we may use cookies and similar technologies like pixels, web beacons, and local storage to collect information about how you use the Loyalty Program powered by RUSH and provide features to you.</td>
                                </tr>
                                <tr>
                                    <td><strong>1.6.2.</strong> You agree that we may ask advertisers or other partners to serve ads for services to your devices, which may use cookies or similar technologies placed by us.</td>
                                </tr>
                            </table>
                            <p><strong>1.7.</strong> Log file information</p>
                            <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td><strong>1.7.1.</strong> Log file information is automatically reported by your browser each time you make a request to access (i.e. visit) a RUSH web page or app. It can also be provided when the content of the RUSH webpage or app is downloaded to your browser or device.</td>
                                </tr>
                                <tr>
                                    <td><strong>1.7.2.</strong> When you use our Service, our servers automatically record certain log file information, including your web request, Internet Protocol ("IP") address, browser type, referring / exit pages and URLs, number of clicks and how you interact with links on the Service, domain names, landing pages, pages viewed, and other such information. We may also collect similar information from emails sent by us to our Users which then help us track which emails are opened and which links are clicked by recipients.</td>
                                </tr>
                            </table>
                            <p><strong>1.8.</strong> Device identifiers:</p>
                            <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td><strong>1.8.1.</strong> When you use a mobile device like a tablet or phone to access our Service, we may access, collect, monitor, store on your device, and/or remotely store one or more "device identifiers." Device identifiers are small data files or similar data structures stored on or associated with your mobile device, which uniquely identify your mobile device. A device identifier may be data stored in connection with the device hardware, data stored in connection with the device's operating system or other software, or data sent to the device by RUSH.</td>
                                </tr>
                                <tr>
                                    <td><strong>1.8.2.</strong> A device identifier may deliver information to us about how you browse and use the Service and may help us provide reports or personalized content and ads. Some features of the Service may not function properly if use or availability of device identifiers is impaired or disabled.</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#useofinfo">
                                <span class="glyphicon glyphicon-plus"></span>
                                How Do We Use Your Information
                            </a>
                        </h4>
                    </div>
                    <div id="useofinfo" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p><strong>2.1.</strong> In addition to some of the specific uses of information we describe in the immediately preceding provision, we may use information that we receive or collect to:</p>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td><strong>2.1.1.</strong> help you efficiently access your information after you sign in;</td>
                                </tr>
                                <tr>
                                    <td><strong>2.1.2.</strong> remember information, upon your instruction, so you will not have to re-enter it during your next use or visit of the Service;</td>
                                </tr>
                                <tr>
                                    <td><strong>2.1.3.</strong> validate the Loyalty Points or Stamps that you received based on the respective Merchant's Terms and Conditions;</td>
                                </tr>
                                <tr>
                                    <td><strong>2.1.4.</strong> provide personalized content and information, including online ads or other forms of marketing from Globe Telecom, Inc., its subsidiaries and affiliates, its Partners and Partner Merchants, to you;</td>
                                </tr>
                                <tr>
                                    <td><strong>2.1.5.</strong> provide, improve, test, and monitor the effectiveness of our Service;</td>
                                </tr>
                                <tr>
                                    <td><strong>2.1.6.</strong> develop and test new products, services and features;</td>
                                </tr>
                                <tr>
                                    <td><strong>2.1.7.</strong> monitor metrics such as total number of visitors, traffic, purchase frequency, and demographic patterns; and/or</td>
                                </tr>
                                <tr>
                                    <td><strong>2.1.8.</strong> diagnose or fix technology problems.</td>
                                </tr>
                            </table>
                            <p><strong>2.2.</strong> We will not disclose or sell your information to any third party outside Globe Telecom, Inc. (or the group of companies of which Globe Telecom, Inc. is a part of, including Globe Telecom, Inc., its subsidiaries and affiliates, its Partners and Partner Merchants) without your consent, except as otherwise provided in this Privacy Policy.</p>
                            <p><strong>2.3.</strong> The above notwithstanding, you hereby agree that:</p>
                            <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td><strong>2.3.1.</strong> We may share your information as well as information from tools like cookies, log files, and device identifiers and location data (such as usage data, referring/exit pages and URLs, platform types, number of clicks, etc.) with organizations that help us provide the Service to you ("Service Providers"). Our Service Providers will be given access to your information as is reasonably necessary to provide the Service under reasonable confidentiality terms.</td>
                                </tr>
                                <tr>
                                    <td><strong>2.3.2.</strong> We may also share certain information such as cookie data with third-party advertising partners to allow such third-party advertising networks to, among other things, deliver targeted advertisements that they believe will be of most interest to you.</td>
                                </tr>
                                <tr>
                                    <td><strong>2.3.3.</strong> We may remove parts of data that can identify you and share anonymized data with other parties. We may also combine your information with other information in a way that it is no longer associated with you and share that aggregated information.</td>
                                </tr>
                                <tr>
                                    <td><strong>2.3.4.</strong>We may share your information where we are required to do so by law or by judicial process or if we reasonably believe that such action is necessary to:</td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <table class="sub-sub" width="100%" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td><strong>a.</strong> comply with the law and the reasonable requests of law enforcement;</td>
                                            </tr>
                                            <tr>
                                                <td><strong>b.</strong> enforce our Terms of Use;</td>
                                            </tr>
                                            <tr>
                                                <td><strong>c.</strong> protect the security or integrity of our Service; and/or</td>
                                            </tr>
                                            <tr>
                                                <td><strong>d.</strong> exercise or protect the rights, property, or personal safety of RUSH, our Users or others.</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#usercontent">
                                <span class="glyphicon glyphicon-plus"></span>
                                User Content
                            </a>
                        </h4>
                    </div>
                    <div id="usercontent" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p><strong>3.1.</strong> Any information or content that you voluntarily disclose for posting to the Service, such as User Content, becomes available to the public, as controlled by any applicable privacy settings that you set. To change your privacy settings on the Service, please change your profile setting. Once you have shared User Content or made it public, that User Content may be re-shared by others.</p>
                            <p><strong>3.2.</strong> If you remove information that you posted to the Service, you understand that copies may still remain viewable in cached and archived pages of the Service, or if other Users or third parties using the RUSH API have copied or saved that information.</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#storeinfo">
                                <span class="glyphicon glyphicon-plus"></span>
                                How Do We Store Your Information
                            </a>
                        </h4>
                    </div>
                    <div id="storeinfo" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p><strong>4.1.</strong> Storage and Processing</p>
                            <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td><strong>4.1.1.</strong> Your information collected through the Service may be stored and processed in the Philippines or any other country in which Globe Telecom, Inc., a company in the same group of companies as Globe Telecom, Inc. or Service Providers maintain facilities.</td>
                                </tr>
                                <tr>
                                    <td><strong>4.1.2.</strong> RUSH may transfer information that we collect about you, including personal information across borders and from your country or jurisdiction to other countries or jurisdictions around the world. We may transfer information, including personal information, to a country and jurisdiction that does not have the same data protection laws as your jurisdiction.</td>
                                </tr>
                                <tr>
                                    <td><strong>4.1.3.</strong> By registering for and using the Service you consent to the transfer of information to the Philippines, or to any other country in which Globe Telecom, Inc., a company in the same group of companies as Globe Telecom, Inc. or Service Providers maintain facilities and the use and disclosure of information about you as described in this Privacy Policy.</td>
                                </tr>
                            </table>
                            <p><strong>4.2.</strong> We use commercially reasonable safeguards to help keep the information collected through the Service secure and take reasonable steps (such as requesting a unique password) to verify your identity before granting you access to your account. However, RUSH cannot ensure the security of any information you transmit to RUSH or guarantee that information on the Service may not be accessed, disclosed, altered, or destroyed.</p>
                            <p><strong>4.3.</strong> You are responsible for maintaining the secrecy of your unique password and account information, and for controlling access to emails, SMS, and app notifications between you, Merchant, and RUSH at all times. Your privacy settings may also be affected by changes the social media services you connect to RUSH make to their services. We are not responsible for the functionality, privacy, or security measures of any other organization.</p>
                            <p><strong>4.4.</strong> Following termination or deactivation of your account, RUSH may retain information (including your profile information) and User Content for a commercially reasonable time for backup, archival, legal and/or audit purposes.</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#infochoices">
                                <span class="glyphicon glyphicon-plus"></span>
                                Your Choices about Your Information
                            </a>
                        </h4>
                    </div>
                    <div id="infochoices" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p><strong>5.1.</strong> You may update your account information at any time by logging in and changing your profile/privacy settings. If you have any questions about reviewing or modifying your account information and/or profile/privacy settings, or the termination or deactivation of your account, you must contact the Merchant offering the Loyalty Program directly.</p>
                            <p><strong>5.2.</strong> You may unsubscribe from email or SMS communications from Merchant offering the Loyalty Program or RUSH by following the unsubscribe or opt-out instructions provided in such communications. You however agree that you may not opt out of Service-related communications (e.g., account verification, purchase and billing confirmations and reminders, changes/updates to features of the Service, technical and security notices) while you are using the Service.</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#childrensprivacy">
                                <span class="glyphicon glyphicon-plus"></span>
                                Children's Privacy
                            </a>
                        </h4>
                    </div>
                    <div id="childrensprivacy" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p>RUSH does not knowingly collect or solicit any information from anyone under the age of 18 or knowingly allow such persons to register for the Service. The Service and its content are not directed at children under the age of 18. In the event that we learn that we have collected personal information from a child under age 18 without parental consent, we will delete that information as quickly as possible. If you believe that we might have any information from or about a child under 18, please contact the Merchant offering the Loyalty Program directly.</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#otherweb">
                                <span class="glyphicon glyphicon-plus"></span>
                                Other Websites and Services
                            </a>
                        </h4>
                    </div>
                    <div id="otherweb" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p>We are not responsible for the practices employed by any websites or services linked to or from our Service, including the information or content contained within them. When you use a link to go from our Service to another website or service, our Privacy Policy does not apply to those third-party websites or services. Your browsing and interaction on any third-party website or service, including those that have a link on our website, are subject to that third party's own rules and policies. In addition, you agree that we are not responsible and do not have control over any third-party that you authorize to access your User Content. If you are using a third-party website or service and you allow them to access your User Content you do so at your own risk.</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#contact">
                                <span class="glyphicon glyphicon-plus"></span>
                                How to Contact Us
                            </a>
                        </h4>
                    </div>
                    <div id="contact" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p>If you have any questions about this Privacy Policy or the Service, please contact us at <a href="mailto:support@rush.ph">support@rush.ph</a> or 2119792.</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#policychanges">
                                <span class="glyphicon glyphicon-plus"></span>
                                Changes to Our Privacy Policy
                            </a>
                        </h4>
                    </div>
                    <div id="policychanges" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p><strong>9.1.</strong> RUSH may revise or modify this Privacy Policy from time to time. Said revisions or modifications shall be posted in Rush Website (rush.ph) and, once published therein, shall become binding on you. It shall be your obligation to be informed thereof by accessing, from time to time, such website where the latest version of this Privacy Policy may be found.</p>
                            <p><strong>9.2.</strong> The above notwithstanding, we may provide you additional forms of notice of revisions or modifications as appropriate under the circumstances. Your continued use of Loyalty Program powered by RUSH or the Service after any revisions or modification to this Privacy Policy will constitute your acceptance of such revision or modification.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="ini-footer2">
            <div class="container">
                <div class="col-md-7"><span class="l-label">Start your RUSH-powered loyalty program.</span></div>
                <div class="col-md-5">
                    <button type="button" class="no-bg-brw r-demo" onclick="window.location.href='{{ url('request-demo#request-demo-form') }}'">Request Demo</button>&nbsp;
                    <button type="button" class="bt-bg f-trial" data-toggle="modal" data-target="#signup">Start Free Trial</button>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="container">
                <div class="col-md-8">
                    <ul class="f-links col-md-6">
                        <li><a href="{{ url('terms-of-use') }}">Terms of use</a></li>
                        <li><a href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
                        <li><a href="{{ url('faqs') }}">FAQS</a></li>
                    </ul>
                    <ul class="s-icons col-md-6">
                        <li>
                            <a href="https://www.facebook.com/rushloyaltyh" target="_blank"><img src="app/page/onboarding/images/facebook-icon-footer.svg"></a>
                        </li>
                        <li>
                            <a href="https://twitter.com/rushrewardsph" target="_blank"><img src="app/page/onboarding/images/twitter-icon-footer.svg"></a>
                        </li>
                        <li>
                            <a href="https://www.linkedin.com/company/rushloyalty/" target="_blank"><img src="app/page/onboarding/images/linkedin-logo.png"></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 copyright">Copyright © RUSH 2017. All Rights Reserved</div>
            </div>
        </div>
    </section>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="app/page/onboarding/js/jquery-1.11.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="app/page/onboarding/js/bootstrap.js"></script>
    <script>
        // Accordion
        $('.collapse').on('shown.bs.collapse', function() {
            $(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
        }).on('hidden.bs.collapse', function() {
            $(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
        });
    </script>

    <script type="text/javascript">
        $(document).ready( function() {

            $('#form_signin_button').on('click', function() {
                $.ajax({
                  type: "POST",
                  url: '{{ url('prelogin') }}',
                  dataType: "json",
                  data: $('#form_signin').serialize()
                }).done( function( response ) {
                    if ( response.login_status == "success" ) {
                        $('#form_signin').submit();
                    } else if( response.login_status == 'failed' ) {
                        $("#loginmessage").removeClass('hide').html( response.error_message )
                    } else {
                        $("#loginmessage").removeClass('hide').html( 'An error has occured. Please try again.' )
                    }
                });
                return false;  
            });

            $('#forgot_password_button').on('click', function() {
                if ( $('#form_signin_username').val() ) {
                    $.ajax({
                      type: "POST",
                      url: '{{ url('preforgot') }}',
                      dataType: "json",
                      data: $('#form_signin').serialize()
                    }).done( function( response ) {
                        if ( response.status == "success" ) {
                            $('<form method="post" id="forgot_pass_form" action="{{ url('forgot-password') }}"></form>').appendTo('body')
                            $('<input>').attr({
                                type: 'hidden',
                                name: 'email',
                                value: $('#form_signin_username').val()
                            }).appendTo('#forgot_pass_form');
                            $('<input>').attr({
                                type: 'hidden',
                                name: 'type',
                                value: response.type
                            }).appendTo('#forgot_pass_form');
                            $('#forgot_pass_form').submit();
                        } else if( response.status == 'failed' ) {
                            $("#loginmessage").removeClass('hide').html( response.message )
                        } else {
                            $("#loginmessage").removeClass('hide').html( 'An error has occured. Please try again.' )
                        }
                    });
                    return false;
                } else {
                    $("#loginmessage").removeClass('hide').html( 'Email is required.' );
                }
            });

            $('#form_signup_button').on('click', function() {
                $('.error-label').remove();
                $.ajax({
                  type: "POST",
                  url: '{{ url('presignup') }}',
                  dataType: "json",
                  data: $('#form_signup').serialize()
                }).done( function( response ) {
                    if ( response.signup_status == "success" ) {
                        $("#su-select-package").modal('show');
                        $("#signup").modal('hide');
                    } else if( response.signup_status == 'failed' ) {
                        $.each( response.errors , function( key, value ) {
                            $('input[name='+ key +'], select[name='+ key +']').parent().append('<div class="error-label">'+value+'</div>');
                        });
                        grecaptcha.reset();
                    } else {
                        $('<div class="error-label">An error has occured. Please try again.</div>').appendTo('#form_signup')
                    }
                });
                return false;  
            });

            $('#form_signup').on('keyup', 'input[name="person_contact"], input[name="business_branches"], input[name="customer_no"]', function () { 
                this.value = this.value.replace(/[^0-9\.]/g,'');
            });

            $('#form_signin').on('keypress', '#form_signin_password', function (e) {
              if (e.which == 13) {
                $("#form_signin_button").click();
                return false;
              }
            });

            $("#su-select-package").on("hidden.bs.modal", function(){
                $("body").css("padding-right", "initial");
            });

            $("#su-select-package button.continue").on("click", function() {
                $('#form_signup').submit();
            });
            
        });
    </script>
</body>

</html>
