<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>RUSH</title>
    <link rel="stylesheet" href="app/page/onboarding/scrollify/main.css">
    <!-- Bootstrap -->
    <link href="app/page/onboarding/css/bootstrap.css" rel="stylesheet">
    <link href="app/page/onboarding/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700" rel="stylesheet">
    <link rel="icon" href="app/page/onboarding/images/favicon.png" type="image/png">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
    @if(getenv('APP_ENV') == 'production')
    <script>
       (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
       (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
       m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
       })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

       ga('create', 'UA-87488508-1', 'auto');
       ga('send', 'pageview');
    </script>
    @endif
</head>

<body>
    <!-- Sign In -->
    <div id="signin" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal"><img src="app/page/onboarding/images/close-button.png"></div>
                    <h4 class="modal-title"><img src="app/page/onboarding/images/rush-logo.svg" width="170"></h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('login') }}" method="post" id="form_signin">
                        <input id="form_signin_username" type="text" class="df-field" placeholder="Email" name="email" required>
                        <input id="form_signin_password" type="password" class="df-field" placeholder="Password" name="password" required>
                        <div class="error-label hide" id="loginmessage"></div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button id="form_signin_button" type="submit" class="bt-bg continue">Continue</button>
                    <div class="forgot"><a id="forgot_password_button" style="cursor: pointer;">Forgot your password?</a></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Sign Up-->
    <div id="signup" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal"><img src="app/page/onboarding/images/close-button.png"></div>
                    <h4 class="modal-title">Sign Up</h4>
                </div>
                <div class="modal-body">
                    <form id="form_signup" action="{{ url('select-package') }}" method="post" data-toggle="validator">
                        <div class="col-md-6 nopadding">
                            <div class="col-md-12 paddingb10 group-field">
                                <label>Name</label>
                                <input type="text" class="df-field" name="person_name" required>
                                {{-- @if( $errors && $errors->has('person_name')) <div class="error-label">{{ $errors->first('person_name') }}</div> @endif --}}
                            </div>
                            <div class="col-md-12 paddingb10 group-field">
                                <label>Designation/Position</label>
                                <input type="text" class="df-field" name="person_position" required>
                                @if( $errors && $errors->has('person_position')) <div class="error-label">{{ $errors->first('person_position') }}</div> @endif
                            </div>
                            <div class="col-md-12 paddingb10 group-field">
                                <label>Mobile No.</label>
                                <input type="text" class="df-field" name="person_contact" required maxlength="11">
                                @if( $errors && $errors->has('person_contact')) <div class="error-label">{{ $errors->first('person_contact') }}</div> @endif
                            </div>
                            <div class="col-md-12 paddingb10 group-field">
                                <label>Email</label>
                                <input type="text" class="df-field" name="person_email" required>
                                @if( $errors && $errors->has('person_email')) <div class="error-label">{{ $errors->first('person_email') }}</div> @endif
                            </div>
                            <div class="col-md-12 paddingb10 group-field">
                                <label>Location</label>
                                <select name="business_location" class="df-field" required>
                                  <option value=''>Select location</option>
                                    <option value='Alaminos'>Alaminos</option>
                                    <option value='Angeles'>Angeles</option>
                                    <option value='Antipolo'>Antipolo</option>
                                    <option value='Bacolod'>Bacolod</option>
                                    <option value='Bacoor'>Bacoor</option>
                                    <option value='Bago'>Bago</option>
                                    <option value='Baguio'>Baguio</option>
                                    <option value='Bais'>Bais</option>
                                    <option value='Balanga'>Balanga</option>
                                    <option value='Batac'>Batac</option>
                                    <option value='Batangas City'>Batangas City</option>
                                    <option value='Bayawan'>Bayawan</option>
                                    <option value='Baybay'>Baybay</option>
                                    <option value='Bayugan'>Bayugan</option>
                                    <option value='Biñan'>Biñan</option>
                                    <option value='Bislig'>Bislig</option>
                                    <option value='Bogo'>Bogo</option>
                                    <option value='Borongan'>Borongan</option>
                                    <option value='Butuan'>Butuan</option>
                                    <option value='Cabadbaran'>Cabadbaran</option>
                                    <option value='Cabanatuan'>Cabanatuan</option>
                                    <option value='Cabuyao'>Cabuyao</option>
                                    <option value='Cadiz'>Cadiz</option>
                                    <option value='Cagayan de Oro'>Cagayan de Oro</option>
                                    <option value='Calamba'>Calamba</option>
                                    <option value='Calapan'>Calapan</option>
                                    <option value='Calbayog'>Calbayog</option>
                                    <option value='Caloocan'>Caloocan</option>
                                    <option value='Candon'>Candon</option>
                                    <option value='Canlaon'>Canlaon</option>
                                    <option value='Carcar'>Carcar</option>
                                    <option value='Catbalogan'>Catbalogan</option>
                                    <option value='Cauayan'>Cauayan</option>
                                    <option value='Cavite City'>Cavite City</option>
                                    <option value='ManCebu Cityila'>Cebu City</option>
                                    <option value='Cotabato City'>Cotabato City</option>
                                    <option value='Dagupan'>Dagupan</option>
                                    <option value='Danao'>Danao</option>
                                    <option value='Dapitan'>Dapitan</option>
                                    <option value='Dasmariñas'>Dasmariñas</option>
                                    <option value='Davao City'>Davao City</option>
                                    <option value='Digos'>Digos</option>
                                    <option value='Dipolog'>Dipolog</option>
                                    <option value='Dumaguete'>Dumaguete</option>
                                    <option value='El Salvador'>El Salvador</option>
                                    <option value='Escalante'>Escalante</option>
                                    <option value='Gapan'>Gapan</option>
                                    <option value='General Santos'>General Santos</option>
                                    <option value='General Trias'>General Trias</option>
                                    <option value='Gingoog'>Gingoog</option>
                                    <option value='Guihulngan'>Guihulngan</option>
                                    <option value='Himamaylan'>Himamaylan</option>
                                    <option value='Ilagan'>Ilagan</option>
                                    <option value='Iligan'>Iligan</option>
                                    <option value='Iloilo City'>Iloilo City</option>
                                    <option value='Imus'>Imus</option>
                                    <option value='Iriga'>Iriga</option>
                                    <option value='Isabela'>Isabela</option>
                                    <option value='Kabankalan'>Kabankalan</option>
                                    <option value='Kidapawan'>Kidapawan</option>
                                    <option value='Koronadal'>Koronadal</option>
                                    <option value='La Carlota'>La Carlota</option>
                                    <option value='Lamitan'>Lamitan</option>
                                    <option value='Laoag'>Laoag</option>
                                    <option value='Lapu-Lapu'>Lapu-Lapu</option>
                                    <option value='Las Piñas'>Las Piñas</option>
                                    <option value='Legazpi'>Legazpi</option>
                                    <option value='Ligao'>Ligao</option>
                                    <option value='Lipa'>Lipa</option>
                                    <option value='Lucena'>Lucena</option>
                                    <option value='Maasin'>Maasin</option>
                                    <option value='Mabalacat'>Mabalacat</option>
                                    <option value='Makati'>Makati</option>
                                    <option value='Malabon'>Malabon</option>
                                    <option value='Malaybalay'>Malaybalay</option>
                                    <option value='Malolos'>Malolos</option>
                                    <option value='Mandaluyong'>Mandaluyong</option>
                                    <option value='Mandaue'>Mandaue</option>
                                    <option value='Manila'>Manila</option>
                                    <option value='Marawi'>Marawi</option>
                                    <option value='Marikina'>Marikina</option>
                                    <option value='Masbate City'>Masbate City</option>
                                    <option value='Mati'>Mati</option>
                                    <option value='Meycauayan'>Meycauayan</option>
                                    <option value='Muñoz'>Muñoz</option>
                                    <option value='Muntinlupa'>Muntinlupa</option>
                                    <option value='Naga'>Naga</option>
                                    <option value='Navotas'>Navotas</option>
                                    <option value='Olongapo'>Olongapo</option>
                                    <option value='Ormoc'>Ormoc</option>
                                    <option value='Oroquieta'>Oroquieta</option>
                                    <option value='Ozamiz'>Ozamiz</option>
                                    <option value='Pagadian'>Pagadian</option>
                                    <option value='Palayan'>Palayan</option>
                                    <option value='Panabo'>Panabo</option>
                                    <option value='Parañaque'>Parañaque</option>
                                    <option value='Pasay'>Pasay</option>
                                    <option value='Pasig'>Pasig</option>
                                    <option value='Passi'>Passi</option>
                                    <option value='Puerto Princesa'>Puerto Princesa</option>
                                    <option value='Quezon City'>Quezon City</option>
                                    <option value='Roxas'>Roxas</option>
                                    <option value='Sagay'>Sagay</option>
                                    <option value='Samal'>Samal</option>
                                    <option value='San Carlos'>San Carlos</option>
                                    <option value='San Carlos'>San Carlos</option>
                                    <option value='San Fernando'>San Fernando</option>
                                    <option value='San Fernando'>San Fernando</option>
                                    <option value='San Jose'>San Jose</option>
                                    <option value='San Jose del Monte'>San Jose del Monte</option>
                                    <option value='San Juan'>San Juan</option>
                                    <option value='San Pablo'>San Pablo</option>
                                    <option value='San Pedro'>San Pedro</option>
                                    <option value='Santa Rosa'>Santa Rosa</option>
                                    <option value='Santiago'>Santiago</option>
                                    <option value='Silay'>Silay</option>
                                    <option value='Sipalay'>Sipalay</option>
                                    <option value='Sorsogon City'>Sorsogon City</option>
                                    <option value='Surigao City'>Surigao City</option>
                                    <option value='Tabaco'>Tabaco</option>
                                    <option value='Tabuk'>Tabuk</option>
                                    <option value='Tacloban'>Tacloban</option>
                                    <option value='Tacurong'>Tacurong</option>
                                    <option value='Tagaytay'>Tagaytay</option>
                                    <option value='Tagbilaran'>Tagbilaran</option>
                                    <option value='Taguig'>Taguig</option>
                                    <option value='Tagum'>Tagum</option>
                                    <option value='Talisay'>Talisay</option>
                                    <option value='Tanauan'>Tanauan</option>
                                    <option value='Tandag'>Tandag</option>
                                    <option value='Tangub'>Tangub</option>
                                    <option value='Tanjay'>Tanjay</option>
                                    <option value='Tarlac City'>Tarlac City</option>
                                    <option value='Tayabas'>Tayabas</option>
                                    <option value='Toledo'>Toledo</option>
                                    <option value='Trece Martires'>Trece Martires</option>
                                    <option value='Tuguegarao'>Tuguegarao</option>
                                    <option value='Urdaneta'>Urdaneta</option>
                                    <option value='Valencia'>Valencia</option>
                                    <option value='Valenzuela'>Valenzuela</option>
                                    <option value='Victorias'>Victorias</option>
                                    <option value='Vigan'>Vigan</option>
                                    <option value='Zamboanga City'>Zamboanga City</option>
                                </select>
                                @if( $errors && $errors->has('wah')) <div class="error-label">{{ $errors->first('wah') }}</div> @endif
                            </div>
                        </div>
                        <div class="col-md-6 nopadding">
                            <div class="col-md-12 paddingb10 group-field">
                                <label>Company Name</label>
                                <input type="text" class="df-field" name="business_name" required>
                                @if( $errors && $errors->has('business_name')) <div class="error-label">{{ $errors->first('business_name') }}</div> @endif
                            </div>
                            <div class="col-md-12 paddingb10 group-field group-field">
                                <label>Industry</label>
                                <select class="df-field" name="business_type" required>
                                    <option value="">Select Industry</option>
                                    <option value="|1">Automotive Dealers and Manufacturers</option>
                                    <option value="|2">Automotive Services</option>
                                    <option value="|3">Entertainment</option>
                                    <option value="|4">Fitness</option>
                                    <option value="|5">Food and Beverage Establishment</option>
                                    <option value="|6">Fuel and Petroleum</option>
                                    <option value="|7">Grocery and Supermarket</option>
                                    <option value="|8">Grooming and Wellness</option>
                                    <option value="|9">Hospitals and Healthcare Services</option>
                                    <option value="|10">Insurance</option>
                                    <option value="|11">Petroleum</option>
                                    <option value="|12">Retail - Apparel</option>
                                    <option value="|18">Retail - Electronics</option>
                                    <option value="|13">Retail - Household and Hardware</option>
                                    <option value="|14">Retail - Sporting Goods</option>
                                    <option value="|15">Telecommunication and Utilities</option>
                                    <option value="|16">Transportation</option>
                                    <option value="|17">Travel and Leisure</option>
                                    <option value="|Others">Others</option>
                                </select>
                                @if( $errors && $errors->has('business_type')) <div class="error-label">{{ $errors->first('business_type') }}</div> @endif
                            </div>
                            <div class="col-md-12 paddingb10 group-field">
                                <label>Business Objective</label>
                                <select name="program_objective" class="df-field" required>
                                    <option value="">Select Objective</option>
                                    <option value="|1">Enhance customer engagement</option>
                                    <option value="|2">Improve customer retention</option>
                                    <option value="|3">Increase frequency of customer visits</option>
                                    <option value="|4">Drive revenue per transaction</option>
                                    <option value="|5">Gather insights and understand customer behavior</option>
                                    <option value="|6">Promote and push new products and services</option>
                                    <option value="|Others">Others</option>
                                </select>
                                @if( $errors && $errors->has('program_objective')) <div class="error-label">{{ $errors->first('program_objective') }}</div> @endif
                            </div>
                            <div class="col-md-12 paddingb10 group-field">
                                <div class="col-xs-6 nopadding branches">
                                    <label>No. of Branches</label>
                                    <input type="text" class="df-field" name="business_branches" required>
                                    @if( $errors && $errors->has('business_branches')) <div class="error-label">{{ $errors->first('business_branches') }}</div> @endif
                                </div>
                                <div class="col-xs-6 nopadding ave  pull-right">
                                    <label>Ave. Customer/Month</label>
                                    <input type="text" class="df-field" name="customer_no" required>
                                    @if( $errors && $errors->has('customer_no')) <div class="error-label">{{ $errors->first('customer_no') }}</div> @endif
                                </div>
                            </div>
                            <div class="col-md-12 paddingb10 group-field text-center">
                                <div class="g-recaptcha" data-sitekey="6LeSJSkTAAAAAJqYJJczKaKyhU6G_0DXkRsZM_1-"></div>
                                <small class="text-danger hidden hidden-text">Please verify captcha</small>
                                @if( $errors && $errors->has('g-recaptcha-response')) <div class="error-label">{{ $errors->first('g-recaptcha-response') }}</div> @endif
                            </div>
                            <input type="submit" class="hide" value="Submit" />
                    </form>
                            <div class="col-md-12">
                                <br>
                                <input id="form_signup_button" type="submit" class="bt-bg continue" value="Continue" />
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    <!-- Continue to Select Package -->
    <div id="su-select-package" class="modal fade" role="dialog">
        <div class="modal-dialog su-packages">
            <div class="modal-content">
                <div class="modal-header aCenter">
                    <div type="button" class="close" data-dismiss="modal"><img src="app/page/onboarding/images/close-button-white.png"></div>
                    <img src="app/page/onboarding/images/multi-platform.png" class="img-reponsive">
                </div>
                <div class="modal-body aCenter">
                    <h3>Choose which loyalty program suits your business needs.</h3>
                </div>
                <div class="modal-footer aCenter">
                    <button type="button" class="bt-bg continue">Continue</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end: Continue to Select Package -->

    <section>
        <nav class="navbar navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#defaultNavbar1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar top-bar"></span>
                        <span class="icon-bar middle-bar"></span>
                        <span class="icon-bar bottom-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ url('/') }}"><img src="app/page/onboarding/images/rush-logo.svg" width="100"></a>
                </div>
                <div class="collapse navbar-collapse" id="defaultNavbar1">
                    <div class="full-mobile">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="{{ url('about') }}">About Us</a></li>
                            <li><a href="{{ url('newsroom') }}">Newsroom</a></li>
                            <li><a href="{{ url('how-it-works') }}">How It Works</a></li>
                            <li><a href="{{ url('products') }}">Products</a></li>
                            <li><a href="{{ url('request-demo#request-demo-form') }}">Request Demo</a></li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <button type="button" class="no-bg-br sign-in" data-toggle="modal" data-target="#signin">Sign In</button>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </section>
    <section class="signup_success">
       <span class="signup_check">
            <img src="{{ url('app/page/onboarding/images/success-icon.svg') }}" alt="" class="img-responsive">
       </span>

       <h1>Thank you for your interest with RUSH!</h1>

       <p>Our loyalty experts will send your login credentials to your email.</p>

       <a href="{{ url('/') }}" class="bt-bg back_home">Back to Home</a>
    </section>
    <section class="newsroom more-news signup_morenews">
        <div class="container">
            <div class="col-md-12">
                <h2 class="more-news-title">Explore More</h2>
            </div>
            @if ($more_news)
                @foreach ($more_news as $news)
                    <div class="col-md-4 mr-news-item">
                        <div class="item-content-header" style="background: url('{{ url('app/page/onboarding-cms/images/newsroom/' . $news->banner_image) }}');"></div>
                        <div class="item-content-body">
                            <span class="title-small">{{ $news->merchant_name }}</span>
                            <a href="{{ url( 'newsroom/' . $news->url ) }}">
                                <h4>{{ $news->title }}</h4>
                            </a>
                            <span class="date-small">{{ $news->publish_date_label }} | Philippines</span>
                        </div>
                    </div>
                @endforeach
            @endif
            <div class="clearfix"></div>
        </div>
    </section>
    <div class="footer-section">
        <div class="ini-footer2 wow fadeIn" data-wow-duration="1s" data-wow-delay=".2s">
            <div class="container">
                <div class="col-md-7"><span class="l-label">Start your RUSH-powered loyalty program.</span></div>
                <div class="col-md-5">
                    <button type="button" class="no-bg-brw r-demo" onclick="window.location.href='{{ url('request-demo#request-demo-form') }}'">Request Demo</button>&nbsp;
                    <button type="button" class="bt-bg f-trial" data-toggle="modal" data-target="#signup">Start Free Trial</button>
                </div>
            </div>
        </div>
        <div class="footer wow fadeIn" data-wow-duration="1s" data-wow-delay=".2s">
            <div class="container">
                <div class="col-md-8">
                    <ul class="f-links col-md-6">
                        <li><a href="{{ url('terms-of-use') }}">Terms of use</a></li>
                        <li><a href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
                        <li><a href="{{ url('faqs') }}">FAQS</a></li>
                    </ul>
                    <ul class="s-icons col-md-6">
                        <li>
                            <a href="https://www.facebook.com/rushloyaltyh" target="_blank"><img src="app/page/onboarding/images/facebook-icon-footer.svg"></a>
                        </li>
                        <li>
                            <a href="https://twitter.com/rushrewardsph" target="_blank"><img src="app/page/onboarding/images/twitter-icon-footer.svg"></a>
                        </li>
                        <li>
                            <a href="https://www.linkedin.com/company/rushloyalty/" target="_blank"><img src="app/page/onboarding/images/linkedin-logo.png"></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 copyright">Copyright © RUSH 2017. All Rights Reserved</div>
            </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="app/page/onboarding/js/jquery-1.11.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="app/page/onboarding/js/bootstrap.js"></script>
    <script src="app/page/onboarding/js/masonry.pkgd.min.js"></script>

    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script type="text/javascript">
        $(document).ready( function() {

            $('#form_signin_button').on('click', function() {
                $.ajax({
                  type: "POST",
                  url: '{{ url('prelogin') }}',
                  dataType: "json",
                  data: $('#form_signin').serialize()
                }).done( function( response ) {
                    if ( response.login_status == "success" ) {
                        $('#form_signin').submit();
                    } else if( response.login_status == 'failed' ) {
                        $("#loginmessage").removeClass('hide').html( response.error_message )
                    } else {
                        $("#loginmessage").removeClass('hide').html( 'An error has occured. Please try again.' )
                    }
                });
                return false;  
            });

            $('#forgot_password_button').on('click', function() {
                if ( $('#form_signin_username').val() ) {
                    $.ajax({
                      type: "POST",
                      url: '{{ url('preforgot') }}',
                      dataType: "json",
                      data: $('#form_signin').serialize()
                    }).done( function( response ) {
                        if ( response.status == "success" ) {
                            $('<form method="post" id="forgot_pass_form" action="{{ url('forgot-password') }}"></form>').appendTo('body')
                            $('<input>').attr({
                                type: 'hidden',
                                name: 'email',
                                value: $('#form_signin_username').val()
                            }).appendTo('#forgot_pass_form');
                            $('<input>').attr({
                                type: 'hidden',
                                name: 'type',
                                value: response.type
                            }).appendTo('#forgot_pass_form');
                            $('#forgot_pass_form').submit();
                        } else if( response.status == 'failed' ) {
                            $("#loginmessage").removeClass('hide').html( response.message )
                        } else {
                            $("#loginmessage").removeClass('hide').html( 'An error has occured. Please try again.' )
                        }
                    });
                    return false;
                } else {
                    $("#loginmessage").removeClass('hide').html( 'Email is required.' );
                }
            });

            $('#form_signup_button').on('click', function() {
                $('.error-label').remove();
                $.ajax({
                  type: "POST",
                  url: '{{ url('presignup') }}',
                  dataType: "json",
                  data: $('#form_signup').serialize()
                }).done( function( response ) {
                    if ( response.signup_status == "success" ) {
                        $("#su-select-package").modal('show');
                        $("#signup").modal('hide');
                    } else if( response.signup_status == 'failed' ) {
                        $.each( response.errors , function( key, value ) {
                            $('input[name='+ key +'], select[name='+ key +']').parent().append('<div class="error-label">'+value+'</div>');
                        });
                        grecaptcha.reset();
                    } else {
                        $('<div class="error-label">An error has occured. Please try again.</div>').appendTo('#form_signup')
                    }
                });
                return false;  
            });

            $('#form_signup').on('keyup', 'input[name="person_contact"], input[name="business_branches"], input[name="customer_no"]', function () { 
                this.value = this.value.replace(/[^0-9\.]/g,'');
            });

            $('#form_signin').on('keypress', '#form_signin_password', function (e) {
              if (e.which == 13) {
                $("#form_signin_button").click();
                return false;
              }
            });

            $("#su-select-package").on("hidden.bs.modal", function(){
                $("body").css("padding-right", "initial");
            });

            $("#su-select-package button.continue").on("click", function() {
                $('#form_signup').submit();
            });
        });        

        //Masonry
        $('.grid').masonry({
            // options
            itemSelector: '.grid-item',
            columnWidth: 37
        });
    </script>
</body>

</html>
