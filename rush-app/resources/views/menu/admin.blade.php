{{--*/ $menu = isset($menu) ? $menu : 'merchant' /*--}}
<div id="sidemenu">
    <div class="logoholder">
        <a href=""><img src="{{ URL::to('/') }}/page/admin/images/logo_2016.png" alt="RUSH Logo"></a>
    </div>
    <div id="menu">
        <ul>
            <li class="das">
                <a href="{{ url('admin') }}">
                    <i class="icon" data-toggle="tooltip" data-placement="right" title="HOME"></i>
                    <span>Home</span>
                </a>
            </li>
            {{ MenuHelper::admin_generate() }}
        </ul>
    </div>
</div>
