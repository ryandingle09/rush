<div id="sidemenu">
    <div class="logoholder">
        <a href="">
            @if(!empty($merchant_logo))
                <img src="{{ $merchant_logo }}" width="50" alt="">
            @else
                <img src="{{ url('../cpanel/assets/images/rush_logo_2016.png') }}" alt="">
            @endif
        </a>
    </div>
    <div id="menu">
        <ul>
            <li class="das">
                <a href="{{ url('dashboard') }}">
                    <i class="icon" data-toggle="tooltip" data-placement="right" title="DASHBOARD"></i>
                    <span>Dashboard</span>
                </a>
            {{ MenuHelper::generate() }}
        </ul>
    </div>
</div>