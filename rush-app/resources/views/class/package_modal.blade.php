<!-- Modal: Add Package -->
<div id="addPackage" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Add Package</div>
            <div class="modal-body">
                <form method="POST" action="{{ url('class/package') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-6">                            
                            <label for="packageName">Package Name</label>
                            <input type='text' class="form-control" name="name" id="packageName" required>
                        </div>
                        <div class="col-md-6">
                            <label for="packageType">Package Type</label>
                            <select required class="selectpicker form-control packageType" name="type" id="packageType" title="Select Type">
                                <option value="Unlimited">Unlimited</option>
                                <option value="Limited">Limited</option>
                                <option value="Reloadable">Reloadable</option>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <label for="packageBranches">Available Branches</label>
                            <select required class="selectpicker form-control" name="branch_ids[]" id="packageBranches" multiple title="Select Branches" data-actions-box="true">
                                @if(!empty($branches))
                                    @foreach($branches as $branch)
                                        <option value="{{ $branch['id'] }}">{{ $branch['branchName'] }}</option>
                                    @endforeach
                                @else
                                    <option>No Registered Branch</option>
                                @endif
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="packageCategories">Categories</label>
                            <select required class="selectpicker form-control" name="category_ids[]" id="packageCategories" multiple title="Select Categories" data-actions-box="true">
                                <option value="1">A</option>
                                <option value="2">B</option>
                                <option value="3">C</option>
                                <option value="4">D</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="packageVisits">Number of Visits</label>
                            <input type="text" class="form-control" data-visits="packageType" name="no_of_visits" id="packageVisits" required>
                        </div>
                        <div class="col-md-6">
                            <label for="packageValidity">Validity</label>
                            <select required class="selectpicker form-control" name="validity" id="packageValidity" title="Select Validity">
                                <option value="1 day">1 day</option>
                                <option value="1 week">1 week</option>
                                <option value="1 month">1 month</option>
                                <option value="2 months">2 months</option>
                                <option value="3 months">3 months</option>
                                <option value="6 months">6 months</option>
                                <option value="9 months">9 months</option>
                                <option value="12 months">12 months</option>
                                <option value="24 months">24 months</option>
                                <option value="36 months">36 months</option>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <label for="packageImage">Image</label>
                            <div class="row">                              
                                <div class="col-md-3">
                                    <img src="{{MigrationHelper::getBaseUrl()}}assets/images/addImage.jpg" class="packageImage" width="64px">
                                </div>
                                <div class="col-md-9 design">
                                    <span class="btn btn-default btn-file">
                                        Choose File
                                        <input type="file" accept="image/gif, image/jpeg, image/png" id="packageImage" name="image_path" class="fuImage" ew="200" eh="200" required>
                                    </span>
                                    <div class="req-size">Required image size: 200px x 200px</div>
                                    <div id="disp_tmp_path" style="display: none;"></div>
                                </div>
                                <div class="col-md-12">
                                    <span class="error-msg hide">Image does not meet the image size requirement. Please check again.</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="allowMultiple">Allow Multiple Stamps/Day</label>
                            <div>
                                <label class="radio-inline"><input type="radio" name="multiple_stamp" checked value="0">No</label>
                                <label class="radio-inline"><input type="radio" name="multiple_stamp" value="1">Yes</label>
                            </div>
                        </div>
                        <div class="col-md-12 margT20">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button type="submit" class="btn btn-primary pull-right">ADD</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--./ end Add Package-->
<!-- Modal: Edit Package -->
<div id="editPackage" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Edit Package</div>
            <div class="modal-body">
                <form method="POST" action="{{ url('class/package') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="package_id">
                    <div class="form-group row">
                        <div class="col-md-6">                            
                            <label for="epackageName">Package Name</label>
                            <input type='text' class="form-control" name="name" id="epackageName" required>
                        </div>
                        <div class="col-md-6">
                            <label for="epackageType">Package Type</label>
                            <select required class="selectpicker form-control packageType" name="type" id="epackageType" title="Select Type">
                                <option value="Unlimited">Unlimited</option>
                                <option value="Limited">Limited</option>
                                <option value="Reloadable">Reloadable</option>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <label for="epackageBranches">Available Branches</label>
                            <select required class="selectpicker form-control" name="branch_ids[]" id="epackageBranches" multiple title="Select Branches" data-actions-box="true">
                                @if(!empty($branches))
                                    @foreach($branches as $branch)
                                        <option value="{{ $branch['id'] }}">{{ $branch['branchName'] }}</option>
                                    @endforeach
                                @else
                                    <option>No Registered Branch</option>
                                @endif
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="packageCategories">Categories</label>
                            <select required class="selectpicker form-control" name="category_ids[]" id="packageCategories" multiple title="Select Categories" data-actions-box="true">
                                <option value="1">A</option>
                                <option value="2">B</option>
                                <option value="3">C</option>
                                <option value="4">D</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="epackageVisits">Number of Visits</label>
                            <input type="text" class="form-control" data-visits="epackageType" name="no_of_visits" id="epackageVisits" required>
                        </div>
                        <div class="col-md-6">
                            <label for="epackageValidity">Validity</label>
                            <select required class="selectpicker form-control packageValidity" name="validity" id="epackageValidity" title="Select Validity">
                                <option value="1 day">1 day</option>
                                <option value="1 week">1 week</option>
                                <option value="1 month">1 month</option>
                                <option value="2 months">2 months</option>
                                <option value="3 months">3 months</option>
                                <option value="6 months">6 months</option>
                                <option value="9 months">9 months</option>
                                <option value="12 months">12 months</option>
                                <option value="24 months">24 months</option>
                                <option value="36 months">36 months</option>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <label for="epackageImage">Image</label>
                            <div class="row">                              
                                <div class="col-md-3">
                                    <img src="{{MigrationHelper::getBaseUrl()}}assets/images/addImage.jpg" class="packageImage epackageImage" width="64px">
                                </div>
                                <div class="col-md-9 design">
                                    <span class="btn btn-default btn-file">
                                        Choose File
                                        <input type="file" accept="image/gif, image/jpeg, image/png" id="epackageImage" name="image_path" class="fuImage" ew="200" eh="200">
                                    </span>
                                    <div class="req-size">Required image size: 200px x 200px</div>
                                    <div id="disp_tmp_path" style="display: none;"></div>
                                </div>
                                <div class="col-md-12">
                                    <span class="error-msg hide">Image does not meet the image size requirement. Please check again.</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="allowMultiple">Allow Multiple Stamps/Day</label>
                            <div>
                                <label class="radio-inline"><input type="radio" name="multiple_stamp" value="0">No</label>
                                <label class="radio-inline"><input type="radio" name="multiple_stamp" value="1">Yes</label>
                            </div>
                        </div>
                        <div class="col-md-12 margT20">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button type="submit" class="btn btn-primary pull-right">UPDATE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--./ end Edit Package-->
<!--Modal: Delete Package-->
<div id="deletePackage" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Delete Package</div>
            <div class="modal-body">
                <p>Are you sure you want to delete this Package?</>
                <span id="smsToDelete"></span>
                <form method="POST" action="{{ url('class/package') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="DELETE">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button class="btn btn-primary pull-right">DELETE</button>
                        </div>
                    </div>
                    <input type="hidden" name="package_id">
                </form>
            </div>

        </div>
    </div>
</div>
<!--./ end Delete Package-->