<!-- Modal: Add Class Schedule -->
<div id="addClassSched" class="modal fade class-sched-modal" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Add Schedule</div>
            <div class="modal-body">
                <form method="POST" action="{{ url('class/classsched') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-6">                            
                            <label for="schedClassName">Class Name</label>
                            <select required class="selectpicker form-control" name="class_id" id="schedClassName" title="Select Class">
                                @foreach($classes as $class)
                                    @if($class->instructors->count() > 0)
                                        <option value="{{ $class->id }}">{{ $class->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        <div class="clearfix"></div>
                        </div>
                        <div class="col-md-6">
                            <label for="schedInstructorName">Instructor</label>
                            <select required class="selectpicker form-control" name="instructor_id" id="schedInstructorName" title="Select Instructor">
                            </select>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <label for="schedBranches">Available Branch</label>
                            <select required class="selectpicker form-control" name="branch_id" id="schedBranches" title="Select Branch" data-actions-box="true">
                                @if(!empty($branches))
                                    @foreach($branches as $branch)
                                        <option value="{{ $branch['id'] }}">{{ $branch['branchName'] }}</option>
                                    @endforeach
                                @else
                                    <option>No Registered Branch</option>
                                @endif
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="schedImage">Image</label>
                            <div class="row">                              
                                <div class="col-md-3">
                                    <img src="{{MigrationHelper::getBaseUrl()}}assets/images/addImage.jpg" class="schedImage" width="64px">
                                </div>
                                <div class="col-md-9 design">
                                    <span class="btn btn-default btn-file">
                                        Choose File
                                        <input type="file" accept="image/gif, image/jpeg, image/png" id="schedImage" name="image_path" class="fuImage" ew="200" eh="200" required>
                                    </span>
                                    <div class="req-size">Required image size: 200px x 200px</div>
                                    <div id="disp_tmp_path" style="display: none;"></div>
                                </div>
                                <div class="col-md-12">
                                    <span class="error-msg hide">Image does not meet the image size requirement. Please check again.</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="start_date">Start Date</label>
                            <div class='input-group date datetimepicker start-date-field' id='start_date'>
                                <input type='text' class="form-control" name="start_date" id="start_date" placeholder="Select Date"  data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-10 form-group">
                            <label for="schedTime">Time</label>
                            <div class="input-group">
                                <span class="input-group-addon">From</span>
                                <select required class="selectpicker form-control time-selector" name="start_hr" id="schedTimeFrom" title="Hr" data-width="fit">
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                </select>
                                <select required class="selectpicker form-control time-selector" name="start_min" id="schedTimeFrom" title="Min" data-width="fit">
                                    <option value="00">00</option>
                                    <option value="05">05</option>
                                    <option value="10">10</option>
                                    <option value="15">15</option>
                                    <option value="20">20</option>
                                    <option value="25">25</option>
                                    <option value="30">30</option>
                                    <option value="35">35</option>
                                    <option value="40">40</option>
                                    <option value="45">45</option>
                                    <option value="50">50</option>
                                    <option value="55">55</option>
                                </select>
                                <select required class="selectpicker form-control time-selector" name="start_period" id="schedTimeFrom" data-width="fit">
                                    <option value="AM">AM</option>
                                    <option value="PM">PM</option>
                                </select>
                                <span class="input-group-addon">to</span>
                                <select required class="selectpicker form-control time-selector" name="end_hr" id="schedTimeFrom" title="Hr" data-width="fit">
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                </select>
                                <select required class="selectpicker form-control time-selector" name="end_min" id="schedTimeFrom" title="Min" data-width="fit">
                                    <option value="00">00</option>
                                    <option value="05">05</option>
                                    <option value="10">10</option>
                                    <option value="15">15</option>
                                    <option value="20">20</option>
                                    <option value="25">25</option>
                                    <option value="30">30</option>
                                    <option value="35">35</option>
                                    <option value="40">40</option>
                                    <option value="45">45</option>
                                    <option value="50">50</option>
                                    <option value="55">55</option>
                                </select>
                                <select required class="selectpicker form-control time-selector" name="end_period" id="schedTimeFrom" data-width="fit">
                                    <option value="AM">AM</option>
                                    <option value="PM">PM</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="classCapacity">Capacity</label>
                            <div class="input-group">
                                <input type="text" name="attendance_capacity" required>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 repeat">
                            <label><input type="checkbox" name="is_repeat" class="repeat-sched" id="arepeat-sched"> Repeat</label>
                            <a href="#" class="btn btn-link hidden view-repeat-schedule">View Repeat Schedule</a>
                            <div class="col-md-12 repeat-pop arepeat-sched" id="repeatDialogSection">
                                <h5>Repeat Schedule</h5>
                                <a class="rep-done" data-id="arepeat-sched">Done</a>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="schedRepeat">Repeats</label>
                                        <select class="selectpicker form-control" name="repeats" id="schedRepeat">
                                            <option option="weekly">Weekly</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6 rep-freq">
                                        <label for="schedRepeatFreq">Repeat Every</label>
                                        <div class="input-group">
                                            <select class="selectpicker form-control" name="repeat_every" id="schedRepeatFreq" aria-describedby="basic-addon2">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                            </select>
                                            <span class="input-group-addon">weeks</span>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-3 form-group">
                                        <label for="schedRepeatStart">Start Date</label>
                                        <div class='input-group date datetimepicker repeat_start_date_field' id='schedRepeatStart'>
                                            <input type='text' class="form-control" name="repeat_start_date_field" id="schedRepeatDate" placeholder="Select Date"  data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-md-9 form-group">
                                        <label for="schedRepeatEnd">End Date</label>
                                        <div class="rep-end">
                                            <div class="rep-opts">
                                                <label><input type="radio" class="radio-inline schedRepeat never" name="repeat_end_selection" value="never" checked="checked"> Never</label>
                                            </div>
                                            <div class="rep-opts">
                                                <label><input type="radio" class="radio-inline schedRepeat after" name="repeat_end_selection" value="after" data-id="asched-after"> After</label>
                                                <div class='pull-right after' id='schedRepeatAfter' style="width: 138px;margin-left: 10px;">
                                                    <input type='text' class="form-control asched-after" name="occurence" id="schedRepeatADate" value="" disabled placeholder="Occurence">
                                                </div>
                                            </div>
                                            <div class="rep-opts">
                                                <label><input type="radio" class="radio-inline schedRepeat on" name="repeat_end_selection" value="on" data-id="aschedOn"> On</label>
                                                <div class='input-group date datetimepicker pull-right on repeat_end_date_field' id='schedRepeatEnd'>
                                                    <input type='text' class="form-control aschedOn" name="repeat_end_date_field" id="schedRepeatEDate" placeholder="Select Date"  data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="" disabled>
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="repeat_end_date">
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Repeat on</label>
                                    </div>
                                    <div class="col-md-12 repeat-on repeat-on-container">
                                        <label><input type="checkbox" name="repeat_on[]" value="sunday"> S</label>
                                        <label><input type="checkbox" name="repeat_on[]" value="monday"> M</label>
                                        <label><input type="checkbox" name="repeat_on[]" value="tuesday"> T</label>
                                        <label><input type="checkbox" name="repeat_on[]" value="wednesday"> W</label>
                                        <label><input type="checkbox" name="repeat_on[]" value="thursday"> Th</label>
                                        <label><input type="checkbox" name="repeat_on[]" value="friday"> F</label>
                                        <label><input type="checkbox" name="repeat_on[]" value="saturday"> S</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <p>Summary :</p>
                                    </div>
                                    <div class="col-md-10">
                                        <p class="summary_text"></p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 margT20">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button class="btn btn-primary pull-right submit-schedule">ADD</button>
                            <button type="submit" class="hidden">ADD</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--./ end Add Class Schedule -->
<!-- Modal: Edit Class Schedule -->
<div id="editClassSched" class="modal fade class-sched-modal" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Edit Schedule</div>
            <div class="modal-body">
                <form method="POST" action="{{ url('class/classsched') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-6">                            
                            <label for="eschedClassName">Class Name</label>
                            <select required class="selectpicker form-control" name="class_id" id="eschedClassName" title="Select Class">
                                @foreach($classes as $class)
                                    @if($class->instructors->count() > 0)
                                        <option value="{{ $class->id }}">{{ $class->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        <div class="clearfix"></div>
                        </div>
                        <div class="col-md-6">
                            <label for="eschedInstructorName">Instructor</label>
                            <select required class="selectpicker form-control" name="instructor_id" id="eschedInstructorName" title="Select Instructor">
                            </select>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <label for="eschedBranches">Available Branch</label>
                            <select required class="selectpicker form-control" name="branch_id" id="eschedBranches" title="Select Branch" data-actions-box="true">
                                @if(!empty($branches))
                                    @foreach($branches as $branch)
                                        <option value="{{ $branch['id'] }}">{{ $branch['branchName'] }}</option>
                                    @endforeach
                                @else
                                    <option>No Registered Branch</option>
                                @endif
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="eschedImage">Image</label>
                            <div class="row">                              
                                <div class="col-md-3">
                                    <img src="{{MigrationHelper::getBaseUrl()}}assets/images/addImage.jpg" class="schedImage" width="64px">
                                </div>
                                <div class="col-md-9 design">
                                    <span class="btn btn-default btn-file">
                                        Choose File
                                        <input type="file" accept="image/gif, image/jpeg, image/png" id="eschedImage" name="image_path" class="fuImage" ew="200" eh="200">
                                    </span>
                                    <div class="req-size">Required image size: 200px x 200px</div>
                                    <div id="disp_tmp_path" style="display: none;"></div>
                                </div>
                                <div class="col-md-12">
                                    <span class="error-msg hide">Image does not meet the image size requirement. Please check again.</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="start_date">Start Date</label>
                            <div class='input-group date datetimepicker start-date-field' id='start_date'>
                                <input type='text' class="form-control" name="start_date" id="start_date" placeholder="Select Date"  data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="eschedTime">Time</label>
                            <div class="input-group">
                                <select required class="selectpicker form-control" name="start_time" id="eschedTimeFrom" title="Start">
                                    <option value="0000">12:00 AM</option>
                                    <option value="0100">01:00 AM</option>
                                    <option value="0200">02:00 AM</option>
                                    <option value="0300">03:00 AM</option>
                                    <option value="0400">04:00 AM</option>
                                    <option value="0500">05:00 AM</option>
                                    <option value="0600">06:00 AM</option>
                                    <option value="0700">07:00 AM</option>
                                    <option value="0800">08:00 AM</option>
                                    <option value="0900">09:00 AM</option>
                                    <option value="1000">10:00 AM</option>
                                    <option value="1100">11:00 AM</option>
                                    <option value="1200">12:00 PM</option>
                                    <option value="1300">01:00 PM</option>
                                    <option value="1400">02:00 PM</option>
                                    <option value="1500">03:00 PM</option>
                                    <option value="1600">04:00 PM</option>
                                    <option value="1700">05:00 PM</option>
                                    <option value="1800">06:00 PM</option>
                                    <option value="1900">07:00 PM</option>
                                    <option value="2000">08:00 PM</option>
                                    <option value="2100">09:00 PM</option>
                                    <option value="2200">10:00 PM</option>
                                    <option value="2300">11:00 PM</option>
                                </select>
                                <span class="input-group-addon">to</span>
                                <select required class="selectpicker form-control" name="end_time" id="eschedTimeTo" title="End">
                                    <option value="0000">12:00 AM</option>
                                    <option value="0100">01:00 AM</option>
                                    <option value="0200">02:00 AM</option>
                                    <option value="0300">03:00 AM</option>
                                    <option value="0400">04:00 AM</option>
                                    <option value="0500">05:00 AM</option>
                                    <option value="0600">06:00 AM</option>
                                    <option value="0700">07:00 AM</option>
                                    <option value="0800">08:00 AM</option>
                                    <option value="0900">09:00 AM</option>
                                    <option value="1000">10:00 AM</option>
                                    <option value="1100">11:00 AM</option>
                                    <option value="1200">12:00 PM</option>
                                    <option value="1300">01:00 PM</option>
                                    <option value="1400">02:00 PM</option>
                                    <option value="1500">03:00 PM</option>
                                    <option value="1600">04:00 PM</option>
                                    <option value="1700">05:00 PM</option>
                                    <option value="1800">06:00 PM</option>
                                    <option value="1900">07:00 PM</option>
                                    <option value="2000">08:00 PM</option>
                                    <option value="2100">09:00 PM</option>
                                    <option value="2200">10:00 PM</option>
                                    <option value="2300">11:00 PM</option>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 repeat">
                            <label><input type="checkbox" name="is_repeat" class="repeat-sched" id="erepeat-sched"> Repeat</label>
                            <a href="#" class="btn btn-link hidden view-repeat-schedule">View Repeat Schedule</a>
                            <div class="col-md-12 repeat-pop erepeat-sched" id="erepeatDialogSection">
                                <h5>Repeat Schedule</h5>
                                <a class="rep-done" data-id="erepeat-sched">Done</a>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="schedRepeat">Repeats</label>
                                        <select class="selectpicker form-control" name="repeats" id="eschedRepeat">
                                            <option value="Weekly">Weekly</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6 rep-freq">
                                        <label for="eschedRepeatFreq">Repeat Every</label>
                                        <div class="input-group">
                                            <select class="selectpicker form-control" name="repeat_every" id="eschedRepeatFreq" aria-describedby="basic-addon2">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                            </select>
                                            <span class="input-group-addon">weeks</span>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-3 form-group">
                                        <label for="eschedRepeatStart">Start Date</label>
                                        <div class='input-group date datetimepicker repeat_start_date_field' id='eschedRepeatStart'>
                                            <input type='text' class="form-control" name="repeat_start_date_field" id="eschedRepeatDate" placeholder="Select Date"  data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-md-9 form-group">
                                        <label for="eschedRepeatEnd">End Date</label>
                                        <div class="rep-end">
                                            <div class="rep-opts">
                                                <label><input type="radio" class="radio-inline schedRepeat never" name="repeat_end_selection" value="never"> Never</label>
                                            </div>
                                            <div class="rep-opts">
                                                <label><input type="radio" class="radio-inline schedRepeat after" name="repeat_end_selection" value="after" data-id="asched-after"> After</label>
                                                <div class='pull-right after' id='schedRepeatAfter' style="width: 126px;margin-left: 10px;">
                                                    <input type='text' class="form-control asched-after" name="occurence" id="schedRepeatADate" value="" disabled placeholder="Occurence">
                                                </div>
                                            </div>
                                            <div class="rep-opts">
                                                <label><input type="radio" class="radio-inline schedRepeat on" name="repeat_end_selection" value="on" data-id="eschedOn"> On</label>
                                                <div class='input-group date datetimepicker pull-right on repeat_end_date_field' id='eschedRepeatEnd'>
                                                    <input type='text' class="form-control eschedOn" name="repeat_end_date_field" id="eschedRepeatEDate" placeholder="Select Date"  data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="" disabled>
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <input type="hidden" name="repeat_end_date">
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Repeat on</label>
                                    </div>
                                    <div class="col-md-12 repeat-on repeat-on-container">
                                        <label><input type="checkbox" name="repeat_on[]" value="sunday"> S</label>
                                        <label><input type="checkbox" name="repeat_on[]" value="monday"> M</label>
                                        <label><input type="checkbox" name="repeat_on[]" value="tuesday"> T</label>
                                        <label><input type="checkbox" name="repeat_on[]" value="wednesday"> W</label>
                                        <label><input type="checkbox" name="repeat_on[]" value="thursday"> Th</label>
                                        <label><input type="checkbox" name="repeat_on[]" value="friday"> F</label>
                                        <label><input type="checkbox" name="repeat_on[]" value="saturday"> S</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <p>Summary :</p>
                                    </div>
                                    <div class="col-md-10">
                                        <p class="summary_text"></p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <input type="hidden" name="schedule_id">
                        <div class="col-md-12 margT20">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button class="btn btn-primary pull-right submit-schedule">UPDATE</button>
                            <button type="submit" class="hidden">UPDATE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--./ end Edit Class Schedule -->
<!--Modal: Delete Class Schedule -->
<div id="deleteClassSched" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Delete Schedule</div>
            <div class="modal-body">
                <p>Are you sure you want to delete this Schedule?</>
                <span id="smsToDelete"></span>
                <form method="POST" action="{{ url('class/classsched') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="DELETE">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button class="btn btn-primary pull-right">DELETE</button>
                        </div>
                    </div>
                    <input type="hidden" name="schedule_id">
                </form>
            </div>

        </div>
    </div>
</div>
<!--./ end Delete Class Schedule -->