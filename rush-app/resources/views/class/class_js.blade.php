<script>
$(document).ready(function(){

    //package management toolbox
    var ClassManagement = {
        url : "{{ url('') }}",
        server_url : "{{MigrationHelper::getServerUrl()}}",
        base_url : "{{MigrationHelper::getBaseUrl()}}",
        csrf_token : $('meta[name="csrf-token"]').attr('content'),
        init : function(){
            var _cm = this;
            var main_nav = $('.navTabsWrap');

            if(main_nav.height() > 38){
                $('.tab-pane .search').css('top', '-200px');
            }

            _cm.utilities.init();
            _cm.package.init();
            _cm.instructor.init();
            _cm.class.init();
            _cm.schedule.init();
            _cm.class_list.init();
            _cm.customer_reservation_info.init();
        },

        package : {
            init : function(){
                var _package = ClassManagement.package;

                _package.initDatatable();
                _package.initEvents();
                _package.PackageStamp.initEvents();
            },

            initDatatable : function(){
                var _utilities = ClassManagement.utilities;

                var table = $('#packageTable');
                var length_selector = 'select[name=packageTable_length]';
                var legnth_container_selector = '#packageTable_length';

                table.DataTable(_utilities.datatable_specs);

                //place the element on a decent position
                $(length_selector).unwrap();
                $(legnth_container_selector).addClass('dataTables_info');
            },

            initEvents : function(){
                var _package = ClassManagement.package;

                var add_package_modal = $('#addPackage');
                var num_of_visits_field = $('[name="no_of_visits"]');

                num_of_visits_field.on('keyup', function () { 
                    this.value = this.value.replace(/[^0-9\.]/g,'');
                });

                add_package_modal.on('show.bs.modal', function(){
                    _package.cleanAddModal();
                });

                $('body').on('click', '.editPackage', function(){
                    _package.processEditModal($(this));
                });

                $('body').on('click', '.deletePackage', function(){
                    _package.showDeleteModal($(this));
                });

                add_package_modal.on('show.bs.modal', function(){
                    _package.cleanAddModal();
                });
            },

            processEditModal : function(edit_package_btn){
                var _package = ClassManagement.package;

                var package_id = edit_package_btn.attr('data-package-id');
                var parent = edit_package_btn.parents('tr');

                _package.showEditModal({
                    id : package_id,
                    name : parent.find('[data-name]').attr('data-name'),
                    type : parent.find('[data-type]').attr('data-type'),
                    no_of_visits : parent.find('[data-visits]').attr('data-visits'),
                    validity : parent.find('[data-validity]').attr('data-validity'),
                    branch_ids : parent.find('[data-branch-ids]').attr('data-branch-ids').split(','),
                    category_ids : parent.find('[data-category-ids]').attr('data-category-ids').split(','),
                    image_path : parent.find('[data-img-path]').attr('data-img-path'),
                    multiple_stamp : parent.find('[data-multiple-stamp]').attr('data-multiple-stamp')
                });
            },

            showEditModal : function(data){
                var target_modal = $('#editPackage');

                target_modal.find('input[name="name"]').val(data.name);
                target_modal.find('select[name="type"]').selectpicker('val', data.type);
                target_modal.find('input[name="no_of_visits"]').val(data.no_of_visits);
                target_modal.find('select[name="validity"]').selectpicker('val', data.validity);
                target_modal.find('select[name="branch_ids[]"]').selectpicker('val', data.branch_ids);
                target_modal.find('select[name="category_ids[]"]').selectpicker('val', data.category_ids);
                target_modal.find('.packageImage').attr('src', data.image_path);
                target_modal.find('input[name="image_path"]').val('');
                target_modal.find('input[name="package_id"]').val(data.id);

                if(data.multiple_stamp === "1"){
                    target_modal.find('input[name="multiple_stamp"][value="1"]').prop('checked', true);
                } else {
                    target_modal.find('input[name="multiple_stamp"][value="0"]').prop('checked', true);
                }

                target_modal.modal('show');
            },

            showDeleteModal : function(delete_package_btn){
                var target_modal = $('#deletePackage');
                var package_id = delete_package_btn.attr('data-package-id');
                var package_id_field = target_modal.find('input[name="package_id"]');

                package_id_field.val(package_id);

                target_modal.modal('show');
            },

            cleanAddModal : function(){
                var _cm = ClassManagement;

                var add_package_modal = $('#addPackage');
                var default_img = _cm.base_url + 'assets/images/addImage.jpg';

                add_package_modal.find('.packageImage').attr('src', default_img);
            },

            PackageStamp : {
                main_modal : $('#managePackageStamp'),
                stamps_modal : $('#stampModal'),
                package_stamps : {},
                initEvents : function(){
                    var _package_stamp = ClassManagement.package.PackageStamp;

                    $('body').on('click', '.editStamp', function(){
                        _package_stamp.processShowMainModal($(this));
                    });

                    $('body').on('click', '.modifyStamps', function(){
                        _package_stamp.processShowStampsModal($(this));
                    });

                    $('.saveStamp').on('click', function(){
                        _package_stamp.addNewStamp();
                    });

                    $('.clearStamp').on('click', function(){
                        _package_stamp.removeStamp();
                    });

                    _package_stamp.bindFileUpload();
                },

                addNewStamp : function(){
                    var _package_stamp = ClassManagement.package.PackageStamp;

                    var main_modal = _package_stamp.main_modal;
                    var stamp_modal = _package_stamp.stamps_modal;
                    var package_stamps = _package_stamp.package_stamps;

                    var stamp_no = stamp_modal.find('input[name="stamp_no"]').val();
                    var stamp_fake_path = stamp_modal.find('input[name="stamp_reward_image"]').attr('data-file');
                    var stamp_name = stamp_modal.find('textarea[name="stamp_reward_name"]').val();
                    var stamp_details = stamp_modal.find('textarea[name="stamp_reward_details"]').val();

                    if(stamp_name.length <= 0 || stamp_details.length <= 0 || stamp_modal.find('canvas').size() <= 0){
                        stamp_modal.find('.modal-body').prepend('<div class="alert alert-danger"><p>Please fill up all the fields</p></div>');
                        return false;
                    }

                    package_stamps[stamp_no] = {
                        'name' : stamp_name,
                        'fake_path' : stamp_fake_path,
                        'details' : stamp_details,
                        'action' : 'add'
                    };

                    $('[data-stamp-no="' + stamp_no + '"]').addClass('with-stamp');
                    main_modal.find('input[name="package_stamps"]').val(JSON.stringify(package_stamps));
                    stamp_modal.modal('hide');
                },

                removeStamp : function(){
                    var _package_stamp = ClassManagement.package.PackageStamp;

                    var main_modal = _package_stamp.main_modal;
                    var stamp_modal = _package_stamp.stamps_modal;
                    var package_stamps = _package_stamp.package_stamps;

                    var stamp_no = stamp_modal.find('input[name="stamp_no"]').val();

                    _package_stamp.cleanStampModal();

                    package_stamps[stamp_no] = {
                        'action' : 'remove'
                    };

                    $('[data-stamp-no="' + stamp_no + '"]').removeClass('with-stamp');
                    main_modal.find('input[name="package_stamps"]').val(JSON.stringify(package_stamps));
                    stamp_modal.modal('hide');
                },

                processShowMainModal : function(manage_stamp_modal_btn){
                    var _package_stamp = ClassManagement.package.PackageStamp;

                    var parent = manage_stamp_modal_btn.parents('tr');
                    var no_of_stamps = parent.find('[data-visits]').attr('data-visits');
                    var main_modal = _package_stamp.main_modal;
                    var stamp_container = main_modal.find('.stamps-container');
                    var stamp_nos = parent.find('input[name="stamp-nos"]').val().split(',');

                    stamp_container.find('li').remove();
                    main_modal.find('input[name="package_stamps"]').val('');
                    main_modal.find('input[name="package_id"]').val(parent.find('[data-id]').attr('data-id'));
                    _package_stamp.package_stamps = {};

                    for (var i = 1; i <= no_of_stamps; i++) {
                        stamp_container.append('<li><a href="#" class="modifyStamps" data-stamp-no="' + i + '">'+ i +'</a></li>');
                    }

                    for (var i in stamp_nos){
                        $('[data-stamp-no="' + stamp_nos[i] + '"]').addClass('with-stamp');
                    }

                    main_modal.modal('show');
                },

                processShowStampsModal : function (stamp_no_btn){
                    var _package_stamp = ClassManagement.package.PackageStamp;
                    var _utilities = ClassManagement.utilities;

                    var stamp_modal = _package_stamp.stamps_modal;
                    var stamp_no = stamp_no_btn.attr('data-stamp-no');
                    var package_stamps = _package_stamp.package_stamps;
                    var main_modal = _package_stamp.main_modal;
                    var package_id = main_modal.find('input[name="package_id"]').val();

                    _package_stamp.cleanStampModal();
                    
                    if(stamp_no_btn.hasClass('with-stamp')){
                        if(package_stamps[stamp_no] == undefined){
                            var url = 'package/getPackageStamp';
                            var param = { package_id : package_id, stamp_no : stamp_no};
                            
                            _utilities.processAjax(url, param, 'get', function(response){
                                stamp_modal.find(".fileinput-button").append('<canvas width="500" height="100" style="background-image: url(' + response.image_path + ')">');
                                stamp_modal.find('[name="stamp_reward_name"]').val(response.name);
                                stamp_modal.find('[name="stamp_reward_details"]').val(response.details);
                            });

                        } else {
                            stamp_modal.find(".fileinput-button").append('<canvas width="500" height="100" style="background-image: url(' + package_stamps[stamp_no].fake_path + ')">');
                            stamp_modal.find('[name="stamp_reward_name"]').val(package_stamps[stamp_no].name);
                            stamp_modal.find('[name="stamp_reward_details"]').val(package_stamps[stamp_no].details);
                        }
                    }

                    stamp_modal.find('#stampModalLabel').text('STAMP ' + stamp_no);
                    stamp_modal.find('input[name="stamp_no"]').val(stamp_no);

                    _package_stamp.stamps_modal.modal('show')
                },

                cleanStampModal : function(){
                    var _package_stamp = ClassManagement.package.PackageStamp;
                    var stamp_modal = _package_stamp.stamps_modal;

                    stamp_modal.find("textarea").val('');
                    stamp_modal.find("input[type=file]").val('');
                    stamp_modal.find("canvas").remove();
                    stamp_modal.find(".alert").remove();
                    stamp_modal.find(".with-img").removeClass("with-img");
                },

                bindFileUpload: function() {
                    $("#stampModal input[type=file]").fileupload({
                        dataType: 'json',
                        autoUpload: false,
                        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                        maxFileSize: 999000,
                        disableImageResize: /Android(?!.*Chrome)|Opera/
                            .test(window.navigator.userAgent),
                        previewMaxWidth: 350,
                        previewMaxHeight: 200,
                        previewCrop: false,
                        replaceFileInput:false
                    }).on('fileuploadprocessalways', function (e, data) {
                        var that = $(this);
                        var modal_body = that.parent().parent().parent().parent().find(".modal-body");
                        var file = data.files[0];
                        var dimensionIsValid = true;

                        var showPreview = function() {
                            that.parent().find("canvas").remove();
                            if (dimensionIsValid) {
                                that.parent().append(file.preview);
                                console.log(file.preview);
                                that.parent().addClass("with-img");
                                if( that.parent().find('input[type=hidden]').size() > 0 ) {
                                    that.parent().find('input[type=hidden]').val(1);
                                }
                                modal_body.find(".alert").addClass('hide');
                            } else {
                                that.parent().removeClass("with-img");
                                modal_body.find(".alert").removeClass('hide');
                            }
                        }

                        var fr = new FileReader;
                        fr.onload = function() {
                            var img = new Image;
                            img.onload = function() {
                                if (img.width != 350 || img.height != 200) {
                                    dimensionIsValid = false;
                                }
                                showPreview();
                            };
                            img.src = fr.result;
                            that.attr('data-file', fr.result);
                        };
                        fr.readAsDataURL(file);
                    });
                },
            }
        },

        instructor : {
            init : function(){
                var _instructor = ClassManagement.instructor;

                _instructor.initDatatable();
                _instructor.initEvents();
            },

            initDatatable : function(){
                var _utilities = ClassManagement.utilities;
                var _instructor = ClassManagement.instructor;

                var table = $('#instructorTable');
                var length_selector = 'select[name=instructorTable_length]';
                var legnth_container_selector = '#instructorTable_length';

				_utilities.executeDataTable(table, length_selector, legnth_container_selector);

                var table = $('#instructorBranchTable');
                var length_selector = 'select[name=instructorBranchTable_length]';
                var legnth_container_selector = '#instructorBranchTable_length';

                _utilities.executeDataTable(table, length_selector, legnth_container_selector);
            },

            initEvents : function(){
                var _instructor = ClassManagement.instructor;

                var add_instructor = $('#addInstructorBtn');
                var add_instructor_with_branch_only = $('#addInstructorBranchBtn');
                var delete_instructor_submit = $('#delete-instructor-btn');

                $('body').on('click', '.editInstructor', function(){
                    _instructor.processEditModal($(this));
                });

                $('body').on('click', '.deleteInstructor', function(){
                    _instructor.showDeleteModal($(this));
                });

                delete_instructor_submit.on('click', function(e){
                    e.preventDefault();
                    _instructor.processDeleteInstructor();
                });

                add_instructor.on('click', function(){
                	var target_modal = $('#addInstructor');

                	_instructor.processInstructorModal(target_modal);
                	target_modal.modal('show');
                });

                add_instructor_with_branch_only.on('click', function(){
                	var target_modal = $('#addInstructor');
                	
                	_instructor.processInstructorBranchModal(target_modal);
                	target_modal.modal('show');
                });
            },

            processInstructorModal : function(target_modal){
                target_modal.find('form').attr('action', ClassManagement.url + "/class/instructor#instructor");
            	target_modal.find('.branch-container').addClass('hidden')
            				.find('select[name="branch_ids[]"]').attr('required', false);
            	target_modal.find('.class-container').removeClass('hidden')
            				.find('select[name="class_ids[]"]').attr('required', true);
            },

            processInstructorBranchModal : function(target_modal){
                target_modal.find('form').attr('action', ClassManagement.url + "/class/instructor#instructor-with-branch-only");
            	target_modal.find('.branch-container').removeClass('hidden').end()
            				.find('select[name="branch_ids[]"]').attr('required', true);
            	target_modal.find('.class-container').addClass('hidden').end()
            				.find('select[name="class_ids[]"]').attr('required', false);
            },

            processEditModal : function(edit_instructor_btn){
                var _instructor = ClassManagement.instructor;

                var instructor_id = edit_instructor_btn.attr('data-instructor-id');
                var parent = edit_instructor_btn.parents('tr');

                if(edit_instructor_btn.hasClass('instructorBranch')){
                	_instructor.processInstructorBranchModal($('#editInstructor'));
                	branch_ids = parent.find('[data-branch-ids]').attr('data-branch-ids').split(',');
                	class_ids = [];
                } else {
                	_instructor.processInstructorModal($('#editInstructor'));
                	class_ids = parent.find('[data-class-ids]').attr('data-class-ids').split(',');
                	branch_ids = [];
                }

                _instructor.showEditModal({
                    id : instructor_id,
                    name : parent.find('[data-name]').attr('data-name'),
                    description : parent.find('[data-description]').attr('data-description'),
                    class_ids : class_ids,
                    branch_ids : branch_ids,
                    image_path : parent.find('[data-img-path]').attr('data-img-path'),
                });
            },

            showEditModal : function(data){
                var target_modal = $('#editInstructor');

                target_modal.find('[name="name"]').val(data.name);
                target_modal.find('[name="description"]').val(data.description);
                target_modal.find('.instructorImage').attr('src', data.image_path);
                target_modal.find('select[name="class_ids[]"]').selectpicker('val', data.class_ids);
                target_modal.find('select[name="branch_ids[]"]').selectpicker('val', data.branch_ids);
                target_modal.find('input[name="instructor_id"]').val(data.id);

                target_modal.modal('show');
            },

            showDeleteModal : function(delete_instructor_btn){
                var target_modal = $('#deleteInstructor');
                var instructor_id = delete_instructor_btn.attr('data-instructor-id');
                var instructor_id_field = target_modal.find('input[name="instructor_id"]');
                var error_field = target_modal.find('.error-delete-instructor');

                var table_element_id = delete_instructor_btn.parents('table').attr('id');

                if(table_element_id == 'instructorTable'){
                    target_modal.find('form').attr('action', ClassManagement.url + "/class/instructor#instructor");
                } else {
                    target_modal.find('form').attr('action', ClassManagement.url + "/class/instructor#instructor-with-branch-only");
                }

                instructor_id_field.val(instructor_id);
                error_field.addClass('hidden');

                target_modal.modal('show');
            },

            processDeleteInstructor : function(){
                var _utilities = ClassManagement.utilities;

                var target_modal = $('#deleteInstructor');
                var instructor_id = target_modal.find('input[name="instructor_id"]').val();
                var error_field = target_modal.find('.error-delete-instructor');
                var main_form = target_modal.find('#delete-instructor-form');

                var ajax_url = "classlist/getInstructorOnGoingClassScheduleList";
                var ajax_data = { instructor_id : instructor_id};

                _utilities.processAjax(ajax_url, ajax_data, 'get', function(response){
                    if(typeof response[0] != "undefined"){
                        error_field.html("On going classes are currently assigned to <strong>" + response[0].instructor.name + "!</strong> To proceed with deletion, please assign new instructor to those classes");
                        error_field.removeClass('hidden');
                    } else {
                        main_form.submit();
                    }
                });
            }
        },

        class : {
            init : function(){
                var _class = ClassManagement.class;

                _class.initDatatable();
                _class.initEvents();
            },

            initDatatable : function(){
                var _utilities = ClassManagement.utilities;

                var table = $('#classTable');
                var length_selector = 'select[name=classTable_length]';
                var legnth_container_selector = '#classTable_length';

                _utilities.executeDataTable(table, length_selector, legnth_container_selector);

                var table = $('#classWithBranchTable');
                var length_selector = 'select[name=classWithBranchTable_length]';
                var legnth_container_selector = '#classWithBranchTable_length';

                _utilities.executeDataTable(table, length_selector, legnth_container_selector);
            },

            initEvents : function(){
                var _class = ClassManagement.class;

                var add_class_btn = $('#addClassBtn');
                var add_class_branch_btn = $('#addClassBranchBtn');
                var delete_class_submit = $('#delete_class_btn');

                $('body').on('click', '.editClass', function(){
                    _class.processEditModal($(this));
                });

                $('body').on('click', '.deleteClass', function(){
                    _class.showDeleteModal($(this));
                });

                add_class_btn.on('click', function(){
                	var target_modal = $('#addClass');

                	_class.processClassModal(target_modal);
                	target_modal.modal('show');
                });

                add_class_branch_btn.on('click', function(){
                	var target_modal = $('#addClass');
                	
                	_class.processClassBranchModal(target_modal);
                	target_modal.modal('show');
                });

                delete_class_submit.on('click', function(e){
                    e.preventDefault();
                    _class.processDeleteClass();
                });
            },

            processClassModal : function(target_modal){
                target_modal.find('form').attr('action', ClassManagement.url + "/class/class#class");
            	target_modal.find('.branch-container').addClass('hidden')
            				.find('select[name="branch_ids[]"]').attr('required', false);
            },

            processClassBranchModal : function(target_modal){
                target_modal.find('form').attr('action', ClassManagement.url + "/class/class#class-with-branch");
            	target_modal.find('.branch-container').removeClass('hidden').end()
            				.find('select[name="branch_ids[]"]').attr('required', true);
            },

            processEditModal : function(edit_class_btn){
                var _class = ClassManagement.class;

                var class_id = edit_class_btn.attr('data-class-id');
                var parent = edit_class_btn.parents('tr');

                if(edit_class_btn.hasClass('classBranch')){
                	_class.processClassBranchModal($('#editClass'));
                	branch_ids = parent.find('[data-branch-ids]').attr('data-branch-ids').split(',');
                } else {
                	_class.processClassModal($('#editClass'));
                	branch_ids = [];
                }

                _class.showEditModal({
                    id : class_id,
                    name : parent.find('[data-name]').attr('data-name'),
                    description : parent.find('[data-description]').attr('data-description'),
                    image_path : parent.find('[data-img-path]').attr('data-img-path'),
                    branch_ids : branch_ids,
                });
            },

            showEditModal : function(data){
                var target_modal = $('#editClass');

                target_modal.find('[name="name"]').val(data.name);
                target_modal.find('[name="description"]').val(data.description);
                target_modal.find('.classImage').attr('src', data.image_path);
                target_modal.find('input[name="class_id"]').val(data.id);
                target_modal.find('select[name="branch_ids[]"]').selectpicker('val', data.branch_ids);

                target_modal.modal('show');
            },

            showDeleteModal : function(delete_class_btn){
                var target_modal = $('#deleteClass');
                var class_id = delete_class_btn.attr('data-class-id');
                var class_id_field = target_modal.find('input[name="class_id"]');

                class_id_field.val(class_id);

                target_modal.modal('show');
            },

            processDeleteClass : function(){
                var _utilities = ClassManagement.utilities;

                var target_modal = $('#deleteClass');
                var class_id = target_modal.find('input[name="class_id"]').val();
                var error_field = target_modal.find('.error-delete-class');
                var main_form = target_modal.find('#delete-class-form');

                var ajax_url = "classlist/getClassOnGoingClassScheduleList";
                var ajax_data = { class_id: class_id };

                _utilities.processAjax(ajax_url, ajax_data, 'get', function(response){
                    if(typeof response[0] != "undefined"){
                        error_field.html("Error: This class has on-going schedule.");
                        error_field.removeClass('hidden');
                    } else {
                        main_form.submit();
                    }
                });
            }
        },

        schedule : {
            init : function(){
                var _schedule = ClassManagement.schedule;

                _schedule.initDatatable();
                _schedule.initEvents();
                _schedule.RepeatDialogSection.init();
            },

            initDatatable : function(){
                var _utilities = ClassManagement.utilities;

                var table = $('#classSchedTable');
                var length_selector = 'select[name=classSchedTable_length]';
                var legnth_container_selector = '#classSchedTable_length';

                table.DataTable(_utilities.datatable_specs);

                //place the element on a decent position
                $(length_selector).unwrap();
                $(legnth_container_selector).addClass('dataTables_info');
            },

            initEvents : function(){
                var _schedule = ClassManagement.schedule;
                var _utilities = ClassManagement.utilities;
                var _repeat_dialog_section = _schedule.RepeatDialogSection;

                var class_sched_modal = $('.class-sched-modal');
                var repeat_sched_checkbox = $(".repeat-sched");
                
                var class_id_field = class_sched_modal.find('select[name="class_id"]');
                var start_date_field = class_sched_modal.find('input[name="start_date"]');
                var add_class_sched_modal = $('#addClassSched');
                var view_repeat_schedule_btn = $('.view-repeat-schedule');
                var submit_schedule = $('.submit-schedule');

                repeat_sched_checkbox.on("change", function(){
                    var _repeat_dialog_section = ClassManagement.schedule.RepeatDialogSection;
                    var _repeat_dialog_data = _repeat_dialog_section.data;

                    var me = $(this);
                    var repeat_sched = me.prop("checked");
                    var repeat_dialog_section_elem = $("." + me.attr("id"));

                    if(repeat_sched) {
                        _repeat_dialog_section.processAddRepeatSchedule();
                        view_repeat_schedule_btn.removeClass('hidden');
                    } else {
                        repeat_dialog_section_elem.hide();
                        view_repeat_schedule_btn.addClass('hidden');
                    }
                });

                view_repeat_schedule_btn.on('click', function(){
                    $(this).parents('div').find('.repeat-sched').trigger('change');
                });

                class_id_field.on('change', function(){
                    var class_id = $(this).val();

                    var ajax_data = _schedule.processInstructorSelectAjax(class_id);

                    _utilities.processAjax(ajax_data.url, ajax_data.data, 'get', _schedule.attachInstructorsOnSelectInstructor);
                });

                $('body').on('click', '.editClassSched', function(){
                    _schedule.processEditModal($(this));
                });

                $('body').on('click', '.deleteClassSched', function(){
                    _schedule.showDeleteModal($(this));
                });

                add_class_sched_modal.on('shown.bs.modal', function(){
                    var current_date = new Date();
                    var days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday']
                    var current_day = current_date.getDay();

                    _schedule.cleanAddModal();
                    _repeat_dialog_section.refreshRepeatDialogData();
                    view_repeat_schedule_btn.addClass('hidden');
                    _repeat_dialog_section.data.repeat_on = [days[current_day]];

                    _repeat_dialog_section.data.repeat_start_date = _utilities.formatDate(current_date);
                    start_date_field.val(_repeat_dialog_section.data.repeat_start_date);
                });

                submit_schedule.on('click', function(e){
                    var me = $(this);

                    e.preventDefault();
                    _repeat_dialog_section.validateClassScheduleList(function(response){
                        if(response != "No error"){
                            alert(response);
                        } else {
                            me.siblings('[type="submit"]').click();
                        }
                    });
                });

                $(document).on('dp.change', '.start-date-field', function() {
                    _repeat_dialog_section.syncStartDateFields($(this).find('input[name="start_date"]').val());
                });

                start_date_field.val(_utilities.formatDate(new Date()));
            },

            cleanAddModal : function(){
                var target_modal = $('#addClassSched');

                target_modal.find('[name="is_repeat"]').prop('checked', false);
            },

            processInstructorSelectAjax : function(class_id){
                var instructor_id_field = $('.class-sched-modal').find('select[name="instructor_id"]');

                var url = 'classsched/getClassInstructorAjax';
                var data = { class_id : class_id};

                instructor_id_field.find('option').remove();
                instructor_id_field.selectpicker('refresh');

                return {url, data};
            },

            processEditModal : function(edit_class_sched_btn){
                var _schedule = ClassManagement.schedule;

                var schedule_id = edit_class_sched_btn.attr('data-class-sched-id');
                var parent = edit_class_sched_btn.parents('tr');

                var repeat_values = $.parseJSON(parent.find('[name="repeat_value"]').val());

                _schedule.showEditModal({
                    schedule_id : schedule_id,
                    class_id : parent.find('[data-class-id]').attr('data-class-id'),
                    instructor_id : parent.find('[data-instructor-id]').attr('data-instructor-id'),
                    instructor_name : parent.find('[data-instructor-name]').attr('data-instructor-name'),
                    image_path : parent.find('[data-img-path]').attr('data-img-path'),
                    branch_id : parent.find('[data-branch-id]').attr('data-branch-id'),
                    start_date : parent.find('[data-start-date]').attr('data-start-date'),
                    start_time : parent.find('[data-start-time]').attr('data-start-time'),
                    end_time : parent.find('[data-end-time]').attr('data-end-time'),
                    schedule_time_id : repeat_values.schedule_time_id,
                    is_repeat : parseInt(repeat_values.is_repeat),
                    repeats : repeat_values.repeats,
                    repeat_every : repeat_values.repeat_every,
                    repeat_selection : repeat_values.repeat_selection,
                    repeat_occurence : repeat_values.repeat_occurence,
                    repeat_on : repeat_values.repeat_on,
                    repeat_end_date : repeat_values.repeat_end_date,
                });
            },

            showEditModal : function(data){
                var _schedule = ClassManagement.schedule;
                var _utilities = ClassManagement.utilities;
                var _repeat_dialog_section = _schedule.RepeatDialogSection;
                var _repeat_dialog_data = _repeat_dialog_section.data;

                var target_modal = $('#editClassSched');
                var view_repeat_schedule_btn = $('.view-repeat-schedule');

                var ajax_data = _schedule.processInstructorSelectAjax(data.class_id);

                _repeat_dialog_section.refreshRepeatDialogData();
                
                _utilities.processAjax(ajax_data.url, ajax_data.data, 'get', function(instructors){
                    var instructor_id_field = target_modal.find('select[name="instructor_id"]');

                    for (var i in instructors) {
                        
                        if(instructors[i].id == undefined){
                            return false;
                        }

                        instructor_id_field.append('<option value="' + instructors[i].id + '">' + instructors[i].name + '</option>')
                    }

                    instructor_id_field.selectpicker('refresh');
                    instructor_id_field.selectpicker('val', data.instructor_id);
                });

                target_modal.find('input[name="schedule_id"]').val(data.schedule_id);
                target_modal.find('select[name="class_id"]').selectpicker('val', data.class_id);
                target_modal.find('select[name="branch_id"]').selectpicker('val', data.branch_id);
                target_modal.find('.schedImage').attr('src', data.image_path);
                target_modal.find('input[name="image_path"]').val('');
                target_modal.find('input[name="start_date"]').val(data.start_date);
                target_modal.find('select[name="start_time"]').selectpicker('val', data.start_time);
                target_modal.find('select[name="end_time"]').selectpicker('val', data.end_time);

                if(data.is_repeat == 1){
                    view_repeat_schedule_btn.removeClass('hidden');
                    target_modal.find('input[name="is_repeat"]').prop('checked', true);
                    
                    _repeat_dialog_data.repeats = data.repeats;
                    _repeat_dialog_data.repeat_every = data.repeat_every;
                    _repeat_dialog_data.repeat_end_selection = data.repeat_selection;
                    _repeat_dialog_data.repeat_end_occurence = data.repeat_occurence;
                    _repeat_dialog_data.repeat_end_date = data.repeat_end_date;
                    _repeat_dialog_data.repeat_on = data.repeat_on.split(',');

                } else {
                    view_repeat_schedule_btn.addClass('hidden');
                    target_modal.find('input[name="is_repeat"]').prop('checked', false);

                    var date = new Date(data.start_date);
                    var days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday']
                    var current_day = date.getDay();

                    _repeat_dialog_data.repeat_on = [days[current_day]];
                }

                _repeat_dialog_data.repeat_start_date = data.start_date;
                _repeat_dialog_data.schedule_time_id = data.schedule_time_id;

                _repeat_dialog_section.base_elem = $('#erepeatDialogSection');
                _repeat_dialog_section.selectCheckboxOfRepeatOn();
                _repeat_dialog_section.fillUpRepeatDialogSection();
                
                target_modal.modal('show');
            },

            showDeleteModal : function(delete_class_sched_btn){
                var target_modal = $('#deleteClassSched');
                var class_sched_id = delete_class_sched_btn.attr('data-class-sched-id');
                var class_sched_id_field = target_modal.find('input[name="schedule_id"]');

                class_sched_id_field.val(class_sched_id);

                target_modal.modal('show');
            },

            attachInstructorsOnSelectInstructor : function(instructors){
                var instructor_id_field = $('.class-sched-modal').find('select[name="instructor_id"]');

                for (var i in instructors) {
                    
                    if(instructors[i].id == undefined){
                        return false;
                    }

                    instructor_id_field.append('<option value="' + instructors[i].id + '">' + instructors[i].name + '</option>')
                }

                instructor_id_field.selectpicker('refresh');
            },

            RepeatDialogSection : {
                base_elem : $('.repeat-pop'),
                data : {
                    schedule_time_id : null,
                    repeats : 'Weekly',
                    repeat_every : '1',
                    repeat_on : [],
                    repeat_start_date : null,
                    repeat_end_selection : null,
                    repeat_end_date : null,
                    repeat_end_occurence : null,
                },

                init : function(){
                    var _shcedule_repeat_dialog = ClassManagement.schedule.RepeatDialogSection;
                    var _base_elem = _shcedule_repeat_dialog.base_elem;
                    var _data = _shcedule_repeat_dialog.data;

                    var repeat_every_field = _base_elem.find('select[name="repeat_every"]');
                    _data.repeat_every = repeat_every_field.val();
                    
                    _shcedule_repeat_dialog.initEvents();
                },

                initEvents : function(){
                    var _utilities = ClassManagement.utilities;
                    var _repeat_dialog_section = ClassManagement.schedule.RepeatDialogSection;
                    var _base_elem = _repeat_dialog_section.base_elem;
                    var _data = _repeat_dialog_section.data;

                    var repeat_on_checkboxes = _base_elem.find('input[name="repeat_on[]"]');
                    var repeat_every_field = _base_elem.find('select[name="repeat_every"]');
                    var repeat_occurence_field = _base_elem.find('input[name="occurence"]');
                    var repeat_end_selection_field = _base_elem.find('input[name="repeat_end_selection"]');
                    var repeat_sched_done_btn = $(".rep-done");

                    repeat_on_checkboxes.on('click', function(){
                        var repeat_on = [];
                        var parent_div = $(this).parents('div');

                        parent_div.find('input[name="repeat_on[]"]').filter(':checked').each(function(key, elem){
                            repeat_on.push($(elem).val());
                        });

                        _data.repeat_on = repeat_on;

                        _repeat_dialog_section.computeEndDate();
                        _repeat_dialog_section.generateSummaryText();
                    });

                    repeat_every_field.on('change', function(){
                        _data.repeat_every = $(this).val();

                        _repeat_dialog_section.computeEndDate();
                        _repeat_dialog_section.generateSummaryText();
                    });

                    repeat_occurence_field.on('keyup change', function(){
                        this.value = this.value.replace(/[^0-9\.]/g,'');

                        if($(this).val().length > 0){
                            _data.repeat_end_occurence = $(this).val();
                        
                            _repeat_dialog_section.computeEndDate();
                            _repeat_dialog_section.generateSummaryText();
                        }
                    });

                    repeat_end_selection_field.on('click', function(){
                        var me = $(this);
                        var endSched = me.val();
                        var toggleSched = me.attr("data-id");
                        var toggleDate = $(".rep-end ." + endSched + " input." + toggleSched);
                        
                        me.closest(".rep-end").find("input:not([type='radio'])").attr({
                            disabled: "disabled",
                            required: false
                        });

                        _data.repeat_end_selection = endSched;

                        $(toggleDate).attr("required", true);
                        $(toggleDate).removeAttr("disabled");

                        _repeat_dialog_section.displayOccurenceOrEndDateOnField();
                        _repeat_dialog_section.computeEndDate();
                        _repeat_dialog_section.generateSummaryText();
                    });

                    repeat_sched_done_btn.on("click", function(e){
                        var parent = $(this).parents('form');

                        var repeat_dialog_section = $("." + $(this).attr("data-id"));
                        var repeat_start_date_field = parent.find('[name="repeat_start_date_field"]:enabled');
                        var repeat_end_date_field = parent.find('[name="repeat_end_date_field"]:enabled');
                        var repeat_occurence_field = parent.find('[name="occurence"]:enabled');
                        var repeat_on_field = parent.find('[name="repeat_on[]"]:checked');

                        if(repeat_start_date_field.length > 0 && repeat_start_date_field.val() == ""){
                            e.preventDefault();

                            alert('Start Date Field is required');
                        } else if (repeat_end_date_field.length > 0 && repeat_end_date_field.val() == "") {
                            e.preventDefault();
                            
                            alert('End Date Field is required');
                        } else if (repeat_occurence_field.length > 0 && repeat_occurence_field.val() == ""){
                            e.preventDefault();
                            
                            alert('Occurence Field is required');
                        } else if (repeat_on_field.length == 0){
                            e.preventDefault();
                            
                            alert('Please select at least one day to be repeat');
                        } else {
                            repeat_dialog_section.hide();  
                        }
                    });

                    $(document).on('dp.change', '.repeat_start_date_field', function() {
                        repeat_start_date_field = $(this).find('[name="repeat_start_date_field"]');

                        _repeat_dialog_section.syncStartDateFields(repeat_start_date_field.val());
                        _repeat_dialog_section.generateSummaryText();
                    });

                    $(document).on('dp.change', '.repeat_end_date_field', function() {
                        repeat_end_date_field = $(this).find('[name="repeat_end_date_field"]');

                        _data.repeat_end_date = repeat_end_date_field.val()
                        _data.repeat_end_occurence = null;

                        _repeat_dialog_section.computeEndDate();
                        _repeat_dialog_section.generateSummaryText();
                    });
                },
                validateClassScheduleList : function(cb){
                    var _utilities = ClassManagement.utilities;
                    var _repeat_dialog_section = ClassManagement.schedule.RepeatDialogSection;
                    var _base_elem = _repeat_dialog_section.base_elem;
                    var _data = _repeat_dialog_section.data;
                    var parent_modal = _base_elem.parents('.class-sched-modal');
                    var url = 'classsched/validateClassScheduleList';
                    var param_data = $.extend({}, _data);

                    param_data.is_repeat = parent_modal.find('input[name="is_repeat"]:checked').length;
                    param_data.start_period = parent_modal.find('select[name="start_period"]').val();
                    param_data.start_hr = parent_modal.find('select[name="start_hr"]').val();
                    param_data.start_min = parent_modal.find('select[name="start_min"]').val();
                    param_data.end_period = parent_modal.find('select[name="end_period"]').val();
                    param_data.end_hr = parent_modal.find('select[name="end_hr"]').val();
                    param_data.end_min = parent_modal.find('select[name="end_min"]').val();
                    param_data.instructor_id = parent_modal.find('select[name="instructor_id"]').val();

                    _utilities.processAjax(url, param_data, 'get', cb);
                },

                refreshRepeatDialogData : function(){
                    var _data = ClassManagement.schedule.RepeatDialogSection.data;

                    _data.schedule_time_id = null;
                    _data.repeats = 'Weekly';
                    _data.repeat_every = '1';
                    _data.repeat_on = null;
                    _data.repeat_start_date = null;
                    _data.repeat_end_selection = 'never';
                    _data.repeat_end_date = null;
                    _data.repeat_end_occurence = null;
                },

                syncStartDateFields : function (date){
                    var _repeat_dialog_section = ClassManagement.schedule.RepeatDialogSection;
                    var _base_elem = _repeat_dialog_section.base_elem;
                    var _data = _repeat_dialog_section.data;

                    var repeat_start_date_field = _base_elem.find('input[name="repeat_start_date_field"]');
                    var start_date_field = $('.class-sched-modal').find('input[name="start_date"]');

                    repeat_start_date_field.val(date);
                    start_date_field.val(date);

                    _data.repeat_start_date = date;
                },

                displayOccurenceOrEndDateOnField : function(){
                    var _data = ClassManagement.schedule.RepeatDialogSection.data;
                    var _base_elem = ClassManagement.schedule.RepeatDialogSection.base_elem;

                    var repeat_occurence_field = _base_elem.find('input[name="occurence"]');
                    var repeat_end_date_field = _base_elem.find('input[name="repeat_end_date_field"]');

                    if(_data.repeat_end_selection == "after"){
                        repeat_occurence_field.val(_data.repeat_end_occurence);
                        repeat_end_date_field.val("");
                    } else if (_data.repeat_end_selection == "on") {
                        repeat_end_date_field.val(_data.repeat_end_date);
                        repeat_occurence_field.val("");
                    } else {
                        _data.repeat_end_date = null;
                        repeat_occurence_field.val("");
                        repeat_end_date_field.val("");
                    }
                },

                processAddRepeatSchedule : function(){
                    var _utilities = ClassManagement.utilities;
                    var _repeat_dialog_section = ClassManagement.schedule.RepeatDialogSection;
                    var _data = _repeat_dialog_section.data;

                    if(_data.schedule_time_id == null){
                        _repeat_dialog_section.base_elem = $('#repeatDialogSection');
                        _repeat_dialog_section.fillUpRepeatDialogSection();
                    }
                    _repeat_dialog_section.showRepeatDialogSection();
                },

                fillUpRepeatDialogSection : function(){
                    var _repeat_dialog_section = ClassManagement.schedule.RepeatDialogSection;
                    var _base_elem = _repeat_dialog_section.base_elem;
                    var _data = _repeat_dialog_section.data;

                    var repeats_field = _base_elem.find('select[name="repeats"]');
                    var repeat_every_field = _base_elem.find('select[name="repeat_every"]');
                    var repeat_start_date_field = _base_elem.find('input[name="repeat_start_date_field"]');
                    var repeat_end_date_input = _base_elem.find('input[name="repeat_end_date"]');
                    var repeat_end_date_field = _base_elem.find('input[name="repeat_end_date_field"]');
                    var repeat_end_selection_field = _base_elem.find('input[name="repeat_end_selection"]');

                    repeats_field.selectpicker('val', _data.repeats);
                    repeat_every_field.selectpicker('val', _data.repeat_every);
                    repeat_start_date_field.val(_data.repeat_start_date);
                    repeat_end_date_input.val(_data.repeat_end_date);
                    repeat_end_date_field.val(_data.repeat_end_date);
                    repeat_end_selection_field.filter('.' + _data.repeat_end_selection).trigger('click');
                },

                showRepeatDialogSection : function(){
                    var _repeat_dialog_section = ClassManagement.schedule.RepeatDialogSection;
                    var _base_elem = _repeat_dialog_section.base_elem;

                    _repeat_dialog_section.selectCheckboxOfRepeatOn();
                    _repeat_dialog_section.generateSummaryText();

                    _base_elem.show();
                },

                selectCheckboxOfRepeatOn : function(){
                    var _data = ClassManagement.schedule.RepeatDialogSection.data;
                    var _base_elem = ClassManagement.schedule.RepeatDialogSection.base_elem;

                    var repeat_on_container = _base_elem.find('.repeat-on-container');

                    repeat_on_container.find('input[type="checkbox"]').prop('checked', false);

                    for (var i in _data.repeat_on) {
                        repeat_on_container.find('input[value="' + _data.repeat_on[i]  + '"]').prop('checked', true)
                    }
                },

                computeEndDate : function(){
                    var _repeat_dialog_section = ClassManagement.schedule.RepeatDialogSection; 
                    var _data = _repeat_dialog_section.data;
                    var _base_elem = _repeat_dialog_section.base_elem;

                    if(_data.repeat_end_selection == "after"){
                        _repeat_dialog_section.computeOccurenceEndDate();
                    }

                    _base_elem.find('input[name="repeat_end_date"]').val(_data.repeat_end_date);
                },

                computeOccurenceEndDate : function(){
                    var _utilities = ClassManagement.utilities;
                    var _repeat_dialog_section = ClassManagement.schedule.RepeatDialogSection;
                    var _base_elem = _repeat_dialog_section.base_elem;
                    var _data = _repeat_dialog_section.data;

                    var days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];

                    var no_of_weeks = (_data.repeat_end_occurence * _data.repeat_every) - 1;
                    var repeat_start_date = new Date(_data.repeat_start_date);
                    var excess_days = (days.length - 1) - repeat_start_date.getDay();
                    
                    var repeat_end_date = new Date(repeat_start_date.setDate(repeat_start_date.getDate() + ((no_of_weeks * 7) + excess_days)));

                    console.log(repeat_end_date);

                    _data.repeat_end_date = _utilities.formatDate(repeat_end_date);
                },

                generateSummaryText : function(){
                    var _base_elem = ClassManagement.schedule.RepeatDialogSection.base_elem;
                    var _data = ClassManagement.schedule.RepeatDialogSection.data;

                    var summary_text = "";

                    var repeat_summary_text_field = _base_elem.find('.summary_text');

                    if(_data.repeat_every > 1){
                        summary_text += 'Every ' + _data.repeat_every + ' weeks';
                    } else {
                        summary_text += 'Weekly';
                    }

                    if(_data.repeat_on.length == 7){
                        summary_text += " all days";
                    } else {
                        var days = [];

                        for (var i in _data.repeat_on) {
                            days.push(_data.repeat_on[i].charAt(0).toUpperCase() + _data.repeat_on[i].slice(1));
                        }

                        summary_text += " on " + days.join();
                    }

                    if(_data.repeat_end_selection == "after" && _data.repeat_end_occurence != null){
                        summary_text += ", " + _data.repeat_end_occurence + " time(s).";
                    } else if (_data.repeat_end_selection == "on"  && _data.repeat_end_date != null){
                        summary_text += ", until " + _data.repeat_end_date;
                    }

                    repeat_summary_text_field.text(summary_text);
                }
            }
        },

        class_list :{
            init : function(){
                var _class_list = ClassManagement.class_list;

                _class_list.initEvents();
                _class_list.class_list_reservation.init();
            },

            initEvents : function(){
                var _class_list = ClassManagement.class_list;

                var options_main_btn = $('div.type-container').find('.main-btn');
                var update_type_modal = $('#classListUpdateType');
                var update_submit = $('#update-submit');
                var attendance_capacity_field = $('[name="attendance_capacity"]');
                
                attendance_capacity_field.on('keyup', function () { 
                    this.value = this.value.replace(/[^0-9\.]/g,'');
                });

                options_main_btn.on('click', function(e){
                    _class_list.toggleElementStates($(this));
                });

                update_submit.on('click', function(e){
                    e.preventDefault();
                    update_type_modal.modal('show');
                });

                update_type_modal.find('.confirm-container .confirm-btn').on('click', function(e){
                    _class_list.processEditModalPost($(this));
                });

                $('body').on('click', '.editClassList', function(){
                    _class_list.showEditModal($(this));
                });

                $('body').on('click', '.cancelClassList', function(){
                    _class_list.showCancelClassListModal($(this));
                });

                $('body').on('click', '.members-reservation', function(){
                    _class_list.processClassListReservationData($(this));
                });
            },

            toggleElementStates : function(options_main_btn){
                var parent = options_main_btn.parents('.type-container');
                var parent_siblings = parent.siblings('.type-container');
                var target_modal = $('#cancelClassList');
                var action = options_main_btn.attr('data-action');

                target_modal.find('input[name="action"]').val(action);
                parent_siblings.find('.confirm-container').slideUp();
                parent_siblings.find('input').attr('disabled', 'disabled');

                parent.find('input').attr('disabled', false);
                parent.find('.confirm-container').slideToggle();
            },

            processEditModalPost : function(class_list_edit_btn){
                var update_type = class_list_edit_btn.attr('data-update-type');
                var edit_form = $('#editClassList').find('form');
                var update_type_modal = $('#classListUpdateType');
                var update_type_form = update_type_modal.find('form');

                if(update_type_form[0].checkValidity()){
                    if(update_type == 'type_block'){
                        var start_date_range = update_type_modal.find('[name="start_date_range"]').val();
                        var end_date_range = update_type_modal.find('[name="end_date_range"]').val();

                        edit_form.find('[name="start_date_range"]').val(start_date_range);
                        edit_form.find('[name="end_date_range"]').val(end_date_range);
                    }

                    edit_form.find('[name="update_type"]').val(update_type);

                    edit_form.submit();
                } else {
                    update_type_form.find('[type="submit"]').click();
                }
            },

            showEditModal : function(class_list_edit_btn){
                var _utilities = ClassManagement.utilities;

                var target_modal = $('#editClassList');
                var parent = class_list_edit_btn.parents('tr');
                var class_list_id = class_list_edit_btn.attr('data-class-list-id');
                var class_id = parent.find('[data-class-id]').attr('data-class-id');
                var branch_id = parent.find('[data-branch-id]').attr('data-branch-id');
                var start_date_time = parent.find('[data-start-date-time]').attr('data-start-date-time');
                var start_time = parent.find('[data-start-time]').attr('data-start-time');
                var end_time = parent.find('[data-end-time]').attr('data-end-time');
                var instructor_id = parent.find('[data-instructor-id]').attr('data-instructor-id');
                var image_path = parent.find('[data-img-path]').attr('data-img-path');
                var attendance_capacity = parent.find('[data-capacity]').attr('data-capacity');

                var instructor_id_field = target_modal.find('select[name="instructor_id"]');

                var url = 'classsched/getClassInstructorAjax';
                var data = { class_id : class_id};

                instructor_id_field.find('option').remove();

                _utilities.processAjax(url, data, 'get', function(instructors){
                    var instructor_id_field = target_modal.find('select[name="instructor_id"]');

                    for (var i in instructors) {
                        
                        if(instructors[i].id == undefined){
                            return false;
                        }

                        instructor_id_field.append('<option value="' + instructors[i].id + '">' + instructors[i].name + '</option>')
                    }

                    instructor_id_field.selectpicker('refresh');
                    instructor_id_field.selectpicker('val', instructor_id);
                });

                target_modal.find('input[name="class_list_id"]').val(class_list_id);
                target_modal.find('select[name="class_id"]').selectpicker('val', class_id);
                target_modal.find('select[name="branch_id"]').selectpicker('val', branch_id);
                target_modal.find('input[name="start_date"]').val(start_date_time);
                target_modal.find('input[name="time_label"]').val(start_time + ' - ' + end_time);
                target_modal.find('input[name="attendance_capacity"]').val(attendance_capacity);
                target_modal.find('.ClassListImage').attr('src', image_path);

                target_modal.modal('show');
            },

            showCancelClassListModal : function(class_list_cancel_btn){
                var target_modal = $('#cancelClassList');
                var class_list_id = class_list_cancel_btn.attr('data-class-list-id');

                target_modal.find('[name="class_list_id"]').val(class_list_id);

                target_modal.modal('show');
            },

            processClassListReservationData : function(member_reservation_btn){
                var _class_list_reservation = ClassManagement.class_list.class_list_reservation;

                var parent = member_reservation_btn.parents('tr');

                _class_list_reservation.class_list_id = parent.attr('data-class-list-id');
                _class_list_reservation.initializeObjectAndPageData();
            },

            class_list_reservation : {
                class_list_id: 0,
                no_shows: 0,
                showed_up: 0,
                booked: 0,
                capacity: 0,

                init : function(){
                    var _class_list_reservation = ClassManagement.class_list.class_list_reservation;

                    var add_client_source_url = 'test source url';
                    var add_client_field = $('#add-client');
                    var add_client_suggestion = new TextFieldSuggestion(add_client_source_url, add_client_field);

                    _class_list_reservation.initEvents();
                },

                initEvents : function(){
                    var _class_list_reservation = ClassManagement.class_list.class_list_reservation;

                    var add_client_btn = $('#add-client-btn');
                    var add_client_field = $('#add-client');
                    var cancel_reservation_btn = $('#cancel-reservation-submit');
                    var confirm_attendance_btn = $('#confirm-attendance');
                    var select_package_btn = $('#select-package-submit');
                    var return_to_class_list_btn = $('#return-to-class-list-btn');

                    //Class List - Book Multiple vars
                    var book_multiple_btn = $('#book-multiple-btn');
                    var multibook_confirm_package = $('#multibook-confirm');
                    var multibook_select_package = $('#selectPackage2');
                    var multibook_submit = $('#multibook-client-submit');

                    var select_date = $('select.book-date');
                    var select_frequency = $('div.book-freq-opt select');

                    add_client_btn.on('click', function(){
                        if(add_client_field.val().trim() === ""){
                            alert('Client Code/Mobile Number is required');
                        } else {
                            _class_list_reservation.registerCustomerToClass();
                        }
                    });

                    select_package_btn.on('click', function(){
                        _class_list_reservation.registerCustomerToClass($('#selectPackage').val());
                    });

                    cancel_reservation_btn.on('click', function(){
                        _class_list_reservation.deleteReservation($(this));
                    });

                    confirm_attendance_btn.on('click', function(){
                        _class_list_reservation.confirmAttendanceReservation($(this));
                    });

                    $('body').on('click', '.deleteReservationBtn', function(){
                        _class_list_reservation.showDeleteReservationModal($(this));
                    });

                    $('body').on('click', '.confirmAttendance', function(){
                        _class_list_reservation.showConfirmReservationModal($(this));
                    });

                    //Class List - Book Multiple
                    book_multiple_btn.on('click', function(){
                        if(add_client_field.val().trim() === ""){
                            alert('Client Code/Mobile Number is required');
                        } else {
                            _class_list_reservation.registerCustomerToClass(0, true);
                        }
                    });

                    multibook_select_package.on('change', function(){
                        multibook_confirm_package.removeAttr('disabled');
                    });

                    multibook_confirm_package.on('click', function(){
                        _class_list_reservation.processMultibookScheduleModal();
                    });

                    select_date.on('change', function(){
                        _class_list_reservation.computeTotalReservations();
                    });

                    select_frequency.on('change', function(){
                        console.log('testset!');
                        _class_list_reservation.computeTotalReservations();
                    });

                    multibook_submit.on('click', function(e){
                        e.preventDefault();
                        _class_list_reservation.multipleBookCustomerToClass();
                    });

                    $('body').on('click', 'input[type="checkbox"][name="book-days"]', function(){
                        _class_list_reservation.computeTotalReservations();
                    });

                    return_to_class_list_btn.on('click', function(){
                        var alert_container = $('div#class-list-alert');

                        alert_container.find('div').remove();
                    });
                },

                initializeObjectAndPageData : function(){
                    var _class_list_reservation = ClassManagement.class_list.class_list_reservation;

                    _class_list_reservation.displayTableData();
                    _class_list_reservation.displayPageData();
                },

                displayTableData : function(){
                    var _class_list_reservation = ClassManagement.class_list.class_list_reservation;
                    var _utilities = ClassManagement.utilities;

                    var url = 'classlist/getAllCustomerFromClass';
                    var param = { class_list_id : _class_list_reservation.class_list_id};

                    var target_table = $('#memberReservationsTable');
                    var reg_type_cms = 0;
                    var no_show = 0;

                    target_table.find('tbody tr').remove();
                    
                    _utilities.processAjax(url, param, 'get', function(response){
                        for (var i in response) {
                            var table_row = _class_list_reservation.transformResponseTorow(response[i]);

                            target_table.append(table_row);
                        }

                        if(response.length <= 0){
                            target_table.append('<tr><td colspan="10" class="no-result-placeholder">No Result.</td></tr>')
                        }
                    });
                },

                displayPageData : function(){
                    var _class_list_reservation = ClassManagement.class_list.class_list_reservation;
                    var _utilities = ClassManagement.utilities;

                    var url = 'classlist/getClassListPageData';
                    var param = { class_list_id : _class_list_reservation.class_list_id };

                    var header_text_elem = $('.class-details .cd-header');

                    header_text_elem.text("Class Reservations");
                    
                    _utilities.processAjax(url, param, 'get', function(response){
                        header_text_elem.text(response.header_text);
                        _class_list_reservation.capacity = response.capacity;
                        _class_list_reservation.booked = response.booked;
                        _class_list_reservation.no_shows = response.no_show;
                        _class_list_reservation.showed_up = response.showed_up;

                        _class_list_reservation.refreshPageValues();
                    });
                },

                registerCustomerToClass : function(customer_package_id = 0, multiple = false){
                    var _utilities = ClassManagement.utilities;
                    var _class_list_reservation = ClassManagement.class_list.class_list_reservation;

                    var client_data_field = $('#add-client');

                    var url = 'classlist/registerCustomerToClass';
                    var param = { client_data : client_data_field.val().trim(), class_list_id : _class_list_reservation.class_list_id, customer_package_id : customer_package_id, multiple : multiple };

                    var target_table = $('#memberReservationsTable');
                    var select_package_modal = $('#selectPackageModal');
                    
                    _utilities.processAjax(url, param, 'post', function(response){
                        select_package_modal.modal('hide');

                        if(typeof response.customer_packages !== "undefined"){
                            if(multiple === true){
                                _class_list_reservation.processSelectPackageMultiBookModal(response.customer_packages);
                            } else {
                                _class_list_reservation.processSelectPackageModal(response.customer_packages);
                            }

                        } else if(typeof response.message !== "undefined"){
                            alert(response.message);
                        } else {
                            var table_row = _class_list_reservation.transformResponseTorow(response);

                            target_table.find('.no-result-placeholder').remove();
                            target_table.append(table_row);
                            client_data_field.val('');

                            _class_list_reservation.booked += 1;
                            _class_list_reservation.no_shows += 1;
                            _class_list_reservation.refreshPageValues();
                        }
                    });
                },

                multipleBookCustomerToClass : function(){
                    var _utilities = ClassManagement.utilities;
                    var _class_list_reservation = ClassManagement.class_list.class_list_reservation;

                    var client_data_field = $('#add-client');
                    var target_table = $('#memberReservationsTable');
                    var target_modal = $('#multibook-client');
                    var submit_button = $('#multibook-client-submit');

                    var customer_package_id = target_modal.find('input[name="customer_package_id"]').val();
                    var reservation_dates = target_modal.find('input[name="reservation_dates"]').val();
                    var select_day_checkbox = $('input[type="checkbox"][name="book-days"]:checked');
                    var days = [];

                    select_day_checkbox.each(function(key, value){
                        days.push($(value).val());
                    });

                    var url = 'classlist/multipleBookCustomerToClass';
                    var param = {
                        client_data : client_data_field.val().trim(),
                        class_list_id : _class_list_reservation.class_list_id,
                        customer_package_id : customer_package_id,
                        reservation_dates : reservation_dates,
                        days : days
                    };

                    submit_button.addClass('disabled');

                    _utilities.processAjax(url, param, 'post', function(response){
                        target_modal.modal('hide');

                        if(typeof response.alert !== 'undefined'){
                            var alert_container = $('div#class-list-alert');
                            var alert_html = '<div class="alert alert-info alert-dismissable">';

                            alert_container.find('div').remove();
                            alert_html += '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                            alert_html += '<ul>';

                            for(var i in response.alert){
                                alert_html += '<li>';

                                if(response.alert[i].status === 'success'){
                                    alert_html += '<strong style="color: green;">Success!</strong> ' + response.alert[i].message + ' in ' + i;
                                } else {
                                    alert_html += '<strong style="color: red;">Failed!</strong> ' + response.alert[i].message + ' in ' + i;
                                }

                                alert_html += '</li>';
                            }

                            alert_html += '</ul>';
                            alert_html += '</div>';

                            alert_container.append(alert_html);
                            submit_button.removeClass('disabled');
                        }

                        if(typeof response.display_data !== "undefined"){
                            var table_row = _class_list_reservation.transformResponseTorow(response.display_data);

                            target_table.find('.no-result-placeholder').remove();
                            target_table.append(table_row);
                            client_data_field.val('');

                            _class_list_reservation.booked += 1;
                            _class_list_reservation.no_shows += 1;
                            _class_list_reservation.refreshPageValues();
                        }
                    });
                },

                transformResponseTorow : function(response){
                    var reg_type_cms = 0;
                    var no_show = 0;
                    var table_row = '<tr data-reservation-id=' + response.id + '>';
                    var customer_package = {
                        type : '',
                        expiration_date : ''
                    };

                    if(response.customer_package !== null){
                        customer_package.type = response.customer_package.package.name;
                        customer_package.expiration_date = response.customer_package.end_date;
                    }

                    table_row += '<td data-fullname="' + response.customer.fullName + '"><a href="javascript:void(0)" data-customer-id="' + response.customer.customerId + '" class="view-customer-info">' + response.customer.fullName + '</a></td>';
                    table_row += '<td>' + response.customer.client_code + '</td>';
                    table_row += '<td>' + response.customer.mobileNumber + '</td>';
                    table_row += '<td>' + customer_package.type + '</td>'; //package type
                    table_row += '<td>' + customer_package.expiration_date + '</td>'; //expiration date

                    if(response.registration_type == reg_type_cms){
                        table_row += '<td><input type="checkbox" disabled></td>';
                        table_row +=  '<td><input type="checkbox" checked disabled></td>';
                    } else {
                        table_row += '<td><input type="checkbox" checked disabled></td>';
                        table_row +=  '<td><input type="checkbox" disabled></td>';
                    }

                    if(response.showed_up == no_show){
                        table_row += '<td><button class="show no confirmAttendance" data-toggle="true">no</button></td>';
                    } else {
                        table_row += '<td><button class="show yes confirmAttendance" data-toggle="false">yes</button></td>';
                    }

                    table_row += '<td><a href="javascript:void(0)" onclick="return false" class="delete deleteReservationBtn"><i class="fa fa-trash"></i> Delete</a></td>';

                    table_row += '</tr>';

                    return table_row;
                },

                showDeleteReservationModal : function(deleteReservationBtn){
                    var target_modal = $('#reservationCancellation');

                    var btn_parent_tr = deleteReservationBtn.parents('tr');
                    var fullname = btn_parent_tr.find('[data-fullname]').attr('data-fullname');

                    target_modal.find('#cancel-reservation-submit').attr('data-reservation-id', btn_parent_tr.attr('data-reservation-id'));
                    target_modal.find('.main-label').text('Cancel reservation for ' + fullname + '?');

                    target_modal.modal('show');
                },

                showConfirmReservationModal : function(confirmReservationBtn){
                    var target_modal = $('#attendanceConfirmation');

                    var btn_parent_tr = confirmReservationBtn.parents('tr');
                    var fullname = btn_parent_tr.find('[data-fullname]').attr('data-fullname');
                    var confirm_button = target_modal.find('#confirm-attendance');
                    var toggle = confirmReservationBtn.attr('data-toggle');

                    confirm_button.attr('data-reservation-id', btn_parent_tr.attr('data-reservation-id'));
                    confirm_button.attr('data-toggle', toggle);

                    console.log(toggle);
                    if(toggle == "false"){
                        target_modal.find('.main-label').text('Attendance already confirmed for ' + fullname + '.');
                        confirm_button.prop('disabled', true);
                    } else {
                        target_modal.find('.main-label').text('Confirm attendance for ' + fullname + '?');
                        confirm_button.removeAttr('disabled');
                    }

                    target_modal.modal('show');
                },

                deleteReservation : function(confirm_btn){
                    var _class_list_reservation = ClassManagement.class_list.class_list_reservation;
                    var _utilities = ClassManagement.utilities;

                    var target_modal = $('#reservationCancellation');
                    var reservation_id = confirm_btn.attr('data-reservation-id');
                    var url = 'classlist/deleteReservation';
                    var param = { reservation_id : reservation_id };

                    var row  = $('tr[data-reservation-id="' + reservation_id + '"]');
                    
                    _utilities.processAjax(url, param, 'post', function(response){
                        if(response == true){
                            row.remove();
                            target_modal.modal('hide');
                            _class_list_reservation.booked -= 1;
                            _class_list_reservation.refreshPageValues();
                        }
                    });
                },

                confirmAttendanceReservation : function(confirm_btn){
                    var _class_list_reservation = ClassManagement.class_list.class_list_reservation;
                    var _utilities = ClassManagement.utilities;

                    var target_modal = $('#attendanceConfirmation');
                    var reservation_id = confirm_btn.attr('data-reservation-id');
                    var toggle = confirm_btn.attr('data-toggle');
                    var url = 'classlist/confirmAttendanceReservation';
                    var param = { reservation_id : reservation_id, toggle : toggle };

                    var row  = $('tr[data-reservation-id="' + reservation_id + '"]');
                    var confirmReservationBtn  = row.find('.confirmAttendance');

                    confirm_btn.prop('disabled', true);
                    
                    _utilities.processAjax(url, param, 'post', function(response){
                        if(typeof response.message != "undefined"){
                            alert(response.message);
                        } else {
                            target_modal.modal('hide');
                            if(response.showed_up == "true"){
                                _class_list_reservation.showed_up += 1;
                                _class_list_reservation.no_shows -= 1;
                                confirmReservationBtn.removeClass('no').addClass('yes');
                                confirmReservationBtn.attr('data-toggle', 'false');
                                confirmReservationBtn.text('yes');
                            } else {
                                _class_list_reservation.showed_up -= 1;
                                _class_list_reservation.no_shows += 1;
                                confirmReservationBtn.removeClass('yes').addClass('no');
                                confirmReservationBtn.attr('data-toggle', 'true');
                                confirmReservationBtn.text('no');
                            }
                            _class_list_reservation.refreshPageValues();
                        }
                    });
                },

                refreshPageValues: function(){
                    var _class_list_reservation = ClassManagement.class_list.class_list_reservation;

                    var tally_text = $('.add-client .tally');
                    var class_list_row = $('tr[data-class-list-id="' + _class_list_reservation.class_list_id + '"]');

                    tally_text.text('No-Shows: ' + _class_list_reservation.no_shows + ' | Showed Up: ' + _class_list_reservation.showed_up + ' | Booked : ' + _class_list_reservation.booked + ' | Capacity : ' + _class_list_reservation.capacity);
                    class_list_row.find('[data-capacity]').find('a').text('Sign in (' + _class_list_reservation.booked + '/' + _class_list_reservation.capacity + ')');
                },

                processSelectPackageModal : function(customer_packages){
                    var target_modal = $('#selectPackageModal');
                    var target_select_field = target_modal.find('select[name="customer_package_id"]');

                    target_select_field.find('option').remove();

                    for (var i in customer_packages) {
                        target_select_field.append('<option value="' + customer_packages[i].id + '">' + customer_packages[i].package.name + '</option>');
                    }

                    target_modal.modal('show');
                },

                processSelectPackageMultiBookModal : function(customer_packages){
                    var target_modal = $('#selectPackageModal-multibook');
                    var target_select_field = target_modal.find('select[name="customer_package_id"]');

                    target_select_field.find('option').remove();

                    for (var i in customer_packages) {
                        target_select_field.append('<option value="' + customer_packages[i].id + '">' + customer_packages[i].package.name + '</option>');
                    }

                    target_modal.modal('show');
                },

                processMultibookScheduleModal : function(){
                    var _utilities = ClassManagement.utilities;
                    var _class_list_reservation = ClassManagement.class_list.class_list_reservation;

                    var mutltibook_schedule_modal = $('#multibook-client');
                    var submit_button = $('#multibook-client-submit');
                    var customer_package_id = $('#selectPackage2').val();

                    var url = 'classlist/getMultibookFormDates';
                    var param = { class_list_id : _class_list_reservation.class_list_id };

                    var today = _utilities.getDayString(new Date());

                    submit_button.removeClass('disabled');

                    $('#selectPackageModal-multibook').modal('hide');

                    _utilities.processAjax(url, param, 'get', function(response){
                        var days_container = mutltibook_schedule_modal.find('div.select-days-container');
                        var book_date_select = mutltibook_schedule_modal.find('select.book-date');

                        days_container.find('div.item').remove();
                        book_date_select.find('option').remove();

                        for(var i in response.days){
                            var checked = (today === i) ? 'checked' : "";

                            days_container.append('<div class="col-md-3 item"> ' +
                                '<label><input type="checkbox" name="book-days" value="' + i + '" ' + checked + '> '
                                + response.days[i] + '</label> </div>');
                        }

                        for(var i in response.dates){
                            book_date_select.append('<option value="' + i + '">'+ response.dates[i] +' </option>');
                        }

                        _class_list_reservation.computeTotalReservations();
                        mutltibook_schedule_modal.find('input[name="customer_package_id"]').val(customer_package_id);
                        mutltibook_schedule_modal.modal('show');
                    });
                },

                computeTotalReservations : function(){
                    var _utilities = ClassManagement.utilities;

                    var mutltibook_schedule_modal = $('#multibook-client');

                    var total_reservation_ctr = 0;
                    var reservation_dates = [];
                    var total_reservation_placeholder = $('.multibook-total .book-total');

                    var start_date = new Date($('select#book-start').val() * 1000);
                    var end_date = new Date($('select#book-end').val() * 1000);
                    var original_start_date = new Date($('select#book-start').val() * 1000);
                    var loop_ctr = 0;

                    var select_day_checkbox = $('input[type="checkbox"][name="book-days"]:checked');
                    var days = [];

                    var book_frequency = ($('#book-freq-num').val() > 0) ? $('#book-freq-num').val() : 1;
                    var book_frequency_multiplier = $('#book-freq-lab').val();

                    if(book_frequency_multiplier === "weeks" ){
                        book_frequency = (book_frequency > 1) ? (book_frequency - 1) * 7  + 1: 1;
                    } else if (book_frequency_multiplier === "months") {
                        book_frequency = (book_frequency > 1) ? (book_frequency - 1) * 30 + 1: 1;
                    } else {
                        book_frequency *= 1;
                    }

                    select_day_checkbox.each(function(key, value){
                        days.push($(value).val());
                    });

                    end_date.setHours(23,59,59,999);

                    do {
                        var day = _utilities.getDayString(start_date);

                        if(days.indexOf(day) > -1){
                            var mm = start_date.getMonth() + 1;
                            var dd = start_date.getDate();
                            var yyyy = start_date.getFullYear();

                            reservation_dates.push(mm + '/' + dd + '/' + yyyy);
                            total_reservation_ctr++;
                        }

                        if(day === "sunday"){
                            var december = 11;
                            var monday = 1;

                            loop_ctr++;
                            if(book_frequency_multiplier === "months"){
                                if (start_date.getMonth() === december) {
                                    //then set to january of next year
                                    start_date.setFullYear(start_date.getFullYear() + 1);
                                } else {
                                    start_date.setMonth(original_start_date.getMonth() + loop_ctr);
                                    start_date.setDate(original_start_date.getDate());
                                }

                                if(start_date.getDay() !== monday){
                                    var next_monday = new Date(start_date);

                                    next_monday.setDate(next_monday.getDate() + (1 + 7 - next_monday.getDay()) % 7);

                                    start_date.setMonth(next_monday.getMonth());
                                    start_date.setDate(next_monday.getDate());
                                }

                            } else {
                                start_date.setDate(start_date.getDate() + book_frequency);
                            }
                        } else {
                            start_date.setDate(start_date.getDate() + 1);
                        }

                    } while(start_date <= end_date);

                    total_reservation_placeholder.text(total_reservation_ctr);
                    mutltibook_schedule_modal.find('input[name="reservation_dates"]').val(JSON.stringify(reservation_dates));
                }
            }
        },

        customer_reservation_info : {
            customer_id : 0,

            init : function(){
                var _customer_reservation_info = ClassManagement.customer_reservation_info;

                _customer_reservation_info.initEvents();
            },

            initEvents : function(){
                var _customer_reservation_info = ClassManagement.customer_reservation_info;

                var return_btn = $('#customer-details').find('.return-btn');

                $('body').on('click', '.view-customer-info', function(){
                    _customer_reservation_info.customer_id = $(this).attr('data-customer-id');
                    _customer_reservation_info.processCustomerInfo();
                });

                return_btn.on('click', function(){
                    _customer_reservation_info.toggleHideCustomerInfo();
                });
            },

            processCustomerInfo : function(){
                var _utilities = ClassManagement.utilities;
                var _customer_reservation_info = ClassManagement.customer_reservation_info;

                var ajax_url = '/app/management/getCustomerWithReserationsDetails';
                var ajax_data = { customer_id : _customer_reservation_info.customer_id };

                _utilities.processAjax(ajax_url, ajax_data, 'get', function(response){
                    _customer_reservation_info.bindCustomerData(response);
                    _customer_reservation_info.toggleShowCustomerInfo();
                });
            },

            bindCustomerData : function(customer_data){
                var customer_info_page = $('#customer-profile-view');

                var customer_info_name_field = customer_info_page.find('.customer-info.full-name');
                var customer_info_code_field = customer_info_page.find('.customer-info.client-code');
                var customer_info_mobile_number_field = customer_info_page.find('.customer-info.mobile-number');
                var customer_info_email_field = customer_info_page.find('.customer-info.email');
                var customer_info_profile_pic = customer_info_page.find('#customer-profile-image');

                var customer_packages = $('#customerpackage').dataTable();
                var customer_current_booking = $('#customerbookings').dataTable();
                var customer_previous_booking = $('#customerbHistory').dataTable();

                customer_packages.fnClearTable();
                customer_current_booking.fnClearTable();
                customer_previous_booking.fnClearTable();

                if(typeof customer_data.packages[0] != "undefined"){
                    customer_packages.fnAddData(customer_data.packages);
                }

                if(typeof customer_data.current_reservation[0] != "undefined"){
                    customer_current_booking.fnAddData(customer_data.current_reservation);
                }

                if(typeof customer_data.previous_reservation[0] != "undefined"){
                    customer_previous_booking.fnAddData(customer_data.previous_reservation);
                }

                customer_info_name_field.html(customer_data.fullName);
                customer_info_code_field.html(customer_data.client_code || "&nbsp;");
                customer_info_mobile_number_field.html(customer_data.mobileNumber);
                customer_info_email_field.html(customer_data.email);
                customer_info_profile_pic.attr('src', customer_data.profileImage);
            },

            toggleShowCustomerInfo : function(){
                var tab_container = $('#content.class-mgmt');
                var customer_info_page = $('#customer-profile-view');

                tab_container.addClass('hidden');
                customer_info_page.removeClass('hidden');
            },

            toggleHideCustomerInfo : function(){
                var tab_container = $('#content.class-mgmt');
                var customer_info_page = $('#customer-profile-view');

                tab_container.removeClass('hidden');
                customer_info_page.addClass('hidden');
            }
        },

        utilities : {
            init : function(){
                $(".actions-btn").on("click", function(e){
                    $(".actions-btn").removeClass("bs-actions-selected");
                    $(this).toggleClass("bs-actions-selected");
                });

                /* Preview Selected Image */    
                $('.fuImage').on('showPreview', function(event) {
                    $thisImg = $(this).attr("id");
                    var tmppath = URL.createObjectURL(event.target.files[0]);
                    if ($(this).attr('validationStatus') != 'invalid') {
                        console.log($('.'+$thisImg));
                        $('.'+$thisImg).fadeIn('fast').attr('src',tmppath);
                        $('#disp_tmp_path').html(tmppath);
                    } else {
                        $('.'+$thisImg).fadeIn('fast').attr('src', '{{MigrationHelper::getBaseUrl()}}assets/images/addImage.jpg');
                    }
                });

                //datatable
                $.extend($.fn.dataTableExt.oStdClasses, {
                    'sWrapper': 'bootstrap-table dataTables_wrapper',
                    'sFilter': 'pull-right search'
                });
            },

            getDayString : function(date){
                var day = date.getDay();

                switch(day){
                    case 0:
                        return 'sunday';
                        break;
                    case 1:
                        return 'monday';
                        break;
                    case 2:
                        return 'tuesday';
                        break;
                    case 3:
                        return 'wednesday';
                        break;
                    case 4:
                        return 'thursday';
                        break;
                    case 5:
                        return 'friday';
                        break;
                    case 6:
                        return 'saturday';
                        break;
                    default:
                        return 'unknown day'
                }
            },

            processAjax : function(url, data, method, param = null){
                var loading_gif = $('#loading');

                loading_gif.removeClass('hidden');
                
                $.ajax({
                    url : url,
                    data : data,
                    method : method,
                    headers: {
                        'X-CSRF-TOKEN': ClassManagement.csrf_token,
                    },
                    success : function(response){
                        if(response){
                            if(typeof param == "function"){
                                param(response);
                            } else {
                                console.log(response);
                            }
                        }
                    },
                    complete: function(){
                        loading_gif.addClass('hidden');
                    }
                });
            },

            //YYYY - MM - DD
            formatDate : function(date){
                var day = date.getDate().toString();
                var month = date.getMonth() + 1;
                var year = date.getFullYear();

                month = month.toString();

                month = (month.length <= 1) ? "0" + month : month;
                day = (day.length <= 1) ? "0" + day : day;

                return year + "-" + month + "-" + day;
            },

            datatable_specs : {
                'order': [[ 0, 'desc' ]],
                'sDom': '<"fixed-table-toolbar"f><"excelButton hide"B><"customButton">rt<"bottom"ilp><"clear">',
                'buttons': [
                    'excel'
                ],
                'language': {
                    'emptyTable': 'No Matching Records Found',
                },
                'oLanguage': {
                   'sSearch': ''
                },
                'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
                'columnDefs' : [
                    { 'width' : "150px", "targets" : 'limited-column' },
                ],
                'autoWidth' : false,
            },

            executeDataTable : function (table, length_selector, legnth_container_selector){
            	var _utilities = ClassManagement.utilities;

            	table.DataTable(_utilities.datatable_specs);

                //place the element on a decent position
                $(length_selector).unwrap();
                $(legnth_container_selector).addClass('dataTables_info');
            }
        }
    };

    ClassManagement.init();

    function TextFieldSuggestion(source_url, target_elem){
        this.source_url = source_url;
        this.target_elem = target_elem;
    };

    TextFieldSuggestion.prototype.init = function(){
        alert(this.source_url);

        this.initEvents();
    };

    TextFieldSuggestion.prototype.initEvents = function(){
        console.log(this.target_elem);
    };

});

</script>