<!-- Customer Profile Viewer -->
<div role="tabpanel" class="tab-pane" id="customer-details">
    <button aria-controls="classlists" role="tab" data-toggle="tab" class="btn btn-primary smsAdd return-btn" data-toggle="modal" onclick="return false"><i class="fa fa-angle-double-left"></i> Return to Class Reservations</button>

    <div class="col-md-12 profile-details">
        <div class="media">
            <div class="media-left infoImg">
                <img class="media-object" src="http://via.placeholder.com/90x90" id="customer-profile-image" width="100">
            </div>
            <div class="media-body">
                <div class="info-wrap">
                    <div class="col-md-2 infoS">Name:</div>
                    <div class="col-md-10 infoF infoS customer-info full-name">Rob Coronel</div>
                    <div class="col-md-2 infoS">Member ID:</div>
                    <div class="col-md-10 infoF infoS customer-info client-code">0309585831</div>
                    <div class="col-md-2 infoS">Mobile Number:</div>
                    <div class="col-md-10 infoF infoS customer-info mobile-number">09171234567</div>
                    <div class="col-md-2 infoS">Email Address:</div>
                    <div class="col-md-10 infoF infoS customer-info email">zrmcoronel@globe.com.ph</div>
                </div>
            </div>
        </div>
    </div>
    <div class="navTabsWrap">
        <ul class="nav nav-tabs" role="tablist" id="tabs">
            <li role="presentation" class="active">
                <a href="" data-target="#cust-package" aria-controls="cust-package" role="tab" data-toggle="tab">CUSTOMER PACKAGE</a>
            </li>
            <li role="presentation">
                <a href="" data-target="#cust-bookings" aria-controls="cust-bookings" role="tab" data-toggle="tab">CURRENT BOOKINGS/RESERVATIONS</a>
            </li>
            <li role="presentation">
                <a href="" data-target="#cust-bookinghistory" aria-controls="cust-bookinghistory" role="tab" data-toggle="tab">BOOKINGS/RESERVATIONS HISTORY</a>
            </li>
        </ul>
    </div>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="cust-package">
            <table id="customerpackage" data-toggle="table" class="data-table management">
                <thead>
                    <th>Branch</th>
                    <th class="hidden-data">Status</th>
                    <th>Package Name</th>
                    <th>OR#</th>
                    <th>Amount</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Remaining</th>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="8">No result</td>
                    </tr>
                </tbody>
            </table>
            <div class="col-md-12 align-center">
                <button type="button" class="btn exportExcel-btn exportCustomerP">Export to Excel</button>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="cust-bookings">
            <div class="custom-filter-custbookings row">
                <div class="col-md-6 filter-calendar" id="external_filter_container">
                    <span>Date Filter</span>
                </div>
                <button id="clear" style="display: none;">Clear All</button>
                <div class="clearfix"></div>
            </div>
            <table id="customerbookings" data-toggle="table" class="data-table management">
                <thead>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Package Name</th>
                    <th>Class Name</th>
                    <th>Branch</th>
                    <th>Instructor</th>
                    <th>Status</th>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="7">No result</td>
                    </tr>
                </tbody>
            </table>
            <div class="col-md-12 align-center">
                <button type="button" class="btn exportExcel-btn exportCustomerB">Export to Excel</button>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="cust-bookinghistory">
            <div class="custom-filter-custhistory row">
                <div class="col-md-6 filter-calendar" id="previous_booking_date_filter">
                    <span>Date Filter</span>
                </div>
                <button id="clear" style="display: none;">Clear All</button>
                <div class="clearfix"></div>
            </div>
            <table id="customerbHistory" data-toggle="table" class="data-table management">
                <thead>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Package Name</th>
                    <th>Class Name</th>
                    <th>Branch</th>
                    <th>Instructor</th>
                    <th>Status</th>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="7">No result</td>
                    </tr>
                </tbody>
            </table>
            <div class="col-md-12 align-center">
                <button type="button" class="btn exportExcel-btn exportCustomerH">Export to Excel</button>
            </div>
        </div>
    </div>
</div>