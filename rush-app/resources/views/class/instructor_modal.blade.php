<!-- Modal: Add Instructor -->
<div id="addInstructor" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Add Instructor</div>
            <div class="modal-body">
                <form method="POST" action="{{ url('class/instructor') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-6">                            
                            <label for="instructorName">Instructor Name</label>
                            <input type='text' class="form-control" name="name" id="instructorName" required>
                        </div>
                        <div class="col-md-6 class-container">
                            <label for="instructorClassName">Class Name</label>
                            <select required class="selectpicker form-control" name="class_ids[]" id="instructorClassName" multiple title="Select Class" data-actions-box="true">
                                @foreach($classes as $class)
                                    <option value="{{ $class->id }}">{{ $class->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6 hidden branch-container">
                            <label for="instructorBranches">Branches</label>
                            <select required class="selectpicker form-control" name="branch_ids[]" id="instructorBranches" multiple title="Select Branches" data-actions-box="true">
                                @if(!empty($branches))
                                    @foreach($branches as $branch)
                                        <option value="{{ $branch['id'] }}">{{ $branch['branchName'] }}</option>
                                    @endforeach
                                @else
                                    <option>No Registered Branch</option>
                                @endif
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label for="instructorDescription">Description</label>
                            <textarea class="form-control" name="description" id="instructorDescription" rows="3"></textarea>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <label for="instructorImage">Image</label>
                            <div class="row">                              
                                <div class="col-md-3">
                                    <img src="{{MigrationHelper::getBaseUrl()}}assets/images/addImage.jpg" class="instructorImage" width="64px">
                                </div>
                                <div class="col-md-9 design">
                                    <span class="btn btn-default btn-file">
                                        Choose File
                                        <input type="file" accept="image/gif, image/jpeg, image/png" id="instructorImage" name="image_path" class="fuImage" ew="200" eh="200" required>
                                    </span>
                                    <div class="req-size">Required image size: 200px x 200px</div>
                                    <div id="disp_tmp_path" style="display: none;"></div>
                                </div>
                                <div class="col-md-12">
                                    <span class="error-msg hide">Image does not meet the image size requirement. Please check again.</span>
                                </div>
                            </div>
                        </div>                        
                        <div class="col-md-12 margT20">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button type="submit" class="btn btn-primary pull-right">ADD</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--./ end Add Instructor -->
<!-- Modal: Edit Instructor -->
<div id="editInstructor" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Edit Instructor</div>
            <div class="modal-body">
                <form method="POST" action="{{ url('class/instructor') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-6">                            
                            <label for="einstructorName">Instructor Name</label>
                            <input type='text' class="form-control" name="name" id="einstructorName" required>
                        </div>
                        <div class="col-md-6 class-container">
                            <label for="einstructorClassName">Class Name</label>
                            <select required class="selectpicker form-control" name="class_ids[]" id="einstructorClassName" multiple title="Select Class" data-actions-box="true">
                                @foreach($classes as $class)
                                    <option value="{{ $class->id }}">{{ $class->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6 hidden branch-container">
                            <label for="einstructorBranches">Branches</label>
                            <select required class="selectpicker form-control" name="branch_ids[]" id="einstructorBranches" multiple title="Select Branches" data-actions-box="true">
                                @if(!empty($branches))
                                    @foreach($branches as $branch)
                                        <option value="{{ $branch['id'] }}">{{ $branch['branchName'] }}</option>
                                    @endforeach
                                @else
                                    <option>No Registered Branch</option>
                                @endif
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label for="instructorDescription">Description</label>
                            <textarea class="form-control" name="description" id="instructorDescription" rows="3"></textarea>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <label for="einstructorImage">Image</label>
                            <div class="row">                              
                                <div class="col-md-3">
                                    <img src="{{MigrationHelper::getBaseUrl()}}assets/images/addImage.jpg" class="instructorImage" width="64px">
                                </div>
                                <div class="col-md-9 design">
                                    <span class="btn btn-default btn-file">
                                        Choose File
                                        <input type="file" accept="image/gif, image/jpeg, image/png" id="einstructorImage" name="image_path" class="fuImage" ew="200" eh="200">
                                    </span>
                                    <div class="req-size">Required image size: 200px x 200px</div>
                                    <div id="disp_tmp_path" style="display: none;"></div>
                                </div>
                                <div class="col-md-12">
                                    <span class="error-msg hide">Image does not meet the image size requirement. Please check again.</span>
                                </div>
                            </div>
                        </div>   
                        <input type="hidden" name="instructor_id">                     
                        <div class="col-md-12 margT20">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button type="submit" class="btn btn-primary pull-right">UPDATE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--./ end Edit Instructor -->
<!--Modal: Delete Instructor -->
<div id="deleteInstructor" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Delete Instructor</div>
            <div class="modal-body">
                <p>Are you sure you want to delete this Instructor?</>
                <p class="error-delete-instructor hidden" style="color: red;"></p>
                <span id="smsToDelete"></span>
                <form method="POST" action="{{ url('class/instructor') }}" id="delete-instructor-form">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="DELETE">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button class="btn btn-primary pull-right" id="delete-instructor-btn">DELETE</button>
                        </div>
                    </div>
                    <input type="hidden" name="instructor_id">
                </form>
            </div>

        </div>
    </div>
</div>
<!--./ end Delete Instructor-->