<div id="managePackageStamp" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth modal-lg">
        <div class="modal-content modlogin">
            <form method="POST" action="{{ url('class/package/publishStamp') }}" enctype="multipart/form-data">
                <div class="modal-header">Manage Stamps and Rewards</div>
                <div class="modal-body">    
                {{ csrf_field() }}
                    <div class="container-fluid">
                        <div class="row">
                            <div>
                                <label for="stamps">Click on stamp below to add reward.</label>
                                <div id="stamps">
                                    <ul class="stamps-container"></ul>
                                </div>
                            </div>    
                        </div>
                        <input type="hidden" name="package_stamps">
                        <input type="hidden" name="package_id">
                    </div> 
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12 margT20">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button type="submit" class="btn btn-primary pull-right">PUBLISH</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="stampModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="stampModalLabel">STAMP </h4>
            </div>
            <div class="modal-body">
                <div class='alert alert-danger hide'>
                    <p>Image does not meet the image size requirement. Please check again.</p>
                </div>
                <div class="form-group">
                    <span class="btn btn-success fileinput-button">
                        <span>Click to add/change image <br /> <i>Required size is 350px x 200px</i></span>
                        <input id="stamp_reward_image" type="file" name="stamp_reward_image">
                    </span>
                </div>
                <div class="form-group">
                    <label>Reward Name</label>
                    <textarea class="stamp_reward_description form-control" name="stamp_reward_name" maxlength="75"></textarea>
                </div>
                <div class="form-group">
                    <label>Reward Details</label>
                    <textarea class="stamp_reward_details form-control" name="stamp_reward_details" maxlength="1000"></textarea>
                </div>
                <input type="hidden" name="stamp_no">
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-6">
                        <button type="button" class="btn btn-default clearStamp">Clear</button>
                    </div>
                    <div class="col-md-6">
                        <button type="button" class="btn btn-primary saveStamp" >Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>