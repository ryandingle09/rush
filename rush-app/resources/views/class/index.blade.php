@extends('layouts.app')

@section('headerPlaceholder')
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css"> <link rel="stylesheet" href="../page/class/css/style.css">
   <style type="text/css">
        table.data-table .limited-column {
            word-break: break-word;
            vertical-align: top;
            white-space: normal;
        }
        div.date-filter{
            padding-top: 10px;
        }
   </style>
@endsection

@section('view')
<div class="container">
    @include('flash.message')
    <div id="content" class="class-mgmt marginTop smstool">
        <!-- Nav tabs -->
        <div class="navTabsWrap">
            <ul class="nav nav-tabs" role="tablist" id="tabs">
                @if($user_type != 3)

                @if (ClassManagementToolHelper::isAllowed(ClassManagementToolHelper::TOOL_PACKAGE_ID, $merchant))
                    <li role="presentation"><a href="#package" aria-controls="package" role="tab" data-toggle="tab">PACKAGE MANAGEMENT</a></li>
                @endif

                @if (ClassManagementToolHelper::isAllowed(ClassManagementToolHelper::TOOL_INSTRUCTOR_ID, $merchant))
                    <li role="presentation"><a href="#instructor" aria-controls="instructor" role="tab" data-toggle="tab">INSTRUCTOR MANAGEMENT</a></li>
                @endif

                @if (ClassManagementToolHelper::isAllowed(ClassManagementToolHelper::TOOL_INSTRUCTOR_WITH_BRANCH_ID, $merchant))
                    <li role="presentation"><a href="#instructor-with-branch-only" aria-controls="instructor" role="tab" data-toggle="tab">INSTRUCTOR</a></li>
                @endif

                @if (ClassManagementToolHelper::isAllowed(ClassManagementToolHelper::TOOL_CLASS_ID, $merchant))
                    <li role="presentation"><a href="#class" aria-controls="class" role="tab" data-toggle="tab">CLASS MANAGEMENT</a></li>
                @endif

                @if (ClassManagementToolHelper::isAllowed(ClassManagementToolHelper::TOOL_ACTIVITY_ID, $merchant))
                    <li role="presentation"><a href="#class-with-branch" aria-controls="class" role="tab" data-toggle="tab">ACTIVITY</a></li>
                @endif

                @if (ClassManagementToolHelper::isAllowed(ClassManagementToolHelper::TOOL_SCHEDULE_ID, $merchant))
                    <li role="presentation"><a href="#classsched" aria-controls="classsched" role="tab" data-toggle="tab">CLASS SCHEDULE MANAGEMENT</a></li>
                @endif
                
                @endif

                @if (ClassManagementToolHelper::isAllowed(ClassManagementToolHelper::TOOL_SCHEDULE_LIST_ID, $merchant))
                    <li role="presentation"><a href="#classlists" aria-controls="classlists" role="tab" data-toggle="tab">CLASS LISTS</a></li>
                @endif
            </ul>
        </div>        

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane" id="package">
                <button href="#addPackage" class="btn btn-primary smsAdd" data-toggle="modal" onclick="return false">Add Package</button>
                <table id="packageTable" class="data-table class-management" data-toggle="table" data-search="true">
                    <thead>
                    <tr>
                        <th data-sortable="true">ID</th>
                        <th data-sortable="true" class="limited-column">Package Name</th>
                        <th data-sortable="true">Package Type</th>
                        <th>Image</th>
                        <th data-sortable="true">No. of Visits</th>
                        <th data-sortable="true">Validity</th>
                        <th data-sortable="true">Categories</th>
                        <th data-sortable="true" class="limited-column">Available Branches</th>
                        <th></th>
                    </tr>
                    </thead>
                    @forelse($packages as $package)
                       <tr>
                            <td data-id="{{ $package->id }}" data-multiple-stamp="{{ $package->multiple_stamp }}">{{ $package->id }}</td>
                            <td data-name="{{ $package->name }}">{{ $package->name }}</td>
                            <td data-type="{{ $package->type }}">{{ $package->type }}</td>
                            <td><img data-img-path="{{ $package->image_path }}" src="{{ $package->image_path }}" width="64px"></td>
                            <td data-visits="{{ $package->no_of_visits }}">{{ $package->no_of_visits }}</td>
                            <td data-validity="{{ $package->validity }}">{{ $package->validity }}</td>
                            <td data-category-ids="{{ implode(',', $package->categories->pluck('class_package_category_list_id')->toArray()) }}">{{ implode(', ', $package->category_names) }}</td>
                            @if($package->branches->count() === count($branches))
                                <td data-branch-ids={{ implode(',', $package->branches->pluck('id')->toArray()) }}>All Branches</td>
                            @else
                                <td data-branch-ids={{ implode(',', $package->branches->pluck('id')->toArray()) }}>{{ implode(', ', $package->branches->pluck('branchName')->toArray()) }}</td>
                            @endif
                            <td>
                                <a href="#" onclick="return false" class="delete deletePackage" data-package-id="{{ $package->id }}"><i class="fa fa-trash"></i> Delete</a>
                                <a href="#" onclick="return false" class="edit editPackage" data-package-id="{{ $package->id }}"><i class="fa fa-edit"></i> Edit</a>
                                <a href="#"onclick="return false" class="edit editStamp" data-package-id="{{ $package->id }}"><i class="fa fa-crosshairs"></i> Manage Stamps</a>
                                <input type="hidden" name="stamp-nos" value="{{ implode(',', $package->stamps->pluck('stamp_no')->toArray()) }}">
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="7">-</td>
                        </tr>
                    @endforelse
                </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="instructor">
                <button class="btn btn-primary smsAdd" id="addInstructorBtn">Add Instructor</button>
                <table id="instructorTable" class="data-table class-management" data-toggle="table" data-search="true">
                    <thead>
                    <tr>
                        <th data-sortable="true">ID</th>
                        <th data-sortable="true">Instructor Name</th>
                        <th>Image</th>
                        <th data-sortable="true" class="limited-column">Description</th>
                        <th class="limited-column" data-sortable="true">Class Name</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        @forelse($intructors as $instructor)
                            <tr>
                                <td>{{ $instructor->id }}</td>
                                <td data-name="{{ $instructor->name }}">{{ $instructor->name }}</td>
                                <td><img data-img-path="{{ $instructor->image_path }}" src="{{ $instructor->image_path }}" width="64px"></td>
                                <td data-description="{{ $instructor->description }}">{{ $instructor->description }}</td>
                                <td data-class-ids="{{ implode(',', $instructor->classes->pluck('id')->toArray()) }}">{{ implode(', ', $instructor->classes->pluck('name')->toArray()) }}</td>
                                <td>
                                    <a href="#" onclick="return false" class="delete deleteInstructor" data-instructor-id="{{ $instructor->id }}"><i class="fa fa-trash"></i> Delete</a>
                                    <a href="#" onclick="return false" class="edit editInstructor" data-instructor-id="{{ $instructor->id }}"><i class="fa fa-edit"></i> Edit</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5">-</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="instructor-with-branch-only">
                <button class="btn btn-primary smsAdd" id="addInstructorBranchBtn">Add Instructor</button>
                <table id="instructorBranchTable" class="data-table" data-toggle="table" data-search="true">
                    <thead>
                    <tr>
                        <th data-sortable="true">ID</th>
                        <th data-sortable="true">Instructor Name</th>
                        <th>Image</th>
                        <th data-sortable="true" class="limited-column">Description</th>
                        <th data-sortable="true" class="limited-column">Branches</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        @forelse($intructors_with_branch_only as $instructor_branch)
                            <tr>
                                <td>{{ $instructor_branch->id }}</td>
                                <td data-name="{{ $instructor_branch->name }}">{{ $instructor_branch->name }}</td>
                                <td><img data-img-path="{{ $instructor_branch->image_path }}" src="{{ $instructor_branch->image_path }}" width="64px"></td>
                                <td data-description="{{ $instructor_branch->description }}">{{ $instructor_branch->description }}</td>
                                <td data-branch-ids="{{ implode(',', $instructor_branch->branches->pluck('id')->toArray()) }}">{{ implode(', ', $instructor_branch->branches->pluck('name')->toArray()) }}</td>
                                <td>
                                    <a href="#" onclick="return false" class="delete deleteInstructor" data-instructor-id="{{ $instructor_branch->id }}"><i class="fa fa-trash"></i> Delete</a>
                                    <a href="#" onclick="return false" class="edit editInstructor instructorBranch" data-instructor-id="{{ $instructor_branch->id }}"><i class="fa fa-edit"></i> Edit</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5">-</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="class">
                <button class="btn btn-primary smsAdd" id="addClassBtn" onclick="return false">Add Class</button>
                <table id="classTable" class="data-table class-management" data-toggle="table" data-search="true">
                    <thead>
                    <tr>
                        <th data-sortable="true">ID</th>
                        <th data-sortable="true" class="limited-column">Name</th>
                        <th>Image</th>
                        <th data-sortable="true" class="limited-column">Description</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        @forelse($classes as $class)
                            <tr>
                                <td>{{ $class->id }}</td>
                                <td data-name="{{ $class->name }}">{{ $class->name }}</td>
                                <td><img data-img-path="{{ $class->image_path }}" src="{{ $class->image_path }}" width="64px"></td>
                                <td data-description="{{ $class->description }}">{{ $class->description }}</td>
                                <td>
                                    <a href="#" onclick="return false" class="delete deleteClass" data-class-id="{{ $class->id }}"><i class="fa fa-trash"></i> Delete</a>
                                    <a href="#" onclick="return false" class="edit editClass" data-class-id="{{ $class->id }}"><i class="fa fa-edit"></i> Edit</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5">-</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="class-with-branch">
                <button class="btn btn-primary smsAdd" id="addClassBranchBtn" onclick="return false">Add Activity</button>
                <table id="classWithBranchTable" class="data-table class-management" data-toggle="table" data-search="true">
                    <thead>
                    <tr>
                        <th data-sortable="true">ID</th>
                        <th data-sortable="true" class="limited-column">Name</th>
                        <th>Image</th>
                        <th data-sortable="true" class="limited-column">Description</th>
                        <th data-sortable="true" class="limited-column">Branches</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        @forelse($classes_with_branch as $class_with_branch)
                            <tr>
                                <td>{{ $class_with_branch->id }}</td>
                                <td data-name="{{ $class_with_branch->name }}">{{ $class_with_branch->name }}</td>
                                <td><img data-img-path="{{ $class_with_branch->image_path }}" src="{{ $class_with_branch->image_path }}" width="64px"></td>
                                <td data-description="{{ $class_with_branch->description }}">{{ $class_with_branch->description }}</td>
                                <td data-branch-ids="{{ implode(',', $class_with_branch->branches->pluck('id')->toArray()) }}">{{ implode(', ', $class_with_branch->branches->pluck('name')->toArray()) }}</td>
                                <td>
                                    <a href="#" onclick="return false" class="delete deleteClass" data-class-id="{{ $class_with_branch->id }}"><i class="fa fa-trash"></i> Delete</a>
                                    <a href="#" onclick="return false" class="edit editClass classBranch" data-class-id="{{ $class_with_branch->id }}"><i class="fa fa-edit"></i> Edit</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5">-</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="classsched">
                <button href="#addClassSched" class="btn btn-primary smsAdd" data-toggle="modal" onclick="return false">Add Class Schedule</button>
                <table id="classSchedTable" data-toggle="table" class="data-table management" data-search="true">
                    <thead>
                        <th data-sortable="true">ID</th>
                        <th data-sortable="true" class="limited-column">Class</th>
                        <th data-sortable="true" class="limited-column">Instructor</th>
                        <th>Image</th>
                        <th data-sortable="true" class="limited-column">Branch</th>
                        <th data-sortable="true">Start Date</th>
                        <th data-sortable="true">Start Time</th>
                        <th data-sortable="true">End Time</th>
                        <th></th>
                    </thead>
                    <tbody>
                        @forelse($class_schedules as $class_schedule)
                            <tr>
                                <td>{{ $class_schedule->id }}</td>
                                <td data-class-name="{{ $class_schedule->classes->name }}" data-class-id="{{ $class_schedule->classes->id }}">{{ $class_schedule->classes->name }}</td>
                                <td data-instructor-name="{{ $class_schedule->instructor->name }}" data-instructor-id="{{ $class_schedule->instructor->id }}">{{ $class_schedule->instructor->name }}</td>
                                <td><img data-img-path="{{ $class_schedule->image_path }}" src="{{ $class_schedule->image_path }}" width="64px"></td>
                                <td data-branch-id="{{ $class_schedule->branch_id }}">{{ $class_schedule->branch->name }}</td>
                                <td data-start-date="{{ $class_schedule->start_date->toDateString() }}">{{ $class_schedule->start_date->toDateString() }}</td>
                                <td data-start-time="{{ str_pad($class_schedule->start_time, 4, 0, STR_PAD_LEFT) }}">{{ $class_schedule->start_time_label }}</td>
                                <td data-end-time="{{ str_pad($class_schedule->end_time, 4, 0, STR_PAD_LEFT) }}">{{ $class_schedule->end_time_label }}</td>
                                <td>
                                    <a href="#" onclick="return false" class="delete deleteClassSched" data-class-sched-id="{{ $class_schedule->id }}"><i class="fa fa-trash"></i> Delete</a>
                                    <!-- <a href="#" onclick="return false" class="edit editClassSched" data-class-sched-id="{{ $class_schedule->id }}"><i class="fa fa-edit"></i> Edit</a> -->

                                    <input type="hidden" name="repeat_value" value='
                                    {
                                        "schedule_time_id" : "{{ $class_schedule->id }}",
                                        "is_repeat" : "{{ $class_schedule->is_repeat }}",
                                        "repeats": "{{ $class_schedule->repeats }}",
                                        "repeat_every" : "{{ $class_schedule->repeat_every }}",
                                        "repeat_selection" : "{{ $class_schedule->repeat_selection }}",
                                        "repeat_occurence" : "{{ $class_schedule->repeat_occurence }}",
                                        "repeat_on" : "{{ $class_schedule->repeat_on }}",
                                        "repeat_end_date" : "{{ $class_schedule->end_date->toDateString() }}"
                                    }
                                    '>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="9">-</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="classlists">
                <button href="#addClassSched" class="btn btn-primary smsAdd" data-toggle="modal" onclick="return false">Add Class Schedule</button>
                <div class="col-md-12">
                    <h4>{{ Carbon\Carbon::today()->format('l') }} <span class="cal-icon"><i class="fa fa-calendar"></i></span></h4>
                    <!--<p>(May 30, 2017)</p>-->
                    <p>({{ Carbon\Carbon::today()->format('F d, Y') }})</p>
                </div>
                <div class="custom-filter row">
                    <div class="col-md-6 dropdowns"></div>
                    <div class="col-md-6 date-filter">
                        <form class="form-inline" action="{{ url('class/classlist#classlists') }}">
                            <div class='input-group date datetimepicker col-md-4'>
                                <input type='text' class="form-control input-sm" name="start_date_rage" placeholder="From"  data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="{{ $start_date_rage or ''}}" required>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                            <div class='input-group date datetimepicker col-md-4'>
                                <input type='text' class="form-control input-sm" name="end_date_rage" placeholder="To"  data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="{{ $end_date_rage or ''}}" required>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                            <button type="submit" class="btn btn-primary btn-sm">Filter Date</button>
                            <a href="{{ url('class/classlist#classlists') }}" class="btn btn-default btn-sm">Reset</a>
                        </form>
                    </div>
                    <button id="clear" style="display: none;">Clear All</button>
                    <div class="clearfix"></div>
                </div>
                <table id="classListTable" data-toggle="table" class="data-table management">
                    <thead>
                        <th class="hidden-data"></th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>No. of Sign Ins</th>
                        <th>Classes</th>
                        <th>Teacher</th>
                        <th>Location</th>
                        <th>Actions</th>
                    </thead>
                    <tfoot>
                        <tr>
                            <th class="hidden-data"></th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>No. of Sign Ins</th>
                            <th>Classes</th>
                            <th>Teacher</th>
                            <th>Location</th>
                            <th>Actions</th>
                        </tr>
                    </tfoot>

                    @forelse($class_lists as $class_list)
                        <tr data-class-list-id="{{ $class_list->id }}">
                            <td class="hidden-data" data-start-date-time="{{ $class_list->start_date_time->format('Y-m-d') }}" data-img-path="{{ $class_list->schedule->image_path }}">{{ $class_list->start_date_time->format('m/d/Y') }}</td>
                            <td>{{ $class_list->start_date_time->format('l') }}, {{ $class_list->start_date_time->format('F d, Y') }}</td>
                            
                            @if($class_list->status == 'cancelled')
                                <td data-start-time="{{ $class_list->start_time_label }}" data-end-time="{{ $class_list->end_time_label }}">{{ $class_list->start_time_label }} - {{ $class_list->end_time_label }} <small style="color: red;">(cancelled)</small></td>
                            @else
                                <td data-start-time="{{ $class_list->start_time_label }}" data-end-time="{{ $class_list->end_time_label }}">{{ $class_list->start_time_label }} - {{ $class_list->end_time_label }}</td>
                            @endif
                            <td data-capacity="{{ $class_list->attendance_capacity }}"><a href="#members-reservation" aria-controls="members-reservation" class="members-reservation" role="tab" data-toggle="tab">Sign in ({{ $class_list->reservations->count() }}/{{ $class_list->attendance_capacity }})</a></td>
                            <td data-class-id="{{ $class_list->schedule->classes->id }}">{{ $class_list->schedule->classes->name }}</td>

                            @if($class_list->instructor_status == 'substitute')
                                <td data-instructor-id="{{ $class_list->instructor->id }}" data-sort-key="{{ $class_list->instructor->name }}">{{ $class_list->instructor->name }} <small style="color: red;">(substitute)</small></td>
                            @else
                                <td data-instructor-id="{{ $class_list->instructor->id }}" data-sort-key="{{ $class_list->instructor->name }}">{{ $class_list->instructor->name }}</td>
                            @endif
                            <td data-branch-id="{{ $class_list->schedule->branch_id }}">{{ $class_list->schedule->branch->name }}</td>
                            <td>
                                <a href="#" onclick="return false" class="editClassList" data-class-list-id="{{ $class_list->id }}">Edit</a> | <a href="#" onclick="return false" class="cancelClassList" data-class-list-id="{{ $class_list->id }}">Cancel</a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="8">-</td>
                        </tr>
                    @endforelse
                </table>
            </div> <!-- end of Class Lists -->
            
            <div role="tabpanel" class="tab-pane" id="members-reservation">
                <button href="#classlists" aria-controls="classlists" role="tab" data-toggle="tab" class="btn btn-primary smsAdd" id="return-to-class-list-btn" data-toggle="modal" onclick="return false"><i class="fa fa-angle-double-left"></i> Return to Class Lists</button>

                <div class="row">
                    <div class="col-sm-7 add-client form-group">
                        <label for="add-client">Client Code/Mobile Number: </label>
                        <input type="text" class="form-control" name="add-client" id="add-client">
                        <button type="submit" class=" btn btn-primary smsAdd" id="add-client-btn">Add to this class</button>
                        <button type="submit" class=" btn btn-primary smsAdd multi-book" id="book-multiple-btn">Book Multiple</button>
                    </div>
                    <div class="col-sm-5 add-client form-group align-right">
                        <span class="tally">No-Shows: 0 | Showed Up: 0 | Booked: 0 | Capacity: 0</span>
                    </div>
                    <div class="col-md-12" id="class-list-alert"></div>
                    <div class="col-md-12 class-details">
                        <h5 class="cd-header">Class Reservation</h5>
                    </div>

                    <div class="col-md-12">
                        <table id="memberReservationsTable" data-toggle="table" class="data-table management">
                            <thead>
                                <th>Client Name</th>
                                <th>Client Code</th>
                                <th>Mobile</th>
                                <th>Package Type</th>
                                <th>Expiration Date</th>
                                <th>Online</th>
                                <th>CMS</th>
                                <th>Showed Up</th>
                                <th>Actions</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="10" class="no-result-placeholder">No Result.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12 align-center">
                        <button type="button" class="btn exportExcel">Export to Excel</button>
                    </div>
                </div>
            </div>     
        </div>
    </div>
    <div id="customer-profile-view" class="hidden">
        @include('class.customer-profile-view')
    </div>
</div>
@endsection

@section('modal')

@include('class.package_modal')
@include('class.package_stamp_modal')
@include('class.instructor_modal')
@include('class.class_modal')
@include('class.class_schedule_modal')
@include('class.class_list_modal')

<div id="loading" class="hidden" style="position: absolute; z-index: 10000; left: 50%; top: 50%;">
    <i class="fa fa-circle-o-notch fa-spin" style="font-size:24px"></i>
</div>
@endsection

@section('footerPlaceholder')
@parent
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
@include('class.class_js')
@include('class.class_list_js')
<script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
<script>
$(document).ready(function(){
    $("#memberReservationsTable").dataTable({
        dom: 'Brtip',
        buttons: [{
            extend: 'excel',
            title: 'Members Reservation List'
        }],
        "bPaginate" : false,
        "bInfo" : false,
    });
    $(".exportExcel").on("click", function(){
        $("#memberReservationsTable_wrapper .buttons-excel").click();
    });
});
</script>

@endsection