<div id="editClassList" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span><div class="modal-header">Edit Schedule</div>
            <div class="modal-body">
                <form method="POST" action="{{ url('class/classlist/updateClassListSchedule') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="update_type" value="one">
                <input type="hidden" name="start_date_range">
                <input type="hidden" name="end_date_range">

                <div class="form-group row">
                    <div class="col-md-6">                            
                            <label for="ClassListName">Class Name</label>
                            <select class="selectpicker form-control" name="class_id" id="ClassListName" title="Select Class" disabled>
                                @foreach($classes as $class)
                                    @if($class->instructors->count() > 0)
                                        <option value="{{ $class->id }}">{{ $class->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        <div class="clearfix"></div>
                        </div>
                        <div class="col-md-6">
                            <label for="ClassListInstructorName">Instructor</label>
                            <select class="selectpicker form-control" name="instructor_id" id="ClassListInstructorName" title="Select Instructor" required>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <label for="ClassListBranches">Available Branch</label>
                            <select class="selectpicker form-control" name="branch_id" id="ClassListBranches" title="Select Branch" data-actions-box="true" disabled>
                                @if(!empty($branches))
                                    @foreach($branches as $branch)
                                        <option value="{{ $branch['id'] }}">{{ $branch['branchName'] }}</option>
                                    @endforeach
                                @else
                                    <option>No Registered Branch</option>
                                @endif
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="ClassListImage">Image</label>
                            <div class="row">                              
                                <div class="col-md-3">
                                    <img src="{{MigrationHelper::getBaseUrl()}}assets/images/addImage.jpg" class="ClassListImage" width="64px">
                                </div>
                                <div class="col-md-9 design">
                                    <span class="btn btn-default btn-file">
                                        Choose File
                                        <input type="file" accept="image/gif, image/jpeg, image/png" id="schedImage" name="image_path" class="fuImage" ew="200" eh="200" disabled>
                                    </span>
                                    <div class="req-size">Required image size: 200px x 200px</div>
                                    <div id="disp_tmp_path" style="display: none;"></div>
                                </div>
                                <div class="col-md-12">
                                    <span class="error-msg hide">Image does not meet the image size requirement. Please check again.</span>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-4 form-group">
                            <label for="start_date">Start Date</label>
                            <div class='input-group date datetimepicker start-date-field' id='estart_date'>
                                <input type='text' class="form-control" name="start_date" id="start_date" placeholder="Select Date"  data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="" disabled>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="ClassListTime">Time</label>
                            <div class="input-group">
                                <input type="text" name="time_label" disabled>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <label for="classCapacity">Capacity</label>
                            <div class="input-group">
                                <input type="text" name="attendance_capacity" required>
                            </div>
                        </div>
                        <div class="col-md-12 margT20">
                            <input type="hidden" name="class_list_id">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button class="btn btn-primary pull-right" id="update-submit">UPDATE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="classListUpdateType" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog sm">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span><div class="modal-header">What would you like to do?</div>
            <div class="modal-body">
                <form method="POST" class="form-inline" enctype="multipart/form-data">
                <div class="form-group row">
                    <div class="row type-container">
                        <a href="javascript:void(0)" class="main-btn" data-action="type_one">
                            <div class="col-md-2">
                                <span class="cancel-ico one-class">
                                    <i class="fa fa-calendar-o"></i>
                                </span>
                            </div>
                            <div class="col-md-10">
                                <h5>Update this one class?</h5>
                                <p>All other instances of the class will remain.</p>
                            </div>
                            </a>
                        </a>
                        <div class="container-fluid confirm-container">
                            <div class="col-md-offset-1 col-md-11">
                                <h5>Do you want to update this one class? <a href="#" class="btn btn-xs btn-primary confirm-btn" data-update-type="type_one">Yes</a></h5>
                            </div>
                        </div>
                    </div>
                    <div class="row type-container">
                        <a href="javascript:void(0)" class="main-btn" data-action="type_block">
                        <div class="col-md-2">
                            <span class="cancel-ico block-class">
                                <i class="fa fa-calendar-minus-o"></i>
                            </span>
                        </div>
                        <div class="col-md-10">
                            <h5>Update a block of related class</h5>
                            <p>Classes with specific date range will be updated.</p>
                        </div>
                        </a>
                        <div class="container-fluid confirm-container" style="display: none;">
                            <div class="col-md-offset-1 col-md-11">
                                <div class='input-group date datetimepicker col-md-4'>
                                    <input type='text' class="form-control input-xs start_date_range" name="start_date_range" placeholder="From"  data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" required disabled>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                                <div class='input-group date datetimepicker col-md-4'>
                                    <input type='text' class="form-control input-xs end_date_range" name="end_date_range" placeholder="To"  data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" required disabled>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                                <a href="#" class="btn btn-primary btn-xs confirm-btn" data-update-type="type_block">Confirm</a>
                            </div>
                        </div>
                    </div>
                    <div class="row type-container">
                        <a href="javascript:void(0)" class="main-btn" data-action="type_following">
                        <div class="col-md-2">
                            <span class="cancel-ico all-following">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                        <div class="col-md-10">
                            <h5>Update all following</h5>
                            <p>This and all following classes will be updated.</p>
                        </div>
                        </a>
                        <div class="container-fluid confirm-container" style="display: none;">
                            <div class="col-md-12">
                                <h5>Do you want to update all the following class? <a href="#" class="btn btn-xs btn-primary confirm-btn" data-update-type="type_following">Yes</a></h5>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="hidden">submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="cancelClassList" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog sm">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span><div class="modal-header">What would you like to do?</div>
            <div class="modal-body">
                <form method="POST" class="form-inline" id="cancelClassForm" action="{{ url('class/classlist/cancelClassListSchedule') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="action" value="type_one">
                <input type="hidden" name="class_list_id">
                <div class="form-group row">
                    <div class="row type-container">
                        <a href="javascript:void(0)" class="main-btn" data-action="type_one">
                            <div class="col-md-2">
                                <span class="cancel-ico one-class">
                                    <i class="fa fa-calendar-o"></i>
                                </span>
                            </div>
                            <div class="col-md-10">
                                <h5>Cancel this one class?</h5>
                                <p>All other instances of the class will remain.</p>
                            </div>
                            </a>
                        </a>
                        <div class="container-fluid confirm-container">
                            <div class="col-md-offset-1 col-md-11">
                                <h5>Do you want to cancel this one class? <a href="#" onclick="document.getElementById('cancelClassForm').submit()" class="btn btn-xs btn-danger">Yes</a></h5>
                            </div>
                        </div>
                    </div>
                    <div class="row type-container">
                        <a href="javascript:void(0)" class="main-btn" data-action="type_block">
                        <div class="col-md-2">
                            <span class="cancel-ico block-class">
                                <i class="fa fa-calendar-minus-o"></i>
                            </span>
                        </div>
                        <div class="col-md-10">
                            <h5>Cancel a block of related class</h5>
                            <p>Classes with specific date range will be cancelled.</p>
                        </div>
                        </a>
                        <div class="container-fluid confirm-container" style="display: none;">
                            <div class="col-md-offset-1 col-md-11">
                                <div class='input-group date datetimepicker col-md-4'>
                                    <input type='text' class="form-control input-xs start_date_range" name="start_date_range" placeholder="From"  data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" required>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                                <div class='input-group date datetimepicker col-md-4'>
                                    <input type='text' class="form-control input-xs end_date_range" name="end_date_range" placeholder="To"  data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" required>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                                <button type="submit" class="btn btn-danger btn-xs">Confirm</button>
                            </div>
                        </div>
                    </div>
                    <div class="row type-container">
                        <a href="javascript:void(0)" class="main-btn" data-action="type_following">
                        <div class="col-md-2">
                            <span class="cancel-ico all-following">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                        <div class="col-md-10">
                            <h5>Cancel all following</h5>
                            <p>This and all following classes will be cancelled.</p>
                        </div>
                        </a>
                        <div class="container-fluid confirm-container" style="display: none;">
                            <div class="col-md-12">
                                <h5>Do you want to cancel all the following class? <a href="#" onclick="document.getElementById('cancelClassForm').submit()" class="btn btn-xs btn-danger">Yes</a></h5>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Attendance Confirmation -->
<div id="attendanceConfirmation" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog sm">
        <div class="modal-content modlogin">
            <div class="modal-header">Attendance Confirmation</div>
            <div class="modal-body">
                <p class="main-label"></p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" >Cancel</button>
                <button class="btn btn-primary pull-right" data-reservation-id="" data-toggle="" id="confirm-attendance">Confirm</button>
            </div>
        </div>
    </div>
</div>
<!-- Reservation Cancellation -->
<div id="reservationCancellation" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog sm">
        <div class="modal-content modlogin">
            <div class="modal-header">Reservation Cancellation</div>
            <div class="modal-body">
                <p class="main-label"></p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" >No</button>
                <button class="btn btn-primary pull-right" data-reservation-id="" id="cancel-reservation-submit">Confirm</button>   
            </div>
        </div>
    </div>
</div>
<div id="selectPackageModal" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog sm">
        <div class="modal-content">
            <div class="modal-header">Select Package</div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="selectPackage">Select Package From Customer</label>
                    <select type="text" id="selectPackage" name="customer_package_id" class="form-control" placeholder="Select Package">
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="client_data">
                <input type="hidden" name="class_list_id">
                <button class="btn btn-default" data-dismiss="modal" >Cancel</button>
                <button class="btn btn-primary pull-right" id="select-package-submit">Confirm</button>   
            </div>
        </div>
    </div>
</div>
<div id="selectPackageModal-multibook" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog sm">
        <div class="modal-content">
            <div class="modal-header">Select Package</div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="selectPackage2">Select Package From Customer</label>
                    <select type="text" id="selectPackage2" name="customer_package_id" class="form-control" placeholder="Select Package">
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" >Cancel</button>
                <button class="btn btn-primary pull-right" id="multibook-confirm">Confirm</button>
            </div>
        </div>
    </div>
</div>
<div id="multibook-client" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog sm">
        <div class="modal-content">
            <div class="modal-header">Recurring Options</div>
            <form action="">
            <div class="modal-body">
                <div class="form-group">
                    <div>
                        <label for="book-freq-num">Make this reservation every</label>
                        <div class="book-freq-opt row">
                            <div class="col-md-6">
                                <select name="book-freq-num" id="book-freq-num" class="form-control">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <select name="book-freq-lab" id="book-freq-lab" class="form-control">
                                    <option value="weeks">Week(s)</option>
                                    <option value="months">Month(s)</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div>
                        <label for="book-days">Select Days</label>
                        <div class="row select-days-container">
                            <div class="col-md-3 item">
                                <label><input type="checkbox" name="book-days" value="monday"> Mon</label>
                            </div>
                            <div class="col-md-3 item">
                                <label><input type="checkbox" name="book-days" value="tuesday"> Tue</label>
                            </div>
                            <div class="col-md-3 item">
                                <label><input type="checkbox" name="book-days" value="wednesday"> Wed</label>
                            </div>
                            <div class="col-md-3 item">
                                <label><input type="checkbox" name="book-days" value="thursday"> Thu</label>
                            </div>
                            <div class="col-md-3 item">
                                <label><input type="checkbox" name="book-days" value="friday"> Fri</label>
                            </div>
                            <div class="col-md-3 item">
                                <label><input type="checkbox" name="book-days" value="sat"> Sat</label>
                            </div>
                            <div class="col-md-3 item">
                                <label><input type="checkbox" name="book-days" value="sun"> Sun</label>
                            </div>
                        </div>
                    </div>
                    <div>
                        <label for="book-start">Start Date</label>
                        <div>
                            <select name="book-start" id="book-start" class="form-control book-date">
                            </select>
                        </div>
                    </div>
                    <div>
                        <label for="book-end">End Date</label>
                        <div>
                            <select name="book-end" id="book-end" class="form-control book-date">
                            </select>
                        </div>
                    </div>
                    <div class="multibook-total">
                        Total # Reservations: <strong class="book-total">1</strong>
                    </div>
                    <input type="hidden" name="customer_package_id">
                    <input type="hidden" name="class_list_id">
                    <input type="hidden" name="reservation_dates">
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" >Cancel</button>
                <button class="btn btn-primary pull-right" id="multibook-client-submit">Make a recurring reservation</button>
            </div>
            </form>
        </div>
    </div>
</div>