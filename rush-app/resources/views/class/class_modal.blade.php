<!-- Modal: Add Class -->
<div id="addClass" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Add Class</div>
            <div class="modal-body">
                <form method="POST" action="{{ url('class/class') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-12">                            
                            <label for="className">Name</label>
                            <input type='text' class="form-control" name="name" id="className" required>
                        </div>
                        <div class="col-md-12">
                            <label for="classDescription">Description</label>
                            <textarea class="form-control" name="description" id="classDescription" rows="3"></textarea>
                        </div>
                        <div class="col-md-6 hidden branch-container">
                            <label for="classBranches">Branches</label>
                            <select required class="selectpicker form-control" name="branch_ids[]" id="classBranches" multiple title="Select Branches" data-actions-box="true">
                                @if(!empty($branches))
                                    @foreach($branches as $branch)
                                        <option value="{{ $branch['id'] }}">{{ $branch['branchName'] }}</option>
                                    @endforeach
                                @else
                                    <option>No Registered Branch</option>
                                @endif
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="classImage">Image</label>
                            <div class="row">                              
                                <div class="col-md-3">
                                    <img src="{{MigrationHelper::getBaseUrl()}}assets/images/addImage.jpg" class="classImage" width="64px">
                                </div>
                                <div class="col-md-9 design">
                                    <span class="btn btn-default btn-file">
                                        Choose File
                                        <input type="file" accept="image/gif, image/jpeg, image/png" id="classImage" name="image_path" class="fuImage" ew="200" eh="200" required>
                                    </span>
                                    <div class="req-size">Required image size: 200px x 200px</div>
                                    <div id="disp_tmp_path" style="display: none;"></div>
                                </div>
                                <div class="col-md-12">
                                    <span class="error-msg hide">Image does not meet the image size requirement. Please check again.</span>
                                </div>
                            </div>
                        </div>                        
                        <div class="col-md-12 margT20">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button type="submit" class="btn btn-primary pull-right">ADD</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--./ end Add Class -->
<!-- Modal: Edit Class -->
<div id="editClass" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Edit Class</div>
            <div class="modal-body">
                <form method="POST" action="{{ url('class/class') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-12">                            
                            <label for="eclassName">Name</label>
                            <input type='text' class="form-control" name="name" id="eclassName" required>
                        </div>
                        <div class="col-md-12">
                            <label for="eclassDescription">Description</label>
                            <textarea class="form-control" name="description" id="eclassDescription" rows="3"></textarea>
                        </div>
                        <div class="col-md-6 hidden branch-container">
                            <label for="eclassBranches">Branches</label>
                            <select required class="selectpicker form-control" name="branch_ids[]" id="eclassBranches" multiple title="Select Branches" data-actions-box="true">
                                @if(!empty($branches))
                                    @foreach($branches as $branch)
                                        <option value="{{ $branch['id'] }}">{{ $branch['branchName'] }}</option>
                                    @endforeach
                                @else
                                    <option>No Registered Branch</option>
                                @endif
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="eclassImage">Image</label>
                            <div class="row">                              
                                <div class="col-md-3">
                                    <img src="{{MigrationHelper::getBaseUrl()}}assets/images/addImage.jpg" class="classImage" width="64px">
                                </div>
                                <div class="col-md-9 design">
                                    <span class="btn btn-default btn-file">
                                        Choose File
                                        <input type="file" accept="image/gif, image/jpeg, image/png" id="eclassImage" name="image_path" class="fuImage" ew="200" eh="200">
                                    </span>
                                    <div class="req-size">Required image size: 200px x 200px</div>
                                    <div id="disp_tmp_path" style="display: none;"></div>
                                </div>
                                <div class="col-md-12">
                                    <span class="error-msg hide">Image does not meet the image size requirement. Please check again.</span>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="class_id">
                        <div class="col-md-12 margT20">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button type="submit" class="btn btn-primary pull-right">UPDATE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--./ end Edit Class -->
<!--Modal: Delete Class -->
<div id="deleteClass" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Delete Class</div>
            <div class="modal-body">
                <p>Are you sure you want to delete this Class?</p>
                <p class="error-delete-class hidden" style="color: red;"></p>
                <span id="smsToDelete"></span>
                <form method="POST" action="{{ url('class/class') }}" id="delete-class-form">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="DELETE">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button class="btn btn-primary pull-right" id="delete_class_btn">DELETE</button>
                        </div>
                    </div>
                    <input type="hidden" name="class_id">
                </form>
            </div>

        </div>
    </div>
</div>
<!--./ end Delete Class -->