<script src="../page/class/js/jquery.dataTables.yadcf.js"></script>
<script>
var oTable;
$(document).ready(function() {
    'use strict';

	var datepickerDefaults = {
		showTodayButton: true,
		showClear: true
	};
    // CUSTOM TABLE FILTER
    $('#classListTable').dataTable({
        // hidden column but included in filter for grouping
        "columnDefs": [
            {
                "targets": [ 1 ],
                "visible": false,
                "searchable": true
            }
        ],

        "bPaginate" : false,
        "bInfo" : false,

        // group by day
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;

            api.column(1, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                    );

                    last = group;
                }
            } );
        },
        initComplete: function () {
            // reposition filters
            $('#classListTable_filter').remove();
            $('#classListTable_length').insertAfter($('#classListTable_info'));
            $('#classListTable tfoot').appendTo($('.dropdowns'));

            $('#classListTable tfoot th').each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />');
            });
            // DataTable
            var table = $('#classListTable').DataTable();
            
            //Clear All
            $('#clear').click(function(){
                $('#cl_classes, #cl_instructor, #cl_branches').val('');
                table.search('').columns().search('').draw();
            });
            // Filter Classes
            this.api().column(4).every( function () {
                var column = this;
                var select = $('<select id="cl_classes"><option value="">Select Class</option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column.search( val ? '^' + val + '$' : '', true, false ).draw();
                    } );

                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                });
            });

            // Filter Instructor
            this.api().column(5).every( function () {
                var column = this;
                var select = $('<select id="cl_instructor"><option value="">Select Instructor</option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val().trim()
                        );

                        column
                            .search( val ? '^'+val : '', true, false )
                            .draw();
                    } );

                column.data().unique().sort().each( function ( d, j ) {
                    var text = d.replace(' <small style="color: red;">(substitute)</small>', '');

                    var pre_existing_instructors = $('.instructor_filters').filter(function() {
                        return $(this).text() === text;
                    });

                    if(pre_existing_instructors.length <= 0){
                        select.append( '<option class="instructor_filters" value="'+text+'">'+text+'</option>' );
                    }
                });
            });
            // Filter Branches
            this.api().column(6).every( function () {
                var column = this;
                console.log(column);
                var select = $('<select id="cl_branches"><option value="">Select Branch</option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );

                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                });
            });
                
        }
    })
    $('.yadcf-filter-reset-button').html('Reset');
    //Clear All Trigger
    $('.yadcf-filter-reset-button').click(function(){
        $('#clear').click();
    });

    // Customer Profile Viewer
    $("a[href='#customer-details']").each(function(){
        $(this).on("click", function(){
            $(".class-mgmt").hide();
            setTimeout(function() {
                $("#customer-profile-view").toggleClass('hidden');
            }, 400);
            
        });
    });
    $("button[href='#classlists']").on("click", function(){
        setTimeout(function() {
            $(".class-mgmt").show();
        }, 400);
    });
    // Customer Table
    $("#customerpackage").dataTable({
        // hidden column but included in filter for grouping
        "dom": "Brtip",
        buttons: [{
            extend: 'excel',
            title: 'Customer Package'
        }],
        "columnDefs": [
            {
                "targets": [ 1 ],
                "visible": false
            }
        ],

        columns: [
            { data: 'branch', name: 'Branch' },
            { data: 'status', name: 'Status' },
            { data: 'package_name', name: 'Package Name' },
            { data: 'or_no', name: 'OR#' },
            { data: 'amount', name: 'Amount' },
            { data: 'start_date', name: 'Start Date' },
            { data: 'end_date', name: 'End Date' },
            { data: 'remaining', name: 'Remaining' },
        ],

        "ordering": false,

        // group by status
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;

            api.column(1, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="7">'+group+'</td></tr>'
                    );

                    last = group;
                }
            } );
        },
    });    
    $(".exportCustomerP").on("click", function(){
        $("#customerpackage_wrapper .buttons-excel").click();
    });
    // Customer Booking
    $("#customerbookings").dataTable({
        "dom": "Brtip", 
        buttons: [{
            extend: 'excel',
            title: 'Customer Current Booking'
        }],
        columns: [
            { data: 'date', name: 'Date' },
            { data: 'time', name: 'Time' },
            { data: 'package_name', name: 'Package Name' },
            { data: 'class_name', name: 'Class Name' },
            { data: 'branch', name: 'Branch' },
            { data: 'instructor', name: 'Instructor' },
            { data: 'status', name: 'Status' },
        ]
    }).yadcf([
        // filter by date
        {
            column_number: 0,
            filter_type: "range_date",
            datepicker_type: 'bootstrap-datetimepicker',
            date_format: 'YYYY-MM-DD',
            filter_plugin_options: datepickerDefaults,
            filter_container_id: "external_filter_container"
        }
    ]);

    $(".exportCustomerB").on("click", function(){
        $("#customerbookings_wrapper .buttons-excel").click();
    });

    // Customer Booking History
    $("#customerbHistory").dataTable({
        "dom": "Brtip",
        buttons: [{
            extend: 'excel',
            title: 'Customer Booking History'
        }],
        columns: [
            { data: 'date', name: 'Date' },
            { data: 'time', name: 'Time' },
            { data: 'package_name', name: 'Package Name' },
            { data: 'class_name', name: 'Class Name' },
            { data: 'branch', name: 'Branch' },
            { data: 'instructor', name: 'Instructor' },
            { data: 'status', name: 'Status' },
        ]
    }).yadcf([
        // filter by date
        {
            column_number: 0,
            filter_type: "range_date",
            datepicker_type: 'bootstrap-datetimepicker',
            date_format: 'YYYY-MM-DD',
            filter_plugin_options: datepickerDefaults,
            filter_container_id: "previous_booking_date_filter"
        }
    ]);

    $(".exportCustomerH").on("click", function(){
        $("#customerbHistory_wrapper .buttons-excel").click();
    });
});
</script>