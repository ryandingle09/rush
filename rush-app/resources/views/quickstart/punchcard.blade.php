<div class="quickstart punchcard" id="qStart">
    <div class="content clearfix">
        <div class="steps row">
            <div class="col-md-1-5 col-xs-12"><h3>QUICK START</h3></div>
            <div class="col-md-1-7 col-xs-2 active pc1">
                <span>1</span>
                <div class="right">
                    <p class="t">STEP 1</p>
                    <!-- <p class="d">Design your App</p> -->
                </div>
                <img src="{{ MigrationHelper::getBaseUrl() }}assets/images/pointer.png" alt="">
            </div>
            
            <div class="col-md-1-7 col-xs-2 pc2">
                <span>2</span>
                <div class="right">
                    <p class="t">STEP 2</p>
                    <!-- <p class="d">&nbsp;</p> -->
                </div>
                <img src="{{ MigrationHelper::getBaseUrl() }}assets/images/pointer.png" alt="">
            </div>
            
            <div class="col-md-1-7 col-xs-2 pc3">
                <span>3</span>
                <div class="right">
                    <p class="t">STEP 3</p>
                    <!-- <p class="d">&nbsp;</p> -->
                </div>
                <img src="{{ MigrationHelper::getBaseUrl() }}assets/images/pointer.png" alt="">
            </div>

            <div class="col-md-1-7 col-xs-2 pc4">
                <span>4</span>
                <div class="right">
                    <p class="t">STEP 4</p>
                    <!-- <p class="d">&nbsp;</p> -->
                </div>
                <img src="{{ MigrationHelper::getBaseUrl() }}assets/images/pointer.png" alt="">
            </div>            
            
            <div class="col-md-1-7 col-xs-2 pc5">
                <span>5</span>
                <div class="right">
                    <p class="t">FINISH</p>
                    <!-- <p class="d">&nbsp;</p> -->
                </div>
                <img src="{{ MigrationHelper::getBaseUrl() }}assets/images/pointer.png" alt="">
            </div>
        </div><!--steps-->

        <div class="qsWrapper qsOne">
          <form action="{{ url('quicksetup/setupProgram') }}" id="step1">
          {{ csrf_field() }}
          <div class="qsHeader">
              <h2>Name Your Program</h2>
          </div><!--qsheader-->
          <div class="qsBody promos">
              <div class="stampsWrap clearfix">
                  <div class="col-md-12 form-group">
                      <label for="programName" id="programNameLabel">Program Name *</label>
                      <input type="text" class="form-control" name="programName" id="programName" maxlength="128" value="{{ $programName }}" maxlength="80">
                  </div>
                  <div class="col-md-12 form-group">
                      <label for="pointsName" id="pointsNameLabel">Stamp Name *</label>
                      <input type="text" class="form-control" name="pointsName" id="pointsName" maxlength="128" value="{{ $pointsName }}" maxlength="80">
                  </div>

              </div>
          </div><!--qsBody-->

          <div class="qsFooter">
              <!--<a href="#" class="skipQuickStart">Skip Quick Start</a>-->
              <button class="btn btn-default nextStep" type="button">NEXT <i class="fa fa-angle-right"></i></button>
          </div><!--qsFooter-->
        </form>
        </div><!--qsWrapper qsOne-->

        <div class="qsWrapper qsTwo">
          <form action="{{ url('quicksetup/saveMerchantAppDesign') }}" enctype="multipart/form-data" id="step2">
          {{ csrf_field() }}
          <div class="qsHeader">
              <h2>Design Your Merchant Mobile App</h2>
          </div><!--qsheader-->
          <div class="qsBody">
              <div class="design" id="merchantAppDesign">
                  <div class="tabletTab" style="display:block;">
                        <div class="col-md-5">
                            <div class="row left">
                                <div class="col-xs-12 clearfix tabletUploadLogo">
                                    <h5 class="withIco"><i class="number">1</i>Upload Logo</h5>
                                    <span id="appLogoMsg" class="error-msg hide">Image does not meet the image size requirement. Please check again.</br></span>
                                    <img src="{{ $merchant_app['logo'] }}" class="imgPrev" alt="">
                                    <div class="pull-left">
                                    <span class="btn btn-default btn-file">
                                    Choose File <input type="file" name="logo" id="appLogo" class="fuImage" ew="200" eh="200">
                                    </span>
                                        <i>Required Size: 200px x 200px</i>
                                    </div>
                                </div>
                                <div class="col-xs-12 clearfix tabletAddBackImage">
                                    <h5 class="withIco"><i class="number">2</i>Add Background Image</h5>
                                    <span id="appBackgroundMsg" class="error-msg hide">Image does not meet the image size requirement. Please check again.</br></span>
                                    <img src="{{ $merchant_app['background'] }}" class="imgPrev" alt="">
                                    <div class="pull-left">
                                    <span class="btn btn-default btn-file">
                                    Choose File <input type="file" name="background" id="appBackground" class="fuImage" ew="1136" eh="640">
                                    </span>
                                        <i>Required Size: 1136px x 640px</i>
                                    </div>
                                </div>
                                <div class="col-xs-12 clearfix tabletChoosePalete">
                                    <h5 class="withIco"><i class="number">3</i>Choose Color Palette</h5>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            {!! $merchantAppPalette !!}
                                            <button class="btn btn-default savePalette" id="saveMerchantAppPalette" type="button">Save Palette</button>
                                        </div>
                                        <div class="customPaletteBox">
                                            <!-- <div class="col-xs-4">
                                                <i>Theme</i>
                                                <input type='text' id="tpOverlay" name="overlayColor"/>
                                                <span class="hex" id="mThemeVal">#00A8A4</span>
                                            </div> -->
                                            <div class="col-xs-6">
                                                <i>Text</i>
                                                <input type='text' id="tpText" name="textColor" value="{{ $merchant_app['textColor'] }}"/>
                                                <span class="hex" id="mTextVal">#00E6C5</span>
                                            </div>
                                            <div class="col-xs-6">
                                                <i>Buttons</i>
                                                <input type='text' id="tpButtons" name="buttonsColor" value="{{ $merchant_app['buttonsColor'] }}"/>
                                                <span class="hex" id="mButtonsVal">#8BFF6F</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
<!--                                 <div class="col-xs-12 clearfix upStampC">
                                    <h5 class="withIco"><i class="number">4</i>Upload Stamp Design</h5>
                                    <span class="sp">Colored</span>
                                    <img src="{{ $merchant_app['stamp'] }}" class="imgPrev" alt="">
                                    <div class="pull-left">
                                    <span class="btn btn-default btn-file">
                                    Choose File <input type="file" name="stamp">
                                    </span>
                                        <i>At least 200px x 200px</i>
                                    </div>
                                </div>
                                <div class="col-xs-12 clearfix upStampG">
                                    <span class="sp">Grayscale</span>
                                    <img src="{{ $merchant_app['stamp'] }}" class="imgPrev stamp-grayscale" alt="">
 -->                                    <!-- <div class="pull-left">
                                    <span class="btn btn-default btn-file">
                                    Choose File <input type="file" name="uploadFile[]">
                                    </span>
                                        <i>At least 200px x 200px</i>
                                    </div> -->
<!--                                 </div>
 --><!--                                 <div class="col-xs-12">
                                    <button class="btn btn-primary saveDesign">Save Design</button>
                                </div> -->
                            </div>
                        </div>
                        <div class="col-md-7 right">
                             <div class="tabletFrame">
                                 <div class="tabletLayout">
                                    <div class="customTablet">
                                        <img src="assets/images/fidelityLogo.png" alt="" class="tabletCustomLogo">
                                        <button class="btn btn-default customButton">Login</button>
                                    </div>
                                 </div>
                             </div>

                        </div>
                    </div><!--mobileTab-->
              </div>
          </div><!--qsBody-->

          <div class="qsFooter">
              <button class="btn btn-default backStep" type="button"><i class="fa fa-angle-left"></i>BACK</button>
              <button class="btn btn-default nextStep" type="button">NEXT <i class="fa fa-angle-right"></i></button>
          </div><!--qsFooter-->
        </form>
        </div><!--qsWrapper-->

        <div class="qsWrapper qsThree">
            <div class="qsHeader">
                <h2>Design Your Customer Mobile App</h2>
            </div>
            <!--qsheader-->
            <div class="qsBody">
                <div class="design" id="punchcardDesign">
                    <form action="{{ url('quicksetup/saveCustomerAppDesign') }}" method="POST" enctype="multipart/form-data" id="step3">
                    {{ csrf_field() }}
                        <div class="tabletTab" style="display:block;">
                            <div class="col-md-4">
                                <div class="row left">
                                    <div class="col-xs-12 clearfix uploadLogo">
                                        <h5 class="withIco"><i class="number">2</i>Upload Logo</h5>
                                        <span class="error-msg hide">Image does not meet the image size requirement. Please check again.</span>
                                        <img src="{{ $customer_app['logo'] }}" class="imgPrev" alt="">
                                        <div class="pull-left">
                                            <span class="btn btn-default btn-file">
                                            Choose File <input type="file" name="logo" class="fuImage" ew="200" eh="200">
                                            </span>
                                            <i>Required image size: 200px x 200px</i>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 clearfix addBackImage">
                                        <h5 class="withIco"><i class="number">3</i>Add Background Image</h5>
                                        <span class="error-msg hide">Image does not meet the image size requirement. Please check again.</span>
                                        <img src="{{ $customer_app['background'] }}" class="imgPrev" alt="">
                                        <div class="pull-left">
                                            <span class="btn btn-default btn-file">
                                            Choose File <input type="file" name="background" class="fuImage" ew="640" eh="1136">
                                            </span>
                                            <i>Required image size: 640px x 1136px</i>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 clearfix choosePalete">
                                        <h5 class="withIco"><i class="number">4</i>Choose Color Palette</h5>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                {!! $customerAppPalette !!}
                                                <button class="btn btn-default savePalette" id="saveCustomerAppPalette" type="button">Save Palette</button>
                                            </div>
                                            <div class="customPaletteBox">
                                                <div class="col-xs-6">
                                                    <i>Theme</i>
                                                    <input type='text' id="pOverlay" name="overlayColor" value="{{ $customer_app['overlayColor'] }}" />
                                                    <span class="hex" id="cThemeVal">#00A8A4</span>
                                                </div>
                                                <div class="col-xs-6">
                                                    <i>Text</i>
                                                    <input type='text' id="pText" name="textColor" value="{{ $customer_app['textColor'] }}" />
                                                    <span class="hex" id="cTextVal">#00E6C5</span>
                                                </div>
                                                <div class="col-xs-6">
                                                    <i>Buttons</i>
                                                    <input type='text' id="pButtons" name="buttonsColor" value="{{ $customer_app['buttonsColor'] }}" />
                                                    <span class="hex" id="cButtonsVal">#8BFF6F</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 clearfix upStampC">
                                        <h5 class="withIco"><i class="number">5</i>Upload Stamp Design</h5>
                                        <span class="sp">Colored</span>
                                        <span class="error-msg hide">Image does not meet the image size requirement. Please check again.</span>
                                        <img src="{{ $customer_app['stamp'] }}" class="imgPrev" alt="">
                                        <div class="pull-left">
                                            <span class="btn btn-default btn-file">
                                            Choose File <input type="file" name="stamp" class="fuImage" ew="200" eh="200">
                                            </span>
                                            <i>Required image size: 200px x 200px</i>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 clearfix upStampG">
                                        <span class="sp">Grayscale</span>
                                        <div class="col-xs-4">
                                            <img src="{{ $customer_app['stamp'] }}" class="imgPrev stamp-grayscale" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 right">
                                <div class="mobileFrame">
                                    <div class="col-xs-6">
                                        <div class="screen1">
                                            <div class="customMobileBgImg">
                                                <div class="customMobileBgColor">
                                                    <div class="inside"><img src="assets/images/fidelityLogo.png" alt="" class="customLogo"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="screen2">
                                            <div class="screen2box">
                                                <div class="topPane"></div>
                                                <div class="text customText">
                                                    <p>Get 1 stamp for every succeeding amount of P60000 ordered. Complete 5-10 stamps to get awesome prizes!. You can claim your rewards at all Vikings Branch.</p>
                                                </div>
                                                <div class="stampWrap">
                                                    <ul>
                                                        <li class="c">1</li>
                                                        <li class="c">2</li>
                                                        <li class="c">3</li>
                                                        <li class="c">4</li>
                                                        <li class="c">5</li>
                                                        <li class="g">6</li>
                                                        <li class="g">7</li>
                                                        <li class="g">8</li>
                                                        <li class="g">9</li>
                                                    </ul>
                                                </div>
                                                <!--stampWrap-->
                                                <button class="redeem btn" type="button">REDEEM</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--mobileFrame-->
                            </div>
                        </div>
                        <!--mobileTab-->
                    </form>
                </div>
            </div>
            <!--qsBody-->
            <div class="qsFooter">
                <button class="btn btn-default backStep" type="button"><i class="fa fa-angle-left"></i>BACK</button>
                <button class="btn btn-default nextStep" type="button">NEXT <i class="fa fa-angle-right"></i></button>
            </div>
        </div>
        <!--qsWrapper-->

        <div class="qsWrapper qsFour">
          <form action="{{ url('quicksetup/saveApplicationDetails') }}" method="POST" enctype="multipart/form-data" id="step4">
          {{ csrf_field() }}
            <div class="qsHeader">
                <h2>Provide Your App Details</h2>
            </div><!--qsheader-->
            <div class="qsBody">
                <div class="stampsWrap clearfix step5 design">
                    <div class="col-md-12 form-group">
                        <label for="appName" id="appNameLabel">App Name *</label>
                        <input type="text" class="form-control" name="appName" id="appName" value="{{ $appName }}">
                    </div>
                    <div class="col-md-12 form-group">
                        <label for="appDescription" id="appDescriptionLabel">Description *</label>
                        <input type="text" class="form-control" name="appDescription" id="appDescription" value="{{ $appDescription }}">
                    </div>
                    <div class="col-md-12 form-group">
                        <label for="appLogo">Appstore Logo *</label>
                        <div class="appstoreLogo">
                            <span class="error-msg hide">Image does not meet the image size requirement. Please check again.</span>
                            <img src="{{ $appLogo }}" class="imgPrev" alt="" id="logoPrev">
                            <span class="btn btn-default btn-file ">
                              Choose File <input type="file" name="logo" id="logo" class="fuImage" ew="1024" eh="1024">
                          </span>
                            <i>Required Size: 1024 x 1024</i>
                        </div>
                    </div>
                </div>
            </div><!--qsBody-->

            <div class="qsFooter">
              <button class="btn btn-default backStep" type="button"><i class="fa fa-angle-left"></i>BACK</button>
              <button class="btn btn-default nextStep" type="button">NEXT <i class="fa fa-angle-right"></i></button>
            </div><!--qsFooter-->
          </form>
        </div><!--qsWrapper-->

        <div class="qsWrapper qsFinish">
          <form action="">
            <div class="qsHeader">
                <h2>Finish Setup</h2>
            </div><!--qsheader-->
            <div class="qsBody">
                 <h4>Your loyalty program will be ready in 3 to 5 business days. We will send you an update via email.</h4>
                <button class="btn btn-default skipQuickStart" type="button">Go to Dashboard  <i class="fa fa-angle-right"></i></button>
                <!-- <button class="btn btn-default nextStep" onclick="location.href='account'" type="button">Change default password  <i class="fa fa-angle-right"></i></button> -->
            </div><!--qsBody-->

            <div class="qsFooter">

            </div><!--qsFooter-->
          </form>
        </div><!--qsWrapper-->

    </div><!--content-->

</div><!--quickstart-->
