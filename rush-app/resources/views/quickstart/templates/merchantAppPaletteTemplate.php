                                                <div class="dropdown presetPalette">
                                                    <button class="btn btn-default dropdown-toggle" type="button" id="tabletPrePalette" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                        <span class="tp2"></span><span class="tp3"></span>
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu prePaletteMnu" aria-labelledby="tabletPrePalette" id="merchantAppPalette">
                                                        <?php foreach($palettes as $palette) { ?>
                                                        <li>
                                                            <a href="" onclick="return false">
                                                                <i style="background-color: <?php echo $palette['g']; ?>"></i>
                                                                <i style="background-color: <?php echo $palette['b']; ?>"></i>
                                                            </a>
                                                            <span><?php echo $palette['name']; ?></span>
                                                        </li>
                                                        <?php } ?>
                                                    </ul>
                                                </div>