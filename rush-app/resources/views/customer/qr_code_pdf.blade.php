<center>
    <h2>{{ $branch_name }} - {{ $qr_code->name }}</h2>
    <b>{{ $qr_code->points }} Points</b><br/><br/>
    <img  src="{{ $qr_code_image_url }}"/>
</center>