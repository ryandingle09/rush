<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,300,300italic,400italic,600italic,700,700italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ url('css/vendor/bootstrap.css') }}">
    <title>{{ $title or "CMS - RUSH" }}</title>
</head>
<body>
    <div class="container" style="margin-top: 20px;">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @elseif (session('success'))
                    <div class="alert alert-success">
                        {!! session('success') !!}
                    </div>
                @endif
                <form method="post" action="{{ url('/customers/register') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="inputFirstName">First Name</label>
                        <input type="text"
                               class="form-control"
                               id="inputFirstName"
                               name="first_name"
                               placeholder="Enter First Name"
                               value="{{ old('first_name') }}"
                               required>
                    </div>
                    <div class="form-group">
                        <label for="inputLastName">Last Name</label>
                        <input type="text"
                               class="form-control"
                               id="inputLastName"
                               name="last_name"
                               placeholder="Enter Last Name"
                               value="{{ old('last_name') }}"
                               required>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail">Email</label>
                        <input type="email"
                               class="form-control"
                               id="inputEmail"
                               name="email"
                               placeholder="Enter Email"
                               value="{{ old('email') }}"
                               required>
                    </div>
                    <div class="form-group">
                        <label for="inputMobileNumber">Mobile Number</label>
                        <div class="input-group">
                            <span class="input-group-addon">+63</span>
                            <input type="text"
                                   class="form-control"
                                   id="inputMobileNumber"
                                   name="mobile_no"
                                   placeholder="Enter Mobile Number"
                                   value="{{ old('mobile_no') }}"
                                   required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputCompany">Company</label>
                        <input type="text"
                               class="form-control"
                               id="inputCompany"
                               name="company"
                               placeholder="Enter Company"
                               value="{{ old('company') }}"
                               required>
                    </div>
                    <div class="form-group">
                        <label for="inputDesignation">Designation</label>
                        <input type="text"
                               class="form-control"
                               id="inputDesignation"
                               name="designation"
                               placeholder="Enter Designation"
                               value="{{ old('designation') }}"
                               required>
                    </div>
                    <div class="form-group">
                        <label for="inputIndustry">Industry</label>
                        <select class="form-control"
                                id="inputIndustry"
                                name="industry"
                                required>
                            <option value="">Choose Industry</option>
                            <option value="Manufacturing"
                                    {{ old('industry') == "Manufacturing" ? "selected=\"selected\"" : "" }}>
                                        Manufacturing</option>
                            <option value="Education"
                                    {{ old('industry') == "Education" ? "selected=\"selected\"" : "" }}>
                                        Education</option>
                            <option value="Banking"
                                    {{ old('industry') == "Banking" ? "selected=\"selected\"" : "" }}>
                                        Banking &amp; Finance</option>
                            <option value="BPO"
                                    {{ old('industry') == "BPO" ? "selected=\"selected\"" : "" }}>
                                        BPO</option>
                            <option value="Hotel, Restaurant and Cafe"
                                    {{ old('industry') == "Hotel, Restaurant and Cafe" ? "selected=\"selected\"" : "" }}>
                                        Hotel, Restaurant &amp; Cafe</option>
                            <option value="Wholesale and Retail"
                                    {{ old('industry') == "Wholesale and Retail" ? "selected=\"selected\"" : "" }}>
                                        Wholesale &amp; Retail</option>
                            <option value="Services"
                                    {{ old('industry') == "Services" ? "selected=\"selected\"" : "" }}>
                                        Services</option>
                            <option value="Government"
                                    {{ old('industry') == "Government" ? "selected=\"selected\"" : "" }}>
                                        Government</option>
                            <option value="IT"
                                    {{ old('industry') == "IT" ? "selected=\"selected\"" : "" }}>
                                        IT</option>
                            <option value="Others"
                                    {{ old('industry') == "Others" ? "selected=\"selected\"" : "" }}>
                                        Others</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="{{ url('page/cms/js/jquery-1.11.3.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('page/cms/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript">
    </script>
</body>
</html>