<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,300,300italic,400italic,600italic,700,700italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ url('css/vendor/bootstrap.css') }}">
    <title>{{ $title or "CMS - RUSH" }}</title>
</head>
<body>
    <div class="container" style="margin-top: 20px;">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $key => $error)
                                @if ($key != 'invalid_file')
                                    <b>{{ $key }}:</b>
                                    <ul>
                                        <?php
                                        $messages = collect(json_decode($error))->flatten(1)->toArray();
                                        ?>
                                        @foreach($messages as $message)
                                            <li>{{ $message }}</li>
                                        @endforeach
                                    </ul>
                                @else
                                    <li>{{ $error }}</li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                @elseif (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                <form method="post"
                      action="{{ url('/customers/importFromCSV') }}"
                      enctype="multipart/form-data"
                    >
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="inputCSVFile">CSV</label>
                        <input type="file"
                               class="form-control"
                               id="inputCSVFile"
                               name="csv_import"
                               required>
                    </div>
                    <button type="submit" class="btn btn-primary pull-right">Import</button>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="{{ url('page/cms/js/jquery-1.11.3.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('page/cms/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript">
    </script>
</body>
</html>