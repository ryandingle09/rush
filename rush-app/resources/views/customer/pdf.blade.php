<!doctype html>
<html lang="en" xmlns:background-size="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=j, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body style="margin: 0in">
@foreach($customers as $customer)
    <?php
        $path = public_path("/assets/images/integrate-id-template.png");
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $background_image ='data:image/' . $type . ';base64,' . base64_encode($data);
    ?>
    <div class="page"
         style="page-break-after:always;margin: 0in;">
        <div style="
                 position: relative;
                 width: 800px;
                 height: 1131px;
                 background-image:url('{!! $background_image !!}');
                 background-repeat:no-repeat;
                 background-size: 100%;">
            <?php
                $top = 693;
                if (strlen($customer->name) > 25) {
                    $top = 658;
                }
            ?>
        <h2 style="position: absolute;
                width: 100%;
                top: {{ $top  }}px;
                font-size: 33px;
                text-align: center;
                text-transform: uppercase;
                color: #000;
                }">{{ $customer->name }}</h2>

            <h2 style="
    position: absolute;
    width: 100%;
    top: 742px;
    font-size: 20px;
    text-align: center;
    text-transform: uppercase;
    color: #000;">{{ $customer->middleName }}</h2>
        {{--<p>--}}
            {{--Mobile Number: {{ $customer->mobile_no }}--}}
        {{--</p>--}}
        {{--<p>--}}
            {{--Client Code: {{ $customer->client_code }}--}}
        {{--</p>--}}

            <?php
            $path = realpath(base_path('../'. $customer->qr->image_url));
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);
            $qr_code__image ='data:image/' . $type . ';base64,' . base64_encode($data);
            ?>
        <p>
            @if ($customer->qr && $customer->qr->image_url)
                <img src="{!! $qr_code__image !!}"
                     style="width: 243px;
                            height: 243px;
                            position: absolute;
                            bottom: 19px;
                            left: 279px;">
            @else
                No Qr code image
            @endif
        </p>
        </div>
    </div>
@endforeach
</body>
</html>
