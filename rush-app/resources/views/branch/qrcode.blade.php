@foreach($branches as $branch)
    <div class="page" style="page-break-after:always;">
        <h2>Booth: {{ $branch['name'] }}</h2>
        <div><br></div>
        @if($branch['date_qr_url'])
            <img src="{{ $branch['date_qr_url'] }}"/>
            <p>Date: {{ $branch['date'] }}</p>
        @else
            <p>No QR</p>
        @endif
    </div>
@endforeach
