@if (count($errors) > 0)
    <div class="alert alert-danger spacing">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<button href="#addVoucherItem" class="btn btn-primary" data-toggle="modal" onclick="return false">Add Item</button>
<!-- <button href="#voucherBulkImport" class="btn btn-primary" data-toggle="modal" onclick="return false">Bulk Import</button> -->
<table id="vouchersTable" class="data-table" data-toggle="table" data-search="true" data-pagination="true">
    <thead>
    <tr>
        <th data-sortable="true">ID</th>
        <th data-sortable="true">Voucher Name</th>
        <th>Description</th>
        <th data-sortable="true">Branch</th>
        <th>Issued</th>
        <th>Available</th>
        <!-- <th>Threshold</th> -->
        <th data-sortable="true">Status</th>
        <th class="action">Action</th>
        <th>Import</th>
    </tr>
    </thead>
    @if ( $vouchers )
    @foreach( $vouchers as $voucher)
        <tr>
            <td>{{ $voucher->id }}</td>
            <td>{{ $voucher->name }}</td>
            <td>{{ $voucher->description }}</td>
            <td>{{ $voucher->branch->branchName }}</td>
            <td class="text-center">{{ $voucher->issued }}</td>
            <td class="text-center">{{ $voucher->available }}</td>
            <!-- <td class="text-center">{{ $voucher->threshold }}</td> -->
            <td>{{ $voucher->enabled_label }}</td>
            <td>
                <a href="#deleteVoucherItem" id="deleteVoucherItemButton" data-toggle="modal" onclick="return false" class="delete" data-id="{{$voucher->id}}"><i class="fa fa-trash"></i> Delete</a>
                <a href="#editVoucherItem" id="editVoucherItemButton" data-toggle="modal" onclick="return false" class="edit" data-id="{{$voucher->id}}" data-name="{{ $voucher->name }}" data-desc="{{ $voucher->description }}" data-threshold="{{ $voucher->threshold }}"><i class="fa fa-edit"></i> Edit</a>
            </td>
            <td>
                <a href="#voucherCodeImport" id="voucherCodeImportButton" data-toggle="modal" data-id="{{$voucher->id}}" onclick="return false" class="import"><i class="fa fa-sign-in"></i></a>
            </td>
        </tr>
    @endforeach
    @endif
</table>

@section('catalog_modals')
@parent
<!-- Modals -->
<div id="addVoucherItem" class="modal fade voucher-modal" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Item Details</div>
            <div class="modal-body">
                <form action="{{ url('redemption/voucher/add') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <label for="voucher-name">Voucher Name</label>
                            <input type="text" name="voucher-name" id="voucher-name" class="form-control">
                        </div>
                        <!-- <div class="col-md-6">
                            <label for="threshold">Threshold</label>
                            <input type="number" name="threshold" id="threshold" class="form-control">
                        </div> -->
                        <div class="col-md-6">
                            <label for="voucher-description">Description</label>
                            <textarea name="voucher-description" id="voucher-description" class="form-control" rows="5"></textarea>
                        </div>
                        <!-- <div class="col-md-6">
                            <div class="row form-group">
                                <div class="col-md-10 padding-right-0">
                                    <label for="import-voucher-code">Import Voucher Code</label>
                                    <input type="text" name="" id="" class="form-control filename-vc">
                                    <input type="file" name="import-voucher-code" id="import-voucher-code" class="file-vc" accept=".csv">
                                </div>
                                <div class="col-md-2">
                                    <label>&nbsp;</label>
                                    <button type="button" class="browse-vc"><i class="fa fa-upload"></i></button>
                                </div>
                            </div>
                        </div> -->
                        <div class="col-md-6">
                            <div class="form-group branches">
                                <label class="voucherBranch">Branches</label>
                                <label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0" style="float: right;height:0;cursor:pointer;">
                                    <input type="checkbox" class="custom-control-input select-all" name="select-all">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Select All</span>
                                </label>
                                <div class="branch-check-wrap">
                                    @if ( $branches )
                                        @foreach( $branches as $branch)
                                        <div class="form-check">
                                            <input type="checkbox" name="branch_ids[]" class="checkbox options branch-options" value="{{ $branch['id'] }}" id="branch_{{ $branch['id'] }}" />
                                            <label class="form-check-label branch-label" for=""> {{ $branch['branchName'] }}
                                                <i class="fa fa-check" aria-hidden="true" style="display: none;"></i>                                      
                                            </label>
                                        </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <button type="button" class="btn btn-default clear-voucher">Clear</button>
                        <button type="submit" class="btn btn-primary pull-right">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="editVoucherItem" class="modal fade voucher-modal" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Item Details</div>
            <div class="modal-body">
                <form action="{{ url('redemption/voucher/edit') }}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="voucher_id" id="evoucher_id" />
                    <div class="row">
                        <div class="col-md-6">
                            <label for="evoucher-name">Voucher Name</label>
                            <input type="text" name="evoucher-name" id="evoucher-name" class="form-control">
                        </div>
                        <!-- <div class="col-md-6">
                            <label for="ethreshold">Threshold</label>
                            <input type="number" name="ethreshold" id="ethreshold" class="form-control">
                        </div> -->
                        <div class="col-md-6">
                            <label for="evoucher-description">Description</label>
                            <textarea name="evoucher-description" id="evoucher-description" class="form-control" rows="5"></textarea>
                        </div>
                        <!-- <div class="col-md-6">
                            <div class="row form-group">
                                <div class="col-md-10 padding-right-0">
                                    <label for="eimport-voucher-code">Import Voucher Code</label>
                                    <input type="text" name="" id="" class="form-control filename-vc">
                                    <input type="file" name="eimport-voucher-code" id="eimport-voucher-code" class="file-vc" accept=".csv">
                                </div>
                                <div class="col-md-2">
                                    <label>&nbsp;</label>
                                    <button type="button" class="browse-vc"><i class="fa fa-upload"></i></button>
                                </div>
                            </div>
                        </div> -->
                    </div>
                    <div class="row">
                        <button type="button" class="btn btn-default clear-voucher">Clear</button>
                        <button type="submit" class="btn btn-primary pull-right">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="deleteVoucherItem" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Delete Voucher</div>
            <div class="modal-body">
                <p>Are you sure you want to delete this item?</p>
                <form action="{{ url('redemption/voucher/delete') }}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="voucher_id" id="dvoucher_id" />
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button class="btn btn-primary pull-right" id="delete_class_btn">DELETE</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<div id="voucherCodeImport" class="modal fade voucher-modal" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Import Voucher Codes</div>
            <div class="modal-body">
                <form action="{{ url('redemption/voucher/uploadcodes') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="voucher_id" id="upload_voucher_id" />
                    <div class="form-group row">
                        <div class="col-md-2">
                            <button type="button" class="browse-vc padding-lr-12">Browse</button>
                        </div>
                        <div class="col-md-10">
                            <input type="text" name="vc-filename" id="vc-filename" class="form-control filename-vc">
                            <input type="file" name="voucher-import" id="voucher-import" class="form-control file-vc" accept=".csv">
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-default clear-voucher">Clear</button>
                            <button class="btn btn-primary pull-right">Import</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<div id="voucherBulkImport" class="modal fade voucher-modal" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Bulk Import Voucher Codes</div>
            <div class="modal-body">
                <form action="" id="">
                    <div class="form-group row">
                        <div class="col-md-2">
                            <button type="button" class="browse-vc padding-lr-12">Browse</button>
                        </div>
                        <div class="col-md-10">
                            <input type="text" name="vc-filename" id="vc-filename" class="form-control filename-vc">
                            <input type="file" name="bulk-import" id="bulk-import" class="form-control file-vc" accept=".csv">
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-default clear-voucher">Clear</button>
                            <button class="btn btn-primary pull-right">Import</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
@endsection

@section('footerPlaceholder')
@parent
<script type="text/javascript">
// Toggle Select All
$('.voucher-modal .branches .select-all').on('click', function() {
    if($('.voucher-modal .branches .select-all').is(':checked')){
        $('.voucher-modal .branches .checkbox').prop('checked', true);
        $(".voucher-modal .branches .fa-check").css("display", "block");
    }
    else
    {
        $('.voucher-modal .branches .checkbox').prop('checked', false);
        $(".voucher-modal .branches .fa-check").css("display", "none");
    }
});
$('.voucher-modal .ebranches .select-all').on('click', function() {
    if($('.voucher-modal .ebranches .select-all').is(':checked')){
        $('.voucher-modal .ebranches .checkbox').prop('checked', true);
        $(".voucher-modal .ebranches .fa-check").css("display", "block");
    }
    else
    {
        $('.voucher-modal .ebranches .checkbox').prop('checked', false);
        $(".voucher-modal .ebranches .fa-check").css("display", "none");
    }
});
// Select Branch Individualy
$('.voucher-modal .branch-label').on('click', function(){
    var _checkBranch = $(this).siblings('.branch-options');
    var _check = $(this).find('.fa-check');
    
    if( _checkBranch.is(':checked') == false ) {
        _checkBranch.prop('checked', true);
        _check.css('display', 'block');
    }
    else {
        _checkBranch.prop('checked', false);
        _check.css('display', 'none');
    }
});
// Mechanics Prod Default on Load
$('#editVoucherItem').on('show.bs.modal', function(){
    if($('#editVoucherItem .ebranches .select-all').is(':checked')){
        $('#editVoucherItem .ebranches .checkbox').prop('checked', true);
        $("#editVoucherItem .ebranches .fa-check").css("display", "block");
    }
    else
    {
        $('#editVoucherItem .ebranches .checkbox').prop('checked', false);
        $("#editVoucherItem .ebranches .fa-check").css("display", "none");
    }
    $('#editVoucherItem .ebranches .checkbox').each(function(){
        if( $(this).attr('checked') ) $(this).siblings('label').click();
    });
});
// Clear Fields
$('.clear-voucher').on('click', function(){
    $(this).closest('form').find('input, textarea, input[type="file"]').val('');
    $(this).closest('form').find('.select-all').prop('checked', false);
    $(this).closest('form').find('.fa-check').css('display', 'none');
});
// Bulk Import Voucher Code
$('.browse-vc').on('click', function(){
    $(this).closest('.form-group').find('.file-vc').click();
});
$('.file-vc').on('change', function(e){
    var _voucherCode = e.target.files[0].name;
    $(this).siblings('.filename-vc').val(_voucherCode);
});
//Toggle Status
$(document).ready(function(){
    $('.toggle-status').on('click', function(){
        if( $(this).hasClass('enabled') ) $(this).removeClass('enabled').addClass('disabled').text('Disabled');
        else $(this).removeClass('disabled').addClass('enabled').text('Enabled');
    });

    $("#vouchersTable").on("click", "#editVoucherItemButton", function() {
        $('#evoucher_id').val( $(this).data('id'));
        $('#evoucher-name').val( $(this).data('name'));
        $('#evoucher-description').val( $(this).data('desc'));
        $('#ethreshold').val( $(this).data('threshold'));
    });

    $("#vouchersTable").on("click", "#deleteVoucherItemButton", function() {
        $('#dvoucher_id').val( $(this).data('id'));
    });

    $("#vouchersTable").on("click", "#voucherCodeImportButton", function() {
        $('#upload_voucher_id').val( $(this).data('id'));
    });
    
    var _searchAlert = $('.search');
    setTimeout(function(){ if ( $('.alert').is(':visible') ) _searchAlert.addClass('adjustTop'); }, 300);
});
</script>
@endsection