@extends('layouts.app')

@section('headerPlaceholder')
<!-- Additional Header items -->
<style>
label.error { color: red !important; }
</style>
<!-- Voucher Management css -->
<link rel="stylesheet" href="page/voucher/css/style.css">
@endsection

@section('view')
<div class="container">
    <div id="content" class="management marginTop">
    
    <!-- Nav tabs -->
    <div class="navTabsWrap">
        <ul class="nav nav-tabs" role="tablist" id="tabs">
            <li role="presentation" class="active"><a href="#reward" aria-controls="reward" role="tab" data-toggle="tab">REWARD</a></li>
            @if ( $merchant->settings->voucher_module )
                <li role="presentation"><a href="#voucher" aria-controls="voucher" role="tab" data-toggle="tab">VOUCHER</a></li>
            @endif
        </ul>
    </div>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="reward">
            <div class="redemption spacing">
                <div class="row">
                <div class="col-md-12">
                    <h4>Items to be redeemed</h4>
                </div>
                <div class="itemsRedeemed webLabels">
                    <div class="col-md-1 numbering">
                        <label for="rProdCount">&nbsp;</label>
                    </div>                        
                    <div class="col-md-1 itemNameLabel redemption-header-wrap">
                        <label for="rProdName redemption-header">Product Name</label>
                    </div>
                    <div class="col-md-2 redemption-header-wrap">
                        <label for="rProdImg redemption-header">Product Image</label>
                    </div>
                    <div class="col-md-2  redemption-header-wrap">
                        <label for="rDescription redemption-header">Description</label>
                    </div>
                    <div class="col-md-2  redemption-header-wrap">
                        <label for="rDescription redemption-header">Points</label>
                    </div>
                    <div class="col-md-1  redemption-header-wrap">
                        <label for="rDescription redemption-header">Status</label>
                    </div>
                    <div class="col-md-1  redemption-header-wrap">
                        <label for="rDescription redemption-header">Actions</label>
                    </div>
                </div><!--itemsRedeemed-->
                <div id="redeem-rows">
                    {{-- */
                      $i = 1;
                    /* --}}
                    @if( $admin_rewards )
                    @foreach( $admin_rewards as $reward)
                    <div class="itemsRedeemed">
                        <div class="col-md-1 numbering">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="number">{{ $i }}</i></span>
                            </div>
                        </div>
                        <div class="itemDetails">
                            <div class="col-md-1 itemName">
                                <div class="input-group">
                                    <span class="redemption-name">{{ $reward->name }}</span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group rProdImg redemption-image">
                                  <img class="imgPrev" alt="" src="{{ $reward->imageURL }}">
                                </div>
                            </div>
                            <div class="col-md-2 itemDesc">
                                <div class="input-group">
                                  <span class="redemption-description">{{ $reward->details }}</span>
                                </div>
                            </div>
                            <div class="col-md-2 itemPts">
                                <div class="input-group">
                                {{--*/
                                    $points_calc = $reward->pointsRequired / $redemption_peso;
                                    $points_calc = round($points_calc * $redemption_points);
                                /*--}}
                                  <span class="redemption-points">{{ $points_calc }} pts</span>
                                </div>
                            </div>
                            <div class="col-md-1 itemStatus">
                                @if ( in_array( $reward->id, $merchant_admin_rewards) )
                                    <a class="toggle-globe-reward enabled" data-id={{ $reward->id }}>enabled</a>
                                    <input type="hidden" id="admin-reward-{{ $reward->id }}" name="admin-reward-{{ $reward->id }}" value="1" />
                                @else
                                    <a class="toggle-globe-reward disabled" data-id={{ $reward->id }}>disabled</a>
                                    <input type="hidden" id="admin-reward-{{ $reward->id }}" name="admin-reward-{{ $reward->id }}" value="0" />
                                @endif
                            </div>
                        </div>
                    </div>
                    {{-- */
                      $i++;
                    /* --}}
                    @endforeach
                    @endif
                    @if($redemptions)
                    @foreach($redemptions as $redemption)
                    <div class="itemsRedeemed item_{{ $redemption->redeemItemId }}">
                        <div class="col-md-1 numbering">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="number">{{ $i }}</i></span>
                            </div>
                        </div>
                        <div class="itemDetails">
                            <div class="col-md-1 itemName">
                                <div class="input-group">
                                    <span class="redemption-name">{{ $redemption->name }}</span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group rProdImg redemption-image">
                                  <img class="imgPrev" alt="" src="{{ $redemption->imageURL }}">
                                </div>
                            </div>
                            <div class="col-md-2 itemDesc">
                                <div class="input-group">
                                  <span class="redemption-description">{{ $redemption->details }}</span>
                                </div>
                            </div>
                            <div class="col-md-2 itemPts">
                                <div class="input-group">
                                  <span class="redemption-points">{{ $redemption->pointsRequired }} pts</span>
                                </div>
                            </div>
                            <div class="col-md-1 itemStatus">
                                <div class="input-group">
                                  &nbsp;
                                </div>
                            </div>
                            <div class="col-md-1 actions">
                                <a href="" onclick="return false"><i class="fa edit fa-pencil x_edit-redeem" data-toggle="modal" data-id="{{ $redemption->redeemItemId }}" data-target="#editRedeemModal" data-name="{{ $redemption->name }}" data-desc="{{ $redemption->details }}" data-points="{{ $redemption->pointsRequired }}" data-branch="{{ $redemption->branch_ids }}" data-img="{{ $redemption->imageURL }}" style="margin: 0;"></i></a>
                                <a href="" onclick="return false"><i class="fa del fa-times x_remove-redeem" data-toggle="modal" data-target="#deleteRedeemModal" data-id="{{ $redemption->redeemItemId }}"></i></a>
                            </div>
                        </div>
                    </div><!--itemsRedeemed-->
                    {{-- */
                      $i++;
                    /* --}}
                    @endforeach
                    @endif
                </div>
                <div class="col-md-12">
                    <button class="btn btn-default x_addRedeemItem" type="button" data-toggle="modal" data-target="#editRedeemModal"><i class="fa fa-plus"></i> Add Item</button>
                </div>
                </div>
            </div>
        </div>
        @if ( $merchant->settings->voucher_module )
        <div role="tabpanel" class="tab-pane" id="voucher">
            <!-- Vouchers Start -->
            @include('redemption.vouchers')
            <!-- Vouchers End -->
        </div>
        @endif
    </div>

    </div>
</div>
    
@section('catalog_modals')
@show
<div class="modal fade rewards-item-modal" id="editRedeemModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Item Details</h4>
                </div>
                <div class="modal-body">
                    <form id="editRedeemForm">
                    {{ csrf_field() }}
                        <div class="left-col">
                            <div class="form-group">
                                <label for="modal_productName" class="input-label">Product Name</label>
                                <input type="text" class="form-control" id="modal_productName" name="product_name" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="modal_productDesc" class="input-label">Description</label>
                                <textarea class="form-control" id="modal_productDesc" name="product_desc" rows="4"></textarea>
                            </div>
                            <div class="form-group branches">
                                <label class="input-label">Branches</label>
                                <label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0" style="float: right;height:0;cursor:pointer;">
                                    <input type="checkbox" class="custom-control-input select-all" name="select-all">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Select All</span>
                                </label>
                                <div class="branch-check-wrap">
                                    @if ( $branches )
                                      @foreach( $branches as $branch)
                                      <div class="form-check">
                                        <div class="form-check">
                                            <input type="checkbox" name="branches[]" class="checkbox options branch-options" value="{{ $branch['id'] }}" id="branch_{{ $branch['id'] }}" />
                                            <label class="form-check-label branch-label" for="branch_{{ $branch['id'] }}"> {{ $branch['branchName'] }}
                                                <i class="fa fa-check" id="branch_{{ $branch['id'] }}_check" aria-hidden="true" style="display: none;"></i>
                                            </label>
                                        </div>
                                      </div>
                                      @endforeach
                                      @endif
                                </div>
                            </div>
                        </div>
                        <div class="right-col">
                            <label class="input-label">Image</label>
                            <div class="preview-img" style="background: #f7f7f7;">
                                <label for="file" class="btn" style="border:none;">Browse</label>
                            </div>
                            <div style="margin-top: 12px;">
                                <label class="custom-file" style="display: inline-block;width:100%;">
                                    <input type="hidden" name="file_change" id="file_change" value='0' />
                                    <input type="file" name="img_file" id="file" class="custom-file-input" style="display:none;" accept="image/*">
                                    <br />
                                    <span style="font-size: 12px;"><i>Required size is 350px x 200px</i></span>
                                </label>
                            </div><br>
                            <div class="form-group points">
                              <label for="modal_points" class="col-2 col-form-label input-label">Points</label>
                              <div>
                                <input class="form-control" name="points" type="number" value="0" id="modal_points">
                              </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-clear-redeem">Clear</button>
                    <button type="button" class="btn btn-default btn-save-redeem">Save</button>
                </div>
            </div>
        </div>
</div>

<div class="modal fade rewards-item-modal" id="deleteRedeemModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Are you sure you want to delete reward?</h4>
                </div>
                <div class="modal-body text-center">
                    <form id="deleteRedeemForm">
                    {{ csrf_field() }}
                    <button type="button" class="btn btn-default btn-delete-no" data-dismiss="modal">No</button>
                    <button type="button" class="btn btn-default btn-delete-yes">Yes</button>
                    </form>
                </div>
            </div>
        </div>
</div>

@endsection

@section('footerPlaceholder')
@parent
<script type="text/javascript">
  $(document).ready( function() {

    $('.toggle-globe-reward').on("click", function() {
        var _element = $(this);
        var _id = _element.attr('data-id');
        var _enabled = $("#admin-reward-"+_id);

        var formData = {
            id: _id,
            enabled: _enabled.val(),
            _token: "{{ csrf_token() }}"
        };

        $.ajax({
            type: "POST",
            headers: {'X-CSRF-TOKEN': "{{csrf_token()}}"},
            url: "{{ URL::to('/') }}/redemption/enable",
            data: formData,
            dataType: 'json',
            success: function(data) {
              if ( data.success === true ) {
                if ( data.enabled_keyword == "enabled" ) {
                    _element.removeClass('disabled').addClass('enabled').text('enabled');
                    _enabled.val('1');
                } else {
                    _element.removeClass('enabled').addClass('disabled').text('disabled');
                    _enabled.val('0');
                }
              } else {
                console.log('Request is not sucessful. Please try again.');
              }
            },
            error: function(data) {
              console.log('Error:', data);
            }
        });

      }); 

    $('.branches .select-all').on('click', function() {
        if($('.branches .select-all').is(':checked')){
            $('.branches .checkbox').prop('checked', true);
            $(".branches .fa-check").css("display", "block");
        }
        else
        {
            $('.branches .checkbox').prop('checked', false);
            $(".branches .fa-check").css("display", "none");
        }
    });
    
    $(".actions").on("click", ".x_edit-redeem", function () {        
        $("#modal_productName").val( $(this).data('name') );
        $("#modal_productDesc").val( $(this).data('desc') );
        $("#modal_points").val( $(this).data('points') );
        $("#file_change").val(0);
        $('.custom-file-input').val(null);
        $(".preview-img").css( 'background-image', 'url("'+$(this).data('img')+'")' );
        $(".preview-img").css( "padding", '0');
        $(".preview-img label").html('&nbsp');
        $(".branch-options").prop('checked',false);
        $(".branches .fa-check").css("display", "none");
        var branch_arr = $(this).data('branch');
        if ( branch_arr == 0 ) {
            $(".branch-options").prop('checked',true);
            $(".branches .fa-check").css("display", "block");
        } else {
            $.each( branch_arr, function( key, value ) {
                $("#branch_" + value).prop('checked',true);
                $("#branch_" + value + "_check").css('display', "block");
            })
        }
        if ( $("input[name=redeem_id").length > 0 ) { $("input[name=redeem_id").remove(); }
        if ( $("label.error").length > 0 ) { $("label.error").remove(); }
        $('<input>').attr({type:'hidden', name: 'redeem_id', value: $(this).data('id') }).appendTo('#editRedeemForm');

        $("#editRedeemForm").validate({
            ignore: [],
            rules: {
                product_name: { required: true },
                product_desc: { required: true },
                points: { required: true },
            },
            submitHandler: function(form) {
                form.submit();
            }
        });

    });

    $(".redemption").on("click", ".x_addRedeemItem", function () {        
        $("#editRedeemForm").find("input[type=number], :text, textarea").val("");
        $("#file_change").val(0);
        $(".preview-img").css( 'background-image', '' );
        $(".preview-img").css( "padding", '106px');
        $(".preview-img label").html('Browse');
        $(".preview-img label").css({"height": "31px", "width": "73px"});
        $('.branches .checkbox').prop('checked', true);
        $(".branches .fa-check").css("display", "block");
        if ( $("input[name=redeem_id").length > 0 ) { $("input[name=redeem_id").remove(); }
        if ( $("label.error").length > 0 ) { $("label.error").remove(); }

        $("#editRedeemForm").validate({
            ignore: [],
            rules: {
                img_file: {  required: true },
                product_name: { required: true },
                product_desc: { required: true },
                points: { required: true },
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });

    $(".btn-clear-redeem").on("click", function() {
        $("#editRedeemForm").find("input[type=number], :text, textarea").val("");
        $('.custom-file-input').val(null);
        $("#file_change").val(0);
        $(".preview-img").css( 'background-image', '' );
        $(".preview-img").css( "padding", '106px');
        $(".preview-img label").html('Browse');
        $(".preview-img label").css({"height": "31px", "width": "73px"});
        $('.branches .checkbox').prop('checked', false);
        $(".branches .fa-check").css("display", "none");
        if ( $("label.error").length > 0 ) { $("label.error").remove(); }
    });

    $(".branch-options").on('change', function() {
        var _check = $(this).siblings(".branch-label").children('.fa-check');
        if ( $(this).is(':checked') == true ) _check.css("display", "block");
        else _check.css("display","none");
    });

    $("#editRedeemForm").on('change', '.custom-file-input', function() {
        var input = this;
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $(".preview-img").css( 'background-image', 'url("' +  e.target.result + '")' );
                $(".preview-img").css( "padding", '0');
                $(".preview-img label").html('&nbsp');
                $(".preview-img label").css({"height": "200px", "width": "350px"});
                $("#file_change").val(1);
            }
            reader.readAsDataURL(input.files[0]);
        }
    });

    $(".btn-save-redeem").on("click", function() {
       var _form = $("#editRedeemForm");
       _form.attr('method','post');
       _form.attr('enctype','multipart/form-data');
       _form.attr('action','{{ URL::to('redemption/save') }}');
       _form.submit();
    });

    $('.x_remove-redeem').on('click',function() {
        if ( $("input[name=redeemId").length > 0 ) { $("input[name=redeemId").remove(); }
        $('<input>').attr({type:'hidden', name: 'redeemId', value: $(this).data('id') }).appendTo('#deleteRedeemForm');
    });

    $('#deleteRedeemForm').on('click', '.btn-delete-yes', function() {
        var _form = $("#deleteRedeemForm");
       _form.attr('method','post');
       _form.attr('action','{{ URL::to('redemption/delete') }}');
       _form.submit();
    });

});
</script>
@endsection
