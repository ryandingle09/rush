<script id="redeemRowEditTemplate" type="text/html">
<div class="col-md-1">
    <div class="input-group">
        <span class="input-group-addon"><i class="number">0</i>
        </span>
    </div>
</div>
<div class="col-md-1">
    <div class="input-group">
        <input type="text" class="form-control rProdName" name="productName[]" id="rProdName" placeholder="e.g: Shampoo 150ml" maxlength="35">
    </div>
</div>
<div class="col-md-2">
    <span class="error-msg hide">Image does not meet the image size requirement.</span>
    <div class="input-group rProdImg">
        <img class="imgPrev" alt="" src="{{MigrationHelper::getBaseUrl()}}assets/images/addImage.jpg">
        <span class="btn btn-default btn-file">
            Browse
            <input type="file" name="productImage[]" class="fuImage" ew="350" eh="200">
        </span>
    </div>
    <i>Required image size: 350px x 200px</i>
</div>
<div class="col-md-2">
        <label for="rBranches redemption-header" class="mobileOnly">Branch</label>
        <div class="input-group">
            @foreach( $branches as $branch ) 
                <label><input type="checkbox" class="rBranch" value="{{ $branch['id'] }}">{{ $branch['branchName']}}</label>
            @endforeach
            <input type="hidden" name="productBranch[]" class="rBranchInput" value="">
        </div>
    </div>
<div class="col-md-2 ">
    <div class="input-group">
        <textarea name="productDescription[]" rows="8" cols="40" class="form-control rDescription" id="rDescription" maxlength="400"></textarea>
    </div>
</div>
<div class="col-md-2 ">
    <div class="input-group">
        <input type="text" class="form-control rPoints" name="points[]" id="rPoints">
        <span class="input-group-addon pts">pts</span>
    </div>
</div>
<div class="col-md-1">
    <a href="" onclick="return false"><i class="fa edit fa-pencil edit-redeem"></i></a>
    <a href="" onclick="return false"><i class="fa del fa-times remove-redeem"></i></a>
</div>
<input type="hidden" name="redeemId[]" class="rRedeemId" id="rRedeemId" value="">
</script>