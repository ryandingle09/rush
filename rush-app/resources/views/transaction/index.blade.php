@extends('layouts.app')

@section('headerPlaceholder')
<style>
    div.dataTables_processing { z-index: 1; }
</style>
@endsection

@section('view')
<div class="container">
    @include('flash.message')
    <div class="row">
        <div class="col-md-6">
            <label></label>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class='input-group date datetimepicker' id='dateStart'>
                            <input type='text' class="form-control" name="dateStart" placeholder="Date Start" data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="" required>
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class='input-group date datetimepicker' id='dateEnd'>
                            <input type='text' class="form-control" name="dateEnd" placeholder="Date End" data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="" required>
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 forAddBtn pull-right filterLabel">
            <div class="input-group">
                <span class="input-group-addon " id="basic-addon2">&nbsp;</span>
            </div>
        </div>
    </div>
    <div id="content" class="">
        <!-- Nav tabs -->
        <div class="navTabsWrap">
            <ul class="nav nav-tabs" role="tablist" id="tabs">
                <li role="presentation" class="active"><a href="#transactions" aria-controls="transactions" role="tab" data-toggle="tab">Transaction History</a></li>
                @if( $merchant->isPro() )
                @if($is_enable_product_module)
                <li role="presentation"><a href="#productTransactions" aria-controls="productTransactions" role="tab" data-toggle="tab">Product Transaction History</a></li>
                @endif
                <li role="presentation"><a href="#guestPurchases" aria-controls="guestPurchases" role="tab" data-toggle="tab">Guest Purchases</a></li>
                <li role="presentation"><a href="#pointsSeeding" aria-controls="pointsSeeding" role="tab" data-toggle="tab">Points Seeding</a></li>
                @endif
                @if (ToolHelper::isAllowed(ToolHelper::TOOL_ATTENDANCE_ID, $merchant))
                <li role="presentation"><a href="#attendance" aria-controls="attendance" role="tab" data-toggle="tab">Attendance</a></li>
                <li role="presentation"><a href="#attendees" aria-controls="attendees" role="tab" data-toggle="tab">Attendees</a></li>
                @endif
                @if($is_enable_votes_table_column)
                <li role="presentation"><a href="#votes" aria-controls="votes" role="tab" data-toggle="tab">Votes</a></li>
                @endif
                @if($is_enable_votes_table_column)
                <li role="presentation"><a href="#votes" aria-controls="votes" role="tab" data-toggle="tab">Votes</a></li>
                @endif
            </ul>
        </div>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane transHistory active" id="transactions">
                    {{-- START: if the merchant package is Loyalty --}}
                    @if( $merchant->isPro() )
                        <table id="historyTable" class="data-table management points-history-table" data-enable-transaction-method="{{ $is_enable_transaction_method}}">
                        <thead>
                        <tr>
                            <th>Date &amp; Time</th>
                            <th>Transaction Type</th>
                            <th>Transaction Method</th>
                            <th>Customer Name</th>
                            <th>Transaction Amount</th>
                            <th>Points</th>
                            <th data-toggle="tooltip" title="Redeemed">Qty (R)</th>
                            <th data-toggle="tooltip" title="Claimed">Qty (C)</th>
                            <th>Reward</th>
                            <th>Ref./OR No.</th>
                            <th>Branch</th>
                            <th>Emp#</th>
                            <th>action</th>
                        </tr>
                        </thead>
                        {{-- END: if the merchant package is Loyalty --}}
                        {{-- START: if the merchant package is Punchcard --}}
                    @else
                        <table id="historyTable" class="data-table management punchcard-history-table">
                        <thead>
                        <tr>
                            <th>Date &amp; Time</th>
                            <th>Promo Title</th>
                            <th>Transaction Type</th>
                            <th>Customer Name</th>
                            <th>Transaction Amount</th>
                            <th>Reward/Stamps</th>
                            <th>Ref No.</th>
                            <th>Branch</th>
                            <th>Emp#</th>
                            <th id="receipt-header" class="@if( $merchant->settings->transaction_receipts ) with-receipt @else without-receipt @endif" >Receipt</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    @endif
                </table>
            </div>
            @if( $merchant->isPro() )
                <div role="tabpanel" class="tab-pane productTransactions" id='productTransactions'>
                    <table id="productTransactionTable" class="data-table management">
                        <thead>
                        <tr>
                            <th>Date &amp; Time</th>
                            <th>OR/Reference</th>
                            <th>Customer</th>
                            <th>Product Name</th>
                            <th>Amount</th>
                            <th>Points</th>
                            <th>Qty</th>
                            <th>Total Amount</th>
                            <th>Total Points</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($product_transactions as $product_transaction)
                            <tr>
                                <td>{{ $product_transaction->created_at }}</td>
                                <td>{{ $product_transaction->customer_transaction->receiptReferenceNumber }}</td>
                                <td>{{ $product_transaction->customer->fullName}}</td>
                                <td>{{ $product_transaction->product->name or ""}}</td>
                                <td>{{ $product_transaction->amount }}</td>
                                <td>{{ $product_transaction->points }}</td>
                                <td>{{ $product_transaction->quantity }}</td>
                                <td>{{ $product_transaction->total_amount }}</td>
                                <td>{{ $product_transaction->total_points }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane guestTransHistory" id='guestPurchases'>
                    <table id="guestTransactionTable" class="data-table management">
                        <thead>
                        <tr>
                            <th>Date &amp; Time</th>
                            <th>Transaction Type</th>
                            <th>Mobile Number</th>
                            <th>Transaction Amount</th>
                            <th>Points</th>
                            <th>Ref./OR No.</th>
                            <th>Branch</th>
                            <th>Emp#</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($guestTransactions) && $guestTransactions)
                            @foreach($guestTransactions as $guestTransaction )
                                <tr>
                                    <td>{{ $guestTransaction->timestamp }}</td>
                                    <td>{{ $guestTransaction->transactionType }}</td>
                                    <td>{{ $guestTransaction->mobileNumber }}</td>
                                    <td>P {{ $guestTransaction->amountPaidWithCash }}</td>
                                    <td>{{ $guestTransaction->pointsEarned }}</td>
                                    <td>{{ $guestTransaction->receiptReferenceNumber }}</td>
                                    <td>{{ $guestTransaction->branch->branchName }}</td>
                                    <td>{{ $guestTransaction->employee_id }}</td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane pointsSeeding" id='pointsSeeding'>
                    <table id="pointsSeedingTable" class="data-table management">
                        <thead>
                        <tr>
                            <th class="text-center">Batch ID</th>
                            <th class="text-left">Upload Date</th>
                            <th class="text-center">Valid Data</th>
                            <th class="text-center">Invalid Data</th>
                            <th class="text-left">Uploaded by</th>
                            <th class="text-right">Action</th>
                        </tr>
                        </thead>
                    </table>
                    <a href="#UploadPointsSeedingModal" id="UploadPointsSeedingCSV" class="btn btn-primary" data-toggle="modal" onclick="return false">Upload CSV</a>
                </div>
            @endif
            @if (ToolHelper::isAllowed(ToolHelper::TOOL_ATTENDANCE_ID, $merchant))
                <div role="tabpanel" class="tab-pane transHistory" id='attendance'>
                    <input type="hidden" id="attendance_button" value="{{ $merchant->attendance_button or '1' }}" /><!--TODO return to 0-->
                    <table id="attendanceTable" class="data-table management">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>name</th>
                            <th>status</th>
                            <th>created_at</th>
                            <th>updated_at</th>
                            <th>action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane transHistory" id='attendees'>
                    <table id="attendeesTable" class="data-table management">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>date_time</th>
                            <th>client_id</th>
                            <th>client</th>
                            <th>type</th>
                            <th>visit_location</th>
                            <th>attendance_id</th>
                            <th>updated_at</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            @endif
            @if($is_enable_votes_table_column)
            <div role="tabpanel" class="tab-pane votes" id='votes'>
                <table id="votesTable" class="data-table management">
                    <thead>
                    <tr>
                        <th>Date &amp; Time</th>
                        <th>Customer Name</th>
                        <th>Title</th>
                        <th>Rating</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            @endif
        </div>
    </div>
</div> <!-- /container -->
@endsection
@section('modal')
@if (ToolHelper::isAllowed(ToolHelper::TOOL_ATTENDANCE_ID, $merchant))
    <div id="addAttendance" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog expandWidth">
            <div class="modal-content modlogin">
                <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
                <div class="modal-header">Add Attendance</div>
                <div class="modal-body">
                    <form method="POST" action="{{MigrationHelper::getAppBaseUrl()}}transactionhistory/attendance" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <div class="col-md-6">
                                <h4>Upload Attendance Data</h4>
                                <hr>
                            </div>
                            <div class="col-md-12 row">
                                <input type="file" accept=".xlsx,.xls" name="attendanceData" required>
                            </div>
                            <div class="col-md-12 margT20">
                                <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                                <button class="btn btn-primary pull-right">ADD</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="clientEarn" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog expandWidth">
            <div class="modal-content modlogin">
                <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
                <div class="modal-header">Process Attendee Stamps</div>
                <div class="modal-body">
                    <form method="POST" action="{{MigrationHelper::getAppBaseUrl()}}transactionhistory/attendance/earn">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <div class="col-md-12 row">
                                <h4>You are about to award stamps, do you want to continue?</h4>
                                <hr>
                            </div>
                            <div class="col-md-12 row">
                                <input type="hidden" name="attendanceId">
                            </div>
                            <div class="col-md-12 margT20">
                                <button class="btn btn-default" data-dismiss="modal" >NO</button>
                                <button class="btn btn-primary pull-right">YES</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endif
<div id="voidEarn" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Void Earn Transaction</div>
            <div class="modal-body">
                <p>Are you sure you want to VOID this transaction?</p>
                <span id="transactionToVoid"></span>
                <form method="POST" id="voidEarnForm">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button class="btn btn-primary pull-right">CONTINUE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="voidPackageTransaction" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <form method="POST" id="voidPackageTransactionForm">
                {{ csrf_field() }}
                <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
                <div class="modal-header">Void Punchcard Transaction</div>
                <div class="modal-body">
                    <p>Are you sure you want to VOID this transaction?</p>
                    <span id="transactionPackage"></span>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12">
                        <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                        <button class="btn btn-primary pull-right">CONTINUE</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@if ( $merchant->isPro() )
<div id="UploadPointsSeedingModal" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <form action="{{ url('transactionhistory/points-seeding-upload') }}" id="UploadPointsSeedingForm" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
                <div class="modal-header">Points Seeding Upload</div>
                <div class="modal-body">
                    @if ( $merchant->settings->customer_login_param == "email" ) 
                    <a href="{{ url('page/point-seeding/point-seeding-sample-email.csv') }}" class="btn btn-info">
                    @else
                    <a href="{{ url('page/point-seeding/point-seeding-sample-mobile.csv') }}" class="btn btn-info">
                    @endif
                        <i class="" aria-hidden="true"></i> Download Sample Template
                    </a>
                    <br /><br />
                    <input type="file" name="point_seeding_file" id="point_seeding_file" accept=".csv" >
                </div>
                <div class="modal-footer">
                    <div class="col-md-12">
                        <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                        <button class="btn btn-primary pull-right">SUBMIT</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endif

@if( $merchant->settings->transaction_receipts )
<div id="transactionReceiptModal" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
                <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
                <div class="modal-header">Transaction Receipt</div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-5">
                            <img id="tr-image" class="img-responsive" >
                        </div>
                        <div class="col-md-7">
                            <div class="form-group row">
                                <div class="col-md-12"><label>Transaction No.</label> <br /> <span id="tr-trnumber"></span></div>
                                <div class="col-md-12"><label>Customer Name</label> <br /> <span id="tr-trcustname"></span></div>
                                <div class="col-md-12"><label>Mobile Number</label> <br /> <span id="tr-trmobilenum"></span></div>
                                <div class="col-md-12"><label>Transacted At</label> <br /> <span id="tr-trbranch"></span></div>
                                <div class="col-md-12"><label>Transacted By</label> <br /> <span id="tr-tremployee"></span></div>
                                <div class="col-md-12"><label>Date of Transaction</label> <br /> <span id="tr-trdate"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12">
                        <a class="btn btn-primary pull-right" href="" id="tr-download-image-link" target="_blank" download>Download Image</a>
                    </div>
                </div>
        </div>
    </div>
</div>
@endif
<div id="loading" class="hidden" style="position: absolute; z-index: 10000; left: 50%; top: 50%;">
    <i class="fa fa-circle-o-notch fa-spin" style="font-size:24px"></i>
</div>
@endsection
@section('footerPlaceholder')
@parent
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.12.1/xlsx.full.min.js"></script>
<script type="text/javascript" src="https://fastcdn.org/FileSaver.js/1.1.20151003/FileSaver.min.js"></script>
<style type="text/css">
    tfoot input {
        width: 100%;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
    }
    pointsSeedingTable { width: 100%; }
</style>
<script type="text/javascript">
$( document ).ready(function() {
    $('[data-toggle="tooltip"]').tooltip({
        container: 'body'
    });

    $('#historyTable').on('click', '.voidEarn' ,function() {
        var $row = $(this).closest('tr');
        var $cell = $row.find('td');
        $("#transactionToVoid").text($cell.eq(2).text() + ' : ' + $cell.eq(8).text());
        $("#voidEarnForm").attr('action', $(this).data('action'));
    });

    $('#historyTable').on('click', '.voidPointsPackageTransaction' ,function() {
        var $row = $(this).closest('tr');
        var $cell = $row.find('td');
        $("#transactionPackage").text($cell.eq(2).text() + ' : ' + $cell.eq(8).text());
        $("#voidPackageTransactionForm").attr('action', $(this).data('action'));
    });

    $('#historyTable').on('click', '.voidPackageTransaction' ,function() {
        var $row = $(this).closest('tr');
        var $cell = $row.find('td');
        $("#transactionPackage").text($cell.eq(2).text() + ' : ' + $cell.eq(6).text());
        $("#voidPackageTransactionForm").attr('action', $(this).data('action'));
    });

    @if (ToolHelper::isAllowed(ToolHelper::TOOL_ATTENDANCE_ID, $merchant))
    $('#attendanceTable').on( 'init.dt', function () {
        var attendanceTable = $('#attendanceTable').dataTable();
        attendanceTable.api().ajax.url('{{MigrationHelper::getAppBaseUrl()}}transactionhistory/attendance').load();

        // bind clicks on View Attendees
        $('#attendanceTable tbody').on( 'click', 'a', function () {
            var data = attendanceTable.api().row( $(this).parents('tr') ).data();
            $(this).trigger('viewAttendees', [data['id'], data['status']]);
        });

        $('#tabs a[href="#attendance"]').on( 'click', function () {
            $('#attendanceTable').dataTable().api().draw();
        });
    } );

    $('#attendanceTable').on('viewAttendees', function(event, attendanceId, status) {
        var attendeesTable = $('#attendeesTable').dataTable();
        attendeesTable.api().clear();
        attendeesTable.api().draw();
        attendeesTable.api().ajax.url('{{MigrationHelper::getAppBaseUrl()}}transactionhistory/attendance/'+attendanceId+'/attendees').load();
        $('#tabs a[href="#attendees"]').tab('show');

        $('#clientEarn input[name=attendanceId]').val(attendanceId);
        // show process button
        if (status == '1') {
            $('#attendeesTable_wrapper button').removeClass('hide');
        } else {
            $('#attendeesTable_wrapper button').addClass('hide');
        }
    });
    @endif

    @if( $merchant->settings->transaction_receipts )
        $("#tr-trnumber, #tr-trcustname, #tr-trmobilenum, #tr-trbranch, #tr-tremployee, #tr-trdate").html('');
        $("#tr-image").attr('src','');
        $("body").on("click", ".transaction_receipts_view", function(){
            var request = $.ajax({
              url: "{{ url('transactionhistory/getreceiptdata' ) }}",
              type: "POST",
              data: {
                        "_token": "{{ csrf_token() }}",
                        "id" : $(this).data('id')
                    },
              dataType: "json"
            }).done( function( msg ) {
              $("#tr-trnumber").html( msg[0].transaction_nunmber );
              $("#tr-trcustname").html( msg[0].customerName );
              $("#tr-trmobilenum").html( msg[0].mobileNumber );
              $("#tr-trbranch").html( msg[0].branchName );
              $("#tr-tremployee").html( msg[0].employeeName );
              $("#tr-trdate").html( msg[0].date_punched );
              $("#tr-image").attr( 'src', '../' + msg[0].image_file );
              $("#tr-download-image-link").attr( 'href', '../' + msg[0].image_file );
              $("#transactionReceiptModal").modal('show');
            }).fail( function( jqXHR, textStatus ) {

            });
        });
    @endif

    var csrf_token = '{{ csrf_token() }}';

    $.extend($.fn.dataTableExt.oStdClasses, {
        'sWrapper': 'bootstrap-table dataTables_wrapper',
        'sFilter': 'pull-right search'
    });
    var emptyTableMessage = '';
    var excelExportButton = '<div class="text-center"><button type="button" class="btn btn-default exportButton">Export to Excel</button></div>';
    var datatableSpecs = {
            'order': [[0, 'desc']],
            'sDom': '<"fixed-table-toolbar"f><"excelButton hide"B><"customButton">rt<"bottom"ilp><"clear">',
            'buttons': [
                'excel'
            ],
            'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
            'scrollX': true,
            'scrollCollapse': true,
            'autoWidth': false
        };
        var pointsSeedingDTAjax = {};
        $('#pointsSeedingTable').dataTable($.extend({}, datatableSpecs, pointsSeedingDTAjax, {
            language: {
                'emptyTable': 'No matching records found',
                'search': ''
            },
            processing: true,
            serverSide: true,
            ajax: {
                'url': '{{ url('points-seeding/batch') }}',
                'type': 'POST',
                'headers': {
                    'X-CSRF-TOKEN': csrf_token
                }
            },
            columns: [
                {data: 'id', name: 'Batch ID', class: 'text-center'},
                {data: 'created_at', name: 'Upload Date', class: 'text-left'},
                {data: 'valid_data', name: 'Valid Data', class: 'text-center'},
                {data: 'invalid_data', name: 'Invalid Data', class: 'text-center'},
                {data: 'customer_name', name: 'Uploaded By', class: 'text-left'},
                {data: 'status', name: 'Action', class: 'text-right', orderable: false,
                    render : function(data, type, row){
                            return '<a href="{{ url('points-seeding') }}/'+ row['id'] + '" class="edit"><i class="fa fa-eye"></i>View</a>';
                    }
                }
            ],
            initComplete: function () {
                $('select[name=pointsSeedingTable_length]').unwrap();
                $('#pointsSeedingTable_length').addClass('dataTables_info');
                $('#pointsSeedingTable_filter input').attr({
                    'type': 'text',
                    'placeholder': 'Search'}).addClass('form-control').css('font-weight', 'normal');
            }
        }));

        if ($('#pointsSeedingTable tbody tr.odd td').text() != emptyTableMessage) {
            $('#pointsSeeding').append(excelExportButton);
        }

        var votesDTAjax = {};

        $('#votes').append(excelExportButton);

        $('#votesTable').dataTable($.extend({}, datatableSpecs, votesDTAjax, {
            language: {
                'emptyTable': 'No matching records found',
                'search': ''
            },
            processing: true,
            serverSide: true,
            ajax: {
                'url': '{{ url('transactionhistory/votes-dt-ajax') }}'
            },
            columns: [
                {data: 'created_at', name: 'Date & Time'},
                {data: 'fullName', name: 'Customer Name'},
                {data: 'post_title', name: 'Title'},
                {data: 'vote', name: 'Rating'}
            ],
            initComplete: function () {
                $('select[name=votesTable_length]').unwrap();
                $('#votesTable_length').addClass('dataTables_info');
                $('#votesTable_filter input').attr({
                    'type': 'text',
                    'placeholder': 'Search'}).addClass('form-control').css('font-weight', 'normal');
            }
        }));

        $('body').on('click', '.exportButton', function(){
            $(this).parent().parent().find('.dataTables_wrapper .excelButton a').click();
        });
});
</script>
@endsection
