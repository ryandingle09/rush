@extends('layouts.app')
@section('headerPlaceholder')
<style>
.edit { color: #C7C0BA; }
</style>
@endsection
@section('view')
<div class="container">
    <div id="content" style="margin-top: 75px;">
        <div class="tab-content" id="pointsSeedingArea">
            <table id="pointsSeedingDataTable">
                <thead>
                    <th>ID</th>
                    <th>Transaction Date</th>
                    <th>Customer</th>
                    <th>Ref./OR No.</th>
                    <th>Transaction Amount</th>
                    <th>Status</th>
                    <th>Action</th>
                </thead>
            </table>
            <a href="{{ url('transactionhistory#pointsSeeding') }}" class="btn btn-primary">Back</a>
            <a href="{{ url('points-seeding/'.$batch_id.'/mass-seed') }}" class="btn btn-primary">Seed All Valid</a>
        </div>
    </div>
</div>
@endsection

@section('modal')
@endsection

@section('footerPlaceholder')
@parent
<script type="text/javascript">
    $( document ).ready(function() {
        var csrf_token = '{{ csrf_token() }}';

        $.extend($.fn.dataTableExt.oStdClasses, {
            'sWrapper': 'bootstrap-table dataTables_wrapper',
            'sFilter': 'pull-right search'
        });
        var emptyTableMessage = '';
        var excelExportButton = '<div class="text-center"><button type="button" class="btn btn-default exportButton">Export to Excel</button></div>';
        var datatableSpecs = {
                'order': [[0, 'desc']],
                'sDom': '<"fixed-table-toolbar"f><"excelButton hide"B><"customButton">rt<"bottom"ilp><"clear">',
                'buttons': [
                    'excel'
                ],
                'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
                'scrollX': true,
                'scrollCollapse': true,
                'autoWidth': false
            };
            var pointsSeedingDTAjax = {};
            $('#pointsSeedingDataTable').dataTable($.extend({}, datatableSpecs, pointsSeedingDTAjax, {
                language: {
                    'emptyTable': 'No matching records found',
                    'search': ''
                },
                processing: true,
                serverSide: true,
                ajax: {
                    'url': '{{ url('points-seeding/data/' . $batch_id) }}',
                    'type': 'POST',
                    'headers': {
                        'X-CSRF-TOKEN': csrf_token
                    }
                },
                columns: [
                    {data: 'id', name: 'ID', class: 'text-center'},
                    {data: 'transaction_date', name: 'Transaction Date', class: 'text-left'},
                    {data: 'customer', name: 'Customer', class: 'text-right'},
                    {data: 'or_number', name: 'Ref./OR No.', class: 'text-right'},
                    {data: 'transaction_amount', name: 'Transaction Amount', class: 'text-right'},
                    {data: 'status', name: 'Status', class: 'text-right',
                        render : function(data, type, row){
                            var status = row.status;
                            if ( row.transaction_id ) {
                                return 'Seeded';
                            } else {
                                if( status == 1 ){
                                    return "Unseeded";
                                }
                                return 'Invalid';
                            }
                        }
                    },
                    {data: 'status', name: 'Action', class: 'text-right',
                        render : function(data, type, row){
                            var status = row.status;
                            if( status == 1 && row.transaction_id == null ){
                                return '<a href="{{ url('points-seeding/seed') }}/' + row.id +'" class="edit"><i class="fa fa-check"></i>Seed</a>';
                            }
                            return '';
                        }
                    }
                ],
                initComplete: function () {
                    $('select[name=pointsSeedingDataTable_length]').unwrap();
                    $('#pointsSeedingDataTable_length').addClass('dataTables_info');
                    $('#pointsSeedingDataTable_filter input').attr({
                        'type': 'text',
                        'placeholder': 'Search'}).addClass('form-control').css('font-weight', 'normal');
                }
            }));

            if ($('#pointsSeedingDataTable tbody tr.odd td').text() != emptyTableMessage) {
                $('#pointsSeedingArea').append(excelExportButton);
            }
    });
</script>
@endsection
