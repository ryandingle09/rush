@extends('onboarding-cms.layout')

@section('view')
@section('view')
<div class="container">
   <div id="content" class="home spacing">
	   <div class="row">
	   	@foreach( $modules as $module_item )
			<div class="module">
				<a class="module-icon" href="{{ url( $module_item->url ) }}" title="{{ json_decode($module_item->other_details)->label }}">
					<i class="icon icon-{{ json_decode($module_item->other_details)->class }}" data-toggle="tooltip" data-placement="center" title="{{$module_item->name}}"></i>
					<span class="module-text">{{ json_decode($module_item->other_details)->label }}</span>
				</a>
			</div>
	   	@endforeach
	   </div>
   </div>
</div> <!-- /container -->
@endsection