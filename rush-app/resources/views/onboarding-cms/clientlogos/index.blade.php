@extends('onboarding-cms.layout')

@section('view')
@if (count($errors) > 0)
	<div class="text-center error-display">
	    <div class="alert alert-danger spacing spacing">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	</div>
@endif

@if ( session('message') )
	<div class="text-center error-display spacing">
	    <div class="alert alert-info spacing">{{ session('message') }}</div>
	</div>
@endif

<div class="button-toolbar clientlogos">
	<button class="btn btn-primary" data-toggle="modal" data-target="#newlogo">Add New Logo</button>
</div>
<div id="content" class="admin-cms clientlogos">
    <table id="clientlogos-table" class="data-table">
		<thead>
			<tr>
				<th></th>
				<th>Logo</th>
				<th>Client Name</th>
				<th>Date Uploaded</th>
				<th>Visibility</th>
				<th></th>
			</tr>
        </thead>
        <tbody>
        	@if ($logos)
	        	@foreach ($logos as $logo)
				<tr>
					<td>{{ $logo->id }}</td>
					<td>
	                    <span class="logo-thumb">
	                        <img src="{{ url( 'page/onboarding-cms/images/partners/' . $logo->logo ) }}">
	                    </span>                    
	                </td>
					<td>{{ $logo->client_name }}</td>
					<td>{{ $logo->created_at_label }}</td>
					<td>
						{{ Form::select('news-visibility-' . $logo->id, [ 1 => 'Visible', 0 => 'Hidden' ], $logo->visibility, ['class' => 'vis-select', 'data-id' => $logo->id]) }}
					</td>
					<td>
	                    <a href="#" data-id="{{ $logo->id }}" data-client_name="{{ $logo->client_name }}" data-logo="{{ $logo->logo }}" data-toggle="modal" data-target="#editlogo" class="edit editLogo"><i class="fa fa-pencil"></i>Edit</a>
						<a href="#" data-id="{{ $logo->id }}" data-toggle="modal" data-target="#deletelogo" data-action="" onclick="return false" class="delete deleteLogo"><i class="fa fa-trash"></i>Delete</a>
					</td>
				</tr>
				@endforeach
			@endif
		</tbody>
	</table>
</div>
@include('onboarding-cms.clientlogos.modal')
@endsection

@section('footerPlaceholder')
@parent
<script src="{{ url('app/page/admin/js/dataTables.rowReorder.min.js') }}"></script>
<script src="{{ url('app/page/admin/js/clientlogos.js') }}"></script>
<script type="text/javascript">
	$("#clientlogos-table").on('change', '.vis-select', function() {
		$.ajax({
			method: "POST",
			url: "{{ url('onboarding-cms/clientlogos/visibility') }}",
			dataType: "json",
			data: { '_token': '{{ csrf_token() }}', id: $(this).data('id'), value: $(this).val() },
		})
		.done( function(msg) {
		})
		.fail( function(msg) {
			console.log( msg );
		});
	});

	$("#clientlogos-table").on('click', '.editLogo', function() {
		$('#clientlogos_idE').val( $(this).data('id') );
		$('#clientNameE').val( $(this).data('client_name') );
		$('#clientimgE-filename').val( $(this).data('logo') );
	});

	$("#clientlogos-table").on('click', '.deleteLogo', function() {
		$('#clientlogos_idD').val( $(this).data('id') );
	});
</script>
@endsection