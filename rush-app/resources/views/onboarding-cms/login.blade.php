<!doctype html>
<html lang="en">
<head>
    <title>RUSH Onboarding CMS - Login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    {{--  Bootstrap core CSS  --}}
    <link rel="stylesheet" href="{{ url('page/onboarding-cms/css/bootstrap.min.css') }}">
    
    {{-- Custom styles for this template --}}
    <link href="{{ url('page/onboarding-cms/css/font-awesome.min.css') }}" rel="stylesheet">
    <style>
        html, body {
            height: 100%;
        }

        body {
            display: -ms-flexbox;
            display: -webkit-box;
            display: flex;
            -ms-flex-align: center;
            -ms-flex-pack: center;
            -webkit-box-align: center;
            align-items: center;
            -webkit-box-pack: center;
            justify-content: center;
            padding-top: 40px;
            padding-bottom: 40px;
            background-color: #f5f5f5;
        }

        .form-signin {
            width: 100%;
            max-width: 330px;
            padding: 15px;
            margin: 0 auto;
        }
        .form-signin .checkbox {
            font-weight: 400;
        }
        .form-signin .form-control {
            position: relative;
            box-sizing: border-box;
            height: auto;
            padding: 10px;
            font-size: 16px;
        }
        .form-signin .form-control:focus {
            z-index: 2;
        }
        .form-signin input[type="email"] {
            margin-bottom: -1px;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }
        .form-signin input[type="password"] {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
        .error-display { width: 100%;max-width: 330px; margin: 0 auto; }
    </style>
</head>

<body>
    <div class="container-fluid text-center">
        <form method="post" action="{{ url('onboarding-cms/login') }}" class="form-signin" />
            {{ csrf_field() }}
            <h1 class="h3 mb-3 font-weight-normal">RUSH</h1>

            {{-- Login username --}}
            <label for="login_username" class="sr-only">Email address</label>
            <input type="email" name="username" id="login_username" class="form-control" placeholder="Username" required autofocus value="{{ old('username') }}">
            
            {{-- Login Password --}}
            <label for="login_password" class="sr-only">Password</label>
            <input type="password" name="password" id="login_password" class="form-control" placeholder="Password" required>
            
            {{-- Login button --}}
            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        </form>

    @if (count($errors) > 0)
        <div class="text-center error-display">
            <div class="alert alert-danger spacing">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif

    @if ( session('message') )
        <div class="text-center error-display">
            <div class="alert alert-info spacing">{{ session('message') }}</div>
        </div>
    @endif
    </div>
</body>

{{-- {{ Javascript Starts }} --}}
<script src="{{ url('page/onboarding-cms/js/jquery-3.2.1.slim.min.js') }}"></script>
<script src="{{ url('page/onboarding-cms/js/popper.min.js') }}"></script>
<script src="{{ url('page/onboarding-cms/js/bootstrap.bundle.min.js') }}"></script>
{{-- {{ Javascript Ends }} --}}
</body>
</html>

