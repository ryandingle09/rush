<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="Fidelity" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>RUSH - Onboarding CMS {{ $cms_pageTitle or '' }}</title>

    <!-- Main Admin CSS Stylesheet -->
    <link rel="stylesheet" href="{{ url('page/onboarding-cms/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('app/page/admin/css/sweetalert.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('app/page/admin/css/chosen.css') }}">

    <!-- fonts-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,300,300italic,400italic,600italic,700,700italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ url('app/css/vendor/bootstrap-table.css') }}">
    <link rel="stylesheet" href="{{ url('app/css/vendor/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ url('app/css/vendor/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('app/css/vendor/spectrum.css') }}">
    <link rel="stylesheet" href="{{ url('app/css/vendor/bootstrap-datetimepicker.min.css') }}">

    <!-- DATATABLES -->
    <link rel="stylesheet" type="text/css" href="{{ url('app/css/vendor/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('app/css/vendor/buttons.dataTables.min.css') }}">
    
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- <script type="text/javascript" src="assets/bower_components/jquery/dist/ie10-viewport-bug-workaround.js"></script> -->

    <!-- Additional Header items -->
    @section('headerPlaceholder')
    @show
</head>
<body>
