<div id="sidemenu">
    <div class="logoholder">
        <a><img src="{{ url('page/onboarding-cms/images/logo_2016.png') }}" alt="RUSH Logo"></a>
    </div>
    <div id="menu">
        <ul>
            @foreach( $modules as $menu )
            <li class="{{ json_decode($menu->other_details)->class }}-menu">
                <a href="{{ url($menu->url) }}">
                    <i class="icon" data-toggle="tooltip" data-placement="right" title="{{$menu->name}}"></i>
                    <span>{{ json_decode($menu->other_details)->label }}</span>
                </a>
            </li>
            @endforeach
        </ul>
    </div>
</div>
