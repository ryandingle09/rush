<div id="rightcontent">
  <div class="header">
    <div class="row">
      <div class="col-xs-7">
          <h3>{{ $cms_sectionTitle or "Onboarding CMS" }}</h3>{{-- Check if controller wants to assign a title to the page. Assign ""Super Admin" by if empty by default --}}
      </div>
      <div class="col-xs-5">
        <div class="howdy">
          <!-- Split button -->
          <div class="btn-group">
            <!-- Large button group -->
            <div class="btn-group">
              <button class="btn btn-default btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <img src="{{ url('app/page/admin/images/avatar.png') }}" alt=""/> <span>{{ $cms_userName or "Onboarding CMS" }}</span>
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                <li><a href="{{ url('onboarding-cms/logout') }}">Logout</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
  {{-- rightcontent div and container div closing tags on admin/footer.blade.php --}}
