<div id="deleteNews" class="modal fade admin-cms-modal" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Delete News</div>
            <div class="modal-body">
                <p>Are you sure you want to delete this item?</p>
                <form action="{{ url('onboarding-cms/news/delete') }}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="news_id" id="news_id" />
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button class="btn btn-primary pull-right" id="delete_class_btn">DELETE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>