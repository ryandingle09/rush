@extends('onboarding-cms.layout')

@section('headerPlaceholder')
    <link rel="stylesheet" href="{{ url('app/css/vendor/spectrum.css') }}">
@endsection

@section('view')
<div class="admin-cms-extratop">
    <a href="{{ url('onboarding-cms/news') }}">
        <span class="back-icon"></span> Back to News
    </a>
    <h3>{{ $news->title }}</h3>
</div>
<div class="admin-cms createnew">
    @if (count($errors) > 0)
        <div class="text-center error-display">
            <div class="alert alert-danger spacing">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li> {{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif

    @if ( session('message') )
        <div class="text-center error-display">
            <div class="alert alert-info spacing">{{ session('message') }}</div>
        </div>
    @endif

    <form action="{{ url('onboarding-cms/news/edit/' . $news->id ) }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="content">
            <h3 class="content-title">Top Section</h3>
            <p><i><span style="color:red">*</span> Indicates required field</i></p>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="newsTitle">Title</label>
                        <input type="text" name="newsTitle" id="newsTitle" class="form-control" value="{{ $news->title }}" maxlength="150" required>
                        <i style="display: inline-block">Max length of 150 characters </i>
                        <i style="display: inline-block;float:right" id="titleChar"> Characters: 0/150 </i>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="newsMerchantName">Merchant Name <span style="color:red">*</span></label>
                        <input type="text" name="newsMerchantName" id="newsMerchantName" class="form-control" value="{{ $news->merchant_name }}" maxlength="150" required>
                        <i style="display: inline-block">Max length of 150 characters </i>
                        <i style="display: inline-block;float:right" id="merchantNameChar"> Characters: 0/150 </i>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="newsDescription">Description  <span style="color:red">*</span></label>
                        <textarea type="text" name="newsDescription" id="newsDescription" class="form-control charcount" rows="5" maxlength="500" >{{ $news->description }}</textarea>
                        <i style="display: inline-block">Max length of 500 characters </i>
                        <i style="display: inline-block;float:right" id="descChar"> Characters: 0/500 </i>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="news_headerBG">Background Color</label>
                        <input type='text' id="newsheadBG" name="newsheadBG" value="{{ $news->background }}" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-8">
                                <label for="news_bgimg">Description Background Image</label>
                                <input type="text" class="form-control" name="news_bgimg-filename" id="news_bgimg-filename" readonly value="{{ $news->background_image }}">
                                <i>Max size: 750 x 900</i>
                                <span id="newsBgImgMsg" class="error-msg hide">Image does not meet the image size requirement. Please check again.</br></span>
                            </div>
                            <div class="col-md-4">
                                <label>&nbsp;</label>
                                <span class="btn btn-default btn-file">
                                    Choose File <input type="file" name="news_bgimg" id="news_bgimg" class="fuImage" ew="750" eh="900" accept="image/jpeg, image/png">
                                </span>
                                <span>
                                    <button type="button" id="btnRemoveBgImg" class="btn btn-default btn-file" style="width: auto !important" > Remove </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-8">
                                <label for="news_logo">Logo</label>
                                <input type="text" class="form-control" name="news_logo-filename" id="news_logo-filename" disabled value="{{ $news->logo }}">
                                <i>Maxi size: 300px x 300px</i>
                                <span id="newsLogoMsg" class="error-msg hide">Image does not meet the image size requirement. Please check again.</br></span>
                            </div>
                            <div class="col-md-4">
                                <label>&nbsp;</label>
                                <span class="btn btn-default btn-file">
                                    Choose File <input type="file" name="news_logo" class="fuImage" ew="300" eh="300" accept="image/jpeg, image/png">
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-8">
                                <label for="news_appimg">Description App Image</label>
                                <input type="text" class="form-control" name="news_appimg-filename" id="news_appimg-filename" disabled value="{{ $news->app_image_top }}">
                                <i>Max size: 530 x 900</i>
                                <span id="newsAppMsg" class="error-msg hide">Image does not meet the image size requirement. Please check again.</br></span>
                            </div>
                            <div class="col-md-4">
                                <label>&nbsp;</label>
                                <span class="btn btn-default btn-file">
                                    Choose File <input type="file" name="news_appimg" class="fuImage" ew="530" eh="900" accept="image/jpeg, image/png">
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="android_link">Android Link</label>
                        <input type="text" name="android_link" id="android_link" class="form-control" value="{{ $news->android_link }}" maxlength="150">
                        <i>Max length of 150 characters </i>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="ios_link">iOS Link</label>
                        <input type="text" name="ios_link" id="ios_link" class="form-control" value="{{ $news->ios_link }}" maxlength="150">
                        <i>Max length of 150 characters </i>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="content">
            <h3 class="content-title">Content</h3>
            <div class="row">
                <div class="col-md-12">
                    <textarea name="news_content" id="news-content" class="textarea-editor">{{ $news->content }}</textarea>
                </div>
            </div>
        </div>

        <div class="content">
            <h3 class="content-title">Bottom Section</h3>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="app_name">App Name</label>
                        <input type="text" name="app_name" id="app_name" class="form-control" maxlength="150"  value="{{ $news->app_name }}" >
                        <i>Max length of 150 characters </i>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-8">
                                <label for="banner_image">Banner Image  <span style="color:red">*</span></label>
                                <input type="text" class="form-control" name="banner_image-filename" id="banner_image-filename" readonly value="{{ $news->banner_image }}">
                                <i>Required size: 360 x 200</i>
                                 <span id="newsBannerImgMsg" class="error-msg hide">Image does not meet the image size requirement. Please check again.</br></span>
                            </div>
                            <div class="col-md-4">
                                <label>&nbsp;</label>
                                <span class="btn btn-default btn-file">
                                    Choose File <input type="file" name="banner_image" class="fuImage" ew="360" eh="200" accept="image/jpeg, image/png">
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="mobile_app_content">Mobile App Content</label>
                        <textarea type="text" name="mobile_app_content" id="mobile_app_content" class="form-control charcount" maxlength="500" rows="5">{{ $news->mobile_app_content }}</textarea>
                        <i style="display: inline-block">Max length of 500 characters </i>
                        <i style="display: inline-block;float:right" id="mobileAppContentChar"> Characters: 0/500 </i>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-8">
                                <label for="app_image1">Mobile App Image 1 </label>
                                <input type="text" class="form-control" name="app_image1-filename" id="app_image1-filename" disabled value="{{ $news->app_image1 }}">
                                <i>Max size: 500 x 700</i>
                                <span id="newsAppImgMsg" class="error-msg hide">Image does not meet the image size requirement. Please check again.</br></span>
                            </div>
                            <div class="col-md-4">
                                <label>&nbsp;</label>
                                <span class="btn btn-default btn-file">
                                    Choose File <input type="file" name="app_image1" class="fuImage" ew="500" eh="700" accept="image/jpeg, image/png">
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="app_image1_label">Mobile App Image 1 Text</label>
                        <textarea type="text" name="app_image1_label" id="app_image1_label" class="form-control" rows="4">{{ $news->app_image1_label }}</textarea>
                        <i style="display: inline-block">Max length of 100 characters </i>
                        <i style="display: inline-block;float:right" id="app_img_txt1"> Characters: 0/100 </i>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-8">
                                <label for="app_image2">Mobile App Image 2 </label>
                                <input type="text" class="form-control" name="app_image2-filename" id="app_image2-filename" disabled value="{{ $news->app_image2 }}">
                                <i>Max size: 500 x 700</i>
                                <span id="newsAppImg2Msg" class="error-msg hide">Image does not meet the image size requirement. Please check again.</br></span>
                            </div>
                            <div class="col-md-4">
                                <label>&nbsp;</label>
                                <span class="btn btn-default btn-file">
                                    Choose File <input type="file" name="app_image2" class="fuImage" ew="500" eh="700" accept="image/jpeg, image/png">
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="app_image2_label">Mobile App Image 2 Text</label>
                        <textarea type="text" name="app_image2_label" id="app_image2_label" class="form-control charcount" rows="4">{{ $news->app_image2_label }}</textarea>
                        <i style="display: inline-block">Max length of 100 characters </i>
                        <i style="display: inline-block;float:right" id="app_img_txt2"> Characters: 0/100 </i>
                    </div>
                    
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-8">
                                <label for="app_image3">Mobile App Image 3 </label>
                                <input type="text" class="form-control" name="app_image3-filename" id="app_image3-filename" disabled value="{{ $news->app_image3 }}">
                                <i>Max size: 500 x 700</i>
                                 <span id="newsAppImg3Msg" class="error-msg hide">Image does not meet the image size requirement. Please check again.</br></span>
                            </div>
                            <div class="col-md-4">
                                <label>&nbsp;</label>
                                <span class="btn btn-default btn-file">
                                    Choose File <input type="file" name="app_image3" class="fuImage" ew="500" eh="700" accept="image/jpeg, image/png">
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="app_image3_label">Mobile App Image 3 Text</label>
                        <textarea type="text" name="app_image3_label" id="app_image3_label" class="form-control charcount" rows="4">{{ $news->app_image3_label }}</textarea>
                        <i style="display: inline-block">Max length of 100 characters </i>
                        <i style="display: inline-block;float:right" id="app_img_txt3"> Characters: 0/100 </i>
                    </div>
                </div>
            </div>
        </div>

        <div class="content button-wrap">
            <button type="submit" name="news_publish" value="true" class="btn btn-primary">Publish</button>
            <button type="submit" name="news_draft" value="true" class="btn btn-default draft">Save as Draft</button>
        </div>
    </form>
</div>
@endsection

@section('footerPlaceholder')
@parent
<script src="{{ url('app/js/vendor/spectrum.js') }}"></script>
<script src="{{ url('app/js/vendor/tinymce/js/tinymce/tinymce.min.js') }}"></script>
<script src="{{ url('app/page/admin/js/tinymce-charactercount.plugin.js') }}"></script>
<script src="{{ url('app/page/admin/js/news.js') }}"></script>
@endsection