@extends('onboarding-cms.layout')

@section('view')

@if (count($errors) > 0)
	<div class="text-center error-display">
	    <div class="alert alert-danger spacing spacing">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	</div>
@endif

@if ( session('message') )
	<div class="text-center error-display spacing">
	    <div class="alert alert-info spacing">{{ session('message') }}</div>
	</div>
@endif

<div class="button-toolbar">
	<a href="{{ url('onboarding-cms/news/create') }}" class="btn btn-primary">Create New</a>
</div>
<div id="content" class="admin-cms">

	<table id="news-table" class="data-table">
		<thead>
			<tr>
				<th>Page Title</th>
				<th>Status</th>
				<th>Published</th>
				<th>Visibility</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@if ($news)
				@foreach ($news as $item)
				<tr>
					<td>{{ $item->title }}</td>
					<td><span class="vis-published">{{ $item->publish_label }}</span></td>
					<td>{{ $item->publish_date_label }}</td>
					<td>
						{{ Form::select('news-visibility-' . $item->id, [ 1 => 'Visible', 0 => 'Hidden' ], $item->visibility, ['class' => 'vis-select', 'data-id' => $item->id]) }}
					</td>
					<td>
	                    <a href="{{ url('onboarding-cms/news/edit/' . $item->id) }}" class="edit"><i class="fa fa-pencil"></i>Edit</a>
						<a href="#" data-id={{ $item->id }} data-toggle="modal" data-target="#deleteNews" data-action="" onclick="return false" class="delete newsDelete"><i class="fa fa-trash"></i>Delete</a>
					</td>
				</tr>
				@endforeach
			@endif
		</tbody>
	</table>
</div>

@include('onboarding-cms.news.modal')
@endsection

@section('footerPlaceholder')
@parent
<script src="{{ url('app/js/vendor/spectrum.js') }}"></script>
<script src="{{ url('app/js/vendor/tinymce/js/tinymce/tinymce.min.js') }}"></script>
<script src="{{ url('app/page/admin/js/news.js') }}"></script>
<script type="text/javascript">
	$("#news-table").on('change', '.vis-select', function() {
		$.ajax({
			method: "POST",
			url: "{{ url('onboarding-cms/news/visibility') }}",
			dataType: "json",
			data: { '_token': '{{ csrf_token() }}', id: $(this).data('id'), value: $(this).val() },
		})
		.done( function(msg) {
		})
		.fail( function(msg) {
			console.log( msg );
		});
	});

	$("#news-table").on('click', '.newsDelete', function() {
		$('#news_id').val( $(this).data('id') );
	});
</script>
@endsection