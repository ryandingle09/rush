<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="Fidelity" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{ $title or "RUSH - Admin" }}</title>

    <!-- Main Admin CSS Stylesheet -->
    <link rel="stylesheet" href="{{ URL::to('/') }}/page/admin/css/style.css">
    <link rel="stylesheet" type="text/css" href="{{ URL::to('/') }}/page/admin/css/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="{{ URL::to('/') }}/page/admin/css/chosen.css">

    <!-- fonts-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,300,300italic,400italic,600italic,700,700italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ URL::to('/') }}/css/vendor/bootstrap-table.css">
    <link rel="stylesheet" href="{{ URL::to('/') }}/css/vendor/font-awesome.css">
    <link rel="stylesheet" href="{{ URL::to('/') }}/css/vendor/bootstrap.css">
    <link rel="stylesheet" href="{{ URL::to('/') }}/css/vendor/spectrum.css" />
    <link rel="stylesheet" href="{{ URL::to('/') }}/css/vendor/bootstrap-datetimepicker.min.css" />
    <!-- <link rel="stylesheet" href="{{ MigrationHelper::getBaseUrl() }}assets/stylesheets/lib/jquery.fileupload.css" /> -->

    <!-- DATATABLES -->
    <link rel="stylesheet" type="text/css" href="{{ URL::to('/') }}/css/vendor/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="{{ URL::to('/') }}/css/vendor/buttons.dataTables.min.css">
    
    

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- <script type="text/javascript" src="assets/bower_components/jquery/dist/ie10-viewport-bug-workaround.js"></script> -->

    <!-- Additional Header items -->
    @section('headerPlaceholder')
    @show
</head>
<body>
