@extends('admin.app')

@section('view')
<div id="content">
  <!-- Nav tabs -->
  <div class="navTabsWrap">
      <ul class="nav nav-tabs" role="tablist" id="tabs">
          <li role="presentation" class="active"><a href="#list" role="tab" data-toggle="tab">Prospects</a></li>
          @if ( $admin_type == 1 )
          <li role="presentation"><a href="#bdaccounts" role="tab" data-toggle="tab">BD Accounts</a></li>
          @endif
      </ul>
  </div>
  <!-- Tab panes -->
  <div class="tab-content">
  <div role="tabpanel" class="tab-pane active" id="list">
    <br /><br /><br />
    <div class="row prospects-table-row-div">
        <div class="col-md-12">
            <table id="prospects-table" class="data-table">
                <thead>
                  <tr>
                    <th class="text-center">ID</th>
                   {{--  <th class="text-left">Fullname</th>
                    <th class="text-left">Firstname</th>
                    <th class="text-left">Lastname</th> --}}
                    <th class="text-left">Email</th>
                    <th class="text-left">Mobile</th>
                    <th class="text-left">Company</th>
                   {{--  <th class="text-left">Industry</th>
                    <th class="text-left">Package</th>
                    <th class="text-left">Objective</th>
                    <th class="text-left">Message</th> --}}
                    <th class="text-left">URL</th>
                    <th class="text-left">IP Address</th>
                    <th class="text-left">BD</th>
                    <th class="text-left">Created at</th>
                    <th class="text-right">Action</th>
                  </tr>
                </thead>
                <tbody>
                @if ( isset($prospects) && $prospects )
                @foreach( $prospects as $prospects )
                  <tr>
                    <td class="text-center"><a href="{{ url('admin/prospects/' . $prospects->id . '/info' ) }}">{{ $prospects->id }}</a></td>
                    {{-- <td class="text-left"> {{ $prospects->fullname }}</td>
                    <td class="text-left"> {{ $prospects->firstname }}</td>
                    <td class="text-left"> {{ $prospects->lastname }}</td> --}}
                    <td class="text-left"> {{ $prospects->email }}</td>
                    <td class="text-left"> {{ $prospects->mobile }}</td>
                    <td class="text-left"> {{ $prospects->company }}</td>
                    {{-- <td class="text-left">
                                          @if( isset($prospects->industry))
                                          @foreach( json_decode($prospects->industry) as $industry ) &bull; {{ $industry }}<br /> @endforeach
                                          @endif
                    </td>
                    <td class="text-left"> {{ $prospects->package }}</td>
                    <td class="text-left">
                                          @if( isset($prospects->objective))
                                          @foreach( json_decode($prospects->objective) as $objective ) &bull; {{ $objective }}<br /> @endforeach
                                          @endif
                    </td>
                    <td class="text-left"> {{ $prospects->message }}</td> --}}
                    <td class="text-left"> {{ $prospects->url }}</td>
                    <td class="text-left"> {{ $prospects->ip_address }}</td>
                    <td class="text-left"> {{ $prospects->bd_account['name'] }}</td>
                    <td class="text-left"> {{ $prospects->created_at }}</td>
                    <td>
                      <a href="{{ url('admin/prospects/' . $prospects->id . '/info' ) }}" class="edit"><i class="fa fa-eye"></i> View</a>
                    </td>
                  </tr>
                @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
  </div>
  @if ( $admin_type == 1 )
  <div role="tabpanel" class="tab-pane" id="bdaccounts">
      <br /><br /><br />
      <div class="row bdaccounts-table-row-div">
          <div class="col-md-12">
            <a class="btn btn-primary yellow-btn" href="{{ url('admin/bdaccounts/add') }}">Add BD Account</a>
          </div>
          <div class="row">
            <div class="col-md-12">&nbsp;</div>
          </div>
          <div class="col-md-12">
              <table id="bdaccounts-table" class="data-table">
                  <thead>
                    <tr>
                      <th class="text-center">ID</th>
                      <th class="text-left">Name</th>
                      <th class="text-left">Email</th>
                      <th class="text-right">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @if ( isset($bd_accounts) && $bd_accounts )
                    @foreach( $bd_accounts as $accounts )
                      <tr>
                        <td class="text-center"><a href="{{ url('admin/bdaccounts/' . $accounts->id . '/info' ) }}">{{ $accounts->id }}</a></td>
                        <td class="text-left"> {{ $accounts->name }}</td>
                        <td class="text-left"> {{ $accounts->email }}</td>
                        <td>
                          <a href="{{ url('admin/bdaccounts/' . $accounts->id . '/delete' ) }}" class="delete delete-bdaccount" data-id="{{ $accounts->id }}"><i class="fa fa-trash"></i> Delete</a>
                          <a href="{{ url('admin/bdaccounts/' . $accounts->id . '/info' ) }}" class="edit"><i class="fa fa-eye"></i> View</a>
                        </td>
                      </tr>
                    @endforeach
                    @endif
                  </tbody>
              </table>
            </div>
      </div>
  </div>
  @endif
  </div>
</div>
</div>
@endsection

@section('footerPlaceholder')
<script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
<script type="text/javascript">
  $(document).ready( function() {
    var emptyTableMessage = 'No matching records found';
    var excelExportButton = '<div class="text-center"><button type="button" class="btn btn-default exportButton">Export to Excel</button></div>';

    /* Initialize Prospects Table DataTable */
    $('#prospects-table').dataTable({
      'sDom': '<"fixed-table-toolbar"fl><"excelButton hide"B>t<"bottom"ip><"clear">',
      'buttons': [{
                  extend: 'excelHtml5',
                  exportOptions: {
                      columns: [0,1,2,3,4,5,6,7,8]
                  }
                }],
      "columnDefs": [
                      {
                        "targets": [4,5],
                        "visible": false
                      }
                    ],
      'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
      'language': { 'emptyTable': emptyTableMessage },
      'paging': true,
      'ordering': false,
      'pagingType': "full_numbers"
    });

    @if ( $admin_type == 1 )
    $('#bdaccounts-table').DataTable({
      'sDom': '<"fixed-table-toolbar"fl><"excelButton hide"B>t<"bottom"ip><"clear">',
      'buttons': [ 'excel' ],
      'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
      'language': { 'emptyTable': emptyTableMessage },
    });
    @endif

    if ($('#prospects-table tbody tr.odd td').text() != emptyTableMessage) {
      $('.prospects-table-row-div').append(excelExportButton);
    }

    // bind excel export button
    $('.tab-pane').on('click', '.exportButton', function(event) {
        $(this).parent().parent().find('.dataTables_wrapper .excelButton a').click();
    });

    @if ( $admin_type == 1 )
    $('.delete-bdaccount').on('click', function(e) {
        e.preventDefault();
        var currentElement = $(this);

        swal({
                title: "Are you sure?",
                text: "You will not be able to recover the data again!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#f6ae1c",
                confirmButtonText: "Yes, delete!",
                closeOnConfirm: false
            },
            function () {
                window.location.href = currentElement.attr('href');
            }
        );
    });
    @endif

  });
</script>
@endsection
