@extends('admin.app')

@section('view')
<div id="content" class="merchant-view">
  <!-- Nav tabs -->
  <div class="navTabsWrap">
      <ul class="nav nav-tabs" id="tabs">
          <li class="active"><a>BD Account</a></li>
      </ul>
  </div>
  <!-- Tab panes -->
  <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="info">
      <div class="row">
          <div class="col-md-12"> &nbsp;</div>
      </div>
      @if ( !$bdaccount )
      <div class="row">
          <div class="col-md-12">BD Account is not existing!</div>
      </div>
      @else
      <form action="{{ url('admin/bdaccounts/'. $bdaccount->id .'/info') }}" method="POST">
        {{ csrf_field() }}
      <div class="row">
          <div class="col-md-12">
              <div class="row">
                  <div class="col-md-6">
                    <label class="col-md-3">Name</label>
                    <div class="col-md-8">
                      <input type="text" class="form-control" name="name" id="name" value="{{ $bdaccount->name }}" required>
                      @if ($errors->has('name'))
                        <div class="error">{{ $errors->first('name', ':message') }}</div>
                      @endif
                    </div>
                  </div>
                  <div class="col-md-6">
                    <label class="col-md-3">Email</label> {{ $bdaccount->email }}
                  </div>
              </div>
          </div>
          <div class="row">
              <div class="col-md-12"> &nbsp; </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-5 form-group">
                <label>Admin Modules</label>
              </div>
            </div>
            <div class="row modules-options">
                @if($admin_modules)
                  @foreach($admin_modules as $module)
                  <div class="col-md-3 form-group">
                    {{ Form::checkbox( 'admin_modules[]', $module->id, ( in_array( $module->id, $bdaccount_modules) ? 1 : 0 ), ['id' => 'modules_' . $module->id]) }}
                    <label class="modules-label" for="modules_{{$module->id}}">{{ $module->name }}</label>
                  </div>
                  @endforeach
                @endif
            </div>
            @if ($errors->has('admin_modules'))
              <div class="col-md-12"><div class="error">{{ $errors->first('admin_modules', ':message') }}</div></div>
            @endif
          </div>
          <div class="col-md-12">
            <div class="row">&nbsp;</div>
          </div>
          <div class="col-md-12 align-right">
            <div class="row merchant-view-buttons">
              <div class="col-md-1 form-group">
                {{ Form::submit('Update', ['class'=>'btn btn-primary']) }}
              </div>
              <div class="col-md-1 form-group">
                {{ link_to( url('admin/prospects'), "Back", $attributes = array('class'=>'btn btn-warning'), $secure = null) }}
              </div>
            </div>
          </div>
      </div>
      </form>
      @endif
    </div>
  </div>
</div>
@endsection

@section('footerPlaceholder')
<script type="text/javascript">
@if( $success && $message )
  swal( "{{ $message }}", "", "success")
@endif
</script>
@endsection
