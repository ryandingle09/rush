@extends('admin.app')

@section('view')
<div id="content" class="merchant-view">
  <!-- Nav tabs -->
  <div class="navTabsWrap">
      <ul class="nav nav-tabs" id="tabs">
          <li class="active"><a>BD Account</a></li>
      </ul>
  </div>
  <!-- Tab panes -->
  <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="info">
      <div class="row">
          <div class="col-md-12"> &nbsp;</div>
      </div>
      <form action="{{ url('admin/bdaccounts/add') }}" method="POST">
        {{ csrf_field() }}
      <div class="row">
          <div class="col-md-12">
              <div class="row">
                  <div class="col-md-6">
                    <label class="col-md-3">Name</label>
                    <div class="col-md-8">
                      <input type="text" class="form-control" name="name" id="name" value="{{ old('name', '') }}" required>
                      @if ($errors->has('name'))
                        <div class="error">{{ $errors->first('name', ':message') }}</div>
                      @endif
                    </div>
                  </div>
                  <div class="col-md-6">
                    <label class="col-md-3">Email</label>
                    <div class="col-md-8">
                    <input type="text" class="form-control" name="email" id="email" value="{{ old('email', '') }}" required>
                      @if ($errors->has('email'))
                        <div class="error">{{ $errors->first('email', ':message') }}</div>
                      @endif
                    </div>
                  </div>
              </div>
          </div>
          <div class="row">
              <div class="col-md-12"> &nbsp; </div>
          </div>
          <div class="col-md-12">
              <div class="row">
                  <div class="col-md-6">
                    <label class="col-md-3">Password</label>
                    <div class="col-md-8">
                      <input type="text" class="form-control" name="password" id="password" value="{{ old('password', '') }}" required>
                      @if ($errors->has('password'))
                        <div class="error">{{ $errors->first('password', ':message') }}</div>
                      @endif
                    </div>
                  </div>
              </div>
          </div>
          <div class="row">
              <div class="col-md-12"> &nbsp; </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-5 form-group">
                <label>Admin Modules</label>
              </div>
            </div>
            <div class="row modules-options">
                @if($admin_modules)
                  @foreach($admin_modules as $module)
                  <div class="col-md-3 form-group">
                    {{ Form::checkbox( 'admin_modules[]', $module->id, 0, ['id' => 'modules_' . $module->id]) }}
                    <label class="modules-label" for="modules_{{$module->id}}">{{ $module->name }}</label>
                  </div>
                  @endforeach
                @endif
            </div>
            @if ($errors->has('admin_modules'))
              <div class="col-md-12"><div class="error">{{ $errors->first('admin_modules', ':message') }}</div></div>
            @endif
          </div>
          <div class="col-md-12">
            <div class="row">&nbsp;</div>
          </div>
          <div class="col-md-12 align-right">
            <div class="row merchant-view-buttons">
              <div class="col-md-1 form-group">
                {{ Form::submit('Update', ['class'=>'btn btn-primary']) }}
              </div>
              <div class="col-md-1 form-group">
                {{ link_to( url('admin/prospects'), "Back", $attributes = array('class'=>'btn btn-warning'), $secure = null) }}
              </div>
            </div>
          </div>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection