@extends('admin.app')

@section('view')
<div id="content" class="merchant-view">
  <!-- Nav tabs -->
  <div class="navTabsWrap">
      <ul class="nav nav-tabs" id="tabs">
          <li class="active"><a>INFO</a></li>
      </ul>
  </div>
  <!-- Tab panes -->
  <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="info">
      <div class="row">
          <div class="col-md-12"> &nbsp; </div>
      </div>
      <form action="{{ url('admin/prospects/'. $prospect->id .'/info') }}" method="POST">
        {{ csrf_field() }}
      <div class="row">
          <div class="col-md-12">
             {{--  <div class="row">
                  <div class="col-md-6">
                    <label class="col-md-3">Fullname</label> {{ $prospect->fullname }}
                  </div>
                  <div class="col-md-6">
                    <label class="col-md-3">Firstname</label> {{ $prospect->firstname }}
                  </div>
              </div> --}}
              <div class="row">
                  <div class="col-md-6">
                    <label class="col-md-3">Email</label> {{ $prospect->email }}
                  </div>
                  <div class="col-md-6">
                    <label class="col-md-3">Mobile</label> {{ $prospect->mobile }}
                  </div>
                  {{-- <div class="col-md-6">
                    <label class="col-md-3">Lastname</label> {{ $prospect->lastname }}
                  </div> --}}
              </div>
              <div class="row">
                  <div class="col-md-6">
                    <label class="col-md-3">Company</label> {{ $prospect->company }}
                  </div>
                  <div class="col-md-6">
                    <label class="col-md-3">IP Address</label> {{ $prospect->ip_address }}
                  </div>
              </div>
              {{-- <div class="row">
                  <div class="col-md-6">
                    <label class="col-md-3">Package</label> {{ $prospect->package }}
                  </div>
                  <div class="col-md-6">
                    <label class="col-md-3">Industry</label>
                      @foreach( json_decode($prospect->industry) as $industry )
                        &bull; {{ $industry }}<br />
                      @endforeach
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-6">
                    <label class="col-md-3">Objective</label>
                    @if( isset($prospect->objective) )
                      @foreach( json_decode($prospect->objective) as $objective )
                        &bull; {{ $objective }}<br />
                      @endforeach
                    @endif
                  </div>
              </div> --}}
              <div class="row">
                  <div class="col-md-6">
                    <label class="col-md-3">URL</label> {{ $prospect->url }}
                  </div>
                  
              </div>
             {{--  <div class="row">
                  <div class="col-md-12">
                    <label class="col-md-2">Message</label>
                  </div>
                  <div class="col-md-12">
                    <div class="col-md-12">{{ $prospect->message }}</div>
                  </div>
              </div> --}}
              <div class="row">
                  <div class="col-md-12"> &nbsp; </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                    <label class="col-md-3">Date</label> {{ $prospect->created_at }}
                </div>
                <div class="col-md-2">
                    <label class="col-md-12">BD Assigned</label>
                </div>
                <div class="col-md-4">
                    {!! Form::select('bd_assigned', $bd_accounts, $prospect->bd_assigned, ['id' => 'bd_assigned', 'class' => 'form-control', 'placeholder' => 'Select from list...', 'required' => 'true'] ) !!}
                </div>
              </div>
          </div>
          <div class="row">
              <div class="col-md-12"> &nbsp; </div>
          </div>
          <div class="col-md-12 align-right">
            <div class="row merchant-view-buttons">
              <div class="col-md-1 form-group">
                {{ Form::submit('Update', ['class'=>'btn btn-primary']) }}
              </div>
              <div class="col-md-1 form-group">
                {{ link_to( url('admin/prospects'), "Back", $attributes = array('class'=>'btn btn-warning'), $secure = null) }}
              </div>
            </div>
          </div>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('footerPlaceholder')
@if( isset($success) )
<script>
  $(document).ready( function() {
    swal("The Prospect Info has been updated!", "", "success")
  });
</script>
@endif
@endsection
