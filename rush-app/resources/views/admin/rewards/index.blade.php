@extends('admin.app')
@section('view')
<div id="content">
    <!-- Nav tabs -->
  <div class="navTabsWrap">
      <ul class="nav nav-tabs" id="tabs">
          <li class="active"><a>REWARDS</a></li>
          <li><a href="{{ URL::to('/') }}/admin/rewards/redemption">REDEMPTION</a></li>
      </ul>
  </div>
  <!-- Tab panes -->
  <div class="tab-content">
      <div role="tabpanel" class="tab-pane active management-rewards">
        <div class="row">
            <div class="col-md-12">
            <table id="merchant-rewards-table" class="data-table">
              <thead>
                <tr>
                  <th class="text-left">Reward Name</th>
                  <th class="text-center">Reward Image</th>
                  <th class="text-left">Description</th>
                  <th class="text-center">Amount</th>
                  <th class="text-center">Subscription Type</th>
                  <th class="text-center">Rewards Status</th>
                </tr>
              </thead>
              <tbody>
              {{-- */
                $i = 1;
              /* --}}
              @foreach($rewards as $reward)
                <tr id="reward-row-{{ $reward->id }}">
                  <td class="text-left"><a href="/{{MigrationHelper::getAppPrefix()}}/admin/rewards/{{ $reward->id }}">{{ $reward->name }}</a></td>
                  <td class="text-center">
                    <img class="rewards-img" src="{{ $reward->imageURL }}" />
                  </td>
                  <td class="text-left">{{ $reward->details }}</td>
                  <td class="text-center">{{ $reward->pointsRequired }}</td>
                  <td class="text-center">{{ $reward->type_to_text }}</td>
                  <td class="text-center">
                    <a data-id="{{ $reward->id }}" class="delete-reward {{ $reward->status_to_text }}" style="text-transform: capitalize;" onclick="return false">{{ $reward->status_to_text }}</a>
                  </td>
                </tr>
                {{-- */
                  $i++;
                /* --}}
              @endforeach
              </tbody>
            </table>
            </div>
            <div class="col-md-12">
              <a href="/{{MigrationHelper::getAppPrefix()}}/admin/rewards/add" class="btn btn-rush">Add New</a>
            </div>
        </div>
    </div>
    </div>
</div><!--/content-->
@endsection

@section('footerPlaceholder')
@parent
<script type="text/javascript">
    $(document).ready( function() { /* Document Ready Open */
      $('#merchant-rewards-table').dataTable({
        paging: true,
        ordering: false,
        searching: false,
        info: false,
      });

      $('.delete-reward').on("click", function() {
        var _id = $(this).attr('data-id');
        swal({
          title: "",
          text: "Are you sure you want to change the status of this entry?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, change it!",
          closeOnConfirm: false
        },
        function(){
          var _form = $('<form/>');
          _form.attr('action', "/{{MigrationHelper::getAppPrefix()}}/admin/reward/status");
          _form.attr('method', "POST");
          $("<input type='hidden' name='id' value='"+_id+"' />").appendTo( _form );
          $('{{csrf_field()}}').appendTo( _form );
          _form.submit();
        });
      });

    });
</script>
@endsection