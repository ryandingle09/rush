@extends('admin.app')
@section('view')
<div id="content">
    <!-- Nav tabs -->
  <div class="navTabsWrap">
      <ul class="nav nav-tabs" id="tabs">
          <li><a href="{{ URL::to('/') }}/admin/rewards">REWARDS</a></li>
          <li class="active"><a>REDEMPTION</a></li>
      </ul>
  </div>
  <!-- Tab panes -->
  <div class="tab-content">
      <div role="tabpanel" class="tab-pane active management-rewards">
        <div class="row">
        <div class="col-md-6">
            <br /><br />
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class='input-group date datetimepicker' id='dateStart'>
                            <input type='text' class="form-control" name="dateStart" placeholder="Date Start" data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="" required>
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class='input-group date datetimepicker' id='dateEnd'>
                            <input type='text' class="form-control" name="dateEnd" placeholder="Date End" data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="" required>
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="row">
            <div class="col-md-12">
            <table id="merchant-rewards-redemption-table" class="data-table">
              <thead>
                <tr>
                  <th class="text-left">Date</th>
                  <th class="text-left">Merchant</th>
                  <th class="text-center">Mobile Number</th>
                  <th class="text-right">Points</th>
                  <th class="text-right">Amount</th>
              </thead>
              <tbody>
              @foreach($transactions as $transaction)
                <tr>
                  <td class="text-left">{{ $transaction->timestamp }}</td>
                  <td class="text-left">{{ $transaction->businessName }}</td>
                  <td class="text-center">{{ $transaction->mobileNumber }}</td>
                  <td class="text-right">{{ $transaction->pointsRedeem }}</td>
                  <td class="text-right">{{ $transaction->pointsRequired }}</td>
                </tr>
              @endforeach
              </tbody>
            </table>
            </div>
        </div>
    </div>
    </div>
</div><!--/content-->
@endsection

@section('footerPlaceholder')
<script type="text/javascript">
  $(document).ready( function() {
    var emptyTableMessage = 'No matching records found';
    var excelExportButton = '<div class="text-center"><button type="button" class="btn btn-default exportButton">Export to Excel</button></div>';

    $.fn.dataTableExt.afnFiltering.push(
        function(oSettings, aData, iDataIndex) {
            var iFini = $('#dateStart input').val();
            var iFfin = $('#dateEnd input').val();
            var iStartDateCol = 0;
            var iEndDateCol = 0;

            var datofini = aData[iStartDateCol].substring(0, 10);
            var datoffin = aData[iEndDateCol].substring(0, 10);

            if (iFini === "" && iFfin === "") {
                return true;
            } else if (iFini <= datofini && iFfin === "") {
                return true;
            } else if (iFfin >= datoffin && iFini === "") {
                return true;
            } else if (iFini <= datofini && iFfin >= datoffin) {
                return true;
            }
            return false;
        }
    );

    /* Initialize Branches Table DataTable */
    var table = $('#merchant-rewards-redemption-table').DataTable({
      'sDom': '<"fixed-table-toolbar"fl><"excelButton hide"B>t<"bottom"ip><"clear">',
      'processing': true,
      'buttons': [ 'excel' ],
      'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
      'language': { 'emptyTable': emptyTableMessage },
    });

    if ($('#merchant-rewards-redemption-table tbody tr.odd td').text() != emptyTableMessage) {
      $('.tab-content').append(excelExportButton);
    }

    $('#dateStart, #dateEnd').datetimepicker({
        format: 'YYYY-MM-DD'
    });

    $('#dateStart, #dateEnd').on('dp.change', function(e) {
        table.draw();
        if ($(this).attr('id') == 'dateStart') {
            $("#dateEnd").data("DateTimePicker").minDate($("#dateStart").data("DateTimePicker").date().format('YYYY-MM-DD'));
        } else {
            $("#dateStart").data("DateTimePicker").maxDate($("#dateEnd").data("DateTimePicker").date().format('YYYY-MM-DD'));
        }
    });

    // bind excel export button
    $('.tab-content').on('click', '.exportButton', function(event) {
        $(this).parent().parent().find('.dataTables_wrapper .excelButton a').click();
    });

  });
</script>

@endsection