@extends('admin.app')
@section('view')
<div id="content" class="management-rewards-add">
    <div class="noTab">
    <form action="/{{MigrationHelper::getAppPrefix()}}/admin/rewards/{{ $reward->id }}" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="status" value="{{ $reward->status }}" />
      <div class="row form-group">
        <div class="col-md-2">
          <label for="name">Reward Name</label>
        </div>
        <div class="col-md-4">
          <input type="text" name="name" id="name" class="form-control" value="{{ $reward->name }}" required />
          <span class="error-msg help-block">
            @if( $errors->has('name'))
               {{ $errors->first('name') }}
            @endif
          </span>
        </div>
      </div>
      <div class="row form-group">
        <div class="col-md-2">
          <label for="details">Reward Description</label>
        </div>
        <div class="col-md-4">
         {{ Form::textarea('details', $reward->details , [ "class" => "form-control", "id" => "details"] ) }}
         <br />
         <span class="error-msg help-block">
            @if($errors->has('details'))
               {{ $errors->first('details') }}
            @endif
          </span>
        </div>
      </div>
      <div class="row form-group">
        <div class="col-md-2">
          <label for="points">Amount</label>
        </div>
        <div class="col-md-4">
          <input type="number" name="points" id="points" class="form-control" value="{{ $reward->pointsRequired }}" required />
          <span class="error-msg help-block">
            @if($errors->has('points'))
               {{ $errors->first('points') }}
            @endif
          </span>
        </div>
      </div>
      <div class="row form-group">
        <div class="col-md-2">
          <label for="points">Subcription Type</label>
        </div>
        <div class="col-md-4">
          @foreach( $subscription_types as $subscription_type)
            {{ Form::checkbox('type[]', $subscription_type->brand, ( $reward->type ) ? in_array($subscription_type->brand, json_decode($reward->type) ) : false, [ "class" => "form-group", "id" => "type_" . trim($subscription_type->brand) ]) }}
            <label for="type_{{ trim($subscription_type->brand) }}" /> {{ $subscription_type->brand }} </label>
          @endforeach
          <span class="error-msg help-block">
            @if($errors->has('type'))
               {{ $errors->first('type') }}
            @endif
          </span>
        </div>
      </div>
      <div class="row form-group">
        <div class="col-md-2">
          <label>Image</label>
        </div>
        <div class="col-md-2">
          <label class="btn btn-rush btn-file">
            Browse<input type="file" name="image" id="image" accept="image/*"/>
          </label>
          <br />
          <em >Required image size: 350px x 200px</em>
        </div>
        <div class="col-md-2">
          <img class="img-preview" style="height: 100px; width: 100px;" src="{{ $reward->imageURL }}"/>
        </div>
        <div class="col-md-3">
          <span class="error-msg help-block">
            @if($errors->has('image'))
               {{ $errors->first('image') }}
            @endif
          </span>
        </div>
      </div>
      <div class="row form-group">
        <div class="col-md-1">
          <input type="submit" class="btn btn-rush" value="Save" />
        </div>
        <div class="col-md-1">
          <a href="/{{MigrationHelper::getAppPrefix()}}/admin/rewards" class="btn btn-rush">Back</a>
        </div>
      </div>
    </form>
    </div>
</div><!--/content-->
@endsection

@section('footerPlaceholder')
@parent
<script type="text/javascript">
 $(document).ready( function() {

    $("#image").bind( "change", function (event) {
        var _file = event.target.files[0];
        var _file_as_url = URL.createObjectURL(event.target.files[0]);

        if ( (/\.(png|jpeg|jpg)$/i).test( _file.name) ) {
          var _img = new Image();
          var height, width;
          _img.onload = function() {
            if ( this.width == 350 && this.height == 200 ) {
              $(".img-preview").attr("src", this.src );  
            } else {
              $(".img-preview").attr("src", null );
              $("#image").val();
              swal("Error", "Invalid image height and width", "error");
            }
          }
          _img.src = _file_as_url;
        } else {
          swal('Error', "Invalid filetype!", 'error');
        }
    });

 });
</script>
@endsection