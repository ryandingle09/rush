@extends('admin.app')

@section('view')
<div class="container">
   <div id="content" class="home spacing">
     <div class="row">
      @foreach( $modules as $module_item )
      <div class="module">
        <a class="module-icon icon-{{ $module_item->code }}" href="{{ url( 'admin/'.$module_item->url ) }}" title="{{ $module_item->name }}">
        <span class="module-text">{{ $module_item->name }}</span>
        </a>
      </div>
      @endforeach
     </div>
   </div>
</div> <!-- /container -->
@endsection
