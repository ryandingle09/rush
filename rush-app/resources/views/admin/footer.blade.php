</div><!-- Close container div -->
</div><!-- Close rightcontent div -->

@section('modal')
@show

<script type="text/javascript" src="{{ URL::to('/') }}/js/vendor/jquery.min.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/vendor/jquery.dataTables.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/vendor/buttons.html5.min.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/vendor/jquery.validate.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/vendor/moment.min.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/vendor/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/vendor/bootstrap-table.min.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/vendor/jquery.fittext.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/vendor/jquery.activeNavigation.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/vendor/bootstrap-table-filter-control.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/vendor/spectrum.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/vendor/background-blur.min.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/vendor/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/vendor/load-image.all.min.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/vendor/jquery.fileupload.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/vendor/jquery.fileupload-process.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/vendor/jquery.fileupload-image.js"></script>
<!-- <script type="text/javascript" src="{{ MigrationHelper::getBaseUrl() }}assets/javascripts/lib/my.class.min.js"></script> -->
<script type="text/javascript" src="{{ URL::to('/') }}/page/admin/js/sweetalert.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>

<script type="text/javascript" src="{{ URL::to('/') }}/page/admin/js/chosen.jquery.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/vendor/bootstrap-datetimepicker.min.js"></script>

@section('footerPlaceholder')
<!-- Additional Footer items -->
@show
</body>
</html>
