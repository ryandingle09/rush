@extends('admin.app')

@section('view')
<div id="content" class="management marginTop">
  <!-- Nav tabs -->
  <div class="navTabsWrap">
    <ul class="nav nav-tabs" role="tablist" id="tabs">
      <li><a href="{{ URL::to('/') }}/admin/management">BRANCH</a></li>
      <li><a href="{{ URL::to('/') }}/admin/management/employees">EMPLOYEE</a></li>
      <li class="active"><a>CUSTOMER</a></li>
      <li><a href="{{ URL::to('/') }}/admin/management/points">POINTS SUMMARY</a></li>
    </ul>
  </div>
  <!-- Tab panes -->
  <div class="tab-content">
    <div class="tab-pane active" id="customer">
      <div class="row">
        <div class="col-md-12">
        </div>
      </div>
      <div class="row" id="management-customers-view">
        <div class="md-col-10">
          <table id="customerTable" class="data-table">
              <thead>
              <tr>
                <th class="text-left">Merchant</th>
                <th class="text-center">Date Registered</th>
                <th class="text-left">Customer Name</th>
                <th class="text-right">Mobile No.</th>
                <th class="text-left">Email Address</th>
                <th class="text-center">Birthdate</th>
                <th class="text-left">Gender</th>
                <th class="text-left">Registration Channel</th>
                <th class="text-left">Branch</th>
              </tr>
              </thead>
              <tbody>
              @if(isset($customers) && $customers)
              @foreach($customers as $customer )
              <tr>
                <td class="text-left">{{ ( $customer->merchant ) ? $customer->merchant->businessName : '' }}</td>
                <td class="text-center">{{ ( $customer->timestamp ) ? \Carbon\Carbon::parse( $customer->timestamp )->format('m-d-Y') : '' }}</td>
                <td class="text-left">{{ $customer->fullName }}</td>
                <td class="text-right">{{ $customer->mobileNumber }}</td>
                <td class="text-left">{{ $customer->email }}</td>
                <td class="text-center">{{ $customer->birthDate }}</td>
                <td class="text-left">{{ $customer->gender }}</td>
                <td class="text-left">{{ $customer->registrationChannel }}</td>
                <td class="text-left">{{ ( $customer->branch ) ? $customer->branch->branchName : '' }}</td>
              </tr>
              @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div><!--/content-->
@endsection

@section('footerPlaceholder')
<script type="text/javascript">
  $(document).ready( function() {
    var emptyTableMessage = 'No matching records found';
    var excelExportButton = '<div class="text-center"><button type="button" class="btn btn-default exportButton">Export to Excel</button></div>';

    /* Initialize Branches Table DataTable */
    $('#customerTable').DataTable({
      'sDom': '<"fixed-table-toolbar"fl><"excelButton hide"B>t<"bottom"ip><"clear">',
      'buttons': [ 'excel' ],
      'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
      'language': { 'emptyTable': emptyTableMessage },
    });

    if ($('#customerTable tbody tr.odd td').text() != emptyTableMessage) {
      $('#management-customers-view').append(excelExportButton);
    }

    // bind excel export button
    $('.tab-pane').on('click', '.exportButton', function(event) {
        $(this).parent().parent().find('.dataTables_wrapper .excelButton a').click();
    });

  });
</script>
@endsection
