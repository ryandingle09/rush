@extends('admin.app')

@section('view')
<div id="content" class="management marginTop">
  <!-- Nav tabs -->
  <div class="navTabsWrap">
    <ul class="nav nav-tabs" role="tablist" id="tabs">
      <li><a href="{{ URL::to('/') }}/admin/management">BRANCH</a></li>
      <li><a href="{{ URL::to('/') }}/admin/management/employees">EMPLOYEE</a></li>
      <li><a href="{{ URL::to('/') }}/admin/management/customers">CUSTOMER</a></li>
      <li class="active"><a>POINTS SUMMARY</a></li>
    </ul>
  </div>
  <!-- Tab panes -->
  <div class="tab-content">
    <div class="tab-pane active" id="points">
      <div class="row">
        <div class="col-md-12">
        </div>
      </div>
      <div class="row" id="management-points-view">
        <div class="md-col-10">
          <table id="pointsTable" class="data-table">
              <thead>
                <tr>
                    <th class="text-left">Merchant</th>
                    <th class="text-center">Date Registered</th>
                    <th class="text-left">Customer Name</th>
                    <th class="text-right">Mobile No.</th>
                    <th class="text-right">Earned</th>
                    <th class="text-right">Burned</th>
                    <th class="text-right">Available</th>
                </tr>
              </thead>
              <tbody>
              @if(isset($points) && $points)
              @foreach($points as $point )
              <tr>
                <td class="text-left">
                  @if ( $point->customer )
                    {{ $point->customer->merchantId }}
                  @endif
                </td>
                <td class="text-center">{{ \Carbon\Carbon::parse( $point->timestamp )->format('m-d-Y') }}</td>
                <td class="text-left">
                  @if ( $point->customer )
                      {{ $point->customer->fullName }}
                  @endif
                </td>
                <td class="text-right">
                  @if ( $point->customer )
                      {{ $point->customer->mobileNumber }}
                  @endif
                </td>
                <td class="text-right">{{ $point->totalPoints }}</td>
                <td class="text-right">{{ $point->usedPoints }}</td>
                <td class="text-right">{{ $point->currentpoints }}</td>
              </tr>
            @endforeach
            @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div><!--/content-->
@endsection

@section('footerPlaceholder')
<script type="text/javascript">
  $(document).ready( function() {
    var emptyTableMessage = 'No matching records found';
    var excelExportButton = '<div class="text-center"><button type="button" class="btn btn-default exportButton">Export to Excel</button></div>';

    /* Initialize Branches Table DataTable */
    $('#pointsTable').DataTable({
      'sDom': '<"fixed-table-toolbar"fl><"excelButton hide"B>t<"bottom"ip><"clear">',
      'buttons': [ 'excel' ],
      'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
      'language': { 'emptyTable': emptyTableMessage },
    });

    if ($('#pointsTable tbody tr.odd td').text() != emptyTableMessage) {
      $('#management-points-view').append(excelExportButton);
    }

    // bind excel export button
    $('.tab-pane').on('click', '.exportButton', function(event) {
        $(this).parent().parent().find('.dataTables_wrapper .excelButton a').click();
    });

  });
</script>
@endsection
