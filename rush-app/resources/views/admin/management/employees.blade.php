@extends('admin.app')

@section('view')
<div id="content" class="management marginTop">
  <!-- Nav tabs -->
  <div class="navTabsWrap">
    <ul class="nav nav-tabs" role="tablist" id="tabs">
      <li><a href="{{ URL::to('/') }}/admin/management">BRANCH</a></li>
      <li  class="active"><a>EMPLOYEE</a></li>
      <li><a href="{{ URL::to('/') }}/admin/management/customers">CUSTOMER</a></li>
      <li><a href="{{ URL::to('/') }}/admin/management/points">POINTS SUMMARY</a></li>
    </ul>
  </div>
  <!-- Tab panes -->
  <div class="tab-content">
    <div class="tab-pane active" id="employee">
      <div class="row">
        <div class="col-md-12">
        </div>
      </div>
      <div class="row" id="management-employees-view">
        <div class="md-col-10">
          <table id="employeeTable" class="data-table">
              <thead>
              <tr>
                  <th class="text-left">Employee Name</th>
                  <th class="text-left">Merchant</th>
                  <th class="text-left">Branch Assigned</th>
                  <th class="text-right">Status</th>
              </tr>
              </thead>
              <tbody>
              @if(isset($employees) && $employees)
              @foreach($employees as $employee )
              <tr>
                <td class="text-left">{{ $employee->employeeName }}</td>
                <td class="text-left">{{ $employee->merchant->businessName }}</td>
                <td class="text-left">{{ $employee->branch->branchName }}</td>
                <td class="text-right {{ $employee->is_deleted_text }} is_deleted_text">{{ $employee->is_deleted_text }}</td>
              </tr>
            @endforeach
            @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div><!--/content-->
@endsection


@section('footerPlaceholder')
<script type="text/javascript">
  $(document).ready( function() {
    var emptyTableMessage = 'No matching records found';
    var excelExportButton = '<div class="text-center"><button type="button" class="btn btn-default exportButton">Export to Excel</button></div>';

    /* Initialize Branches Table DataTable */
    $('#employeeTable').DataTable({
      'sDom': '<"fixed-table-toolbar"fl><"excelButton hide"B>t<"bottom"ip><"clear">',
      'buttons': [ 'excel' ],
      'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
      'language': { 'emptyTable': emptyTableMessage },
    });

    if ($('#employeeTable tbody tr.odd td').text() != emptyTableMessage) {
      $('#management-employees-view').append(excelExportButton);
    }

    // bind excel export button
    $('.tab-pane').on('click', '.exportButton', function(event) {
        $(this).parent().parent().find('.dataTables_wrapper .excelButton a').click();
    });

  });
</script>
@endsection
