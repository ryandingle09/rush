@extends('admin.app')

@section('view')
<div id="content">
  <div id="settings-view">
    <form action="{{ URL::to('/').'/admin/settings' }}" method="POST">
    {{ csrf_field() }}
    <div class="row">
      <div class="col-md-12">

        <div class="row">
          <div class="col-md-5">
            <label class="control-label" for="firstname">Firstname</label>
            <input class="form-control" type="text" name="firstname" id="firstname" value="{{ $admin->firstname }}">
          </div>
        </div>
        <br />
        <div class="row">
          <div class="col-md-5">
            <label class="control-label" for="lastname">Lastname</label>
            <input class="form-control" type="text" name="lastname" id="lastname" value="{{ $admin->lastname }}">
          </div>
        </div>
        <br />
        <div class="row">
          <div class="col-md-5">
            <label class="control-label" for="password">Password</label>
            <input class="form-control" type="password" name="password" id="password" value="">
          </div>
        </div>
        <br />
        <div class="row settings-view-buttons">
          <div class="col-md-1 form-group">
            {{ Form::submit('Update', ['class'=>'btn btn-primary']) }}
          </div>
        </div>
      </div>
    </div>
    </form>
  </div>
</div>
@endsection

@section('footerPlaceholder')
  <script type="text/javascript">
    $(document).ready( function() {
      @if ( isset($updated) && $updated == TRUE )
        swal("The Admin Info has been updated!", "", "success")
      @endif
    });
  </script>
@endsection
