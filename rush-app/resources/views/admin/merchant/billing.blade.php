@extends('admin.app')

@section('view')
<div id="content" class="merchant-view">
  <!-- Nav tabs -->
  <div class="navTabsWrap">
    <ul class="nav nav-tabs" role="tablist" id="tabs">
      <ul class="nav nav-tabs" id="tabs">
          <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}">INFO</a></li>
          <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/branches">BRANCH</a></li>
          <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/employees">EMPLOYEE</a></li>
          <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/customers">CUSTOMER</a></li>
          <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/points">POINTS</a></li>
          <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/transactions">TRANSACTIONS</a></li>
          <li class="active"><a>BILLING</a></li>
      </ul>
    </ul>
  </div>
  <!-- Tab panes -->
  <div class="tab-content">
    <div class="tab-pane active" id="billing">
      <div class="row" id="management-billing-view" style="margin-top: none;">
        <div class="row">
            <div class="col-md-12">
                <table data-toggle="table" class="data-table account" data-pagination="false" data-search="false">
                    <thead>
                        <tr>
                            <th class="text-left">Bill Number </th>
                            <th class="text-left">Billing Period</th>
                            <th class="text-left">Statement Date</th>
                            <th class="text-left">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach ($billing_history as $billing_data)
                      <tr>
                          <td class="text-left">{{ $billing_data->merchant_id .'-'. $billing_data->billing_number }}</td>
                          <td class="text-left">{{ Carbon\Carbon::parse($billing_data->billing_period_start)->format('F j') }} - {{ Carbon\Carbon::parse($billing_data->billing_period_end)->format('F j, Y') }}</td>
                          <td class="text-left">{{ Carbon\Carbon::parse($billing_data->statement_date)->format('F j, Y') }}</td>
                          <td class="text-left">
                            <a class="btn btn-primary yellow-btn" href="{{ MigrationHelper::getAppBaseUrl() }}admin/merchant/{{ $merchant_id}}/billing-view/{{ $billing_data->id}}">View Bill</a>
                            <a class="btn btn-primary yellow-btn" href="{{ MigrationHelper::getAppBaseUrl() }}admin/merchant/{{ $merchant_id}}/statement/{{$billing_data->id}}" target="_new">Download PDF</a>
                          </td>
                      </tr>
                      @endforeach
                    </tbody>
                </table>
            </div>
        </div>
      </div>
    </div>
  </div>
</div><!--/content-->
@endsection

@section('footerPlaceholder')
<script type="text/javascript">
  $(document).ready( function() {
    var emptyTableMessage = 'No matching records found';
    var excelExportButton = '<div class="text-center"><button type="button" class="btn btn-default exportButton">Export to Excel</button></div>';

    $.fn.dataTableExt.afnFiltering.push(
        function(oSettings, aData, iDataIndex) {
            var iFini = $('#dateStart input').val();
            var iFfin = $('#dateEnd input').val();
            var iStartDateCol = 0;
            var iEndDateCol = 0;

            var datofini = aData[iStartDateCol].substring(0, 10);
            var datoffin = aData[iEndDateCol].substring(0, 10);

            if (iFini === "" && iFfin === "") {
                return true;
            } else if (iFini <= datofini && iFfin === "") {
                return true;
            } else if (iFfin >= datoffin && iFini === "") {
                return true;
            } else if (iFini <= datofini && iFfin >= datoffin) {
                return true;
            }
            return false;
        }
    );

    /* Initialize Branches Table DataTable */
    var table = $('#transactionsTable').DataTable({
      'sDom': '<"fixed-table-toolbar"fl><"excelButton hide"B>t<"bottom"ip><"clear">',
      'processing': true,
      'buttons': [ 'excel' ],
      'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
      'language': { 'emptyTable': emptyTableMessage },
    });

    if ($('#transactionsTable tbody tr.odd td').text() != emptyTableMessage) {
      $('#management-transactions-view').append(excelExportButton);
    }

    $('#dateStart, #dateEnd').datetimepicker({
        format: 'YYYY-MM-DD'
    });

    $('#dateStart, #dateEnd').on('dp.change', function(e) {
        table.draw();
        if ($(this).attr('id') == 'dateStart') {
            $("#dateEnd").data("DateTimePicker").minDate($("#dateStart").data("DateTimePicker").date().format('YYYY-MM-DD'));
        } else {
            $("#dateStart").data("DateTimePicker").maxDate($("#dateEnd").data("DateTimePicker").date().format('YYYY-MM-DD'));
        }
    });

    // bind excel export button
    $('.tab-pane').on('click', '.exportButton', function(event) {
        $(this).parent().parent().find('.dataTables_wrapper .excelButton a').click();
    });

  });
</script>

@endsection
