@extends('admin.app')

@section('view')
<div id="content" class="merchant-view">
  <!-- Nav tabs -->
  <div class="navTabsWrap">
    <ul class="nav nav-tabs" role="tablist" id="tabs">
      <ul class="nav nav-tabs" id="tabs">
          <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}">INFO</a></li>
          <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/branches">BRANCH</a></li>
          <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/employees">EMPLOYEE</a></li>
          <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/customers">CUSTOMER</a></li>
          <li class="active"><a>POINTS</a></li>
          <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/transactions">TRANSACTIONS</a></li>
          <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/billing">BILLING</a></li>
      </ul>
    </ul>
  </div>
  <!-- Tab panes -->
  <div class="tab-content">
    <div class="tab-pane active" id="employee">
      <div class="row">
        <div class="col-md-12">
        </div>
      </div>
      <div class="row" id="management-points-view">
        <div class="md-col-10">
          <table id="pointsTable" class="data-table">
              <thead>
              <tr>
                <th class="text-center">Date Registered</th>
                <th class="text-left">Customer Name</th>
                <th class="text-right">Mobile No.</th>
                <th class="text-right">Earned</th>
                <th class="text-right">Burned</th>
                <th class="text-right">Available</th>
              </tr>
              </thead>
              <tbody>
              @if(isset($customers) && $customers)
              @foreach($customers as $customer )
              <tr>
                <td class="text-center">
                  {{ \Carbon\Carbon::parse($customer->timestamp)->format('m-d-Y') }}
                </td>
                <td class="text-left">{{ $customer->fullName }}</td>
                <td class="text-right">{{ $customer->mobileNumber }}</td>
                <td class="text-right">{{ ($customer->points) ? $customer->points->totalPoints : '' }}</td>
                <td class="text-right">{{ ($customer->points) ? $customer->points->usedPoints : '' }}</td>
                <td class="text-right">{{ ($customer->points) ? $customer->points->currentpoints : '' }}</td>
              </tr>
              @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div><!--/content-->
@endsection

@section('footerPlaceholder')
<script type="text/javascript">
  $(document).ready( function() {
    var emptyTableMessage = 'No matching records found';
    var excelExportButton = '<div class="text-center"><button type="button" class="btn btn-default exportButton">Export to Excel</button></div>';

    /* Initialize Branches Table DataTable */
    $('#pointsTable').DataTable({
      'sDom': '<"fixed-table-toolbar"fl><"excelButton hide"B>t<"bottom"ip><"clear">',
      'buttons': [ 'excel' ],
      'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
      'language': { 'emptyTable': emptyTableMessage },
    });

    if ($('#pointsTable tbody tr.odd td').text() != emptyTableMessage) {
      $('#management-points-view').append(excelExportButton);
    }

    // bind excel export button
    $('.tab-pane').on('click', '.exportButton', function(event) {
        $(this).parent().parent().find('.dataTables_wrapper .excelButton a').click();
    });

  });
</script>
@endsection
