@extends('admin.app')

@section('view')
<div id="content" class="merchant-view">
  <!-- Nav tabs -->
  <div class="navTabsWrap">
    <ul class="nav nav-tabs" role="tablist" id="tabs">
      <ul class="nav nav-tabs" id="tabs">
          <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}">INFO</a></li>
          <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/branches">BRANCH</a></li>
          <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/employees">EMPLOYEE</a></li>
          <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/customers">CUSTOMER</a></li>
          <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/points">POINTS</a></li>
          <li class="active"><a>TRANSACTIONS</a></li>
          <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/billing">BILLING</a></li>
      </ul>
    </ul>
  </div>
  <!-- Tab panes -->
  <div class="tab-content">
    <div class="tab-pane active" id="transactions">
      <div class="row">
        <div class="col-md-6">
            <br /><br />
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class='input-group date datetimepicker' id='dateStart'>
                            <input type='text' class="form-control" name="dateStart" placeholder="Date Start" data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="" required>
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class='input-group date datetimepicker' id='dateEnd'>
                            <input type='text' class="form-control" name="dateEnd" placeholder="Date End" data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="" required>
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <div class="row" id="management-transactions-view" style="margin-top: none;">
        <div class="md-col-10">
          <table id="transactionsTable" class="data-table">
            {{-- START: if the merchant package is Loyalty --}}
            @if( $package_id == 1 )
                <thead>
                <tr>
                    <th>Date &amp; Time</th>
                    <th>Transaction Type</th>
                    <th>Customer Name</th>
                    <th>Transaction Amount</th>
                    <th>Points</th>
                    <th>Reward</th>
                    <th>Ref./OR No.</th>
                    <th>Branch</th>
                    <th>Emp#</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($transactions) && $transactions)
                    @foreach($transactions as $transaction )
                    <tr>
                      <td>{{ $transaction->timestamp }}</td>
                      <td>{{ $transaction->transactionType }}</td>
                      <td>{{ $transaction->customerName }}</td>
                      <td>P {{ $transaction->amountPaidWithCash }}</td>
                      <td>{{ $transaction->points }}</td>
                      <td>{{ $transaction->reward }}</td>
                      <td>{{ $transaction->receiptReferenceNumber }}</td>
                      <td>{{ $transaction->branchName }}</td>
                      <td>{{ $transaction->employeeNumber }}</td>
                    </tr>
                @endforeach
                @endif
                </tbody>
            {{-- END: if the merchant package is Loyalty --}}
            {{-- START: if the merchant package is Punchcard --}}
            @else
                <thead>
                <tr>
                    <th>Date &amp; Time</th>
                    <th>Promo Title</th>
                    <th>Transaction Type</th>
                    <th>Customer Name</th>
                    <th>Transaction Amount</th>
                    <th>Reward/Stamps</th>
                    <th>Branch</th>
                    <th>Emp#</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($transactions) && $transactions)
                    @foreach($transactions as $transaction)
                    <tr>
                        <td>{{ $transaction->timestamp }}</td>
                        <td>{{ $transaction->promoTitle }}</td>
                        <td>{{ $transaction->transactionType }}</td>
                        <td>{{ $transaction->customerName }}</td>
                        <td>{{ $transaction->amountPaidWithCash }}</td>
                        <td>{{ $transaction->stamps }}</td>
                        <td>{{ $transaction->branchName }}</td>
                        <td>{{ $transaction->employeeNumber }}</td>
                    </tr>
                    @endforeach
                @endif
                </tbody>
            @endif
          </table>
        </div>
      </div>
    </div>
  </div>
</div><!--/content-->
@endsection

@section('footerPlaceholder')
<script type="text/javascript">
  $(document).ready( function() {
    var emptyTableMessage = 'No matching records found';
    var excelExportButton = '<div class="text-center"><button type="button" class="btn btn-default exportButton">Export to Excel</button></div>';

    $.fn.dataTableExt.afnFiltering.push(
        function(oSettings, aData, iDataIndex) {
            var iFini = $('#dateStart input').val();
            var iFfin = $('#dateEnd input').val();
            var iStartDateCol = 0;
            var iEndDateCol = 0;

            var datofini = aData[iStartDateCol].substring(0, 10);
            var datoffin = aData[iEndDateCol].substring(0, 10);

            if (iFini === "" && iFfin === "") {
                return true;
            } else if (iFini <= datofini && iFfin === "") {
                return true;
            } else if (iFfin >= datoffin && iFini === "") {
                return true;
            } else if (iFini <= datofini && iFfin >= datoffin) {
                return true;
            }
            return false;
        }
    );

    /* Initialize Branches Table DataTable */
    var table = $('#transactionsTable').DataTable({
      'sDom': '<"fixed-table-toolbar"fl><"excelButton hide"B>t<"bottom"ip><"clear">',
      'processing': true,
      'buttons': [ 'excel' ],
      'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
      'language': { 'emptyTable': emptyTableMessage },
    });

    if ($('#transactionsTable tbody tr.odd td').text() != emptyTableMessage) {
      $('#management-transactions-view').append(excelExportButton);
    }

    $('#dateStart, #dateEnd').datetimepicker({
        format: 'YYYY-MM-DD'
    });

    $('#dateStart, #dateEnd').on('dp.change', function(e) {
        table.draw();
        if ($(this).attr('id') == 'dateStart') {
            $("#dateEnd").data("DateTimePicker").minDate($("#dateStart").data("DateTimePicker").date().format('YYYY-MM-DD'));
        } else {
            $("#dateStart").data("DateTimePicker").maxDate($("#dateEnd").data("DateTimePicker").date().format('YYYY-MM-DD'));
        }
    });

    // bind excel export button
    $('.tab-pane').on('click', '.exportButton', function(event) {
        $(this).parent().parent().find('.dataTables_wrapper .excelButton a').click();
    });

  });
</script>

@endsection
