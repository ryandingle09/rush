@extends('admin.app')

@section('view')
  <div id="content" class="merchant-view">
      <!-- Nav tabs -->
      <div class="navTabsWrap">
        <ul class="nav nav-tabs" id="tabs">
            <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}">INFO</a></li>
            <li class="active"><a>BRANCH</a></li>
            <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/employees">EMPLOYEE</a></li>
            <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/customers">CUSTOMER</a></li>
            <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/points">POINTS</a></li>
            <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/transactions">TRANSACTIONS</a></li>
            <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/billing">BILLING</a></li>
        </ul>
      </div>
      <!-- Tab panes -->
      <div class="tab-content">
          <div class="tab-pane active" id="branch">
            <div class="row">
              <div class="col-md-12">
              </div>
            </div>
            <div class="row" id="management-branches-view">
              <div class="md-col-10">
                <table id="branchTable" class="data-table">
                  <thead>
                  <tr>
                      <th class="text-left">Branch Name</th>
                      <th class="text-left">Location</th>
                      <th class="text-left">Business Type</th>
                      <th class="text-right">Status</th>
                  </tr>
                  </thead>
                  <tbody>
                  @if(isset($branches) && $branches)
                  @foreach($branches as $branch)
                    <tr>
                      <td class="text-left">{{ $branch->branchName }}</td>
                      <td class="text-left">{{ $branch->street_name }} {{ $branch->district }} {{ $branch->city }} {{ $branch->zipcode }}</td>
                      <td class="text-left">
                        @if ( $branch->merchant )
                          @if ( ( strpos($branch->merchant->businessType, '|') !== false ))
                            @foreach( $branch->merchant->business_type_name as $business_type)
                              {{ $business_type->name }}
                            @endforeach
                          @else
                            {{ $branch->merchant->businessType }}
                          @endif
                        @endif
                      </td>
                      <td class="text-right {{ $branch->is_deleted_text }} is_deleted_text">{{ $branch->is_deleted_text }}</td>
                    </tr>
                  @endforeach
                @endif
                  </tbody>
                </table>
              </div>
            </div>
          </div>
      </div>
  </div><!--/content-->
@endsection

@section('footerPlaceholder')
<script type="text/javascript">
  $(document).ready( function() {
    var emptyTableMessage = 'No matching records found';
    var excelExportButton = '<div class="text-center"><button type="button" class="btn btn-default exportButton">Export to Excel</button></div>';

    /* Initialize Branches Table DataTable */
    $('#branchTable').DataTable({
      'sDom': '<"fixed-table-toolbar"fl><"excelButton hide"B>t<"bottom"ip><"clear">',
      'buttons': [ 'excel' ],
      'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
      'language': { 'emptyTable': emptyTableMessage },
    });

    if ($('#branchTable tbody tr.odd td').text() != emptyTableMessage) {
      $('#management-branches-view').append(excelExportButton);
    }

    // bind excel export button
    $('.tab-pane').on('click', '.exportButton', function(event) {
        $(this).parent().parent().find('.dataTables_wrapper .excelButton a').click();
    });

  });
</script>
@endsection
