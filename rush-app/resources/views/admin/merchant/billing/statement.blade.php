<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="{{ url('page/billing/billing.css') }}">
	<title>CMS - RUSH</title>
</head>
<body class="statement-pdf">
	<div id="soa_wrap">
		<div id="soa_header">
			<div class="left">
				<img class="soa-logo" src="{{ url('page/assets/globe-logo-blue.png') }}" >
				<p>Globe Telecom, Inc.</p>
			</div>
			<div class="right">
				<p>STATEMENT OF ACCOUNT</p>
			</div>
		</div><br>
		<div class="container soa">
			<div class="row">
				<div class="left"><!-- ///////////////// Left column ///////////////// -->
					<div class="row">
						<div class="soa_acctNum">
							<img src="{{ url('../repository/merchant/barcode/' . $billing->barcode) }}" />
							<span>{{ $billing->account_number }}</span>
						</div>
					</div>
					<h3 class="soa_heading1">{{ $billing->authorized_rep }}</h3>
					<p id="soa_name">
						{{ $merchant->billingName or $billing->business_name }}
					</p>
					<p id="soa_address">
						{{ $merchant->street1 }}<br />
						{{ $merchant->street2 }}<br />
						{{ $merchant->address }}<br />
						<b>TIN #:</b> {{ $merchant->TIN }}
					</p>
					<h3 class="soa_heading1">Statement Summary</h3>
					{{--*/ $remaining_bal = 0 /*--}}
					@if ( !empty($previous_bill) )
					<table class="table statement_summary">
						<thead>
							<tr class="soa_heading2">
								<th style="text-align:left;">Previous Bill Charges</th>
								<th class="align_right">Amount</th>
							</tr>
						</thead>
						<tbody>
							<tr class="soa_item_list">
								<td class="soa_items">Amount Due from Previous Bill</td>
								<td class="soa_amounts align_right">{{ number_format( $previous_bill->total_amount, 2, ".", ",") }}</td>
							</tr><hr>
							<tr class="soa_item_list">
								<td class="soa_items">Less: Payments</td>
								<td class="soa_amounts align_right">({{ number_format( $previous_bill->payment_amount, 2, ".", ",") }})</td>
							</tr>
							<tr>
								<td colspan=2>
									<hr/>
								</td>
							</tr>
							<tr>
								<td class="soa_items">Remaining Balance from Previous Bill</td>
								{{--*/ $remaining_bal = $previous_bill->total_amount - $previous_bill->payment_amount /*--}}
								<td class="soa_amounts align_right">{{ number_format( $remaining_bal, 2, ".", ",") }}</td>
							</tr>
						</tbody>
					</table>
					@endif
					<!-- <table class="table curr_bill_charges">
						<thead>
							<tr class="soa_heading2">
								<th>Current Bill Charges</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr class="soa_item_list" >
								<td class="soa_items">Service Fee</td>
								<td class="soa_amounts align_right">{{ number_format( $billing->service_fee, 2, ".", ",") }}</td>
							</tr>
							@if ( $merchant->add_ons->count() > 0 )
							<tr class="soa_item_list" >
								<td class="soa_items">Add-Ons</td>
								<td class="soa_amounts align_right">{{ number_format( $billing->add_ons_fee, 2, ".", ",") }}</td>
							</tr>
							@endif
							<tr class="soa_item_list" >
								<td class="soa_items">VAT</td>
								<td class="soa_amounts align_right">{{ number_format( $billing->vat_fee, 2, ".", ",") }}</td>
							</tr>
							<tr>
								<td colspan=2 style="padding:0px!important;">
									<hr/>
								</td>
							</tr>
							<tr>
								<td class="soa_items">Total Current Bill</td>
								<td class="soa_amounts align_right">{{ number_format( $billing->total_current, 2, ".", ",") }}</td>
							</tr>
							{{--*/ $total_amount = $billing->total_amount /*--}}
							@if ( $merchant->status == 'Trial' )
							<tr>
								<td class="soa_items">Less: Payments(Trial)</td>
								<td class="soa_amounts align_right">- {{ number_format( $billing->total_current, 2, ".", ",") }}</td>
							</tr>
							{{--*/ $total_amount = $billing->total_amount - $billing->total_current /*--}}
							@endif
							<tr class="soa_totalAmtDue orange_box">
								<td>Total Amount Due</td>
								<td class="align_right">{{ number_format( $total_amount, 2, ".", ",") }}</td>
							</tr>
						</tbody>
					</table> -->
				</div>
				<div class="right">
					<!-- ///////////////// Right column ///////////////// -->
					<div class="col-lg-12">
						<div>
							<div class="account_summary">
								<table class="table">
									<thead>
										<tr>
											<th class="soa_heading1" colspan="2"><span>Account Summary</span><br><br></th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="soa_items">Account Number</td>
											<td class="align_right soa_amounts">{{ $billing->account_number }}</td>
										</tr>
										<tr>
											<td class="soa_items">RUSH Solution</td>
											<td class="align_right soa_amounts">{{ $billing->package->name }}({{ $billing->status }})</td>
										</tr>
										<tr>
											<td class="soa_items">Billing Number</td>
											<td class="align_right soa_amounts">{{ $billing->merchant_id .'-'. $billing->billing_number }}</td>
										</tr>
										<tr>
											<td class="soa_items">Billing Period</td>
											<td class="align_right soa_amounts">{{ Carbon\Carbon::parse($billing->billing_period_start)->format('M j') }} - {{ Carbon\Carbon::parse($billing->billing_period_end)->format('M j, Y') }}</td>
										</tr>
										<tr>
											<td class="soa_items">Statement Date</td>
											<td class="align_right soa_amounts">{{ Carbon\Carbon::parse($billing->statement_date)->format('M j, Y') }}</td>
										</tr>
										<tr class="soa_total_label orange_box">
											<td>Due Date</td>
											<td  class="align_right" style="font-family:Open Sans Semibold;">{{ Carbon\Carbon::parse($billing->due_date)->format('M j, Y') }}</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div>
								<p class="soa_note"><em>
									Please examine your Statement of Account immediately. If no discrepancy is reported within 30 days from this bill's cut-off date, the contents of this statement will be considered correct. Thank you.
								</em>
							</p>
						</div>
					</div>
				</div>
			</div>
			<!-- ///////////////// Bottom Row ///////////////// -->
			<div class="col-lg-12 usage_breakdown"><hr/>
				<h3 class="soa_heading1">Usage Breakdown</h3>
				<table class="table">
					<thead>
						<tr class="soa_heading2">
							<th align="left" style="width:25%;">Description</th>
							<th align="center">Quantity</th>
							<th align="right">Amount</th>
							<th align="right">Total</th>
						</tr>
					</thead>
					<tbody>
						{{--*/ $service_breakdown = json_decode($billing->service_breakdown) /*--}}
						<tr>
							<td class="soa_items">Monthly Service Fee @if ( $merchant->status == 'Trial' ) *Trial @endif</td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						@if ( isset($service_breakdown->service_fee) )
						<tr>
							<td class="soa_items" align="left" style="padding-left:40px!important;">Service Fee</td>
							<td class="soa_items" align="center" >{{ $service_breakdown->service_quantity }}</td>
							<td class="soa_items" align="right" >{{ number_format( $service_breakdown->service_fee, 2, ".", ",") }}</td>
							@if ( $billing->package_id == 1 )
							@if ( isset($service_breakdown->msf_type) && $service_breakdown->msf_type == 'fixed' )
							<td class="soa_items" align="right">{{ number_format( ($service_breakdown->service_fee * $service_breakdown->service_quantity ), 2, ".", ",") }}</td>
							@else
							<td class="soa_items" align="right">{{ number_format( ($service_breakdown->service_fee * ( $service_breakdown->service_quantity / 100 ) ), 2, ".", ",") }}</td>
							@endif
							@else
							<td class="soa_items" align="right">{{ number_format( ($service_breakdown->service_fee * $service_breakdown->service_quantity ), 2, ".", ",") }}</td>
							@endif
						</tr>
						@endif
						@if ( isset($service_breakdown->customer_count) && isset($service_breakdown->customer_reg_fee) && $service_breakdown->customer_count > 0 )
						<tr>
							<td class="soa_items" align="left" style="padding-left:40px!important;">Customer Registration Fee</td>
							<td class="soa_items" align="center">{{ $service_breakdown->customer_count }}</td>
							<td class="soa_items" align="right" >{{ number_format( 20, 2, ".", ",") }}</td>
							<td class="soa_items" align="right" >{{ number_format( $service_breakdown->customer_reg_fee, 2, ".", ",") }}</td>
						</tr>
						@endif
						@if ( isset($service_breakdown->additional_sms_transaction) && isset($service_breakdown->additional_sms_transaction_fee) && $service_breakdown->additional_sms_transaction > 0 )
						<tr>
							<td class="soa_items" style="padding-left:40px!important;">SMS Transactions</td>
							<td class="soa_items" align="center">{{ $service_breakdown->additional_sms_transaction }}</td>
							<td class="soa_items" align="right">{{ number_format( $service_breakdown->additional_sms_transaction_charge, 2, ".", ",") }}</td>
							<td class="soa_items" align="right">{{ number_format( $service_breakdown->additional_sms_transaction_fee, 2, ".", ",") }}</td>
						</tr>
						@endif
						@if ( isset($service_breakdown->merchant_sms_broadcast) && isset($service_breakdown->merchant_sms_broadcast_fee) && $service_breakdown->merchant_sms_broadcast > 0 )
						<tr>
							<td class="soa_items" style="padding-left:40px!important;">SMS Broadcasts</td>
							<td class="soa_items" align="center">{{ $service_breakdown->merchant_sms_broadcast }}</td>
							<td class="soa_items" align="right">{{ number_format( $service_breakdown->merchant_sms_broadcast_charge, 2, ".", ",") }}</td>
							<td class="soa_items" align="right">{{ number_format( $service_breakdown->merchant_sms_broadcast_fee, 2, ".", ",") }}</td>
						</tr>
						@endif
						@if ( isset($service_breakdown->additional_admin_rewards_transactions) && isset($service_breakdown->additional_admin_rewards_transactions) && $service_breakdown->additional_admin_rewards_transactions > 0 )
						<tr>
							<td class="soa_items" style="padding-left:40px!important;">Globe SKUs Redemption</td>
							<td class="soa_items" align="center"></td>
							<td class="soa_items" align="right">{{ number_format( $service_breakdown->additional_admin_rewards_transactions, 2, ".", ",") }}</td>
							<td class="soa_items" align="right">{{ number_format( $service_breakdown->additional_admin_rewards_transactions, 2, ".", ",") }}</td>
						</tr>
						@endif
						@if ( isset($service_breakdown->cms_tools_fee) )
						<tr>
							<td class="soa_items" align="left" style="padding-left:40px!important;">SMS Tool Fee</td>
							<td class="soa_items" align="center">1</td>
							<td class="soa_items" align="right">{{ number_format( $service_breakdown->cms_tools_fee, 2, ".", ",") }}</td>
							<td class="soa_items" align="right" >{{ number_format( $service_breakdown->cms_tools_fee, 2, ".", ",") }}</td>
						</tr>
						@endif
						<tr>
							<td class="soa_items" align="left" colspan="4">Add-Ons</td>
						</tr>
						
						@if ( $merchant->add_ons->count() > 0 || $billing->add_ons_fee > 0 )
						@foreach ($merchant->add_ons as $add_ons )
						
						@if ( $add_ons->id == 1 && $additional_sms_credits['qty'] > 0 )
						<tr>
							<td class="soa_items" align="left" style="padding-left:40px!important;">{{ $add_ons->name }}</td>
							<td class="soa_items" align="center">{{ $additional_sms_credits['qty'] }}</td>
							<td class="soa_items" align="right">0.45</td>
							<td class="soa_items" align="right">{{ number_format( $additional_sms_credits['fee'], 2, ".", "," ) }}</td>
						</tr>
						@endif
						@if ( $add_ons->id == 2 && $app_zero_rating['qty'] > 0 )
						<tr>
							<td class="soa_items" style="padding-left:40px!important;">{{ $add_ons->name }}</td>
							<td class="soa_items" align="center">{{ $app_zero_rating['qty'] }}</td>
							<td class="soa_items" align="right">1.79</td>
							<td class="soa_items" align="right">{{ number_format( $app_zero_rating['fee'], 2, ".", "," ) }}</td>
						</tr>
						@endif
						@if ( $add_ons->id == 3 && $white_labeled_card['qty'] > 0 )
						<tr>
							<td class="soa_items" style="padding-left:40px!important;">{{ $add_ons->name }}</td>
							<td class="soa_items" align="center">{{ $white_labeled_card['qty'] }}</td>
							<td class="soa_items" align="right">50.00</td>
							<td class="soa_items" align="right">{{ number_format( $white_labeled_card['fee'], 2, ".", "," ) }}</td>
						</tr>
						@endif
						@endforeach
						@endif
						<tr>
							<td class="soa_items" align="left">VAT</td>
							<td></td>
							<td></td>
							<td class="soa_items" align="right">{{ number_format( $billing->vat_fee, 2, ".", ",") }}</td>
						</tr>
						<tr>
							<td class="soa_items" align="left">Total Current Bill</td>
							<td></td>
							<td></td>
							<td class="soa_items" align="right">{{ number_format( $billing->total_current, 2, ".", ",") }}</td>
						</tr>
						<tr>
							<td class="soa_items" align="left">Total Previous Bill</td>
							<td></td>
							<td></td>
							<td class="soa_items" align="right">{{ number_format( $remaining_bal, 2, ".", ",") }}</td>
						</tr>
						<tr class="soa_total_label breakdown_total">
							<td style="font-weight:bold;" align="left">Total Amount Due</td>
							<td></td>
							<td></td>
							<td class="soa-items" align="right">{{ number_format( $billing->total_amount, 2, ".", ",") }}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div id="soa_footer">
		<div class="left">
			<p><span class="first-line">VAT REG TIN 000-768-480-000</span><br />
				32nd Street Corner 7th Avenue, Bonifacio Global City, Taguig, Philippines<br />
				P.O. Box 284 CPO Manila, Liwasang Bonifacio, 1099 Manila, Philippines<br />
			</div>
			<div class="right">
				<p>
					<span class="first-line">&nbsp;</span>
					Tel: +632 7302939<br />
					Fax: +632 7304061
				</p>
			</div>
		</div>
	</div>
</body>
</html>
