@extends('admin.app')

@section('view')
<div class="container">
		<div id="content" class="view-billing">
    <div class="row">
      <div class="col-lg-12">
        <br />
        <p>An error has been detected. Please try again.</p>
        <p>You will be redirected in a while (or <a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/billing">click here</a>)</p>
      </div>
    </div>
	  </div>
</div> <!-- /container -->
@endsection

@section('footerPlaceholder')
@parent
<script type="text/javascript">
$(document).ready( function() {
  setTimeout(function() {
    window.location.href = "{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/billing";
  }, 3000);
});
</script>
@endsection
