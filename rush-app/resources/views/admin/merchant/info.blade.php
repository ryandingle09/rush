@extends('admin.app')

@section('view')
<div id="content" class="merchant-view">
  <!-- Nav tabs -->
  <div class="navTabsWrap">
      <ul class="nav nav-tabs" id="tabs">
          <li class="active"><a>INFO</a></li>
          <li><a href="{{ url( 'admin/merchant/' . $merchant_id . '/branches') }}">BRANCH</a></li>
          <li><a href="{{ url( 'admin/merchant/' . $merchant_id . '/employees') }}">EMPLOYEE</a></li>
          <li><a href="{{ url( 'admin/merchant/' . $merchant_id . '/customers') }}">CUSTOMER</a></li>
          <li><a href="{{ url( 'admin/merchant/' . $merchant_id . '/points') }}">POINTS</a></li>
          <li><a href="{{ url( 'admin/merchant/' . $merchant_id . '/transactions') }}">TRANSACTIONS</a></li>
          <li><a href="{{ url( 'admin/merchant/' . $merchant_id . '/billing') }}">BILLING</a></li>
      </ul>
  </div>
  <!-- Tab panes -->
  <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="info">
      <div class="row">
          <div class="col-md-12"></div>
      </div>
      <form action="{{ URL::to('/').'/admin/merchant/'.$merchant->merchantid}}" method="POST">
        {{ csrf_field() }}
      <div class="row">
          <div class="col-md-12">
              <div class="row">
                  <div class="col-md-5 form-group">
                    <label for="status">Status</label>
                    <div class="radio">
                      <label><input type="radio" name="status" id="status_trial" value="Trial" {{ ( $merchant->status == "Trial" ) ? 'checked': '' }}>Trial</label>
                    </div>
                    <div class="radio">
                      <label><input type="radio" name="status" id="status_full"  value="Full" {{ ( $merchant->status == "Full" ) ? 'checked': '' }}>Full</label>
                    </div>
                  </div>
                  <div class="col-md-5 form-group trial_date_area">
                    <label for="trial_date">Trial Date End</label>
                    <input type="text" class="form-control" name="trial_date_end" id="trial_date_end" value="{{ ( isset($trial_end_date) ) ? Carbon\Carbon::createFromFormat('d/m/Y', $trial_end_date)->format('m/d/Y') : '' }}" required>
                    @if ($errors->has('trial_date_end'))
                      <div class="error">{{ $errors->first('trial_date_end', ':message') }}</div>
                    @endif
                    <div class="checkbox">
                      <label><input type="checkbox" name="auto_update_status" id="auto_update_status" value="TRUE" {{ ( $merchant->auto_update_status == 1 ) ? 'checked': '' }}>Convert to Full on Trial End</label>
                    </div>
                  </div>
              </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-5 form-group">
                  <label for="businessName">Merchant Name</label>
                  <input type="text" class="form-control" name="businessName" id="businessName" value="{{ $merchant->businessName }}" required>
                  @if ($errors->has('businessName'))
                    <div class="error">{{ $errors->first('businessName', ':message') }}</div>
                  @endif
              </div>
              <div class="col-md-5 form-group">
                  <label for="contactPerson">Contact Person</label>
                  <input type="text" class="form-control" name="contactPerson" id="contactPerson" value="{{ $merchant->contactPerson }}" required>
                  @if ($errors->has('contactPerson'))
                    <div class="error">{{ $errors->first('contactPerson', ':message') }}</div>
                  @endif
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-5 form-group">
                  <label for="contactPersonNumber">Contact Person Number</label>
                  <input type="text" class="form-control" name="contactPersonNumber" id="contactPersonNumber" value="{{ $merchant->contactPersonNumber }}" required>
                  @if ($errors->has('contactPersonNumber'))
                    <div class="error">{{ $errors->first('contactPersonNumber', ':message') }}</div>
                  @endif
              </div>
              <div class="col-md-5 form-group">
                  <label for="contactPersonEmail">Contact Person Email</label>
                  <input type="email" class="form-control" name="contactPersonEmail" id="contactPersonEmail" value="{{ $merchant->contactPersonEmail }}" required>
                  @if ($errors->has('contactPersonEmail'))
                    <div class="error">{{ $errors->first('contactPersonEmail', ':message') }}</div>
                  @endif
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-5 form-group">
                  <label for="businessType">Business Type</label>
                  @if ( $business_type_selected )
                    {!! Form::select('business_type[]', $business_type, $business_type_selected, ['id' => 'industry', 'multiple' => 'multiple', 'class' => 'form-control chosen-select', 'data-placeholder' => ' ' ] ) !!}
                  @else
                    {!! Form::select('business_type[]', $business_type, 'Others', ['id' => 'industry', 'multiple' => 'multiple', 'class' => 'form-control chosen-select', 'data-placeholder' => ' ' ] ) !!}
                  @endif
                  @if ($errors->has('business_type[]'))
                    <div class="error">{{ $errors->first('business_type', ':message') }}</div>
                  @endif
              </div>
              <div id="others" class="col-md-5 form-group">
                    <label>Others<span class="required">&nbsp;*</span></label>
                    <input id="others_ID" type="text" class="form-control" name="business_type_other" value="{{ $merchant->businessType }}">
                    @if ($errors->has('business_type_other'))
                      <div class="error">{{ $errors->first('business_type_other', ':message') }}</div>
                    @endif
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-5 form-group">
                  <label for="package_availed">Package Availed</label>
                  {{ Form::select('package_availed', $packages, $merchant->packageId, ['class' => 'form-control']) }}
                  @if ($errors->has('package_availed'))
                    <div class="error">{{ $errors->first('package_availed', ':message') }}</div>
                  @endif
              </div>
              <div class="col-md-5 form-group">
                  <label for="enabled">Enabled/Disabled</label>
                  {{ Form::select('enabled', ['0'=> 'Disabled','1' => 'Enabled'], $merchant->enabled, ['class' => 'form-control', 'id'=>'enabled']) }}
                  @if ($errors->has('enabled'))
                    <div class="error">{{ $errors->first('enabled', ':message') }}</div>
                  @endif
              </div>
            </div>
          </div>
          <div class="col-md-12 reason-section">
            <div class="row">
              <div class="col-md-10 form-group">
                  <label for="package_availed">Reason</label>
                  {{ Form::textarea('reason', '', ['class' => 'form-control', 'id'=>'reason']) }}
                  @if ($errors->has('reason'))
                    <div class="error">{{ $errors->first('reason', ':message') }}</div>
                  @endif
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-5 form-group">
                  {{--*/
                    
                    $fee = 0;
                    if ( $merchant->PackageId == 1 ) $fee = "1.5";
                    else $fee = "1299.00";
                    $fee = ( $merchant->monthly_service_fee ) ? $merchant->monthly_service_fee : $fee;

                  /*--}}
                  <label for="monthly_service_fee">Monthly Service Fee</label>
                  <input type="text" class="form-control" name="monthly_service_fee" id="monthly_service_fee" value="{{ $fee }}">
                  @if ($errors->has('monthly_service_fee'))
                    <div class="error">{{ $errors->first('monthly_service_fee', ':message') }}</div>
                  @endif
              </div>
              <div class="col-md-5 form-group">
                  <label for="msf_type">MSF Type</label>
                  {{--*/
                    
                    $msf_options = [ 'fixed' => 'Fixed'];
                    if ( $merchant->packageId == 1 ) $msf_options = [ 'fixed' => 'Fixed', 'percentage' => 'Percentage'];

                  /*--}}
                  {{ Form::select('msf_type', $msf_options, $merchant->msf_type, ['class' => 'form-control', 'id'=>'msf_type']) }}
                  @if ($errors->has('msf_type'))
                    <div class="error">{{ $errors->first('msf_type', ':message') }}</div>
                  @endif
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-5 form-group">
                  <label for="sms_transactions_charge">SMS Transaction Fee</label>
                  {{--*/
                    $fee = ( $merchant->sms_transactions_charge ) ? $merchant->sms_transactions_charge : 0.50;
                  /*--}}
                  <input type="text" class="form-control" name="sms_transactions_charge" id="sms_transactions_charge" value="{{ $fee }}">
                  @if ($errors->has('sms_transactions_charge'))
                    <div class="error">{{ $errors->first('sms_transactions_charge', ':message') }}</div>
                  @endif
                  <span class="text-right text-italic" style="font-size:13px">
                      <em>If this is blank, the system will use the default 0.50 PHP for both package.</em>
                  </span>
              </div>
              <div class="col-md-5 form-group">
                  <label for="branch_count">Branch Count</label>
                  <input type="number" class="form-control" name="branch_count" id="branch_count" value="{{ $merchant->branch_count }}">
                  @if ($errors->has('branch_count') )
                    <div class="error">{{ $errors->first('branch_count', ':message') }}</div>
                  @endif
              </div>
          </div>
        </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-5 form-group">
                  <label for="street1">Street 1</label>
                  <input type="text" class="form-control" name="street1" id="street1" value="{{ $merchant->street1 }}">
                  @if ($errors->has('street1') )
                    <div class="error">{{ $errors->first('street1', ':message') }}</div>
                  @endif
              </div>
              <div class="col-md-5 form-group">
                  <label for="street2">Street 2</label>
                  <input type="text" class="form-control" name="street2" id="street2" value="{{ $merchant->street2 }}">
                  @if ($errors->has('street2'))
                    <div class="error">{{ $errors->first('street2', ':message') }}</div>
                  @endif
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-5 form-group">
                  <label for="address">Location</label>
                  {!! Form::select('address', $address,$merchant->address,  ['id' => 'address', 'class' => 'form-control', 'requred' ] ) !!}
                  @if ($errors->has('address'))
                    <div class="error">{{ $errors->first('address', ':message') }}</div>
                  @endif
              </div><div class="col-md-5 form-group">
                  <label for="vanity_url">Vanity URL</label>
                  <input type="text" class="form-control" name="vanity_url" id="vanity_url" value="{{ $merchant->settings->vanity_url or '' }}">
                  @if ($errors->has('vanity_url'))
                    <div class="error">{{ $errors->first('vanity_url', ':message') }}</div>
                  @endif
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-5 form-group">
                  <label for="tin_number">TIN Number</label>
                  <input type="text" class="form-control" name="tin_number" id="tin_number" value="{{ $merchant->TIN }}">
                  @if ($errors->has('tin_number'))
                    <div class="error">{{ $errors->first('tin_number', ':message') }}</div>
                  @endif
              </div>
              <div class="col-md-5 form-group">
                  <label for="billing_name">Billing Name</label>
                  <input type="text" class="form-control" name="billing_name" id="billing_name" value="{{ $merchant->billingName }}">
                  @if ($errors->has('billing_name'))
                    <div class="error">{{ $errors->first('billing_name', ':message') }}</div>
                  @endif
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-5 form-group">
                  <label for="reseller">Reseller</label>
                  {!! Form::select('reseller', $resellers, $merchant->reseller_id, ['id' => 'reseller', 'class' => 'form-control', 'placeholder' => 'Select from list...' ] ) !!}
                  @if ($errors->has('reseller'))
                    <div class="error">{{ $errors->first('reseller', ':message') }}</div>
                  @endif
              </div>
              <div class="col-md-5 form-group">
                  <label for="reseller_email">Reseller Email</label>
                  <input type="email" class="form-control" name="reseller_email" id="reseller_email" value="{{ $merchant->reseller_email }}">
                  @if ($errors->has('reseller_email'))
                    <div class="error">{{ $errors->first('reseller_email', ':message') }}</div>
                  @endif
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-5 form-group">
                  <label for="bcu_enabled">Business Center Users</label>
                  {{ Form::select('bcu_enabled', ['0'=> 'Disabled','1' => 'Enabled'], $merchant->bcu_enabled, ['class' => 'form-control', 'id'=>'bcu_enabled']) }}
                  @if ($errors->has('bcu_enabled'))
                    <div class="error">{{ $errors->first('bcu_enabled', ':message') }}</div>
                  @endif
              </div>
              <div class="col-md-5 form-group">
                  <label for="new_billing_calculation">Billing Calculation</label>
                  {{ Form::select('new_billing_calculation', ['0'=> 'Old','1' => 'New'], $merchant->new_billing_calculation, ['class' => 'form-control', 'id'=>'new_billing_calculation']) }}
                  @if ($errors->has('new_billing_calculation'))
                    <div class="error">{{ $errors->first('new_billing_calculation', ':message') }}</div>
                  @endif
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-5 form-group">
                  <label for="allow_registration">Allow Registration</label>
                  {{ Form::select('allow_registration', ['0'=> 'Disabled','1' => 'Enabled'], $merchant_allow_registration, ['class' => 'form-control', 'id'=>'allow_registration']) }}
                  @if ($errors->has('allow_registration'))
                    <div class="error">{{ $errors->first('allow_registration', ':message') }}</div>
                  @endif
              </div>
              <div class="col-md-5 form-group">
                  <label for="voucher_module">Voucher Module</label>
                  {{ Form::select('voucher_module', ['0'=> 'Disabled','1' => 'Enabled'], $merchant_voucher_module, ['class' => 'form-control', 'id'=>'voucher_module']) }}
                  @if ($errors->has('voucher_module'))
                    <div class="error">{{ $errors->first('voucher_module', ':message') }}</div>
                  @endif
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-5 form-group">
                  <label for="code_management">Code Management</label>
                  {{ Form::select('code_management', ['0'=> 'Disabled','1' => 'Enabled'], $merchant_code_management, ['class' => 'form-control', 'id'=>'code_management']) }}
                  @if ($errors->has('code_management'))
                    <div class="error">{{ $errors->first('code_management', ':message') }}</div>
                  @endif
              </div>
              <div class="col-md-5 form-group">
                  <label for="enable_feedback_category">Feedback Category</label>
                  {{ Form::select('enable_feedback_category', ['0'=> 'Disabled','1' => 'Enabled'], $merchant_enable_feedback_category, ['class' => 'form-control', 'id'=>'enable_feedback_category']) }}
                  @if ($errors->has('enable_feedback_category'))
                    <div class="error">{{ $errors->first('enable_feedback_category', ':message') }}</div>
                  @endif
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-5 form-group">
                  <label for="add_member">Customer Upload</label>
                  {{ Form::select('add_member', ['0'=> 'Disabled','1' => 'Enabled'], $merchant_add_member, ['class' => 'form-control', 'id'=>'add_member']) }}
                  @if ($errors->has('add_member'))
                    <div class="error">{{ $errors->first('add_member', ':message') }}</div>
                  @endif
              </div>
              <div class="col-md-5 form-group">
                  <label for="customer_login_param">Customer Login</label>
                  {{ Form::select('customer_login_param', ['mobile'=> 'Mobile','email' => 'Email'], $merchant_customer_login_param, ['class' => 'form-control', 'id'=>'customer_login_param']) }}
                  @if ($errors->has('customer_login_param'))
                    <div class="error">{{ $errors->first('customer_login_param', ':message') }}</div>
                  @endif
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-5 form-group">
                  <label for="maintenance">Merchant Maintenance</label>
                  {{ Form::select('maintenance', ['0'=> 'Disabled','1' => 'Enabled'], $merchant_maintenance, ['class' => 'form-control', 'id'=>'maintenance']) }}
                  @if ($errors->has('maintenance'))
                    <div class="error">{{ $errors->first('maintenance', ':message') }}</div>
                  @endif
              </div>
              <div class="col-md-5 form-group">
                  <label for="maintenance_text">Maintenance Message</label>
                  <textarea class="form-control" name="maintenance_text" id="maintenance_text" style="resize: none;" maxlength="60" cols="20" rows="3">{{ $merchant_maintenance_text }}</textarea>
                  <span class="text-right text-italic" style="font-size:13px">
                      <em>60 characters maximum.</em>
                  </span>
                  @if ($errors->has('maintenance_text'))
                    <div class="error">{{ $errors->first('maintenance_text', ':message') }}</div>
                  @endif
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-5 form-group">
                  <label for="toggle_branch_feature">Toggle Branch Feature</label>
                  {{ Form::select('toggle_branch_feature', ['0'=> 'Hide','1' => 'Show'], $merchant_toggle_branch_feature, ['class' => 'form-control', 'id'=>'toggle_branch_feature']) }}
                  @if ($errors->has('toggle_branch_feature'))
                    <div class="error">{{ $errors->first('toggle_branch_feature', ':message') }}</div>
                  @endif
              </div>
              <div class="col-md-5 form-group">
                  <label for="accrued_billing">Accrued Billing</label>
                  {{ Form::select('accrued_billing', [true=> 'True',false =>'False'], $merchant_accrued_billing, ['class' => 'form-control', 'id'=>'accrued_billing']) }}
                  @if ($errors->has('accrued_billing'))
                    <div class="error">{{ $errors->first('accrued_billing', ':message') }}</div>
                  @endif
              </div>
            </div>
          </div>
          <div class="col-md-12" id="tier-area">
            <div class="row">
              <div class="col-md-12 form-group">
                  <label>New Billing Tiers</label>
              </div>
              <div class="col-md-12 form-group">
                  <div class="col-md-12">
                    <div class="col-md-3 form-group"><label>Member count</label></div>
                    <div class="col-md-3 form-group"><label>Monthly Service Fee</label></div>
                    <div class="col-md-3 form-group"><label>SMS Fee</label></div>
                    <div class="col-md-3 form-group"><label>Label</label></div>
                  </div>
                @foreach( $billing_tiers as $billing_tier )
                  <div class="col-md-12">
                    <div class="col-md-3 form-group">
                      <input type="text" class="form-control" disabled value="{{ $billing_tier['member_count'] }}"/>
                    </div>
                    <div class="col-md-3 form-group">
                      <input type="number" class="form-control" placeholder="{{ $billing_tier['msf'] }}" name="msf_price_{{ $billing_tier['name'] }}" value ="{{ isset( object_get( $merchant_billing_tiers, $billing_tier['name'] )->msf ) ? object_get( $merchant_billing_tiers, $billing_tier['name'] )->msf : '' }}" /> 
                    </div>
                    <div class="col-md-3 form-group">
                      <input type="number" class="form-control" placeholder="{{ $billing_tier['sms'] }}" name="sms_price_{{ $billing_tier['name'] }}" value="{{ isset( object_get( $merchant_billing_tiers, $billing_tier['name'] )->sms ) ? object_get( $merchant_billing_tiers, $billing_tier['name'] )->sms : '' }}" />
                    </div>
                    <div class="col-md-3 form-group">
                      <input type="string" class="form-control" placeholder="{{ $billing_tier['label'] }}" name="label_price_{{ $billing_tier['name'] }}" value="{{ isset( object_get( $merchant_billing_tiers, $billing_tier['name'] )->label ) ? object_get( $merchant_billing_tiers, $billing_tier['name'] )->label : '' }}" />
                    </div>
                  </div>
                @endforeach
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-12 form-group">
                  <label>Billing Add-ons</label>
              </div>
              <div class="col-md-12 form-group">
                  <button id="add-add-ons" onclick="return false;" style="padding:8px; border-radius: 5px; border: none;"><i class="fa fa-plus" aria-hidden="true"></i></button>
              </div>
              <div class="add-ons-area col-md-12 form-group">
              @if ( $merchant->billing_add_ons )
              @foreach( json_decode( $merchant->billing_add_ons) as $add_on )
                <div class="col-md-12">
                  <div class="col-md-4 form-group">
                    <input type="text" class="form-control" placeholder="Description" name="add-ons-desc[]" required value="{{ $add_on->description }}" />
                  </div>
                  <div class="col-md-2 form-group">
                    <input type="text" class="form-control" placeholder="Quantity" name="add-ons-quantity[]" required value="{{ $add_on->quantity }}"/>
                  </div>
                  <div class="col-md-2 form-group">
                    <input type="text" class="form-control" placeholder="Amount" name="add-ons-amount[]" required value="{{ $add_on->amount }}"/>
                  </div>
                  <div class="col-md-2 form-group">
                    {!! Form::select('add-ons-vat[]', [ 0 => 'without VAT', 1 => 'with VAT' ], $add_on->vat, [ 'class' => 'form-control' ] ) !!}
                  </div>
                  <div class="col-md-2 form-group">
                    <button class="remove-add-ons" onclick="return false;" style="padding:8px; border-radius: 5px; border: none;"><i class="fa fa-minus" aria-hidden="true"></i></button>
                  </div>
                </div>
                @endforeach
                @endif
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-5 form-group">
                <label>CMS Modules</label>
              </div>
            </div>
            <div class="row modules-options">
                @if($cms_modules)
                  @foreach($cms_modules as $module)
                  <div class="col-md-3 form-group">
                    {{ Form::checkbox( 'cms_modules[]', $module->id, ( in_array( $module->id, $merchant_cms_modules) ? 1 : 0 ), ['id' => 'modules_' . $module->id]) }}
                    <label class="modules-label" for="modules_{{$module->id}}">{{ $module->name }}</label>
                  </div>
                  @endforeach
                @endif
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">&nbsp;</div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-5 form-group">
                <label>Broadcast Tools</label>
              </div>
            </div>
            <div class="row modules-options">
                @if($broadcast_tools_modules)
                  @foreach($broadcast_tools_modules as $module)
                  <div class="col-md-3 form-group">
                    {{ Form::checkbox( 'broadcast_tools_modules[]', $module->id, ( in_array( $module->id, $merchant_broadcast_tools) ? 1 : 0 ), ['id' => 'modules_' . $module->id]) }}
                    <label class="modules-label" for="modules_{{$module->id}}">{{ $module->name }}</label>
                  </div>
                  @endforeach
                @endif
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">&nbsp;</div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-5 form-group">
                <label>ClassManagement Tools</label>
              </div>
            </div>
            <div class="row modules-options">
                @if($class_management_tools_modules)
                  @foreach($class_management_tools_modules as $module)
                  <div class="col-md-3 form-group">
                    {{ Form::checkbox( 'class_management_tools_modules[]', $module->id, ( in_array( $module->id, $merchant_class_management_tools) ? 1 : 0 ), ['id' => 'modules_' . $module->id]) }}
                    <label class="modules-label" for="modules_{{$module->id}}">{{ $module->name }}</label>
                  </div>
                  @endforeach
                @endif
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">&nbsp;</div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-5 form-group">
                <label id="enabled-disabled-toggle">Enabled/Disabled Logs</label>
              </div>
            </div>
          </div>
          <div class="col-md-12 merchant-enabled-logs">
            <div class="row">
              <div class="col-md-12">
                <table id="merchant-enabled-logs-table" class="data-table">
                  <thead>
                    <tr>
                      <th class="text-center">Enabled/Disabled</th>
                      <th class="text-left">Reason</th>
                      <th class="text-center">Logged by</th>
                      <th class="text-right">Date</th>
                    </tr>
                  </thead>
                  <tbody>
                    @if ( isset($enabled_logs) && $enabled_logs )
                      @foreach( $enabled_logs as $enabled_log )
                        <tr>
                          <td class="text-center">{{ $enabled_log->enabledText }}</td>
                          <td class="text-left">{!! nl2br(e($enabled_log->reason)) !!}</td>
                          <td class="text-center">{{ $enabled_log->admin->firstname }} {{ $enabled_log->admin->lastname }}</td>
                          <td class="text-right">{{ Carbon\Carbon::parse( $enabled_log->created_at )->format('m/d/Y') }}</td>
                        </tr>
                      @endforeach
                    @endif
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">&nbsp;</div>
          </div>
          <div class="col-md-12 align-right">
            <div class="row merchant-view-buttons">
              <div class="col-md-1 form-group">
                {{ Form::submit('Update', ['class'=>'btn btn-primary']) }}
              </div>
              <div class="col-md-1 form-group">
                {{ link_to( URL::to('/') . '/admin', "Back", $attributes = array('class'=>'btn btn-warning'), $secure = null) }}
              </div>
            </div>
          </div>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('footerPlaceholder')
<script type="text/javascript">
  $(document).ready( function() { /* Document Ready Open */

    $("#industry").chosen();
    $(".reason-section").hide();

    $('#merchant-enabled-logs-table').dataTable({
      paging: true,
      ordering: false,
      searching: false,
      info: false,
      columns: [
        { width: "10%" },
        { width: "60%" },
        { width: "20%" },
        { width: "10%" },
      ]
    });

    $(".merchant-enabled-logs").hide();

    if ( $('#industry').find(":selected").text().indexOf( "Others" ) == -1 )
    {
      $('#others').hide();
    }

    if ( $("#status_trial").attr('checked') ) {
      $(".trial_date_area").show();
      $(".trial_date_area").prop('requred', true);
    } else {
      $(".trial_date_area").hide();
      $(".trial_date_area").prop('requred', false);
    }

    $('#trial_date_end').datetimepicker({
      format: 'MM/DD/YYYY'
    });

    $("input[name='status']").change( function() {
      if ( $(this).val() == "Full" ) {
        $(".trial_date_area").hide();
      } else {
        $(".trial_date_area").show();
      }
    });

    $("#industry").chosen().change(function(){
      $('#others_ID ').val('');
      var text  = $('#industry').find(":selected").text();
      var term = "Others";
      if( text.indexOf( term ) != -1 )
      {
          $('#industry option:selected').prop('selected', false);
          $('#industry').trigger('chosen:updated');
          $("#others").show(1,
            function() {
              $('#others_ID ').focus();
              $("#others_ID").prop("required", true);
            }
          );
      } else {
        $('#others').hide();
        $("#others_ID").val();
        $("#others_ID").prop("required", false);
      }
    });

    $("#enabled").bind("change", function() {
      var _this = $(this);
      if ( _this.val() !== '{{ $merchant->enabled }}' ) { $(".reason-section").show(); $("#reason").prop('required',true); }
      else { $(".reason-section").hide(); $("#reason").prop('required',false);  }

    });

    $("#enabled-disabled-toggle").bind("click", function() {
      $(".merchant-enabled-logs").slideToggle();
    });

    @if ( isset($updated) && $updated === TRUE )
      swal("The Merchant Info has been updated!", "", "success")
    @endif

    $(document).on('change', '#reseller', function () {
        if ( $(this).val() != '' ) {
            $('#reseller_email').prop('required', true);
        } else {
            $('#reseller_email').removeAttr('required');
        }
    });

    $(document).on('click', '#add-add-ons', function() {
      var template = $('#add-ons-template').html();
      $('.add-ons-area').append( template );
    });

    $(document).on('click', '.remove-add-ons', function() {
      $(this).parent().parent().remove();
    });

    $(document).on('change', '#new_billing_calculation', function() {
      if ( $(this).val() == 1 ) {
        $("#tier-area").show();
      } else {
        $("#tier-area").hide();
      }
    });

    if ( $('#new_billing_calculation').val() == 1 ) { $("#tier-area").show(); }
    else { $("#tier-area").hide(); }

  }); /* Document Ready Close */
</script>

<script id="add-ons-template" type="text/template">
  <div class="col-md-12">
    <div class="col-md-4 form-group">
      <input type="text" class="form-control" placeholder="Description" name="add-ons-desc[]" required />
    </div>
    <div class="col-md-2 form-group">
      <input type="text" class="form-control" placeholder="Quantity" name="add-ons-quantity[]" required />
    </div>
    <div class="col-md-2 form-group">
      <input type="text" class="form-control" placeholder="Amount" name="add-ons-amount[]" required />
    </div>
    <div class="col-md-2 form-group">
      {!! Form::select('add-ons-vat[]', [ 0 => 'without VAT', 1 => 'with VAT' ], null, [ 'class' => 'form-control' ] ) !!}
    </div>
    <div class="col-md-2 form-group">
      <button class="remove-add-ons" onclick="return false;" style="padding:8px; border-radius: 5px; border: none;"><i class="fa fa-minus" aria-hidden="true"></i></button>
    </div>
  </div>
</script>
@endsection