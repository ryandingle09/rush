@extends('admin.app')

@section('view')
<div id="content">
  <!-- Nav tabs -->
  <div class="navTabsWrap">
      <ul class="nav nav-tabs" role="tablist" id="tabs">
          <li role="presentation" class="active"><a href="#list" role="tab" data-toggle="tab">Merchants</a></li>
          <!-- <li role="presentation"><a href="#payment" role="tab" data-toggle="tab">Payment</a></li> -->
      </ul>
  </div>
  <!-- Tab panes -->
  <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="list">
          <div class="row">
            <div class="col-md-12">
            </div>
          </div>
          <div class="row merchants-table-row-div">
              <div class="col-md-12">
                  <table id="merchants-table" class="data-table">
                      <thead>
                        <tr>
                          <th></th>
                          <th class="text-left">Business Name</th>
                          <th class="text-left">RUSH Solution</th>
                          <th class="text-right">Outstanding Balance</th>
                          <th class="text-center">Due Date</th>
                          <th class="text-left">Status</th>
                          <th class="text-center">Enabled</th>
                        </tr>
                      </thead>
                      <tbody>
                      @if ( isset($merchants) && $merchants )
                      @foreach( $merchants as $merchant )
                        {{-- */ $latest_billing = $merchant->billings->last(); /* --}}
                        <tr>
                          <td class="text-right">
                            <a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant->merchantid }}">{{ $merchant->merchantid }}</a>
                          </td>
                          <td class="text-left"><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant->merchantid }}">{{ $merchant->businessName }}</a></td>
                          <td class="text-left">{{ ( $merchant->package ) ? $merchant->package->name : '' }}</td>
                          <td class="text-right">{{ number_format( ( !$latest_billing ? 0 :$latest_billing->outstanding_balance), 2, '.',',') }}</td>
                          <td class="text-center">{{ ( !$latest_billing ? '-' : Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $latest_billing->due_date )->format('m/d/Y')) }}</td>
                          <td class="text-left">{{ $merchant->status_for_admin }}</td>
                          <td class="text-center"><a id="merchant-row-{{ $merchant->merchantid }}" class="enabled-disabled-toggle {{ $merchant->enabled_for_admin }}" data-status="{{ $merchant->enabled_for_admin}}" data-id="{{$merchant->merchantid}}">{{ $merchant->enabled_for_admin }}</a></td>
                        </tr>
                      @endforeach
                      @endif
                      </tbody>
                  </table>
                </div>
          </div>
      </div>
      <!-- <div role="tabpanel" class="tab-pane" id="payment">
        <div class="noTab">
        </div>
      </div> -->
  </div>
</div>
@endsection

@section('modal')
<div id="enabled-disabled-toggle-modal" class="modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"><h3>Reason</h3></div>
      <div class="modal-body">
      <div class="row">
        <div class="col-md-12 form-group">
          <textarea id="enabled-disabled-reason" class="form-control"></textarea>
          <input type="hidden" name="merchant_id" id="merchant_id_holder" />
          <input type="hidden" name="status" id="merchant_status_holder" />
        </div>
      </div>
      <div class="row">
        <div class="col-md-3 form-group">
          <button id="enabled-disabled-submit" class="btn btn-primary form-control">Submit</button>
        </div>
        <div class="col-md-3 form-group">
          <button id="enabled-disabled-cancel" class="btn btn-warning form-control">Cancel</button>
        </div>
      </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('footerPlaceholder')
<script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
<script type="text/javascript">
  $(document).ready( function() {
    var emptyTableMessage = 'No matching records found';
    var excelExportButton = '<div class="text-center"><button type="button" class="btn btn-default exportButton">Export to Excel</button></div>';

    /* Initialize Merchants Table DataTable */
    $('#merchants-table').dataTable({
      'sDom': '<"fixed-table-toolbar"fl><"excelButton hide"B>t<"bottom"ip><"clear">',
      'buttons': [ 'excel' ],
      'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
      'language': { 'emptyTable': emptyTableMessage },
      'paging': true,
      'ordering': false,
      'pagingType': "full_numbers",
      fnDrawCallback: function() {
        initialise();
      },
      'columns': [
          { width: "5%" },
          { width: "25%" },
          { width: "20%" },
          { width: "15%" },
          { width: "11%" },
          { width: "12%" },
          { width: "12%" },
        ]
    });

    if ($('#merchants-table tbody tr.odd td').text() != emptyTableMessage) {
      $('.merchants-table-row-div').append(excelExportButton);
    }

    // bind excel export button
    $('.tab-pane').on('click', '.exportButton', function(event) {
        $(this).parent().parent().find('.dataTables_wrapper .excelButton a').click();
    });

  });

  function toggleEnabledDisabledMerchant() {

    var _text = $('#enabled-disabled-reason').val();
    var _id = $('#merchant_id_holder').val()
    var _status = $("#merchant_status_holder").val();

    var formData = {
        id: _id,
        enabled: _status,
        text: _text,
    };

    var _element = $("#merchant-row-"+_id);

    $.ajax({
        type: "POST",
        headers: {'X-CSRF-TOKEN': "{{csrf_token()}}"},
        url: "{{ URL::to('/') }}/admin/enable",
        data: formData,
        dataType: 'json',
        success: function(data) {
          if ( data.enabled === true ) {
            _element.attr('data-status', 'enabled').removeClass('disabled').addClass('enabled').text('enabled');
          } else {
            _element.attr('data-status', 'disabled').removeClass('enabled').addClass('disabled').text('disabled');
          }
        },
        error: function(data) {
          console.log('Error:', data);
        }
    });

    $("#merchant_id_holder").val('');
    $("#merchant_status_holder").val('');
    $("#enabled-disabled-reason").val('');
    $("#enabled-disabled-toggle-modal").css('display', "none");
  }

  function initialise() {

    $("#enabled-disabled-cancel").unbind("click").bind("click", function() {
        $("#merchant_id_holder").val('');
        $("#merchant_status_holder").val('');
        $("#enabled-disabled-reason").val('');
        $("#enabled-disabled-toggle-modal").css('display', "none");
    });

    $(".enabled-disabled-toggle").unbind("click").bind("click", function() {
      var _id = $(this).attr('data-id');
      var _enabled = $(this).attr('data-status');

      $("#merchant_id_holder").val( $(this).attr('data-id') );
      $("#merchant_status_holder").val( $(this).attr('data-status') );
      $("#enabled-disabled-submit").unbind("click").bind("click", function() {
        if ( $("#enabled-disabled-reason").val() != "" ) {
          toggleEnabledDisabledMerchant();
        } else {
          swal("Please state the reason!", "", "warning");
        }
      });
      $("#enabled-disabled-toggle-modal").css('display', "block");
    });
  };
</script>
@endsection
