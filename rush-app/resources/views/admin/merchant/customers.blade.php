@extends('admin.app')

@section('view')
<div id="content" class="merchant-view">
  <!-- Nav tabs -->
  <div class="navTabsWrap">
    <ul class="nav nav-tabs" id="tabs">
        <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}">INFO</a></li>
        <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/branches">BRANCH</a></li>
        <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/employees">EMPLOYEE</a></li>
        <li class="active"><a >CUSTOMER</a></li>
        <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/points">POINTS</a></li>
        <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/transactions">TRANSACTIONS</a></li>
        <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/billing">BILLING</a></li>
    </ul>
  </div>
  <!-- Tab panes -->
  <div class="tab-content">
    <div class="tab-pane active" id="customer">
      <div class="row">
        <div class="col-md-12">
        </div>
      </div>
      <div class="row" id="management-customers-view">
        <div class="md-col-10">
          <table id="customerTable" class="data-table">
              <thead>
              <tr>
                <th>Date Registered</th>
                <th>Customer Name</th>
                <th>Mobile No.</th>
                <th>Email Address</th>
                <th>Birthdate</th>
                <th>Gender</th>
                <th>Registration Channel</th>
                <th>Branch</th>
              </tr>
              </thead>
              <tbody>
              @if(isset($customers) && $customers)
              @foreach($customers as $customer )
              <tr>
                <td>{{ $customer->timestamp }}</td>
                <td>{{ $customer->fullName }}</td>
                <td>{{ $customer->mobileNumber }}</td>
                <td>{{ $customer->email }}</td>
                <td>{{ $customer->birthDate }}</td>
                <td>{{ $customer->gender }}</td>
                <td>{{ $customer->registrationChannel }}</td>
                <td>{{ ( $customer->branch) ? $customer->branch->branchName : '' }}</td>
              </tr>
            @endforeach
            @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div><!--/content-->
@endsection

@section('footerPlaceholder')
<script type="text/javascript">
  $(document).ready( function() {
    var emptyTableMessage = 'No matching records found';
    var excelExportButton = '<div class="text-center"><button type="button" class="btn btn-default exportButton">Export to Excel</button></div>';

    /* Initialize Branches Table DataTable */
    $('#customerTable').DataTable({
      'sDom': '<"fixed-table-toolbar"fl><"excelButton hide"B>t<"bottom"ip><"clear">',
      'buttons': [ 'excel' ],
      'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
      'language': { 'emptyTable': emptyTableMessage },
    });

    if ($('#customerTable tbody tr.odd td').text() != emptyTableMessage) {
      $('#management-customers-view').append(excelExportButton);
    }

    // bind excel export button
    $('.tab-pane').on('click', '.exportButton', function(event) {
        $(this).parent().parent().find('.dataTables_wrapper .excelButton a').click();
    });

  });
</script>
@endsection

