@extends('admin.app')

@section('view')
<div id="content" class="merchant-view">
  <!-- Nav tabs -->
  <div class="navTabsWrap">
    <ul class="nav nav-tabs" role="tablist" id="tabs">
      <ul class="nav nav-tabs" id="tabs">
          <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}">INFO</a></li>
          <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/branches">BRANCH</a></li>
          <li class="active"><a>EMPLOYEE</a></li>
          <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/customers">CUSTOMER</a></li>
          <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/points">POINTS</a></li>
          <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/transactions">TRANSACTIONS</a></li>
          <li><a href="{{ URL::to('/') }}/admin/merchant/{{ $merchant_id }}/billing">BILLING</a></li>
      </ul>
    </ul>
  </div>
  <!-- Tab panes -->
  <div class="tab-content">
    <div class="tab-pane active" id="employee">
      <div class="row">
        <div class="col-md-12">
        </div>
      </div>
      <div class="row" id="management-employees-view">
        <div class="md-col-10">
          <table id="employeeTable" class="data-table">
              <thead>
              <tr>
                  <th class="text-left">Employee Name</th>
                  <th class="text-left">Branch Assigned</th>
                  <th class="text-right">Status</th>
              </tr>
              </thead>
              <tbody>
              @if(isset($employees) && $employees)
              @foreach($employees as $employee )
              <tr>
                <td class="text-left">{{ $employee->employeeName }}</td>
                <td class="text-left">{{ $employee->branch->branchName }}</td>
                <td class="text-right {{ $employee->is_deleted_text }} is_deleted_text">{{ $employee->is_deleted_text }}</td>
              </tr>
            @endforeach
            @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div><!--/content-->
@endsection

@section('footerPlaceholder')
<script type="text/javascript">
  $(document).ready( function() {
    var emptyTableMessage = 'No matching records found';
    var excelExportButton = '<div class="text-center"><button type="button" class="btn btn-default exportButton">Export to Excel</button></div>';

    /* Initialize Branches Table DataTable */
    $('#employeeTable').DataTable({
      'sDom': '<"fixed-table-toolbar"fl><"excelButton hide"B>t<"bottom"ip><"clear">',
      'buttons': [ 'excel' ],
      'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
      'language': { 'emptyTable': emptyTableMessage },
    });

    if ($('#employeeTable tbody tr.odd td').text() != emptyTableMessage) {
      $('#management-employees-view').append(excelExportButton);
    }

    // bind excel export button
    $('.tab-pane').on('click', '.exportButton', function(event) {
        $(this).parent().parent().find('.dataTables_wrapper .excelButton a').click();
    });

  });
</script>
@endsection
