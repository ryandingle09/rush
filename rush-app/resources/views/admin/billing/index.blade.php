@extends('admin.app')

@section('view')
<div id="content">
  <!-- Nav tabs -->
  <div class="navTabsWrap">
      <ul class="nav nav-tabs" role="tablist" id="tabs">
          <li role="presentation" class="active"><a href="#list" role="tab" data-toggle="tab">Billings</a></li>
      </ul>
  </div>
  <!-- Tab panes -->
  <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="list">
          <div class="row">
            <div class="col-md-12">
            </div>
          </div>
          <div class="row billings-table-row-div">
              <div class="col-md-12">
                  <table id="billings-table" class="data-table">
                      <thead>
                        <tr>
                          <th class="text-center">Id</th>
                          <th class="text-left">Merchant</th>
                          <th class="text-left">Package</th>
                          <th class="text-left">Period</th>
                          <th class="text-left">Statement Date</th>
                          <th class="text-left">Due Date</th>
                          <th class="text-center">Status</th>
                          <th class="text-right">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      @if ( isset($billings) && $billings )
                      @foreach( $billings as $billing )
                        <tr>
                          <td class="text-center">
                            {{ $billing->id }}
                          </td>
                          <td class="text-left">
                            {{ $billing->business_name }}
                          </td>
                          <td class="text-left">
                            @if( $billing->package )
                              {{ $billing->package->name }}
                            @endif
                          </td>
                          <td class="text-left">
                            {{ Carbon\Carbon::parse($billing->billing_period_start)->format('M j') }} - {{ Carbon\Carbon::parse($billing->billing_period_end)->format('M j, Y') }}
                          </td>
                          <td>{{ Carbon\Carbon::parse($billing->statement_date)->format('M j, Y') }}</td>
                          <td>{{ Carbon\Carbon::parse($billing->due_date)->format('M j, Y') }}</td>
                          <td class="text-center">
                            @if( $billing->is_paid_text == 'paid')
                              <span style="color:green; text-transform: capitalize;">{{ $billing->is_paid_text }}</span>
                            @elseif ( $billing->is_paid_text == "pending" )
                              <span style="color:orange; text-transform: capitalize;">{{ $billing->is_paid_text }}</span>
                            @else
                              <span style="color:red; text-transform: capitalize;">{{ $billing->is_paid_text }}</span>
                            @endif
                          </td>
                          <td>
                            <a href="{{ url('admin/billing/' . $billing->id ) }}" class="edit">
                              <i class="fa fa-pencil"></i> Edit
                            </a>
                          </td>
                        </tr>
                      @endforeach
                      @endif
                      </tbody>
                  </table>
                </div>
          </div>
      </div>
  </div>
</div>
@endsection


@section('footerPlaceholder')
<script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
<script type="text/javascript">
  $(document).ready( function() {
    var emptyTableMessage = 'No matching records found';
    var excelExportButton = '<div class="text-center"><button type="button" class="btn btn-default exportButton">Export to Excel</button></div>';

    /* Initialize Billings Table DataTable */
    $('#billings-table').dataTable({
      'sDom': '<"fixed-table-toolbar"fl><"excelButton hide"B>t<"bottom"ip><"clear">',
      'buttons': [ 'excel' ],
      'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
      'language': { 'emptyTable': emptyTableMessage },
      'paging': true,
      'ordering': false,
      'pagingType': "full_numbers",
      'columns': [
          { width: "10%" },
          { width: "15%" },
          { width: "15%" },
          { width: "25%" },
          { width: "15%" },
          { width: "15%" },
          { width: "10%" },
          { width: "10%" },
        ]
    });

    if ($('#billings-table tbody tr.odd td').text() != emptyTableMessage) {
      $('.billings-table-row-div').append(excelExportButton);
    }

    // bind excel export button
    $('.tab-pane').on('click', '.exportButton', function(event) {
        $(this).parent().parent().find('.dataTables_wrapper .excelButton a').click();
    });

  });
</script>
@endsection
