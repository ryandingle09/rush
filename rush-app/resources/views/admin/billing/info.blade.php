@extends('admin.app')

@section('view')
<div id="content">
  <!-- Nav tabs -->
  <div class="navTabsWrap">
      <ul class="nav nav-tabs" role="tablist" id="tabs">
          <li role="presentation" class="active"><a href="#info" role="tab" data-toggle="tab">TAG PAYMENT</a></li>
      </ul>
  </div>
  <!-- Tab panes -->
  <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="info">
        <br /><br />
        <form action="{{ url( '/admin/billing/' . $billing->id ) }}" method="POST" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="row">
            <div class="col-md-2"><label>Billing ID</label></div>
            <div class="col-md-2">{{ $billing->id }}</div>
          </div>
          <div class="row">
            <div class="col-md-2"><label>Merchant</label></div>
            <div class="col-md-3">{{ $billing->business_name }}</div>
          </div>
          <div class="row">
            <div class="col-md-2"><label>Account Number</label></div>
            <div class="col-md-3">{{ $billing->account_number }}</div>
          </div>
          <div class="row">
            <div class="col-md-2 form-group"><label>RUSH Solution</label></div>
            <div class="col-md-2">@if( $billing->package ) {{ $billing->package->name }} @endif</div>
          </div>
          <div class="row">
            <div class="col-md-2 form-group"><label>Total</label></div>
            <div class="col-md-2">{{ number_format( $billing->total_amount, 2, ".", ",") }}</div>
          </div>
          <div class="row">
            <div class="col-md-2 form-group"><label for="payment_type">Payment Method</label></div>
            <div class="col-md-3 form-group">
              {{ Form::select('payment_type', [ null => '--' , 'cash' => 'Cash', 'credit_card' => 'Credit Card', 'check' => 'Check', 'bill' => 'Charge to Bill'], $billing->payment_type, [ 'id' => 'payment_type', 'class' => 'form-control']) }}
            </div>
          </div>
          <div class="row">
            <div class="col-md-2 form-group"><label for="or_number">Payment Status</label></div>
            <div class="col-md-3 form-group">
              {{ Form::select('is_paid', [ 0 => 'Unpaid' , 1 => 'Paid', 2 => 'Pending'], $billing->is_paid, [ 'id' => 'is_paid', 'class' => 'form-control']) }}
            </div>
          </div>
          <div class="row">
            <div class="col-md-2 form-group"><label for="or_number">OR Number</label></div>
            <div class="col-md-3 form-group">
              <input type='text' id="or_number" name="or_number" class="form-control" value="{{ $billing->or_number }}">
            </div>
          </div>
          <div class="row">
            <div class="col-md-2 form-group"><label for="payment_date">Payment Date</label></div>
            <div class="col-md-3 form-group">
              <input type='text' id="payment_date" name="payment_date" class="form-control" value="{{ Carbon\Carbon::parse($billing->payment_date)->format('m/d/Y') }}" placeholder="mm/dd/yyyy"/>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2 form-group"><label for="payment_amount">Amount Paid</label></div>
            <div class="col-md-3 form-group">
              <input type='text' id="payment_amount" name="payment_amount" class="form-control" value="{{ $billing->payment_amount }}" />
            </div>
          </div>
          <div class="row">
            <div class="col-md-2 form-group"><label for="reference_number">Payment Reference Number</label></div>
            <div class="col-md-3 form-group">
              <input type='text' id="reference_number" name="reference_number" class="form-control" value="{{ $billing->reference_number }}" />
            </div>
          </div>
          <div class="row">
            <div class="col-md-2 form-group"><label>Upload Deposit Slip</label></div>
            <div class="col-md-3 form-group">
              <label for="deposit_slip" id="deposit_upload_btn">
              <input type="file" id="deposit_slip" name="deposit_slip" class="form-control" accept="image/*" value="{{ $billing->deposit_slip }}"/>
              <span>Browse</span>
              </label>
            </div>
            <div class="col-md-12 form-group">
              <img id="image" style="border: 1px dotted #e7e7e7;" src="{{ $billing->deposit_slip }}" />
            </div>
          </div>
          <div class="row">
              <div class="col-md-1 form-group">
                <input class="btn btn-primary yellow-btn" id="billing-tag-submit" type="submit" value="Update">
              </div>
              <div class="col-md-1 form-group">
                <a href="{{ url('admin/billing') }}" class="btn btn-warning">Back</a>
              </div>
          </div>
        </form>
      </div>
  </div>
</div>
@endsection


@section('footerPlaceholder')
@parent
<script type="text/javascript">
  $(document).ready( function() {
      $('#payment_date').datetimepicker({
        format: 'MM/DD/YYYY'
      });

      document.getElementById("deposit_slip").onchange = function () {
          var reader = new FileReader();
          reader.onload = function (e) {
              // get loaded data and render thumbnail.
              document.getElementById("image").src = e.target.result;
          };
          // read the image file as a data URL.
          reader.readAsDataURL(this.files[0]);
      };

      @if ( !$billing->is_latest )
      $('body').on('click', '#billing-tag-submit', function() {
        swal({
          title: "",
          html: true,
          text: "<p>You can only tag payment on the latest billing of a merchant.</p><br /><p><a href=\"{{ url('admin/billing/' . $billing->latest_billing->id ) }}\">Click here to go to this merchant's latest billing.</a></p>",
          button: "Close"
        });
        return false;
      });
      @endif

  });
</script>
@endsection
