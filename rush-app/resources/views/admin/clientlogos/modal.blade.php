<!-- New Logo -->
<div id="newlogo" class="modal fade admin-cms-modal" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Add New Logo</div>
            <div class="modal-body">
                <form action="{{ url('onboarding-cms/clientlogos/add') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12">
                            <label for="clientName">Client Name</label>
                            <input type="text" class="form-control" name="clientName" id="clientName" required>
                        </div>
                        <div class="col-md-8">
                            <label for="clientimg-filename">Upload Image</label>
                            <input type="text" class="form-control" name="clientimg-filename" id="clientimg-filename" disabled>
                            <i>Required Size: 200px x 250px</i>
                        </div>
                        <div class="col-md-4">
                            <label for="clientimg">&nbsp;</label>
                            <span class="btn btn-default btn-file">
                                Choose File <input type="file" name="clientimg" class="fuImage" ew="200" eh="250" accept="image/jpeg, image/png" required>
                            </span>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button class="btn btn-primary pull-right">UPLOAD LOGO</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Edit Logo -->
<div id="editlogo" class="modal fade admin-cms-modal" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Edit Logo</div>
            <div class="modal-body">
                <form action="{{ url('onboarding-cms/clientlogos/edit') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="clientlogos_id" id="clientlogos_idE" />
                    <div class="row">
                        <div class="col-md-12">
                            <label for="clientNameE">Client Name</label>
                            <input type="text" class="form-control" name="clientNameE" id="clientNameE" required>
                        </div>
                        <div class="col-md-8">
                            <label for="clientimgE-filename">Upload Image</label>
                            <input type="text" class="form-control" name="clientimgE-filename" id="clientimgE-filename" disabled>
                            <i>Required Size: 200px x 250px</i>
                        </div>
                        <div class="col-md-4">
                            <label for="clientimgE">&nbsp;</label>
                            <span class="btn btn-default btn-file">
                                Choose File <input type="file" name="clientimgE" class="fuImage" ew="200" eh="250" accept="image/jpeg, image/png">
                            </span>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button class="btn btn-primary pull-right">UPLOAD LOGO</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Delete Logo -->
<div id="deletelogo" class="modal fade admin-cms-modal" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Delete Client Logo</div>
            <div class="modal-body">
                <p>Are you sure you want to delete this item?</p>
                <form action="{{ url('onboarding-cms/clientlogos/delete') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="clientlogos_id" id="clientlogos_idD" />
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button class="btn btn-primary pull-right" id="delete_class_btn">DELETE</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>