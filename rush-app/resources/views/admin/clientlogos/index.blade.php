@extends('admin.app')

@section('view')
<!-- <div id="result"></div> -->
<div class="button-toolbar clientlogos">
	<button class="btn btn-primary" data-toggle="modal" data-target="#newlogo">Add New Logo</button>
</div>
<div id="content" class="admin-cms clientlogos">
    <table id="clientlogos-table" class="data-table">
		<thead>
			<tr>
				<th></th>
				<th>Logo</th>
				<th>Client Name</th>
				<th>Date Uploaded</th>
				<th>Visibility</th>
				<th></th>
			</tr>
        </thead>
        <tfoot>
            <tr>
                <th></th>
                <th>Logo</th>
                <th>Client Name</th>
                <th>Date Uploaded</th>
                <th>Visibility</th>
                <th></th>
            </tr>
        </tfoot>
		<tbody>
			<tr>
				<td>1</td>
				<td>
                    <span class="logo-thumb">
                        <img src="" alt="">
                    </span>                    
                </td>
				<td>Yoga +</td>
				<td>January 1, 2018</td>
				<td>
					<select name="" id="news-visibility-1" class="vis-select">
						<option value="1">Visible</option>
						<option value="0">Hidden</option>
					</select>
				</td>
				<td>
                    <a href="#" data-toggle="modal" data-target="#editlogo" class="edit"><i class="fa fa-pencil"></i>Edit</a>
					<a href="#" data-toggle="modal" data-target="#deletelogo" data-action="" onclick="return false" class="delete"><i class="fa fa-trash"></i>Delete</a>
				</td>
			</tr>
			<tr>
                <td>2</td>
				<td>
                    <span class="logo-thumb">
                        <img src="" alt="">
                    </span>                    
                </td>
				<td>The Common Space</td>
				<td>January 2, 2018</td>
				<td>
					<select name="" id="news-visibility-2" class="vis-select">
						<option value="1">Visible</option>
						<option value="0" selected>Hidden</option>
					</select>
				</td>
				<td>
                    <a href="#" data-toggle="modal" data-target="#editlogo" class="edit"><i class="fa fa-pencil"></i>Edit</a>
					<a href="#" data-toggle="modal" data-target="#deletelogo" data-action="" onclick="return false" class="delete"><i class="fa fa-trash"></i>Delete</a>
				</td>
			</tr>
			<tr>
				<td>3</td>
				<td>
                    <span class="logo-thumb">
                        <img src="" alt="">
                    </span>                    
                </td>
				<td>Gold's Gym</td>
				<td>January 5, 2018</td>
				<td>
					<select name="" id="news-visibility-3" class="vis-select">
						<option value="1">Visible</option>
						<option value="0">Hidden</option>
					</select>
				</td>
				<td>
                    <a href="#" data-toggle="modal" data-target="#editlogo" class="edit"><i class="fa fa-pencil"></i>Edit</a>
					<a href="#" data-toggle="modal" data-target="#deletelogo" data-action="" onclick="return false" class="delete"><i class="fa fa-trash"></i>Delete</a>
				</td>
			</tr>
			<tr>
                <td>4</td>
				<td>
                    <span class="logo-thumb">
                        <img src="" alt="">
                    </span>                    
                </td>
				<td>Auntie Anne's</td>
				<td>December 26, 2017</td>
				<td>
					<select name="" id="news-visibility-4" class="vis-select">
						<option value="1">Visible</option>
						<option value="0" selected>Hidden</option>
					</select>
				</td>
				<td>
                    <a href="#" data-toggle="modal" data-target="#editlogo" class="edit"><i class="fa fa-pencil"></i>Edit</a>
					<a href="#" data-toggle="modal" data-target="#deletelogo" data-action="" onclick="return false" class="delete"><i class="fa fa-trash"></i>Delete</a>
				</td>
			</tr>
			<tr>
                <td>5</td>
				<td>
                    <span class="logo-thumb">
                        <img src="" alt="">
                    </span>                    
                </td>
				<td>Mathemagis</td>
				<td>July 26, 2017</td>
				<td>
					<select name="" id="news-visibility-5" class="vis-select">
						<option value="1" selected>Visible</option>
						<option value="0">Hidden</option>
					</select>
				</td>
				<td>
                    <a href="#" data-toggle="modal" data-target="#editlogo" class="edit"><i class="fa fa-pencil"></i>Edit</a>
					<a href="#" data-toggle="modal" data-target="#deletelogo" data-action="" onclick="return false" class="delete"><i class="fa fa-trash"></i>Delete</a>
				</td>
			</tr>
		</tbody>
	</table>
</div>
@include('admin.clientlogos.modal')
@endsection

@section('footerPlaceholder')
@parent
<script src="{{ url('page/admin/js/dataTables.rowReorder.min.js') }}"></script>
<script src="{{ url('page/admin/js/clientlogos.js') }}"></script>

@endsection