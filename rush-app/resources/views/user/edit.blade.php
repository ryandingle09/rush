@extends('layouts.app')

@section('view')
<div class="container">
    <div id="content" class="design spacing">
        
    <form action="{{ url('users/'.$id.'/edituser') }}" method="POST">
    {{ csrf_field() }}
      <div class="row form-group">
        <div class="col-md-2">
          <label for="name">Name</label>
        </div>
        <div class="col-md-4">
          <input type="text" name="name" id="name" class="form-control" value="{{ $user->name }}" required pattern="[a-zA-Z0-9 ]+"/>
          <span class="error-msg help-block">
            @if( $errors->has('name'))
               {{ $errors->first('name') }}
            @endif
          </span>
        </div>
      </div>
      <div class="row form-group">
        <div class="col-md-2">
          <label for="email">Email</label>
        </div>
        <div class="col-md-4">
          <input type="text" name="email_display" id="email_display" class="form-control" value="{{ $user->email }}" disabled />
          <input type="hidden" name="email" id="email" class="form-control" value="{{ $user->email }}" />
        </div>
      </div>
      <div class="row form-group">
        <div class="col-md-2">
          <label for="password">Password</label>
        </div>
        <div class="col-md-4">
          <input type="password" name="password" id="password" class="form-control" value=""/>
          <span class="error-msg help-block">
            @if( $errors->has('password'))
               {{ $errors->first('password') }}
            @endif
          </span>
        </div>
      </div>

      <div class="row form-group">
        <div class="col-md-12">
          <label for="password">Modules</label>
        </div>
        <div class="cold-md-12">
          <span class="error-msg help-block">
            @if( $errors->has('modules'))
               {{ $errors->first('modules') }}
            @endif
          </span>
        </div>
        <div class="col-md-12">
        <div class="row form-group">
            @foreach( $modules as $module )
            <div class="col-md-3">
            @if ( !empty($user_modules) && in_array($module->module_id, $user_modules) )
              {{ Form::checkbox('modules[]', $module->module_id, ['checked' => true] ) }} {{ $module->name }}
            @else
              {{ Form::checkbox('modules[]', $module->module_id ) }} {{ $module->name }}
            @endif
                
            </div>
            @endforeach
          </div>
        </div>
      </div>

      <div class="row form-group">
        <div class="col-md-1">
          {{ Form::submit('Submit', ['class'=>'btn btn-primary']) }}
        </div>
        <div class="col-md-1">
          {{ link_to( url('users') , 'Cancel', ['class'=>'btn btn-primary']) }}
        </div>
      </div>
    </form>

    </div><!-- content -->
</div> <!-- container -->
@endsection

@section('footerPlaceholder')
@parent
<script type="text/javascript">
$(document).ready( function() {
  $("#name").keypress(function (e) {
      var regex = new RegExp("^[a-zA-Z0-9 ]+$");
      var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      if (regex.test(str)) {
          console.log('true');
          return true;
      } else {
          console.log('false');
      }

      e.preventDefault();
      return false;
  });
});
</script>
@endsection