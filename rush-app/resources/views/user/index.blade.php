@extends('layouts.app')

@section('view')
<div class="container">
    <div id="content" class="management marginTop">
        <!-- Nav tabs -->
        <div class="navTabsWrap">
            <ul class="nav nav-tabs" role="tablist" id="tabs">
                <li role="presentation" class="active"><a href="#users" aria-controls="users" role="tab" data-toggle="tab">Users</a></li>
                @if ( $merchant->bcu_enabled )
                    <li role="presentation"><a href="#bcu" aria-controls="bcu" role="tab" data-toggle="tab">BCU</a></li>
                @endif
            </ul>
        </div>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="users">
                <table id="usersTable" data-toggle="table" class="data-table management" data-pagination="true" data-search="true">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ( $sub_users as $user )
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td class="{{ $user->status }}">{{ $user->status }}</td>
                        <td>
                            @if ( $user->is_deleted == 0 )
                            <a href="#" data-toggle="modal" data-target="#deleteUser" data-action="{{ url('users/deleteuser/' . $user->id ) }}" onclick="return false" class="delete userDelete"><i class="fa fa-trash"></i>Disable</a>
                            @else
                            <a href="#" data-toggle="modal" data-target="#enableUser" data-action="{{ url('users/enableuser/' . $user->id ) }}" onclick="return false" class="delete userEnable"><i class="fa fa-toggle-up"></i>Enable</a>
                            @endif
                            <a href="{{ url('users/'.$user->id.'/edituser') }}" class="edit"><i class="fa fa-pencil"></i>Edit</a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>

                <a href="{{ url('users/adduser')}}" data-toggle="modal" class="btn btn-primary"><i class="fa fa-plus"></i>Add User</a>
            </div>

            <div role="tabpanel" class="tab-pane" id="bcu">
            <div class="noTab spacing">
              <form action="{{ url('users#bcu') }}" method="POST">
                {{ csrf_field() }}
                <div class="row form-group">
                    <div class="col-md-12">
                      <label for="password">Modules</label>
                    </div>
                    <div class="cold-md-12">
                      <span class="error-msg help-block">
                        @if( $errors->has('modules'))
                           {{ $errors->first('modules') }}
                        @endif
                      </span>
                    </div>
                    
                    <div class="col-md-12">
                    <div class="row form-group">
                        @foreach( $modules as $module )
                        <div class="col-md-3">
                        @if ( !empty($merchant_bcu_modules) && in_array($module->module_id, $merchant_bcu_modules) )
                          {{ Form::checkbox('modules[]', $module->module_id, ['checked' => true] ) }} {{ $module->name }}
                        @else
                          {{ Form::checkbox('modules[]', $module->module_id ) }} {{ $module->name }}
                        @endif
                            
                        </div>
                        @endforeach
                      </div>
                    </div>

                  </div>
                  <div class="row form-group">
                    <div class="col-md-1">
                      {{ Form::submit('Submit', ['class'=>'btn btn-primary']) }}
                    </div>
                  </div>
              </form>
            </div>
            </div>

        </div>
    </div>
</div> <!-- /container -->
@endsection

@section('modal')
@parent
<div id="deleteUser" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Disable User</div>
            <div class="modal-body">
                <p>Are you sure you want to disable this user?</>            
                <span id="userToDelete"></span>
                <form method="POST" id="deleteUserForm">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button class="btn btn-primary pull-right">Disable</button>
                        </div>                        
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<div id="enableUser" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Enable User</div>
            <div class="modal-body">
                <p>Are you sure you want to enable this user?</>            
                <span id="userToEnable"></span>
                <form method="POST" id="enableUserForm">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button class="btn btn-primary pull-right">Enable</button>
                        </div>                        
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
@endsection

@section('footerPlaceholder')
@parent
<style>
    #usersTable .enabled {
        text-transform: capitalize;
        color: green;
    }

    #usersTable .disabled {
        color: red; 
        text-transform: capitalize; 
    }
</style>
<script type="text/javascript">
    $(document).ready( function() {
        $('.bootstrap-table').on('click', '.userDelete', function() {
            var $row = $(this).closest('tr');
            var $cell = $row.find('td');
            $('#userToDelete').text($cell.eq(1).text());
            $('#deleteUserForm').attr('action',$(this).data('action'));
        });

        $('.bootstrap-table').on('click', '.userEnable', function() {
            var $row = $(this).closest('tr');
            var $cell = $row.find('td');
            $('#userToEnable').text($cell.eq(1).text());
            $('#enableUserForm').attr('action',$(this).data('action'));
        });
    });
</script>
@endsection