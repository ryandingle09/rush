@extends('layouts.app')

@section('headerPlaceholder')
<link rel="stylesheet" href="page/promos/css/style.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">

<style>
.bootstrap-select{
    margin-bottom: 25px !important;
}

.bootstrap-select button[data-toggle="dropdown"]{
    outline: 0 !important;
    width: 100% !important;
    background: #fff;
    border: 0;
    border-radius: 0;
    float: none;
    display: block;
}
</style>
@endsection

@section('view')
<div class="container">
    @include('flash.message')
    <div id="content" class="smstool setupPunch marginTop">
        <!-- Nav tabs -->
        <div class="navTabsWrap">
            <ul class="nav nav-tabs" role="tablist" id="tabs">
                <li role="presentation" class="active"><a href="#mechanics" aria-controls="mechanics" role="tab" data-toggle="tab">Program Mechanics</a></li>
                <li role="presentation"><a href="#membership" aria-controls="membership" role="tab" data-toggle="tab">Membership</a></li>
                @if($is_enable_product_module)
                <li role="presentation"><a href="#mechanics-per-product" aria-controls="mechanics-per-product" role="tab" data-toggle="tab">Mechanics per Product</a></li>
                <li role="presentation"><a href="#mechanics-product-qr" aria-controls="mechanics-product-qr" role="tab" data-toggle="tab">Mechanics Per Product QR</a></li>
                @endif
            </ul>
        </div>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="mechanics">
                <button href="#addConversion" class="btn btn-primary smsAdd" data-toggle="modal" onclick="return false">Add Mechanics</button>
                <table id="conversionTable" data-toggle="table" class="data-table management" data-pagination="true" data-search="true">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Earning Amount</th>
                        <th>Earning Points</th>
                        <th>Redemption Points</th>
                        <th>Redemption Amount</th>
                        <th>Type</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if (count($conversions) > 0)
                    @foreach ($conversions as $conversion)
                    <?php $customAttributes = [
                        'branch_id' => $conversion->branch_id,
                        'branch_name' => $conversion->branch_name,
                        'membership_id' => $conversion->membership_id,
                        'membership_name' => $conversion->membership_name
                    ]; ?>
                    <tr {!!CmsHelper::getDomDataFormat(
                        CmsHelper::getArrayByKeys(array_merge($conversion->toArray(), $customAttributes),
                        ['id', 'earning_peso', 'earning_points', 'redemption_points', 'redemption_peso', 'transformable_id', 'transformable_type', 'branch_id', 'branch_name', 'membership_id', 'membership_name']))
                    !!}>
                        <td>{{$conversion->id}}</td>
                        <td>{{$conversion->earning_peso}}</td>
                        <td>{{$conversion->earning_points}}</td>
                        <td>{{$conversion->redemption_points}}</td>
                        <td>{{$conversion->redemption_peso}}</td>
                        <td>{{$conversion->transformable_type}}</td>
                        <td>{{$conversion->membership_name}}@if(!empty($conversion->branch_name) && !empty($conversion->membership_name)){{':'}}@endif{{$conversion->branch_name}}</td>
                        <td>
                            <a href="#" data-toggle="modal" data-target="#addConversion" onclick="return false" class="edit conversionEdit"><i class="fa fa-copy"></i>Copy</a>
                        </td>
                    </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="membership">
                <button href="#addMembership" id='addMembershipButton' class="btn btn-primary smsAdd" data-toggle="modal" onclick="return false">Add Membership</button>
                <table id="membershipTable" data-toggle="table" class="data-table management" data-pagination="true" data-search="true">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Level</th>
                        <!--<th>Frequency Redemption</th>-->
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if (count($memberships) > 0)
                    @foreach ($memberships as $membership)
                    <tr {!!CmsHelper::getDomDataFormat(CmsHelper::getArrayByKeys($membership->toArray(), ['id', 'name', 'level', 'frequency_redemption']))!!}>
                        <td>{{$membership->id}}</td>
                        <td>{{$membership->name}}</td>
                        <td>{{$membership->level}}</td>
                        <!--<td>{{$membership->frequency_redemption}}</td>-->
                        <td>
                            <a href="#" data-toggle="modal" data-target="#addMembership" onclick="return false" class="edit membershipEdit"><i class="fa fa-pencil"></i>Edit</a>
                        </td>
                    </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="mechanics-per-product">
                <button href="#addMechanicsPprod" id='addMechanicsPprodButton' class="btn btn-primary smsAdd" data-toggle="modal" onclick="return false">Add Product</button>
                <table id="mechanicsPprodTable" data-toggle="table" class="data-table management" data-pagination="true" data-search="true">
                    <thead>
                    <tr>
                        <th>Item Code</th>
                        <th>Item Name</th>
                        <th>Price</th>
                        <th>Points</th>
                        <th>Branch</th>
                        <th>Date Created</th>
                        <th>Date Updated</th>
                        <th>Updated By</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr data-product-id="{{ $product->id }}">
                            <td data-code="{{ $product->code }}">{{ $product->code }}</td>
                            <td data-name="{{ $product->name }}">{{ $product->name }}</td>
                            <td data-amount="{{ $product->amount }}">{{ $product->amount }}</td>
                            <td data-points="{{ $product->points }}">{{ $product->points }}</td>
                            <td data-branch-ids="{{ $product->branch_ids }}">{{ $product->branches_name }}</td>
                            <td>{{ $product->created_at }}</td>
                            <td>{{ $product->updated_at }}</td>
                            <td>{{ $product->user_last_updated->personName or " " }}</td>
                            <td>
                                <a href="#" data-action-url="{{MigrationHelper::getAppBaseUrl()}}promos/product/{{ $product->id }}" onclick="return false" class="delete delMechanicsPprod"><i class="fa fa-trash"></i>Delete</a>
                                <a href="#" class="edit editMechanicsPprod"><i class="fa fa-pencil"></i>Edit</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="mechanics-product-qr">
                <button href="#addProdQR" class="btn btn-primary smsAdd" data-toggle="modal" onclick="return false">Add Product QR</button>
                <button href="#downloadAllProdQR" class="btn btn-primary smsAdd hidden" data-toggle="modal" onclick="return false">Download All QR</button>
                <table id="productQR" data-toggle="table" class="data-table management" data-pagination="true" data-search="true">
                    <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Load</th>
                            <th>Points</th>
                            <th>Scan Limit</th>
                            <th>Branch</th>
                            <th>Type</th>
                            <th>Status</th>
                            <th>Date Created</th>
                            <th>Date Updated</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($products_qr as $product_qr)
                            <tr data-product-qr-id="{{ $product_qr->id }}">
                                <td data-name="{{ $product_qr->name }}">{{ $product_qr->name }}</td>
                                <td data-load="{{ $product_qr->load }}">{{ $product_qr->load }}</td>
                                <td data-points="{{ $product_qr->points }}">{{ $product_qr->points }}</td>
                                <td data-customer-scan-limit="{{ $product_qr->customer_scan_limit }}">{{ $product_qr->customer_scan_limit }}</td>
                                <td data-branch-ids="{{ $product_qr->branch_ids }}">{{ implode(',', $product_qr->branches) }}</td>
                                <td data-type="{{ $product_qr->type }}">{{ $product_qr->type }}</td>
                                <td data-active="{{ $product_qr->active }}">{{ $product_qr->status_label }}</td>
                                <td>{{ $product_qr->created_at }}</td>
                                <td>{{ $product_qr->updated_at }}</td>
                                <td>
                                    <a href="{{ $product_qr->image_url }}" download type="button" class="btn btn-primary download-qr-btn pull-left">Download QR</a>
                                    <a href="#" class="edit editProdQR pull-left"><i class="fa fa-pencil"></i>Edit</a>
                                    <a href="#" data-action-url="{{MigrationHelper::getAppBaseUrl()}}promos/product_qr/{{ $product_qr->id }}" onclick="return false" class="delete deleteProdQR pull-left"><i class="fa fa-trash"></i>Delete</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div><!--noTab-->
    </div><!--/content-->
</div> <!-- /container -->
@endsection

@section('modal')
<!--  MODALS -->
<div id="addConversion" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Add Program Mechanics</div>
            <div class="modal-body">
                <form action="{{MigrationHelper::getAppBaseUrl()}}promos" method="post" class="form-inline">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-12">
                            <h4>Earning Conversion</h4>
                            <div class="form-group">
                                <label for="earningPointsAmount">Php</label>
                                <input currency required type="text" class="form-control" id="earningPointsAmount" name="earningPointsAmount" value="{{$earningPointsAmount or 1}}">
                                <label for="earningPoints">=</label>
                                {{Form::selectRange('earningPoints', 1, 20, $earningPoints, ['class' => 'form-control', 'required' => null])}}
                                <label for="earningPoints">Point</label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h4>Redemption Conversion</h4>
                            <div class="form-group">
                                <label for="redemptionPoints">Point</label>
                                {{Form::selectRange('redemptionPoints', 1, 20, $redemptionPoints, ['class' => 'form-control', 'required' => null])}}
                                <label for="redemptionPoints">=</label>
                                <input currency required  type="text" class="form-control" id="redemptionPointsAmount" name="redemptionPointsAmount" value="{{$redemptionPointsAmount or 1}}">
                                <label for="redemptionPointsAmount">Php</label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h4>Scope</h4>
                        </div>
                        <div class="col-md-12">
                            {{Form::label('branch', 'Branch')}}
                            {{Form::select('branch', $branchOptions, null, ['placeholder' => 'choose...'])}}
                        </div>
                        <div class="col-md-12">
                            {{Form::label('membership', 'Membership')}}
                            {{Form::select('membership', $membershipOptions, null, ['placeholder' => 'choose...'])}}
                        </div>
                        <div class="col-md-12">
                            {{Form::hidden('transformable_id')}}
                            {{Form::hidden('transformable_type')}}
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button class="btn btn-primary pull-right">ADD</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="addMembership" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header"><span class='toggleText'>ADD</span> Membership</div>
            <div class="modal-body">
                <form action="{{MigrationHelper::getAppBaseUrl()}}promos/membership" method="post" class="form-inline">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-12">
                            <h4>Membership Details</h4>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                {{Form::label('name', 'Name')}}
                                {{Form::text('name', '', ['required', 'maxlength' => 20])}}
                            </div>
                            <div class="col-md-12">
                                {{Form::label('level', 'Level')}}
                                {{Form::select('level', $levelOptions, null, ['required', 'placeholder' => 'choose...'])}}
                            </div>
                            <div class="col-md-12 hide">
                                {{Form::label('frequencyRedemption', 'Frequency of Redemption')}}
                                {{Form::text('frequencyRedemption', '')}}
                            </div>
                        </div>
                        <div class="col-md-12">
                            {{Form::hidden('membership_id')}}
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button class="btn btn-primary pull-right">SAVE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Mechanics per Product -->
<div id="addMechanicsPprod" class="modal fade mechanicsPprod" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Add Product</div>
            <div class="modal-body">
                <form action="{{MigrationHelper::getAppBaseUrl()}}promos/product" method="post">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <label for="prodCode">Product Code</label>
                            <input type="text" name="code" id="prodCode" class="form-control" required>
                        </div>
                        <div class="col-md-6">
                            <label for="prodName">Product Name</label>
                            <input type="text" name="name" id="prodName" class="form-control" required>
                        </div>
                        <div class="col-md-6">
                            <label for="prodPrice">Price</label>
                            <input type="text" name="amount" id="prodPrice" class="form-control" required>
                        </div>
                        <div class="col-md-6">
                            <label for="prodPoints">Points</label>
                            <input type="text" name="points" id="prodPoints" class="form-control" required>
                        </div>
                        <div class="col-md-6">
                            <label for="classBranches">Branches</label>
                            <select required class="selectpicker form-control" name="branch_ids[]" id="classBranches" multiple title="Select Branches" data-actions-box="true">
                                @if(!empty($branchOptions))
                                    @foreach($branchOptions as $branch_key => $branch_name)
                                        <option value="{{ $branch_key }}">{{ $branch_name }}</option>
                                    @endforeach
                                @else
                                    <option>No Registered Branch</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button type="submit" class="btn btn-primary pull-right">SAVE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Add Product QR -->
<div id="addProdQR" class="modal fade productQR" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Add Product</div>
            <div class="modal-body">
                <form action="{{MigrationHelper::getAppBaseUrl()}}promos/product_qr" method="post">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="qr-prodName">Product Name</label>
                            <input type="text" name="name" id="qr-prodName" class="form-control" required>
                        </div>
                        <div class="col-md-6">
                            <label for="qr-load">Load</label>
                            <input type="text" name="load" id="qr-load" class="form-control num-only" required>
                        </div>
                        <div class="col-md-6">
                            <label for="qr-points">Points</label>
                            <input type="text" name="points" id="qr-points" class="form-control num-only" required>
                        </div>
                        <div class="col-md-6">
                            <label for="qr-custScanLimit">Customer Scan Limit</label>
                            <input type="text" name="customer_scan_limit" id="qr-custScanLimit" class="form-control num-only" required>
                        </div>
                        <div>
                            <div class="col-md-6">
                                <label for="classBranches">Branches</label>
                                <select required class="selectpicker form-control" name="branch_ids[]" id="productBranches" multiple title="Select Branches" data-actions-box="true">
                                    @if(!empty($branchOptions))
                                        @foreach($branchOptions as $branch_key => $branch_name)
                                            <option value="{{ $branch_key }}">{{ $branch_name }}</option>
                                        @endforeach
                                    @else
                                        <option>No Registered Branch</option>
                                    @endif
                                </select>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12 prod-qr-type">
                                        <label for="qr-type">Type</label>
                                        <select name="type" id="qr-type" class="form-control" required>
                                            <option disabled selected>Select Type</option>
                                            <option value="earned">Earned</option>
                                            <option value="redeemed">Redeemed</option>
                                        </select>
                                    </div>
                                    <div class="col-md-12 prod-qr-status">
                                        <div>
                                            <label for="qr-status">Status</label>
                                        </div>
                                        <div class="qr-status-options">
                                            <label class="qr-status-label">
                                                <i class="fa fa-check-square-o qr-enabled"></i>
                                                <input type="radio" name="active" id="qr-enabled" class="qr-status-box" hidden value="1" checked="checked"> Enabled
                                            </label>
                                            <label class="qr-status-label">
                                                <i class="fa fa-square-o qr-disabled"></i>
                                                <input type="radio" name="active" id="qr-disabled" class="qr-status-box" hidden value="0"> Disabled
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button class="btn btn-primary pull-right">SAVE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End . Add Product QR -->

<div id="editMechanicsPprod" class="modal fade mechanicsPprod" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Edit Product</div>
            <div class="modal-body">
                <form action="{{MigrationHelper::getAppBaseUrl()}}promos/product" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    <input type="hidden" name="product_id">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="prodCode">Product Code</label>
                            <input type="text" name="code" id="prodCode" class="form-control" required>
                        </div>
                        <div class="col-md-6">
                            <label for="prodName">Product Name</label>
                            <input type="text" name="name" id="prodName" class="form-control" required>
                        </div>
                        <div class="col-md-6">
                            <label for="prodPrice">Price</label>
                            <input type="text" name="amount" id="prodPrice" class="form-control" required>
                        </div>
                        <div class="col-md-6">
                            <label for="prodPoints">Points</label>
                            <input type="text" name="points" id="prodPoints" class="form-control" required>
                        </div>
                        <div class="col-md-6">
                            <label for="classBranches">Branches</label>
                            <select required class="selectpicker form-control" name="branch_ids[]" id="classBranches" multiple title="Select Branches" data-actions-box="true">
                                @if(!empty($branchOptions))
                                    @foreach($branchOptions as $branch_key => $branch_name)
                                        <option value="{{ $branch_key }}">{{ $branch_name }}</option>
                                    @endforeach
                                @else
                                    <option>No Registered Branch</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button type="submit" class="btn btn-primary pull-right">UPDATE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Edit Product QR -->
<div id="editProdQR" class="modal fade productQR" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Edit Product</div>
            <div class="modal-body">
                <form action="{{MigrationHelper::getAppBaseUrl()}}promos/product_qr" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    <input type="hidden" name="product_qr_id">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="eqr-prodName">Product Name</label>
                            <input type="text" name="name" id="eqr-prodName" class="form-control" required>
                        </div>
                        <div class="col-md-6">
                            <label for="eqr-load">Load</label>
                            <input type="text" name="load" id="eqr-load" class="form-control num-only" required>
                        </div>
                        <div class="col-md-6">
                            <label for="eqr-points">Points</label>
                            <input type="text" name="points" id="eqr-points" class="form-control num-only" required>
                        </div>
                        <div class="col-md-6">
                            <label for="eqr-custScanLimit">Customer Scan Limit</label>
                            <input type="text" name="customer_scan_limit" id="eqr-custScanLimit" class="form-control num-only" required>
                        </div>
                        <div>
                            <div class="col-md-6">
                                <label for="classBranches">Branches</label>
                                <select required class="selectpicker form-control" name="branch_ids[]" id="eproductBranches" multiple title="Select Branches" data-actions-box="true">
                                    @if(!empty($branchOptions))
                                        @foreach($branchOptions as $branch_key => $branch_name)
                                            <option value="{{ $branch_key }}">{{ $branch_name }}</option>
                                        @endforeach
                                    @else
                                        <option>No Registered Branch</option>
                                    @endif
                                </select>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12 prod-qr-type">
                                        <label for="qr-type">Type</label>
                                        <select name="type" id="eqr-type" class="form-control" required>
                                            <option disabled selected>Select Type</option>
                                            <option value="earned">Earned</option>
                                            <option value="redeemed">Redeemed</option>
                                        </select>
                                    </div>
                                    <div class="col-md-12 prod-qr-status">
                                        <div>
                                            <label for="qr-status">Status</label>
                                        </div>
                                        <div class="qr-status-options">
                                            <label class="qr-status-label">
                                                <i class="fa fa-square-o qr-enabled"></i>
                                                <input type="radio" name="active" id="qr-enabled" class="qr-status-box" hidden value="1"> Enabled
                                            </label>
                                            <label class="qr-status-label">
                                                <i class="fa fa-square-o qr-disabled"></i>
                                                <input type="radio" name="active" id="qr-disabled" class="qr-status-box" hidden value="0"> Disabled
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button class="btn btn-primary pull-right">UPDATE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End . Edit Product QR -->

<div id="delMechanicsPprod" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Delete Product</div>
            <div class="modal-body">
                <p>Are you sure you want to delete this item?</p>
                <strong id="prodToDelete">#XF2145</strong>
                <form action="" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <div class="form-group row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <button class="btn btn-default  pull-right" data-dismiss="modal">CANCEL</button></div>
                            <div class="col-md-6">
                                <button class="btn btn-primary">DELETE</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Delete Product QR -->
<div id="deleteProdQR" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Delete Product</div>
            <div class="modal-body">
                <form action="" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <p>Are you sure you want to delete this Product?</p> <strong id="prodToDelete">Dessenge Shampoo</strong>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button class="btn btn-default" data-dismiss="modal">CANCEL</button>
                            <button class="btn btn-primary pull-right">DELETE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End . Delete Product QR -->
@endsection

@section('footerPlaceholder')
@parent
<script type="text/javascript">

$('#conversionTable').on('click', '.conversionEdit', function() {
    var $row = $(this).closest('tr');
    var $conversionForm = $('#addConversion');
    $conversionForm.find('[name=earningPointsAmount]').val($row.data('earning_peso'));
    $conversionForm.find('[name=earningPoints]').val($row.data('earning_points'));
    $conversionForm.find('[name=redemptionPointsAmount]').val($row.data('redemption_peso'));
    $conversionForm.find('[name=redemptionPoints]').val($row.data('redemption_points'));
    $conversionForm.find('[name=transformable_id]').val($row.data('transformable_id'));
    $conversionForm.find('[name=transformable_type]').val($row.data('transformable_type'));
    $conversionForm.find('[name=branch]').val($row.data('branch_id'));
    $conversionForm.find('[name=membership]').val($row.data('membership_id'));
});

$('#membershipTable').on('click', '.membershipEdit', function() {
    var $row = $(this).closest('tr');
    var $membershipForm = $('#addMembership');
    $membershipForm.find('[name=name]').val($row.data('name'));
    $membershipForm.find('[name=level]').val($row.data('level'));
    $membershipForm.find('[name=frequencyRedemption]').val($row.data('frequency_redemption'));
    $membershipForm.find('[name=membership_id]').val($row.data('id'));
    $membershipForm.find('.toggleText').trigger('toggleTextEvent', ['EDIT']);
});

$('.toggleText').bind( "toggleTextEvent", function(e, newText) {
  $(this).text(newText);
});

$('#addMembershipButton').on('click', function() {
    $('#addMembership input').not('[name=_token]').val('');
    $('#addMembership .toggleText').trigger('toggleTextEvent', ['ADD']);
});
//** MECHANICS PER PRODUCT  **//
// Price to Points
$('.mechanicsPprod #prodPrice, #prodPoints').on('keypress', function(){
    if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
        event.preventDefault();
    }
});

$('#mechanicsPprodTable').on('click', '.editMechanicsPprod', function(e) {
    e.preventDefault();

    var parent_tr = $(this).parents('tr');
    var product_id = parent_tr.attr('data-product-id');
    var target_modal = $('#editMechanicsPprod');

    var code = parent_tr.find('[data-code]').attr('data-code');
    var name = parent_tr.find('[data-name]').attr('data-name');
    var amount = parent_tr.find('[data-amount]').attr('data-amount');
    var points = parent_tr.find('[data-points]').attr('data-points');
    var branch_ids = parent_tr.find('[data-branch-ids]').attr('data-branch-ids');
    var type = parent_tr.find('[data-type]').attr('data-type');

    target_modal.find("[name='product_id']").val(product_id);
    target_modal.find("[name='code']").val(code);
    target_modal.find("[name='name']").val(name);
    target_modal.find("[name='amount']").val(amount);
    target_modal.find("[name='points']").val(points);
    target_modal.find("[name='type']").val(type);

    if(branch_ids === "0"){
        target_modal.find("[name='branch_ids[]']").selectpicker("selectAll");
    } else {
        target_modal.find("[name='branch_ids[]']").selectpicker('val', branch_ids.split(','));
    }

    target_modal.modal('show');
});

$('#mechanicsPprodTable').on('click', '.delMechanicsPprod', function(e) {
    e.preventDefault();

    var parent_tr = $(this).parents('tr');
    var action_url = $(this).attr('data-action-url');
    var target_modal = $('#delMechanicsPprod');

    var code = parent_tr.find('[data-code]').attr('data-code');

    target_modal.find("form").attr('action', action_url);
    target_modal.find('#prodToDelete').text(code);
    target_modal.modal('show');
});

// Number Only
$('.num-only').keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        return false;
    }
});

$('#productQR').on('click', '.editProdQR', function(e) {
    e.preventDefault();

    var parent_tr = $(this).parents('tr');
    var product_qr_id = parent_tr.attr('data-product-qr-id');
    var target_modal = $('#editProdQR');

    var name = parent_tr.find('[data-name]').attr('data-name');
    var load = parent_tr.find('[data-load]').attr('data-load');
    var points = parent_tr.find('[data-points]').attr('data-points');
    var customer_scan_limit = parent_tr.find('[data-customer-scan-limit]').attr('data-customer-scan-limit');
    var branch_ids = parent_tr.find('[data-branch-ids]').attr('data-branch-ids');
    var type = parent_tr.find('[data-type]').attr('data-type');
    var active = parent_tr.find('[data-active]').attr('data-active');

    target_modal.find("[name='product_qr_id']").val(product_qr_id);
    target_modal.find("[name='name']").val(name);
    target_modal.find("[name='load']").val(load);
    target_modal.find("[name='points']").val(points);
    target_modal.find("[name='customer_scan_limit']").val(customer_scan_limit);
    target_modal.find("[name='type']").selectpicker('val', [type]);

    if(branch_ids === "0"){
        target_modal.find("[name='branch_ids[]']").selectpicker("selectAll");
    } else {
        target_modal.find("[name='branch_ids[]']").selectpicker('val', branch_ids.split(','));
    }

    if(active === "0"){
        target_modal.find("[name='active'][value='0']").prop('checked', true);
        target_modal.find("[name='active'][value='1']").prop('checked', false);
    } else {
        target_modal.find("[name='active'][value='1']").prop('checked', true);
        target_modal.find("[name='active'][value='0']").prop('checked', false);
    }

    target_modal.modal('show');
});

$('#productQR').on('click', '.deleteProdQR', function(e) {
    e.preventDefault();

    var parent_tr = $(this).parents('tr');
    var action_url = $(this).attr('data-action-url');
    var target_modal = $('#deleteProdQR');

    var name = parent_tr.find('[data-name]').attr('data-name');

    target_modal.find("form").attr('action', action_url);
    target_modal.find('#prodToDelete').text(name);
    target_modal.modal('show');
});

// Product QR - Status
$('.qr-status-label').on('click', function(e){
    var _radioOption = $(this).find('.qr-status-box').attr('id');
    var _radioCheck = $('i.' + _radioOption);
    _radioCheck.removeClass('fa-square-o').addClass('fa-check-square-o').siblings('.qr-status-box').prop('checked', true);
    $('i.fa-square-o, i.fa-check-square-o').not('.' + _radioOption).removeClass('fa-check-square-o').addClass('fa-square-o').siblings('.qr-status-box').prop('checked', false);
    e.preventDefault();
});
// Product QR - Default on Edit
$('#editProdQR').on('show.bs.modal', function(){
    if($('#editProdQR .ebranches .select-all').is(':checked')){
        $('#editProdQR .ebranches .checkbox').prop('checked', true);
        $("#editProdQR .ebranches .fa-check").css("display", "block");
    }
    else
    {
        $('#editProdQR .ebranches .checkbox').prop('checked', false);
        $("#editProdQR .ebranches .fa-check").css("display", "none");
    }
    $('#editProdQR .ebranches .checkbox').each(function(){
        if( $(this).attr('checked') ) $(this).siblings('label').click();
    });
    $('#editProdQR .qr-status-box').each(function(){
        if( $(this).is(':checked') ) $(this).siblings('.fa').removeClass('fa-square-o').addClass('fa-check-square-o');
    })
});
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
@endsection
