<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
  <meta property="og:title" content="RUSH - Loyalty Made Easy">
  <meta property="og:image" content="http://rush.ph/app/assets/images/rush-image1.jpg">
  <meta property="og:image" content="http://rush.ph/app/assets/images/rush-logo.jpg">
  <meta property="og:description" content="Conveniently develop and deploy a custom-branded mobile app in just three days. Easily build, track, and manage your own customer loyalty program with RUSH. Sign up and get a 30-day free trial.">
  <meta property="og:type" content="website" />
  <meta property="og:image:type" content="image/jpeg" />
  <link rel="icon" href="http://rush.ph/app/page/onboarding/images/favicon.png" type="image/png">
	<title>RUSH</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="{{ url('app/page/my-business-demo/components/bootstrap/dist/css/bootstrap.css') }}">
	<link rel="stylesheet" href="{{ url('app/page/my-business-demo/css/style.css') }}">
	<style>
	@media screen and (max-width: 575px){
		#rc-imageselect, .g-recaptcha {transform:scale(0.77);-webkit-transform:scale(0.77);transform-origin:0 0;-webkit-transform-origin:0 0;}
		}
	@media screen and (max-width: 360px){
		#rc-imageselect, .g-recaptcha {transform:scale(0.66);-webkit-transform:scale(0.66);transform-origin:0 0;-webkit-transform-origin:0 0;}
		}
	</style>
	<script src="//assets.adobedtm.com/09e448d55636ef986d22fd41e1f6c9f074c8d4fe/satelliteLib-974d698ad7e033074e0e7d57efa7a8fe900a289a.js"></script>
</head>
<body>
<nav>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand globe-logo" href="#">
      	<img src="{{ url('app/page/my-business-demo/images/logo-globe-mybusiness.svg') }}" alt="" width="160px">
      </a>
	    <a class="navbar-brand rush-logo" href="#">
      	<img src="{{ url('app/page/my-business-demo/images/img-rush-logonew.svg') }}" alt="" width="100px">
      </a>
    </div>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#request-appointment" class="ra">REQUEST APPOINTMENT</a></li>
        
      </ul>
  </div>
</nav>

<section>
	<div id="banner-carousel" class="carousel slide" data-ride="carousel">
	  <!-- Wrapper for slides -->
	  <div class="carousel-inner" role="listbox">
	    <div class="item loyalty active">
	      <img src="{{ url('app/page/my-business-demo/images/bg-banner.svg') }}" alt="...">
	      <div class="carousel-caption">
	      	<div class="left-block">
		        <h3>Loyalty Made Easy.</h3>
		        <p>
		        	Build & foster long-lasting customer relationships with a RUSH-powered loyalty program. 
		        </p>
		        <p>
		        	Create, design and manage your own customer loyalty program easily with RUSH. Conveniently develop and deploy custom-branded mobile add in as short as 3 days.	
		        </p>
	      	</div>
	      	<div class="right-block">
	      		<img src="{{ url('app/page/my-business-demo/images/orig.png') }}" alt="">
	      	</div>
	      </div>
	    </div>
    	<div class="item basic">
			<img src="{{ url('app/page/my-business-demo/images/bg-banner.svg') }}" alt="...">
			<div class="carousel-caption">
				<div class="left-block">
					<h3>Punch Card Package</h3>
					<p>White label digital punchcard</p>
					<h3>Issue Stamps</h3>
					<p>Create a loyalty program with a digital stamp card that will reward customers for their visit or purchase. </p>
				</div>
				<div class="right-block">
					<img src="{{ url('app/page/my-business-demo/images/basic.png') }}" alt="">
				</div>
			</div>
		</div>
	    <div class="item pro">
			<img src="{{ url('app/page/my-business-demo/images/bg-banner.svg') }}" alt="...">
			<div class="carousel-caption">
				<div class="left-block">
					<h3>Points Package</h3>
					<p>White label points-based loyalty program</p>
					<h3>Issue Points</h3>
					<p>Customers can earn points with every purchase and can redeem a reward once they reach a set number of points.</p>
				</div>
				<div class="right-block">
					<img src="{{ url('app/page/my-business-demo/images/pro.png') }}" alt="">
				</div>
			</div>
		</div>
	  </div>

	  <a class="left carousel-control" href="#banner-carousel" role="button" data-slide="prev">
	    <img src="{{ url('app/page/my-business-demo/images/icon-back.svg') }}" alt="" width="50px">
	  </a>
	  <a class="right carousel-control" href="#banner-carousel" role="button" data-slide="next">
	    <img src="{{ url('app/page/my-business-demo/images/icon-next.svg') }}" alt="" width="50px">
	  </a>
	</div>
</section>
<div class="padY40"></div>
<section>
	<div class="container">
		<h3 class="header">We will get in touch with you within 3-5 business days.</h3>
		<div class="row" id="items">
			<div class="col-md-12">
				<div class="it">
					<div>
						<img src="{{ url('app/page/my-business-demo/images/icon-request-appointment.svg') }}" alt="" style="margin-top: 10px;width: 200px;">
					</div>
					<h3>Request Appointment</h3>
					<p>An account manager will get in touch with you within 3-5 business days.</p>
					<br>
					<button type="button" onclick="javascript:window.location.href='#request-appointment'; return false;">REQUEST AN APPOINTMENT</button>
				</div>
			</div>
		</div>
	</div>
</section>
	<div class="padY60"></div>
<section class="testimonials-wrapper">
 <div id="testimonials" class="carousel slide" data-ride="carousel">

   <!-- Wrapper for slides -->
   <div class="carousel-inner" role="listbox">
     <div class="item active">
       <div class="carousel-caption">
         <h3>Globe myBusiness</h3>
         <p>We made it possible to take your reward program to the next level. Streamline your program and makes it easier for your customers to track and redeem their rewards. The platform itself is flexible, user friendly and comprehensive.</p>
        <!--<div class="testimonial-photo">
          <img src="assets/images/GMB.png" alt="">
        </div>-->
        <b>RUSH</b><br>
        <span class="t-position">Loyalty Made Easy</span>
       </div>
     </div>
<!--     <div class="item">
       <div class="carousel-caption">
         <h3>Auntie Anne's Philippines</h3>
         <p>RUSH has made it possible for us to take our reward program to the next level. It helped us streamline our program and makes it easier for our customers to track and redeem their rewards. The platform itself is flexible, user friendly and comprehensive.</p>
        <div class="testimonial-photo">
         <img src="assets/images/AA.png" alt="">
        </div>
        <b>MICHELLE GUBALLA</b><br>
        <span class="t-position">Marketing Manager</span>
       </div>
     </div>-->
   </div>
   <!-- Controls -->
  <!-- <a class="left carousel-control " href="#testimonials" role="button" data-slide="prev">
	    <img src="{{ url('app/page/my-business-demo/images/icon-back.svg') }}" alt="" width="50px">
	  </a>
	  <a class="right carousel-control" href="#testimonials" role="button" data-slide="next">
	    <img src="{{ url('app/page/my-business-demo/images/icon-next.svg') }}" alt="" width="50px">
	  </a>
   </a>-->
 </div>
</section>
<section id="form-section">
      <div class="container rq-lowerpart">
        <div class="col-md-6 col-sm-6 col-xs-12">
          	<h3>Benefits of RUSH-powered loyalty program:</h3>
          	<ul>
            	<li>
	              	<div class="x dm-icon">
	                	<img src="{{ url('app/page/my-business-demo/images/icon-quick-easy-setup.svg') }}">
	              	</div>
	              	<div class="col-md-10">
		                <h4>Quick and easy setup</h4>
		                <span>Start rewarding your customers with points or stamps in a jiff.</span>
	               	</div>
              	</li>
              	<li>
	                <div class="dm-icon">
	                  <img src="{{ url('app/page/my-business-demo/images/icon-multiple-store-integrations.svg') }}">
	                </div>
	                <div class="col-md-10">
	                  <h4>Multiple store integrations</h4>
	                  <span>Seamlessly integrate your loyalty program to your
	                    POS system via a widget or through a full-API
	                    integration.</span>
	                  </div>
                </li>
                <li>
                  <div class="dm-icon">
                    <img src="{{ url('app/page/my-business-demo/images/icon-highly-customizable-reward-system.svg') }}">
                  </div>
                  <div class="col-md-10">
                    <h4>Highly-customizable reward system</h4>
                    <span>Create your reward mechanics depending on your
                      business goals.</span>
                    </div>
                </li>
                <li>
                    <div class=" dm-icon">
                      <img src="{{ url('app/page/my-business-demo/images/icon-realtime-dashboard.svg') }}">
                    </div>
                    <div class="col-md-10">
                      <h4>Real time dashboard with actionable insights</h4>
                      <span>Obtain real time customer data through transactions
                        and get insights on how to improve your program
                        and business.</span>
                      </div>
                </li>
                <li>
	                  <div class="dm-icon">
	                    <img src="{{ url('app/page/my-business-demo/images/icon-multiple-store-integrations.svg') }}">
	                  </div>
	                  <div class="col-md-10">
	                    <h4>Multi-channel broadcast tool</h4>
	                    <span>Reach your customers via built-in marketing tools.<br><br><br><br><br></span>
	                  </div>
                </li>
            </ul>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
        	<form method="post" action="" id="request-appointment">
        	<div class="row">
        		{{ csrf_field() }}
          		<h2><b>Request An Appointment</b></h2>
          		 <label>Contact information</label>
          		<div class="col-md-12 paddingb10 group-field">
                    <div class="col-md-6 group-field">
                        <label>First Name*</label>
                        <input type="text" class="df-field" name="fname" required>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5 group-field">
                        <label>Last Name*</label>
                        <input type="text" class="df-field" name="lname" required>
                    </div>
                    @if( $errors->has('name'))<div class="error-label">{{ $errors->first('fname') }}</div> @endif
                    @if( $errors->has('name'))<div class="error-label">{{ $errors->first('lname') }}</div> @endif
                </div>
                <div class="col-md-12 paddingb10 group-field">
                    <label>Mobile*</label>
                    <div class="input-group">
                        <span class="input-group-addon">+63</span>
                        <input type="text" class="df-field mobile" name="mobile" required maxlength="10" placeholder="9xxxxxxxxx">
                    </div>
                    @if( $errors->has('mobile'))<div class="error-label">{{ $errors->first('mobile') }}</div> @endif
                </div>
              	<div class="col-md-12 paddingb10 group-field">
	                <label>Email*</label>
	                <input type="text" class="df-field" name="email" required>
	                @if( $errors->has('email'))<div class="error-label">*{{ $errors->first('email') }}</div> @endif
              	</div>
    			<div class="col-md-12 paddingb10 group-field">
        			<label>Company Name*</label>
	                <input type="text" class="df-field" name="company" required>
	                @if( $errors->has('company'))<div class="error-label">*{{ $errors->first('company') }}</div> @endif
	            </div>
              	<div class="col-md-12 paddingb10 group-field ">
                	<label>Industry*</label>
	                <select class="df-field" name="industry[]" required>
						<option selected="selected" value="">Select Industry</option>
						<option>Automotive Dealers and Manufacturers</option>
						<option>Automotive Services</option>
						<option>Entertainment</option>
						<option>Fitness</option>
						<option>Food and Beverage Establishment</option>
						<option>Fuel and Petroleum</option>
						<option>Grocery and Supermarket</option>
						<option>Grooming and Wellness</option>
						<option>Hospitals and Healthcare Services</option>
						<option>Insurance</option>
						<option>Petroleum</option>
						<option>Retail - Apparel</option>
						<option>Retail - Electronics</option>
						<option>Retail - Household and Hardware</option>
						<option>Retail - Sporting Goods</option>
						<option>Telecommunication and Utilities</option>
						<option>Transportation</option>
						<option>Travel and Leisure</option>
						<option>Others</option>
	                </select>
	                @if( $errors->has('industry'))<div class="error-label">*{{ $errors->first('industry') }}</div> @endif
              	</div>
                    <div class="col-md-12 paddingb20 group-field multiselect">
                        <label>Preferred product(Optional)</label>
                        <div class="col-md-12 nopadding-left">
                            <div class="col-md-4 nopadding-left">
                                <input type="checkbox" id="basic" name="product[]" value="Punch Card">
                                <label for="basic"> Punch Card</label></div>
                            <div class="col-md-4 nopadding-left">
                                <input type="checkbox" id="pro" name="product[]" value="Points">
                                <label for="pro"> Points</label></div>
                        </div>
                    </div>
       			<div class="col-md-12 padding20 group-field">
            		<label>Objective of Loyalty Program (Optional)</label>
                   	<div class="multiselect">
				        <div class="selectBox">
				            <select>
				                <option selected="selected">Select Objective</option>
				            </select>
				            <div class="overSelect"></div>
				        </div>
						<div id="checkboxes" >
                            <div class="col-md-12">
                                <input type="checkbox" id="one" name="program[]" value="Enhance customer engagement" class="selected">
                                <label for="one">Enhance customer engagement</label>
                            </div>
                            <div class="col-md-12">
                                <input type="checkbox" id="two" name="program[]" value="Improve customer retention" class="selected">
                                <label for="two">Improve customer retention</label>
                            </div>
                            <div class="col-md-12">
                                <input type="checkbox" id="three" name="program[]" value="Increase frequency of customer visits" class="selected">
                                <label for="three">Increase frequency of customer visits</label>
                                <input type="checkbox" id="four" name="program[]" value="Drive revenue per transaction" class="selected">
                                <label for="four">Drive revenue per transaction</label>
                                <input type="checkbox" id="five" name="program[]" value="Gather insights and understand customer behavior">
                                <label for="five">Gather insights and understand customer behavior</label>
                                <input type="checkbox" id="six" name="program[]" value="Promote and push new products and services" class="selected">
                                <label for="six">Promote and push new products and services</label>
                                <input type="checkbox" id="seven" name="program[]" value="Others">
                                <label for="seven">Others</label>
                            </div>
                        </div>
					</div>
				</div>
				<div class="col-md-12 padding20 group-field">
                    <textarea class="form-control" placeholder="Tell us more about your business (optional)" rows="5" name="details"></textarea>
                </div>
			  	<div class="col-md-12 paddingb10 group-field">
			    	<div class="g-recaptcha" data-sitekey="6LeSJSkTAAAAAJqYJJczKaKyhU6G_0DXkRsZM_1-"></div>
			    	@if( $errors->has('g-recaptcha-response'))<div class="error-label">*{{ $errors->first('g-recaptcha-response') }}</div> @endif
			  	</div>
			  	<div class="col-md-12">
				    <br>
				    <button type="submit" class="bt-bg continue">SUBMIT</button>
			  	</div>
        	</div>
	      	</form>
	      	<div class="modal fade" id="success" tabindex="-1" role="dialog" aria-labelledby="successLabel">
			  <div class="modal-dialog modal-sm" role="document">
			    <div class="modal-content">
			      <img src="{{ url('app/page/my-business-demo/images/success-icon-blue.svg') }}" alt="" class="success-img">
			      <h3>Thank you for your interest in RUSH!</h3>
			      <small>An account manager will get in touch with you within 3-5 business days.</small>
			      <br>
			      <br>
			      <a href="#" data-dismiss="modal">DONE</a>
			    </div>
			  </div>
			</div>
	    </div>
	  </div>
</section>
<footer>
	Copyright © RUSH GLOBE MyBusiness 2017. All Rights Reserved
</footer>
<script type="text/javascript">_satellite.pageBottom();</script>
</body>
<script src="{{ url('app/page/my-business-demo/components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ url('app/page/my-business-demo/components/bootstrap/dist/js/bootstrap.js') }}"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script>

    $(document).ready(function() {

        var checkboxes = $('#checkboxes');
        var checkoption = $("#checkboxes input[type='checkbox']");

        // Uncheck when Reload
        $('#checkboxes input[type="checkbox"]:checked').prop('checked', false);

        // Hide Dropdown
        checkboxes.slideUp();

        // Open Dropdown
        $('.overSelect').click(function() {
            checkboxes.slideToggle('fast');
        });

        // Count Objective
        $(checkoption).click(function() {
        	selectedCount = $('#checkboxes input[type="checkbox"]:checked').length;
            $(this).toggleClass('selected')

            if (!selectedCount) {
                $('.selectBox option:selected').text("Select Objective");
            }else{
	            $('.selectBox option:selected').text("Selected " + selectedCount);
            }
        });

        $('#checkboxes').click(function(e) {
            e.stopPropagation();
        });

        $('.overSelect').on('click', function(e){
            e.stopPropagation();
         })
        
        $(document.body).on('click', function(){
            $('#checkboxes').hide()
        })

        if($('.error-label').length){
        	$('html,body').animate({
			   scrollTop: $("#form-section").offset().top
			});
        }

        $('#request-appointment').on('keyup', 'input[name="mobile"]', function () { 
            this.value = this.value.replace(/[^0-9]/g,'');
        });
        
        @if(isset($input_success))
        	$('#success').modal('show')
        @endif
    });
	    
</script>
</html>