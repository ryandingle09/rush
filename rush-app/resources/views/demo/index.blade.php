<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Rush - Request a demo</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="{{ url('app/page/rush-demo/components/bootstrap/dist/css/bootstrap.css') }}">
	<link rel="stylesheet" href="{{ url('app/page/rush-demo/css/style.css') }}">
	<link rel="stylesheet" href="{{ url('app/page/rush-demo/components/chosen/chosen.css') }}">
	<link rel="stylesheet" href="{{ url('app/page/rush-demo/components/font-awesome/css/font-awesome.css')}}">
</head>
<body>
	<nav class="navbar">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="/">
					<img alt="Brand" src="app/page/rush-demo/images/rush_logo.png" width="80px">
				</a>
			</div>
		</div>
	</nav>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<h4>LOYALTY MADE EASY!</h4>
					<p>
						Build & foster long-lasting customer relationships with a RUSH-powered loyalty program.
					</p>
				</div>
				<div class="col-md-3 pull-right hidden-xs hidden-sm">
					<div class="envelope-imgs">
						<img src="{{ url('app/page/rush-demo/images/icons/mail.png') }}" class="small">
						<img src="{{ url('app/page/rush-demo/images/icons/mail.png') }}" class="medium">
					</div>
				</div>
			</div>
		</div>
	</section>
	<main >
		<div class="container">
			<div class="row">
				<div class="col-md-5 left-block">
					<h3>
						ENGAGE WITH YOUR CUSTOMERS THROUGH LOYALTY PROGRAMS
					</h3>
					<p>Create, design and manage your own customer loyalty program easily with RUSH. <br><br>Conveniently develop and deploy a custom-branded mobile app in as short as 3 days.</p>
					<br />
					<img src="{{ url('app/page/rush-demo/images/image.png') }}" alt="" width="100%" class="hidden-xs">
				</div>
				<img src="{{ url('app/page/rush-demo/images/image.png') }}" alt="" width="100%" class="visible-xs">
				@if( isset($message) && $message )
				<div class="col-md-6 col-xs-12 right-block pull-right">
					{!! $message !!}
				</div>
				@endif
				<div class="col-md-6 col-xs-12 right-block pull-right {{$form}}">
					<h3>
						LOYALTY MADE EASY! Fill up form to request a demo
					</h3>
					<form action="{{ Request::url() }}" method="post">
						{{ csrf_field() }}
						<label for="" style="display:block;">Contact Information</label>
						<input type="text" required placeholder="Firstname *" name="fname" class="form-control" value="{{ old('fname','') }}">
						@if( $errors->has('fname'))<span class="error-msg help-block">{{ $errors->first('fname') }}</span>@endif

						<input type="text" required placeholder="Lastname *" name="lname" class="form-control" value="{{ old('lname','') }}">
						@if( $errors->has('lname'))<span class="error-msg help-block">{{ $errors->first('lname') }}</span>@endif
						
						<div class="input-group" style="margin-bottom: 15px">
						<div class="input-group-addon">+63</div>
						<input style="margin-bottom:0;" type="text" required placeholder="Mobile Number * (9xxxxxxxxx) " name="mobile" class="form-control" maxlength="10" value="{{ old('mobile','') }}">
						</div>
						@if( $errors->has('mobile'))<span class="error-msg help-block">{{ $errors->first('mobile') }}</span>@endif

						<input type="email" required placeholder="Email Address *" name="email" class="form-control" value="{{ old('email','') }}">
						@if( $errors->has('email'))<span class="error-msg help-block">{{ $errors->first('email') }}</span>@endif

						<input type="text" required placeholder="Company Name *" name="companyname" class="form-control" value="{{ old('companyname','') }}">
						@if( $errors->has('companyname'))<span class="error-msg help-block">{{ $errors->first('companyname') }}</span>@endif
						
						<label for="" style="display:block;">Industry</label>
						{{ Form::select( 'industry[]', $industry_options, old('industry',null), ['id'=>'industry','data-placeholder'=>'Select Industry*','class'=>'form-control' ] ) }}
						@if( $errors->has('industry'))<span class="error-msg help-block">{{ $errors->first('industry') }}</span>@endif
						
						<label for="" style="display:block;">Loyalty Package</label>
						<div class="tix">
							{{ Form::checkbox('package[]', 'RUSH Basic (White Label Punch Card Program)', old('package', false), [ 'id'=>'a1' ] ) }}
							<label for="a1">RUSH Basic (White Label Punch Card Program)</label>
						</div>
						<div class="tix">
							{{ Form::checkbox('package[]', 'RUSH Pro (White Label Points Program)', old('package', false), [ 'id'=>'a2' ] ) }}
							<label for="a2">RUSH Pro (White Label Points Program)</label>
						</div>
						@if( $errors->has('package'))<span class="error-msg help-block">{{ $errors->first('package') }}</span>@endif

						<label for="" style="display:block;">Objective of the Loyalty Program</label>
						<div class="drp-down" id="objective">
							<button type="button" class="field form-control" >Select Objective</button>
							<div class="content">
								{{--*/ $i = 1 /*--}}
								@foreach($objective_options as $option)
								<div class="tix">
									{{ Form::checkbox('objective[]', $option, false, [ 'id'=> 'b'.$i ] ) }}
									<label for="b{{ $i }}">{{ $option }}</label>
								</div>
								{{--*/ $i++ /*--}}
								@endforeach
							</div>
						</div>
						@if( $errors->has('objective'))<span class="error-msg help-block">{{ $errors->first('objective') }}</span>@endif

						<textarea name="message" class="form-control" placeholder="Tell us more about your business (optional)">{{ old('message','') }}</textarea>
						@if( $errors->has('message'))<span class="error-msg help-block">{{ $errors->first('message') }}</span>@endif
                        
                        <div class="g-recaptcha" data-sitekey="6LeSJSkTAAAAAJqYJJczKaKyhU6G_0DXkRsZM_1-"></div>
                        <small class="text-danger hidden hidden-text">Please verify captcha</small>
                        @if( $errors->has('g-recaptcha-response'))<span class="error-msg help-block">{{ $errors->first('g-recaptcha-response') }}</span>@endif
						<div class="center">
							<button class="btn btn-round">REQUEST A DEMO</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</main>
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-10  col-sm-5 col-xs-7 footer-content">
					<img src="{{ url('app/page/rush-demo/images/rush_logo.png') }}" width="150px">
					<span>&copy; 2016 RUSH | All right reserved.</span>
				</div>
				<div class="col-md-2  col-sm-5 col-xs-5 footer-content pull-right">
					<label>FOLLOW US</label>
					<a href="https://www.facebook.com/rushrewardsph" class="social-icons"><i class="fa fa-facebook"></i></a>
					<a href="https://twitter.com/rushrewardsph" class="social-icons"><i class="fa fa-twitter"></i></a>
					<a href="https://www.instagram.com/rushrewardsph" class="social-icons"><i class="fa fa-instagram"></i></a>
				</div>
			</div>
		</div>
	</footer>
	<script src="{{ url('app/page/rush-demo//components/jquery/dist/jquery.min.js') }}"></script>
	<script src="{{ url('app/page/rush-demo//components/chosen/chosen.jquery.js') }}"></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<script>
		var allRadios = document.getElementsByName('package');
        var booRadio;
        var x = 0;
        for(x = 0; x < allRadios.length; x++){

            allRadios[x].onclick = function() {
                if(booRadio == this){
                    this.checked = false;
                    booRadio = null;
                }else{
                    booRadio = this;
                }
            };
        }

        $(document.body).on('click', function(){
        	$('.content').removeClass('open')
        })
        $('.content').click(function(e) {
	        e.stopPropagation();
	   	});
        $('.drp-down .field').on('click', function(e){
        	$(this).next('.content').toggleClass('open')
        	e.stopPropagation();
        })
        $('[name="objective[]"').on('change', function(){
        	var $checkboxes = $('#objective input[type="checkbox"]');
        	var countCheckedCheckboxes = $checkboxes.filter(':checked').length;
        	if(countCheckedCheckboxes !=0 ) {
        		$('.drp-down .field').addClass('active')
	        	$('#objective button').text(countCheckedCheckboxes + ' Selected');
        	}
        	else{
        		$('.drp-down .field').removeClass('active')
        		$('#objective button').text('Select Objective');
        	} 
        })
	    
	    $('form').on('submit', function (){
	        if($("#industry").val() == '' ) {
	          $('#industry_chosen').focus()
	          return false;
	        }
	        var googleResponse = jQuery('#g-recaptcha-response').val();
	        if (!googleResponse) {
	            $('.hidden-text').removeClass("hidden");
	            return false;
	        } else {
	            return true;
	        }
	        return true;
	    })
		$('select').on('change',function(){
			if($(this).val()!=""){
				$(this).css({'color':'#555'})
			}else {
				$(this).css({'color':'#dedede'})
			}
		})
	</script>
</body>
</html>
