@extends('layouts.app')

@section('headerPlaceholder')
   <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">-->
   <link rel="stylesheet" href="page/event-attendees/css/style.css">
@endsection

@section('view')
<div class="container">
    @include('flash.message')
    <div id="content" class="event-attendees-mgmt marginTop">
        <!-- Nav tabs -->
        <div class="navTabsWrap">
            <ul class="nav nav-tabs active" role="tablist" id="tabs">
                <li role="presentation"><a href="#attendees" aria-controls="attendees" role="tab" data-toggle="tab">CUSTOMER</a></li>
            </ul>
            <a href="#uploadAttendees" data-toggle="modal" class="addNew addAttendee" onclick="return false"><i class="fa fa-plus"></i> Upload Attendees</a>
            <a href="#addAttendee" data-toggle="modal" class="addNew addAttendee" onclick="return false"><i class="fa fa-plus"></i> Add Attendee</a>
        </div>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="attendees">
                <table id="attendeesTable" class="data-table management">
                    <thead>
                        <tr>
                            <th data-sortable="true">Date Registered</th>
                            <th data-sortable="true">Customer Name</th>
                            <th data-sortable="true">Mobile No.</th>
                            <th data-sortable="true">Email Address</th>
                            <th style="display:none;">uuid</th>
                            <th data-sortable="true">Confirm Attendance</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
<!--Add Attendee-->
<div id="addAttendee" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Add Attendee</div>
            <div class="modal-body">
                <form method="POST" action="{{ url('event-attendees/attendee') }}">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="attendeeName">Name</label>
                            <input type='text' class="form-control" name="attendeeName" id="attendeeName" pattern="[a-zA-Z\s]+" title="Name must be alphabets only" required>
                        </div>
                        <div class="col-md-12 group-field">
                            <label for="attendeeMobile">Mobile Number</label>
                            <div class="input-group">
                                <span class="input-group-addon">+63</span>
                                <input type="text" class="form-control" name="attendeeMobile" id="attendeeMobile" placeholder="9XXXXXXXXX" minlength="10" maxlength="10" pattern="[0-9]+" required title="Enter numbers only">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="attendeeEmail">Email</label>
                            <input type='email' class="form-control" name="attendeeEmail" id="attendeeEmail" required>
                        </div>
                        <div class="col-md-12">
                            <label for="attendeeAttandance">Confirm Attendance</label> &nbsp; <label><input type='checkbox' class="attendanceYes" name="attendeeAttendance" id="attendeeAttendance"> Yes</label>
                        </div>
                        <div class="col-md-12 margT20">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button type="submit" class="btn btn-primary pull-right">ADD</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Edit Attendee-->
<div id="editAttendee" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Edit Attendee</div>
            <div class="modal-body">
                <form method="POST" action="{{ url('event-attendees/attendee/update') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="e_attendee" id="e_attendee">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="e_attendeeName">Name</label>
                            <input type='text' class="form-control" name="e_attendeeName" id="e_attendeeName" pattern="[a-zA-Z\s]+" title="Name must be alphabets only" required>
                        </div>
                        <div class="col-md-12 group-field">
                            <label for="e_attendeeMobile">Mobile Number</label>
                            <div class="input-group">
                                <span class="input-group-addon">+63</span>
                                <input type="text" class="form-control" name="e_attendeeMobile" id="e_attendeeMobile" placeholder="9XXXXXXXXX" minlength="10" maxlength="10" pattern="[0-9]+" required title="Enter numbers only">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="e_attendeeEmail">Email</label>
                            <input type='email' class="form-control" name="e_attendeeEmail" id="e_attendeeEmail" required>
                        </div>
                        <div class="col-md-12">
                            <label for="e_attendeeAttandance">Confirm Attendance</label> &nbsp; <label><input type='checkbox' class="attendanceYes" name="e_attendeeAttendance" id="e_attendeeAttendance"> Yes</label>
                        </div>
                        <div class="col-md-12 margT20">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button type="submit" class="btn btn-primary pull-right">SAVE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Upload Attendee-->
<div id="uploadAttendees" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Add Attendees</div>
            <div class="modal-body">
                <form method="POST" action="{{ url('event-attendees/attendee/upload') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="attendeesData">Upload Attendees Data</label>
                        </div>
                        <div class="col-md-12">
                            <span class="btn btn-default btn-file">
                                Choose File
                                <input type="file" accept=".xlsx,.xls" name="attendeesData" required>
                            </span> &nbsp;
                            <span id="data_filename"></span>
                        </div>
                        <div class="col-md-12 margT20">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button type="submit" class="btn btn-primary pull-right">ADD</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Confirm Attendance-->
<div id="confirmAttendance" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Confirm Attendance</div>
            <div class="modal-body">
                <p>Confirm attendance for <span id="attendeeToConfirm"></span></p>
                <form method="POST" action="{{ url('event-attendees/attendee/confirm') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="c_attendee" id="c_attendee">
                    <div class="form-group row">
                        <div class="col-md-12 margT20">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button type="submit" class="btn btn-primary pull-right">CONFIRM</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Delete Attendance-->
<div id="deleteAttendee" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog expandWidth">
        <div class="modal-content modlogin">
            <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
            <div class="modal-header">Delete Attendee</div>
            <div class="modal-body">
                <p>Are you sure you want to delete this Attendee?</p>
                <span id="attendeeToDelete"></span>
                <form method="POST" action="{{ url('event-attendees/attendee/delete') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="attendee" id="attendee">
                    <div class="form-group row">
                        <div class="col-md-12 margT20">
                            <button class="btn btn-default" data-dismiss="modal" >CANCEL</button>
                            <button type="submit" class="btn btn-primary pull-right">DELETE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footerPlaceholder')
@parent
<script>
/*    $(document).ready(function(){
        $("#attendeesTable").dataTable({
            'order': [[ 0, 'desc' ]],
            'sDom': '<"fixed-table-toolbar"f><"excelButton hide"B><"customButton">rt<"bottom"ilp><"clear">',
            'buttons': [
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4 ]
                    },
                    oSelectorOpts: { filter: 'applied', order: 'current' }
                }
            ],
            'language': {
                'emptyTable': 'No Matching Records Found',
            },
            'oLanguage': {
                'sSearch': ''
            },
            'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']]
        });        
        $(".exportButton").click(function(){
            $('.buttons-excel').click();
        });
    });
    document.getElementById('attendeesData').onchange = function () {
        $("#data_filename").text(this.value.split('\\')[2]);
    };*/
</script>
@endsection