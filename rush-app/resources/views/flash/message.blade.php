<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info', 'debug'] as $msg)
    @if(Session::has('alert-'.$msg))
    @if($msg == 'debug')
    <pre class="alert alert-danger">{{Session::get('alert-'.$msg)}}</pre>
    @else
    <p class="alert alert-{{$msg}}">{{Session::get('alert-'.$msg)}}</p>
    @endif
    @endif
    @endforeach

    @if ($errors->has())
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
            {{ $error }}<br>
        @endforeach
    </div>
    @endif
</div>