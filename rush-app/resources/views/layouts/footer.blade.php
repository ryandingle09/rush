<script type="text/javascript">
/* <![CDATA[ */
    var RUSH = { base_url: '{!! url('/') !!}' };
/* ]]> */
</script>
<script type="text/javascript" src="{{ url('page/cms/js/jquery-1.11.3.min.js') }}"></script>
<script type="text/javascript" src="{{ url('page/cms/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ url('page/cms/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ url('page/cms/js/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ url('page/cms/js/buttons.html5.min.js') }}"></script>
<script type="text/javascript" src="{{ url('page/cms/js/lib/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ url('page/cms/js/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ url('page/cms/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ url('page/cms/js/bootstrap-table.min.js') }}"></script>
<script type="text/javascript" src="{{ url('page/cms/js/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ url('page/cms/js/d3.v3.min.js') }}"></script>
<script type="text/javascript" src="{{ url('page/cms/js/lib/jquery.fittext.js') }}"></script>
<script type="text/javascript" src="{{ url('page/cms/js/lib/jquery.activeNavigation.js') }}"></script>
<script type="text/javascript" src="{{ url('page/cms/js/lib/bootstrap-table-filter-control.js') }}"></script>
<script type="text/javascript" src="{{ url('page/cms/js/lib/spectrum.js') }}"></script>
<script type="text/javascript" src="{{ url('page/cms/js/lib/my.class.min.js') }}"></script>
<script type="text/javascript" src="{{ url('page/cms/js/lib/blur.min.js') }}"></script>
<script type="text/javascript" src="{{ url('page/cms/js/lib/jquery.ui.widget.js') }}"></script>
<script type="text/javascript" src="{{ url('page/cms/js/lib/load-image.all.min.js') }}"></script>
<script type="text/javascript" src="{{ url('page/cms/js/lib/jquery.fileupload.js') }}"></script>
<script type="text/javascript" src="{{ url('page/cms/js/lib/jquery.fileupload-process.js') }}"></script>
<script type="text/javascript" src="{{ url('page/cms/js/lib/jquery.fileupload-image.js') }}"></script>
<script type="text/javascript" src="{{ url('page/cms/js/djslo-1.0.0.js') }}" data-base_url="{{ url('/') }}/" data-quicksetup="{{ $quickstart or "none" }}" data-module="{{ $module or "none" }}"></script>
@section('footerPlaceholder')
<!-- Additional Footer items -->
@show

@if(getenv('APP_ENV') == 'production' || getenv('APP_ENV') == 'local')
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117476542-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
	  @if(getenv('APP_ENV') == 'production')
	  	gtag('config', 'UA-117491129-1');
	  @elseif(getenv('APP_ENV') == 'local')
	  	gtag('config', 'UA-117476542-1');
	  @endif
</script>
<script>
	var _merchantId = {{ session('user_id') }};
	var _merchantName = "{{ session('login_name') }}";
</script>
<script type="text/javascript" src="{{ url('page/cms/js/events-analytics.js') }}"></script>
@endif

</body>
</html>
