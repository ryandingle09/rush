@include('layouts.header')

{{-- */ $menu = isset($menu) ? $menu : 'menu.loyalty'; /*--}}
{{-- @include('citolaravel::' . $menu, ['serverUrl' => MigrationHelper::getServerUrl(), 'baseUrl' => MigrationHelper::getBaseUrl()]) --}}

{!! MenuHelper::menu( $menu ) !!}

@include('layouts.top')

@section('view')
@show

@include('layouts.bottom')

@if ( isset($quickstart_view) )
@include( $quickstart_view )
@section('quickstart_view')
@show
@endif

@section('modal')
@show

@if( isset($message) )
<div id="{{ $flash_data_type or 'flash_message_success' }}">
  {{ $message or '' }}
</div>
@endif

@include('layouts.footer')