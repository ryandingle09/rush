<div id="rightcontent">
  <div class="header test only">
      <div class="row">
          <div class="col-xs-8">
              <h3>{{ $header_text or "Merchant CMS" }} - {{ $program_name }}</h3>
          </div>
          <div class="col-xs-4">
              <div class="howdy">
                  <!-- Split button -->
                  <div class="btn-group">
                      <!-- Large button group -->
                      <div class="btn-group">
                          <button class="btn btn-default btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <img src="{{ MigrationHelper::getBaseUrl() }}assets/images/avatar.png" alt=""/> <span>Hi, {{ $login_name }}</span>
                              <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu">
                            @if ( session('user_type') == 1 || session('user_type') == 2 ) <li><a href="{{ url('changepass') }}">Change Password</a></li> @endif
                            <li><a href="{{ url('logout') }}">Logout</a></li>
                          </ul>
                      </div>

                  </div>
              </div>
          </div>
      </div>
  </div>
{{-- rightcontent div closing tags on admin/bottom.blade.php --}}
