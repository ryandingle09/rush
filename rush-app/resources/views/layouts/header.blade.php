<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="Fidelity" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{ $title or "CMS - RUSH" }}</title>

    <!-- fonts-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,300,300italic,400italic,600italic,700,700italic' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="{{ url('page/cms/css/bootstrap-table.min.css') }}">
    <link rel="stylesheet" href="{{ url('page/cms/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ url('page/cms/css/lib/spectrum.css') }}" />
    <link rel="stylesheet" href="{{ url('page/cms/css/bootstrap-datetimepicker.min.css') }}" />
    <link rel="stylesheet" href="{{ url('page/cms/css/lib/jquery.fileupload.css') }}" />

    <!-- DATATABLES -->
    <link rel="stylesheet" href="{{ url('page/cms/css/jquery.dataTables.css') }}" />
    <link rel="stylesheet" href="{{ url('page/cms/css/buttons.dataTables.min.css') }}">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{ url('page/cms/css/app.css') }}" />
    <!-- Global Overidding CSS -->
    <link rel="stylesheet" href="{{ url('css/override.css') }}" />

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script type="text/javascript" src="{{ url('page/cms/js/ie10-viewport-bug-workaround.js') }}"></script>
    @if(getenv('APP_ENV') == 'production')
    <script>
       (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
       (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
       m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
       })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

       ga('create', 'UA-87488508-1', 'auto');
       ga('send', 'pageview');
    </script>
    @endif
@section('headerPlaceholder')
<!-- Additional Header items -->
@show
</head>
<body>
