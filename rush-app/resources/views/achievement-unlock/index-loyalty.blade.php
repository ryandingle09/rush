@extends('layouts.app')

@section('headerPlaceholder')
    @parent
    <link rel="stylesheet" href="page/achievement-unlock/css/style.css">
@endsection

@section('view')
<div class="container">
    <div id="content" class="spacing setup-achievement-unlock">
        <div class="noTab">
            <div class="row">
                <div class="col-md-12">
                    <h4>Setup Member's Achievement</h4>
                    @if( session('status') )
                        <div class="alert alert-success" role="alert">
                            <p>{{ session('status') }}</p>
                        </div>
                    @endif
                    <form method="post" class="form-inline achievement-option-lists">
                        {{ csrf_field() }}
                        <div class="row fields">
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label class="control-label">
                                        <input type="checkbox" name="achievements[0][complete_profile][enabled]" class="option-enable" {{ isset($achievements['complete_profile']) &&  isset($achievements['complete_profile']->options->enabled) && $achievements['complete_profile']->options->enabled ?  'checked' : null }}> Member to complete profile
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12 pts-cont hide">
                                <div class="col-md-12">
                                    <div class="pts-wrap">
                                        <label class="control-label">Points = </label>
                                        <input type="text" class="form-control points numeric" name="achievements[0][complete_profile][points]" value="{{ isset($achievements['complete_profile']) &&  isset($achievements['complete_profile']->options->points) ?  $achievements['complete_profile']->options->points : null }}">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="achievements[0][complete_profile][repeat]" value="0">
                        </div>
                        <div class="row fields">
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="achievements[1][customer_app_activation][enabled]"  {{ isset($achievements['customer_app_activation']) &&  isset($achievements['customer_app_activation']->options->enabled) && $achievements['customer_app_activation']->options->enabled ?  'checked' : null }}> Member activation through customer app
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12 pts-cont hide">
                                <div class="col-md-12">
                                    <div class="pts-wrap">
                                        <label for="">Points = </label>
                                        <input type="text" class="form-control points numeric" name="achievements[1][customer_app_activation][points]" value="{{ isset($achievements['customer_app_activation']) &&  isset($achievements['customer_app_activation']->options->points) ?  $achievements['customer_app_activation']->options->points : null }}">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="achievements[1][customer_app_activation][repeat]" value="0">
                        </div>
                        <div class="row fields">
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="achievements[2][facebook_like][enabled]" {{ isset($achievements['facebook_like']) &&  isset($achievements['facebook_like']->options->enabled) && $achievements['facebook_like']->options->enabled ?  'checked' : null }}> Like us on Facebook
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12 pts-cont">
                                {{--<div class="col-md-3 hide">--}}
                                    {{--<div class="pts-wrap">--}}
                                        {{--<label>Points = </label>--}}
                                        {{--<input type="text" class="form-control points numeric" name="achievements[2][facebook_like][points]" value="{{ isset($achievements['facebook_like']) &&  isset($achievements['facebook_like']->options->points) ?  $achievements['facebook_like']->options->points : null }}">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                <div class="col-md-9">
                                    <div class="pts-wrap">
                                        <label class="control-label">FB Link =</label>
                                        <input type="text" class="form-control input-large" name="achievements[2][facebook_like][fb_page]" value="{{ isset($achievements['facebook_like']) &&  isset($achievements['facebook_like']->options->fb_page) ?  $achievements['facebook_like']->options->fb_page : null }}">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="achievements[2][facebook_like][repeat]" value="0">
                        </div>
                        <div class="row fields">
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="achievements[3][members_referral][enabled]" {{ isset($achievements['members_referral']) &&  isset($achievements['members_referral']->options->enabled) && $achievements['members_referral']->options->enabled ?  'checked' : null }}> Member's invite/referral
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12 pts-cont hide">
                                <div class="col-md-4">
                                    <div class="pts-wrap">
                                        <label>Referror Points = </label>
                                        <input type="text" class="form-control points numeric" name="achievements[3][members_referral][points]" value="{{ isset($achievements['members_referral']) &&  isset($achievements['members_referral']->options->points) ?  $achievements['members_referral']->options->points : null }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="pts-wrap">
                                        <label>Referee Points = </label>
                                        <input type="text" class="form-control points numeric" name="achievements[3][members_referral][referee_points]" value="{{ isset($achievements['members_referral']) &&  isset($achievements['members_referral']->options->referee_points) ?  $achievements['members_referral']->options->referee_points : 0 }}">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="achievements[3][members_referral][repeat]" value="1">
                        </div>
                        <div class="row fields">
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="achievements[4][transaction_no_with_minimum_amount][enabled]" class="numeric" {{ isset($achievements['transaction_no_with_minimum_amount']) &&  isset($achievements['transaction_no_with_minimum_amount']->options->enabled) && $achievements['transaction_no_with_minimum_amount']->options->enabled ?  'checked' : null }}> First x transaction minimum amount of x
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12 pts-cont hide">
                                <div class="col-md-4">
                                    <div class="pts-wrap">
                                        <label>Points = </label>
                                        <input type="text" class="form-control points numeric" name="achievements[4][transaction_no_with_minimum_amount][points]" value="{{ isset($achievements['transaction_no_with_minimum_amount']) &&  isset($achievements['transaction_no_with_minimum_amount']->options->points) ?  $achievements['transaction_no_with_minimum_amount']->options->points : null }}" >
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="pts-wrap">
                                        <label class="control-label"># Transactions = </label>
                                        <input type="text" name="achievements[4][transaction_no_with_minimum_amount][transaction_no]" class="form-control input-mini numeric" value="{{ isset($achievements['transaction_no_with_minimum_amount']) &&  isset($achievements['transaction_no_with_minimum_amount']->options->transaction_no) ?  $achievements['transaction_no_with_minimum_amount']->options->transaction_no : null }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="pts-wrap">
                                        <label class="control-label">Minimum Amount = </label>
                                        <input type="text" name="achievements[4][transaction_no_with_minimum_amount][amount]" class="form-control input-mini numeric" value="{{ isset($achievements['transaction_no_with_minimum_amount']) &&  isset($achievements['transaction_no_with_minimum_amount']->options->amount) ?  $achievements['transaction_no_with_minimum_amount']->options->amount : null }}">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="achievements[4][transaction_no_with_minimum_amount][repeat]" value="0">
                        </div>
                        <div class="row fields">
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="achievements[5][redemption_no_with_minimum_points][enabled]" {{ isset($achievements['redemption_no_with_minimum_points']) &&  isset($achievements['redemption_no_with_minimum_points']->options->enabled) && $achievements['redemption_no_with_minimum_points']->options->enabled ?  'checked' : null }}> First x number of redemptions
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12 pts-cont hide">
                                <div class="col-sm-4">
                                    <div class="pts-wrap">
                                        <label>Points = </label>
                                        <input type="text" class="form-control points numeric" name="achievements[5][redemption_no_with_minimum_points][points]" value="{{ isset($achievements['redemption_no_with_minimum_points']) &&  isset($achievements['redemption_no_with_minimum_points']->options->points) ?  $achievements['redemption_no_with_minimum_points']->options->points : null }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="pts-wrap">
                                        <label for="" class="control-label">Redemptions Points = </label>
                                        <input type="text" class="input-mini form-control numeric" name="achievements[5][redemption_no_with_minimum_points][redeemed_points]"  value="{{ isset($achievements['redemption_no_with_minimum_points']) &&  isset($achievements['redemption_no_with_minimum_points']->options->redeemed_points) ?  $achievements['redemption_no_with_minimum_points']->options->redeemed_points : null }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="pts-wrap">
                                        <label for="" class="control-label"># of Redemptions = </label>
                                        <input type="text" class="input-mini form-control numeric" name="achievements[5][redemption_no_with_minimum_points][redemption_no]" value="{{ isset($achievements['redemption_no_with_minimum_points']) &&  isset($achievements['redemption_no_with_minimum_points']->options->redemption_no) ?  $achievements['redemption_no_with_minimum_points']->options->redemption_no : null }}">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="achievements[5][redemption_no_with_minimum_points][repeat]" value="0">
                        </div>
                        <div class="row fields">
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="numeric" name="achievements[6][purchase_no_within_year][enabled]" {{ isset($achievements['purchase_no_within_year']) &&  isset($achievements['purchase_no_within_year']->options->enabled) && $achievements['purchase_no_within_year']->options->enabled ?  'checked' : null }}> Total x amount of purchases within x year(s)
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12 pts-cont hide">
                                <div class="col-md-4">
                                    <div class="pts-wrap">
                                        <label>Points = </label>
                                        <input type="text" class="form-control points numeric" name="achievements[6][purchase_no_within_year][points]" value="{{ isset($achievements['purchase_no_within_year']) &&  isset($achievements['purchase_no_within_year']->options->points) ?  $achievements['purchase_no_within_year']->options->points : null }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="pts-wrap">
                                        <label for="" class="control-label">Total Amount = </label>
                                        <input type="text" class="input-mini form-control numeric" name="achievements[6][purchase_no_within_year][amount]" value="{{ isset($achievements['purchase_no_within_year']) &&  isset($achievements['purchase_no_within_year']->options->amount) ?  $achievements['purchase_no_within_year']->options->amount : null }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="pts-wrap">
                                        <label for="" class="control-label"># of Year(s) = </label>
                                        <input type="text" class="input-mini form-control numeric" name="achievements[6][purchase_no_within_year][year]" value="{{ isset($achievements['purchase_no_within_year']) &&  isset($achievements['purchase_no_within_year']->options->year) ?  $achievements['purchase_no_within_year']->options->year : null }}">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="achievements[6][purchase_no_within_year][repeat]" value="0">
                        </div>
                        <div class="row fields">
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="achievements[7][special_day_purchase][enabled]" {{ isset($achievements['special_day_purchase']) &&  isset($achievements['special_day_purchase']->options->enabled) && $achievements['special_day_purchase']->options->enabled ?  'checked' : null }}> Special day of purchase
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12 pts-cont hide">
                                <div class="col-md-3">
                                    <div class="pts-wrap">
                                        <label>Points = </label>
                                        <input type="text" class="form-control points numeric" name="achievements[7][special_day_purchase][points]"  value="{{ isset($achievements['special_day_purchase']) &&  isset($achievements['special_day_purchase']->options->points) ?  $achievements['special_day_purchase']->options->points : null }}">
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="pts-wrap">
                                        <label class="control-label">Date = </label>
                                        <div class='input-group date datetimepicker input-medium'>
                                            <input type='text' class="form-control" name="achievements[7][special_day_purchase][date]" placeholder="Select Date"  data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="{{ isset($achievements['special_day_purchase']) &&  isset($achievements['special_day_purchase']->options->date) ?  $achievements['special_day_purchase']->options->date : null }}">
                                            <span class="input-group-addon out"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="achievements[7][special_day_purchase][repeat]" value="1">
                        </div>
                        <div class="row fields">
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="achievements[8][double_points][enabled]" {{ isset($achievements['double_points']) &&  isset($achievements['double_points']->options->enabled) && $achievements['double_points']->options->enabled ?  'checked' : null }}> Double Points
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12 pts-cont hide">
                                <div class="col-md-12">
                                    <div class="pts-wrap">
                                        <label>Date = </label>
                                        <div class='input-group date datetimepicker input-medium'>
                                            <input type='text' class="form-control" name="achievements[8][double_points][date]" placeholder="Select Date"  data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="{{ isset($achievements['double_points']) &&  isset($achievements['double_points']->options->date) ?  $achievements['double_points']->options->date : null }}">
                                            <span class="input-group-addon out"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="achievements[8][double_points][repeat]" value="1">
                        </div>
                        <div class="row fields">
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="achievements[9][attended_event][enabled]" {{ isset($achievements['attended_event']) &&  isset($achievements['attended_event']->options->enabled) && $achievements['attended_event']->options->enabled ?  'checked' : null }}> Attended event
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-12 pts-cont wbg hide">
                                    <div class="col-md-4">
                                        <div class="pts-wrap nobg">
                                            <label>Points = </label>
                                            <input type="text" class="form-control points numeric" name="achievements[9][attended_event][points]"  value="{{ isset($achievements['attended_event']) &&  isset($achievements['attended_event']->options->points) ?  $achievements['attended_event']->options->points : null }}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="pts-wrap nobg">
                                            <label>Name = </label>
                                            <input type='text' placeholder="Name" class="form-control input-medium" name="achievements[9][attended_event][name]" value="{{ isset($achievements['attended_event']) &&  isset($achievements['attended_event']->options->name) ?  $achievements['attended_event']->options->name : null }}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="pts-wrap nobg">
                                            <label>Date = </label>
                                            <div class='input-group date datetimepicker input-medium'>
                                                <input type='text' class="form-control" name="achievements[9][attended_event][date]" placeholder="Select Date"  data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="{{ isset($achievements['attended_event']) &&  isset($achievements['attended_event']->options->date) ?  $achievements['attended_event']->options->date : null }}">
                                                <span class="input-group-addon out"><i class="fa fa-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="pts-wrap nobg">
                                            <div class="form-group">
                                                <label class="control-label">Customer CSV file = </label>
                                                <input type="file" class="form-control input-file-csv " placeholder="Select file">
                                                @if(isset($achievements['attended_event']) && isset($achievements['attended_event']->options->customers) && $achievements['attended_event']->options->customers )
                                                <span>No. of Customers: {{ count(explode("\r\n",$achievements['attended_event']->options->customers)) }}</span>
                                                <button class="give-points btn btn-primary" data-attr-achievement_name="attended_event" ><i class='fa fa-circle-o-notch fa-spin'></i> Give Points</button>
                                                @endif
                                                <input type="hidden" name="achievements[9][attended_event][customers]" value="{{ isset($achievements['attended_event']) && isset($achievements['attended_event']->options->customers) ? $achievements['attended_event']->options->customers : null }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="achievements[9][attended_event][repeat]" value="1">
                        </div>
                        <div class="row fields">
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="achievements[10][answering_survey][enabled]" {{ isset($achievements['answering_survey']) &&  isset($achievements['answering_survey']->options->enabled) && $achievements['answering_survey']->options->enabled ?  'checked' : null }}> Answering Survey
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-12 pts-cont wbg hide">
                                    <div class="col-md-4">
                                        <div class="pts-wrap nobg">
                                            <label>Points = </label>
                                            <input type="text" class="form-control points numeric" name="achievements[10][answering_survey][points]"  value="{{ isset($achievements['answering_survey']) &&  isset($achievements['answering_survey']->options->points) ?  $achievements['answering_survey']->options->points : null }}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="pts-wrap nobg">
                                            <label>Name = </label>
                                            <input type='text' placeholder="Name" class="form-control input-medium" name="achievements[10][answering_survey][name]" value="{{ isset($achievements['answering_survey']) &&  isset($achievements['answering_survey']->options->name) ?  $achievements['answering_survey']->options->name : null }}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="pts-wrap nobg">
                                            <label>Date = </label>
                                            <div class='input-group date datetimepicker input-medium'>
                                                <input type='text' class="form-control" name="achievements[10][answering_survey][date]" placeholder="Select Date"  data-error="Invalid date value" pattern="^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$" value="{{ isset($achievements['answering_survey']) &&  isset($achievements['answering_survey']->options->date) ?  $achievements['answering_survey']->options->date : null }}">
                                                <span class="input-group-addon out"><i class="fa fa-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="pts-wrap nobg">
                                            <div class="form-group">
                                                <label class="control-label">Customer CSV file = </label>
                                                <input type='file' class="form-control input-file-csv" placeholder="Select file">
                                                @if(isset($achievements['answering_survey']) && isset($achievements['answering_survey']->options->customers) && $achievements['answering_survey']->options->customers)
                                                <span>No. of Customers: {{ count(explode("\r\n",$achievements['answering_survey']->options->customers)) }}</span>
                                                <button class="give-points btn btn-primary" data-attr-achievement_name="answering_survey"><i class='fa fa-circle-o-notch fa-spin'></i> Give Points</button>
                                                @endif
                                                <input type="hidden" name="achievements[10][answering_survey][customers]" value="{{ isset($achievements['answering_survey']) && isset($achievements['answering_survey']->options->customers) ? $achievements['answering_survey']->options->customers : null }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="achievements[10][answering_survey][repeat]" value="1">
                            </div>
                        </div>
                        @if($is_enable_transaction_method)
                        <div class="row fields">
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="achievements[11][gcash_transaction][enabled]" {{ isset($achievements['gcash_transaction']) &&  isset($achievements['gcash_transaction']->options->enabled) && $achievements['gcash_transaction']->options->enabled ?  'checked' : null }}> GCash Transaction
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12 pts-cont">
                                <div class="col-md-9">
                                    <div class="pts-wrap">
                                        <label class="control-label">Points Multiplier =</label>
                                        <input type="text" class="form-control input-large" name="achievements[11][gcash_transaction][multiplier]" value="{{ isset($achievements['gcash_transaction']) &&  isset($achievements['gcash_transaction']->options->multiplier) ?  $achievements['gcash_transaction']->options->multiplier : null }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        <!-- </div> -->
                        <div class="actionButton">
                            <button class="btn btn-default resetButton" type="reset">Reset</button>
                            <button class="btn btn-primary saveButton" type="submit">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> <!-- /container -->
@endsection

@section('footerPlaceholder')
    @parent
    <script type="text/javascript" src="{{ url('page/achievement-unlock/js/scripts.js') }}"></script>
@endsection
