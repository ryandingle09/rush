@extends('layouts.app')

@section('view')
<div class="container">

        <div id="content" class="management spacing account">
            <h4>Change Password</h4>
            <form action="{{ url('changepass') }}" id="changePassword" method="post">
            {{ csrf_field() }}
              <div class="row">
                <div class="col-sm-5">
                      <div class="form-group">
                          <label for="old_password">Old Password *</label>
                          <input type="password" class="form-control" name="old_password" id="old_password" maxlength="32">
                      </div>
                      <div class="form-group">
                          <label for="new_password">New Password *</label>
                          <input type="password" class="form-control" name="new_password" id="new_password" maxlength="32">
                      </div>
                      <div class="form-group">
                          <label for="confirm_password">Confirm New Password *</label>
                          <input type="password" class="form-control" name="confirm_password" id="confirm_password" maxlength="32">
                      </div>
                </div>
                <div class="col-md-12">
                  <button class="btn btn-primary updateProfile" type="submit">Update</button>
                </div>
              </div>
              </form>


        </div><!--/content-->
    </div> <!-- /container -->
@endsection
