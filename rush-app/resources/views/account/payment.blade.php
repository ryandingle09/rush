@extends('layouts.app')

@section('view')
<div class="container">
    <div id="content" class="billing container">
      <form id="frm_payt_otc" action="{{ MigrationHelper::getAppBaseUrl() }}billing/payment" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
          <div class="col-lg-12 title">
            <h2><a href="{{ MigrationHelper::getAppBaseUrl() }}billing">Account Summary</a> &gt; Payment</h2>
            <hr/>
          </div>
        </div>



        <div class="row">
          <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <label class="bill_label">Account Number</label>
            <p><input type="text" name="acctNumber" class="form-control bill_value col-sm-12 col-xs-12" value="{{ $merchant->account_number }}" readonly></p>
          </div>
          <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <label class="bill_label">Account Name</label>
            <p><input type="text" name="acctName" class="form-control bill_value col-sm-12 col-xs-12" value="{{ $merchant->businessName }}" readonly></p>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <label class="bill_label">Payment Method</label>
            <p>
            {{ Form::select('paytMethod', [ 'cash' => 'Cash', 'credit_card' => 'Credit Card', 'check' => 'Check', 'bill' => 'Charge to Bill'], null, [ 'id' => 'paytMethod', 'class' => 'form-control col-sm-12 col-xs-12', 'required' => true]) }}
            </p>
          </div>
          <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <label class="bill_label">RUSH Solution</label>
            <p><input type="text" name="rushSoln" class="form-control bill_value col-sm-12 col-xs-12" value="{{ $merchant->package->name }}({{ $merchant->package->Category }})" readonly></p>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 otc-fields">
            <p><label for="payment_date" class="bill_label">Payment Date</label></p>
            <p><input type="text" name="payment_date" id="payment_date" value="{{ old('payment_date') }}" class="form-control col-sm-12 col-xs-12" style="text-align:center;" placeholder="mm/dd/yyyy"/></p>
          </div>
          <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <p><label for="bill_amt"  class="bill_label">Amount</label></p>
            <p><input type="number" name="amount" id="bill_amt" step="0.01" placeholder="0.00" value="{{ old('amount','0.00') }}" class="form-control col-sm-12 col-xs-12"/></p>
          </div>
        </div>
        <div class="row otc-fields">
          <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <p><label for="bill_ref_no" class="bill_label">Reference Number</label></p>
            <p><input type="text" name="refNo" id="bill_ref_no" step="0.01" class="form-control col-sm-12 col-xs-12" required value="{{ old('refNo') }}" /></p>
          </div>
          <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <p><label class="bill_label">Upload Deposit Slip</label></p>
            <p>
              <label for="bill_upload_btn" id="custom_upload_btn">
              <input type="file" id="bill_upload_btn" name="upload" class="form-control col-sm-12 col-xs-12" accept="image/*" required />
              <span>Browse</span>
              </label>
              <img id="image" style="max-width: 70px;max-height:50px;" class="otc-fields" />
            </p>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6">
            <div class="row otc-fields">
              <div class="col-lg-12 title">
                <p class="italic">
                  Payment may be deposited at any BPI branch with the following details:<br/><br />
                  <b>Bank:</b> BPI<br/>
                  <b>Account Name:</b> Globe Telecom<br />
                  <b>Bank Account No:</b> 1891004601<br />
                  <b>Bank Account Type:</b> Current<br />
                  <br />
                  <b>Reference Number Format:</b> PNC/PR23/YYYYMMDD<br />
                  <i><Year of Deposit><Month of Deposit><Date of Deposit><br />
                  Example:* If payment is deposited on Feb 20, 2016 reference number will be: PNC/PR23/20160220</i>
                  <br /><br />
                  Check payments must be payable to "Globe Telecom". At the back of the check, write RUSH account number, account name, authorized contact person, and telephone number.
                  <br /><br />
                  Deposits via check from other issuing banks must be paid at least 3 days before due date for clearing.<br />
                  Deposit slip should be uploaded into your Web Dashboard or emailed to billing@rush.ph once payment has been made.
                  <br /><br />
                </p>
              </div>
            </div>
          </div>
          <div class="form-group col-lg-6 col-md-6">
            <input type="hidden" name="billing_id" value="{{ $billing->id }}" />
            <br><input type="submit" id="submit_otc" class="bill_btn col-lg-7 col-md-7 col-sm-12 col-xs-12" value="Submit Payment &rarr;">
          </div>
        </div>
      </form>
    </div>
</div> <!-- /container -->
@endsection

@section('modal')
<!--  MODAL POP UP -->
<div id="paymentModalOTC" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="bill_popup modal-body">
            <h2>Thank You!</h2>
            <p>Your payment details has been successfully submitted! Please wait at least seven (7) working days for your payment confirmation.</p>
            <p>Your official Receipt will become available once payment is confirmed.</p><br>
            <center><span class="span_cc_btn"><button id="paymentModalOTCButtonOkay">Okay</button></span></center>
          </div>
        </div>
    </div>
</div>
<div id="paymentModalCC" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="bill_popup modal-body">
            <h2>Thank You!</h2>
            <p>Your payment details has been successfully submitted! Please wait at least seven (7) working days for your payment confirmation.</p>
            <p>Your official Receipt will become available once payment is confirmed.</p><br>
            <center><span class="span_cc_btn"><button id="paymentModalCCButtonOkay">Okay</button></span></center>
          </div>
        </div>
    </div>
</div>
<!--  END MODAL POP UP -->
@endsection

@section('footerPlaceholder')
@parent
<script type="text/javascript">

    // Get the modal
    var modal = document.getElementById('paymentModalOTC');

    /*Get billing form values, submit it then display modal.*/
    $(document).ready(function() {

      $('#payment_date').datetimepicker({
        format: 'MM-DD-YYYY'
      });

      $("#bill_ref_no").keypress(function (e) {
          var regex = new RegExp("^[a-zA-Z0-9]+$");
          var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
          if (regex.test(str)) {
              return true;
          }
          e.preventDefault();
          return false;
      });

        $("#submit_otc").click(function(e) {
            e.preventDefault();
            var _paymentMethod = $("#paytMethod").val();
            if ( _paymentMethod == 'credit_card' ) {
              var _url = $("#frm_payt_otc").attr("action");
              var _data = {
                paytMethod : $('#paytMethod').val(),
                billing_id : $('input[name="billing_id"]').val(),
                _token : $('input[name="_token"]').val(),
                payment_date : $('input[name="payment_date"]').val(),
                amount : $('input[name="amount"]').val()
              }
              $.post( _url, _data )
                  .done(function( data ) {
                  $form = $("<form></form>");
                  $form.attr('action', '{{ $payment_url }}');
                  $form.attr('method', 'POST');
                  $form.append('<input name="paymentrequest" type="hidden" value="'+data.paymentrequest+'">');
                  $('body').append($form);
                  $form.submit();
                });
            } else {
              var _filled = 0;

              $("select, input").each(function() {
                 if ( $(this).val() == "" || $(this).val() == null ) {
                   _filled--;
                   if ( $(this).attr('name') == "upload" ) {
                    $("#custom_upload_btn").css('border-color', 'red');
                    $("#custom_upload_btn").css('color', 'red');
                   } else {
                     $(this).focus();
                   }
                   return false;
                 }
              });

              if ( _filled >= 0 ) {
                $("#paymentModalOTC").css('display', "block");
              }
            }
        });

        document.getElementById("bill_upload_btn").onchange = function () {
            var reader = new FileReader();
            reader.onload = function (e) {
                // get loaded data and render thumbnail.
                document.getElementById("image").src = e.target.result;
            };
            // read the image file as a data URL.
            reader.readAsDataURL(this.files[0]);
        };

    });

    // When the user clicks on <span>, close the modal
    $('#paymentModalOTCButtonOkay').click( function() {
        $("#paymentModalOTC").css('display', "none");
        $("#frm_payt_otc").submit();
    });

    // When the user clicks on <span>, close the modal
    $('#paymentModalCCButtonOkay').click( function() {
        $("#paymentModalCC").css('display', "none");
    });

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            $("#paymentModalOTC").css('display', "none");
            $("#paymentModalCC").css('display', "none");
        }
    }

    $("#paytMethod").change( function() {
      if ( $(this).val() == 'credit_card' ) {
        $(".otc-fields").css('display', "none");
      } else {
        $(".otc-fields").css('display', "block");
      }
    });
</script>
@endsection
