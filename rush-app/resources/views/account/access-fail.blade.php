@extends('layouts.app')

@section('view')
<div class="container">
		<div id="content" class="view-billing">
    <div class="row">
      <div class="col-lg-12">
        <br />
        <p>You have insufficient privilege for accessing this data.</p>
        <p>You will be redirected in a while (or <a href="{{ MigrationHelper::getAppBaseUrl() }}billing">click here</a>)</p>
      </div>
    </div>
	  </div>
</div> <!-- /container -->
@endsection

@section('footerPlaceholder')
@parent
<script type="text/javascript">
$(document).ready( function() {
  setTimeout(function() {
    window.location.href = "{{ MigrationHelper::getAppBaseUrl() }}account";
  }, 3000);
});
</script>
@endsection
