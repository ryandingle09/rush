@extends('layouts.app')

@section('view')
<div class="container">
    <div id="content">
      <!-- Nav tabs -->
      <div class="navTabsWrap">
          <ul class="nav nav-tabs" role="tablist" id="tabs">
              <li role="presentation" class="active"><a href="#account" aria-controls="account" role="tab" data-toggle="tab">ACCOUNT</a></li>
              <li role="presentation"><a href="#payment-history" aria-controls="payment-history" role="tab" data-toggle="tab">PAYMENT HISTORY</a></li>
              <li role="presentation"><a href="#billing-history" aria-controls="billing-history" role="tab" data-toggle="tab">BILLING HISTORY</a></li>
          </ul>
      </div>
      <!-- Tab panes -->
      <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="account">
            <div class="noTab account-tab">
                <div class="row">
                    <div class="col-md-12" id="account-summary-table">
                        <h4>Account Summary</h4>
                        <table data-toggle="table" class="data-table account" data-pagination="false" data-search="false">
                            <thead>
                                <tr>
                                    <th class="text-center">Account Number</th>
                                    <th class="text-left">Business Name</th>
                                    <th class="text-left">RUSH Solution</th>
                                    <th class="text-right">Oustanding Balance</th>
                                    <th class="text-left">As of</th>
                                    <th class="text-left">Add-ons</th>
                                    <th class="text-left">Status</th>
                                    <th class="text-left">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                              @if ( !empty($billing) )
                                <tr>
                                    <td class="text-center">{{ $billing->account_number }}</td>
                                    <td class="text-left">{{ $billing->business_name }}</td>
                                    <td class="text-left">{{ $billing->package->name }}({{ $billing->status }})</td>
                                    <td class="text-right">{{ number_format( $billing->total_amount, 2, ".", ",") }}</td>
                                    <td class="text-left">{{ Carbon\Carbon::parse($billing->statement_date)->format('F j, Y h:i:s A') }}</td>
                                    <td class="text-left">
                                      @foreach ( $merchant->add_ons as $add_ons)
                                      @if ( $add_ons->id != 3 )
                                        <p>{{ $add_ons->name }}</p>
                                      @endif
                                      @endforeach
                                    </td>
                                    <td class="text-left">{{ $billing->status }}</td>
                                    <td class="text-left">
                                      <a href="{{ MigrationHelper::getAppBaseUrl() }}billing/view/{{ $billing->id}}/billing" data-toggle="modal" class="edit">View Bill</a>
                                    </td>
                                </tr>
                              @else
                              <tr>
                                  <td class="text-center">{{ $merchant->account_number }}</td>
                                  <td class="text-left">{{ $merchant->businessName }}</td>
                                  <td class="text-left">{{ $merchant->package->name }}({{ $merchant->status }})</td>
                                  <td class="text-right">0.00</td>
                                  <td class="text-left">{{ Carbon\Carbon::now()->format('F j, Y h:i:s A') }}</td>
                                  <td class="text-left">
                                    @foreach ( $merchant->add_ons as $add_ons)
                                      <p>{{ $add_ons->name }}</p>
                                    @endforeach
                                  </td>
                                  <td class="text-left">{{ $merchant->status }}</td>
                                  <td class="text-left">
                                    <a href="#" id="view_account" data-toggle="modal" class="edit">View Account</a>
                                  </td>
                              </tr>
                              @endif
                            </tbody>
                        </table>
                      </div>
                        @if ( empty($billing) )
                      <div class="col-md-12" id="view-account-area">
                        <div class="row">
                          <div class="col-lg-12 title">
                            <h4><a href="#" id="account-summary-link">Account Summary</a> &gt; View Account</h4>
                            <hr/>
                          </div>
                        </div>
                        <div class="view-bill-upper">
              						<div class="row">
              							<div class="col-md-3"><label for="view_account_number">Account Number</label></div>
              							<div class="col-md-3"><p>{{$merchant->account_number}}</p></div>
              							<div class="col-md-3"><label for="view_account_number">Oustanding Balance</label></div>
              							<div class="col-md-3"><p>0.00</p></div>
              						</div>
              						<div class="row">
              							<div class="col-md-3"><label for="view_businessName">Business Name</label></div>
              							<div class="col-md-3"><p>{{$merchant->businessName}}</p></div>
              							<div class="col-md-3"><label for="view_businessName">As of</label></div>
              							<div class="col-md-3"><p class="form-control-static">{{ Carbon\Carbon::now()->format('F j, Y h:i:s A') }}</p></div>
              						</div>
              						<div class="row">
              							<div class="col-md-3"><label for="view_businessType">Business Type</label></div>
              							<div class="col-md-3">
              								<p>
              								@foreach ($merchant->business_type_name as $business_type)
              									{{ $business_type->name }} <br />
              								@endforeach
              								</p>
              							</div>
              							<div class="col-md-3"><label for="view_businessName">Branches:</label></div>
              							<div class="col-md-3"><p>{{ $merchant->active_branches_count }}</p></div>
              						</div>
              						<div class="row">
              							<div class="col-md-3"><label for="view_businessType">Location</label></div>
              							<div class="col-md-3"><p>{{$merchant->address}}</p></div>
              							<div class="col-md-3"><label for="view_businessType">RUSH Solution</label></div>
              							<div class="col-md-3"><p>{{ $merchant->package->name }}({{ $merchant->status }})</p></div>
              						</div>
              						<div class="row">
              							<div class="col-md-3"><label for="view_businessType">Authorized Rep</label></div>
              							<div class="col-md-3"><p>{{$merchant->contactPerson}}</p></div>
              							<div class="col-md-3"><label for="view_businessType">Contact Number</label></div>
              							<div class="col-md-3"><p>{{$merchant->contactPersonNumber}}</p></div>
              						</div>
              				</div>
                      <div class="row" style="margin-top: 10px;padding: 15px;">
                        <div class="col-md-12" style="background-color:#F2F4F6;font-weight:bold;padding: 5px;">
                          No billing records found.
                        </div>
                      </div>
                    </div>
                    @endif
                </div>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="payment-history">
            <div class="noTab">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Payment History</h4>
                        <table data-toggle="table" class="data-table account" data-pagination="false" data-search="false">
                            <thead>
                                <tr>
                                    <th class="text-left">Reference Number</th>
                                    <th class="text-left">Payment Date</th>
                                    <th class="text-right">Amount</th>
                                    <th class="text-center">View Details</th>
                                    <th class="text-left">Payment Mode</th>
                                    <th class="text-left">OR</th>
                                </tr>
                            </thead>
                            <tbody>
                              @foreach ($payments as $payment)
                              <tr>
                                  <td class="text-left">{{ $payment->reference_number }}</td>
                                  <td class="text-left">{{ Carbon\Carbon::parse($payment->payment_date)->format('F j, Y') }}</td>
                                  <td class="text-right">{{ number_format( $payment->payment_amount, 2, ".", ",") }}</td>
                                  <td class="text-center">
                                    @if ( $payment->deposit_slip && file_exists( public_path() . "/../../repository/merchant/slip/" . $payment->deposit_slip ) )
                                      <a href="#" class="pop">
                                        <img src="{{ MigrationHelper::getServerUrl() }}repository/merchant/slip/{{ $payment->deposit_slip }}" style="width: 20px; height: 20px;">
                                      </a>
                                    @endif
                                  </td>
                                  <td class="text-left">{{ $payment->payment_type }}</td>
                                  <td class="text-left">{{ $payment->or_number }}</td>
                              </tr>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="billing-history">
            <div class="noTab">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Billing History</h4>
                        <table data-toggle="table" class="data-table account" data-pagination="false" data-search="false">
                            <thead>
                                <tr>
                                    <th class="text-left">Bill Number </th>
                                    <th class="text-left">Billing Period</th>
                                    <th class="text-left">Statement Date</th>
                                    <th class="text-left">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                              @foreach ($billing_history as $billing_data)
                              <tr>
                                  <td class="text-left">{{ $billing_data->merchant_id . '-' . $billing_data->billing_number }}</td>
                                  <td class="text-left">{{ Carbon\Carbon::parse($billing_data->billing_period_start)->format('F j') }} - {{ Carbon\Carbon::parse($billing_data->billing_period_end)->format('F j, Y') }}</td>
                                  <td class="text-left">{{ Carbon\Carbon::parse($billing_data->statement_date)->format('F j, Y') }}</td>
                                  <td class="text-left">
                                    <a class="btn btn-primary view-bill-button" data-id="{{ $billing_data->id}}" href="{{ MigrationHelper::getAppBaseUrl() }}billing/view/{{ $billing_data->id}}/billing">View Bill</a>
                                    <a class="btn btn-primary download-pdf-button" data-id="{{ $billing_data->id}}" href="{{ MigrationHelper::getAppBaseUrl() }}billing/statement/{{$billing_data->id}}" target="_new">Download PDF</a>
                                  </td>
                              </tr>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
          </div>
      </div>
    </div>
</div> <!-- /container -->
@endsection

@section('modal')
<!-- MODALS -->
<div id="imagemodal" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <span class="ex" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></span>
      <div class="modal-body">
        <div class="form-group">
      	   <img src="" class="imagepreview" style="width: 100%;">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('footerPlaceholder')
@parent
<script type="text/javascript">
  $(function() {
      $('.pop').on('click', function() {
        $('.imagepreview').attr('src', $(this).find('img').attr('src'));
        $('#imagemodal').modal('show');
      });

      $("#view-account-area").hide();

      $("#view_account, #account-summary-link").on('click', function() {
        $("#view-account-area").toggle();
        $("#account-summary-table").toggle();
      });
  });
</script>
@endsection
