@extends('layouts.app')

@section('view')
<div class="container" >
    <div style="height:600px;">
        <object data="{{ url( $helpPdf ) }}" type="application/pdf" width="100%" height="100%">
            <iframe src="{{ url( $helpPdf ) }}" width="100%" height="100%" style="border: none;">This browser does not support PDFs. Please download the PDF to view it: <a href="{{ url( $helpPdf ) }}">Download PDF</a>
            </iframe>
        </object>
    </div><!--/content-->
</div> <!-- /container -->
@endsection

