@extends('layouts.app')

@section('view')
<div class="container">
	<div id="content">
		<div class="row">
			<div class="col-md-12">
				<div class="view-bill-upper">
					<div class="row">
						<div class="col-lg-12 title">
							<h2><a href="{{ MigrationHelper::getAppBaseUrl() }}billing">Account Summary</a> &gt; Billing</h2>
							<hr/>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3"><label for="view_account_number">Account Number</label></div>
						<div class="col-md-3"><p>{{$billing->account_number}}</p></div>
						<div class="col-md-3"><label for="view_account_number">Oustanding Balance</label></div>
						<div class="col-md-3"><p>{{ number_format( $billing->total_amount, 2, ".", ",") }}</p></div>
					</div>
					<div class="row">
						<div class="col-md-3"><label for="view_businessName">Business Name</label></div>
						<div class="col-md-3"><p>{{$billing->business_name}}</p></div>
						<div class="col-md-3"><label for="view_businessName">As of</label></div>
						<div class="col-md-3"><p class="form-control-static">{{ Carbon\Carbon::parse($billing->statement_date)->format('F j, Y h:i:s A') }}</p></div>
					</div>
					<div class="row">
						<div class="col-md-3"><label for="view_businessType">Business Type</label></div>
						<div class="col-md-3">
							<p>
								@foreach ($merchant->business_type_name as $business_type)
								{{ $business_type->name }} <br />
								@endforeach
							</p>
						</div>
						<div class="col-md-3"><label for="view_businessName">Branches:</label></div>
						<div class="col-md-3"><p>{{ $billing->active_branches_count }}</p></div>
					</div>
					<div class="row">
						<div class="col-md-3"><label for="view_businessType">Location</label></div>
						<div class="col-md-3"><p>{{$billing->location}}</p></div>
						<div class="col-md-3"><label for="view_businessType">RUSH Solution</label></div>
						<div class="col-md-3"><p>{{ $billing->package->name }}({{ $billing->status }})</p></div>
					</div>
					<div class="row">
						<div class="col-md-3"><label for="view_businessType">Authorized Rep</label></div>
						<div class="col-md-3"><p>{{$billing->authorized_rep}}</p></div>
						<div class="col-md-3"><label for="view_businessType">Contact Number</label></div>
						<div class="col-md-3"><p>{{$billing->contact_number}}</p></div>
					</div>
					<div class="row">
						<div class="col-md-12 text-right">
							@if ( $billing->is_paid == 0 && $billing->is_latest )
							<a class="btn btn-primary submit-payment-button" href="{{ MigrationHelper::getAppBaseUrl() }}billing/{{$billing->id}}/payment">Submit Payment</a>
							@endif
							<a class="btn btn-primary" href="{{ MigrationHelper::getAppBaseUrl() }}billing/statement/{{$billing->id}}" target="_new">Download SOA</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="view-bill-statement">
			<div class="row">
				<div class="col-lg-6">
					<!-- ///////////////// Left column ///////////////// -->
					<div class="row">
						<div class="col-lg-12"><img src="{{ MigrationHelper::getServerUrl() }}repository/merchant/barcode/{{$billing->barcode}}"/></div>
						<div class="col-lg-12 soa_acctNum">{{ $billing->account_number }}</div>
					</div>
					<h3 class="soa_heading1">{{ $billing->authorized_rep }}</h3>
					<p id="soa_name">
						{{ $merchant->billingName or $billing->business_name }}
					</p>
					<p id="soa_address">
						{{ $merchant->street1 }}<br />
						{{ $merchant->street2 }}<br />
						{{ $merchant->address }}<br />
						<b>TIN #:</b> {{ $merchant->TIN }}
					</p>
					<h3 class="soa_heading1">Statement Summary</h3>
					<table class="table statement_summary">
						{{--*/ $remaining_bal = 0 /*--}}
						@if ( !empty($previous_bill) )
						<thead>
							<tr class="soa_heading2" style="border-bottom:1px solid #f9f9f9;">
								<th style="width:75%;">Previous Bill Charges</th>
								<th class="text-right">Amount</th>
							</tr>
						</thead>
						<tbody>
							<tr class="soa_item_list" style="border-top: none !important;">
								<td class="soa_items">Amount Due from Previous Bill</td>
								<td class="soa_amounts text-right">{{ number_format( $previous_bill->total_amount, 2, ".", ",") }}</td>
							</tr>
							<tr class="soa_item_list">
								<td class="soa_items">Less: Payments</td>
								<td class="soa_amounts text-right">({{ number_format( $previous_bill->payment_amount, 2, ".", ",") }})</td>
							</tr>
							<tr>
								<td colspan=2 style="padding:0px!important;">
									<hr/>
								</td>
							</tr>
							<tr>
								<td class="soa_items">Remaining Balance from Previous Bill</td>
								{{--*/ $remaining_bal = $previous_bill->total_amount - $previous_bill->payment_amount /*--}}
								<td class="soa_amounts text-right">{{ number_format( $remaining_bal, 2, ".", ",") }}</td>
							</tr>
						</tbody>
						@endif
					</table>
					<!-- <table class="table">
						<thead>
							<tr class="soa_heading2">
								<th style="width:75%;">Current Bill Charges</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr class="soa_item_list" >
								<td class="soa_items">Service Fee</td>
								<td class="soa_amounts text-right">{{ number_format( $billing->service_fee, 2, ".", ",") }}</td>
							</tr>
							@if ( $merchant->add_ons->count() > 0 )
							<tr class="soa_item_list" >
								<td class="soa_items">Add-Ons</td>
								<td class="soa_amounts text-right">{{ number_format( $billing->add_ons_fee, 2, ".", ",") }}</td>
							</tr>
							@endif
							<tr class="soa_item_list" >
								<td class="soa_items">VAT</td>
								<td class="soa_amounts text-right">{{ number_format( $billing->vat_fee, 2, ".", ",") }}</td>
							</tr>
							<tr>
								<td colspan=2 style="padding:0px!important;">
									<hr/>
								</td>
							</tr>
							<tr>
								<td class="soa_items">Total Current Bill</td>
								<td class="soa_amounts text-right">{{ number_format( $billing->total_current, 2, ".", ",") }}</td>
							</tr>
							{{--*/ $total_amount = $billing->total_amount /*--}}
							@if ( $merchant->status == 'Trial' )
							<tr>
								<td class="soa_items">Less: Payments(Trial)</td>
								<td class="soa_amounts text-right">- {{ number_format( $billing->total_current, 2, ".", ",") }}</td>
							</tr>
							{{--*/ $total_amount = $billing->total_amount - $billing->total_current /*--}}
							@endif
							<tr class="soa_totalAmtDue orange_box">
								<td>Total Amount Due</td>
								<td class="text-right">{{ number_format( $total_amount, 2, ".", ",") }}</td>
							</tr>
						</tbody>
					</table> -->
				</div>
				<div class="col-lg-6">
					<!-- ///////////////// Right column ///////////////// -->
					<div class="col-lg-12">
						<div>
							<div class="account_summary">
								<table class="table">
									<thead>
										<tr>
											<th class="soa_heading1" colspan="2"><span>Account Summary</span><br><br></th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="soa_items">Account Number</td>
											<td class="text-right soa_amounts">{{ $billing->account_number }}</td>
										</tr>
										<tr>
											<td class="soa_items">RUSH Solution</td>
											<td class="text-right soa_amounts">{{ $billing->package->name }}({{ $billing->status }})</td>
										</tr>
										<tr>
											<td class="soa_items">Billing Number</td>
											<td class="text-right soa_amounts">{{ $billing->merchant_id . '-'. $billing->billing_number }}</td>
										</tr>
										<tr>
											<td class="soa_items">Billing Period</td>
											<td class="text-right soa_amounts">{{ Carbon\Carbon::parse($billing->billing_period_start)->format('M j') }} - {{ Carbon\Carbon::parse($billing->billing_period_end)->format('M j, Y') }}</td>
										</tr>
										<tr>
											<td class="soa_items">Statement Date</td>
											<td class="text-right soa_amounts">{{ Carbon\Carbon::parse($billing->statement_date)->format('M j, Y') }}</td>
										</tr>
										<tr class="soa_total_label orange_box">
											<td>Due Date</td>
											<td  class="text-right" style="font-family:Open Sans Semibold;">{{ Carbon\Carbon::parse($billing->due_date)->format('M j, Y') }}</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div>
								<p class="soa_note"><em>
									Please examine your Statement of Account immediately. If no discrepancy is reported within 30 days from this bill's cut-off date, the contents of this statement will be considered correct. Thank you.
								</em>
							</p>
						</div>
					</div>
				</div>
			</div>
			<!-- ///////////////// Bottom Row ///////////////// -->
			<div class="col-lg-12 usage_breakdown"><hr/>
				<h3 class="soa_heading1">Usage Breakdown</h3>
				<table class="table">
					<thead>
						<tr class="soa_heading2">
							<th style="width:25%;">Description</th>
							<th class="text-right">Quantity</th>
							<th class="text-right">Amount</th>
							<th class="text-right">Total</th>
						</tr>
					</thead>
					<tbody>
						{{--*/ $service_breakdown = json_decode($billing->service_breakdown) /*--}}
						<tr>
							<td class="soa_items">Monthly Service Fee @if ( $merchant->status == 'Trial' ) *Trial @endif</td>
							<td class="soa_items text-right"></td>
							<td class="soa_items text-right"></td>
							<td class="soa_items text-right"></td>
						</tr>

						@if ( isset($service_breakdown->service_fee) )
						<tr>
							<td class="soa_items" style="padding-left:40px!important;">Service Fee {{ $service_breakdown->service_fee_label or '' }}</td>
							<td class="soa_items text-right">{{ $service_breakdown->service_quantity }}</td>
							<td class="soa_items text-right">{{ number_format( $service_breakdown->service_fee, 2, ".", ",") }}</td>
							@if ( $billing->package_id == 1 )
								@if ( isset($service_breakdown->msf_type) && $service_breakdown->msf_type == 'fixed' )
									<td class="soa_items text-right">{{ number_format( ($service_breakdown->service_fee * $service_breakdown->service_quantity ), 2, ".", ",") }}</td>
								@else
									<td class="soa_items text-right">{{ @number_format( ($service_breakdown->service_fee * ( $service_breakdown->service_quantity / 100 ) ), 2, ".", ",") }}</td>
								@endif
							@else
							<td class="soa_items text-right">{{ number_format( ($service_breakdown->service_fee * $service_breakdown->service_quantity ), 2, ".", ",") }}</td>
							@endif
						</tr>
						@endif
						
						@if ( isset($service_breakdown->additional_sms_transaction) && isset($service_breakdown->additional_sms_transaction_fee) && $service_breakdown->additional_sms_transaction > 0 )
						<tr>
							<td class="soa_items" style="padding-left:40px!important;">SMS Transactions</td>
							<td class="soa_items text-right">{{ $service_breakdown->additional_sms_transaction }}</td>
							<td class="soa_items text-right">{{ number_format( $service_breakdown->additional_sms_transaction_charge, 2, ".", ",") }}</td>
							<td class="soa_items text-right">{{ number_format( $service_breakdown->additional_sms_transaction_fee, 2, ".", ",") }}</td>
						</tr>
						@endif

						@if ( isset($service_breakdown->merchant_sms_broadcast) && isset($service_breakdown->merchant_sms_broadcast_fee) && $service_breakdown->merchant_sms_broadcast > 0 )
						<tr>
							<td class="soa_items" style="padding-left:40px!important;">SMS Broadcasts</td>
							<td class="soa_items text-right">{{ $service_breakdown->merchant_sms_broadcast }}</td>
							<td class="soa_items text-right">{{ number_format( $service_breakdown->merchant_sms_broadcast_charge, 2, ".", ",") }}</td>
							<td class="soa_items text-right">{{ number_format( $service_breakdown->merchant_sms_broadcast_fee, 2, ".", ",") }}</td>
						</tr>
						@endif
						
						@if ( isset($service_breakdown->additional_admin_rewards_transactions) && isset($service_breakdown->additional_admin_rewards_transactions) && $service_breakdown->additional_admin_rewards_transactions > 0 )
						<tr>
							<td class="soa_items" style="padding-left:40px!important;">Globe SKUs Redemption</td>
							<td class="soa_items text-right"></td>
							<td class="soa_items text-right">{{ number_format( $service_breakdown->additional_admin_rewards_transactions, 2, ".", ",") }}</td>
							<td class="soa_items text-right">{{ number_format( $service_breakdown->additional_admin_rewards_transactions, 2, ".", ",") }}</td>
						</tr>
						@endif
						@if ( isset($service_breakdown->cms_tools_fee) )
						<tr>
							<td class="soa_items" style="padding-left:40px!important;">SMS Tool Fee</td>
							<td class="soa_items text-right">1</td>
							<td class="soa_items text-right">{{ number_format( $service_breakdown->cms_tools_fee, 2, ".", ",") }}</td>
							<td class="soa_items text-right">{{ number_format( ( $service_breakdown->cms_tools_fee / 1.12 ), 2, ".", ",") }}</td>
						</tr>
						@endif
						<tr>
							<td class="soa_items" colspan="4">Add-ons</td>
						</tr>
						@if ( $merchant->add_ons->count() > 0 || $billing->add_ons_fee > 0 )
						@foreach ($merchant->add_ons as $add_ons )
						
						@if ( $add_ons->id == 1 && $additional_sms_credits['qty'] > 0 )
						<tr>
							<td class="soa_items" style="padding-left:40px!important;">{{ $add_ons->name }}</td>
							<td class="soa_items text-right">{{ $additional_sms_credits['qty'] }}</td>
							<td class="soa_items text-right">0.45</td>
							<td class="soa_items text-right">{{ number_format( $additional_sms_credits['fee'], 2, ".", "," ) }}</td>
						</tr>
						@endif
						
						@if ( $add_ons->id == 2 && $app_zero_rating['qty'] > 0 )
						<tr>
							<td class="soa_items" style="padding-left:40px!important;">{{ $add_ons->name }}</td>
							<td class="soa_items text-right">{{ $app_zero_rating['qty'] }}</td>
							<td class="soa_items text-right">1.79</td>
							<td class="soa_items text-right">{{ number_format( $app_zero_rating['fee'], 2, ".", "," ) }}</td>
						</tr>
						@endif
						
						@if ( $add_ons->id == 3 && $white_labeled_card['qty'] > 0 )
						<tr>
							<td class="soa_items" style="padding-left:40px!important;">{{ $add_ons->name }}</td>
							<td class="soa_items text-right">{{ $white_labeled_card['qty'] }}</td>
							<td class="soa_items text-right">50.00</td>
							<td class="soa_items text-right">{{ number_format( $white_labeled_card['fee'], 2, ".", "," ) }}</td>
						</tr>
						@endif
						@endforeach
						@endif

						@if( $billing->billing_add_ons )
						{{--*/ $custom_addons = json_decode($billing->billing_add_ons) /*--}}
							@foreach( $custom_addons as $custom_addon )
								<tr>
									<td class="soa_items" style="padding-left:40px!important;">{{ $custom_addon->description }}</td>
									<td class="soa_items text-right">{{ $custom_addon->quantity }}</td>
									<td class="soa_items text-right">{{ number_format( $custom_addon->amount, 2, ".", "," ) }}</td>
									@if ( $custom_addon->vat )
									<td class="soa_items text-right">{{ number_format( ( $custom_addon->quantity * $custom_addon->amount ) / 1.12 , 2, ".", "," ) }}</td>
									@else
									<td class="soa_items text-right">{{ number_format( $custom_addon->quantity * $custom_addon->amount, 2, ".", "," ) }}</td>
									@endif
								</tr>
							@endforeach
						@endif

						<tr>
							<td>VAT</td>
							<td></td>
							<td></td>
							<td class="text-right" style="font-family:Open Sans Semibold;">{{ number_format( $billing->vat_fee, 2, ".", ",") }}</td>
						</tr>

						<tr>
							<td>Total Current Bill</td>
							<td></td>
							<td></td>
							<td class="text-right">{{ number_format( $billing->total_current, 2, ".", ",") }}</td>
						</tr>
						<tr>
							<td>Total Previous Bill</td>
							<td></td>
							<td></td>
							<td class="text-right">{{ number_format( $remaining_bal, 2, ".", ",") }}</td>
						</tr>
						<tr class="soa_total_label breakdown_total">
							<td style="font-weight:bold;">Total Amount Due</td>
							<td></td>
							<td class="text-right"></td>
							<td class="text-right" style="font-family:Open Sans Semibold;">{{ number_format( $billing->total_amount, 2, ".", ",") }}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div> <!-- /container -->
@endsection
