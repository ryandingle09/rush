(function ($) {

    var achievementUnlock = $('.setup-achievement-unlock');

    $(document).ready(function () {
        init();
        validations();
        csvFiles();
        givePoints();
        giveStamps();
    });

    function init() {
        achievementUnlock.find('.numeric').numeric();

        // clear form on reset
        var optionListsForm = achievementUnlock.find('form.achievement-option-lists');
        var optionlistformresetbutton = achievementUnlock.find('button.resetbutton');
        optionlistformresetbutton.click(function (e) {
            e.preventdefault();
            optionlistsform.clearform();
        });

        optionListsForm.submit(function () {
            if ($(this).find('.has-error').length > 0) {
                $(this).find('.has-error').focus();
                return false;
            }
            return true;
        });

    }

    function validations() {
        achievementUnlock.find('input[type=checkbox]').change(function () {
            var row = $(this).closest('.fields');
            if ($(this).is(":checked")) {
                row.find('.pts-cont').removeClass('hide');
                row.find('.points').focus();
                row.find('input[type=text]').each(function (index) {
                    $(this).prop('required', true);
                });
                // row.find('.give-points').removeAttr('disabled');
            } else {
                row.find('input[type=text]').each(function (index) {
                    row.find('.pts-cont').addClass('hide');
                    $(this).removeAttr('required');
                });
                // row.find('.give-points').attr('disabled', true);
            }
        });
        achievementUnlock.find('input[type=checkbox]').each(function () {
            var row = $(this).closest('.fields');
            if ($(this).is(":checked")) {
                row.find('.pts-cont').removeClass('hide');
            } else {
                row.find('.pts-cont').addClass('hide');
            }
        });
    }

    function csvFiles() {
        $('.input-file-csv').change(function () {
            var input = $(this);
            var csv = $(this).prop('files')[0];
            var extension = input.val().replace(/^.*\./, '');
            if (csv && extension == 'csv') {
                fr = new FileReader();
                fr.onload = function () {
                    input.parent().find('input[type=hidden]').val(fr.result);
                    input.parent('.form-group').removeClass('has-error');
                }
                fr.readAsText(csv);
            } else {
                input.parent().find('input[type=hidden]').val('');
                input.parent('.form-group').addClass('has-error');
            }
        });
    }

    function givePoints() {
        $('.row.fields').each(function () {
            console.log($(this).find('input[type=checkbox]:checked').length);
            if ($(this).find('input[type=checkbox]:checked').length) {
                $(this).find('.give-points').removeAttr('disabled');
            } else {
                $(this).find('.give-points').attr('disabled', true);
            }
        });

        $('.give-points').find('i').hide();
        $('.give-points').click(function (e) {
            var button = $(this);
            var token = $('input[name=_token]').val();
            button.attr('disabled', true);
            button.find('i').show();
            var row = $(this).closest('.fields');
            $.ajax({
                type: 'POST',
                url: RUSH.base_url + '/achievement-unlock/give-points',
                headers: {
                    'X-CSRF-TOKEN': token
                },
                data: { name: button.attr('data-attr-achievement_name') },
                success: function (response) {
                    button.removeAttr('disabled');
                    if (response.status) {
                        swal({
                            title: "Success",
                            text: "Achievement unlock points has been sent to customers."
                        },
                            function () {
                                window.location.href = window.location.href;
                            });
                    } else {
                        swal('There\'s a problem sending points.');
                    }
                }
            });
            e.preventDefault();
        });
    }


    function giveStamps() {
        $('.row.fields').each(function () {
            console.log($(this).find('input[type=checkbox]:checked').length);
            if ($(this).find('input[type=checkbox]:checked').length) {
                $(this).find('.give-stamps').removeAttr('disabled');
            } else {
                $(this).find('.give-stamps').attr('disabled', true);
            }
        });

        $('.give-stamps').find('i').hide();
        $('.give-stamps').click(function (e) {
            var button = $(this);
            var token = $('input[name=_token]').val();
            button.attr('disabled', true);
            button.find('i').show();
            var row = $(this).closest('.fields');
            $.ajax({
                type: 'POST',
                url: RUSH.base_url + '/achievement-unlock/give-stamps',
                headers: {
                    'X-CSRF-TOKEN': token
                },
                data: { name: button.attr('data-attr-achievement_name') },
                success: function (response) {
                    button.removeAttr('disabled');
                    if (response.status) {
                        swal({
                            title: "Success",
                            text: "Achievement unlock stamps has been sent to customers."
                        },
                            function () {
                                window.location.href = window.location.href;
                            });
                    } else {
                        swal('There\'s a problem sending stamps.');
                    }
                }
            });
            e.preventDefault();
        });
    }
})(jQuery);
