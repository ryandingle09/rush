(function ($) {
    $(document).ready(function () {
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
            $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
        }); 

        var emptyTableMessage = 'No matching records found';
        var excelExportButton = '<div class="text-center" id="export-btn-container"><button type="button" class="btn btn-default exportButton">Export to Excel</button></div>';

        $.extend($.fn.dataTableExt.oStdClasses, {
            'sWrapper': 'bootstrap-table dataTables_wrapper',
            'sFilter': 'pull-right search'
        });

        var datatableSpecs = {
            'order': [[ 0, 'desc' ]],
            'sDom': '<"fixed-table-toolbar"f><"excelButton hide"B><"customButton">rt<"bottom"ilp><"clear">',
            'buttons': [
                'excel'
            ],
            'language': {
                'emptyTable': emptyTableMessage
            },
            'oLanguage': {
               'sSearch': ''
            },
            'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
            'scrollX': true,
            'sScrollX' : '100%',
            'scrollY': '300px',
            'sScrollY' : '100%',
            'autoWidth': false
        };

        var dataTableAjax = {
            processing: true,
            serverSide: true
        };

        var customerStampsTable = $('#stampsSummaryTable').DataTable( $.extend({}, datatableSpecs, dataTableAjax, {
            
            ajax: {
                'url': customer_stamp_summary_base_url + '/customer-stamps-summary',
                'data': function(d) {
                            var punchcard = $('#punchardSelect').length ?  $('#punchardSelect').val() : '';
                            d.punchcard_id = punchcard;
                },

            },
            columns: [
                { data: 'timestamp', name: 'Date Registered' },
                { data: 'fullName', name: 'Customer Name' },
                { data: 'mobileNumber', name: 'Mobile No.' },
                { data: 'punchCard', name: 'Punch Card' },
                { data: 'validUntil', name: 'Valid Until' },
                { data: 'status', name: 'Status' },
                { data: 'target', name: 'Target' },
                { data: 'accomplished', name: 'Accomplished' },
                { data: 'balance', name: 'Balance' },
                { data: 'redemption_status', name: 'Redemption Status',
                    render : function(data, type, row){
                        var redemption_status = row.redemption_status;

                        if(redemption_status){
                            return "Yes";
                        }

                        return "No";
                    }
                },
            ],
            initComplete: function () {
                $(".customerStampsToolbar")
                    .insertAfter( "#stampsSummaryTable_wrapper .fixed-table-toolbar" );
                var bootstrapSearchInput = $('#stampsSummaryTable_wrapper .search input');
                bootstrapSearchInput
                    .attr('type', 'text')
                    .attr('placeholder', 'Search')
                    .addClass('form-control');
                $('select[name=stampsSummaryTable_length]').unwrap();
                $('#stampsSummaryTable_length').addClass('dataTables_info');
                var api = this.api();
                $('#punchardSelect').change(function() {
                    api.ajax.reload();
                });
                if (!enable_stamps_summary_punchcard_redemption) {
                    api.column(-1).visible(false);
                }
            }
        }));
        $('#stamps-summary').append(excelExportButton);
        $('#redeemAll').click(function(event){
            var punchcard = $('#punchardSelect').length ?  $('#punchardSelect').val() : '';
            var btn = $(this);
            btn.attr('disabled', 'disabled');
            btn.find('.fa-spin').removeClass('hide');
            $.ajax({
                    'url': customer_stamp_summary_base_url + '/redeem-stamps',
                    'method': 'POST',
                    'data': {punchcard_id: punchcard},
                    'headers': {
                        'X-CSRF-TOKEN': csrf_token
                    }
                })
                .done(function(data){
                    btn.removeAttr('disabled');
                    btn.find('.fa-spin').addClass('hide');
                    if (data.error_code !== '0x0') {
                        swal(data.message);
                    } else {
                        if (data.data) {
                            swal(data.data + " stamp card(s) has been redeemed.");
                            customerStampsTable.ajax.reload();
                        } else {
                            swal("No stamps to redeem!");
                        }
                    }
                });
        });

        var classSummaryTable = $('#classSummaryTable').DataTable($.extend({}, datatableSpecs, dataTableAjax, {

            ajax: {
                'url': customer_stamp_summary_base_url + '/class-package-summary',
            },
            columns: [
                {data: 'timestamp', name: 'Date Registered'},
                {data: 'fullName', name: 'Customer Name'},
                {data: 'mobileNumber', name: 'Mobile No.'},
                {data: 'punchCard', name: 'Package'},
                {data: 'validUntil', name: 'Valid Until'},
                {data: 'status', name: 'Status'},
                {data: 'target', name: 'Target'},
                {data: 'accomplished', name: 'Accomplished'},
                {data: 'balance', name: 'Balance'},
                {data: 'branch', name: 'Branch'},
                { data: 'status', name: 'Action',
                    render : function(data, type, row){
                        var status = row.status;

                        if(status === "Active"){
                            return "<a href='#' data-package-id='" + row.customerClassPackageId + "' class='expire-customer-package btn btn-xs btn-danger'>Expire</a>"
                        }

                        return '';
                    }
                }
            ],
            initComplete: function () {
                $('select[name=classSummaryTable_length]').unwrap();
                $('#classSummaryTable_length').addClass('dataTables_info');
                var api = this.api();
            }
        }));
        $('#class-summary').append(excelExportButton);

        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });

        $('#classSummaryTable').on('click', '.expire-customer-package', function(){
            var me = $(this);
            var customer_package_id = me.attr('data-package-id');
            var target_modal = $('#expirePackage');

            target_modal.find('[name="customer_class_package_id"]').val(customer_package_id);
            target_modal.modal('show');
        });
    });
})(jQuery);